/*
               File: K2BFixRecentLinksCaptionLength
        Description: Returns recent link caption with its length fixed
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:34.8
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bfixrecentlinkscaptionlength : GXProcedure
   {
      public k2bfixrecentlinkscaptionlength( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bfixrecentlinkscaptionlength( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Caption ,
                           out String aP1_NewCaption )
      {
         this.AV8Caption = aP0_Caption;
         this.AV9NewCaption = "" ;
         initialize();
         executePrivate();
         aP1_NewCaption=this.AV9NewCaption;
      }

      public String executeUdp( String aP0_Caption )
      {
         this.AV8Caption = aP0_Caption;
         this.AV9NewCaption = "" ;
         initialize();
         executePrivate();
         aP1_NewCaption=this.AV9NewCaption;
         return AV9NewCaption ;
      }

      public void executeSubmit( String aP0_Caption ,
                                 out String aP1_NewCaption )
      {
         k2bfixrecentlinkscaptionlength objk2bfixrecentlinkscaptionlength;
         objk2bfixrecentlinkscaptionlength = new k2bfixrecentlinkscaptionlength();
         objk2bfixrecentlinkscaptionlength.AV8Caption = aP0_Caption;
         objk2bfixrecentlinkscaptionlength.AV9NewCaption = "" ;
         objk2bfixrecentlinkscaptionlength.context.SetSubmitInitialConfig(context);
         objk2bfixrecentlinkscaptionlength.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bfixrecentlinkscaptionlength);
         aP1_NewCaption=this.AV9NewCaption;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bfixrecentlinkscaptionlength)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9NewCaption = AV8Caption;
         GXt_int1 = AV10Length;
         new k2bgetrecentlinkscaptionlength(context ).execute( out  GXt_int1) ;
         AV10Length = GXt_int1;
         if ( AV10Length > 3 )
         {
            AV10Length = (int)(AV10Length-3);
         }
         if ( StringUtil.Len( AV8Caption) > AV10Length )
         {
            AV9NewCaption = "..." + StringUtil.Substring( AV9NewCaption, StringUtil.Len( AV8Caption)-AV10Length+1, StringUtil.Len( AV8Caption));
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV10Length ;
      private int GXt_int1 ;
      private String AV8Caption ;
      private String AV9NewCaption ;
      private String aP1_NewCaption ;
   }

}
