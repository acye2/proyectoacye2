/*
               File: type_SdtRecorrido_Detalle
        Description: Recorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:39:0.37
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Recorrido.Detalle" )]
   [XmlType(TypeName =  "Recorrido.Detalle" , Namespace = "PACYE2" )]
   [Serializable]
   public class SdtRecorrido_Detalle : GxSilentTrnSdt, IGxSilentTrnGridItem
   {
      public SdtRecorrido_Detalle( )
      {
      }

      public SdtRecorrido_Detalle( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"RecorridoDetalleID", typeof(Guid)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Detalle");
         metadata.Set("BT", "RecorridoDetalle");
         metadata.Set("PK", "[ \"RecorridoDetalleID\" ]");
         metadata.Set("PKAssigned", "[ \"RecorridoDetalleID\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"PersonaID\" ],\"FKMap\":[ \"IDCoach-PersonaID\" ] },{ \"FK\":[ \"PersonaID\" ],\"FKMap\":[ \"RecorridoDetallePaciente-PersonaID\" ] },{ \"FK\":[ \"RecorridoID\" ],\"FKMap\":[  ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GxStringCollection StateAttributes( )
      {
         GxStringCollection state = new GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Modified");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Recorridodetalleid_Z");
         state.Add("gxTpr_Recorridodetallepaciente_Z");
         state.Add("gxTpr_Idcoach_Z");
         state.Add("gxTpr_Tiposenal_Z");
         state.Add("gxTpr_Tipoobstaculo_Z");
         state.Add("gxTpr_Incidente_Z");
         state.Add("gxTpr_Emergencia_Z");
         state.Add("gxTpr_Imei_Z");
         state.Add("gxTpr_Timestamp_Z_Nullable");
         state.Add("gxTpr_Recorridodetallepaciente_N");
         state.Add("gxTpr_Idcoach_N");
         state.Add("gxTpr_Tiposenal_N");
         state.Add("gxTpr_Longitud_N");
         state.Add("gxTpr_Latitud_N");
         state.Add("gxTpr_Tipoobstaculo_N");
         state.Add("gxTpr_Incidente_N");
         state.Add("gxTpr_Emergencia_N");
         state.Add("gxTpr_Imei_N");
         state.Add("gxTpr_Timestamp_N");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         SdtRecorrido_Detalle sdt ;
         sdt = (SdtRecorrido_Detalle)(source);
         gxTv_SdtRecorrido_Detalle_Recorridodetalleid = sdt.gxTv_SdtRecorrido_Detalle_Recorridodetalleid ;
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente = sdt.gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente ;
         gxTv_SdtRecorrido_Detalle_Idcoach = sdt.gxTv_SdtRecorrido_Detalle_Idcoach ;
         gxTv_SdtRecorrido_Detalle_Tiposenal = sdt.gxTv_SdtRecorrido_Detalle_Tiposenal ;
         gxTv_SdtRecorrido_Detalle_Longitud = sdt.gxTv_SdtRecorrido_Detalle_Longitud ;
         gxTv_SdtRecorrido_Detalle_Latitud = sdt.gxTv_SdtRecorrido_Detalle_Latitud ;
         gxTv_SdtRecorrido_Detalle_Tipoobstaculo = sdt.gxTv_SdtRecorrido_Detalle_Tipoobstaculo ;
         gxTv_SdtRecorrido_Detalle_Incidente = sdt.gxTv_SdtRecorrido_Detalle_Incidente ;
         gxTv_SdtRecorrido_Detalle_Emergencia = sdt.gxTv_SdtRecorrido_Detalle_Emergencia ;
         gxTv_SdtRecorrido_Detalle_Imei = sdt.gxTv_SdtRecorrido_Detalle_Imei ;
         gxTv_SdtRecorrido_Detalle_Timestamp = sdt.gxTv_SdtRecorrido_Detalle_Timestamp ;
         gxTv_SdtRecorrido_Detalle_Mode = sdt.gxTv_SdtRecorrido_Detalle_Mode ;
         gxTv_SdtRecorrido_Detalle_Modified = sdt.gxTv_SdtRecorrido_Detalle_Modified ;
         gxTv_SdtRecorrido_Detalle_Initialized = sdt.gxTv_SdtRecorrido_Detalle_Initialized ;
         gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z = sdt.gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z ;
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z = sdt.gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z ;
         gxTv_SdtRecorrido_Detalle_Idcoach_Z = sdt.gxTv_SdtRecorrido_Detalle_Idcoach_Z ;
         gxTv_SdtRecorrido_Detalle_Tiposenal_Z = sdt.gxTv_SdtRecorrido_Detalle_Tiposenal_Z ;
         gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z = sdt.gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z ;
         gxTv_SdtRecorrido_Detalle_Incidente_Z = sdt.gxTv_SdtRecorrido_Detalle_Incidente_Z ;
         gxTv_SdtRecorrido_Detalle_Emergencia_Z = sdt.gxTv_SdtRecorrido_Detalle_Emergencia_Z ;
         gxTv_SdtRecorrido_Detalle_Imei_Z = sdt.gxTv_SdtRecorrido_Detalle_Imei_Z ;
         gxTv_SdtRecorrido_Detalle_Timestamp_Z = sdt.gxTv_SdtRecorrido_Detalle_Timestamp_Z ;
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N = sdt.gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N ;
         gxTv_SdtRecorrido_Detalle_Idcoach_N = sdt.gxTv_SdtRecorrido_Detalle_Idcoach_N ;
         gxTv_SdtRecorrido_Detalle_Tiposenal_N = sdt.gxTv_SdtRecorrido_Detalle_Tiposenal_N ;
         gxTv_SdtRecorrido_Detalle_Longitud_N = sdt.gxTv_SdtRecorrido_Detalle_Longitud_N ;
         gxTv_SdtRecorrido_Detalle_Latitud_N = sdt.gxTv_SdtRecorrido_Detalle_Latitud_N ;
         gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N = sdt.gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N ;
         gxTv_SdtRecorrido_Detalle_Incidente_N = sdt.gxTv_SdtRecorrido_Detalle_Incidente_N ;
         gxTv_SdtRecorrido_Detalle_Emergencia_N = sdt.gxTv_SdtRecorrido_Detalle_Emergencia_N ;
         gxTv_SdtRecorrido_Detalle_Imei_N = sdt.gxTv_SdtRecorrido_Detalle_Imei_N ;
         gxTv_SdtRecorrido_Detalle_Timestamp_N = sdt.gxTv_SdtRecorrido_Detalle_Timestamp_N ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("RecorridoDetalleID", gxTv_SdtRecorrido_Detalle_Recorridodetalleid, false);
         AddObjectProperty("RecorridoDetallePaciente", gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente, false);
         AddObjectProperty("RecorridoDetallePaciente_N", gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N, false);
         AddObjectProperty("IDCoach", gxTv_SdtRecorrido_Detalle_Idcoach, false);
         AddObjectProperty("IDCoach_N", gxTv_SdtRecorrido_Detalle_Idcoach_N, false);
         AddObjectProperty("TipoSenal", gxTv_SdtRecorrido_Detalle_Tiposenal, false);
         AddObjectProperty("TipoSenal_N", gxTv_SdtRecorrido_Detalle_Tiposenal_N, false);
         AddObjectProperty("Longitud", gxTv_SdtRecorrido_Detalle_Longitud, false);
         AddObjectProperty("Longitud_N", gxTv_SdtRecorrido_Detalle_Longitud_N, false);
         AddObjectProperty("Latitud", gxTv_SdtRecorrido_Detalle_Latitud, false);
         AddObjectProperty("Latitud_N", gxTv_SdtRecorrido_Detalle_Latitud_N, false);
         AddObjectProperty("TipoObstaculo", gxTv_SdtRecorrido_Detalle_Tipoobstaculo, false);
         AddObjectProperty("TipoObstaculo_N", gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N, false);
         AddObjectProperty("Incidente", gxTv_SdtRecorrido_Detalle_Incidente, false);
         AddObjectProperty("Incidente_N", gxTv_SdtRecorrido_Detalle_Incidente_N, false);
         AddObjectProperty("Emergencia", gxTv_SdtRecorrido_Detalle_Emergencia, false);
         AddObjectProperty("Emergencia_N", gxTv_SdtRecorrido_Detalle_Emergencia_N, false);
         AddObjectProperty("IMEI", gxTv_SdtRecorrido_Detalle_Imei, false);
         AddObjectProperty("IMEI_N", gxTv_SdtRecorrido_Detalle_Imei_N, false);
         datetime_STZ = gxTv_SdtRecorrido_Detalle_Timestamp;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("TimeStamp", sDateCnv, false);
         AddObjectProperty("TimeStamp_N", gxTv_SdtRecorrido_Detalle_Timestamp_N, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtRecorrido_Detalle_Mode, false);
            AddObjectProperty("Modified", gxTv_SdtRecorrido_Detalle_Modified, false);
            AddObjectProperty("Initialized", gxTv_SdtRecorrido_Detalle_Initialized, false);
            AddObjectProperty("RecorridoDetalleID_Z", gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z, false);
            AddObjectProperty("RecorridoDetallePaciente_Z", gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z, false);
            AddObjectProperty("IDCoach_Z", gxTv_SdtRecorrido_Detalle_Idcoach_Z, false);
            AddObjectProperty("TipoSenal_Z", gxTv_SdtRecorrido_Detalle_Tiposenal_Z, false);
            AddObjectProperty("TipoObstaculo_Z", gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z, false);
            AddObjectProperty("Incidente_Z", gxTv_SdtRecorrido_Detalle_Incidente_Z, false);
            AddObjectProperty("Emergencia_Z", gxTv_SdtRecorrido_Detalle_Emergencia_Z, false);
            AddObjectProperty("IMEI_Z", gxTv_SdtRecorrido_Detalle_Imei_Z, false);
            datetime_STZ = gxTv_SdtRecorrido_Detalle_Timestamp_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("TimeStamp_Z", sDateCnv, false);
            AddObjectProperty("RecorridoDetallePaciente_N", gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N, false);
            AddObjectProperty("IDCoach_N", gxTv_SdtRecorrido_Detalle_Idcoach_N, false);
            AddObjectProperty("TipoSenal_N", gxTv_SdtRecorrido_Detalle_Tiposenal_N, false);
            AddObjectProperty("Longitud_N", gxTv_SdtRecorrido_Detalle_Longitud_N, false);
            AddObjectProperty("Latitud_N", gxTv_SdtRecorrido_Detalle_Latitud_N, false);
            AddObjectProperty("TipoObstaculo_N", gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N, false);
            AddObjectProperty("Incidente_N", gxTv_SdtRecorrido_Detalle_Incidente_N, false);
            AddObjectProperty("Emergencia_N", gxTv_SdtRecorrido_Detalle_Emergencia_N, false);
            AddObjectProperty("IMEI_N", gxTv_SdtRecorrido_Detalle_Imei_N, false);
            AddObjectProperty("TimeStamp_N", gxTv_SdtRecorrido_Detalle_Timestamp_N, false);
         }
         return  ;
      }

      public void UpdateDirties( SdtRecorrido_Detalle sdt )
      {
         if ( sdt.IsDirty("RecorridoDetalleID") )
         {
            gxTv_SdtRecorrido_Detalle_Recorridodetalleid = sdt.gxTv_SdtRecorrido_Detalle_Recorridodetalleid ;
         }
         if ( sdt.IsDirty("RecorridoDetallePaciente") )
         {
            gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente = sdt.gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente ;
         }
         if ( sdt.IsDirty("IDCoach") )
         {
            gxTv_SdtRecorrido_Detalle_Idcoach = sdt.gxTv_SdtRecorrido_Detalle_Idcoach ;
         }
         if ( sdt.IsDirty("TipoSenal") )
         {
            gxTv_SdtRecorrido_Detalle_Tiposenal = sdt.gxTv_SdtRecorrido_Detalle_Tiposenal ;
         }
         if ( sdt.IsDirty("Longitud") )
         {
            gxTv_SdtRecorrido_Detalle_Longitud = sdt.gxTv_SdtRecorrido_Detalle_Longitud ;
         }
         if ( sdt.IsDirty("Latitud") )
         {
            gxTv_SdtRecorrido_Detalle_Latitud = sdt.gxTv_SdtRecorrido_Detalle_Latitud ;
         }
         if ( sdt.IsDirty("TipoObstaculo") )
         {
            gxTv_SdtRecorrido_Detalle_Tipoobstaculo = sdt.gxTv_SdtRecorrido_Detalle_Tipoobstaculo ;
         }
         if ( sdt.IsDirty("Incidente") )
         {
            gxTv_SdtRecorrido_Detalle_Incidente = sdt.gxTv_SdtRecorrido_Detalle_Incidente ;
         }
         if ( sdt.IsDirty("Emergencia") )
         {
            gxTv_SdtRecorrido_Detalle_Emergencia = sdt.gxTv_SdtRecorrido_Detalle_Emergencia ;
         }
         if ( sdt.IsDirty("IMEI") )
         {
            gxTv_SdtRecorrido_Detalle_Imei = sdt.gxTv_SdtRecorrido_Detalle_Imei ;
         }
         if ( sdt.IsDirty("TimeStamp") )
         {
            gxTv_SdtRecorrido_Detalle_Timestamp = sdt.gxTv_SdtRecorrido_Detalle_Timestamp ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "RecorridoDetalleID" )]
      [  XmlElement( ElementName = "RecorridoDetalleID"   )]
      public Guid gxTpr_Recorridodetalleid
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Recorridodetalleid ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Recorridodetalleid = (Guid)(value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Recorridodetalleid");
         }

      }

      [  SoapElement( ElementName = "RecorridoDetallePaciente" )]
      [  XmlElement( ElementName = "RecorridoDetallePaciente"   )]
      public Guid gxTpr_Recorridodetallepaciente
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N = 0;
            gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente = (Guid)(value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Recorridodetallepaciente");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N = 1;
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "IDCoach" )]
      [  XmlElement( ElementName = "IDCoach"   )]
      public Guid gxTpr_Idcoach
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Idcoach ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Idcoach_N = 0;
            gxTv_SdtRecorrido_Detalle_Idcoach = (Guid)(value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Idcoach");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Idcoach_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Idcoach_N = 1;
         gxTv_SdtRecorrido_Detalle_Idcoach = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Idcoach_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoSenal" )]
      [  XmlElement( ElementName = "TipoSenal"   )]
      public short gxTpr_Tiposenal
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Tiposenal ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Tiposenal_N = 0;
            gxTv_SdtRecorrido_Detalle_Tiposenal = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Tiposenal");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Tiposenal_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Tiposenal_N = 1;
         gxTv_SdtRecorrido_Detalle_Tiposenal = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Tiposenal_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Longitud" )]
      [  XmlElement( ElementName = "Longitud"   )]
      public String gxTpr_Longitud
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Longitud ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Longitud_N = 0;
            gxTv_SdtRecorrido_Detalle_Longitud = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Longitud");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Longitud_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Longitud_N = 1;
         gxTv_SdtRecorrido_Detalle_Longitud = "";
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Longitud_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Latitud" )]
      [  XmlElement( ElementName = "Latitud"   )]
      public String gxTpr_Latitud
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Latitud ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Latitud_N = 0;
            gxTv_SdtRecorrido_Detalle_Latitud = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Latitud");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Latitud_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Latitud_N = 1;
         gxTv_SdtRecorrido_Detalle_Latitud = "";
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Latitud_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoObstaculo" )]
      [  XmlElement( ElementName = "TipoObstaculo"   )]
      public short gxTpr_Tipoobstaculo
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Tipoobstaculo ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N = 0;
            gxTv_SdtRecorrido_Detalle_Tipoobstaculo = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Tipoobstaculo");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Tipoobstaculo_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N = 1;
         gxTv_SdtRecorrido_Detalle_Tipoobstaculo = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Tipoobstaculo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Incidente" )]
      [  XmlElement( ElementName = "Incidente"   )]
      public short gxTpr_Incidente
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Incidente ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Incidente_N = 0;
            gxTv_SdtRecorrido_Detalle_Incidente = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Incidente");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Incidente_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Incidente_N = 1;
         gxTv_SdtRecorrido_Detalle_Incidente = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Incidente_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Emergencia" )]
      [  XmlElement( ElementName = "Emergencia"   )]
      public short gxTpr_Emergencia
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Emergencia ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Emergencia_N = 0;
            gxTv_SdtRecorrido_Detalle_Emergencia = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Emergencia");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Emergencia_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Emergencia_N = 1;
         gxTv_SdtRecorrido_Detalle_Emergencia = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Emergencia_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "IMEI" )]
      [  XmlElement( ElementName = "IMEI"   )]
      public String gxTpr_Imei
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Imei ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Imei_N = 0;
            gxTv_SdtRecorrido_Detalle_Imei = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Imei");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Imei_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Imei_N = 1;
         gxTv_SdtRecorrido_Detalle_Imei = "";
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Imei_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TimeStamp" )]
      [  XmlElement( ElementName = "TimeStamp"  , IsNullable=true )]
      public string gxTpr_Timestamp_Nullable
      {
         get {
            if ( gxTv_SdtRecorrido_Detalle_Timestamp == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtRecorrido_Detalle_Timestamp).value ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Timestamp_N = 0;
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtRecorrido_Detalle_Timestamp = DateTime.MinValue;
            else
               gxTv_SdtRecorrido_Detalle_Timestamp = DateTime.Parse( value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Timestamp
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Timestamp ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Timestamp_N = 0;
            gxTv_SdtRecorrido_Detalle_Timestamp = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Timestamp");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Timestamp_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Timestamp_N = 1;
         gxTv_SdtRecorrido_Detalle_Timestamp = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Timestamp_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Mode ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Mode_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Mode = "";
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Modified" )]
      [  XmlElement( ElementName = "Modified"   )]
      public short gxTpr_Modified
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Modified ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Modified = value;
            SetDirty("Modified");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Modified_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Modified = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Modified_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Initialized ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Initialized = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Initialized");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Initialized_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RecorridoDetalleID_Z" )]
      [  XmlElement( ElementName = "RecorridoDetalleID_Z"   )]
      public Guid gxTpr_Recorridodetalleid_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z = (Guid)(value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Recorridodetalleid_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RecorridoDetallePaciente_Z" )]
      [  XmlElement( ElementName = "RecorridoDetallePaciente_Z"   )]
      public Guid gxTpr_Recorridodetallepaciente_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z = (Guid)(value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Recorridodetallepaciente_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "IDCoach_Z" )]
      [  XmlElement( ElementName = "IDCoach_Z"   )]
      public Guid gxTpr_Idcoach_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Idcoach_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Idcoach_Z = (Guid)(value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Idcoach_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Idcoach_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Idcoach_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Idcoach_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoSenal_Z" )]
      [  XmlElement( ElementName = "TipoSenal_Z"   )]
      public short gxTpr_Tiposenal_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Tiposenal_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Tiposenal_Z = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Tiposenal_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Tiposenal_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Tiposenal_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Tiposenal_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoObstaculo_Z" )]
      [  XmlElement( ElementName = "TipoObstaculo_Z"   )]
      public short gxTpr_Tipoobstaculo_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Tipoobstaculo_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Incidente_Z" )]
      [  XmlElement( ElementName = "Incidente_Z"   )]
      public short gxTpr_Incidente_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Incidente_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Incidente_Z = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Incidente_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Incidente_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Incidente_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Incidente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Emergencia_Z" )]
      [  XmlElement( ElementName = "Emergencia_Z"   )]
      public short gxTpr_Emergencia_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Emergencia_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Emergencia_Z = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Emergencia_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Emergencia_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Emergencia_Z = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Emergencia_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "IMEI_Z" )]
      [  XmlElement( ElementName = "IMEI_Z"   )]
      public String gxTpr_Imei_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Imei_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Imei_Z = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Imei_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Imei_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Imei_Z = "";
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Imei_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TimeStamp_Z" )]
      [  XmlElement( ElementName = "TimeStamp_Z"  , IsNullable=true )]
      public string gxTpr_Timestamp_Z_Nullable
      {
         get {
            if ( gxTv_SdtRecorrido_Detalle_Timestamp_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtRecorrido_Detalle_Timestamp_Z).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtRecorrido_Detalle_Timestamp_Z = DateTime.MinValue;
            else
               gxTv_SdtRecorrido_Detalle_Timestamp_Z = DateTime.Parse( value);
            gxTv_SdtRecorrido_Detalle_Modified = 1;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Timestamp_Z
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Timestamp_Z ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Timestamp_Z = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Timestamp_Z");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Timestamp_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Timestamp_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Timestamp_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RecorridoDetallePaciente_N" )]
      [  XmlElement( ElementName = "RecorridoDetallePaciente_N"   )]
      public short gxTpr_Recorridodetallepaciente_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Recorridodetallepaciente_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "IDCoach_N" )]
      [  XmlElement( ElementName = "IDCoach_N"   )]
      public short gxTpr_Idcoach_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Idcoach_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Idcoach_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Idcoach_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Idcoach_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Idcoach_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Idcoach_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoSenal_N" )]
      [  XmlElement( ElementName = "TipoSenal_N"   )]
      public short gxTpr_Tiposenal_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Tiposenal_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Tiposenal_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Tiposenal_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Tiposenal_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Tiposenal_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Tiposenal_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Longitud_N" )]
      [  XmlElement( ElementName = "Longitud_N"   )]
      public short gxTpr_Longitud_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Longitud_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Longitud_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Longitud_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Longitud_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Longitud_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Longitud_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Latitud_N" )]
      [  XmlElement( ElementName = "Latitud_N"   )]
      public short gxTpr_Latitud_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Latitud_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Latitud_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Latitud_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Latitud_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Latitud_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Latitud_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TipoObstaculo_N" )]
      [  XmlElement( ElementName = "TipoObstaculo_N"   )]
      public short gxTpr_Tipoobstaculo_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Tipoobstaculo_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Incidente_N" )]
      [  XmlElement( ElementName = "Incidente_N"   )]
      public short gxTpr_Incidente_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Incidente_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Incidente_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Incidente_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Incidente_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Incidente_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Incidente_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Emergencia_N" )]
      [  XmlElement( ElementName = "Emergencia_N"   )]
      public short gxTpr_Emergencia_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Emergencia_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Emergencia_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Emergencia_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Emergencia_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Emergencia_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Emergencia_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "IMEI_N" )]
      [  XmlElement( ElementName = "IMEI_N"   )]
      public short gxTpr_Imei_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Imei_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Imei_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Imei_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Imei_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Imei_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Imei_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "TimeStamp_N" )]
      [  XmlElement( ElementName = "TimeStamp_N"   )]
      public short gxTpr_Timestamp_N
      {
         get {
            return gxTv_SdtRecorrido_Detalle_Timestamp_N ;
         }

         set {
            gxTv_SdtRecorrido_Detalle_Timestamp_N = value;
            gxTv_SdtRecorrido_Detalle_Modified = 1;
            SetDirty("Timestamp_N");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_Timestamp_N_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle_Timestamp_N = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_Timestamp_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtRecorrido_Detalle_Recorridodetalleid = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Detalle_Idcoach = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Detalle_Longitud = "";
         gxTv_SdtRecorrido_Detalle_Latitud = "";
         gxTv_SdtRecorrido_Detalle_Imei = "";
         gxTv_SdtRecorrido_Detalle_Timestamp = (DateTime)(DateTime.MinValue);
         gxTv_SdtRecorrido_Detalle_Mode = "";
         gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Detalle_Idcoach_Z = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Detalle_Imei_Z = "";
         gxTv_SdtRecorrido_Detalle_Timestamp_Z = (DateTime)(DateTime.MinValue);
         datetime_STZ = (DateTime)(DateTime.MinValue);
         sDateCnv = "";
         sNumToPad = "";
         return  ;
      }

      private short gxTv_SdtRecorrido_Detalle_Tiposenal ;
      private short gxTv_SdtRecorrido_Detalle_Tipoobstaculo ;
      private short gxTv_SdtRecorrido_Detalle_Incidente ;
      private short gxTv_SdtRecorrido_Detalle_Emergencia ;
      private short gxTv_SdtRecorrido_Detalle_Modified ;
      private short gxTv_SdtRecorrido_Detalle_Initialized ;
      private short gxTv_SdtRecorrido_Detalle_Tiposenal_Z ;
      private short gxTv_SdtRecorrido_Detalle_Tipoobstaculo_Z ;
      private short gxTv_SdtRecorrido_Detalle_Incidente_Z ;
      private short gxTv_SdtRecorrido_Detalle_Emergencia_Z ;
      private short gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_N ;
      private short gxTv_SdtRecorrido_Detalle_Idcoach_N ;
      private short gxTv_SdtRecorrido_Detalle_Tiposenal_N ;
      private short gxTv_SdtRecorrido_Detalle_Longitud_N ;
      private short gxTv_SdtRecorrido_Detalle_Latitud_N ;
      private short gxTv_SdtRecorrido_Detalle_Tipoobstaculo_N ;
      private short gxTv_SdtRecorrido_Detalle_Incidente_N ;
      private short gxTv_SdtRecorrido_Detalle_Emergencia_N ;
      private short gxTv_SdtRecorrido_Detalle_Imei_N ;
      private short gxTv_SdtRecorrido_Detalle_Timestamp_N ;
      private String gxTv_SdtRecorrido_Detalle_Mode ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtRecorrido_Detalle_Timestamp ;
      private DateTime gxTv_SdtRecorrido_Detalle_Timestamp_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtRecorrido_Detalle_Longitud ;
      private String gxTv_SdtRecorrido_Detalle_Latitud ;
      private String gxTv_SdtRecorrido_Detalle_Imei ;
      private String gxTv_SdtRecorrido_Detalle_Imei_Z ;
      private Guid gxTv_SdtRecorrido_Detalle_Recorridodetalleid ;
      private Guid gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente ;
      private Guid gxTv_SdtRecorrido_Detalle_Idcoach ;
      private Guid gxTv_SdtRecorrido_Detalle_Recorridodetalleid_Z ;
      private Guid gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_Z ;
      private Guid gxTv_SdtRecorrido_Detalle_Idcoach_Z ;
   }

   [DataContract(Name = @"Recorrido.Detalle", Namespace = "PACYE2")]
   public class SdtRecorrido_Detalle_RESTInterface : GxGenericCollectionItem<SdtRecorrido_Detalle>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtRecorrido_Detalle_RESTInterface( ) : base()
      {
      }

      public SdtRecorrido_Detalle_RESTInterface( SdtRecorrido_Detalle psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "RecorridoDetalleID" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Recorridodetalleid
      {
         get {
            return sdt.gxTpr_Recorridodetalleid ;
         }

         set {
            sdt.gxTpr_Recorridodetalleid = (Guid)(value);
         }

      }

      [DataMember( Name = "RecorridoDetallePaciente" , Order = 1 )]
      [GxSeudo()]
      public Guid gxTpr_Recorridodetallepaciente
      {
         get {
            return sdt.gxTpr_Recorridodetallepaciente ;
         }

         set {
            sdt.gxTpr_Recorridodetallepaciente = (Guid)(value);
         }

      }

      [DataMember( Name = "IDCoach" , Order = 2 )]
      [GxSeudo()]
      public Guid gxTpr_Idcoach
      {
         get {
            return sdt.gxTpr_Idcoach ;
         }

         set {
            sdt.gxTpr_Idcoach = (Guid)(value);
         }

      }

      [DataMember( Name = "TipoSenal" , Order = 3 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Tiposenal
      {
         get {
            return sdt.gxTpr_Tiposenal ;
         }

         set {
            sdt.gxTpr_Tiposenal = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Longitud" , Order = 4 )]
      public String gxTpr_Longitud
      {
         get {
            return sdt.gxTpr_Longitud ;
         }

         set {
            sdt.gxTpr_Longitud = value;
         }

      }

      [DataMember( Name = "Latitud" , Order = 5 )]
      public String gxTpr_Latitud
      {
         get {
            return sdt.gxTpr_Latitud ;
         }

         set {
            sdt.gxTpr_Latitud = value;
         }

      }

      [DataMember( Name = "TipoObstaculo" , Order = 6 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Tipoobstaculo
      {
         get {
            return sdt.gxTpr_Tipoobstaculo ;
         }

         set {
            sdt.gxTpr_Tipoobstaculo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Incidente" , Order = 7 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Incidente
      {
         get {
            return sdt.gxTpr_Incidente ;
         }

         set {
            sdt.gxTpr_Incidente = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "Emergencia" , Order = 8 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Emergencia
      {
         get {
            return sdt.gxTpr_Emergencia ;
         }

         set {
            sdt.gxTpr_Emergencia = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "IMEI" , Order = 9 )]
      [GxSeudo()]
      public String gxTpr_Imei
      {
         get {
            return sdt.gxTpr_Imei ;
         }

         set {
            sdt.gxTpr_Imei = value;
         }

      }

      [DataMember( Name = "TimeStamp" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Timestamp
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Timestamp) ;
         }

         set {
            sdt.gxTpr_Timestamp = DateTimeUtil.CToT2( value);
         }

      }

      public SdtRecorrido_Detalle sdt
      {
         get {
            return (SdtRecorrido_Detalle)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtRecorrido_Detalle() ;
         }
      }

   }

}
