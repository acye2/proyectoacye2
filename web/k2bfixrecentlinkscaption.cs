/*
               File: K2BFixRecentLinksCaption
        Description: Fix caption to show in recent links
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:33.95
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bfixrecentlinkscaption : GXProcedure
   {
      public k2bfixrecentlinkscaption( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bfixrecentlinkscaption( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Caption ,
                           out String aP1_NewCaption )
      {
         this.AV8Caption = aP0_Caption;
         this.AV9NewCaption = "" ;
         initialize();
         executePrivate();
         aP1_NewCaption=this.AV9NewCaption;
      }

      public String executeUdp( String aP0_Caption )
      {
         this.AV8Caption = aP0_Caption;
         this.AV9NewCaption = "" ;
         initialize();
         executePrivate();
         aP1_NewCaption=this.AV9NewCaption;
         return AV9NewCaption ;
      }

      public void executeSubmit( String aP0_Caption ,
                                 out String aP1_NewCaption )
      {
         k2bfixrecentlinkscaption objk2bfixrecentlinkscaption;
         objk2bfixrecentlinkscaption = new k2bfixrecentlinkscaption();
         objk2bfixrecentlinkscaption.AV8Caption = aP0_Caption;
         objk2bfixrecentlinkscaption.AV9NewCaption = "" ;
         objk2bfixrecentlinkscaption.context.SetSubmitInitialConfig(context);
         objk2bfixrecentlinkscaption.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bfixrecentlinkscaption);
         aP1_NewCaption=this.AV9NewCaption;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bfixrecentlinkscaption)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         GXt_char1 = AV9NewCaption;
         new k2bremovestringsinrecentlinkscaption(context ).execute(  AV8Caption, out  GXt_char1) ;
         AV9NewCaption = GXt_char1;
         GXt_char1 = AV9NewCaption;
         new k2bfixrecentlinkscaptionlength(context ).execute(  AV9NewCaption, out  GXt_char1) ;
         AV9NewCaption = GXt_char1;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXt_char1 = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV8Caption ;
      private String AV9NewCaption ;
      private String GXt_char1 ;
      private String aP1_NewCaption ;
   }

}
