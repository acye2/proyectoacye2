/*
               File: K2BLinkTo
        Description: K2B Link To
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:34.68
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2blinkto : GXProcedure
   {
      public k2blinkto( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2blinkto( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( GxHttpRequest aP0_Request ,
                           out String aP1_URLString )
      {
         this.AV8Request = aP0_Request;
         this.AV9URLString = "" ;
         initialize();
         executePrivate();
         aP1_URLString=this.AV9URLString;
      }

      public String executeUdp( GxHttpRequest aP0_Request )
      {
         this.AV8Request = aP0_Request;
         this.AV9URLString = "" ;
         initialize();
         executePrivate();
         aP1_URLString=this.AV9URLString;
         return AV9URLString ;
      }

      public void executeSubmit( GxHttpRequest aP0_Request ,
                                 out String aP1_URLString )
      {
         k2blinkto objk2blinkto;
         objk2blinkto = new k2blinkto();
         objk2blinkto.AV8Request = aP0_Request;
         objk2blinkto.AV9URLString = "" ;
         objk2blinkto.context.SetSubmitInitialConfig(context);
         objk2blinkto.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2blinkto);
         aP1_URLString=this.AV9URLString;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2blinkto)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9URLString = AV8Request.ScriptName;
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8Request.QueryString)) )
         {
            AV9URLString = AV9URLString + "?" + AV8Request.QueryString;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV9URLString ;
      private String aP1_URLString ;
      private GxHttpRequest AV8Request ;
   }

}
