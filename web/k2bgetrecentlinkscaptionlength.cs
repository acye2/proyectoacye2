/*
               File: K2BGetRecentLinksCaptionLength
        Description: Return max caption length
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:34.32
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bgetrecentlinkscaptionlength : GXProcedure
   {
      public k2bgetrecentlinkscaptionlength( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bgetrecentlinkscaptionlength( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out int aP0_CaptionLength )
      {
         this.AV8CaptionLength = 0 ;
         initialize();
         executePrivate();
         aP0_CaptionLength=this.AV8CaptionLength;
      }

      public int executeUdp( )
      {
         this.AV8CaptionLength = 0 ;
         initialize();
         executePrivate();
         aP0_CaptionLength=this.AV8CaptionLength;
         return AV8CaptionLength ;
      }

      public void executeSubmit( out int aP0_CaptionLength )
      {
         k2bgetrecentlinkscaptionlength objk2bgetrecentlinkscaptionlength;
         objk2bgetrecentlinkscaptionlength = new k2bgetrecentlinkscaptionlength();
         objk2bgetrecentlinkscaptionlength.AV8CaptionLength = 0 ;
         objk2bgetrecentlinkscaptionlength.context.SetSubmitInitialConfig(context);
         objk2bgetrecentlinkscaptionlength.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bgetrecentlinkscaptionlength);
         aP0_CaptionLength=this.AV8CaptionLength;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bgetrecentlinkscaptionlength)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8CaptionLength = 20;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8CaptionLength ;
      private int aP0_CaptionLength ;
   }

}
