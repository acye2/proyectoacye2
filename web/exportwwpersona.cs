/*
               File: ExportWWPersona
        Description: Stub for ExportWWPersona
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 12:48:51.18
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class exportwwpersona : GXProcedure
   {
      public exportwwpersona( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public exportwwpersona( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_PersonaPNombre ,
                           DateTime aP1_PersonaFechaNacimiento_From ,
                           DateTime aP2_PersonaFechaNacimiento_To ,
                           short aP3_OrderedBy )
      {
         this.AV2PersonaPNombre = aP0_PersonaPNombre;
         this.AV3PersonaFechaNacimiento_From = aP1_PersonaFechaNacimiento_From;
         this.AV4PersonaFechaNacimiento_To = aP2_PersonaFechaNacimiento_To;
         this.AV5OrderedBy = aP3_OrderedBy;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_PersonaPNombre ,
                                 DateTime aP1_PersonaFechaNacimiento_From ,
                                 DateTime aP2_PersonaFechaNacimiento_To ,
                                 short aP3_OrderedBy )
      {
         exportwwpersona objexportwwpersona;
         objexportwwpersona = new exportwwpersona();
         objexportwwpersona.AV2PersonaPNombre = aP0_PersonaPNombre;
         objexportwwpersona.AV3PersonaFechaNacimiento_From = aP1_PersonaFechaNacimiento_From;
         objexportwwpersona.AV4PersonaFechaNacimiento_To = aP2_PersonaFechaNacimiento_To;
         objexportwwpersona.AV5OrderedBy = aP3_OrderedBy;
         objexportwwpersona.context.SetSubmitInitialConfig(context);
         objexportwwpersona.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objexportwwpersona);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((exportwwpersona)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2PersonaPNombre,(DateTime)AV3PersonaFechaNacimiento_From,(DateTime)AV4PersonaFechaNacimiento_To,(short)AV5OrderedBy} ;
         ClassLoader.Execute("aexportwwpersona","GeneXus.Programs","aexportwwpersona", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 4 ) )
         {
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV5OrderedBy ;
      private DateTime AV3PersonaFechaNacimiento_From ;
      private DateTime AV4PersonaFechaNacimiento_To ;
      private String AV2PersonaPNombre ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private Object[] args ;
   }

}
