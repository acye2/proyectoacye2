/*
               File: PromptLayoutModern
        Description: Prompt Layout Minimalistic
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:45.11
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class promptlayoutmodern : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public promptlayoutmodern( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public promptlayoutmodern( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "promptlayoutmodern_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA122( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START122( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, false);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171524513", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"align=\"right\""+" "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" data-gx-class=\"Form\" novalidate action=\""+formatLink("promptlayoutmodern.aspx") +"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "Form", true);
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE122( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT122( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("promptlayoutmodern.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "PromptLayoutModern" ;
      }

      public override String GetPgmdesc( )
      {
         return "Prompt Layout Minimalistic" ;
      }

      protected void WB120( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section_FixedDataTitle", "left", "top", "", "", "div");
            wb_table1_4_122( true) ;
         }
         else
         {
            wb_table1_4_122( false) ;
         }
         return  ;
      }

      protected void wb_table1_4_122e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMargincontainer_Internalname, 1, 0, "px", 0, "px", "Section_Basic_Margin_Left_Right_Bottom", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionbottomfixeddatacontainer_Internalname, 1, 0, "px", 0, "px", "Section_Basic_Marging_Top", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "0a4d316b-8139-4c49-8a86-ef436b82dce2", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage1_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            wb_table2_16_122( true) ;
         }
         else
         {
            wb_table2_16_122( false) ;
         }
         return  ;
      }

      protected void wb_table2_16_122e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START122( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Prompt Layout Minimalistic", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP120( ) ;
      }

      protected void WS122( )
      {
         START122( ) ;
         EVT122( ) ;
      }

      protected void EVT122( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Start */
                              E11122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Load */
                              E12122 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                 }
                                 dynload_actions( ) ;
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE122( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA122( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF122( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF122( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E12122 ();
            WB120( ) ;
         }
      }

      protected void send_integrity_lvl_hashes122( )
      {
      }

      protected void STRUP120( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11122 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E11122 ();
         if (returnInSub) return;
      }

      protected void E11122( )
      {
         /* Start Routine */
         Form.Jscriptsrc.Add("jquery.min.js") ;
         Form.Jscriptsrc.Add("K2BTools.js") ;
      }

      protected void nextLoad( )
      {
      }

      protected void E12122( )
      {
         /* Load Routine */
      }

      protected void wb_table2_16_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table_Basic_Widht100Percent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellCell_formsectionleft_Internalname+"\"  style=\""+CSSHelper.Prettify( "vertical-align:top;width:200px")+"\" class=''>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionformsectionleftcontainer_Internalname, 1, 0, "px", 0, "px", "Section_FormSectionLeftContainer", "left", "top", "", "", "div");
            context.WriteHtmlText( "Form Section Left ") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection_actionstop_Internalname, 1, 0, "px", 0, "px", "Section_ActionsTop", "left", "top", "", "", "div");
            wb_table3_22_122( true) ;
         }
         else
         {
            wb_table3_22_122( false) ;
         }
         return  ;
      }

      protected void wb_table3_22_122e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionformsectiontopcontainer_Internalname, 1, 0, "px", 0, "px", "Section_FormSectionTopContainer", "left", "top", "", "", "div");
            context.WriteHtmlText( "Form Section&nbsp;Top ") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionerrorviewercontainer_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "fefdff99-f319-4261-b9be-2cd062c58d76", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage13_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionfiltersheader_Internalname, 1, 0, "px", 0, "px", "Section_FiltersHeader", "left", "top", " "+"onclick=\"ChangeDivClassAndColapseDiv(event, 'Section_FiltersHeader', 'Section_FiltersHeaderClose', 'Section_Filters', 'Section_Basic_DisplayNone');\""+" "+"title=\"Haz clic para maximizar/minimizar\""+" ", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockfiltertitle_Internalname, "", "", "", lblTextblockfiltertitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock_FilterTitle", 0, "", 1, 1, 0, "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionfilterscontainer_Internalname, 1, 0, "px", 0, "px", "Section_Filters", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "66924303-c244-470c-a1ed-a434ff4aa5ac", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage7_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionruntimecolumnsmanagercontainer_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section_PagingGridOrderedby", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectiongridcontainer_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "3d30ce2e-216c-4178-bccf-54b83375eda2", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage8_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection_pagingandorderby_Internalname, 1, 0, "px", 0, "px", "Section_Basic_Padding_3px", "left", "top", "", "", "div");
            wb_table4_48_122( true) ;
         }
         else
         {
            wb_table4_48_122( false) ;
         }
         return  ;
      }

      protected void wb_table4_48_122e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionformsectionbottomcontainer_Internalname, 1, 0, "px", 0, "px", "Section_FormSectionBottomContainer", "left", "top", "", "", "div");
            context.WriteHtmlText( "Form Section Bottom ") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionactionsbottomcontainer_Internalname, 1, 0, "px", 0, "px", "Section_Basic_Marging_Top", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "b9eeb433-71fc-4ceb-80c3-2dbb129a3ef8", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage12_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionhiddenitemscontainer_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellCell_formsectionright_Internalname+"\"  style=\""+CSSHelper.Prettify( "vertical-align:top;width:200px")+"\" class=''>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionformsectionrightcontainer_Internalname, 1, 0, "px", 0, "px", "Section_FormSectionRightContainer", "left", "top", "", "", "div");
            context.WriteHtmlText( "Form Section Right ") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_16_122e( true) ;
         }
         else
         {
            wb_table2_16_122e( false) ;
         }
      }

      protected void wb_table4_48_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table_Basic_Widht100Percent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:20px")+"\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionorderedbycontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "c3af5837-cc46-4a35-8321-db248f6e45ac", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage14_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right;width:1316px")+"\">") ;
            wb_table5_54_122( true) ;
         }
         else
         {
            wb_table5_54_122( false) ;
         }
         return  ;
      }

      protected void wb_table5_54_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_48_122e( true) ;
         }
         else
         {
            wb_table4_48_122e( false) ;
         }
      }

      protected void wb_table5_54_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblK2btablewwpagingcontainer_Internalname, tblK2btablewwpagingcontainer_Internalname, "", "", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionpagingleftcontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "9371a372-2567-4377-8562-96ded7ad22ba", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage9_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionpagingpagecountcontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "d6199181-d1da-4cb6-8b1b-73391ad40192", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage10_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionpagingrightcontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "d96f11d6-34a4-4092-823a-9e676427c33e", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage11_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_54_122e( true) ;
         }
         else
         {
            wb_table5_54_122e( false) ;
         }
      }

      protected void wb_table3_22_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table_TopActions", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionactionsleftcontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "f4711365-5ef8-4f90-8131-72041dc9a899", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage4_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table6_28_122( true) ;
         }
         else
         {
            wb_table6_28_122( false) ;
         }
         return  ;
      }

      protected void wb_table6_28_122e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_22_122e( true) ;
         }
         else
         {
            wb_table3_22_122e( false) ;
         }
      }

      protected void wb_table6_28_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblK2btableactionsright_Internalname, tblK2btableactionsright_Internalname, "", "TableActionContainer", 0, "right", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionactionstopcontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "fdbb5408-ec94-4fc0-9a3d-6c258791e14a", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage5_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionactionsrightcontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "4644cfd5-2e62-4fc1-a8b5-079151463bcc", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage6_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_28_122e( true) ;
         }
         else
         {
            wb_table6_28_122e( false) ;
         }
      }

      protected void wb_table1_4_122( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblK2btablepgmdescriptioncontent_Internalname, tblK2btablepgmdescriptioncontent_Internalname, "", "Table_TitleDsc", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-left;text-align:-moz-left;text-align:-webkit-left")+"\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionpgmdescriptioncontainer_Internalname, 1, 0, "px", 0, "px", "Section_Basic_Margin_Right", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "4b2898a1-9f69-437b-ab22-c75f16f9fab8", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage2_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsectionfirstrowfixeddatacontainer_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "68eeb96e-6fd4-42ea-b34f-d33adbed6613", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage3_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_PromptLayoutModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_4_122e( true) ;
         }
         else
         {
            wb_table1_4_122e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA122( ) ;
         WS122( ) ;
         WE122( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), false);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171524565", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
            context.AddJavascriptSource("promptlayoutmodern.js", "?201811171524565", false);
         }
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgImage2_Internalname = "IMAGE2";
         divK2bsectionpgmdescriptioncontainer_Internalname = "K2BSECTIONPGMDESCRIPTIONCONTAINER";
         imgImage3_Internalname = "IMAGE3";
         divK2bsectionfirstrowfixeddatacontainer_Internalname = "K2BSECTIONFIRSTROWFIXEDDATACONTAINER";
         tblK2btablepgmdescriptioncontent_Internalname = "K2BTABLEPGMDESCRIPTIONCONTENT";
         imgImage1_Internalname = "IMAGE1";
         divK2bsectionbottomfixeddatacontainer_Internalname = "K2BSECTIONBOTTOMFIXEDDATACONTAINER";
         divK2bsectionformsectionleftcontainer_Internalname = "K2BSECTIONFORMSECTIONLEFTCONTAINER";
         cellCell_formsectionleft_Internalname = "CELL_FORMSECTIONLEFT";
         imgImage4_Internalname = "IMAGE4";
         divK2bsectionactionsleftcontainer_Internalname = "K2BSECTIONACTIONSLEFTCONTAINER";
         imgImage5_Internalname = "IMAGE5";
         divK2bsectionactionstopcontainer_Internalname = "K2BSECTIONACTIONSTOPCONTAINER";
         imgImage6_Internalname = "IMAGE6";
         divK2bsectionactionsrightcontainer_Internalname = "K2BSECTIONACTIONSRIGHTCONTAINER";
         tblK2btableactionsright_Internalname = "K2BTABLEACTIONSRIGHT";
         tblTable2_Internalname = "TABLE2";
         divSection_actionstop_Internalname = "SECTION_ACTIONSTOP";
         divK2bsectionformsectiontopcontainer_Internalname = "K2BSECTIONFORMSECTIONTOPCONTAINER";
         imgImage13_Internalname = "IMAGE13";
         divK2bsectionerrorviewercontainer_Internalname = "K2BSECTIONERRORVIEWERCONTAINER";
         lblTextblockfiltertitle_Internalname = "TEXTBLOCKFILTERTITLE";
         divK2bsectionfiltersheader_Internalname = "K2BSECTIONFILTERSHEADER";
         imgImage7_Internalname = "IMAGE7";
         divK2bsectionfilterscontainer_Internalname = "K2BSECTIONFILTERSCONTAINER";
         divK2bsectionruntimecolumnsmanagercontainer_Internalname = "K2BSECTIONRUNTIMECOLUMNSMANAGERCONTAINER";
         imgImage8_Internalname = "IMAGE8";
         divK2bsectiongridcontainer_Internalname = "K2BSECTIONGRIDCONTAINER";
         imgImage14_Internalname = "IMAGE14";
         divK2bsectionorderedbycontainer_Internalname = "K2BSECTIONORDEREDBYCONTAINER";
         imgImage9_Internalname = "IMAGE9";
         divK2bsectionpagingleftcontainer_Internalname = "K2BSECTIONPAGINGLEFTCONTAINER";
         imgImage10_Internalname = "IMAGE10";
         divK2bsectionpagingpagecountcontainer_Internalname = "K2BSECTIONPAGINGPAGECOUNTCONTAINER";
         imgImage11_Internalname = "IMAGE11";
         divK2bsectionpagingrightcontainer_Internalname = "K2BSECTIONPAGINGRIGHTCONTAINER";
         tblK2btablewwpagingcontainer_Internalname = "K2BTABLEWWPAGINGCONTAINER";
         tblTable3_Internalname = "TABLE3";
         divSection_pagingandorderby_Internalname = "SECTION_PAGINGANDORDERBY";
         divK2bsectionformsectionbottomcontainer_Internalname = "K2BSECTIONFORMSECTIONBOTTOMCONTAINER";
         imgImage12_Internalname = "IMAGE12";
         divK2bsectionactionsbottomcontainer_Internalname = "K2BSECTIONACTIONSBOTTOMCONTAINER";
         divK2bsectionhiddenitemscontainer_Internalname = "K2BSECTIONHIDDENITEMSCONTAINER";
         divK2bsectionformsectionrightcontainer_Internalname = "K2BSECTIONFORMSECTIONRIGHTCONTAINER";
         cellCell_formsectionright_Internalname = "CELL_FORMSECTIONRIGHT";
         tblTable1_Internalname = "TABLE1";
         divMargincontainer_Internalname = "MARGINCONTAINER";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Prompt Layout Minimalistic";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         sImgUrl = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         sStyleString = "";
         lblTextblockfiltertitle_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMargincontainer_Internalname ;
      private String divK2bsectionbottomfixeddatacontainer_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String sImgUrl ;
      private String imgImage1_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String cellCell_formsectionleft_Internalname ;
      private String divK2bsectionformsectionleftcontainer_Internalname ;
      private String divSection_actionstop_Internalname ;
      private String divK2bsectionformsectiontopcontainer_Internalname ;
      private String divK2bsectionerrorviewercontainer_Internalname ;
      private String imgImage13_Internalname ;
      private String divK2bsectionfiltersheader_Internalname ;
      private String lblTextblockfiltertitle_Internalname ;
      private String lblTextblockfiltertitle_Jsonclick ;
      private String divK2bsectionfilterscontainer_Internalname ;
      private String imgImage7_Internalname ;
      private String divK2bsectionruntimecolumnsmanagercontainer_Internalname ;
      private String divK2bsectiongridcontainer_Internalname ;
      private String imgImage8_Internalname ;
      private String divSection_pagingandorderby_Internalname ;
      private String divK2bsectionformsectionbottomcontainer_Internalname ;
      private String divK2bsectionactionsbottomcontainer_Internalname ;
      private String imgImage12_Internalname ;
      private String divK2bsectionhiddenitemscontainer_Internalname ;
      private String cellCell_formsectionright_Internalname ;
      private String divK2bsectionformsectionrightcontainer_Internalname ;
      private String tblTable3_Internalname ;
      private String divK2bsectionorderedbycontainer_Internalname ;
      private String imgImage14_Internalname ;
      private String tblK2btablewwpagingcontainer_Internalname ;
      private String divK2bsectionpagingleftcontainer_Internalname ;
      private String imgImage9_Internalname ;
      private String divK2bsectionpagingpagecountcontainer_Internalname ;
      private String imgImage10_Internalname ;
      private String divK2bsectionpagingrightcontainer_Internalname ;
      private String imgImage11_Internalname ;
      private String tblTable2_Internalname ;
      private String divK2bsectionactionsleftcontainer_Internalname ;
      private String imgImage4_Internalname ;
      private String tblK2btableactionsright_Internalname ;
      private String divK2bsectionactionstopcontainer_Internalname ;
      private String imgImage5_Internalname ;
      private String divK2bsectionactionsrightcontainer_Internalname ;
      private String imgImage6_Internalname ;
      private String tblK2btablepgmdescriptioncontent_Internalname ;
      private String divK2bsectionpgmdescriptioncontainer_Internalname ;
      private String imgImage2_Internalname ;
      private String divK2bsectionfirstrowfixeddatacontainer_Internalname ;
      private String imgImage3_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

}
