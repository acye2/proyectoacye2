/*
               File: PAgregarPaciente
        Description: PAgregar Paciente
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:36.92
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pagregarpaciente : GXProcedure
   {
      public pagregarpaciente( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public pagregarpaciente( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_PersonaPNombre ,
                           ref String aP1_PersonaPApellido ,
                           ref String aP2_PersonaEMail ,
                           ref String aP3_PersonaSexo ,
                           ref short aP4_PersonaPacienteOrigenSordera ,
                           ref String aP5_Coach )
      {
         this.AV8PersonaPNombre = aP0_PersonaPNombre;
         this.AV9PersonaPApellido = aP1_PersonaPApellido;
         this.AV10PersonaEMail = aP2_PersonaEMail;
         this.AV11PersonaSexo = aP3_PersonaSexo;
         this.AV12PersonaPacienteOrigenSordera = aP4_PersonaPacienteOrigenSordera;
         this.AV13Coach = aP5_Coach;
         initialize();
         executePrivate();
         aP0_PersonaPNombre=this.AV8PersonaPNombre;
         aP1_PersonaPApellido=this.AV9PersonaPApellido;
         aP2_PersonaEMail=this.AV10PersonaEMail;
         aP3_PersonaSexo=this.AV11PersonaSexo;
         aP4_PersonaPacienteOrigenSordera=this.AV12PersonaPacienteOrigenSordera;
         aP5_Coach=this.AV13Coach;
      }

      public String executeUdp( ref String aP0_PersonaPNombre ,
                                ref String aP1_PersonaPApellido ,
                                ref String aP2_PersonaEMail ,
                                ref String aP3_PersonaSexo ,
                                ref short aP4_PersonaPacienteOrigenSordera )
      {
         this.AV8PersonaPNombre = aP0_PersonaPNombre;
         this.AV9PersonaPApellido = aP1_PersonaPApellido;
         this.AV10PersonaEMail = aP2_PersonaEMail;
         this.AV11PersonaSexo = aP3_PersonaSexo;
         this.AV12PersonaPacienteOrigenSordera = aP4_PersonaPacienteOrigenSordera;
         this.AV13Coach = aP5_Coach;
         initialize();
         executePrivate();
         aP0_PersonaPNombre=this.AV8PersonaPNombre;
         aP1_PersonaPApellido=this.AV9PersonaPApellido;
         aP2_PersonaEMail=this.AV10PersonaEMail;
         aP3_PersonaSexo=this.AV11PersonaSexo;
         aP4_PersonaPacienteOrigenSordera=this.AV12PersonaPacienteOrigenSordera;
         aP5_Coach=this.AV13Coach;
         return AV13Coach ;
      }

      public void executeSubmit( ref String aP0_PersonaPNombre ,
                                 ref String aP1_PersonaPApellido ,
                                 ref String aP2_PersonaEMail ,
                                 ref String aP3_PersonaSexo ,
                                 ref short aP4_PersonaPacienteOrigenSordera ,
                                 ref String aP5_Coach )
      {
         pagregarpaciente objpagregarpaciente;
         objpagregarpaciente = new pagregarpaciente();
         objpagregarpaciente.AV8PersonaPNombre = aP0_PersonaPNombre;
         objpagregarpaciente.AV9PersonaPApellido = aP1_PersonaPApellido;
         objpagregarpaciente.AV10PersonaEMail = aP2_PersonaEMail;
         objpagregarpaciente.AV11PersonaSexo = aP3_PersonaSexo;
         objpagregarpaciente.AV12PersonaPacienteOrigenSordera = aP4_PersonaPacienteOrigenSordera;
         objpagregarpaciente.AV13Coach = aP5_Coach;
         objpagregarpaciente.context.SetSubmitInitialConfig(context);
         objpagregarpaciente.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objpagregarpaciente);
         aP0_PersonaPNombre=this.AV8PersonaPNombre;
         aP1_PersonaPApellido=this.AV9PersonaPApellido;
         aP2_PersonaEMail=this.AV10PersonaEMail;
         aP3_PersonaSexo=this.AV11PersonaSexo;
         aP4_PersonaPacienteOrigenSordera=this.AV12PersonaPacienteOrigenSordera;
         aP5_Coach=this.AV13Coach;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((pagregarpaciente)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV15Persona = new SdtPersona(context);
         AV15Persona.gxTpr_Personapnombre = AV8PersonaPNombre;
         AV15Persona.gxTpr_Personasnombre = "";
         AV15Persona.gxTpr_Personapapellido = AV9PersonaPApellido;
         AV15Persona.gxTpr_Personasapellido = "";
         AV15Persona.gxTpr_Personasexo = AV11PersonaSexo;
         AV15Persona.gxTpr_Personadpi = "";
         AV15Persona.gxTpr_Personatipo = 2;
         AV15Persona.gxTv_SdtPersona_Personafechanacimiento_SetNull();
         AV15Persona.gxTpr_Personapacienteorigensordera = AV12PersonaPacienteOrigenSordera;
         AV15Persona.gxTpr_Personaemail = AV10PersonaEMail;
         AV15Persona.Save();
         if ( AV15Persona.Success() )
         {
            pr_gam.commit( "PAgregarPaciente");
            pr_default.commit( "PAgregarPaciente");
         }
         else
         {
            pr_gam.rollback( "PAgregarPaciente");
            pr_default.rollback( "PAgregarPaciente");
         }
         /*
            INSERT RECORD ON TABLE CoachPaciente

         */
         A12CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( AV13Coach));
         A13CoachPaciente_Paciente = (Guid)(AV15Persona.gxTpr_Personaid);
         A18CoachPaciente_FechaAsignacion = Gx_date;
         n18CoachPaciente_FechaAsignacion = false;
         A19CoachPaciente_DispositivoID = "-";
         n19CoachPaciente_DispositivoID = false;
         n20CoachPaciente_Token = false;
         /* Using cursor P002M2 */
         pr_default.execute(0, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente, n18CoachPaciente_FechaAsignacion, A18CoachPaciente_FechaAsignacion, n19CoachPaciente_DispositivoID, A19CoachPaciente_DispositivoID, n20CoachPaciente_Token, A20CoachPaciente_Token});
         pr_default.close(0);
         dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
         if ( (pr_default.getStatus(0) == 1) )
         {
            context.Gx_err = 1;
            Gx_emsg = (String)(context.GetMessage( "GXM_noupdate", ""));
         }
         else
         {
            context.Gx_err = 0;
            Gx_emsg = "";
         }
         /* End Insert */
         pr_gam.commit( "PAgregarPaciente");
         pr_default.commit( "PAgregarPaciente");
         this.cleanup();
         if (true) return;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV15Persona = new SdtPersona(context);
         A12CoachPaciente_Coach = (Guid)(Guid.Empty);
         A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         Gx_date = DateTime.MinValue;
         A19CoachPaciente_DispositivoID = "";
         A20CoachPaciente_Token = "";
         Gx_emsg = "";
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.pagregarpaciente__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pagregarpaciente__default(),
            new Object[][] {
                new Object[] {
               }
            }
         );
         Gx_date = DateTimeUtil.Today( context);
         /* GeneXus formulas. */
         Gx_date = DateTimeUtil.Today( context);
         context.Gx_err = 0;
      }

      private short AV12PersonaPacienteOrigenSordera ;
      private int GX_INS2 ;
      private String AV11PersonaSexo ;
      private String Gx_emsg ;
      private DateTime A18CoachPaciente_FechaAsignacion ;
      private DateTime Gx_date ;
      private bool n18CoachPaciente_FechaAsignacion ;
      private bool n19CoachPaciente_DispositivoID ;
      private bool n20CoachPaciente_Token ;
      private String AV8PersonaPNombre ;
      private String AV9PersonaPApellido ;
      private String AV10PersonaEMail ;
      private String AV13Coach ;
      private String A19CoachPaciente_DispositivoID ;
      private String A20CoachPaciente_Token ;
      private Guid A12CoachPaciente_Coach ;
      private Guid A13CoachPaciente_Paciente ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_PersonaPNombre ;
      private String aP1_PersonaPApellido ;
      private String aP2_PersonaEMail ;
      private String aP3_PersonaSexo ;
      private short aP4_PersonaPacienteOrigenSordera ;
      private String aP5_Coach ;
      private IDataStoreProvider pr_gam ;
      private IDataStoreProvider pr_default ;
      private SdtPersona AV15Persona ;
   }

   public class pagregarpaciente__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class pagregarpaciente__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new UpdateCursor(def[0])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmP002M2 ;
        prmP002M2 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_FechaAsignacion",SqlDbType.DateTime,8,0} ,
        new Object[] {"@CoachPaciente_DispositivoID",SqlDbType.VarChar,75,0} ,
        new Object[] {"@CoachPaciente_Token",SqlDbType.VarChar,40,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("P002M2", "INSERT INTO [CoachPaciente]([CoachPaciente_Coach], [CoachPaciente_Paciente], [CoachPaciente_FechaAsignacion], [CoachPaciente_DispositivoID], [CoachPaciente_Token], [CoachPaciente_CoachPNombre], [CoachPaciente_CoachPApellido]) VALUES(@CoachPaciente_Coach, @CoachPaciente_Paciente, @CoachPaciente_FechaAsignacion, @CoachPaciente_DispositivoID, @CoachPaciente_Token, '', '')", GxErrorMask.GX_NOMASK,prmP002M2)
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(3, (DateTime)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              return;
     }
  }

}

}
