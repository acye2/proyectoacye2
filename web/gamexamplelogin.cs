/*
               File: GAMExampleLogin
        Description: Login
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 13:45:37.44
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexamplelogin : GXHttpHandler, System.Web.SessionState.IRequiresSessionState
   {
      public gamexamplelogin( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gamexamplelogin( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavLogonto = new GXCombobox();
         chkavKeepmeloggedin = new GXCheckbox();
         chkavRememberme = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            ValidateSpaRequest();
            PA1M2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS1M2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE1M2( ) ;
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( "Login") ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111813453763", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gamexamplelogin.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "vAPPLICATIONCLIENTID", StringUtil.RTrim( AV6ApplicationClientId));
         GxWebStd.gx_hidden_field( context, "vLANGUAGE", StringUtil.RTrim( AV18Language));
         GxWebStd.gx_hidden_field( context, "vUSERREMEMBERME", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28UserRememberMe), 2, 0, ",", "")));
      }

      protected void RenderHtmlCloseForm1M2( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendSecurityToken((String)(sPrefix));
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "GAMExampleLogin" ;
      }

      public override String GetPgmdesc( )
      {
         return "Login" ;
      }

      protected void WB1M0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "BodyContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable1_Internalname, 1, 0, "px", 0, "px", "LoginContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablelogin_Internalname, 1, 0, "px", 0, "px", "TableLogin", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "INGRESAR", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "BigTitle", 0, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCurrentrepository_Internalname, lblCurrentrepository_Caption, "", "", lblCurrentrepository_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "VersionText", 0, "", lblCurrentrepository_Visible, 1, 0, "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-8 col-xs-offset-2", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", cmbavLogonto.Visible, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavLogonto_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavLogonto_Internalname, "Log On To", "col-sm-3 LoginComboAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavLogonto, cmbavLogonto_Internalname, StringUtil.RTrim( AV20LogOnTo), 1, cmbavLogonto_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", cmbavLogonto.Visible, cmbavLogonto.Enabled, 0, 0, 0, "em", 0, "", "", "LoginComboAttribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_GAMExampleLogin.htm");
            cmbavLogonto.CurrentValue = StringUtil.RTrim( AV20LogOnTo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Values", (String)(cmbavLogonto.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-8 col-xs-offset-2", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavUsername_Internalname, "User Name", "col-sm-3 LoginAttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsername_Internalname, AV26UserName, StringUtil.RTrim( context.localUtil.Format( AV26UserName, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "Usuario", edtavUsername_Jsonclick, 0, "LoginAttribute", "", "", "", "", 1, edtavUsername_Enabled, 0, "text", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, 0, 0, true, "GAMUserIdentification", "left", true, "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-8 col-xs-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavUserpassword_Internalname, "User Password", "col-sm-3 LoginAttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 31,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserpassword_Internalname, StringUtil.RTrim( AV27UserPassword), StringUtil.RTrim( context.localUtil.Format( AV27UserPassword, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,31);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "Contrase�a", edtavUserpassword_Jsonclick, 0, "LoginAttribute", "", "", "", "", 1, edtavUserpassword_Enabled, 0, "text", "", 50, "chr", 1, "row", 50, -1, 0, 0, 1, 0, 0, true, "GAMPassword", "left", true, "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-8 col-xs-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", chkavKeepmeloggedin.Visible, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavKeepmeloggedin_Internalname+"\"", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'',0)\"";
            ClassString = "CheckBox Label";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavKeepmeloggedin_Internalname, StringUtil.BoolToStr( AV17KeepMeLoggedIn), "", "", chkavKeepmeloggedin.Visible, chkavKeepmeloggedin.Enabled, "true", "Mantener sesi�n activa", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(36, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-8 col-xs-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", chkavRememberme.Visible, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavRememberme_Internalname+"\"", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'',false,'',0)\"";
            ClassString = "CheckBox Label";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRememberme_Internalname, StringUtil.BoolToStr( AV21RememberMe), "", "", chkavRememberme.Visible, chkavRememberme.Enabled, "true", "Recordarme", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(41, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-7 col-xs-offset-3 col-sm-8 col-sm-offset-2", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            ClassString = "BtnLogin";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttLogin_Internalname, "", "ENTRAR", bttLogin_Jsonclick, 5, "ENTRAR", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbrememberme2_Internalname, "�Olvid� su contrase�a?", "", "", lblTbrememberme2_Jsonclick, "'"+""+"'"+",false,"+"'"+"e111m1_client"+"'", "", "PagingText TextLikeLink", 7, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            wb_table1_50_1M2( true) ;
         }
         else
         {
            wb_table1_50_1M2( false) ;
         }
         return  ;
      }

      protected void wb_table1_50_1M2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Right", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divButtons_Internalname, divButtons_Visible, 0, "px", 0, "px", "TableButtons", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-xs col-sm-5 col-sm-offset-1", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "OR LOGIN WITH", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "SpecialText", 0, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2 col-xs-offset-2 col-sm-1 col-sm-offset-0", "left", "top", "", "", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "1965aed6-daad-4687-8b71-4797860e630c", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgButtonfacebook_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgButtonfacebook_Visible, 1, "Sign in with Facebook", "Sign in with Facebook", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgButtonfacebook_Jsonclick, "'"+""+"'"+",false,"+"'"+"EBUTTONFACEBOOK.CLICK."+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2 col-sm-1", "left", "top", "", "", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "5b513ef3-c434-4c8c-aacb-feefd834b2ee", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgButtontwitter_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgButtontwitter_Visible, 1, "Sign in with Twitter", "Sign in with Twitter", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgButtontwitter_Jsonclick, "'"+""+"'"+",false,"+"'"+"EBUTTONTWITTER.CLICK."+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2 col-sm-1", "left", "top", "", "", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "57720a5a-eb93-4911-895a-35207ed2dd03", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgButtongoogle_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgButtongoogle_Visible, 1, "Sign in with Google", "Sign in with Google", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgButtongoogle_Jsonclick, "'"+""+"'"+",false,"+"'"+"EBUTTONGOOGLE.CLICK."+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2 col-sm-1", "left", "top", "", "", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "6cdd3e18-cc5b-44e0-bd22-3efaf48a6c40", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgButtongamremote_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgButtongamremote_Visible, 1, "Sign in with remote GAM ", "Sign in with remote GAM ", 0, 0, 0, "px", 0, "px", 0, 0, 5, imgButtongamremote_Jsonclick, "'"+""+"'"+",false,"+"'"+"EBUTTONGAMREMOTE.CLICK."+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleLogin.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Right", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START1M2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Login", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1M0( ) ;
      }

      protected void WS1M2( )
      {
         START1M2( ) ;
         EVT1M2( ) ;
      }

      protected void EVT1M2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E121M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Refresh */
                           E131M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                                 /* Execute user event: Enter */
                                 E141M2 ();
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "BUTTONFACEBOOK.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           E151M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "BUTTONGOOGLE.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           E161M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "BUTTONTWITTER.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           E171M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "BUTTONGAMREMOTE.CLICK") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           E181M2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Load */
                           E191M2 ();
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE1M2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm1M2( ) ;
            }
         }
      }

      protected void PA1M2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavLogonto.Name = "vLOGONTO";
            cmbavLogonto.WebTags = "";
            if ( cmbavLogonto.ItemCount > 0 )
            {
               AV20LogOnTo = cmbavLogonto.getValidValue(AV20LogOnTo);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20LogOnTo", AV20LogOnTo);
            }
            chkavKeepmeloggedin.Name = "vKEEPMELOGGEDIN";
            chkavKeepmeloggedin.WebTags = "";
            chkavKeepmeloggedin.Caption = "Mantener sesi�n activa";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "TitleCaption", chkavKeepmeloggedin.Caption, true);
            chkavKeepmeloggedin.CheckedValue = "false";
            chkavRememberme.Name = "vREMEMBERME";
            chkavRememberme.WebTags = "";
            chkavRememberme.Caption = "Recordarme";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "TitleCaption", chkavRememberme.Caption, true);
            chkavRememberme.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavLogonto_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavLogonto.ItemCount > 0 )
         {
            AV20LogOnTo = cmbavLogonto.getValidValue(AV20LogOnTo);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20LogOnTo", AV20LogOnTo);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavLogonto.CurrentValue = StringUtil.RTrim( AV20LogOnTo);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Values", cmbavLogonto.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1M2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF1M2( )
      {
         initialize_formulas( ) ;
         /* Execute user event: Refresh */
         E131M2 ();
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E191M2 ();
            WB1M0( ) ;
         }
      }

      protected void send_integrity_lvl_hashes1M2( )
      {
      }

      protected void STRUP1M0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E121M2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavLogonto.CurrentValue = cgiGet( cmbavLogonto_Internalname);
            AV20LogOnTo = cgiGet( cmbavLogonto_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20LogOnTo", AV20LogOnTo);
            AV26UserName = cgiGet( edtavUsername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26UserName", AV26UserName);
            AV27UserPassword = cgiGet( edtavUserpassword_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27UserPassword", AV27UserPassword);
            AV17KeepMeLoggedIn = StringUtil.StrToBool( cgiGet( chkavKeepmeloggedin_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17KeepMeLoggedIn", AV17KeepMeLoggedIn);
            AV21RememberMe = StringUtil.StrToBool( cgiGet( chkavRememberme_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21RememberMe", AV21RememberMe);
            /* Read saved values. */
            AV6ApplicationClientId = cgiGet( "vAPPLICATIONCLIENTID");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E121M2 ();
         if (returnInSub) return;
      }

      protected void E121M2( )
      {
         /* Start Routine */
         lblCurrentrepository_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblCurrentrepository_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblCurrentrepository_Visible), 5, 0)), true);
         AV15isOK = new SdtGAM(context).checkconnection();
         AV10ConnectionInfoCollection = new SdtGAM(context).getconnections();
         if ( ! AV15isOK )
         {
            if ( AV10ConnectionInfoCollection.Count > 0 )
            {
               AV15isOK = new SdtGAM(context).setconnection(((SdtGAMConnectionInfo)AV10ConnectionInfoCollection.Item(1)).gxTpr_Name, out  AV12Errors);
            }
         }
         if ( AV10ConnectionInfoCollection.Count > 1 )
         {
            AV29GAMRepository = new SdtGAMRepository(context).get();
            lblCurrentrepository_Caption = "Repository: "+AV29GAMRepository.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblCurrentrepository_Internalname, "Caption", lblCurrentrepository_Caption, true);
            lblCurrentrepository_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblCurrentrepository_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblCurrentrepository_Visible), 5, 0)), true);
         }
         /* Execute user subroutine: 'DISPLAYEXTERNALAUTHENTICATIONS' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E131M2( )
      {
         /* Refresh Routine */
         AV16isRedirect = false;
         AV13ErrorsLogin = new SdtGAMRepository(context).getlasterrors();
         if ( AV13ErrorsLogin.Count > 0 )
         {
            if ( ((SdtGAMError)AV13ErrorsLogin.Item(1)).gxTpr_Code == 1 )
            {
            }
            else if ( ( ((SdtGAMError)AV13ErrorsLogin.Item(1)).gxTpr_Code == 24 ) || ( ((SdtGAMError)AV13ErrorsLogin.Item(1)).gxTpr_Code == 23 ) )
            {
               CallWebObject(formatLink("gamexamplechangepassword.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId)));
               context.wjLocDisableFrm = 1;
               AV16isRedirect = true;
            }
            else if ( ((SdtGAMError)AV13ErrorsLogin.Item(1)).gxTpr_Code == 161 )
            {
               CallWebObject(formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId)));
               context.wjLocDisableFrm = 1;
               AV16isRedirect = true;
            }
            else
            {
               AV27UserPassword = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27UserPassword", AV27UserPassword);
               AV12Errors = AV13ErrorsLogin;
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S122 ();
               if (returnInSub) return;
            }
         }
         if ( ! AV16isRedirect )
         {
            AV24SessionValid = new SdtGAMSession(context).isvalid(out  AV23Session, out  AV12Errors);
            if ( AV24SessionValid && ! AV23Session.gxTpr_Isanonymous )
            {
               AV25URL = new SdtGAMRepository(context).getlasterrorsurl();
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25URL)) )
               {
                  new SdtGAMRepository(context).applicationgohome("8d9934db-05db-4d64-adba-5e0466c3appU") ;
                  AV12Errors = new SdtGAMRepository(context).getlasterrors();
                  /* Execute user subroutine: 'DISPLAYMESSAGES' */
                  S122 ();
                  if (returnInSub) return;
               }
               else
               {
                  CallWebObject(formatLink(AV25URL) );
                  context.wjLocDisableFrm = 0;
               }
            }
            else
            {
               cmbavLogonto.removeAllItems();
               AV9AuthenticationTypes = new SdtGAMRepository(context).getenabledauthenticationtypes(AV18Language, out  AV12Errors);
               AV34GXV1 = 1;
               while ( AV34GXV1 <= AV9AuthenticationTypes.Count )
               {
                  AV8AuthenticationType = ((SdtGAMAuthenticationTypeSimple)AV9AuthenticationTypes.Item(AV34GXV1));
                  if ( AV8AuthenticationType.gxTpr_Needusername )
                  {
                     cmbavLogonto.addItem(AV8AuthenticationType.gxTpr_Name, AV8AuthenticationType.gxTpr_Description, 0);
                  }
                  AV34GXV1 = (int)(AV34GXV1+1);
               }
               if ( cmbavLogonto.ItemCount <= 1 )
               {
                  cmbavLogonto.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavLogonto.Visible), 5, 0)), true);
               }
               else
               {
                  AV20LogOnTo = ((SdtGAMAuthenticationTypeSimple)AV9AuthenticationTypes.Item(1)).gxTpr_Name;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20LogOnTo", AV20LogOnTo);
               }
               AV15isOK = new SdtGAMRepository(context).getrememberlogin(out  AV20LogOnTo, out  AV26UserName, out  AV28UserRememberMe, out  AV12Errors);
               if ( AV28UserRememberMe == 2 )
               {
                  AV21RememberMe = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21RememberMe", AV21RememberMe);
               }
               AV22Repository = new SdtGAMRepository(context).get();
               if ( cmbavLogonto.ItemCount > 1 )
               {
                  AV20LogOnTo = AV22Repository.gxTpr_Defaultauthenticationtypename;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20LogOnTo", AV20LogOnTo);
               }
               if ( StringUtil.StrCmp(AV22Repository.gxTpr_Userremembermetype, "Login") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)), true);
               }
               else if ( StringUtil.StrCmp(AV22Repository.gxTpr_Userremembermetype, "Auth") == 0 )
               {
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)), true);
               }
               else if ( StringUtil.StrCmp(AV22Repository.gxTpr_Userremembermetype, "Both") == 0 )
               {
                  chkavRememberme.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)), true);
                  chkavKeepmeloggedin.Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)), true);
               }
               else
               {
                  chkavRememberme.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRememberme_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavRememberme.Visible), 5, 0)), true);
                  chkavKeepmeloggedin.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavKeepmeloggedin_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavKeepmeloggedin.Visible), 5, 0)), true);
               }
            }
         }
         /*  Sending Event outputs  */
         cmbavLogonto.CurrentValue = StringUtil.RTrim( AV20LogOnTo);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogonto_Internalname, "Values", cmbavLogonto.ToJavascriptSource(), true);
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E141M2 ();
         if (returnInSub) return;
      }

      protected void E141M2( )
      {
         /* Enter Routine */
         if ( AV17KeepMeLoggedIn )
         {
            AV5AdditionalParameter.gxTpr_Rememberusertype = 3;
         }
         else if ( AV21RememberMe )
         {
            AV5AdditionalParameter.gxTpr_Rememberusertype = 2;
         }
         else
         {
            AV5AdditionalParameter.gxTpr_Rememberusertype = 1;
         }
         AV5AdditionalParameter.gxTpr_Authenticationtypename = AV20LogOnTo;
         AV19LoginOK = new SdtGAMRepository(context).login(AV26UserName, AV27UserPassword, AV5AdditionalParameter, out  AV12Errors);
         if ( AV19LoginOK )
         {
            AV7ApplicationData = new SdtGAMSession(context).getapplicationdata();
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV7ApplicationData)) )
            {
               AV14GAMExampleSDTApplicationData.FromJSonString(AV7ApplicationData, null);
            }
            AV31GAMUser = new SdtGAMUser(context).getbylogin(AV20LogOnTo, AV26UserName, out  AV12Errors);
            AV30WebSession.Set("COACH", StringUtil.Trim( AV31GAMUser.gxTpr_Externalid));
            AV25URL = new SdtGAMRepository(context).getlasterrorsurl();
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV25URL)) )
            {
               CallWebObject(formatLink("principal.aspx") );
               context.wjLocDisableFrm = 1;
               AV12Errors = new SdtGAMRepository(context).getlasterrors();
               /* Execute user subroutine: 'DISPLAYMESSAGES' */
               S122 ();
               if (returnInSub) return;
            }
            else
            {
               CallWebObject(formatLink("principal.aspx") );
               context.wjLocDisableFrm = 1;
            }
         }
         else
         {
            if ( AV12Errors.Count > 0 )
            {
               if ( ( ((SdtGAMError)AV12Errors.Item(1)).gxTpr_Code == 24 ) || ( ((SdtGAMError)AV12Errors.Item(1)).gxTpr_Code == 23 ) )
               {
                  CallWebObject(formatLink("gamexamplechangepassword.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId)));
                  context.wjLocDisableFrm = 1;
               }
               else if ( ((SdtGAMError)AV12Errors.Item(1)).gxTpr_Code == 161 )
               {
                  CallWebObject(formatLink("gamexampleupdateregisteruser.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV6ApplicationClientId)));
                  context.wjLocDisableFrm = 1;
               }
               else
               {
                  AV27UserPassword = "";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27UserPassword", AV27UserPassword);
                  /* Execute user subroutine: 'DISPLAYMESSAGES' */
                  S122 ();
                  if (returnInSub) return;
               }
            }
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5AdditionalParameter", AV5AdditionalParameter);
      }

      protected void S122( )
      {
         /* 'DISPLAYMESSAGES' Routine */
         AV35GXV2 = 1;
         while ( AV35GXV2 <= AV12Errors.Count )
         {
            AV11Error = ((SdtGAMError)AV12Errors.Item(AV35GXV2));
            if ( AV11Error.gxTpr_Code != 13 )
            {
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV11Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV11Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            }
            AV35GXV2 = (int)(AV35GXV2+1);
         }
      }

      protected void E151M2( )
      {
         /* Buttonfacebook_Click Routine */
         new SdtGAMRepository(context).loginfacebook() ;
      }

      protected void E161M2( )
      {
         /* Buttongoogle_Click Routine */
         new SdtGAMRepository(context).logingoogle() ;
      }

      protected void E171M2( )
      {
         /* Buttontwitter_Click Routine */
         new SdtGAMRepository(context).logintwitter() ;
      }

      protected void E181M2( )
      {
         /* Buttongamremote_Click Routine */
         new SdtGAMRepository(context).logingamremote() ;
      }

      protected void S112( )
      {
         /* 'DISPLAYEXTERNALAUTHENTICATIONS' Routine */
         divButtons_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divButtons_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divButtons_Visible), 5, 0)), true);
         if ( new SdtGAMRepository(context).canauthenticatewith("Facebook") )
         {
            imgButtonfacebook_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtonfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtonfacebook_Visible), 5, 0)), true);
            divButtons_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divButtons_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divButtons_Visible), 5, 0)), true);
         }
         else
         {
            imgButtonfacebook_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtonfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtonfacebook_Visible), 5, 0)), true);
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("Google") )
         {
            imgButtongoogle_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtongoogle_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtongoogle_Visible), 5, 0)), true);
            divButtons_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divButtons_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divButtons_Visible), 5, 0)), true);
         }
         else
         {
            imgButtongoogle_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtongoogle_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtongoogle_Visible), 5, 0)), true);
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("Twitter") )
         {
            imgButtontwitter_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtontwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtontwitter_Visible), 5, 0)), true);
            divButtons_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divButtons_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divButtons_Visible), 5, 0)), true);
         }
         else
         {
            imgButtontwitter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtontwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtontwitter_Visible), 5, 0)), true);
         }
         if ( new SdtGAMRepository(context).canauthenticatewith("GAMRemote") )
         {
            imgButtongamremote_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtongamremote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtongamremote_Visible), 5, 0)), true);
            divButtons_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divButtons_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divButtons_Visible), 5, 0)), true);
         }
         else
         {
            imgButtongamremote_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgButtongamremote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgButtongamremote_Visible), 5, 0)), true);
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E191M2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_50_1M2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"Right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-Right;text-align:-moz-Right;text-align:-webkit-Right")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock3_Internalname, "o", "", "", lblTextblock3_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "SpecialText", 0, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"Left\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-Left;text-align:-moz-Left;text-align:-webkit-Left")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTbregister_Internalname, "crear una cuenta", "", "", lblTbregister_Jsonclick, "'"+""+"'"+",false,"+"'"+"e201m1_client"+"'", "", "ActionText TextLikeLink", 7, "", 1, 1, 0, "HLP_GAMExampleLogin.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_50_1M2e( true) ;
         }
         else
         {
            wb_table1_50_1M2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1M2( ) ;
         WS1M2( ) ;
         WE1M2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2018111813454772", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gamexamplelogin.js", "?2018111813454774", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         lblCurrentrepository_Internalname = "CURRENTREPOSITORY";
         cmbavLogonto_Internalname = "vLOGONTO";
         edtavUsername_Internalname = "vUSERNAME";
         edtavUserpassword_Internalname = "vUSERPASSWORD";
         chkavKeepmeloggedin_Internalname = "vKEEPMELOGGEDIN";
         chkavRememberme_Internalname = "vREMEMBERME";
         bttLogin_Internalname = "LOGIN";
         lblTbrememberme2_Internalname = "TBREMEMBERME2";
         lblTextblock3_Internalname = "TEXTBLOCK3";
         lblTbregister_Internalname = "TBREGISTER";
         tblTable2_Internalname = "TABLE2";
         lblTextblock2_Internalname = "TEXTBLOCK2";
         imgButtonfacebook_Internalname = "BUTTONFACEBOOK";
         imgButtontwitter_Internalname = "BUTTONTWITTER";
         imgButtongoogle_Internalname = "BUTTONGOOGLE";
         imgButtongamremote_Internalname = "BUTTONGAMREMOTE";
         divButtons_Internalname = "BUTTONS";
         divTablelogin_Internalname = "TABLELOGIN";
         divTable1_Internalname = "TABLE1";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         chkavRememberme.Caption = "";
         chkavKeepmeloggedin.Caption = "";
         imgButtongamremote_Visible = 1;
         imgButtongoogle_Visible = 1;
         imgButtontwitter_Visible = 1;
         imgButtonfacebook_Visible = 1;
         divButtons_Visible = 1;
         chkavRememberme.Enabled = 1;
         chkavRememberme.Visible = 1;
         chkavKeepmeloggedin.Enabled = 1;
         chkavKeepmeloggedin.Visible = 1;
         edtavUserpassword_Jsonclick = "";
         edtavUserpassword_Enabled = 1;
         edtavUsername_Jsonclick = "";
         edtavUsername_Enabled = 1;
         cmbavLogonto_Jsonclick = "";
         cmbavLogonto.Enabled = 1;
         cmbavLogonto.Visible = 1;
         lblCurrentrepository_Caption = "Text Block";
         lblCurrentrepository_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV6ApplicationClientId',fld:'vAPPLICATIONCLIENTID',pic:'',nv:''},{av:'AV18Language',fld:'vLANGUAGE',pic:'',nv:''},{av:'AV26UserName',fld:'vUSERNAME',pic:'',nv:''},{av:'AV28UserRememberMe',fld:'vUSERREMEMBERME',pic:'Z9',nv:0}],oparms:[{av:'AV6ApplicationClientId',fld:'vAPPLICATIONCLIENTID',pic:'',nv:''},{av:'AV27UserPassword',fld:'vUSERPASSWORD',pic:'',nv:''},{av:'cmbavLogonto'},{av:'AV20LogOnTo',fld:'vLOGONTO',pic:'',nv:''},{av:'AV21RememberMe',fld:'vREMEMBERME',pic:'',nv:false},{av:'chkavRememberme.Visible',ctrl:'vREMEMBERME',prop:'Visible'},{av:'chkavKeepmeloggedin.Visible',ctrl:'vKEEPMELOGGEDIN',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E141M2',iparms:[{av:'AV17KeepMeLoggedIn',fld:'vKEEPMELOGGEDIN',pic:'',nv:false},{av:'AV21RememberMe',fld:'vREMEMBERME',pic:'',nv:false},{av:'cmbavLogonto'},{av:'AV20LogOnTo',fld:'vLOGONTO',pic:'',nv:''},{av:'AV26UserName',fld:'vUSERNAME',pic:'',nv:''},{av:'AV27UserPassword',fld:'vUSERPASSWORD',pic:'',nv:''},{av:'AV6ApplicationClientId',fld:'vAPPLICATIONCLIENTID',pic:'',nv:''}],oparms:[{av:'AV6ApplicationClientId',fld:'vAPPLICATIONCLIENTID',pic:'',nv:''},{av:'AV27UserPassword',fld:'vUSERPASSWORD',pic:'',nv:''}]}");
         setEventMetadata("'FORGOTPASSWORD'","{handler:'E111M1',iparms:[{av:'AV6ApplicationClientId',fld:'vAPPLICATIONCLIENTID',pic:'',nv:''}],oparms:[{av:'AV6ApplicationClientId',fld:'vAPPLICATIONCLIENTID',pic:'',nv:''}]}");
         setEventMetadata("'REGISTER'","{handler:'E201M1',iparms:[],oparms:[]}");
         setEventMetadata("BUTTONFACEBOOK.CLICK","{handler:'E151M2',iparms:[],oparms:[]}");
         setEventMetadata("BUTTONGOOGLE.CLICK","{handler:'E161M2',iparms:[],oparms:[]}");
         setEventMetadata("BUTTONTWITTER.CLICK","{handler:'E171M2',iparms:[],oparms:[]}");
         setEventMetadata("BUTTONGAMREMOTE.CLICK","{handler:'E181M2',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         AV6ApplicationClientId = "";
         AV18Language = "";
         GX_FocusControl = "";
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         lblTextblock1_Jsonclick = "";
         lblCurrentrepository_Jsonclick = "";
         TempTags = "";
         AV20LogOnTo = "";
         AV26UserName = "";
         AV27UserPassword = "";
         bttLogin_Jsonclick = "";
         lblTbrememberme2_Jsonclick = "";
         lblTextblock2_Jsonclick = "";
         sImgUrl = "";
         imgButtonfacebook_Jsonclick = "";
         imgButtontwitter_Jsonclick = "";
         imgButtongoogle_Jsonclick = "";
         imgButtongamremote_Jsonclick = "";
         Form = new GXWebForm();
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10ConnectionInfoCollection = new GXExternalCollection<SdtGAMConnectionInfo>( context, "SdtGAMConnectionInfo", "GeneXus.Programs");
         AV12Errors = new GXExternalCollection<SdtGAMError>( context, "SdtGAMError", "GeneXus.Programs");
         AV29GAMRepository = new SdtGAMRepository(context);
         AV13ErrorsLogin = new GXExternalCollection<SdtGAMError>( context, "SdtGAMError", "GeneXus.Programs");
         AV23Session = new SdtGAMSession(context);
         AV25URL = "";
         AV9AuthenticationTypes = new GXExternalCollection<SdtGAMAuthenticationTypeSimple>( context, "SdtGAMAuthenticationTypeSimple", "GeneXus.Programs");
         AV8AuthenticationType = new SdtGAMAuthenticationTypeSimple(context);
         AV22Repository = new SdtGAMRepository(context);
         AV5AdditionalParameter = new SdtGAMLoginAdditionalParameters(context);
         AV7ApplicationData = "";
         AV14GAMExampleSDTApplicationData = new SdtGAMExampleSDTApplicationData(context);
         AV31GAMUser = new SdtGAMUser(context);
         AV30WebSession = context.GetSession();
         AV11Error = new SdtGAMError(context);
         sStyleString = "";
         lblTextblock3_Jsonclick = "";
         lblTbregister_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short AV28UserRememberMe ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int lblCurrentrepository_Visible ;
      private int edtavUsername_Enabled ;
      private int edtavUserpassword_Enabled ;
      private int divButtons_Visible ;
      private int imgButtonfacebook_Visible ;
      private int imgButtontwitter_Visible ;
      private int imgButtongoogle_Visible ;
      private int imgButtongamremote_Visible ;
      private int AV34GXV1 ;
      private int AV35GXV2 ;
      private int idxLst ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String AV6ApplicationClientId ;
      private String AV18Language ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String divTable1_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String divTablelogin_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String lblCurrentrepository_Internalname ;
      private String lblCurrentrepository_Caption ;
      private String lblCurrentrepository_Jsonclick ;
      private String cmbavLogonto_Internalname ;
      private String TempTags ;
      private String AV20LogOnTo ;
      private String cmbavLogonto_Jsonclick ;
      private String edtavUsername_Internalname ;
      private String edtavUsername_Jsonclick ;
      private String edtavUserpassword_Internalname ;
      private String AV27UserPassword ;
      private String edtavUserpassword_Jsonclick ;
      private String chkavKeepmeloggedin_Internalname ;
      private String chkavRememberme_Internalname ;
      private String bttLogin_Internalname ;
      private String bttLogin_Jsonclick ;
      private String lblTbrememberme2_Internalname ;
      private String lblTbrememberme2_Jsonclick ;
      private String divButtons_Internalname ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String sImgUrl ;
      private String imgButtonfacebook_Internalname ;
      private String imgButtonfacebook_Jsonclick ;
      private String imgButtontwitter_Internalname ;
      private String imgButtontwitter_Jsonclick ;
      private String imgButtongoogle_Internalname ;
      private String imgButtongoogle_Jsonclick ;
      private String imgButtongamremote_Internalname ;
      private String imgButtongamremote_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sStyleString ;
      private String tblTable2_Internalname ;
      private String lblTextblock3_Internalname ;
      private String lblTextblock3_Jsonclick ;
      private String lblTbregister_Internalname ;
      private String lblTbregister_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool AV17KeepMeLoggedIn ;
      private bool AV21RememberMe ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV15isOK ;
      private bool AV16isRedirect ;
      private bool AV24SessionValid ;
      private bool AV19LoginOK ;
      private String AV7ApplicationData ;
      private String AV26UserName ;
      private String AV25URL ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavLogonto ;
      private GXCheckbox chkavKeepmeloggedin ;
      private GXCheckbox chkavRememberme ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IGxSession AV30WebSession ;
      private GXExternalCollection<SdtGAMAuthenticationTypeSimple> AV9AuthenticationTypes ;
      private GXExternalCollection<SdtGAMConnectionInfo> AV10ConnectionInfoCollection ;
      private GXExternalCollection<SdtGAMError> AV12Errors ;
      private GXExternalCollection<SdtGAMError> AV13ErrorsLogin ;
      private SdtGAMLoginAdditionalParameters AV5AdditionalParameter ;
      private SdtGAMAuthenticationTypeSimple AV8AuthenticationType ;
      private SdtGAMError AV11Error ;
      private SdtGAMExampleSDTApplicationData AV14GAMExampleSDTApplicationData ;
      private SdtGAMRepository AV29GAMRepository ;
      private SdtGAMRepository AV22Repository ;
      private SdtGAMUser AV31GAMUser ;
      private SdtGAMSession AV23Session ;
   }

}
