/*
               File: Principal
        Description: Principal
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:35:16.53
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class principal : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public principal( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public principal( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavPersonatipo = new GXCombobox();
         cmbavGridsettingsrowsperpage_gridcoachpaciente = new GXCombobox();
         cmbavGridsettingsrowsperpage_gridemergenciabitacora = new GXCombobox();
         cmbavEmergenciaestado = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridcoachpaciente") == 0 )
            {
               nRC_GXsfl_91 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_91_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_91_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridcoachpaciente_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridcoachpaciente") == 0 )
            {
               AV49CurrentPage_GridEmergenciaBitacora = (short)(NumberUtil.Val( GetNextPar( ), "."));
               A1PersonaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               A2PersonaPNombre = GetNextPar( );
               A4PersonaPApellido = GetNextPar( );
               A8PersonaTipo = (short)(NumberUtil.Val( GetNextPar( ), "."));
               A11PersonaEMail = GetNextPar( );
               n11PersonaEMail = false;
               AV69Pgmname = GetNextPar( );
               AV22CurrentPage_GridCoachPaciente = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV27RowsPerPage_GridCoachPaciente = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV26HasNextPage_GridCoachPaciente = (bool)(BooleanUtil.Val(GetNextPar( )));
               A12CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               A13CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               A16CoachPaciente_PacientePNombre = GetNextPar( );
               n16CoachPaciente_PacientePNombre = false;
               A17CoachPaciente_PacientePApellid = GetNextPar( );
               n17CoachPaciente_PacientePApellid = false;
               A18CoachPaciente_FechaAsignacion = context.localUtil.ParseDateParm( GetNextPar( ));
               n18CoachPaciente_FechaAsignacion = false;
               A19CoachPaciente_DispositivoID = GetNextPar( );
               n19CoachPaciente_DispositivoID = false;
               A20CoachPaciente_Token = GetNextPar( );
               n20CoachPaciente_Token = false;
               AV29CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV13COACH = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridcoachpaciente_refresh( AV49CurrentPage_GridEmergenciaBitacora, A1PersonaID, A2PersonaPNombre, A4PersonaPApellido, A8PersonaTipo, A11PersonaEMail, AV69Pgmname, AV22CurrentPage_GridCoachPaciente, AV27RowsPerPage_GridCoachPaciente, AV26HasNextPage_GridCoachPaciente, A12CoachPaciente_Coach, A13CoachPaciente_Paciente, A16CoachPaciente_PacientePNombre, A17CoachPaciente_PacientePApellid, A18CoachPaciente_FechaAsignacion, A19CoachPaciente_DispositivoID, A20CoachPaciente_Token, AV29CoachPaciente_Coach, AV13COACH) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridemergenciabitacora") == 0 )
            {
               nRC_GXsfl_179 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_179_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_179_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGridemergenciabitacora_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Gridemergenciabitacora") == 0 )
            {
               AV22CurrentPage_GridCoachPaciente = (short)(NumberUtil.Val( GetNextPar( ), "."));
               A1PersonaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               A2PersonaPNombre = GetNextPar( );
               A4PersonaPApellido = GetNextPar( );
               A8PersonaTipo = (short)(NumberUtil.Val( GetNextPar( ), "."));
               A11PersonaEMail = GetNextPar( );
               n11PersonaEMail = false;
               AV69Pgmname = GetNextPar( );
               AV49CurrentPage_GridEmergenciaBitacora = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV54RowsPerPage_GridEmergenciaBitacora = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV53HasNextPage_GridEmergenciaBitacora = (bool)(BooleanUtil.Val(GetNextPar( )));
               A43EmergenciaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               A44EmergenciaFecha = context.localUtil.ParseDTimeParm( GetNextPar( ));
               A45EmergenciaEstado = (short)(NumberUtil.Val( GetNextPar( ), "."));
               A41EmergenciaPaciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               n41EmergenciaPaciente = false;
               A49EmergenciaPacienteNombre = GetNextPar( );
               n49EmergenciaPacienteNombre = false;
               A42EmergenciaCoach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               n42EmergenciaCoach = false;
               A46EmergenciaLongitud = GetNextPar( );
               n46EmergenciaLongitud = false;
               A47EmergenciaLatitud = GetNextPar( );
               n47EmergenciaLatitud = false;
               A48EmergenciaMensaje = GetNextPar( );
               n48EmergenciaMensaje = false;
               AV60EmergenciaCoach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV13COACH = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGridemergenciabitacora_refresh( AV22CurrentPage_GridCoachPaciente, A1PersonaID, A2PersonaPNombre, A4PersonaPApellido, A8PersonaTipo, A11PersonaEMail, AV69Pgmname, AV49CurrentPage_GridEmergenciaBitacora, AV54RowsPerPage_GridEmergenciaBitacora, AV53HasNextPage_GridEmergenciaBitacora, A43EmergenciaID, A44EmergenciaFecha, A45EmergenciaEstado, A41EmergenciaPaciente, A49EmergenciaPacienteNombre, A42EmergenciaCoach, A46EmergenciaLongitud, A47EmergenciaLatitud, A48EmergenciaMensaje, AV60EmergenciaCoach, AV13COACH) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "principal_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA1G2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START1G2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111817351673", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManager.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/json2005.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/rsh.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManagerCreate.js", "", false);
         context.AddJavascriptSource("Tab/TabRender.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("principal.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_91", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_91), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_179", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_179), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCURRENTPAGE_GRIDCOACHPACIENTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22CurrentPage_GridCoachPaciente), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCURRENTPAGE_GRIDEMERGENCIABITACORA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49CurrentPage_GridEmergenciaBitacora), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERSONAID", A1PersonaID.ToString());
         GxWebStd.gx_hidden_field( context, "PERSONAPNOMBRE", A2PersonaPNombre);
         GxWebStd.gx_hidden_field( context, "PERSONAPAPELLIDO", A4PersonaPApellido);
         GxWebStd.gx_hidden_field( context, "PERSONATIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A8PersonaTipo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "PERSONAEMAIL", A11PersonaEMail);
         GxWebStd.gx_hidden_field( context, "vROWSPERPAGE_GRIDCOACHPACIENTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV27RowsPerPage_GridCoachPaciente), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV69Pgmname));
         GxWebStd.gx_boolean_hidden_field( context, "vHASNEXTPAGE_GRIDCOACHPACIENTE", AV26HasNextPage_GridCoachPaciente);
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_COACH", A12CoachPaciente_Coach.ToString());
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_PACIENTE", A13CoachPaciente_Paciente.ToString());
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_PACIENTEPNOMBRE", A16CoachPaciente_PacientePNombre);
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_PACIENTEPAPELLID", A17CoachPaciente_PacientePApellid);
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_FECHAASIGNACION", context.localUtil.DToC( A18CoachPaciente_FechaAsignacion, 0, "/"));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_DISPOSITIVOID", A19CoachPaciente_DispositivoID);
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_TOKEN", A20CoachPaciente_Token);
         GxWebStd.gx_hidden_field( context, "vCOACH", AV13COACH);
         GxWebStd.gx_boolean_hidden_field( context, "vCONFIRMATIONREQUIRED", AV45ConfirmationRequired);
         GxWebStd.gx_hidden_field( context, "vCONFIRMATIONSUBID", StringUtil.RTrim( AV46ConfirmationSubId));
         GxWebStd.gx_hidden_field( context, "vGRIDKEY_COACHPACIENTE_COACH", AV47GridKey_CoachPaciente_Coach.ToString());
         GxWebStd.gx_hidden_field( context, "vGRIDKEY_COACHPACIENTE_PACIENTE", AV48GridKey_CoachPaciente_Paciente.ToString());
         GxWebStd.gx_hidden_field( context, "vROWSPERPAGE_GRIDEMERGENCIABITACORA", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV54RowsPerPage_GridEmergenciaBitacora), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vHASNEXTPAGE_GRIDEMERGENCIABITACORA", AV53HasNextPage_GridEmergenciaBitacora);
         GxWebStd.gx_hidden_field( context, "EMERGENCIAID", A43EmergenciaID.ToString());
         GxWebStd.gx_hidden_field( context, "EMERGENCIAFECHA", context.localUtil.TToC( A44EmergenciaFecha, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "EMERGENCIAESTADO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A45EmergenciaEstado), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "EMERGENCIAPACIENTE", A41EmergenciaPaciente.ToString());
         GxWebStd.gx_hidden_field( context, "EMERGENCIAPACIENTENOMBRE", A49EmergenciaPacienteNombre);
         GxWebStd.gx_hidden_field( context, "EMERGENCIACOACH", A42EmergenciaCoach.ToString());
         GxWebStd.gx_hidden_field( context, "EMERGENCIALONGITUD", A46EmergenciaLongitud);
         GxWebStd.gx_hidden_field( context, "EMERGENCIALATITUD", A47EmergenciaLatitud);
         GxWebStd.gx_hidden_field( context, "EMERGENCIAMENSAJE", A48EmergenciaMensaje);
         GxWebStd.gx_hidden_field( context, "TABS_TABSCONTROL_Class", StringUtil.RTrim( Tabs_tabscontrol_Class));
         GxWebStd.gx_hidden_field( context, "TABS_TABSCONTROL_Pagecount", StringUtil.LTrim( StringUtil.NToC( (decimal)(Tabs_tabscontrol_Pagecount), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TABS_TABSCONTROL_Historymanagement", StringUtil.BoolToStr( Tabs_tabscontrol_Historymanagement));
         GxWebStd.gx_hidden_field( context, "GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(divGridsettings_contentoutertablegridcoachpaciente_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(divGridsettings_contentoutertablegridemergenciabitacora_Visible), 5, 0, ".", "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE1G2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT1G2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("principal.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Principal" ;
      }

      public override String GetPgmdesc( )
      {
         return "Principal" ;
      }

      protected void WB1G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_MainContentTable", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divContenttable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"TABS_TABSCONTROLContainer"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TABS_TABSCONTROLContainer"+"title1"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTab_tabcontrol_title_Internalname, "General", "", "", lblTab_tabcontrol_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Principal.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "Tab_TabControl") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TABS_TABSCONTROLContainer"+"panel1"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintabresponsivetable_tab_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divResponsivetable_mainattributes_attributes_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_FloatLeft", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAttributescontainertable_attributes_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavPersonaid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPersonaid_Internalname, "ID", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPersonaid_Internalname, AV14PersonaID.ToString(), AV14PersonaID.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPersonaid_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtavPersonaid_Enabled, 1, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavPersonapnombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPersonapnombre_Internalname, "Nombre", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPersonapnombre_Internalname, AV15PersonaPNombre, StringUtil.RTrim( context.localUtil.Format( AV15PersonaPNombre, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPersonapnombre_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtavPersonapnombre_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavPersonapapellido_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPersonapapellido_Internalname, "Apellido", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPersonapapellido_Internalname, AV16PersonaPApellido, StringUtil.RTrim( context.localUtil.Format( AV16PersonaPApellido, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPersonapapellido_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtavPersonapapellido_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavPersonatipo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavPersonatipo_Internalname, "Tipo", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavPersonatipo, cmbavPersonatipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0)), 1, cmbavPersonatipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavPersonatipo.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute_Trn", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "", true, "HLP_Principal.htm");
            cmbavPersonatipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPersonatipo_Internalname, "Values", (String)(cmbavPersonatipo.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavPersonaemail_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPersonaemail_Internalname, "EMail", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPersonaemail_Internalname, AV18PersonaEMail, StringUtil.RTrim( context.localUtil.Format( AV18PersonaEMail, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+""+"'"+",false,"+"'"+""+"'", "mailto:"+AV18PersonaEMail, "", "", "", edtavPersonaemail_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtavPersonaemail_Enabled, 1, "email", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GeneXus\\Email", "left", true, "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TABS_TABSCONTROLContainer"+"title2"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTab1_tabcontrol_title_Internalname, "Pacientes", "", "", lblTab1_tabcontrol_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Principal.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "Tab1_TabControl") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TABS_TABSCONTROLContainer"+"panel2"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintabresponsivetable_tab1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_globalgridtable_gridcoachpaciente_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_RoundedBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_table3_gridcoachpaciente_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_TopBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_tableactionscontainer_gridcoachpaciente_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_FullWidth", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table1_64_1G2( true) ;
         }
         else
         {
            wb_table1_64_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table1_64_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaingrid_responsivetable_gridcoachpaciente_Internalname, 1, 0, "px", 0, "px", "Section_Grid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            GridcoachpacienteContainer.SetWrapped(nGXWrapped);
            if ( GridcoachpacienteContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridcoachpacienteContainer"+"DivS\" data-gxgridid=\"91\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridcoachpaciente_Internalname, subGridcoachpaciente_Internalname, "", "Grid_WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridcoachpaciente_Backcolorstyle == 0 )
               {
                  subGridcoachpaciente_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridcoachpaciente_Class) > 0 )
                  {
                     subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"Title";
                  }
               }
               else
               {
                  subGridcoachpaciente_Titlebackstyle = 1;
                  if ( subGridcoachpaciente_Backcolorstyle == 1 )
                  {
                     subGridcoachpaciente_Titlebackcolor = subGridcoachpaciente_Allbackcolor;
                     if ( StringUtil.Len( subGridcoachpaciente_Class) > 0 )
                     {
                        subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridcoachpaciente_Class) > 0 )
                     {
                        subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Coach Token") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Paciente Token") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Paciente") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fecha de registro") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Paciente_Dispositivo ID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Paciente_Token") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridcoachpacienteContainer.AddObjectProperty("GridName", "Gridcoachpaciente");
            }
            else
            {
               GridcoachpacienteContainer.AddObjectProperty("GridName", "Gridcoachpaciente");
               GridcoachpacienteContainer.AddObjectProperty("Class", "Grid_WorkWith");
               GridcoachpacienteContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Backcolorstyle), 1, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Sortable), 1, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("CmpContext", "");
               GridcoachpacienteContainer.AddObjectProperty("InMasterPage", "false");
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", StringUtil.RTrim( AV42Quitar_Action));
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavQuitar_action_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", StringUtil.RTrim( AV43VerRecorridos_Action));
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavVerrecorridos_action_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", AV29CoachPaciente_Coach.ToString());
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCoachpaciente_coach_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", AV30CoachPaciente_Paciente.ToString());
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCoachpaciente_paciente_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", AV31CoachPaciente_PacientePNombre);
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCoachpaciente_pacientepnombre_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", AV32CoachPaciente_PacientePApellido);
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCoachpaciente_pacientepapellido_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", context.localUtil.Format(AV33CoachPaciente_FechaAsignacion, "99/99/99"));
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCoachpaciente_fechaasignacion_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", AV34CoachPaciente_DispositivoID);
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCoachpaciente_dispositivoid_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridcoachpacienteColumn.AddObjectProperty("Value", AV35CoachPaciente_Token);
               GridcoachpacienteColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCoachpaciente_token_Enabled), 5, 0, ".", "")));
               GridcoachpacienteContainer.AddColumnProperties(GridcoachpacienteColumn);
               GridcoachpacienteContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Allowselection), 1, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Selectioncolor), 9, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Allowhovering), 1, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Hoveringcolor), 9, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Allowcollapsing), 1, 0, ".", "")));
               GridcoachpacienteContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 91 )
         {
            wbEnd = 0;
            nRC_GXsfl_91 = (short)(nGXsfl_91_idx-1);
            if ( GridcoachpacienteContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridcoachpacienteContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridcoachpaciente", GridcoachpacienteContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridcoachpacienteContainerData", GridcoachpacienteContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridcoachpacienteContainerData"+"V", GridcoachpacienteContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"GridcoachpacienteContainerData"+"V"+"\" value='"+GridcoachpacienteContainer.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table2_103_1G2( true) ;
         }
         else
         {
            wb_table2_103_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table2_103_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table3_109_1G2( true) ;
         }
         else
         {
            wb_table3_109_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table3_109_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divResponsivetable_containernode_actions_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_FullWidth", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table4_132_1G2( true) ;
         }
         else
         {
            wb_table4_132_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table4_132_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TABS_TABSCONTROLContainer"+"title3"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTab3_tabcontrol_title_Internalname, "Emergencias", "", "", lblTab3_tabcontrol_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Principal.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "Tab3_TabControl") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TABS_TABSCONTROLContainer"+"panel3"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintabresponsivetable_tab3_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_globalgridtable_gridemergenciabitacora_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_RoundedBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_table3_gridemergenciabitacora_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_TopBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_tableactionscontainer_gridemergenciabitacora_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_FullWidth", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table5_152_1G2( true) ;
         }
         else
         {
            wb_table5_152_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table5_152_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaingrid_responsivetable_gridemergenciabitacora_Internalname, 1, 0, "px", 0, "px", "Section_Grid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            GridemergenciabitacoraContainer.SetWrapped(nGXWrapped);
            if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridemergenciabitacoraContainer"+"DivS\" data-gxgridid=\"179\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGridemergenciabitacora_Internalname, subGridemergenciabitacora_Internalname, "", "Grid_WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGridemergenciabitacora_Backcolorstyle == 0 )
               {
                  subGridemergenciabitacora_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGridemergenciabitacora_Class) > 0 )
                  {
                     subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"Title";
                  }
               }
               else
               {
                  subGridemergenciabitacora_Titlebackstyle = 1;
                  if ( subGridemergenciabitacora_Backcolorstyle == 1 )
                  {
                     subGridemergenciabitacora_Titlebackcolor = subGridemergenciabitacora_Allbackcolor;
                     if ( StringUtil.Len( subGridemergenciabitacora_Class) > 0 )
                     {
                        subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGridemergenciabitacora_Class) > 0 )
                     {
                        subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "ID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fecha") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Estado") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Paciente") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Paciente") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Coach") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Longitud") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Latitud") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Mensaje") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridemergenciabitacoraContainer.AddObjectProperty("GridName", "Gridemergenciabitacora");
            }
            else
            {
               GridemergenciabitacoraContainer.AddObjectProperty("GridName", "Gridemergenciabitacora");
               GridemergenciabitacoraContainer.AddObjectProperty("Class", "Grid_WorkWith");
               GridemergenciabitacoraContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Backcolorstyle), 1, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Sortable), 1, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("CmpContext", "");
               GridemergenciabitacoraContainer.AddObjectProperty("InMasterPage", "false");
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", AV56EmergenciaID.ToString());
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergenciaid_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", context.localUtil.TToC( AV57EmergenciaFecha, 10, 8, 0, 3, "/", ":", " "));
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergenciafecha_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV58EmergenciaEstado), 4, 0, ".", "")));
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbavEmergenciaestado.Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", AV59EmergenciaPaciente.ToString());
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergenciapaciente_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", AV65EmergenciaPacienteNombre);
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergenciapacientenombre_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", AV60EmergenciaCoach.ToString());
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergenciacoach_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", AV61EmergenciaLongitud);
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergencialongitud_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", AV62EmergenciaLatitud);
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergencialatitud_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridemergenciabitacoraColumn.AddObjectProperty("Value", AV63EmergenciaMensaje);
               GridemergenciabitacoraColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavEmergenciamensaje_Enabled), 5, 0, ".", "")));
               GridemergenciabitacoraContainer.AddColumnProperties(GridemergenciabitacoraColumn);
               GridemergenciabitacoraContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Allowselection), 1, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Selectioncolor), 9, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Allowhovering), 1, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Hoveringcolor), 9, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Allowcollapsing), 1, 0, ".", "")));
               GridemergenciabitacoraContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 179 )
         {
            wbEnd = 0;
            nRC_GXsfl_179 = (short)(nGXsfl_179_idx-1);
            if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridemergenciabitacoraContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridemergenciabitacora", GridemergenciabitacoraContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridemergenciabitacoraContainerData", GridemergenciabitacoraContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridemergenciabitacoraContainerData"+"V", GridemergenciabitacoraContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"GridemergenciabitacoraContainerData"+"V"+"\" value='"+GridemergenciabitacoraContainer.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table6_191_1G2( true) ;
         }
         else
         {
            wb_table6_191_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table6_191_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table7_197_1G2( true) ;
         }
         else
         {
            wb_table7_197_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table7_197_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table8_217_1G2( true) ;
         }
         else
         {
            wb_table8_217_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table8_217_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"K2BCONTROLBEAUTIFY1Container"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START1G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Principal", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1G0( ) ;
      }

      protected void WS1G2( )
      {
         START1G2( ) ;
         EVT1G2( ) ;
      }

      protected void EVT1G2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'SAVEGRIDSETTINGS(GRIDCOACHPACIENTE)'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'SaveGridSettings(GridCoachPaciente)' */
                              E111G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'E_AGREGARPACIENTE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'E_AgregarPaciente' */
                              E121G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CONFIRMYES'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'ConfirmYes' */
                              E131G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'SAVEGRIDSETTINGS(GRIDEMERGENCIABITACORA)'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'SaveGridSettings(GridEmergenciaBitacora)' */
                              E141G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 25), "GRIDCOACHPACIENTE.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 22), "GRIDCOACHPACIENTE.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'E_QUITAR'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "'E_VERRECORRIDOS'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 10), "'E_QUITAR'") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 17), "'E_VERRECORRIDOS'") == 0 ) )
                           {
                              nGXsfl_91_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
                              SubsflControlProps_912( ) ;
                              AV42Quitar_Action = cgiGet( edtavQuitar_action_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQuitar_action_Internalname, AV42Quitar_Action);
                              AV43VerRecorridos_Action = cgiGet( edtavVerrecorridos_action_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVerrecorridos_action_Internalname, AV43VerRecorridos_Action);
                              if ( StringUtil.StrCmp(cgiGet( edtavCoachpaciente_coach_Internalname), "") == 0 )
                              {
                                 AV29CoachPaciente_Coach = (Guid)(Guid.Empty);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_coach_Internalname, AV29CoachPaciente_Coach.ToString());
                              }
                              else
                              {
                                 try
                                 {
                                    AV29CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCoachpaciente_coach_Internalname)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_coach_Internalname, AV29CoachPaciente_Coach.ToString());
                                 }
                                 catch ( Exception e )
                                 {
                                    GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCOACHPACIENTE_COACH");
                                    GX_FocusControl = edtavCoachpaciente_coach_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                    wbErr = true;
                                 }
                              }
                              if ( StringUtil.StrCmp(cgiGet( edtavCoachpaciente_paciente_Internalname), "") == 0 )
                              {
                                 AV30CoachPaciente_Paciente = (Guid)(Guid.Empty);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_paciente_Internalname, AV30CoachPaciente_Paciente.ToString());
                              }
                              else
                              {
                                 try
                                 {
                                    AV30CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCoachpaciente_paciente_Internalname)));
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_paciente_Internalname, AV30CoachPaciente_Paciente.ToString());
                                 }
                                 catch ( Exception e )
                                 {
                                    GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCOACHPACIENTE_PACIENTE");
                                    GX_FocusControl = edtavCoachpaciente_paciente_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                    wbErr = true;
                                 }
                              }
                              AV31CoachPaciente_PacientePNombre = cgiGet( edtavCoachpaciente_pacientepnombre_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_pacientepnombre_Internalname, AV31CoachPaciente_PacientePNombre);
                              AV32CoachPaciente_PacientePApellido = cgiGet( edtavCoachpaciente_pacientepapellido_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_pacientepapellido_Internalname, AV32CoachPaciente_PacientePApellido);
                              if ( context.localUtil.VCDateTime( cgiGet( edtavCoachpaciente_fechaasignacion_Internalname), 0, 0) == 0 )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Coach Paciente_Fecha Asignacion"}), 1, "vCOACHPACIENTE_FECHAASIGNACION");
                                 GX_FocusControl = edtavCoachpaciente_fechaasignacion_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV33CoachPaciente_FechaAsignacion = (DateTime)(DateTime.MinValue);
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_fechaasignacion_Internalname, context.localUtil.Format(AV33CoachPaciente_FechaAsignacion, "99/99/99"));
                              }
                              else
                              {
                                 AV33CoachPaciente_FechaAsignacion = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavCoachpaciente_fechaasignacion_Internalname), 0));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_fechaasignacion_Internalname, context.localUtil.Format(AV33CoachPaciente_FechaAsignacion, "99/99/99"));
                              }
                              AV34CoachPaciente_DispositivoID = cgiGet( edtavCoachpaciente_dispositivoid_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_dispositivoid_Internalname, AV34CoachPaciente_DispositivoID);
                              AV35CoachPaciente_Token = cgiGet( edtavCoachpaciente_token_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_token_Internalname, AV35CoachPaciente_Token);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Refresh */
                                    E151G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E161G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDCOACHPACIENTE.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E171G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDCOACHPACIENTE.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E181G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'E_QUITAR'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: 'E_Quitar' */
                                    E191G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "'E_VERRECORRIDOS'") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: 'E_VerRecorridos' */
                                    E201G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                           else if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 30), "GRIDEMERGENCIABITACORA.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 27), "GRIDEMERGENCIABITACORA.LOAD") == 0 ) )
                           {
                              nGXsfl_179_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_179_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_179_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1795( ) ;
                              AV56EmergenciaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtavEmergenciaid_Internalname)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciaid_Internalname, AV56EmergenciaID.ToString());
                              AV57EmergenciaFecha = context.localUtil.CToT( cgiGet( edtavEmergenciafecha_Internalname), 0);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciafecha_Internalname, context.localUtil.TToC( AV57EmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
                              cmbavEmergenciaestado.Name = cmbavEmergenciaestado_Internalname;
                              cmbavEmergenciaestado.CurrentValue = cgiGet( cmbavEmergenciaestado_Internalname);
                              AV58EmergenciaEstado = (short)(NumberUtil.Val( cgiGet( cmbavEmergenciaestado_Internalname), "."));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmergenciaestado_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0)));
                              AV59EmergenciaPaciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtavEmergenciapaciente_Internalname)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciapaciente_Internalname, AV59EmergenciaPaciente.ToString());
                              AV65EmergenciaPacienteNombre = cgiGet( edtavEmergenciapacientenombre_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciapacientenombre_Internalname, AV65EmergenciaPacienteNombre);
                              AV60EmergenciaCoach = (Guid)(StringUtil.StrToGuid( cgiGet( edtavEmergenciacoach_Internalname)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciacoach_Internalname, AV60EmergenciaCoach.ToString());
                              AV61EmergenciaLongitud = cgiGet( edtavEmergencialongitud_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergencialongitud_Internalname, AV61EmergenciaLongitud);
                              AV62EmergenciaLatitud = cgiGet( edtavEmergencialatitud_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergencialatitud_Internalname, AV62EmergenciaLatitud);
                              AV63EmergenciaMensaje = cgiGet( edtavEmergenciamensaje_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciamensaje_Internalname, AV63EmergenciaMensaje);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "GRIDEMERGENCIABITACORA.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E211G2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRIDEMERGENCIABITACORA.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E221G5 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA1G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavPersonatipo.Name = "vPERSONATIPO";
            cmbavPersonatipo.WebTags = "";
            cmbavPersonatipo.addItem("1", "COACH", 0);
            cmbavPersonatipo.addItem("2", "Paciente", 0);
            if ( cmbavPersonatipo.ItemCount > 0 )
            {
               AV17PersonaTipo = (short)(NumberUtil.Val( cmbavPersonatipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0)));
            }
            cmbavGridsettingsrowsperpage_gridcoachpaciente.Name = "vGRIDSETTINGSROWSPERPAGE_GRIDCOACHPACIENTE";
            cmbavGridsettingsrowsperpage_gridcoachpaciente.WebTags = "";
            cmbavGridsettingsrowsperpage_gridcoachpaciente.addItem("10", "10", 0);
            cmbavGridsettingsrowsperpage_gridcoachpaciente.addItem("20", "20", 0);
            cmbavGridsettingsrowsperpage_gridcoachpaciente.addItem("50", "50", 0);
            cmbavGridsettingsrowsperpage_gridcoachpaciente.addItem("100", "100", 0);
            cmbavGridsettingsrowsperpage_gridcoachpaciente.addItem("200", "200", 0);
            if ( cmbavGridsettingsrowsperpage_gridcoachpaciente.ItemCount > 0 )
            {
               AV28GridSettingsRowsPerPage_GridCoachPaciente = (short)(NumberUtil.Val( cmbavGridsettingsrowsperpage_gridcoachpaciente.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GridSettingsRowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0)));
            }
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.Name = "vGRIDSETTINGSROWSPERPAGE_GRIDEMERGENCIABITACORA";
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.WebTags = "";
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.addItem("10", "10", 0);
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.addItem("20", "20", 0);
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.addItem("50", "50", 0);
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.addItem("100", "100", 0);
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.addItem("200", "200", 0);
            if ( cmbavGridsettingsrowsperpage_gridemergenciabitacora.ItemCount > 0 )
            {
               AV55GridSettingsRowsPerPage_GridEmergenciaBitacora = (short)(NumberUtil.Val( cmbavGridsettingsrowsperpage_gridemergenciabitacora.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridSettingsRowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0)));
            }
            GXCCtl = "vEMERGENCIAESTADO_" + sGXsfl_179_idx;
            cmbavEmergenciaestado.Name = GXCCtl;
            cmbavEmergenciaestado.WebTags = "";
            cmbavEmergenciaestado.addItem("1", "Activa", 0);
            cmbavEmergenciaestado.addItem("2", "Finalizada", 0);
            if ( cmbavEmergenciaestado.ItemCount > 0 )
            {
               AV58EmergenciaEstado = (short)(NumberUtil.Val( cmbavEmergenciaestado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmergenciaestado_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPersonaid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridcoachpaciente_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_912( ) ;
         while ( nGXsfl_91_idx <= nRC_GXsfl_91 )
         {
            sendrow_912( ) ;
            nGXsfl_91_idx = (short)(((subGridcoachpaciente_Islastpage==1)&&(nGXsfl_91_idx+1>subGridcoachpaciente_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( GridcoachpacienteContainer));
         /* End function gxnrGridcoachpaciente_newrow */
      }

      protected void gxnrGridemergenciabitacora_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1795( ) ;
         while ( nGXsfl_179_idx <= nRC_GXsfl_179 )
         {
            sendrow_1795( ) ;
            nGXsfl_179_idx = (short)(((subGridemergenciabitacora_Islastpage==1)&&(nGXsfl_179_idx+1>subGridemergenciabitacora_Recordsperpage( )) ? 1 : nGXsfl_179_idx+1));
            sGXsfl_179_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_179_idx), 4, 0)), 4, "0");
            SubsflControlProps_1795( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( GridemergenciabitacoraContainer));
         /* End function gxnrGridemergenciabitacora_newrow */
      }

      protected void gxgrGridcoachpaciente_refresh( short AV49CurrentPage_GridEmergenciaBitacora ,
                                                    Guid A1PersonaID ,
                                                    String A2PersonaPNombre ,
                                                    String A4PersonaPApellido ,
                                                    short A8PersonaTipo ,
                                                    String A11PersonaEMail ,
                                                    String AV69Pgmname ,
                                                    short AV22CurrentPage_GridCoachPaciente ,
                                                    short AV27RowsPerPage_GridCoachPaciente ,
                                                    bool AV26HasNextPage_GridCoachPaciente ,
                                                    Guid A12CoachPaciente_Coach ,
                                                    Guid A13CoachPaciente_Paciente ,
                                                    String A16CoachPaciente_PacientePNombre ,
                                                    String A17CoachPaciente_PacientePApellid ,
                                                    DateTime A18CoachPaciente_FechaAsignacion ,
                                                    String A19CoachPaciente_DispositivoID ,
                                                    String A20CoachPaciente_Token ,
                                                    Guid AV29CoachPaciente_Coach ,
                                                    String AV13COACH )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRIDCOACHPACIENTE_nCurrentRecord = 0;
         RF1G2( ) ;
         /* End function gxgrGridcoachpaciente_refresh */
      }

      protected void gxgrGridemergenciabitacora_refresh( short AV22CurrentPage_GridCoachPaciente ,
                                                         Guid A1PersonaID ,
                                                         String A2PersonaPNombre ,
                                                         String A4PersonaPApellido ,
                                                         short A8PersonaTipo ,
                                                         String A11PersonaEMail ,
                                                         String AV69Pgmname ,
                                                         short AV49CurrentPage_GridEmergenciaBitacora ,
                                                         short AV54RowsPerPage_GridEmergenciaBitacora ,
                                                         bool AV53HasNextPage_GridEmergenciaBitacora ,
                                                         Guid A43EmergenciaID ,
                                                         DateTime A44EmergenciaFecha ,
                                                         short A45EmergenciaEstado ,
                                                         Guid A41EmergenciaPaciente ,
                                                         String A49EmergenciaPacienteNombre ,
                                                         Guid A42EmergenciaCoach ,
                                                         String A46EmergenciaLongitud ,
                                                         String A47EmergenciaLatitud ,
                                                         String A48EmergenciaMensaje ,
                                                         Guid AV60EmergenciaCoach ,
                                                         String AV13COACH )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         /* Execute user event: Refresh */
         E151G2 ();
         GRIDEMERGENCIABITACORA_nCurrentRecord = 0;
         RF1G5( ) ;
         /* End function gxgrGridemergenciabitacora_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavPersonatipo.ItemCount > 0 )
         {
            AV17PersonaTipo = (short)(NumberUtil.Val( cmbavPersonatipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavPersonatipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPersonatipo_Internalname, "Values", cmbavPersonatipo.ToJavascriptSource(), true);
         }
         if ( cmbavGridsettingsrowsperpage_gridcoachpaciente.ItemCount > 0 )
         {
            AV28GridSettingsRowsPerPage_GridCoachPaciente = (short)(NumberUtil.Val( cmbavGridsettingsrowsperpage_gridcoachpaciente.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GridSettingsRowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavGridsettingsrowsperpage_gridcoachpaciente.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname, "Values", cmbavGridsettingsrowsperpage_gridcoachpaciente.ToJavascriptSource(), true);
         }
         if ( cmbavGridsettingsrowsperpage_gridemergenciabitacora.ItemCount > 0 )
         {
            AV55GridSettingsRowsPerPage_GridEmergenciaBitacora = (short)(NumberUtil.Val( cmbavGridsettingsrowsperpage_gridemergenciabitacora.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridSettingsRowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname, "Values", cmbavGridsettingsrowsperpage_gridemergenciabitacora.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         /* Execute user event: Refresh */
         E151G2 ();
         RF1G2( ) ;
         RF1G5( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV69Pgmname = "Principal";
         context.Gx_err = 0;
         edtavPersonaid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPersonaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPersonaid_Enabled), 5, 0)), true);
         edtavQuitar_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuitar_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuitar_action_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavVerrecorridos_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVerrecorridos_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVerrecorridos_action_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_coach_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_coach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_coach_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_paciente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_paciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_paciente_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_pacientepnombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_pacientepnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_pacientepnombre_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_pacientepapellido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_pacientepapellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_pacientepapellido_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_fechaasignacion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_fechaasignacion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_fechaasignacion_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_dispositivoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_dispositivoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_dispositivoid_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_token_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_token_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_token_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavEmergenciaid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciaid_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciafecha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciafecha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciafecha_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         cmbavEmergenciaestado.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEmergenciaestado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavEmergenciaestado.Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciapaciente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciapaciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciapaciente_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciapacientenombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciapacientenombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciapacientenombre_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciacoach_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciacoach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciacoach_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergencialongitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergencialongitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergencialongitud_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergencialatitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergencialatitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergencialatitud_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciamensaje_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciamensaje_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciamensaje_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavConfirmmessage_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConfirmmessage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConfirmmessage_Enabled), 5, 0)), true);
      }

      protected void RF1G2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridcoachpacienteContainer.ClearRows();
         }
         wbStart = 91;
         E171G2 ();
         nGXsfl_91_idx = 1;
         sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
         SubsflControlProps_912( ) ;
         bGXsfl_91_Refreshing = true;
         GridcoachpacienteContainer.AddObjectProperty("GridName", "Gridcoachpaciente");
         GridcoachpacienteContainer.AddObjectProperty("CmpContext", "");
         GridcoachpacienteContainer.AddObjectProperty("InMasterPage", "false");
         GridcoachpacienteContainer.AddObjectProperty("Class", "Grid_WorkWith");
         GridcoachpacienteContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridcoachpacienteContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridcoachpacienteContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Backcolorstyle), 1, 0, ".", "")));
         GridcoachpacienteContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridcoachpaciente_Sortable), 1, 0, ".", "")));
         GridcoachpacienteContainer.PageSize = subGridcoachpaciente_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_912( ) ;
            E181G2 ();
            wbEnd = 91;
            WB1G0( ) ;
         }
         bGXsfl_91_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes1G2( )
      {
      }

      protected void RF1G5( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridemergenciabitacoraContainer.ClearRows();
         }
         wbStart = 179;
         E211G2 ();
         nGXsfl_179_idx = 1;
         sGXsfl_179_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_179_idx), 4, 0)), 4, "0");
         SubsflControlProps_1795( ) ;
         bGXsfl_179_Refreshing = true;
         GridemergenciabitacoraContainer.AddObjectProperty("GridName", "Gridemergenciabitacora");
         GridemergenciabitacoraContainer.AddObjectProperty("CmpContext", "");
         GridemergenciabitacoraContainer.AddObjectProperty("InMasterPage", "false");
         GridemergenciabitacoraContainer.AddObjectProperty("Class", "Grid_WorkWith");
         GridemergenciabitacoraContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridemergenciabitacoraContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridemergenciabitacoraContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Backcolorstyle), 1, 0, ".", "")));
         GridemergenciabitacoraContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridemergenciabitacora_Sortable), 1, 0, ".", "")));
         GridemergenciabitacoraContainer.PageSize = subGridemergenciabitacora_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1795( ) ;
            E221G5 ();
            wbEnd = 179;
            WB1G0( ) ;
         }
         bGXsfl_179_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes1G5( )
      {
      }

      protected int subGridcoachpaciente_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridcoachpaciente_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridcoachpaciente_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridcoachpaciente_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridemergenciabitacora_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGridemergenciabitacora_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGridemergenciabitacora_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGridemergenciabitacora_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP1G0( )
      {
         /* Before Start, stand alone formulas. */
         AV69Pgmname = "Principal";
         context.Gx_err = 0;
         edtavPersonaid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPersonaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPersonaid_Enabled), 5, 0)), true);
         edtavQuitar_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavQuitar_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavQuitar_action_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavVerrecorridos_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVerrecorridos_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVerrecorridos_action_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_coach_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_coach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_coach_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_paciente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_paciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_paciente_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_pacientepnombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_pacientepnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_pacientepnombre_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_pacientepapellido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_pacientepapellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_pacientepapellido_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_fechaasignacion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_fechaasignacion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_fechaasignacion_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_dispositivoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_dispositivoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_dispositivoid_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavCoachpaciente_token_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCoachpaciente_token_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCoachpaciente_token_Enabled), 5, 0)), !bGXsfl_91_Refreshing);
         edtavEmergenciaid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciaid_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciafecha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciafecha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciafecha_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         cmbavEmergenciaestado.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEmergenciaestado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavEmergenciaestado.Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciapaciente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciapaciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciapaciente_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciapacientenombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciapacientenombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciapacientenombre_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciacoach_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciacoach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciacoach_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergencialongitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergencialongitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergencialongitud_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergencialatitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergencialatitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergencialatitud_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavEmergenciamensaje_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEmergenciamensaje_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEmergenciamensaje_Enabled), 5, 0)), !bGXsfl_179_Refreshing);
         edtavConfirmmessage_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConfirmmessage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConfirmmessage_Enabled), 5, 0)), true);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E161G2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( StringUtil.StrCmp(cgiGet( edtavPersonaid_Internalname), "") == 0 )
            {
               AV14PersonaID = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14PersonaID", AV14PersonaID.ToString());
            }
            else
            {
               try
               {
                  AV14PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtavPersonaid_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14PersonaID", AV14PersonaID.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vPERSONAID");
                  GX_FocusControl = edtavPersonaid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            AV15PersonaPNombre = cgiGet( edtavPersonapnombre_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15PersonaPNombre", AV15PersonaPNombre);
            AV16PersonaPApellido = cgiGet( edtavPersonapapellido_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16PersonaPApellido", AV16PersonaPApellido);
            cmbavPersonatipo.Name = cmbavPersonatipo_Internalname;
            cmbavPersonatipo.CurrentValue = cgiGet( cmbavPersonatipo_Internalname);
            AV17PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbavPersonatipo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0)));
            AV18PersonaEMail = cgiGet( edtavPersonaemail_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PersonaEMail", AV18PersonaEMail);
            cmbavGridsettingsrowsperpage_gridcoachpaciente.Name = cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname;
            cmbavGridsettingsrowsperpage_gridcoachpaciente.CurrentValue = cgiGet( cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname);
            AV28GridSettingsRowsPerPage_GridCoachPaciente = (short)(NumberUtil.Val( cgiGet( cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GridSettingsRowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0)));
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.Name = cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname;
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.CurrentValue = cgiGet( cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname);
            AV55GridSettingsRowsPerPage_GridEmergenciaBitacora = (short)(NumberUtil.Val( cgiGet( cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridSettingsRowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0)));
            AV44ConfirmMessage = cgiGet( edtavConfirmmessage_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ConfirmMessage", AV44ConfirmMessage);
            /* Read saved values. */
            nRC_GXsfl_91 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_91"), ",", "."));
            nRC_GXsfl_179 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_179"), ",", "."));
            AV49CurrentPage_GridEmergenciaBitacora = (short)(context.localUtil.CToN( cgiGet( "vCURRENTPAGE_GRIDEMERGENCIABITACORA"), ",", "."));
            AV53HasNextPage_GridEmergenciaBitacora = StringUtil.StrToBool( cgiGet( "vHASNEXTPAGE_GRIDEMERGENCIABITACORA"));
            AV54RowsPerPage_GridEmergenciaBitacora = (short)(context.localUtil.CToN( cgiGet( "vROWSPERPAGE_GRIDEMERGENCIABITACORA"), ",", "."));
            AV46ConfirmationSubId = cgiGet( "vCONFIRMATIONSUBID");
            AV22CurrentPage_GridCoachPaciente = (short)(context.localUtil.CToN( cgiGet( "vCURRENTPAGE_GRIDCOACHPACIENTE"), ",", "."));
            AV26HasNextPage_GridCoachPaciente = StringUtil.StrToBool( cgiGet( "vHASNEXTPAGE_GRIDCOACHPACIENTE"));
            AV27RowsPerPage_GridCoachPaciente = (short)(context.localUtil.CToN( cgiGet( "vROWSPERPAGE_GRIDCOACHPACIENTE"), ",", "."));
            Tabs_tabscontrol_Class = cgiGet( "TABS_TABSCONTROL_Class");
            Tabs_tabscontrol_Pagecount = (int)(context.localUtil.CToN( cgiGet( "TABS_TABSCONTROL_Pagecount"), ",", "."));
            Tabs_tabscontrol_Historymanagement = StringUtil.StrToBool( cgiGet( "TABS_TABSCONTROL_Historymanagement"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void S122( )
      {
         /* 'U_REFRESHPAGE' Routine */
      }

      protected void S112( )
      {
         /* 'U_STARTPAGE' Routine */
         AV13COACH = AV12WebSession.Get("COACH");
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13COACH", AV13COACH);
         GX_msglist.addItem(AV13COACH);
         /* Using cursor H001G2 */
         pr_default.execute(0);
         while ( (pr_default.getStatus(0) != 101) )
         {
            A1PersonaID = (Guid)((Guid)(H001G2_A1PersonaID[0]));
            A2PersonaPNombre = H001G2_A2PersonaPNombre[0];
            A4PersonaPApellido = H001G2_A4PersonaPApellido[0];
            A8PersonaTipo = H001G2_A8PersonaTipo[0];
            A11PersonaEMail = H001G2_A11PersonaEMail[0];
            n11PersonaEMail = H001G2_n11PersonaEMail[0];
            if ( A1PersonaID == StringUtil.StrToGuid( StringUtil.Trim( AV13COACH)) )
            {
               AV14PersonaID = (Guid)(A1PersonaID);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14PersonaID", AV14PersonaID.ToString());
               AV15PersonaPNombre = A2PersonaPNombre;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15PersonaPNombre", AV15PersonaPNombre);
               AV16PersonaPApellido = A4PersonaPApellido;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16PersonaPApellido", AV16PersonaPApellido);
               AV17PersonaTipo = A8PersonaTipo;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0)));
               AV18PersonaEMail = A11PersonaEMail;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18PersonaEMail", AV18PersonaEMail);
               edtavPersonaid_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPersonaid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPersonaid_Enabled), 5, 0)), true);
               edtavPersonapnombre_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPersonapnombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPersonapnombre_Enabled), 5, 0)), true);
               edtavPersonapapellido_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPersonapapellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPersonapapellido_Enabled), 5, 0)), true);
               cmbavPersonatipo.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPersonatipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavPersonatipo.Enabled), 5, 0)), true);
               edtavPersonaemail_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavPersonaemail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPersonaemail_Enabled), 5, 0)), true);
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
      }

      protected void S132( )
      {
         /* 'U_OPENPAGE' Routine */
      }

      protected void E151G2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         /* Execute user subroutine: 'U_STARTPAGE' */
         S112 ();
         if (returnInSub) return;
         AV45ConfirmationRequired = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ConfirmationRequired", AV45ConfirmationRequired);
         subGridcoachpaciente_Backcolorstyle = 3;
         divGridsettings_contentoutertablegridcoachpaciente_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divGridsettings_contentoutertablegridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divGridsettings_contentoutertablegridcoachpaciente_Visible), 5, 0)), true);
         if ( (0==AV22CurrentPage_GridCoachPaciente) )
         {
            AV22CurrentPage_GridCoachPaciente = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CurrentPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22CurrentPage_GridCoachPaciente), 4, 0)));
         }
         AV23Reload_GridCoachPaciente = true;
         subGridemergenciabitacora_Backcolorstyle = 3;
         divGridsettings_contentoutertablegridemergenciabitacora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divGridsettings_contentoutertablegridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divGridsettings_contentoutertablegridemergenciabitacora_Visible), 5, 0)), true);
         if ( (0==AV49CurrentPage_GridEmergenciaBitacora) )
         {
            AV49CurrentPage_GridEmergenciaBitacora = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49CurrentPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49CurrentPage_GridEmergenciaBitacora), 4, 0)));
         }
         AV50Reload_GridEmergenciaBitacora = true;
         /* Execute user subroutine: 'U_REFRESHPAGE' */
         S122 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
         cmbavPersonatipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPersonatipo_Internalname, "Values", cmbavPersonatipo.ToJavascriptSource(), true);
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E161G2 ();
         if (returnInSub) return;
      }

      protected void E161G2( )
      {
         /* Start Routine */
         AV27RowsPerPage_GridCoachPaciente = 20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27RowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27RowsPerPage_GridCoachPaciente), 4, 0)));
         AV28GridSettingsRowsPerPage_GridCoachPaciente = 20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GridSettingsRowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0)));
         AV54RowsPerPage_GridEmergenciaBitacora = 20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54RowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54RowsPerPage_GridEmergenciaBitacora), 4, 0)));
         AV55GridSettingsRowsPerPage_GridEmergenciaBitacora = 20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridSettingsRowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0)));
         /* Execute user subroutine: 'U_OPENPAGE' */
         S132 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADGRIDSTATE(GRIDCOACHPACIENTE)' */
         S142 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADGRIDSTATE(GRIDEMERGENCIABITACORA)' */
         S152 ();
         if (returnInSub) return;
         tblTableconditionalconfirm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTableconditionalconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTableconditionalconfirm_Visible), 5, 0)), true);
      }

      protected void S202( )
      {
         /* 'U_WHENNONE(GRIDCOACHPACIENTE)' Routine */
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE(GRIDCOACHPACIENTE)' Routine */
         AV19GridStateKey = "GridCoachPaciente";
         new k2bloadgridstate(context ).execute(  AV69Pgmname,  AV19GridStateKey, out  AV20GridState) ;
         if ( AV20GridState.gxTpr_Currentpage > 0 )
         {
            AV22CurrentPage_GridCoachPaciente = AV20GridState.gxTpr_Currentpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CurrentPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22CurrentPage_GridCoachPaciente), 4, 0)));
         }
         if ( AV20GridState.gxTpr_Rowsperpage > 0 )
         {
            AV27RowsPerPage_GridCoachPaciente = AV20GridState.gxTpr_Rowsperpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27RowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27RowsPerPage_GridCoachPaciente), 4, 0)));
            AV28GridSettingsRowsPerPage_GridCoachPaciente = AV20GridState.gxTpr_Rowsperpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GridSettingsRowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0)));
         }
      }

      protected void S162( )
      {
         /* 'SAVEGRIDSTATE(GRIDCOACHPACIENTE)' Routine */
         AV19GridStateKey = "GridCoachPaciente";
         new k2bloadgridstate(context ).execute(  AV69Pgmname,  AV19GridStateKey, out  AV20GridState) ;
         AV20GridState.gxTpr_Currentpage = AV22CurrentPage_GridCoachPaciente;
         AV20GridState.gxTpr_Rowsperpage = AV27RowsPerPage_GridCoachPaciente;
         AV20GridState.gxTpr_Filtervalues.Clear();
         new k2bsavegridstate(context ).execute(  AV69Pgmname,  AV19GridStateKey,  AV20GridState) ;
      }

      protected void E111G2( )
      {
         /* 'SaveGridSettings(GridCoachPaciente)' Routine */
         if ( AV27RowsPerPage_GridCoachPaciente != AV28GridSettingsRowsPerPage_GridCoachPaciente )
         {
            AV27RowsPerPage_GridCoachPaciente = AV28GridSettingsRowsPerPage_GridCoachPaciente;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27RowsPerPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV27RowsPerPage_GridCoachPaciente), 4, 0)));
            AV22CurrentPage_GridCoachPaciente = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CurrentPage_GridCoachPaciente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22CurrentPage_GridCoachPaciente), 4, 0)));
         }
         gxgrGridcoachpaciente_refresh( AV49CurrentPage_GridEmergenciaBitacora, A1PersonaID, A2PersonaPNombre, A4PersonaPApellido, A8PersonaTipo, A11PersonaEMail, AV69Pgmname, AV22CurrentPage_GridCoachPaciente, AV27RowsPerPage_GridCoachPaciente, AV26HasNextPage_GridCoachPaciente, A12CoachPaciente_Coach, A13CoachPaciente_Paciente, A16CoachPaciente_PacientePNombre, A17CoachPaciente_PacientePApellid, A18CoachPaciente_FechaAsignacion, A19CoachPaciente_DispositivoID, A20CoachPaciente_Token, AV29CoachPaciente_Coach, AV13COACH) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE(GRIDCOACHPACIENTE)' */
         S162 ();
         if (returnInSub) return;
         divGridsettings_contentoutertablegridcoachpaciente_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divGridsettings_contentoutertablegridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divGridsettings_contentoutertablegridcoachpaciente_Visible), 5, 0)), true);
         /*  Sending Event outputs  */
         cmbavPersonatipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPersonatipo_Internalname, "Values", cmbavPersonatipo.ToJavascriptSource(), true);
      }

      protected void E171G2( )
      {
         /* Gridcoachpaciente_Refresh Routine */
         /* Execute user subroutine: 'SAVEGRIDSTATE(GRIDCOACHPACIENTE)' */
         S162 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEPAGINGCONTROLS(GRIDCOACHPACIENTE)' */
         S172 ();
         if (returnInSub) return;
         subGridcoachpaciente_Backcolorstyle = 3;
         /* Execute user subroutine: 'U_GRIDREFRESH(GRIDCOACHPACIENTE)' */
         S182 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S194( )
      {
         /* 'U_LOADROWVARS(GRIDCOACHPACIENTE)' Routine */
         if ( AV29CoachPaciente_Coach == StringUtil.StrToGuid( AV13COACH) )
         {
            AV24LoadRow_GridCoachPaciente = true;
         }
         else
         {
            AV24LoadRow_GridCoachPaciente = false;
         }
      }

      protected void S172( )
      {
         /* 'UPDATEPAGINGCONTROLS(GRIDCOACHPACIENTE)' Routine */
         lblPaginationbar_firstpagetextblockgridcoachpaciente_Caption = "1";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridcoachpaciente_Internalname, "Caption", lblPaginationbar_firstpagetextblockgridcoachpaciente_Caption, true);
         lblPaginationbar_previouspagetextblockgridcoachpaciente_Caption = StringUtil.Str( (decimal)(AV22CurrentPage_GridCoachPaciente-1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgridcoachpaciente_Internalname, "Caption", lblPaginationbar_previouspagetextblockgridcoachpaciente_Caption, true);
         lblPaginationbar_currentpagetextblockgridcoachpaciente_Caption = StringUtil.Str( (decimal)(AV22CurrentPage_GridCoachPaciente), 4, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_currentpagetextblockgridcoachpaciente_Internalname, "Caption", lblPaginationbar_currentpagetextblockgridcoachpaciente_Caption, true);
         lblPaginationbar_nextpagetextblockgridcoachpaciente_Caption = StringUtil.Str( (decimal)(AV22CurrentPage_GridCoachPaciente+1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgridcoachpaciente_Internalname, "Caption", lblPaginationbar_nextpagetextblockgridcoachpaciente_Caption, true);
         lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Internalname, "Class", lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class, true);
         lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Internalname, "Class", lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class, true);
         if ( (0==AV22CurrentPage_GridCoachPaciente) || ( AV22CurrentPage_GridCoachPaciente <= 1 ) )
         {
            lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Internalname, "Class", lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class, true);
            cellPaginationbar_firstpagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgridcoachpaciente_Internalname, "Class", cellPaginationbar_firstpagecellgridcoachpaciente_Class, true);
            lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible), 5, 0)), true);
            cellPaginationbar_spacingleftcellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridcoachpaciente_Internalname, "Class", cellPaginationbar_spacingleftcellgridcoachpaciente_Class, true);
            lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible), 5, 0)), true);
            cellPaginationbar_previouspagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_previouspagecellgridcoachpaciente_Internalname, "Class", cellPaginationbar_previouspagecellgridcoachpaciente_Class, true);
            lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible), 5, 0)), true);
         }
         else
         {
            cellPaginationbar_previouspagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationPrevious";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_previouspagecellgridcoachpaciente_Internalname, "Class", cellPaginationbar_previouspagecellgridcoachpaciente_Class, true);
            lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible), 5, 0)), true);
            if ( AV22CurrentPage_GridCoachPaciente == 2 )
            {
               cellPaginationbar_firstpagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgridcoachpaciente_Internalname, "Class", cellPaginationbar_firstpagecellgridcoachpaciente_Class, true);
               lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible), 5, 0)), true);
               cellPaginationbar_spacingleftcellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridcoachpaciente_Internalname, "Class", cellPaginationbar_spacingleftcellgridcoachpaciente_Class, true);
               lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible), 5, 0)), true);
            }
            else
            {
               cellPaginationbar_firstpagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationLeft";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgridcoachpaciente_Internalname, "Class", cellPaginationbar_firstpagecellgridcoachpaciente_Class, true);
               lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible), 5, 0)), true);
               if ( AV22CurrentPage_GridCoachPaciente == 3 )
               {
                  cellPaginationbar_spacingleftcellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridcoachpaciente_Internalname, "Class", cellPaginationbar_spacingleftcellgridcoachpaciente_Class, true);
                  lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible), 5, 0)), true);
               }
               else
               {
                  cellPaginationbar_spacingleftcellgridcoachpaciente_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridcoachpaciente_Internalname, "Class", cellPaginationbar_spacingleftcellgridcoachpaciente_Class, true);
                  lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible), 5, 0)), true);
               }
            }
         }
         if ( ! AV26HasNextPage_GridCoachPaciente )
         {
            lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Internalname, "Class", lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class, true);
            cellPaginationbar_spacingrightcellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingrightcellgridcoachpaciente_Internalname, "Class", cellPaginationbar_spacingrightcellgridcoachpaciente_Class, true);
            lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacingrighttextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible), 5, 0)), true);
            cellPaginationbar_nextpagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_nextpagecellgridcoachpaciente_Internalname, "Class", cellPaginationbar_nextpagecellgridcoachpaciente_Class, true);
            lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible), 5, 0)), true);
         }
         else
         {
            cellPaginationbar_nextpagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationNext";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_nextpagecellgridcoachpaciente_Internalname, "Class", cellPaginationbar_nextpagecellgridcoachpaciente_Class, true);
            lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible), 5, 0)), true);
            cellPaginationbar_spacingrightcellgridcoachpaciente_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingrightcellgridcoachpaciente_Internalname, "Class", cellPaginationbar_spacingrightcellgridcoachpaciente_Class, true);
            lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacingrighttextblockgridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible), 5, 0)), true);
         }
         if ( ( AV22CurrentPage_GridCoachPaciente <= 1 ) && ! AV26HasNextPage_GridCoachPaciente )
         {
            tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblPaginationbar_pagingcontainertablegridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible), 5, 0)), true);
         }
         else
         {
            tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblPaginationbar_pagingcontainertablegridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible), 5, 0)), true);
         }
      }

      private void E181G2( )
      {
         /* Gridcoachpaciente_Load Routine */
         tblI_noresultsfoundtablename_gridcoachpaciente_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblI_noresultsfoundtablename_gridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblI_noresultsfoundtablename_gridcoachpaciente_Visible), 5, 0)), true);
         AV25I_LoadCount_GridCoachPaciente = 0;
         AV26HasNextPage_GridCoachPaciente = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26HasNextPage_GridCoachPaciente", AV26HasNextPage_GridCoachPaciente);
         AV70GXLvl261 = 0;
         /* Using cursor H001G3 */
         pr_default.execute(1);
         while ( (pr_default.getStatus(1) != 101) )
         {
            A12CoachPaciente_Coach = (Guid)((Guid)(H001G3_A12CoachPaciente_Coach[0]));
            A16CoachPaciente_PacientePNombre = H001G3_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = H001G3_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = H001G3_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = H001G3_n17CoachPaciente_PacientePApellid[0];
            A18CoachPaciente_FechaAsignacion = H001G3_A18CoachPaciente_FechaAsignacion[0];
            n18CoachPaciente_FechaAsignacion = H001G3_n18CoachPaciente_FechaAsignacion[0];
            A19CoachPaciente_DispositivoID = H001G3_A19CoachPaciente_DispositivoID[0];
            n19CoachPaciente_DispositivoID = H001G3_n19CoachPaciente_DispositivoID[0];
            A20CoachPaciente_Token = H001G3_A20CoachPaciente_Token[0];
            n20CoachPaciente_Token = H001G3_n20CoachPaciente_Token[0];
            A13CoachPaciente_Paciente = (Guid)((Guid)(H001G3_A13CoachPaciente_Paciente[0]));
            A16CoachPaciente_PacientePNombre = H001G3_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = H001G3_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = H001G3_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = H001G3_n17CoachPaciente_PacientePApellid[0];
            AV70GXLvl261 = 1;
            AV29CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_coach_Internalname, AV29CoachPaciente_Coach.ToString());
            AV30CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_paciente_Internalname, AV30CoachPaciente_Paciente.ToString());
            AV31CoachPaciente_PacientePNombre = A16CoachPaciente_PacientePNombre;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_pacientepnombre_Internalname, AV31CoachPaciente_PacientePNombre);
            AV32CoachPaciente_PacientePApellido = A17CoachPaciente_PacientePApellid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_pacientepapellido_Internalname, AV32CoachPaciente_PacientePApellido);
            AV33CoachPaciente_FechaAsignacion = A18CoachPaciente_FechaAsignacion;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_fechaasignacion_Internalname, context.localUtil.Format(AV33CoachPaciente_FechaAsignacion, "99/99/99"));
            AV34CoachPaciente_DispositivoID = A19CoachPaciente_DispositivoID;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_dispositivoid_Internalname, AV34CoachPaciente_DispositivoID);
            AV35CoachPaciente_Token = A20CoachPaciente_Token;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavCoachpaciente_token_Internalname, AV35CoachPaciente_Token);
            AV42Quitar_Action = "Quitar";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavQuitar_action_Internalname, AV42Quitar_Action);
            AV43VerRecorridos_Action = "Recorridos";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavVerrecorridos_action_Internalname, AV43VerRecorridos_Action);
            AV25I_LoadCount_GridCoachPaciente = (short)(AV25I_LoadCount_GridCoachPaciente+1);
            AV24LoadRow_GridCoachPaciente = true;
            /* Execute user subroutine: 'U_LOADROWVARS(GRIDCOACHPACIENTE)' */
            S194 ();
            if ( returnInSub )
            {
               pr_default.close(1);
               returnInSub = true;
               if (true) return;
            }
            if ( AV24LoadRow_GridCoachPaciente )
            {
               if ( AV25I_LoadCount_GridCoachPaciente > AV27RowsPerPage_GridCoachPaciente * AV22CurrentPage_GridCoachPaciente )
               {
                  AV26HasNextPage_GridCoachPaciente = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26HasNextPage_GridCoachPaciente", AV26HasNextPage_GridCoachPaciente);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               if ( ( AV25I_LoadCount_GridCoachPaciente > ( AV27RowsPerPage_GridCoachPaciente * ( AV22CurrentPage_GridCoachPaciente - 1 ) ) ) && ( AV25I_LoadCount_GridCoachPaciente <= ( AV27RowsPerPage_GridCoachPaciente * AV22CurrentPage_GridCoachPaciente ) ) )
               {
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 91;
                  }
                  sendrow_912( ) ;
                  if ( isFullAjaxMode( ) && ! bGXsfl_91_Refreshing )
                  {
                     context.DoAjaxLoad(91, GridcoachpacienteRow);
                  }
               }
            }
            else
            {
               AV25I_LoadCount_GridCoachPaciente = (short)(AV25I_LoadCount_GridCoachPaciente-1);
            }
            pr_default.readNext(1);
         }
         pr_default.close(1);
         if ( AV70GXLvl261 == 0 )
         {
            tblI_noresultsfoundtablename_gridcoachpaciente_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblI_noresultsfoundtablename_gridcoachpaciente_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblI_noresultsfoundtablename_gridcoachpaciente_Visible), 5, 0)), true);
            /* Execute user subroutine: 'U_WHENNONE(GRIDCOACHPACIENTE)' */
            S202 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEPAGINGCONTROLS(GRIDCOACHPACIENTE)' */
         S172 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S182( )
      {
         /* 'U_GRIDREFRESH(GRIDCOACHPACIENTE)' Routine */
      }

      protected void S212( )
      {
         /* 'U_AGREGARPACIENTE' Routine */
         context.PopUp(formatLink("pacienteagregar.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV13COACH)), new Object[] {"AV13COACH"});
         context.DoAjaxRefreshForm();
      }

      protected void E121G2( )
      {
         /* 'E_AgregarPaciente' Routine */
         /* Execute user subroutine: 'U_AGREGARPACIENTE' */
         S212 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S232( )
      {
         /* 'U_QUITAR' Routine */
         GXt_char1 = StringUtil.Trim( AV29CoachPaciente_Coach.ToString());
         GXt_char2 = StringUtil.Trim( AV30CoachPaciente_Paciente.ToString());
         new pquitarpaciente(context ).execute( ref  GXt_char1, ref  GXt_char2) ;
         context.DoAjaxRefreshForm();
      }

      protected void E191G2( )
      {
         /* 'E_Quitar' Routine */
         AV44ConfirmMessage = "�Est�s seguro?";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44ConfirmMessage", AV44ConfirmMessage);
         /* Execute user subroutine: 'U_CONFIRMATIONREQUIRED(QUITAR)' */
         S222 ();
         if (returnInSub) return;
         if ( AV45ConfirmationRequired )
         {
            AV46ConfirmationSubId = "'U_Quitar'";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46ConfirmationSubId", AV46ConfirmationSubId);
            AV23Reload_GridCoachPaciente = false;
            AV47GridKey_CoachPaciente_Coach = (Guid)(AV29CoachPaciente_Coach);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47GridKey_CoachPaciente_Coach", AV47GridKey_CoachPaciente_Coach.ToString());
            AV48GridKey_CoachPaciente_Paciente = (Guid)(AV30CoachPaciente_Paciente);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48GridKey_CoachPaciente_Paciente", AV48GridKey_CoachPaciente_Paciente.ToString());
            tblTableconditionalconfirm_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTableconditionalconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTableconditionalconfirm_Visible), 5, 0)), true);
         }
         else
         {
            /* Execute user subroutine: 'U_QUITAR' */
            S232 ();
            if (returnInSub) return;
         }
         /*  Sending Event outputs  */
      }

      protected void S242( )
      {
         /* 'U_VERRECORRIDOS' Routine */
         context.PopUp(formatLink("wprecorridos.aspx") + "?" + UrlEncode(StringUtil.RTrim(A13CoachPaciente_Paciente.ToString())), new Object[] {""});
      }

      protected void E201G2( )
      {
         /* 'E_VerRecorridos' Routine */
         /* Execute user subroutine: 'U_VERRECORRIDOS' */
         S242 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void E131G2( )
      {
         /* 'ConfirmYes' Routine */
         tblTableconditionalconfirm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTableconditionalconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTableconditionalconfirm_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(AV46ConfirmationSubId, "'U_Quitar'") == 0 )
         {
            /* Execute user subroutine: 'E_SETROWPOSITION(QUITAR)' */
            S252 ();
            if (returnInSub) return;
         }
         /*  Sending Event outputs  */
      }

      protected void S222( )
      {
         /* 'U_CONFIRMATIONREQUIRED(QUITAR)' Routine */
         AV45ConfirmationRequired = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45ConfirmationRequired", AV45ConfirmationRequired);
      }

      protected void S252( )
      {
         /* 'E_SETROWPOSITION(QUITAR)' Routine */
         /* Start For Each Line in Gridcoachpaciente */
         nRC_GXsfl_91 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_91"), ",", "."));
         nGXsfl_91_fel_idx = 0;
         while ( nGXsfl_91_fel_idx < nRC_GXsfl_91 )
         {
            nGXsfl_91_fel_idx = (short)(((subGridcoachpaciente_Islastpage==1)&&(nGXsfl_91_fel_idx+1>subGridcoachpaciente_Recordsperpage( )) ? 1 : nGXsfl_91_fel_idx+1));
            sGXsfl_91_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_912( ) ;
            AV42Quitar_Action = cgiGet( edtavQuitar_action_Internalname);
            AV43VerRecorridos_Action = cgiGet( edtavVerrecorridos_action_Internalname);
            if ( StringUtil.StrCmp(cgiGet( edtavCoachpaciente_coach_Internalname), "") == 0 )
            {
               AV29CoachPaciente_Coach = (Guid)(Guid.Empty);
            }
            else
            {
               try
               {
                  AV29CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCoachpaciente_coach_Internalname)));
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCOACHPACIENTE_COACH");
                  GX_FocusControl = edtavCoachpaciente_coach_Internalname;
                  wbErr = true;
               }
            }
            if ( StringUtil.StrCmp(cgiGet( edtavCoachpaciente_paciente_Internalname), "") == 0 )
            {
               AV30CoachPaciente_Paciente = (Guid)(Guid.Empty);
            }
            else
            {
               try
               {
                  AV30CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCoachpaciente_paciente_Internalname)));
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCOACHPACIENTE_PACIENTE");
                  GX_FocusControl = edtavCoachpaciente_paciente_Internalname;
                  wbErr = true;
               }
            }
            AV31CoachPaciente_PacientePNombre = cgiGet( edtavCoachpaciente_pacientepnombre_Internalname);
            AV32CoachPaciente_PacientePApellido = cgiGet( edtavCoachpaciente_pacientepapellido_Internalname);
            if ( context.localUtil.VCDateTime( cgiGet( edtavCoachpaciente_fechaasignacion_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Coach Paciente_Fecha Asignacion"}), 1, "vCOACHPACIENTE_FECHAASIGNACION");
               GX_FocusControl = edtavCoachpaciente_fechaasignacion_Internalname;
               wbErr = true;
               AV33CoachPaciente_FechaAsignacion = (DateTime)(DateTime.MinValue);
            }
            else
            {
               AV33CoachPaciente_FechaAsignacion = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavCoachpaciente_fechaasignacion_Internalname), 0));
            }
            AV34CoachPaciente_DispositivoID = cgiGet( edtavCoachpaciente_dispositivoid_Internalname);
            AV35CoachPaciente_Token = cgiGet( edtavCoachpaciente_token_Internalname);
            if ( ( AV29CoachPaciente_Coach == AV47GridKey_CoachPaciente_Coach ) && ( AV30CoachPaciente_Paciente == AV48GridKey_CoachPaciente_Paciente ) )
            {
               /* Execute user subroutine: 'U_QUITAR' */
               S232 ();
               if (returnInSub) return;
            }
            /* End For Each Line */
         }
         if ( nGXsfl_91_fel_idx == 0 )
         {
            nGXsfl_91_idx = 1;
            sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
            SubsflControlProps_912( ) ;
         }
         nGXsfl_91_fel_idx = 1;
      }

      protected void S305( )
      {
         /* 'U_WHENNONE(GRIDEMERGENCIABITACORA)' Routine */
      }

      protected void S152( )
      {
         /* 'LOADGRIDSTATE(GRIDEMERGENCIABITACORA)' Routine */
         AV19GridStateKey = "GridEmergenciaBitacora";
         new k2bloadgridstate(context ).execute(  AV69Pgmname,  AV19GridStateKey, out  AV20GridState) ;
         if ( AV20GridState.gxTpr_Currentpage > 0 )
         {
            AV49CurrentPage_GridEmergenciaBitacora = AV20GridState.gxTpr_Currentpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49CurrentPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49CurrentPage_GridEmergenciaBitacora), 4, 0)));
         }
         if ( AV20GridState.gxTpr_Rowsperpage > 0 )
         {
            AV54RowsPerPage_GridEmergenciaBitacora = AV20GridState.gxTpr_Rowsperpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54RowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54RowsPerPage_GridEmergenciaBitacora), 4, 0)));
            AV55GridSettingsRowsPerPage_GridEmergenciaBitacora = AV20GridState.gxTpr_Rowsperpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV55GridSettingsRowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0)));
         }
      }

      protected void S262( )
      {
         /* 'SAVEGRIDSTATE(GRIDEMERGENCIABITACORA)' Routine */
         AV19GridStateKey = "GridEmergenciaBitacora";
         new k2bloadgridstate(context ).execute(  AV69Pgmname,  AV19GridStateKey, out  AV20GridState) ;
         AV20GridState.gxTpr_Currentpage = AV49CurrentPage_GridEmergenciaBitacora;
         AV20GridState.gxTpr_Rowsperpage = AV54RowsPerPage_GridEmergenciaBitacora;
         AV20GridState.gxTpr_Filtervalues.Clear();
         new k2bsavegridstate(context ).execute(  AV69Pgmname,  AV19GridStateKey,  AV20GridState) ;
      }

      protected void E141G2( )
      {
         /* 'SaveGridSettings(GridEmergenciaBitacora)' Routine */
         if ( AV54RowsPerPage_GridEmergenciaBitacora != AV55GridSettingsRowsPerPage_GridEmergenciaBitacora )
         {
            AV54RowsPerPage_GridEmergenciaBitacora = AV55GridSettingsRowsPerPage_GridEmergenciaBitacora;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV54RowsPerPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54RowsPerPage_GridEmergenciaBitacora), 4, 0)));
            AV49CurrentPage_GridEmergenciaBitacora = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49CurrentPage_GridEmergenciaBitacora", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49CurrentPage_GridEmergenciaBitacora), 4, 0)));
         }
         gxgrGridemergenciabitacora_refresh( AV22CurrentPage_GridCoachPaciente, A1PersonaID, A2PersonaPNombre, A4PersonaPApellido, A8PersonaTipo, A11PersonaEMail, AV69Pgmname, AV49CurrentPage_GridEmergenciaBitacora, AV54RowsPerPage_GridEmergenciaBitacora, AV53HasNextPage_GridEmergenciaBitacora, A43EmergenciaID, A44EmergenciaFecha, A45EmergenciaEstado, A41EmergenciaPaciente, A49EmergenciaPacienteNombre, A42EmergenciaCoach, A46EmergenciaLongitud, A47EmergenciaLatitud, A48EmergenciaMensaje, AV60EmergenciaCoach, AV13COACH) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE(GRIDEMERGENCIABITACORA)' */
         S262 ();
         if (returnInSub) return;
         divGridsettings_contentoutertablegridemergenciabitacora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divGridsettings_contentoutertablegridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divGridsettings_contentoutertablegridemergenciabitacora_Visible), 5, 0)), true);
         /*  Sending Event outputs  */
         cmbavPersonatipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV17PersonaTipo), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavPersonatipo_Internalname, "Values", cmbavPersonatipo.ToJavascriptSource(), true);
      }

      protected void E211G2( )
      {
         /* Gridemergenciabitacora_Refresh Routine */
         /* Execute user subroutine: 'SAVEGRIDSTATE(GRIDEMERGENCIABITACORA)' */
         S262 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEPAGINGCONTROLS(GRIDEMERGENCIABITACORA)' */
         S272 ();
         if (returnInSub) return;
         subGridemergenciabitacora_Backcolorstyle = 3;
         /* Execute user subroutine: 'U_GRIDREFRESH(GRIDEMERGENCIABITACORA)' */
         S282 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S296( )
      {
         /* 'U_LOADROWVARS(GRIDEMERGENCIABITACORA)' Routine */
         if ( AV60EmergenciaCoach == StringUtil.StrToGuid( AV13COACH) )
         {
            AV51LoadRow_GridEmergenciaBitacora = true;
         }
         else
         {
            AV51LoadRow_GridEmergenciaBitacora = false;
         }
      }

      protected void S272( )
      {
         /* 'UPDATEPAGINGCONTROLS(GRIDEMERGENCIABITACORA)' Routine */
         lblPaginationbar_firstpagetextblockgridemergenciabitacora_Caption = "1";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridemergenciabitacora_Internalname, "Caption", lblPaginationbar_firstpagetextblockgridemergenciabitacora_Caption, true);
         lblPaginationbar_previouspagetextblockgridemergenciabitacora_Caption = StringUtil.Str( (decimal)(AV49CurrentPage_GridEmergenciaBitacora-1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgridemergenciabitacora_Internalname, "Caption", lblPaginationbar_previouspagetextblockgridemergenciabitacora_Caption, true);
         lblPaginationbar_currentpagetextblockgridemergenciabitacora_Caption = StringUtil.Str( (decimal)(AV49CurrentPage_GridEmergenciaBitacora), 4, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_currentpagetextblockgridemergenciabitacora_Internalname, "Caption", lblPaginationbar_currentpagetextblockgridemergenciabitacora_Caption, true);
         lblPaginationbar_nextpagetextblockgridemergenciabitacora_Caption = StringUtil.Str( (decimal)(AV49CurrentPage_GridEmergenciaBitacora+1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgridemergenciabitacora_Internalname, "Caption", lblPaginationbar_nextpagetextblockgridemergenciabitacora_Caption, true);
         lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Internalname, "Class", lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class, true);
         lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Internalname, "Class", lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class, true);
         if ( (0==AV49CurrentPage_GridEmergenciaBitacora) || ( AV49CurrentPage_GridEmergenciaBitacora <= 1 ) )
         {
            lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Internalname, "Class", lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class, true);
            cellPaginationbar_firstpagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_firstpagecellgridemergenciabitacora_Class, true);
            lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible), 5, 0)), true);
            cellPaginationbar_spacingleftcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_spacingleftcellgridemergenciabitacora_Class, true);
            lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible), 5, 0)), true);
            cellPaginationbar_previouspagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_previouspagecellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_previouspagecellgridemergenciabitacora_Class, true);
            lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible), 5, 0)), true);
         }
         else
         {
            cellPaginationbar_previouspagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationPrevious";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_previouspagecellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_previouspagecellgridemergenciabitacora_Class, true);
            lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible), 5, 0)), true);
            if ( AV49CurrentPage_GridEmergenciaBitacora == 2 )
            {
               cellPaginationbar_firstpagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_firstpagecellgridemergenciabitacora_Class, true);
               lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible), 5, 0)), true);
               cellPaginationbar_spacingleftcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_spacingleftcellgridemergenciabitacora_Class, true);
               lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible), 5, 0)), true);
            }
            else
            {
               cellPaginationbar_firstpagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationLeft";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_firstpagecellgridemergenciabitacora_Class, true);
               lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible), 5, 0)), true);
               if ( AV49CurrentPage_GridEmergenciaBitacora == 3 )
               {
                  cellPaginationbar_spacingleftcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_spacingleftcellgridemergenciabitacora_Class, true);
                  lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible), 5, 0)), true);
               }
               else
               {
                  cellPaginationbar_spacingleftcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_spacingleftcellgridemergenciabitacora_Class, true);
                  lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible), 5, 0)), true);
               }
            }
         }
         if ( ! AV53HasNextPage_GridEmergenciaBitacora )
         {
            lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Internalname, "Class", lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class, true);
            cellPaginationbar_spacingrightcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingrightcellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_spacingrightcellgridemergenciabitacora_Class, true);
            lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible), 5, 0)), true);
            cellPaginationbar_nextpagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_nextpagecellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_nextpagecellgridemergenciabitacora_Class, true);
            lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible), 5, 0)), true);
         }
         else
         {
            cellPaginationbar_nextpagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationNext";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_nextpagecellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_nextpagecellgridemergenciabitacora_Class, true);
            lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible), 5, 0)), true);
            cellPaginationbar_spacingrightcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingrightcellgridemergenciabitacora_Internalname, "Class", cellPaginationbar_spacingrightcellgridemergenciabitacora_Class, true);
            lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible), 5, 0)), true);
         }
         if ( ( AV49CurrentPage_GridEmergenciaBitacora <= 1 ) && ! AV53HasNextPage_GridEmergenciaBitacora )
         {
            tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblPaginationbar_pagingcontainertablegridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible), 5, 0)), true);
         }
         else
         {
            tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblPaginationbar_pagingcontainertablegridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible), 5, 0)), true);
         }
      }

      protected void S282( )
      {
         /* 'U_GRIDREFRESH(GRIDEMERGENCIABITACORA)' Routine */
      }

      private void E221G5( )
      {
         /* Gridemergenciabitacora_Load Routine */
         tblI_noresultsfoundtablename_gridemergenciabitacora_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblI_noresultsfoundtablename_gridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblI_noresultsfoundtablename_gridemergenciabitacora_Visible), 5, 0)), true);
         AV52I_LoadCount_GridEmergenciaBitacora = 0;
         AV53HasNextPage_GridEmergenciaBitacora = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53HasNextPage_GridEmergenciaBitacora", AV53HasNextPage_GridEmergenciaBitacora);
         AV72GXLvl581 = 0;
         /* Using cursor H001G4 */
         pr_default.execute(2);
         while ( (pr_default.getStatus(2) != 101) )
         {
            A43EmergenciaID = (Guid)((Guid)(H001G4_A43EmergenciaID[0]));
            A44EmergenciaFecha = H001G4_A44EmergenciaFecha[0];
            A45EmergenciaEstado = H001G4_A45EmergenciaEstado[0];
            A41EmergenciaPaciente = (Guid)((Guid)(H001G4_A41EmergenciaPaciente[0]));
            n41EmergenciaPaciente = H001G4_n41EmergenciaPaciente[0];
            A49EmergenciaPacienteNombre = H001G4_A49EmergenciaPacienteNombre[0];
            n49EmergenciaPacienteNombre = H001G4_n49EmergenciaPacienteNombre[0];
            A42EmergenciaCoach = (Guid)((Guid)(H001G4_A42EmergenciaCoach[0]));
            n42EmergenciaCoach = H001G4_n42EmergenciaCoach[0];
            A46EmergenciaLongitud = H001G4_A46EmergenciaLongitud[0];
            n46EmergenciaLongitud = H001G4_n46EmergenciaLongitud[0];
            A47EmergenciaLatitud = H001G4_A47EmergenciaLatitud[0];
            n47EmergenciaLatitud = H001G4_n47EmergenciaLatitud[0];
            A48EmergenciaMensaje = H001G4_A48EmergenciaMensaje[0];
            n48EmergenciaMensaje = H001G4_n48EmergenciaMensaje[0];
            A49EmergenciaPacienteNombre = H001G4_A49EmergenciaPacienteNombre[0];
            n49EmergenciaPacienteNombre = H001G4_n49EmergenciaPacienteNombre[0];
            AV72GXLvl581 = 1;
            AV56EmergenciaID = (Guid)(A43EmergenciaID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciaid_Internalname, AV56EmergenciaID.ToString());
            AV57EmergenciaFecha = A44EmergenciaFecha;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciafecha_Internalname, context.localUtil.TToC( AV57EmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
            AV58EmergenciaEstado = A45EmergenciaEstado;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmergenciaestado_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0)));
            AV59EmergenciaPaciente = (Guid)(A41EmergenciaPaciente);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciapaciente_Internalname, AV59EmergenciaPaciente.ToString());
            AV65EmergenciaPacienteNombre = A49EmergenciaPacienteNombre;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciapacientenombre_Internalname, AV65EmergenciaPacienteNombre);
            AV60EmergenciaCoach = (Guid)(A42EmergenciaCoach);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciacoach_Internalname, AV60EmergenciaCoach.ToString());
            AV61EmergenciaLongitud = A46EmergenciaLongitud;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergencialongitud_Internalname, AV61EmergenciaLongitud);
            AV62EmergenciaLatitud = A47EmergenciaLatitud;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergencialatitud_Internalname, AV62EmergenciaLatitud);
            AV63EmergenciaMensaje = A48EmergenciaMensaje;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavEmergenciamensaje_Internalname, AV63EmergenciaMensaje);
            AV52I_LoadCount_GridEmergenciaBitacora = (short)(AV52I_LoadCount_GridEmergenciaBitacora+1);
            AV51LoadRow_GridEmergenciaBitacora = true;
            /* Execute user subroutine: 'U_LOADROWVARS(GRIDEMERGENCIABITACORA)' */
            S296 ();
            if ( returnInSub )
            {
               pr_default.close(2);
               returnInSub = true;
               if (true) return;
            }
            if ( AV51LoadRow_GridEmergenciaBitacora )
            {
               if ( AV52I_LoadCount_GridEmergenciaBitacora > AV54RowsPerPage_GridEmergenciaBitacora * AV49CurrentPage_GridEmergenciaBitacora )
               {
                  AV53HasNextPage_GridEmergenciaBitacora = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53HasNextPage_GridEmergenciaBitacora", AV53HasNextPage_GridEmergenciaBitacora);
                  /* Exit For each command. Update data (if necessary), close cursors & exit. */
                  if (true) break;
               }
               if ( ( AV52I_LoadCount_GridEmergenciaBitacora > ( AV54RowsPerPage_GridEmergenciaBitacora * ( AV49CurrentPage_GridEmergenciaBitacora - 1 ) ) ) && ( AV52I_LoadCount_GridEmergenciaBitacora <= ( AV54RowsPerPage_GridEmergenciaBitacora * AV49CurrentPage_GridEmergenciaBitacora ) ) )
               {
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 179;
                  }
                  sendrow_1795( ) ;
                  if ( isFullAjaxMode( ) && ! bGXsfl_179_Refreshing )
                  {
                     context.DoAjaxLoad(179, GridemergenciabitacoraRow);
                  }
               }
            }
            else
            {
               AV52I_LoadCount_GridEmergenciaBitacora = (short)(AV52I_LoadCount_GridEmergenciaBitacora-1);
            }
            pr_default.readNext(2);
         }
         pr_default.close(2);
         if ( AV72GXLvl581 == 0 )
         {
            tblI_noresultsfoundtablename_gridemergenciabitacora_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblI_noresultsfoundtablename_gridemergenciabitacora_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblI_noresultsfoundtablename_gridemergenciabitacora_Visible), 5, 0)), true);
            /* Execute user subroutine: 'U_WHENNONE(GRIDEMERGENCIABITACORA)' */
            S305 ();
            if (returnInSub) return;
         }
         /* Execute user subroutine: 'UPDATEPAGINGCONTROLS(GRIDEMERGENCIABITACORA)' */
         S272 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
         cmbavEmergenciaestado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0));
      }

      protected void wb_table8_217_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTableconditionalconfirm_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTableconditionalconfirm_Internalname, tblTableconditionalconfirm_Internalname, "", "Table_ConditionalConfirm", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_220_1G2( true) ;
         }
         else
         {
            wb_table9_220_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table9_220_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_217_1G2e( true) ;
         }
         else
         {
            wb_table8_217_1G2e( false) ;
         }
      }

      protected void wb_table9_220_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblSection_condconf_dialog_Internalname, tblSection_condconf_dialog_Internalname, "", "Section_CondConf_Dialog", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_223_1G2( true) ;
         }
         else
         {
            wb_table10_223_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table10_223_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_220_1G2e( true) ;
         }
         else
         {
            wb_table9_220_1G2e( false) ;
         }
      }

      protected void wb_table10_223_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblSection_condconf_dialog_inner_Internalname, tblSection_condconf_dialog_inner_Internalname, "", "Section_CondConf_DialogInner", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavConfirmmessage_Internalname, "Confirm Message", "col-sm-3 Attribute_ConditionalConfirmLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 227,'',false,'" + sGXsfl_91_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConfirmmessage_Internalname, StringUtil.RTrim( AV44ConfirmMessage), StringUtil.RTrim( context.localUtil.Format( AV44ConfirmMessage, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,227);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConfirmmessage_Jsonclick, 0, "Attribute_ConditionalConfirm", "", "", "", "", 1, edtavConfirmmessage_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_230_1G2( true) ;
         }
         else
         {
            wb_table11_230_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table11_230_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_223_1G2e( true) ;
         }
         else
         {
            wb_table10_223_1G2e( false) ;
         }
      }

      protected void wb_table11_230_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblConfirm_hidden_actionstable_Internalname, tblConfirm_hidden_actionstable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 233,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction_Confirm";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttI_buttonconfirmyes_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(91), 2, 0)+","+"null"+");", "Aceptar", bttI_buttonconfirmyes_Jsonclick, 5, "Aceptar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CONFIRMYES\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 235,'',false,'',0)\"";
            ClassString = "K2BToolsButton_GreyAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttI_buttonconfirmno_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(91), 2, 0)+","+"null"+");", "Cancelar", bttI_buttonconfirmno_Jsonclick, 7, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e231g1_client"+"'", TempTags, "", 2, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_230_1G2e( true) ;
         }
         else
         {
            wb_table11_230_1G2e( false) ;
         }
      }

      protected void wb_table7_197_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblPaginationbar_pagingcontainertablegridemergenciabitacora_Internalname, tblPaginationbar_pagingcontainertablegridemergenciabitacora_Internalname, "", "K2BToolsTable_PaginationContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_previouspagebuttoncellgridemergenciabitacora_Internalname+"\"  class='K2BToolsCell_PaginationFirst'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Internalname, "&laquo;", "", "", lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+"e241g1_client"+"'", "", lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class, 7, "", 1, 1, 1, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_firstpagecellgridemergenciabitacora_Internalname+"\"  class='"+cellPaginationbar_firstpagecellgridemergenciabitacora_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_firstpagetextblockgridemergenciabitacora_Internalname, lblPaginationbar_firstpagetextblockgridemergenciabitacora_Caption, "", "", lblPaginationbar_firstpagetextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+"e251g1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_spacingleftcellgridemergenciabitacora_Internalname+"\"  class='"+cellPaginationbar_spacingleftcellgridemergenciabitacora_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Internalname, "...", "", "", lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_previouspagecellgridemergenciabitacora_Internalname+"\"  class='"+cellPaginationbar_previouspagecellgridemergenciabitacora_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_previouspagetextblockgridemergenciabitacora_Internalname, lblPaginationbar_previouspagetextblockgridemergenciabitacora_Caption, "", "", lblPaginationbar_previouspagetextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+"e241g1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_currentpagecellgridemergenciabitacora_Internalname+"\"  class='K2BToolsCell_PaginationCurrentPage'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_currentpagetextblockgridemergenciabitacora_Internalname, lblPaginationbar_currentpagetextblockgridemergenciabitacora_Caption, "", "", lblPaginationbar_currentpagetextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationCurrent", 0, "", 1, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_nextpagecellgridemergenciabitacora_Internalname+"\"  class='"+cellPaginationbar_nextpagecellgridemergenciabitacora_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_nextpagetextblockgridemergenciabitacora_Internalname, lblPaginationbar_nextpagetextblockgridemergenciabitacora_Caption, "", "", lblPaginationbar_nextpagetextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+"e261g1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_spacingrightcellgridemergenciabitacora_Internalname+"\"  class='"+cellPaginationbar_spacingrightcellgridemergenciabitacora_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Internalname, "...", "", "", lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_nextpagebuttoncellgridemergenciabitacora_Internalname+"\"  class='K2BToolsCell_PaginationLast'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Internalname, "&raquo;", "", "", lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+"e261g1_client"+"'", "", lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class, 7, "", 1, 1, 1, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_197_1G2e( true) ;
         }
         else
         {
            wb_table7_197_1G2e( false) ;
         }
      }

      protected void wb_table6_191_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblI_noresultsfoundtablename_gridemergenciabitacora_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblI_noresultsfoundtablename_gridemergenciabitacora_Internalname, tblI_noresultsfoundtablename_gridemergenciabitacora_Internalname, "", "K2BToolsTable_NoResultsFound", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblI_noresultsfoundtextblock_gridemergenciabitacora_Internalname, "No hay resultados", "", "", lblI_noresultsfoundtextblock_gridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_NoResultsFound", 0, "", 1, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_191_1G2e( true) ;
         }
         else
         {
            wb_table6_191_1G2e( false) ;
         }
      }

      protected void wb_table5_152_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblLayoutdefined_table2_gridemergenciabitacora_Internalname, tblLayoutdefined_table2_gridemergenciabitacora_Internalname, "", "K2BToolsTable_FloatRight", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridsettings_globaltablegridemergenciabitacora_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_GridSettingsContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGridsettings_labelgridemergenciabitacora_Internalname, "Config. tabla &#x25BC", "", "", lblGridsettings_labelgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+"e271g1_client"+"'", "", "K2BToolsTextBlock_GridSettingsTitle", 7, "", 1, 1, 1, "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridsettings_contentoutertablegridemergenciabitacora_Internalname, divGridsettings_contentoutertablegridemergenciabitacora_Visible, 0, "px", 0, "px", "K2BToolsTable_GridSettings", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table12_164_1G2( true) ;
         }
         else
         {
            wb_table12_164_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table12_164_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 173,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttGridsettings_savegridemergenciabitacora_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(91), 2, 0)+","+"null"+");", "Guardar", bttGridsettings_savegridemergenciabitacora_Jsonclick, 5, "Guardar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SAVEGRIDSETTINGS(GRIDEMERGENCIABITACORA)\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_152_1G2e( true) ;
         }
         else
         {
            wb_table5_152_1G2e( false) ;
         }
      }

      protected void wb_table12_164_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblGridsettings_tablecontentgridemergenciabitacora_Internalname, tblGridsettings_tablecontentgridemergenciabitacora_Internalname, "", "K2BToolsTable_GridSettingsContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGridsettings_rowsperpagetextblockgridemergenciabitacora_Internalname, "Filas por p�gina", "", "", lblGridsettings_rowsperpagetextblockgridemergenciabitacora_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname, "Grid Settings Rows Per Page_Grid Emergencia Bitacora", "col-sm-3 K2BToolsEnhancedComboLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 170,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGridsettingsrowsperpage_gridemergenciabitacora, cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0)), 1, cmbavGridsettingsrowsperpage_gridemergenciabitacora_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "Filas por p�gina", 1, cmbavGridsettingsrowsperpage_gridemergenciabitacora.Enabled, 0, 0, 0, "em", 0, "", "", "K2BToolsEnhancedCombo", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,170);\"", "", true, "HLP_Principal.htm");
            cmbavGridsettingsrowsperpage_gridemergenciabitacora.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV55GridSettingsRowsPerPage_GridEmergenciaBitacora), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname, "Values", (String)(cmbavGridsettingsrowsperpage_gridemergenciabitacora.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_164_1G2e( true) ;
         }
         else
         {
            wb_table12_164_1G2e( false) ;
         }
      }

      protected void wb_table4_132_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblActionscontainertableleft_actions_Internalname, tblActionscontainertableleft_actions_Internalname, "", "K2BToolsTableActionsLeftContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 135,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttAgregarpaciente_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(91), 2, 0)+","+"null"+");", "Agregar", bttAgregarpaciente_Jsonclick, 5, "", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'E_AGREGARPACIENTE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_132_1G2e( true) ;
         }
         else
         {
            wb_table4_132_1G2e( false) ;
         }
      }

      protected void wb_table3_109_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblPaginationbar_pagingcontainertablegridcoachpaciente_Internalname, tblPaginationbar_pagingcontainertablegridcoachpaciente_Internalname, "", "K2BToolsTable_PaginationContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_previouspagebuttoncellgridcoachpaciente_Internalname+"\"  class='K2BToolsCell_PaginationFirst'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Internalname, "&laquo;", "", "", lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+"e281g1_client"+"'", "", lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class, 7, "", 1, 1, 1, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_firstpagecellgridcoachpaciente_Internalname+"\"  class='"+cellPaginationbar_firstpagecellgridcoachpaciente_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_firstpagetextblockgridcoachpaciente_Internalname, lblPaginationbar_firstpagetextblockgridcoachpaciente_Caption, "", "", lblPaginationbar_firstpagetextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+"e291g1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_spacingleftcellgridcoachpaciente_Internalname+"\"  class='"+cellPaginationbar_spacingleftcellgridcoachpaciente_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_spacinglefttextblockgridcoachpaciente_Internalname, "...", "", "", lblPaginationbar_spacinglefttextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_previouspagecellgridcoachpaciente_Internalname+"\"  class='"+cellPaginationbar_previouspagecellgridcoachpaciente_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_previouspagetextblockgridcoachpaciente_Internalname, lblPaginationbar_previouspagetextblockgridcoachpaciente_Caption, "", "", lblPaginationbar_previouspagetextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+"e281g1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_currentpagecellgridcoachpaciente_Internalname+"\"  class='K2BToolsCell_PaginationCurrentPage'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_currentpagetextblockgridcoachpaciente_Internalname, lblPaginationbar_currentpagetextblockgridcoachpaciente_Caption, "", "", lblPaginationbar_currentpagetextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationCurrent", 0, "", 1, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_nextpagecellgridcoachpaciente_Internalname+"\"  class='"+cellPaginationbar_nextpagecellgridcoachpaciente_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_nextpagetextblockgridcoachpaciente_Internalname, lblPaginationbar_nextpagetextblockgridcoachpaciente_Caption, "", "", lblPaginationbar_nextpagetextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+"e301g1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_spacingrightcellgridcoachpaciente_Internalname+"\"  class='"+cellPaginationbar_spacingrightcellgridcoachpaciente_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_spacingrighttextblockgridcoachpaciente_Internalname, "...", "", "", lblPaginationbar_spacingrighttextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_nextpagebuttoncellgridcoachpaciente_Internalname+"\"  class='K2BToolsCell_PaginationLast'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Internalname, "&raquo;", "", "", lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+"e301g1_client"+"'", "", lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class, 7, "", 1, 1, 1, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_109_1G2e( true) ;
         }
         else
         {
            wb_table3_109_1G2e( false) ;
         }
      }

      protected void wb_table2_103_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblI_noresultsfoundtablename_gridcoachpaciente_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblI_noresultsfoundtablename_gridcoachpaciente_Internalname, tblI_noresultsfoundtablename_gridcoachpaciente_Internalname, "", "K2BToolsTable_NoResultsFound", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblI_noresultsfoundtextblock_gridcoachpaciente_Internalname, "No hay resultados", "", "", lblI_noresultsfoundtextblock_gridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_NoResultsFound", 0, "", 1, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_103_1G2e( true) ;
         }
         else
         {
            wb_table2_103_1G2e( false) ;
         }
      }

      protected void wb_table1_64_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblLayoutdefined_table2_gridcoachpaciente_Internalname, tblLayoutdefined_table2_gridcoachpaciente_Internalname, "", "K2BToolsTable_FloatRight", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridsettings_globaltablegridcoachpaciente_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_GridSettingsContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGridsettings_labelgridcoachpaciente_Internalname, "Config. tabla &#x25BC", "", "", lblGridsettings_labelgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+"e311g1_client"+"'", "", "K2BToolsTextBlock_GridSettingsTitle", 7, "", 1, 1, 1, "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridsettings_contentoutertablegridcoachpaciente_Internalname, divGridsettings_contentoutertablegridcoachpaciente_Visible, 0, "px", 0, "px", "K2BToolsTable_GridSettings", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table13_76_1G2( true) ;
         }
         else
         {
            wb_table13_76_1G2( false) ;
         }
         return  ;
      }

      protected void wb_table13_76_1G2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttGridsettings_savegridcoachpaciente_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(91), 2, 0)+","+"null"+");", "Guardar", bttGridsettings_savegridcoachpaciente_Jsonclick, 5, "Guardar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SAVEGRIDSETTINGS(GRIDCOACHPACIENTE)\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Principal.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_64_1G2e( true) ;
         }
         else
         {
            wb_table1_64_1G2e( false) ;
         }
      }

      protected void wb_table13_76_1G2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblGridsettings_tablecontentgridcoachpaciente_Internalname, tblGridsettings_tablecontentgridcoachpaciente_Internalname, "", "K2BToolsTable_GridSettingsContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGridsettings_rowsperpagetextblockgridcoachpaciente_Internalname, "Filas por p�gina", "", "", lblGridsettings_rowsperpagetextblockgridcoachpaciente_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_Principal.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname, "Grid Settings Rows Per Page_Grid Coach Paciente", "col-sm-3 K2BToolsEnhancedComboLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'" + sGXsfl_91_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGridsettingsrowsperpage_gridcoachpaciente, cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0)), 1, cmbavGridsettingsrowsperpage_gridcoachpaciente_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "Filas por p�gina", 1, cmbavGridsettingsrowsperpage_gridcoachpaciente.Enabled, 0, 0, 0, "em", 0, "", "", "K2BToolsEnhancedCombo", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", "", true, "HLP_Principal.htm");
            cmbavGridsettingsrowsperpage_gridcoachpaciente.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV28GridSettingsRowsPerPage_GridCoachPaciente), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname, "Values", (String)(cmbavGridsettingsrowsperpage_gridcoachpaciente.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_76_1G2e( true) ;
         }
         else
         {
            wb_table13_76_1G2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1G2( ) ;
         WS1G2( ) ;
         WE1G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("K2BControlBeautify/montrezorro-bootstrap-checkbox/css/bootstrap-checkbox.css", "");
         AddStyleSheetFile("K2BControlBeautify/silviomoreto-bootstrap-select/dist/css/bootstrap-select.css", "");
         AddStyleSheetFile("K2BControlBeautify/toastr-master/toastr.min.css", "");
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2018111817352281", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("principal.js", "?2018111817352281", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManager.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/json2005.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/rsh.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManagerCreate.js", "", false);
         context.AddJavascriptSource("Tab/TabRender.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_912( )
      {
         edtavQuitar_action_Internalname = "vQUITAR_ACTION_"+sGXsfl_91_idx;
         edtavVerrecorridos_action_Internalname = "vVERRECORRIDOS_ACTION_"+sGXsfl_91_idx;
         edtavCoachpaciente_coach_Internalname = "vCOACHPACIENTE_COACH_"+sGXsfl_91_idx;
         edtavCoachpaciente_paciente_Internalname = "vCOACHPACIENTE_PACIENTE_"+sGXsfl_91_idx;
         edtavCoachpaciente_pacientepnombre_Internalname = "vCOACHPACIENTE_PACIENTEPNOMBRE_"+sGXsfl_91_idx;
         edtavCoachpaciente_pacientepapellido_Internalname = "vCOACHPACIENTE_PACIENTEPAPELLIDO_"+sGXsfl_91_idx;
         edtavCoachpaciente_fechaasignacion_Internalname = "vCOACHPACIENTE_FECHAASIGNACION_"+sGXsfl_91_idx;
         edtavCoachpaciente_dispositivoid_Internalname = "vCOACHPACIENTE_DISPOSITIVOID_"+sGXsfl_91_idx;
         edtavCoachpaciente_token_Internalname = "vCOACHPACIENTE_TOKEN_"+sGXsfl_91_idx;
      }

      protected void SubsflControlProps_fel_912( )
      {
         edtavQuitar_action_Internalname = "vQUITAR_ACTION_"+sGXsfl_91_fel_idx;
         edtavVerrecorridos_action_Internalname = "vVERRECORRIDOS_ACTION_"+sGXsfl_91_fel_idx;
         edtavCoachpaciente_coach_Internalname = "vCOACHPACIENTE_COACH_"+sGXsfl_91_fel_idx;
         edtavCoachpaciente_paciente_Internalname = "vCOACHPACIENTE_PACIENTE_"+sGXsfl_91_fel_idx;
         edtavCoachpaciente_pacientepnombre_Internalname = "vCOACHPACIENTE_PACIENTEPNOMBRE_"+sGXsfl_91_fel_idx;
         edtavCoachpaciente_pacientepapellido_Internalname = "vCOACHPACIENTE_PACIENTEPAPELLIDO_"+sGXsfl_91_fel_idx;
         edtavCoachpaciente_fechaasignacion_Internalname = "vCOACHPACIENTE_FECHAASIGNACION_"+sGXsfl_91_fel_idx;
         edtavCoachpaciente_dispositivoid_Internalname = "vCOACHPACIENTE_DISPOSITIVOID_"+sGXsfl_91_fel_idx;
         edtavCoachpaciente_token_Internalname = "vCOACHPACIENTE_TOKEN_"+sGXsfl_91_fel_idx;
      }

      protected void sendrow_912( )
      {
         SubsflControlProps_912( ) ;
         WB1G0( ) ;
         GridcoachpacienteRow = GXWebRow.GetNew(context,GridcoachpacienteContainer);
         if ( subGridcoachpaciente_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridcoachpaciente_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridcoachpaciente_Class, "") != 0 )
            {
               subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"Odd";
            }
         }
         else if ( subGridcoachpaciente_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridcoachpaciente_Backstyle = 0;
            subGridcoachpaciente_Backcolor = subGridcoachpaciente_Allbackcolor;
            if ( StringUtil.StrCmp(subGridcoachpaciente_Class, "") != 0 )
            {
               subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"Uniform";
            }
         }
         else if ( subGridcoachpaciente_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridcoachpaciente_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridcoachpaciente_Class, "") != 0 )
            {
               subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"Odd";
            }
            subGridcoachpaciente_Backcolor = (int)(0x0);
         }
         else if ( subGridcoachpaciente_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridcoachpaciente_Backstyle = 1;
            if ( ((int)((nGXsfl_91_idx) % (2))) == 0 )
            {
               subGridcoachpaciente_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridcoachpaciente_Class, "") != 0 )
               {
                  subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"Even";
               }
            }
            else
            {
               subGridcoachpaciente_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridcoachpaciente_Class, "") != 0 )
               {
                  subGridcoachpaciente_Linesclass = subGridcoachpaciente_Class+"Odd";
               }
            }
         }
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+"Grid_WorkWith"+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_91_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavQuitar_action_Enabled!=0)&&(edtavQuitar_action_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 92,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavQuitar_action_Internalname,StringUtil.RTrim( AV42Quitar_Action),(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavQuitar_action_Enabled!=0)&&(edtavQuitar_action_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,92);\"" : " "),"'"+""+"'"+",false,"+"'"+"E\\'E_QUITAR\\'."+sGXsfl_91_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavQuitar_action_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn",(String)"",(short)-1,(int)edtavQuitar_action_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavVerrecorridos_action_Enabled!=0)&&(edtavVerrecorridos_action_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 93,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavVerrecorridos_action_Internalname,StringUtil.RTrim( AV43VerRecorridos_Action),(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavVerrecorridos_action_Enabled!=0)&&(edtavVerrecorridos_action_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,93);\"" : " "),"'"+""+"'"+",false,"+"'"+"E\\'E_VERRECORRIDOS\\'."+sGXsfl_91_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavVerrecorridos_action_Jsonclick,(short)5,(String)"Attribute",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn",(String)"",(short)-1,(int)edtavVerrecorridos_action_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)20,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCoachpaciente_coach_Enabled!=0)&&(edtavCoachpaciente_coach_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 94,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute_Grid";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCoachpaciente_coach_Internalname,AV29CoachPaciente_Coach.ToString(),AV29CoachPaciente_Coach.ToString(),TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavCoachpaciente_coach_Enabled!=0)&&(edtavCoachpaciente_coach_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,94);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCoachpaciente_coach_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavCoachpaciente_coach_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)91,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCoachpaciente_paciente_Enabled!=0)&&(edtavCoachpaciente_paciente_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 95,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute_Grid";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCoachpaciente_paciente_Internalname,AV30CoachPaciente_Paciente.ToString(),AV30CoachPaciente_Paciente.ToString(),TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavCoachpaciente_paciente_Enabled!=0)&&(edtavCoachpaciente_paciente_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,95);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCoachpaciente_paciente_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavCoachpaciente_paciente_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)91,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCoachpaciente_pacientepnombre_Enabled!=0)&&(edtavCoachpaciente_pacientepnombre_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 96,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute_Grid";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCoachpaciente_pacientepnombre_Internalname,(String)AV31CoachPaciente_PacientePNombre,(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavCoachpaciente_pacientepnombre_Enabled!=0)&&(edtavCoachpaciente_pacientepnombre_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,96);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCoachpaciente_pacientepnombre_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavCoachpaciente_pacientepnombre_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCoachpaciente_pacientepapellido_Enabled!=0)&&(edtavCoachpaciente_pacientepapellido_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 97,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute_Grid";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCoachpaciente_pacientepapellido_Internalname,(String)AV32CoachPaciente_PacientePApellido,(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavCoachpaciente_pacientepapellido_Enabled!=0)&&(edtavCoachpaciente_pacientepapellido_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,97);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCoachpaciente_pacientepapellido_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavCoachpaciente_pacientepapellido_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCoachpaciente_fechaasignacion_Enabled!=0)&&(edtavCoachpaciente_fechaasignacion_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 98,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute_Grid";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCoachpaciente_fechaasignacion_Internalname,context.localUtil.Format(AV33CoachPaciente_FechaAsignacion, "99/99/99"),context.localUtil.Format( AV33CoachPaciente_FechaAsignacion, "99/99/99"),TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavCoachpaciente_fechaasignacion_Enabled!=0)&&(edtavCoachpaciente_fechaasignacion_Visible!=0) ? " onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,98);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCoachpaciente_fechaasignacion_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavCoachpaciente_fechaasignacion_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCoachpaciente_dispositivoid_Enabled!=0)&&(edtavCoachpaciente_dispositivoid_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 99,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute_Grid";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCoachpaciente_dispositivoid_Internalname,(String)AV34CoachPaciente_DispositivoID,(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavCoachpaciente_dispositivoid_Enabled!=0)&&(edtavCoachpaciente_dispositivoid_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,99);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCoachpaciente_dispositivoid_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)0,(int)edtavCoachpaciente_dispositivoid_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)75,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridcoachpacienteContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavCoachpaciente_token_Enabled!=0)&&(edtavCoachpaciente_token_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 100,'',false,'"+sGXsfl_91_idx+"',91)\"" : " ");
         ROClassString = "Attribute_Grid";
         GridcoachpacienteRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavCoachpaciente_token_Internalname,(String)AV35CoachPaciente_Token,(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavCoachpaciente_token_Enabled!=0)&&(edtavCoachpaciente_token_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,100);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavCoachpaciente_token_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)0,(int)edtavCoachpaciente_token_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)91,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         send_integrity_lvl_hashes1G2( ) ;
         GridcoachpacienteContainer.AddRow(GridcoachpacienteRow);
         nGXsfl_91_idx = (short)(((subGridcoachpaciente_Islastpage==1)&&(nGXsfl_91_idx+1>subGridcoachpaciente_Recordsperpage( )) ? 1 : nGXsfl_91_idx+1));
         sGXsfl_91_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_91_idx), 4, 0)), 4, "0");
         SubsflControlProps_912( ) ;
         /* End function sendrow_912 */
      }

      protected void SubsflControlProps_1795( )
      {
         edtavEmergenciaid_Internalname = "vEMERGENCIAID_"+sGXsfl_179_idx;
         edtavEmergenciafecha_Internalname = "vEMERGENCIAFECHA_"+sGXsfl_179_idx;
         cmbavEmergenciaestado_Internalname = "vEMERGENCIAESTADO_"+sGXsfl_179_idx;
         edtavEmergenciapaciente_Internalname = "vEMERGENCIAPACIENTE_"+sGXsfl_179_idx;
         edtavEmergenciapacientenombre_Internalname = "vEMERGENCIAPACIENTENOMBRE_"+sGXsfl_179_idx;
         edtavEmergenciacoach_Internalname = "vEMERGENCIACOACH_"+sGXsfl_179_idx;
         edtavEmergencialongitud_Internalname = "vEMERGENCIALONGITUD_"+sGXsfl_179_idx;
         edtavEmergencialatitud_Internalname = "vEMERGENCIALATITUD_"+sGXsfl_179_idx;
         edtavEmergenciamensaje_Internalname = "vEMERGENCIAMENSAJE_"+sGXsfl_179_idx;
      }

      protected void SubsflControlProps_fel_1795( )
      {
         edtavEmergenciaid_Internalname = "vEMERGENCIAID_"+sGXsfl_179_fel_idx;
         edtavEmergenciafecha_Internalname = "vEMERGENCIAFECHA_"+sGXsfl_179_fel_idx;
         cmbavEmergenciaestado_Internalname = "vEMERGENCIAESTADO_"+sGXsfl_179_fel_idx;
         edtavEmergenciapaciente_Internalname = "vEMERGENCIAPACIENTE_"+sGXsfl_179_fel_idx;
         edtavEmergenciapacientenombre_Internalname = "vEMERGENCIAPACIENTENOMBRE_"+sGXsfl_179_fel_idx;
         edtavEmergenciacoach_Internalname = "vEMERGENCIACOACH_"+sGXsfl_179_fel_idx;
         edtavEmergencialongitud_Internalname = "vEMERGENCIALONGITUD_"+sGXsfl_179_fel_idx;
         edtavEmergencialatitud_Internalname = "vEMERGENCIALATITUD_"+sGXsfl_179_fel_idx;
         edtavEmergenciamensaje_Internalname = "vEMERGENCIAMENSAJE_"+sGXsfl_179_fel_idx;
      }

      protected void sendrow_1795( )
      {
         SubsflControlProps_1795( ) ;
         WB1G0( ) ;
         GridemergenciabitacoraRow = GXWebRow.GetNew(context,GridemergenciabitacoraContainer);
         if ( subGridemergenciabitacora_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridemergenciabitacora_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridemergenciabitacora_Class, "") != 0 )
            {
               subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"Odd";
            }
         }
         else if ( subGridemergenciabitacora_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridemergenciabitacora_Backstyle = 0;
            subGridemergenciabitacora_Backcolor = subGridemergenciabitacora_Allbackcolor;
            if ( StringUtil.StrCmp(subGridemergenciabitacora_Class, "") != 0 )
            {
               subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"Uniform";
            }
         }
         else if ( subGridemergenciabitacora_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridemergenciabitacora_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridemergenciabitacora_Class, "") != 0 )
            {
               subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"Odd";
            }
            subGridemergenciabitacora_Backcolor = (int)(0x0);
         }
         else if ( subGridemergenciabitacora_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridemergenciabitacora_Backstyle = 1;
            if ( ((int)((nGXsfl_179_idx) % (2))) == 0 )
            {
               subGridemergenciabitacora_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridemergenciabitacora_Class, "") != 0 )
               {
                  subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"Even";
               }
            }
            else
            {
               subGridemergenciabitacora_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridemergenciabitacora_Class, "") != 0 )
               {
                  subGridemergenciabitacora_Linesclass = subGridemergenciabitacora_Class+"Odd";
               }
            }
         }
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+"Grid_WorkWith"+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_179_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergenciaid_Internalname,AV56EmergenciaID.ToString(),AV56EmergenciaID.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergenciaid_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)0,(int)edtavEmergenciaid_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)179,(short)1,(short)0,(short)0,(bool)false,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergenciafecha_Internalname,context.localUtil.TToC( AV57EmergenciaFecha, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( AV57EmergenciaFecha, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergenciafecha_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn",(String)"",(short)-1,(int)edtavEmergenciafecha_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)179,(short)1,(short)-1,(short)0,(bool)false,(String)"",(String)"right",(bool)false});
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         if ( ( cmbavEmergenciaestado.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "vEMERGENCIAESTADO_" + sGXsfl_179_idx;
            cmbavEmergenciaestado.Name = GXCCtl;
            cmbavEmergenciaestado.WebTags = "";
            cmbavEmergenciaestado.addItem("1", "Activa", 0);
            cmbavEmergenciaestado.addItem("2", "Finalizada", 0);
            if ( cmbavEmergenciaestado.ItemCount > 0 )
            {
               AV58EmergenciaEstado = (short)(NumberUtil.Val( cmbavEmergenciaestado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, cmbavEmergenciaestado_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0)));
            }
         }
         /* ComboBox */
         GridemergenciabitacoraRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbavEmergenciaestado,(String)cmbavEmergenciaestado_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0)),(short)1,(String)cmbavEmergenciaestado_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbavEmergenciaestado.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute_Grid",(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(String)"",(String)"",(bool)false});
         cmbavEmergenciaestado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV58EmergenciaEstado), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEmergenciaestado_Internalname, "Values", (String)(cmbavEmergenciaestado.ToJavascriptSource()), !bGXsfl_179_Refreshing);
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergenciapaciente_Internalname,AV59EmergenciaPaciente.ToString(),AV59EmergenciaPaciente.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergenciapaciente_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)0,(int)edtavEmergenciapaciente_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)179,(short)1,(short)0,(short)0,(bool)false,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergenciapacientenombre_Internalname,(String)AV65EmergenciaPacienteNombre,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergenciapacientenombre_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavEmergenciapacientenombre_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)179,(short)1,(short)-1,(short)-1,(bool)false,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergenciacoach_Internalname,AV60EmergenciaCoach.ToString(),AV60EmergenciaCoach.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergenciacoach_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)0,(int)edtavEmergenciacoach_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)179,(short)1,(short)0,(short)0,(bool)false,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergencialongitud_Internalname,(String)AV61EmergenciaLongitud,(String)AV61EmergenciaLongitud,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergencialongitud_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavEmergencialongitud_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)179,(short)1,(short)0,(short)-1,(bool)false,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergencialatitud_Internalname,(String)AV62EmergenciaLatitud,(String)AV62EmergenciaLatitud,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergencialatitud_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavEmergencialatitud_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)179,(short)1,(short)0,(short)-1,(bool)false,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         if ( GridemergenciabitacoraContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridemergenciabitacoraRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavEmergenciamensaje_Internalname,(String)AV63EmergenciaMensaje,(String)AV63EmergenciaMensaje,(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavEmergenciamensaje_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavEmergenciamensaje_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)179,(short)1,(short)0,(short)-1,(bool)false,(String)"",(String)"left",(bool)false});
         send_integrity_lvl_hashes1G5( ) ;
         GridemergenciabitacoraContainer.AddRow(GridemergenciabitacoraRow);
         nGXsfl_179_idx = (short)(((subGridemergenciabitacora_Islastpage==1)&&(nGXsfl_179_idx+1>subGridemergenciabitacora_Recordsperpage( )) ? 1 : nGXsfl_179_idx+1));
         sGXsfl_179_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_179_idx), 4, 0)), 4, "0");
         SubsflControlProps_1795( ) ;
         /* End function sendrow_1795 */
      }

      protected void init_default_properties( )
      {
         lblTab_tabcontrol_title_Internalname = "TAB_TABCONTROL_TITLE";
         edtavPersonaid_Internalname = "vPERSONAID";
         edtavPersonapnombre_Internalname = "vPERSONAPNOMBRE";
         edtavPersonapapellido_Internalname = "vPERSONAPAPELLIDO";
         cmbavPersonatipo_Internalname = "vPERSONATIPO";
         edtavPersonaemail_Internalname = "vPERSONAEMAIL";
         divAttributescontainertable_attributes_Internalname = "ATTRIBUTESCONTAINERTABLE_ATTRIBUTES";
         divResponsivetable_mainattributes_attributes_Internalname = "RESPONSIVETABLE_MAINATTRIBUTES_ATTRIBUTES";
         divMaintabresponsivetable_tab_Internalname = "MAINTABRESPONSIVETABLE_TAB";
         lblTab1_tabcontrol_title_Internalname = "TAB1_TABCONTROL_TITLE";
         lblGridsettings_labelgridcoachpaciente_Internalname = "GRIDSETTINGS_LABELGRIDCOACHPACIENTE";
         lblGridsettings_rowsperpagetextblockgridcoachpaciente_Internalname = "GRIDSETTINGS_ROWSPERPAGETEXTBLOCKGRIDCOACHPACIENTE";
         cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname = "vGRIDSETTINGSROWSPERPAGE_GRIDCOACHPACIENTE";
         tblGridsettings_tablecontentgridcoachpaciente_Internalname = "GRIDSETTINGS_TABLECONTENTGRIDCOACHPACIENTE";
         bttGridsettings_savegridcoachpaciente_Internalname = "GRIDSETTINGS_SAVEGRIDCOACHPACIENTE";
         divGridsettings_contentoutertablegridcoachpaciente_Internalname = "GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE";
         divGridsettings_globaltablegridcoachpaciente_Internalname = "GRIDSETTINGS_GLOBALTABLEGRIDCOACHPACIENTE";
         tblLayoutdefined_table2_gridcoachpaciente_Internalname = "LAYOUTDEFINED_TABLE2_GRIDCOACHPACIENTE";
         divLayoutdefined_tableactionscontainer_gridcoachpaciente_Internalname = "LAYOUTDEFINED_TABLEACTIONSCONTAINER_GRIDCOACHPACIENTE";
         edtavQuitar_action_Internalname = "vQUITAR_ACTION";
         edtavVerrecorridos_action_Internalname = "vVERRECORRIDOS_ACTION";
         edtavCoachpaciente_coach_Internalname = "vCOACHPACIENTE_COACH";
         edtavCoachpaciente_paciente_Internalname = "vCOACHPACIENTE_PACIENTE";
         edtavCoachpaciente_pacientepnombre_Internalname = "vCOACHPACIENTE_PACIENTEPNOMBRE";
         edtavCoachpaciente_pacientepapellido_Internalname = "vCOACHPACIENTE_PACIENTEPAPELLIDO";
         edtavCoachpaciente_fechaasignacion_Internalname = "vCOACHPACIENTE_FECHAASIGNACION";
         edtavCoachpaciente_dispositivoid_Internalname = "vCOACHPACIENTE_DISPOSITIVOID";
         edtavCoachpaciente_token_Internalname = "vCOACHPACIENTE_TOKEN";
         lblI_noresultsfoundtextblock_gridcoachpaciente_Internalname = "I_NORESULTSFOUNDTEXTBLOCK_GRIDCOACHPACIENTE";
         tblI_noresultsfoundtablename_gridcoachpaciente_Internalname = "I_NORESULTSFOUNDTABLENAME_GRIDCOACHPACIENTE";
         divMaingrid_responsivetable_gridcoachpaciente_Internalname = "MAINGRID_RESPONSIVETABLE_GRIDCOACHPACIENTE";
         lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_previouspagebuttoncellgridcoachpaciente_Internalname = "PAGINATIONBAR_PREVIOUSPAGEBUTTONCELLGRIDCOACHPACIENTE";
         lblPaginationbar_firstpagetextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_firstpagecellgridcoachpaciente_Internalname = "PAGINATIONBAR_FIRSTPAGECELLGRIDCOACHPACIENTE";
         lblPaginationbar_spacinglefttextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_spacingleftcellgridcoachpaciente_Internalname = "PAGINATIONBAR_SPACINGLEFTCELLGRIDCOACHPACIENTE";
         lblPaginationbar_previouspagetextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_previouspagecellgridcoachpaciente_Internalname = "PAGINATIONBAR_PREVIOUSPAGECELLGRIDCOACHPACIENTE";
         lblPaginationbar_currentpagetextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_currentpagecellgridcoachpaciente_Internalname = "PAGINATIONBAR_CURRENTPAGECELLGRIDCOACHPACIENTE";
         lblPaginationbar_nextpagetextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_nextpagecellgridcoachpaciente_Internalname = "PAGINATIONBAR_NEXTPAGECELLGRIDCOACHPACIENTE";
         lblPaginationbar_spacingrighttextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_spacingrightcellgridcoachpaciente_Internalname = "PAGINATIONBAR_SPACINGRIGHTCELLGRIDCOACHPACIENTE";
         lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Internalname = "PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRIDCOACHPACIENTE";
         cellPaginationbar_nextpagebuttoncellgridcoachpaciente_Internalname = "PAGINATIONBAR_NEXTPAGEBUTTONCELLGRIDCOACHPACIENTE";
         tblPaginationbar_pagingcontainertablegridcoachpaciente_Internalname = "PAGINATIONBAR_PAGINGCONTAINERTABLEGRIDCOACHPACIENTE";
         divLayoutdefined_table3_gridcoachpaciente_Internalname = "LAYOUTDEFINED_TABLE3_GRIDCOACHPACIENTE";
         divLayoutdefined_globalgridtable_gridcoachpaciente_Internalname = "LAYOUTDEFINED_GLOBALGRIDTABLE_GRIDCOACHPACIENTE";
         bttAgregarpaciente_Internalname = "AGREGARPACIENTE";
         tblActionscontainertableleft_actions_Internalname = "ACTIONSCONTAINERTABLELEFT_ACTIONS";
         divResponsivetable_containernode_actions_Internalname = "RESPONSIVETABLE_CONTAINERNODE_ACTIONS";
         divMaintabresponsivetable_tab1_Internalname = "MAINTABRESPONSIVETABLE_TAB1";
         lblTab3_tabcontrol_title_Internalname = "TAB3_TABCONTROL_TITLE";
         lblGridsettings_labelgridemergenciabitacora_Internalname = "GRIDSETTINGS_LABELGRIDEMERGENCIABITACORA";
         lblGridsettings_rowsperpagetextblockgridemergenciabitacora_Internalname = "GRIDSETTINGS_ROWSPERPAGETEXTBLOCKGRIDEMERGENCIABITACORA";
         cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname = "vGRIDSETTINGSROWSPERPAGE_GRIDEMERGENCIABITACORA";
         tblGridsettings_tablecontentgridemergenciabitacora_Internalname = "GRIDSETTINGS_TABLECONTENTGRIDEMERGENCIABITACORA";
         bttGridsettings_savegridemergenciabitacora_Internalname = "GRIDSETTINGS_SAVEGRIDEMERGENCIABITACORA";
         divGridsettings_contentoutertablegridemergenciabitacora_Internalname = "GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA";
         divGridsettings_globaltablegridemergenciabitacora_Internalname = "GRIDSETTINGS_GLOBALTABLEGRIDEMERGENCIABITACORA";
         tblLayoutdefined_table2_gridemergenciabitacora_Internalname = "LAYOUTDEFINED_TABLE2_GRIDEMERGENCIABITACORA";
         divLayoutdefined_tableactionscontainer_gridemergenciabitacora_Internalname = "LAYOUTDEFINED_TABLEACTIONSCONTAINER_GRIDEMERGENCIABITACORA";
         edtavEmergenciaid_Internalname = "vEMERGENCIAID";
         edtavEmergenciafecha_Internalname = "vEMERGENCIAFECHA";
         cmbavEmergenciaestado_Internalname = "vEMERGENCIAESTADO";
         edtavEmergenciapaciente_Internalname = "vEMERGENCIAPACIENTE";
         edtavEmergenciapacientenombre_Internalname = "vEMERGENCIAPACIENTENOMBRE";
         edtavEmergenciacoach_Internalname = "vEMERGENCIACOACH";
         edtavEmergencialongitud_Internalname = "vEMERGENCIALONGITUD";
         edtavEmergencialatitud_Internalname = "vEMERGENCIALATITUD";
         edtavEmergenciamensaje_Internalname = "vEMERGENCIAMENSAJE";
         lblI_noresultsfoundtextblock_gridemergenciabitacora_Internalname = "I_NORESULTSFOUNDTEXTBLOCK_GRIDEMERGENCIABITACORA";
         tblI_noresultsfoundtablename_gridemergenciabitacora_Internalname = "I_NORESULTSFOUNDTABLENAME_GRIDEMERGENCIABITACORA";
         divMaingrid_responsivetable_gridemergenciabitacora_Internalname = "MAINGRID_RESPONSIVETABLE_GRIDEMERGENCIABITACORA";
         lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_previouspagebuttoncellgridemergenciabitacora_Internalname = "PAGINATIONBAR_PREVIOUSPAGEBUTTONCELLGRIDEMERGENCIABITACORA";
         lblPaginationbar_firstpagetextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_firstpagecellgridemergenciabitacora_Internalname = "PAGINATIONBAR_FIRSTPAGECELLGRIDEMERGENCIABITACORA";
         lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_spacingleftcellgridemergenciabitacora_Internalname = "PAGINATIONBAR_SPACINGLEFTCELLGRIDEMERGENCIABITACORA";
         lblPaginationbar_previouspagetextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_previouspagecellgridemergenciabitacora_Internalname = "PAGINATIONBAR_PREVIOUSPAGECELLGRIDEMERGENCIABITACORA";
         lblPaginationbar_currentpagetextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_currentpagecellgridemergenciabitacora_Internalname = "PAGINATIONBAR_CURRENTPAGECELLGRIDEMERGENCIABITACORA";
         lblPaginationbar_nextpagetextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_nextpagecellgridemergenciabitacora_Internalname = "PAGINATIONBAR_NEXTPAGECELLGRIDEMERGENCIABITACORA";
         lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_spacingrightcellgridemergenciabitacora_Internalname = "PAGINATIONBAR_SPACINGRIGHTCELLGRIDEMERGENCIABITACORA";
         lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Internalname = "PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRIDEMERGENCIABITACORA";
         cellPaginationbar_nextpagebuttoncellgridemergenciabitacora_Internalname = "PAGINATIONBAR_NEXTPAGEBUTTONCELLGRIDEMERGENCIABITACORA";
         tblPaginationbar_pagingcontainertablegridemergenciabitacora_Internalname = "PAGINATIONBAR_PAGINGCONTAINERTABLEGRIDEMERGENCIABITACORA";
         divLayoutdefined_table3_gridemergenciabitacora_Internalname = "LAYOUTDEFINED_TABLE3_GRIDEMERGENCIABITACORA";
         divLayoutdefined_globalgridtable_gridemergenciabitacora_Internalname = "LAYOUTDEFINED_GLOBALGRIDTABLE_GRIDEMERGENCIABITACORA";
         divMaintabresponsivetable_tab3_Internalname = "MAINTABRESPONSIVETABLE_TAB3";
         Tabs_tabscontrol_Internalname = "TABS_TABSCONTROL";
         divContenttable_Internalname = "CONTENTTABLE";
         edtavConfirmmessage_Internalname = "vCONFIRMMESSAGE";
         bttI_buttonconfirmyes_Internalname = "I_BUTTONCONFIRMYES";
         bttI_buttonconfirmno_Internalname = "I_BUTTONCONFIRMNO";
         tblConfirm_hidden_actionstable_Internalname = "CONFIRM_HIDDEN_ACTIONSTABLE";
         tblSection_condconf_dialog_inner_Internalname = "SECTION_CONDCONF_DIALOG_INNER";
         tblSection_condconf_dialog_Internalname = "SECTION_CONDCONF_DIALOG";
         tblTableconditionalconfirm_Internalname = "TABLECONDITIONALCONFIRM";
         K2bcontrolbeautify1_Internalname = "K2BCONTROLBEAUTIFY1";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         subGridcoachpaciente_Internalname = "GRIDCOACHPACIENTE";
         subGridemergenciabitacora_Internalname = "GRIDEMERGENCIABITACORA";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavEmergenciamensaje_Jsonclick = "";
         edtavEmergencialatitud_Jsonclick = "";
         edtavEmergencialongitud_Jsonclick = "";
         edtavEmergenciacoach_Jsonclick = "";
         edtavEmergenciapacientenombre_Jsonclick = "";
         edtavEmergenciapaciente_Jsonclick = "";
         cmbavEmergenciaestado_Jsonclick = "";
         edtavEmergenciafecha_Jsonclick = "";
         edtavEmergenciaid_Jsonclick = "";
         edtavCoachpaciente_token_Jsonclick = "";
         edtavCoachpaciente_token_Visible = 0;
         edtavCoachpaciente_dispositivoid_Jsonclick = "";
         edtavCoachpaciente_dispositivoid_Visible = 0;
         edtavCoachpaciente_fechaasignacion_Jsonclick = "";
         edtavCoachpaciente_fechaasignacion_Visible = -1;
         edtavCoachpaciente_pacientepapellido_Jsonclick = "";
         edtavCoachpaciente_pacientepapellido_Visible = -1;
         edtavCoachpaciente_pacientepnombre_Jsonclick = "";
         edtavCoachpaciente_pacientepnombre_Visible = -1;
         edtavCoachpaciente_paciente_Jsonclick = "";
         edtavCoachpaciente_paciente_Visible = -1;
         edtavCoachpaciente_coach_Jsonclick = "";
         edtavCoachpaciente_coach_Visible = -1;
         edtavVerrecorridos_action_Jsonclick = "";
         edtavVerrecorridos_action_Visible = -1;
         edtavQuitar_action_Jsonclick = "";
         edtavQuitar_action_Visible = -1;
         cmbavGridsettingsrowsperpage_gridcoachpaciente_Jsonclick = "";
         cmbavGridsettingsrowsperpage_gridcoachpaciente.Enabled = 1;
         divGridsettings_contentoutertablegridcoachpaciente_Visible = 1;
         lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible = 1;
         lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible = 1;
         lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible = 1;
         lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible = 1;
         lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible = 1;
         cmbavGridsettingsrowsperpage_gridemergenciabitacora_Jsonclick = "";
         cmbavGridsettingsrowsperpage_gridemergenciabitacora.Enabled = 1;
         divGridsettings_contentoutertablegridemergenciabitacora_Visible = 1;
         lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible = 1;
         lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible = 1;
         lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible = 1;
         lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible = 1;
         lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible = 1;
         edtavConfirmmessage_Jsonclick = "";
         edtavConfirmmessage_Enabled = 1;
         tblI_noresultsfoundtablename_gridemergenciabitacora_Visible = 1;
         tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible = 1;
         cellPaginationbar_nextpagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationNext";
         cellPaginationbar_spacingrightcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
         cellPaginationbar_previouspagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationPrevious";
         cellPaginationbar_spacingleftcellgridemergenciabitacora_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
         cellPaginationbar_firstpagecellgridemergenciabitacora_Class = "K2BToolsCell_PaginationLeft";
         lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class = "K2BToolsTextBlock_PaginationNormal";
         lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class = "K2BToolsTextBlock_PaginationNormal";
         lblPaginationbar_nextpagetextblockgridemergenciabitacora_Caption = "#";
         lblPaginationbar_currentpagetextblockgridemergenciabitacora_Caption = "#";
         lblPaginationbar_previouspagetextblockgridemergenciabitacora_Caption = "#";
         lblPaginationbar_firstpagetextblockgridemergenciabitacora_Caption = "1";
         tblI_noresultsfoundtablename_gridcoachpaciente_Visible = 1;
         tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible = 1;
         cellPaginationbar_nextpagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationNext";
         cellPaginationbar_spacingrightcellgridcoachpaciente_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
         cellPaginationbar_previouspagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationPrevious";
         cellPaginationbar_spacingleftcellgridcoachpaciente_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
         cellPaginationbar_firstpagecellgridcoachpaciente_Class = "K2BToolsCell_PaginationLeft";
         lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class = "K2BToolsTextBlock_PaginationNormal";
         lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class = "K2BToolsTextBlock_PaginationNormal";
         lblPaginationbar_nextpagetextblockgridcoachpaciente_Caption = "#";
         lblPaginationbar_currentpagetextblockgridcoachpaciente_Caption = "#";
         lblPaginationbar_previouspagetextblockgridcoachpaciente_Caption = "#";
         lblPaginationbar_firstpagetextblockgridcoachpaciente_Caption = "1";
         tblTableconditionalconfirm_Visible = 1;
         subGridemergenciabitacora_Allowcollapsing = 0;
         subGridemergenciabitacora_Allowselection = 0;
         subGridemergenciabitacora_Sortable = 0;
         edtavEmergenciamensaje_Enabled = 0;
         edtavEmergencialatitud_Enabled = 0;
         edtavEmergencialongitud_Enabled = 0;
         edtavEmergenciacoach_Enabled = 0;
         edtavEmergenciapacientenombre_Enabled = 0;
         edtavEmergenciapaciente_Enabled = 0;
         cmbavEmergenciaestado.Enabled = 0;
         edtavEmergenciafecha_Enabled = 0;
         edtavEmergenciaid_Enabled = 0;
         subGridemergenciabitacora_Class = "Grid_WorkWith";
         subGridemergenciabitacora_Backcolorstyle = 0;
         subGridcoachpaciente_Allowcollapsing = 0;
         subGridcoachpaciente_Allowselection = 0;
         subGridcoachpaciente_Sortable = 0;
         edtavCoachpaciente_token_Enabled = 1;
         edtavCoachpaciente_dispositivoid_Enabled = 1;
         edtavCoachpaciente_fechaasignacion_Enabled = 1;
         edtavCoachpaciente_pacientepapellido_Enabled = 1;
         edtavCoachpaciente_pacientepnombre_Enabled = 1;
         edtavCoachpaciente_paciente_Enabled = 1;
         edtavCoachpaciente_coach_Enabled = 1;
         edtavVerrecorridos_action_Enabled = 1;
         edtavQuitar_action_Enabled = 1;
         subGridcoachpaciente_Class = "Grid_WorkWith";
         subGridcoachpaciente_Backcolorstyle = 0;
         edtavPersonaemail_Jsonclick = "";
         edtavPersonaemail_Enabled = 1;
         cmbavPersonatipo_Jsonclick = "";
         cmbavPersonatipo.Enabled = 1;
         edtavPersonapapellido_Jsonclick = "";
         edtavPersonapapellido_Enabled = 1;
         edtavPersonapnombre_Jsonclick = "";
         edtavPersonapnombre_Enabled = 1;
         edtavPersonaid_Jsonclick = "";
         edtavPersonaid_Enabled = 1;
         Tabs_tabscontrol_Historymanagement = Convert.ToBoolean( 0);
         Tabs_tabscontrol_Pagecount = 3;
         Tabs_tabscontrol_Class = "Tab";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Principal";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''}],oparms:[{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("'TOGGLEGRIDSETTINGS(GRIDCOACHPACIENTE)'","{handler:'E311G1',iparms:[{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0}],oparms:[{av:'cmbavGridsettingsrowsperpage_gridcoachpaciente'},{av:'AV28GridSettingsRowsPerPage_GridCoachPaciente',fld:'vGRIDSETTINGSROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'}]}");
         setEventMetadata("'SAVEGRIDSETTINGS(GRIDCOACHPACIENTE)'","{handler:'E111G2',iparms:[{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'cmbavGridsettingsrowsperpage_gridcoachpaciente'},{av:'AV28GridSettingsRowsPerPage_GridCoachPaciente',fld:'vGRIDSETTINGSROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("GRIDCOACHPACIENTE.REFRESH","{handler:'E171G2',iparms:[{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false}],oparms:[{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'lblPaginationbar_firstpagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_previouspagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_currentpagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_nextpagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRIDCOACHPACIENTE',prop:'Class'},{av:'cellPaginationbar_firstpagecellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_FIRSTPAGECELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_spacingleftcellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_SPACINGLEFTCELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_previouspagecellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGECELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_spacingrightcellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_SPACINGRIGHTCELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_nextpagecellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_NEXTPAGECELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_PAGINGCONTAINERTABLEGRIDCOACHPACIENTE',prop:'Visible'}]}");
         setEventMetadata("'PAGINGNEXT(GRIDCOACHPACIENTE)'","{handler:'E301G1',iparms:[{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("'PAGINGPREVIOUS(GRIDCOACHPACIENTE)'","{handler:'E281G1',iparms:[{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("'PAGINGFIRST(GRIDCOACHPACIENTE)'","{handler:'E291G1',iparms:[{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("GRIDCOACHPACIENTE.LOAD","{handler:'E181G2',iparms:[{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false}],oparms:[{av:'tblI_noresultsfoundtablename_gridcoachpaciente_Visible',ctrl:'I_NORESULTSFOUNDTABLENAME_GRIDCOACHPACIENTE',prop:'Visible'},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV30CoachPaciente_Paciente',fld:'vCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV31CoachPaciente_PacientePNombre',fld:'vCOACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'AV32CoachPaciente_PacientePApellido',fld:'vCOACHPACIENTE_PACIENTEPAPELLIDO',pic:'',nv:''},{av:'AV33CoachPaciente_FechaAsignacion',fld:'vCOACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'AV34CoachPaciente_DispositivoID',fld:'vCOACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'AV35CoachPaciente_Token',fld:'vCOACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV42Quitar_Action',fld:'vQUITAR_ACTION',pic:'',nv:''},{av:'AV43VerRecorridos_Action',fld:'vVERRECORRIDOS_ACTION',pic:'',nv:''},{av:'lblPaginationbar_firstpagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_previouspagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_currentpagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_nextpagetextblockgridcoachpaciente_Caption',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Caption'},{av:'lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRIDCOACHPACIENTE',prop:'Class'},{av:'cellPaginationbar_firstpagecellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_FIRSTPAGECELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_spacingleftcellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_SPACINGLEFTCELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_previouspagecellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGECELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_spacingrightcellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_SPACINGRIGHTCELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'cellPaginationbar_nextpagecellgridcoachpaciente_Class',ctrl:'PAGINATIONBAR_NEXTPAGECELLGRIDCOACHPACIENTE',prop:'Class'},{av:'lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDCOACHPACIENTE',prop:'Visible'},{av:'tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible',ctrl:'PAGINATIONBAR_PAGINGCONTAINERTABLEGRIDCOACHPACIENTE',prop:'Visible'}]}");
         setEventMetadata("'E_AGREGARPACIENTE'","{handler:'E121G2',iparms:[{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''}],oparms:[{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''}]}");
         setEventMetadata("'E_QUITAR'","{handler:'E191G2',iparms:[{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV30CoachPaciente_Paciente',fld:'vCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV44ConfirmMessage',fld:'vCONFIRMMESSAGE',pic:'',nv:''},{av:'AV46ConfirmationSubId',fld:'vCONFIRMATIONSUBID',pic:'',nv:''},{av:'AV47GridKey_CoachPaciente_Coach',fld:'vGRIDKEY_COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV48GridKey_CoachPaciente_Paciente',fld:'vGRIDKEY_COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'AV30CoachPaciente_Paciente',fld:'vCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}]}");
         setEventMetadata("'E_VERRECORRIDOS'","{handler:'E201G2',iparms:[{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'}]}");
         setEventMetadata("'CONFIRMYES'","{handler:'E131G2',iparms:[{av:'AV46ConfirmationSubId',fld:'vCONFIRMATIONSUBID',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',grid:91,pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV47GridKey_CoachPaciente_Coach',fld:'vGRIDKEY_COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV30CoachPaciente_Paciente',fld:'vCOACHPACIENTE_PACIENTE',grid:91,pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV48GridKey_CoachPaciente_Paciente',fld:'vGRIDKEY_COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'AV30CoachPaciente_Paciente',fld:'vCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}]}");
         setEventMetadata("'CONFIRMNO'","{handler:'E231G1',iparms:[],oparms:[{av:'AV46ConfirmationSubId',fld:'vCONFIRMATIONSUBID',pic:'',nv:''},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'}]}");
         setEventMetadata("'TOGGLEGRIDSETTINGS(GRIDEMERGENCIABITACORA)'","{handler:'E271G1',iparms:[{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0}],oparms:[{av:'cmbavGridsettingsrowsperpage_gridemergenciabitacora'},{av:'AV55GridSettingsRowsPerPage_GridEmergenciaBitacora',fld:'vGRIDSETTINGSROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'}]}");
         setEventMetadata("'SAVEGRIDSETTINGS(GRIDEMERGENCIABITACORA)'","{handler:'E141G2',iparms:[{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'cmbavGridsettingsrowsperpage_gridemergenciabitacora'},{av:'AV55GridSettingsRowsPerPage_GridEmergenciaBitacora',fld:'vGRIDSETTINGSROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("GRIDEMERGENCIABITACORA.REFRESH","{handler:'E211G2',iparms:[{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false}],oparms:[{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'lblPaginationbar_firstpagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_previouspagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_currentpagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_nextpagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Class'},{av:'cellPaginationbar_firstpagecellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_FIRSTPAGECELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_spacingleftcellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_SPACINGLEFTCELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_previouspagecellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGECELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_spacingrightcellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_SPACINGRIGHTCELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_nextpagecellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_NEXTPAGECELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_PAGINGCONTAINERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'}]}");
         setEventMetadata("'PAGINGNEXT(GRIDEMERGENCIABITACORA)'","{handler:'E261G1',iparms:[{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("'PAGINGPREVIOUS(GRIDEMERGENCIABITACORA)'","{handler:'E241G1',iparms:[{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("'PAGINGFIRST(GRIDEMERGENCIABITACORA)'","{handler:'E251G1',iparms:[{av:'GRIDEMERGENCIABITACORA_nFirstRecordOnPage',nv:0},{av:'GRIDEMERGENCIABITACORA_nEOF',nv:0},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',pic:'',nv:''},{av:'A8PersonaTipo',fld:'PERSONATIPO',pic:'ZZZ9',nv:0},{av:'A11PersonaEMail',fld:'PERSONAEMAIL',pic:'',nv:''},{av:'AV69Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'GRIDCOACHPACIENTE_nFirstRecordOnPage',nv:0},{av:'GRIDCOACHPACIENTE_nEOF',nv:0},{av:'AV27RowsPerPage_GridCoachPaciente',fld:'vROWSPERPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'AV26HasNextPage_GridCoachPaciente',fld:'vHASNEXTPAGE_GRIDCOACHPACIENTE',pic:'',nv:false},{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A16CoachPaciente_PacientePNombre',fld:'COACHPACIENTE_PACIENTEPNOMBRE',pic:'',nv:''},{av:'A17CoachPaciente_PacientePApellid',fld:'COACHPACIENTE_PACIENTEPAPELLID',pic:'',nv:''},{av:'A18CoachPaciente_FechaAsignacion',fld:'COACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'A19CoachPaciente_DispositivoID',fld:'COACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'A20CoachPaciente_Token',fld:'COACHPACIENTE_TOKEN',pic:'',nv:''},{av:'AV29CoachPaciente_Coach',fld:'vCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV45ConfirmationRequired',fld:'vCONFIRMATIONREQUIRED',pic:'',nv:false},{av:'subGridcoachpaciente_Backcolorstyle',ctrl:'GRIDCOACHPACIENTE',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridcoachpaciente_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDCOACHPACIENTE',prop:'Visible'},{av:'AV22CurrentPage_GridCoachPaciente',fld:'vCURRENTPAGE_GRIDCOACHPACIENTE',pic:'ZZZ9',nv:0},{av:'subGridemergenciabitacora_Backcolorstyle',ctrl:'GRIDEMERGENCIABITACORA',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegridemergenciabitacora_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV14PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV15PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV16PersonaPApellido',fld:'vPERSONAPAPELLIDO',pic:'',nv:''},{av:'cmbavPersonatipo'},{av:'AV17PersonaTipo',fld:'vPERSONATIPO',pic:'ZZZ9',nv:0},{av:'AV18PersonaEMail',fld:'vPERSONAEMAIL',pic:'',nv:''},{av:'edtavPersonaid_Enabled',ctrl:'vPERSONAID',prop:'Enabled'},{av:'edtavPersonapnombre_Enabled',ctrl:'vPERSONAPNOMBRE',prop:'Enabled'},{av:'edtavPersonapapellido_Enabled',ctrl:'vPERSONAPAPELLIDO',prop:'Enabled'},{av:'edtavPersonaemail_Enabled',ctrl:'vPERSONAEMAIL',prop:'Enabled'}]}");
         setEventMetadata("GRIDEMERGENCIABITACORA.LOAD","{handler:'E221G5',iparms:[{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A44EmergenciaFecha',fld:'EMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'A45EmergenciaEstado',fld:'EMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'A41EmergenciaPaciente',fld:'EMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A49EmergenciaPacienteNombre',fld:'EMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'A42EmergenciaCoach',fld:'EMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A46EmergenciaLongitud',fld:'EMERGENCIALONGITUD',pic:'',nv:''},{av:'A47EmergenciaLatitud',fld:'EMERGENCIALATITUD',pic:'',nv:''},{av:'A48EmergenciaMensaje',fld:'EMERGENCIAMENSAJE',pic:'',nv:''},{av:'AV54RowsPerPage_GridEmergenciaBitacora',fld:'vROWSPERPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV49CurrentPage_GridEmergenciaBitacora',fld:'vCURRENTPAGE_GRIDEMERGENCIABITACORA',pic:'ZZZ9',nv:0},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV13COACH',fld:'vCOACH',pic:'',nv:''},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false}],oparms:[{av:'tblI_noresultsfoundtablename_gridemergenciabitacora_Visible',ctrl:'I_NORESULTSFOUNDTABLENAME_GRIDEMERGENCIABITACORA',prop:'Visible'},{av:'AV53HasNextPage_GridEmergenciaBitacora',fld:'vHASNEXTPAGE_GRIDEMERGENCIABITACORA',pic:'',nv:false},{av:'AV56EmergenciaID',fld:'vEMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV57EmergenciaFecha',fld:'vEMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'cmbavEmergenciaestado'},{av:'AV58EmergenciaEstado',fld:'vEMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'AV59EmergenciaPaciente',fld:'vEMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV65EmergenciaPacienteNombre',fld:'vEMERGENCIAPACIENTENOMBRE',pic:'',nv:''},{av:'AV60EmergenciaCoach',fld:'vEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV61EmergenciaLongitud',fld:'vEMERGENCIALONGITUD',pic:'',nv:''},{av:'AV62EmergenciaLatitud',fld:'vEMERGENCIALATITUD',pic:'',nv:''},{av:'AV63EmergenciaMensaje',fld:'vEMERGENCIAMENSAJE',pic:'',nv:''},{av:'lblPaginationbar_firstpagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_previouspagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_currentpagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_nextpagetextblockgridemergenciabitacora_Caption',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Caption'},{av:'lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Class'},{av:'cellPaginationbar_firstpagecellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_FIRSTPAGECELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_spacingleftcellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_SPACINGLEFTCELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_previouspagecellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGECELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_spacingrightcellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_SPACINGRIGHTCELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'cellPaginationbar_nextpagecellgridemergenciabitacora_Class',ctrl:'PAGINATIONBAR_NEXTPAGECELLGRIDEMERGENCIABITACORA',prop:'Class'},{av:'lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRIDEMERGENCIABITACORA',prop:'Visible'},{av:'tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible',ctrl:'PAGINATIONBAR_PAGINGCONTAINERTABLEGRIDEMERGENCIABITACORA',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A1PersonaID = (Guid)(Guid.Empty);
         A2PersonaPNombre = "";
         A4PersonaPApellido = "";
         A11PersonaEMail = "";
         AV69Pgmname = "";
         A12CoachPaciente_Coach = (Guid)(Guid.Empty);
         A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         A16CoachPaciente_PacientePNombre = "";
         A17CoachPaciente_PacientePApellid = "";
         A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         A19CoachPaciente_DispositivoID = "";
         A20CoachPaciente_Token = "";
         AV29CoachPaciente_Coach = (Guid)(Guid.Empty);
         AV13COACH = "";
         GXKey = "";
         A43EmergenciaID = (Guid)(Guid.Empty);
         A44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         A41EmergenciaPaciente = (Guid)(Guid.Empty);
         A49EmergenciaPacienteNombre = "";
         A42EmergenciaCoach = (Guid)(Guid.Empty);
         A46EmergenciaLongitud = "";
         A47EmergenciaLatitud = "";
         A48EmergenciaMensaje = "";
         AV60EmergenciaCoach = (Guid)(Guid.Empty);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV46ConfirmationSubId = "";
         AV47GridKey_CoachPaciente_Coach = (Guid)(Guid.Empty);
         AV48GridKey_CoachPaciente_Paciente = (Guid)(Guid.Empty);
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         lblTab_tabcontrol_title_Jsonclick = "";
         TempTags = "";
         AV14PersonaID = (Guid)(Guid.Empty);
         AV15PersonaPNombre = "";
         AV16PersonaPApellido = "";
         AV18PersonaEMail = "";
         lblTab1_tabcontrol_title_Jsonclick = "";
         GridcoachpacienteContainer = new GXWebGrid( context);
         sStyleString = "";
         subGridcoachpaciente_Linesclass = "";
         GridcoachpacienteColumn = new GXWebColumn();
         AV42Quitar_Action = "";
         AV43VerRecorridos_Action = "";
         AV30CoachPaciente_Paciente = (Guid)(Guid.Empty);
         AV31CoachPaciente_PacientePNombre = "";
         AV32CoachPaciente_PacientePApellido = "";
         AV33CoachPaciente_FechaAsignacion = DateTime.MinValue;
         AV34CoachPaciente_DispositivoID = "";
         AV35CoachPaciente_Token = "";
         lblTab3_tabcontrol_title_Jsonclick = "";
         GridemergenciabitacoraContainer = new GXWebGrid( context);
         subGridemergenciabitacora_Linesclass = "";
         GridemergenciabitacoraColumn = new GXWebColumn();
         AV56EmergenciaID = (Guid)(Guid.Empty);
         AV57EmergenciaFecha = (DateTime)(DateTime.MinValue);
         AV59EmergenciaPaciente = (Guid)(Guid.Empty);
         AV65EmergenciaPacienteNombre = "";
         AV61EmergenciaLongitud = "";
         AV62EmergenciaLatitud = "";
         AV63EmergenciaMensaje = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GXCCtl = "";
         AV44ConfirmMessage = "";
         AV12WebSession = context.GetSession();
         scmdbuf = "";
         H001G2_A1PersonaID = new Guid[] {Guid.Empty} ;
         H001G2_A2PersonaPNombre = new String[] {""} ;
         H001G2_A4PersonaPApellido = new String[] {""} ;
         H001G2_A8PersonaTipo = new short[1] ;
         H001G2_A11PersonaEMail = new String[] {""} ;
         H001G2_n11PersonaEMail = new bool[] {false} ;
         AV19GridStateKey = "";
         AV20GridState = new SdtK2BGridState(context);
         H001G3_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         H001G3_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         H001G3_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         H001G3_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         H001G3_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         H001G3_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         H001G3_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         H001G3_A19CoachPaciente_DispositivoID = new String[] {""} ;
         H001G3_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         H001G3_A20CoachPaciente_Token = new String[] {""} ;
         H001G3_n20CoachPaciente_Token = new bool[] {false} ;
         H001G3_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         GridcoachpacienteRow = new GXWebRow();
         GXt_char1 = "";
         GXt_char2 = "";
         H001G4_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         H001G4_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         H001G4_A45EmergenciaEstado = new short[1] ;
         H001G4_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         H001G4_n41EmergenciaPaciente = new bool[] {false} ;
         H001G4_A49EmergenciaPacienteNombre = new String[] {""} ;
         H001G4_n49EmergenciaPacienteNombre = new bool[] {false} ;
         H001G4_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         H001G4_n42EmergenciaCoach = new bool[] {false} ;
         H001G4_A46EmergenciaLongitud = new String[] {""} ;
         H001G4_n46EmergenciaLongitud = new bool[] {false} ;
         H001G4_A47EmergenciaLatitud = new String[] {""} ;
         H001G4_n47EmergenciaLatitud = new bool[] {false} ;
         H001G4_A48EmergenciaMensaje = new String[] {""} ;
         H001G4_n48EmergenciaMensaje = new bool[] {false} ;
         GridemergenciabitacoraRow = new GXWebRow();
         bttI_buttonconfirmyes_Jsonclick = "";
         bttI_buttonconfirmno_Jsonclick = "";
         lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Jsonclick = "";
         lblPaginationbar_firstpagetextblockgridemergenciabitacora_Jsonclick = "";
         lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Jsonclick = "";
         lblPaginationbar_previouspagetextblockgridemergenciabitacora_Jsonclick = "";
         lblPaginationbar_currentpagetextblockgridemergenciabitacora_Jsonclick = "";
         lblPaginationbar_nextpagetextblockgridemergenciabitacora_Jsonclick = "";
         lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Jsonclick = "";
         lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Jsonclick = "";
         lblI_noresultsfoundtextblock_gridemergenciabitacora_Jsonclick = "";
         lblGridsettings_labelgridemergenciabitacora_Jsonclick = "";
         bttGridsettings_savegridemergenciabitacora_Jsonclick = "";
         lblGridsettings_rowsperpagetextblockgridemergenciabitacora_Jsonclick = "";
         bttAgregarpaciente_Jsonclick = "";
         lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Jsonclick = "";
         lblPaginationbar_firstpagetextblockgridcoachpaciente_Jsonclick = "";
         lblPaginationbar_spacinglefttextblockgridcoachpaciente_Jsonclick = "";
         lblPaginationbar_previouspagetextblockgridcoachpaciente_Jsonclick = "";
         lblPaginationbar_currentpagetextblockgridcoachpaciente_Jsonclick = "";
         lblPaginationbar_nextpagetextblockgridcoachpaciente_Jsonclick = "";
         lblPaginationbar_spacingrighttextblockgridcoachpaciente_Jsonclick = "";
         lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Jsonclick = "";
         lblI_noresultsfoundtextblock_gridcoachpaciente_Jsonclick = "";
         lblGridsettings_labelgridcoachpaciente_Jsonclick = "";
         bttGridsettings_savegridcoachpaciente_Jsonclick = "";
         lblGridsettings_rowsperpagetextblockgridcoachpaciente_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.principal__default(),
            new Object[][] {
                new Object[] {
               H001G2_A1PersonaID, H001G2_A2PersonaPNombre, H001G2_A4PersonaPApellido, H001G2_A8PersonaTipo, H001G2_A11PersonaEMail, H001G2_n11PersonaEMail
               }
               , new Object[] {
               H001G3_A12CoachPaciente_Coach, H001G3_A16CoachPaciente_PacientePNombre, H001G3_n16CoachPaciente_PacientePNombre, H001G3_A17CoachPaciente_PacientePApellid, H001G3_n17CoachPaciente_PacientePApellid, H001G3_A18CoachPaciente_FechaAsignacion, H001G3_n18CoachPaciente_FechaAsignacion, H001G3_A19CoachPaciente_DispositivoID, H001G3_n19CoachPaciente_DispositivoID, H001G3_A20CoachPaciente_Token,
               H001G3_n20CoachPaciente_Token, H001G3_A13CoachPaciente_Paciente
               }
               , new Object[] {
               H001G4_A43EmergenciaID, H001G4_A44EmergenciaFecha, H001G4_A45EmergenciaEstado, H001G4_A41EmergenciaPaciente, H001G4_n41EmergenciaPaciente, H001G4_A49EmergenciaPacienteNombre, H001G4_n49EmergenciaPacienteNombre, H001G4_A42EmergenciaCoach, H001G4_n42EmergenciaCoach, H001G4_A46EmergenciaLongitud,
               H001G4_n46EmergenciaLongitud, H001G4_A47EmergenciaLatitud, H001G4_n47EmergenciaLatitud, H001G4_A48EmergenciaMensaje, H001G4_n48EmergenciaMensaje
               }
            }
         );
         AV69Pgmname = "Principal";
         /* GeneXus formulas. */
         AV69Pgmname = "Principal";
         context.Gx_err = 0;
         edtavPersonaid_Enabled = 0;
         edtavQuitar_action_Enabled = 0;
         edtavVerrecorridos_action_Enabled = 0;
         edtavCoachpaciente_coach_Enabled = 0;
         edtavCoachpaciente_paciente_Enabled = 0;
         edtavCoachpaciente_pacientepnombre_Enabled = 0;
         edtavCoachpaciente_pacientepapellido_Enabled = 0;
         edtavCoachpaciente_fechaasignacion_Enabled = 0;
         edtavCoachpaciente_dispositivoid_Enabled = 0;
         edtavCoachpaciente_token_Enabled = 0;
         edtavEmergenciaid_Enabled = 0;
         edtavEmergenciafecha_Enabled = 0;
         cmbavEmergenciaestado.Enabled = 0;
         edtavEmergenciapaciente_Enabled = 0;
         edtavEmergenciapacientenombre_Enabled = 0;
         edtavEmergenciacoach_Enabled = 0;
         edtavEmergencialongitud_Enabled = 0;
         edtavEmergencialatitud_Enabled = 0;
         edtavEmergenciamensaje_Enabled = 0;
         edtavConfirmmessage_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_91 ;
      private short nGXsfl_91_idx=1 ;
      private short nRC_GXsfl_179 ;
      private short AV49CurrentPage_GridEmergenciaBitacora ;
      private short A8PersonaTipo ;
      private short AV22CurrentPage_GridCoachPaciente ;
      private short AV27RowsPerPage_GridCoachPaciente ;
      private short nGXsfl_179_idx=1 ;
      private short AV54RowsPerPage_GridEmergenciaBitacora ;
      private short A45EmergenciaEstado ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV17PersonaTipo ;
      private short subGridcoachpaciente_Backcolorstyle ;
      private short subGridcoachpaciente_Titlebackstyle ;
      private short subGridcoachpaciente_Sortable ;
      private short subGridcoachpaciente_Allowselection ;
      private short subGridcoachpaciente_Allowhovering ;
      private short subGridcoachpaciente_Allowcollapsing ;
      private short subGridcoachpaciente_Collapsed ;
      private short subGridemergenciabitacora_Backcolorstyle ;
      private short subGridemergenciabitacora_Titlebackstyle ;
      private short subGridemergenciabitacora_Sortable ;
      private short AV58EmergenciaEstado ;
      private short subGridemergenciabitacora_Allowselection ;
      private short subGridemergenciabitacora_Allowhovering ;
      private short subGridemergenciabitacora_Allowcollapsing ;
      private short subGridemergenciabitacora_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV28GridSettingsRowsPerPage_GridCoachPaciente ;
      private short AV55GridSettingsRowsPerPage_GridEmergenciaBitacora ;
      private short GRIDCOACHPACIENTE_nEOF ;
      private short GRIDEMERGENCIABITACORA_nEOF ;
      private short AV25I_LoadCount_GridCoachPaciente ;
      private short AV70GXLvl261 ;
      private short nGXsfl_91_fel_idx=1 ;
      private short AV52I_LoadCount_GridEmergenciaBitacora ;
      private short AV72GXLvl581 ;
      private short nGXWrapped ;
      private short subGridcoachpaciente_Backstyle ;
      private short subGridemergenciabitacora_Backstyle ;
      private int divGridsettings_contentoutertablegridcoachpaciente_Visible ;
      private int divGridsettings_contentoutertablegridemergenciabitacora_Visible ;
      private int Tabs_tabscontrol_Pagecount ;
      private int edtavPersonaid_Enabled ;
      private int edtavPersonapnombre_Enabled ;
      private int edtavPersonapapellido_Enabled ;
      private int edtavPersonaemail_Enabled ;
      private int subGridcoachpaciente_Titlebackcolor ;
      private int subGridcoachpaciente_Allbackcolor ;
      private int edtavQuitar_action_Enabled ;
      private int edtavVerrecorridos_action_Enabled ;
      private int edtavCoachpaciente_coach_Enabled ;
      private int edtavCoachpaciente_paciente_Enabled ;
      private int edtavCoachpaciente_pacientepnombre_Enabled ;
      private int edtavCoachpaciente_pacientepapellido_Enabled ;
      private int edtavCoachpaciente_fechaasignacion_Enabled ;
      private int edtavCoachpaciente_dispositivoid_Enabled ;
      private int edtavCoachpaciente_token_Enabled ;
      private int subGridcoachpaciente_Selectioncolor ;
      private int subGridcoachpaciente_Hoveringcolor ;
      private int subGridemergenciabitacora_Titlebackcolor ;
      private int subGridemergenciabitacora_Allbackcolor ;
      private int edtavEmergenciaid_Enabled ;
      private int edtavEmergenciafecha_Enabled ;
      private int edtavEmergenciapaciente_Enabled ;
      private int edtavEmergenciapacientenombre_Enabled ;
      private int edtavEmergenciacoach_Enabled ;
      private int edtavEmergencialongitud_Enabled ;
      private int edtavEmergencialatitud_Enabled ;
      private int edtavEmergenciamensaje_Enabled ;
      private int subGridemergenciabitacora_Selectioncolor ;
      private int subGridemergenciabitacora_Hoveringcolor ;
      private int subGridcoachpaciente_Islastpage ;
      private int subGridemergenciabitacora_Islastpage ;
      private int edtavConfirmmessage_Enabled ;
      private int tblTableconditionalconfirm_Visible ;
      private int lblPaginationbar_firstpagetextblockgridcoachpaciente_Visible ;
      private int lblPaginationbar_spacinglefttextblockgridcoachpaciente_Visible ;
      private int lblPaginationbar_previouspagetextblockgridcoachpaciente_Visible ;
      private int lblPaginationbar_spacingrighttextblockgridcoachpaciente_Visible ;
      private int lblPaginationbar_nextpagetextblockgridcoachpaciente_Visible ;
      private int tblPaginationbar_pagingcontainertablegridcoachpaciente_Visible ;
      private int tblI_noresultsfoundtablename_gridcoachpaciente_Visible ;
      private int lblPaginationbar_firstpagetextblockgridemergenciabitacora_Visible ;
      private int lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Visible ;
      private int lblPaginationbar_previouspagetextblockgridemergenciabitacora_Visible ;
      private int lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Visible ;
      private int lblPaginationbar_nextpagetextblockgridemergenciabitacora_Visible ;
      private int tblPaginationbar_pagingcontainertablegridemergenciabitacora_Visible ;
      private int tblI_noresultsfoundtablename_gridemergenciabitacora_Visible ;
      private int idxLst ;
      private int subGridcoachpaciente_Backcolor ;
      private int edtavQuitar_action_Visible ;
      private int edtavVerrecorridos_action_Visible ;
      private int edtavCoachpaciente_coach_Visible ;
      private int edtavCoachpaciente_paciente_Visible ;
      private int edtavCoachpaciente_pacientepnombre_Visible ;
      private int edtavCoachpaciente_pacientepapellido_Visible ;
      private int edtavCoachpaciente_fechaasignacion_Visible ;
      private int edtavCoachpaciente_dispositivoid_Visible ;
      private int edtavCoachpaciente_token_Visible ;
      private int subGridemergenciabitacora_Backcolor ;
      private long GRIDCOACHPACIENTE_nCurrentRecord ;
      private long GRIDEMERGENCIABITACORA_nCurrentRecord ;
      private long GRIDCOACHPACIENTE_nFirstRecordOnPage ;
      private long GRIDEMERGENCIABITACORA_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_91_idx="0001" ;
      private String AV69Pgmname ;
      private String GXKey ;
      private String sGXsfl_179_idx="0001" ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV46ConfirmationSubId ;
      private String Tabs_tabscontrol_Class ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String divContenttable_Internalname ;
      private String lblTab_tabcontrol_title_Internalname ;
      private String lblTab_tabcontrol_title_Jsonclick ;
      private String divMaintabresponsivetable_tab_Internalname ;
      private String divResponsivetable_mainattributes_attributes_Internalname ;
      private String divAttributescontainertable_attributes_Internalname ;
      private String edtavPersonaid_Internalname ;
      private String TempTags ;
      private String edtavPersonaid_Jsonclick ;
      private String edtavPersonapnombre_Internalname ;
      private String edtavPersonapnombre_Jsonclick ;
      private String edtavPersonapapellido_Internalname ;
      private String edtavPersonapapellido_Jsonclick ;
      private String cmbavPersonatipo_Internalname ;
      private String cmbavPersonatipo_Jsonclick ;
      private String edtavPersonaemail_Internalname ;
      private String edtavPersonaemail_Jsonclick ;
      private String lblTab1_tabcontrol_title_Internalname ;
      private String lblTab1_tabcontrol_title_Jsonclick ;
      private String divMaintabresponsivetable_tab1_Internalname ;
      private String divLayoutdefined_globalgridtable_gridcoachpaciente_Internalname ;
      private String divLayoutdefined_table3_gridcoachpaciente_Internalname ;
      private String divLayoutdefined_tableactionscontainer_gridcoachpaciente_Internalname ;
      private String divMaingrid_responsivetable_gridcoachpaciente_Internalname ;
      private String sStyleString ;
      private String subGridcoachpaciente_Internalname ;
      private String subGridcoachpaciente_Class ;
      private String subGridcoachpaciente_Linesclass ;
      private String AV42Quitar_Action ;
      private String AV43VerRecorridos_Action ;
      private String divResponsivetable_containernode_actions_Internalname ;
      private String lblTab3_tabcontrol_title_Internalname ;
      private String lblTab3_tabcontrol_title_Jsonclick ;
      private String divMaintabresponsivetable_tab3_Internalname ;
      private String divLayoutdefined_globalgridtable_gridemergenciabitacora_Internalname ;
      private String divLayoutdefined_table3_gridemergenciabitacora_Internalname ;
      private String divLayoutdefined_tableactionscontainer_gridemergenciabitacora_Internalname ;
      private String divMaingrid_responsivetable_gridemergenciabitacora_Internalname ;
      private String subGridemergenciabitacora_Internalname ;
      private String subGridemergenciabitacora_Class ;
      private String subGridemergenciabitacora_Linesclass ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavQuitar_action_Internalname ;
      private String edtavVerrecorridos_action_Internalname ;
      private String edtavCoachpaciente_coach_Internalname ;
      private String edtavCoachpaciente_paciente_Internalname ;
      private String edtavCoachpaciente_pacientepnombre_Internalname ;
      private String edtavCoachpaciente_pacientepapellido_Internalname ;
      private String edtavCoachpaciente_fechaasignacion_Internalname ;
      private String edtavCoachpaciente_dispositivoid_Internalname ;
      private String edtavCoachpaciente_token_Internalname ;
      private String edtavEmergenciaid_Internalname ;
      private String edtavEmergenciafecha_Internalname ;
      private String cmbavEmergenciaestado_Internalname ;
      private String edtavEmergenciapaciente_Internalname ;
      private String edtavEmergenciapacientenombre_Internalname ;
      private String edtavEmergenciacoach_Internalname ;
      private String edtavEmergencialongitud_Internalname ;
      private String edtavEmergencialatitud_Internalname ;
      private String edtavEmergenciamensaje_Internalname ;
      private String GXCCtl ;
      private String cmbavGridsettingsrowsperpage_gridcoachpaciente_Internalname ;
      private String cmbavGridsettingsrowsperpage_gridemergenciabitacora_Internalname ;
      private String edtavConfirmmessage_Internalname ;
      private String AV44ConfirmMessage ;
      private String scmdbuf ;
      private String divGridsettings_contentoutertablegridcoachpaciente_Internalname ;
      private String divGridsettings_contentoutertablegridemergenciabitacora_Internalname ;
      private String tblTableconditionalconfirm_Internalname ;
      private String lblPaginationbar_firstpagetextblockgridcoachpaciente_Caption ;
      private String lblPaginationbar_firstpagetextblockgridcoachpaciente_Internalname ;
      private String lblPaginationbar_previouspagetextblockgridcoachpaciente_Caption ;
      private String lblPaginationbar_previouspagetextblockgridcoachpaciente_Internalname ;
      private String lblPaginationbar_currentpagetextblockgridcoachpaciente_Caption ;
      private String lblPaginationbar_currentpagetextblockgridcoachpaciente_Internalname ;
      private String lblPaginationbar_nextpagetextblockgridcoachpaciente_Caption ;
      private String lblPaginationbar_nextpagetextblockgridcoachpaciente_Internalname ;
      private String lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Class ;
      private String lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Internalname ;
      private String lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Class ;
      private String lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Internalname ;
      private String cellPaginationbar_firstpagecellgridcoachpaciente_Class ;
      private String cellPaginationbar_firstpagecellgridcoachpaciente_Internalname ;
      private String cellPaginationbar_spacingleftcellgridcoachpaciente_Class ;
      private String cellPaginationbar_spacingleftcellgridcoachpaciente_Internalname ;
      private String lblPaginationbar_spacinglefttextblockgridcoachpaciente_Internalname ;
      private String cellPaginationbar_previouspagecellgridcoachpaciente_Class ;
      private String cellPaginationbar_previouspagecellgridcoachpaciente_Internalname ;
      private String cellPaginationbar_spacingrightcellgridcoachpaciente_Class ;
      private String cellPaginationbar_spacingrightcellgridcoachpaciente_Internalname ;
      private String lblPaginationbar_spacingrighttextblockgridcoachpaciente_Internalname ;
      private String cellPaginationbar_nextpagecellgridcoachpaciente_Class ;
      private String cellPaginationbar_nextpagecellgridcoachpaciente_Internalname ;
      private String tblPaginationbar_pagingcontainertablegridcoachpaciente_Internalname ;
      private String tblI_noresultsfoundtablename_gridcoachpaciente_Internalname ;
      private String GXt_char1 ;
      private String GXt_char2 ;
      private String sGXsfl_91_fel_idx="0001" ;
      private String lblPaginationbar_firstpagetextblockgridemergenciabitacora_Caption ;
      private String lblPaginationbar_firstpagetextblockgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_previouspagetextblockgridemergenciabitacora_Caption ;
      private String lblPaginationbar_previouspagetextblockgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_currentpagetextblockgridemergenciabitacora_Caption ;
      private String lblPaginationbar_currentpagetextblockgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_nextpagetextblockgridemergenciabitacora_Caption ;
      private String lblPaginationbar_nextpagetextblockgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Class ;
      private String lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Class ;
      private String lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Internalname ;
      private String cellPaginationbar_firstpagecellgridemergenciabitacora_Class ;
      private String cellPaginationbar_firstpagecellgridemergenciabitacora_Internalname ;
      private String cellPaginationbar_spacingleftcellgridemergenciabitacora_Class ;
      private String cellPaginationbar_spacingleftcellgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Internalname ;
      private String cellPaginationbar_previouspagecellgridemergenciabitacora_Class ;
      private String cellPaginationbar_previouspagecellgridemergenciabitacora_Internalname ;
      private String cellPaginationbar_spacingrightcellgridemergenciabitacora_Class ;
      private String cellPaginationbar_spacingrightcellgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Internalname ;
      private String cellPaginationbar_nextpagecellgridemergenciabitacora_Class ;
      private String cellPaginationbar_nextpagecellgridemergenciabitacora_Internalname ;
      private String tblPaginationbar_pagingcontainertablegridemergenciabitacora_Internalname ;
      private String tblI_noresultsfoundtablename_gridemergenciabitacora_Internalname ;
      private String tblSection_condconf_dialog_Internalname ;
      private String tblSection_condconf_dialog_inner_Internalname ;
      private String edtavConfirmmessage_Jsonclick ;
      private String tblConfirm_hidden_actionstable_Internalname ;
      private String bttI_buttonconfirmyes_Internalname ;
      private String bttI_buttonconfirmyes_Jsonclick ;
      private String bttI_buttonconfirmno_Internalname ;
      private String bttI_buttonconfirmno_Jsonclick ;
      private String cellPaginationbar_previouspagebuttoncellgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_previouspagebuttontextblockgridemergenciabitacora_Jsonclick ;
      private String lblPaginationbar_firstpagetextblockgridemergenciabitacora_Jsonclick ;
      private String lblPaginationbar_spacinglefttextblockgridemergenciabitacora_Jsonclick ;
      private String lblPaginationbar_previouspagetextblockgridemergenciabitacora_Jsonclick ;
      private String cellPaginationbar_currentpagecellgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_currentpagetextblockgridemergenciabitacora_Jsonclick ;
      private String lblPaginationbar_nextpagetextblockgridemergenciabitacora_Jsonclick ;
      private String lblPaginationbar_spacingrighttextblockgridemergenciabitacora_Jsonclick ;
      private String cellPaginationbar_nextpagebuttoncellgridemergenciabitacora_Internalname ;
      private String lblPaginationbar_nextpagebuttontextblockgridemergenciabitacora_Jsonclick ;
      private String lblI_noresultsfoundtextblock_gridemergenciabitacora_Internalname ;
      private String lblI_noresultsfoundtextblock_gridemergenciabitacora_Jsonclick ;
      private String tblLayoutdefined_table2_gridemergenciabitacora_Internalname ;
      private String divGridsettings_globaltablegridemergenciabitacora_Internalname ;
      private String lblGridsettings_labelgridemergenciabitacora_Internalname ;
      private String lblGridsettings_labelgridemergenciabitacora_Jsonclick ;
      private String bttGridsettings_savegridemergenciabitacora_Internalname ;
      private String bttGridsettings_savegridemergenciabitacora_Jsonclick ;
      private String tblGridsettings_tablecontentgridemergenciabitacora_Internalname ;
      private String lblGridsettings_rowsperpagetextblockgridemergenciabitacora_Internalname ;
      private String lblGridsettings_rowsperpagetextblockgridemergenciabitacora_Jsonclick ;
      private String cmbavGridsettingsrowsperpage_gridemergenciabitacora_Jsonclick ;
      private String tblActionscontainertableleft_actions_Internalname ;
      private String bttAgregarpaciente_Internalname ;
      private String bttAgregarpaciente_Jsonclick ;
      private String cellPaginationbar_previouspagebuttoncellgridcoachpaciente_Internalname ;
      private String lblPaginationbar_previouspagebuttontextblockgridcoachpaciente_Jsonclick ;
      private String lblPaginationbar_firstpagetextblockgridcoachpaciente_Jsonclick ;
      private String lblPaginationbar_spacinglefttextblockgridcoachpaciente_Jsonclick ;
      private String lblPaginationbar_previouspagetextblockgridcoachpaciente_Jsonclick ;
      private String cellPaginationbar_currentpagecellgridcoachpaciente_Internalname ;
      private String lblPaginationbar_currentpagetextblockgridcoachpaciente_Jsonclick ;
      private String lblPaginationbar_nextpagetextblockgridcoachpaciente_Jsonclick ;
      private String lblPaginationbar_spacingrighttextblockgridcoachpaciente_Jsonclick ;
      private String cellPaginationbar_nextpagebuttoncellgridcoachpaciente_Internalname ;
      private String lblPaginationbar_nextpagebuttontextblockgridcoachpaciente_Jsonclick ;
      private String lblI_noresultsfoundtextblock_gridcoachpaciente_Internalname ;
      private String lblI_noresultsfoundtextblock_gridcoachpaciente_Jsonclick ;
      private String tblLayoutdefined_table2_gridcoachpaciente_Internalname ;
      private String divGridsettings_globaltablegridcoachpaciente_Internalname ;
      private String lblGridsettings_labelgridcoachpaciente_Internalname ;
      private String lblGridsettings_labelgridcoachpaciente_Jsonclick ;
      private String bttGridsettings_savegridcoachpaciente_Internalname ;
      private String bttGridsettings_savegridcoachpaciente_Jsonclick ;
      private String tblGridsettings_tablecontentgridcoachpaciente_Internalname ;
      private String lblGridsettings_rowsperpagetextblockgridcoachpaciente_Internalname ;
      private String lblGridsettings_rowsperpagetextblockgridcoachpaciente_Jsonclick ;
      private String cmbavGridsettingsrowsperpage_gridcoachpaciente_Jsonclick ;
      private String ROClassString ;
      private String edtavQuitar_action_Jsonclick ;
      private String edtavVerrecorridos_action_Jsonclick ;
      private String edtavCoachpaciente_coach_Jsonclick ;
      private String edtavCoachpaciente_paciente_Jsonclick ;
      private String edtavCoachpaciente_pacientepnombre_Jsonclick ;
      private String edtavCoachpaciente_pacientepapellido_Jsonclick ;
      private String edtavCoachpaciente_fechaasignacion_Jsonclick ;
      private String edtavCoachpaciente_dispositivoid_Jsonclick ;
      private String edtavCoachpaciente_token_Jsonclick ;
      private String sGXsfl_179_fel_idx="0001" ;
      private String edtavEmergenciaid_Jsonclick ;
      private String edtavEmergenciafecha_Jsonclick ;
      private String cmbavEmergenciaestado_Jsonclick ;
      private String edtavEmergenciapaciente_Jsonclick ;
      private String edtavEmergenciapacientenombre_Jsonclick ;
      private String edtavEmergenciacoach_Jsonclick ;
      private String edtavEmergencialongitud_Jsonclick ;
      private String edtavEmergencialatitud_Jsonclick ;
      private String edtavEmergenciamensaje_Jsonclick ;
      private String Tabs_tabscontrol_Internalname ;
      private String K2bcontrolbeautify1_Internalname ;
      private DateTime A44EmergenciaFecha ;
      private DateTime AV57EmergenciaFecha ;
      private DateTime A18CoachPaciente_FechaAsignacion ;
      private DateTime AV33CoachPaciente_FechaAsignacion ;
      private bool entryPointCalled ;
      private bool n11PersonaEMail ;
      private bool AV26HasNextPage_GridCoachPaciente ;
      private bool n16CoachPaciente_PacientePNombre ;
      private bool n17CoachPaciente_PacientePApellid ;
      private bool n18CoachPaciente_FechaAsignacion ;
      private bool n19CoachPaciente_DispositivoID ;
      private bool n20CoachPaciente_Token ;
      private bool AV53HasNextPage_GridEmergenciaBitacora ;
      private bool n41EmergenciaPaciente ;
      private bool n49EmergenciaPacienteNombre ;
      private bool n42EmergenciaCoach ;
      private bool n46EmergenciaLongitud ;
      private bool n47EmergenciaLatitud ;
      private bool n48EmergenciaMensaje ;
      private bool toggleJsOutput ;
      private bool AV45ConfirmationRequired ;
      private bool Tabs_tabscontrol_Historymanagement ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_91_Refreshing=false ;
      private bool bGXsfl_179_Refreshing=false ;
      private bool gx_refresh_fired ;
      private bool returnInSub ;
      private bool AV23Reload_GridCoachPaciente ;
      private bool AV50Reload_GridEmergenciaBitacora ;
      private bool AV24LoadRow_GridCoachPaciente ;
      private bool AV51LoadRow_GridEmergenciaBitacora ;
      private String A46EmergenciaLongitud ;
      private String A47EmergenciaLatitud ;
      private String A48EmergenciaMensaje ;
      private String AV61EmergenciaLongitud ;
      private String AV62EmergenciaLatitud ;
      private String AV63EmergenciaMensaje ;
      private String A2PersonaPNombre ;
      private String A4PersonaPApellido ;
      private String A11PersonaEMail ;
      private String A16CoachPaciente_PacientePNombre ;
      private String A17CoachPaciente_PacientePApellid ;
      private String A19CoachPaciente_DispositivoID ;
      private String A20CoachPaciente_Token ;
      private String AV13COACH ;
      private String A49EmergenciaPacienteNombre ;
      private String AV15PersonaPNombre ;
      private String AV16PersonaPApellido ;
      private String AV18PersonaEMail ;
      private String AV31CoachPaciente_PacientePNombre ;
      private String AV32CoachPaciente_PacientePApellido ;
      private String AV34CoachPaciente_DispositivoID ;
      private String AV35CoachPaciente_Token ;
      private String AV65EmergenciaPacienteNombre ;
      private String AV19GridStateKey ;
      private Guid A1PersonaID ;
      private Guid A12CoachPaciente_Coach ;
      private Guid A13CoachPaciente_Paciente ;
      private Guid AV29CoachPaciente_Coach ;
      private Guid A43EmergenciaID ;
      private Guid A41EmergenciaPaciente ;
      private Guid A42EmergenciaCoach ;
      private Guid AV60EmergenciaCoach ;
      private Guid AV47GridKey_CoachPaciente_Coach ;
      private Guid AV48GridKey_CoachPaciente_Paciente ;
      private Guid AV14PersonaID ;
      private Guid AV30CoachPaciente_Paciente ;
      private Guid AV56EmergenciaID ;
      private Guid AV59EmergenciaPaciente ;
      private IGxSession AV12WebSession ;
      private GXWebGrid GridcoachpacienteContainer ;
      private GXWebGrid GridemergenciabitacoraContainer ;
      private GXWebRow GridcoachpacienteRow ;
      private GXWebRow GridemergenciabitacoraRow ;
      private GXWebColumn GridcoachpacienteColumn ;
      private GXWebColumn GridemergenciabitacoraColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavPersonatipo ;
      private GXCombobox cmbavGridsettingsrowsperpage_gridcoachpaciente ;
      private GXCombobox cmbavGridsettingsrowsperpage_gridemergenciabitacora ;
      private GXCombobox cmbavEmergenciaestado ;
      private IDataStoreProvider pr_default ;
      private Guid[] H001G2_A1PersonaID ;
      private String[] H001G2_A2PersonaPNombre ;
      private String[] H001G2_A4PersonaPApellido ;
      private short[] H001G2_A8PersonaTipo ;
      private String[] H001G2_A11PersonaEMail ;
      private bool[] H001G2_n11PersonaEMail ;
      private Guid[] H001G3_A12CoachPaciente_Coach ;
      private String[] H001G3_A16CoachPaciente_PacientePNombre ;
      private bool[] H001G3_n16CoachPaciente_PacientePNombre ;
      private String[] H001G3_A17CoachPaciente_PacientePApellid ;
      private bool[] H001G3_n17CoachPaciente_PacientePApellid ;
      private DateTime[] H001G3_A18CoachPaciente_FechaAsignacion ;
      private bool[] H001G3_n18CoachPaciente_FechaAsignacion ;
      private String[] H001G3_A19CoachPaciente_DispositivoID ;
      private bool[] H001G3_n19CoachPaciente_DispositivoID ;
      private String[] H001G3_A20CoachPaciente_Token ;
      private bool[] H001G3_n20CoachPaciente_Token ;
      private Guid[] H001G3_A13CoachPaciente_Paciente ;
      private Guid[] H001G4_A43EmergenciaID ;
      private DateTime[] H001G4_A44EmergenciaFecha ;
      private short[] H001G4_A45EmergenciaEstado ;
      private Guid[] H001G4_A41EmergenciaPaciente ;
      private bool[] H001G4_n41EmergenciaPaciente ;
      private String[] H001G4_A49EmergenciaPacienteNombre ;
      private bool[] H001G4_n49EmergenciaPacienteNombre ;
      private Guid[] H001G4_A42EmergenciaCoach ;
      private bool[] H001G4_n42EmergenciaCoach ;
      private String[] H001G4_A46EmergenciaLongitud ;
      private bool[] H001G4_n46EmergenciaLongitud ;
      private String[] H001G4_A47EmergenciaLatitud ;
      private bool[] H001G4_n47EmergenciaLatitud ;
      private String[] H001G4_A48EmergenciaMensaje ;
      private bool[] H001G4_n48EmergenciaMensaje ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtK2BGridState AV20GridState ;
   }

   public class principal__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
         ,new ForEachCursor(def[2])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001G2 ;
          prmH001G2 = new Object[] {
          } ;
          Object[] prmH001G3 ;
          prmH001G3 = new Object[] {
          } ;
          Object[] prmH001G4 ;
          prmH001G4 = new Object[] {
          } ;
          def= new CursorDef[] {
              new CursorDef("H001G2", "SELECT [PersonaID], [PersonaPNombre], [PersonaPApellido], [PersonaTipo], [PersonaEMail] FROM [Persona] WITH (NOLOCK) ORDER BY [PersonaID] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001G2,100,0,false,false )
             ,new CursorDef("H001G3", "SELECT T1.[CoachPaciente_Coach], T2.[PersonaPNombre] AS CoachPaciente_PacientePNombre, T2.[PersonaPApellido] AS CoachPaciente_PacientePApellid, T1.[CoachPaciente_FechaAsignacion], T1.[CoachPaciente_DispositivoID], T1.[CoachPaciente_Token], T1.[CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM ([CoachPaciente] T1 WITH (NOLOCK) INNER JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = T1.[CoachPaciente_Paciente]) ORDER BY T1.[CoachPaciente_Coach], T1.[CoachPaciente_Paciente] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001G3,100,0,true,false )
             ,new CursorDef("H001G4", "SELECT T1.[EmergenciaID], T1.[EmergenciaFecha], T1.[EmergenciaEstado], T1.[EmergenciaPaciente] AS EmergenciaPaciente, T2.[PersonaPNombre] AS EmergenciaPacienteNombre, T1.[EmergenciaCoach], T1.[EmergenciaLongitud], T1.[EmergenciaLatitud], T1.[EmergenciaMensaje] FROM ([EmergenciaBitacora] T1 WITH (NOLOCK) LEFT JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = T1.[EmergenciaPaciente]) ORDER BY T1.[EmergenciaID] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001G4,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
                ((short[]) buf[3])[0] = rslt.getShort(4) ;
                ((String[]) buf[4])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(5);
                return;
             case 1 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[2])[0] = rslt.wasNull(2);
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((DateTime[]) buf[5])[0] = rslt.getGXDate(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((String[]) buf[7])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((Guid[]) buf[11])[0] = rslt.getGuid(7) ;
                return;
             case 2 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
                ((short[]) buf[2])[0] = rslt.getShort(3) ;
                ((Guid[]) buf[3])[0] = rslt.getGuid(4) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(4);
                ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(5);
                ((Guid[]) buf[7])[0] = rslt.getGuid(6) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(6);
                ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(7);
                ((String[]) buf[11])[0] = rslt.getLongVarchar(8) ;
                ((bool[]) buf[12])[0] = rslt.wasNull(8);
                ((String[]) buf[13])[0] = rslt.getLongVarchar(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

 }

}
