/*
               File: Gx0070
        Description: Selection List Emergencia Bitacora
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:34:10.15
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gx0070 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gx0070( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public gx0070( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out Guid aP0_pEmergenciaID )
      {
         this.AV11pEmergenciaID = Guid.Empty ;
         executePrivate();
         aP0_pEmergenciaID=this.AV11pEmergenciaID;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavCemergenciaestado = new GXCombobox();
         cmbEmergenciaEstado = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_64 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_64_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_64_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               subGrid1_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV6cEmergenciaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV7cEmergenciaFecha = context.localUtil.ParseDTimeParm( GetNextPar( ));
               AV8cEmergenciaEstado = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV9cEmergenciaPaciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV10cEmergenciaCoach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( subGrid1_Rows, AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV11pEmergenciaID = (Guid)(StringUtil.StrToGuid( gxfirstwebparm));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11pEmergenciaID", AV11pEmergenciaID.ToString());
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "gx0070_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdpromptmasterpage", "GeneXus.Programs.rwdpromptmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,true);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA2Z2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START2Z2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111817341019", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gx0070.aspx") + "?" + UrlEncode(AV11pEmergenciaID.ToString())+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCEMERGENCIAID", AV6cEmergenciaID.ToString());
         GxWebStd.gx_hidden_field( context, "GXH_vCEMERGENCIAFECHA", context.localUtil.TToC( AV7cEmergenciaFecha, 10, 8, 0, 3, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GXH_vCEMERGENCIAESTADO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV8cEmergenciaEstado), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCEMERGENCIAPACIENTE", AV9cEmergenciaPaciente.ToString());
         GxWebStd.gx_hidden_field( context, "GXH_vCEMERGENCIACOACH", AV10cEmergenciaCoach.ToString());
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_64", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_64), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPEMERGENCIAID", AV11pEmergenciaID.ToString());
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ADVANCEDCONTAINER_Class", StringUtil.RTrim( divAdvancedcontainer_Class));
         GxWebStd.gx_hidden_field( context, "BTNTOGGLE_Class", StringUtil.RTrim( bttBtntoggle_Class));
         GxWebStd.gx_hidden_field( context, "EMERGENCIAIDFILTERCONTAINER_Class", StringUtil.RTrim( divEmergenciaidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "EMERGENCIAFECHAFILTERCONTAINER_Class", StringUtil.RTrim( divEmergenciafechafiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "EMERGENCIAESTADOFILTERCONTAINER_Class", StringUtil.RTrim( divEmergenciaestadofiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "EMERGENCIAPACIENTEFILTERCONTAINER_Class", StringUtil.RTrim( divEmergenciapacientefiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "EMERGENCIACOACHFILTERCONTAINER_Class", StringUtil.RTrim( divEmergenciacoachfiltercontainer_Class));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", "notset");
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE2Z2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT2Z2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gx0070.aspx") + "?" + UrlEncode(AV11pEmergenciaID.ToString()) ;
      }

      public override String GetPgmname( )
      {
         return "Gx0070" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selection List Emergencia Bitacora" ;
      }

      protected void WB2Z0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMain_Internalname, 1, 0, "px", 0, "px", "ContainerFluid PromptContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 PromptAdvancedBarCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAdvancedcontainer_Internalname, 1, 0, "px", 0, "px", divAdvancedcontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divEmergenciaidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divEmergenciaidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblemergenciaidfilter_Internalname, "Emergencia ID", "", "", lblLblemergenciaidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e112z1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCemergenciaid_Internalname, "Emergencia ID", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_64_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCemergenciaid_Internalname, AV6cEmergenciaID.ToString(), AV6cEmergenciaID.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCemergenciaid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCemergenciaid_Visible, edtavCemergenciaid_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divEmergenciafechafiltercontainer_Internalname, 1, 0, "px", 0, "px", divEmergenciafechafiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblemergenciafechafilter_Internalname, "Emergencia Fecha", "", "", lblLblemergenciafechafilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e122z1_client"+"'", "", "WWAdvancedLabel WWDateFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCemergenciafecha_Internalname, "Emergencia Fecha", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_64_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCemergenciafecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCemergenciafecha_Internalname, context.localUtil.TToC( AV7cEmergenciaFecha, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV7cEmergenciaFecha, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'spa',false,0);"+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCemergenciafecha_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCemergenciafecha_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx0070.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divEmergenciaestadofiltercontainer_Internalname, 1, 0, "px", 0, "px", divEmergenciaestadofiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblemergenciaestadofilter_Internalname, "Emergencia Estado", "", "", lblLblemergenciaestadofilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e132z1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavCemergenciaestado_Internalname, "Emergencia Estado", "col-sm-3 AttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_64_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavCemergenciaestado, cmbavCemergenciaestado_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0)), 1, cmbavCemergenciaestado_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavCemergenciaestado.Visible, cmbavCemergenciaestado.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "", true, "HLP_Gx0070.htm");
            cmbavCemergenciaestado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCemergenciaestado_Internalname, "Values", (String)(cmbavCemergenciaestado.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divEmergenciapacientefiltercontainer_Internalname, 1, 0, "px", 0, "px", divEmergenciapacientefiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblemergenciapacientefilter_Internalname, "Emergencia Paciente", "", "", lblLblemergenciapacientefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e142z1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCemergenciapaciente_Internalname, "Emergencia Paciente", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_64_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCemergenciapaciente_Internalname, AV9cEmergenciaPaciente.ToString(), AV9cEmergenciaPaciente.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCemergenciapaciente_Jsonclick, 0, "Attribute", "", "", "", "", edtavCemergenciapaciente_Visible, edtavCemergenciapaciente_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divEmergenciacoachfiltercontainer_Internalname, 1, 0, "px", 0, "px", divEmergenciacoachfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblemergenciacoachfilter_Internalname, "Emergencia Coach", "", "", lblLblemergenciacoachfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e152z1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCemergenciacoach_Internalname, "Emergencia Coach", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_64_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCemergenciacoach_Internalname, AV10cEmergenciaCoach.ToString(), AV10cEmergenciaCoach.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCemergenciacoach_Jsonclick, 0, "Attribute", "", "", "", "", edtavCemergenciacoach_Visible, edtavCemergenciacoach_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 WWGridCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridtable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-sm hidden-md hidden-lg ToggleCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = bttBtntoggle_Class;
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntoggle_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(64), 2, 0)+","+"null"+");", "|||", bttBtntoggle_Jsonclick, 7, "|||", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e162z1_client"+"'", TempTags, "", 2, "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"64\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "PromptGrid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"SelectionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "ID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"DescriptionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fecha") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Estado") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Paciente") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  Grid1Container = new GXWebGrid( context);
               }
               else
               {
                  Grid1Container.Clear();
               }
               Grid1Container.SetWrapped(nGXWrapped);
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "PromptGrid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV5LinkSelection));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavLinkselection_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A43EmergenciaID.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.localUtil.TToC( A44EmergenciaFecha, 10, 8, 0, 3, "/", ":", " "));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtEmergenciaFecha_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A45EmergenciaEstado), 4, 0, ".", "")));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A41EmergenciaPaciente.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 64 )
         {
            wbEnd = 0;
            nRC_GXsfl_64 = (short)(nGXsfl_64_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
               Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(64), 2, 0)+","+"null"+");", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gx0070.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START2Z2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Selection List Emergencia Bitacora", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2Z0( ) ;
      }

      protected void WS2Z2( )
      {
         START2Z2( ) ;
         EVT2Z2( ) ;
      }

      protected void EVT2Z2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRID1PAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRID1PAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid1_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid1_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid1_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid1_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_64_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
                              SubsflControlProps_642( ) ;
                              AV5LinkSelection = cgiGet( edtavLinkselection_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV15Linkselection_GXI : context.convertURL( context.PathToRelativeUrl( AV5LinkSelection))), !bGXsfl_64_Refreshing);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "SrcSet", context.GetImageSrcSet( AV5LinkSelection), true);
                              A43EmergenciaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmergenciaID_Internalname)));
                              A44EmergenciaFecha = context.localUtil.CToT( cgiGet( edtEmergenciaFecha_Internalname), 0);
                              cmbEmergenciaEstado.Name = cmbEmergenciaEstado_Internalname;
                              cmbEmergenciaEstado.CurrentValue = cgiGet( cmbEmergenciaEstado_Internalname);
                              A45EmergenciaEstado = (short)(NumberUtil.Val( cgiGet( cmbEmergenciaEstado_Internalname), "."));
                              A41EmergenciaPaciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmergenciaPaciente_Internalname)));
                              n41EmergenciaPaciente = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E172Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E182Z2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Cemergenciaid Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCEMERGENCIAID")) != AV6cEmergenciaID )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cemergenciafecha Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCEMERGENCIAFECHA"), 0) != AV7cEmergenciaFecha )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cemergenciaestado Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCEMERGENCIAESTADO"), ",", ".") != Convert.ToDecimal( AV8cEmergenciaEstado )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cemergenciapaciente Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCEMERGENCIAPACIENTE")) != AV9cEmergenciaPaciente )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cemergenciacoach Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCEMERGENCIACOACH")) != AV10cEmergenciaCoach )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: Enter */
                                          E192Z2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE2Z2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA2Z2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavCemergenciaestado.Name = "vCEMERGENCIAESTADO";
            cmbavCemergenciaestado.WebTags = "";
            cmbavCemergenciaestado.addItem("1", "Activa", 0);
            cmbavCemergenciaestado.addItem("2", "Finalizada", 0);
            if ( cmbavCemergenciaestado.ItemCount > 0 )
            {
               AV8cEmergenciaEstado = (short)(NumberUtil.Val( cmbavCemergenciaestado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cEmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0)));
            }
            GXCCtl = "EMERGENCIAESTADO_" + sGXsfl_64_idx;
            cmbEmergenciaEstado.Name = GXCCtl;
            cmbEmergenciaEstado.WebTags = "";
            cmbEmergenciaEstado.addItem("1", "Activa", 0);
            cmbEmergenciaEstado.addItem("2", "Finalizada", 0);
            if ( cmbEmergenciaEstado.ItemCount > 0 )
            {
               A45EmergenciaEstado = (short)(NumberUtil.Val( cmbEmergenciaEstado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0))), "."));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_642( ) ;
         while ( nGXsfl_64_idx <= nRC_GXsfl_64 )
         {
            sendrow_642( ) ;
            nGXsfl_64_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_64_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_64_idx+1));
            sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
            SubsflControlProps_642( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Grid1Container));
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int subGrid1_Rows ,
                                        Guid AV6cEmergenciaID ,
                                        DateTime AV7cEmergenciaFecha ,
                                        short AV8cEmergenciaEstado ,
                                        Guid AV9cEmergenciaPaciente ,
                                        Guid AV10cEmergenciaCoach )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         GRID1_nCurrentRecord = 0;
         RF2Z2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_EMERGENCIAID", GetSecureSignedToken( "", A43EmergenciaID, context));
         GxWebStd.gx_hidden_field( context, "EMERGENCIAID", A43EmergenciaID.ToString());
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavCemergenciaestado.ItemCount > 0 )
         {
            AV8cEmergenciaEstado = (short)(NumberUtil.Val( cmbavCemergenciaestado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cEmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavCemergenciaestado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCemergenciaestado_Internalname, "Values", cmbavCemergenciaestado.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2Z2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF2Z2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 64;
         nGXsfl_64_idx = 1;
         sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
         SubsflControlProps_642( ) ;
         bGXsfl_64_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "PromptGrid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_642( ) ;
            GXPagingFrom2 = (int)(((10==0) ? 0 : GRID1_nFirstRecordOnPage));
            GXPagingTo2 = ((10==0) ? 10000 : subGrid1_Recordsperpage( )+1);
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV7cEmergenciaFecha ,
                                                 AV8cEmergenciaEstado ,
                                                 AV9cEmergenciaPaciente ,
                                                 AV10cEmergenciaCoach ,
                                                 A44EmergenciaFecha ,
                                                 A45EmergenciaEstado ,
                                                 A41EmergenciaPaciente ,
                                                 A42EmergenciaCoach ,
                                                 AV6cEmergenciaID } ,
                                                 new int[]{
                                                 TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                                 }
            } ) ;
            /* Using cursor H002Z2 */
            pr_default.execute(0, new Object[] {AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach, GXPagingFrom2, GXPagingTo2, GXPagingTo2});
            nGXsfl_64_idx = 1;
            sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
            SubsflControlProps_642( ) ;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( 10 == 0 ) || ( GRID1_nCurrentRecord < subGrid1_Recordsperpage( ) ) ) ) )
            {
               A42EmergenciaCoach = (Guid)((Guid)(H002Z2_A42EmergenciaCoach[0]));
               n42EmergenciaCoach = H002Z2_n42EmergenciaCoach[0];
               A41EmergenciaPaciente = (Guid)((Guid)(H002Z2_A41EmergenciaPaciente[0]));
               n41EmergenciaPaciente = H002Z2_n41EmergenciaPaciente[0];
               A45EmergenciaEstado = H002Z2_A45EmergenciaEstado[0];
               A44EmergenciaFecha = H002Z2_A44EmergenciaFecha[0];
               A43EmergenciaID = (Guid)((Guid)(H002Z2_A43EmergenciaID[0]));
               /* Execute user event: Load */
               E182Z2 ();
               pr_default.readNext(0);
            }
            GRID1_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 64;
            WB2Z0( ) ;
         }
         bGXsfl_64_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes2Z2( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_EMERGENCIAID"+"_"+sGXsfl_64_idx, GetSecureSignedToken( sGXsfl_64_idx, A43EmergenciaID, context));
      }

      protected int subGrid1_Pagecount( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV7cEmergenciaFecha ,
                                              AV8cEmergenciaEstado ,
                                              AV9cEmergenciaPaciente ,
                                              AV10cEmergenciaCoach ,
                                              A44EmergenciaFecha ,
                                              A45EmergenciaEstado ,
                                              A41EmergenciaPaciente ,
                                              A42EmergenciaCoach ,
                                              AV6cEmergenciaID } ,
                                              new int[]{
                                              TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.DATE, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN
                                              }
         } ) ;
         /* Using cursor H002Z3 */
         pr_default.execute(1, new Object[] {AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach});
         GRID1_nRecordCount = H002Z3_AGRID1_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID1_nRecordCount) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(10*1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID1_nFirstRecordOnPage/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected short subgrid1_firstpage( )
      {
         GRID1_nFirstRecordOnPage = 0;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid1_nextpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ( GRID1_nRecordCount >= subGrid1_Recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
         return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid1_previouspage( )
      {
         if ( GRID1_nFirstRecordOnPage >= subGrid1_Recordsperpage( ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid1_lastpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( GRID1_nRecordCount > subGrid1_Recordsperpage( ) )
         {
            if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-subGrid1_Recordsperpage( ));
            }
            else
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))));
            }
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected int subgrid1_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(subGrid1_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cEmergenciaID, AV7cEmergenciaFecha, AV8cEmergenciaEstado, AV9cEmergenciaPaciente, AV10cEmergenciaCoach) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return (int)(0) ;
      }

      protected void STRUP2Z0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E172Z2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( StringUtil.StrCmp(cgiGet( edtavCemergenciaid_Internalname), "") == 0 )
            {
               AV6cEmergenciaID = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cEmergenciaID", AV6cEmergenciaID.ToString());
            }
            else
            {
               try
               {
                  AV6cEmergenciaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCemergenciaid_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cEmergenciaID", AV6cEmergenciaID.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCEMERGENCIAID");
                  GX_FocusControl = edtavCemergenciaid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavCemergenciafecha_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Emergencia Fecha"}), 1, "vCEMERGENCIAFECHA");
               GX_FocusControl = edtavCemergenciafecha_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV7cEmergenciaFecha = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cEmergenciaFecha", context.localUtil.TToC( AV7cEmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV7cEmergenciaFecha = context.localUtil.CToT( cgiGet( edtavCemergenciafecha_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cEmergenciaFecha", context.localUtil.TToC( AV7cEmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
            }
            cmbavCemergenciaestado.Name = cmbavCemergenciaestado_Internalname;
            cmbavCemergenciaestado.CurrentValue = cgiGet( cmbavCemergenciaestado_Internalname);
            AV8cEmergenciaEstado = (short)(NumberUtil.Val( cgiGet( cmbavCemergenciaestado_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cEmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(AV8cEmergenciaEstado), 4, 0)));
            if ( StringUtil.StrCmp(cgiGet( edtavCemergenciapaciente_Internalname), "") == 0 )
            {
               AV9cEmergenciaPaciente = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cEmergenciaPaciente", AV9cEmergenciaPaciente.ToString());
            }
            else
            {
               try
               {
                  AV9cEmergenciaPaciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCemergenciapaciente_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cEmergenciaPaciente", AV9cEmergenciaPaciente.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCEMERGENCIAPACIENTE");
                  GX_FocusControl = edtavCemergenciapaciente_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            if ( StringUtil.StrCmp(cgiGet( edtavCemergenciacoach_Internalname), "") == 0 )
            {
               AV10cEmergenciaCoach = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cEmergenciaCoach", AV10cEmergenciaCoach.ToString());
            }
            else
            {
               try
               {
                  AV10cEmergenciaCoach = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCemergenciacoach_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cEmergenciaCoach", AV10cEmergenciaCoach.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCEMERGENCIACOACH");
                  GX_FocusControl = edtavCemergenciacoach_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            /* Read saved values. */
            nRC_GXsfl_64 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_64"), ",", "."));
            GRID1_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID1_nFirstRecordOnPage"), ",", "."));
            GRID1_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID1_nEOF"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCEMERGENCIAID")) != AV6cEmergenciaID )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCEMERGENCIAFECHA"), 0) != AV7cEmergenciaFecha )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCEMERGENCIAESTADO"), ",", ".") != Convert.ToDecimal( AV8cEmergenciaEstado )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCEMERGENCIAPACIENTE")) != AV9cEmergenciaPaciente )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCEMERGENCIACOACH")) != AV10cEmergenciaCoach )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E172Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E172Z2( )
      {
         /* Start Routine */
         Form.Caption = StringUtil.Format( "Lista de Selecci�n %1", "Emergencia Bitacora", "", "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption, true);
         AV12ADVANCED_LABEL_TEMPLATE = "%1 <strong>%2</strong>";
      }

      private void E182Z2( )
      {
         /* Load Routine */
         AV5LinkSelection = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLinkselection_Internalname, AV5LinkSelection);
         AV15Linkselection_GXI = GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         sendrow_642( ) ;
         GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ! bGXsfl_64_Refreshing )
         {
            context.DoAjaxLoad(64, Grid1Row);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E192Z2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E192Z2( )
      {
         /* Enter Routine */
         AV11pEmergenciaID = (Guid)(A43EmergenciaID);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11pEmergenciaID", AV11pEmergenciaID.ToString());
         context.setWebReturnParms(new Object[] {(Guid)AV11pEmergenciaID});
         context.setWebReturnParmsMetadata(new Object[] {"AV11pEmergenciaID"});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         /*  Sending Event outputs  */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV11pEmergenciaID = (Guid)((Guid)getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11pEmergenciaID", AV11pEmergenciaID.ToString());
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2Z2( ) ;
         WS2Z2( ) ;
         WE2Z2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2018111817341092", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gx0070.js", "?2018111817341092", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_642( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_64_idx;
         edtEmergenciaID_Internalname = "EMERGENCIAID_"+sGXsfl_64_idx;
         edtEmergenciaFecha_Internalname = "EMERGENCIAFECHA_"+sGXsfl_64_idx;
         cmbEmergenciaEstado_Internalname = "EMERGENCIAESTADO_"+sGXsfl_64_idx;
         edtEmergenciaPaciente_Internalname = "EMERGENCIAPACIENTE_"+sGXsfl_64_idx;
      }

      protected void SubsflControlProps_fel_642( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_64_fel_idx;
         edtEmergenciaID_Internalname = "EMERGENCIAID_"+sGXsfl_64_fel_idx;
         edtEmergenciaFecha_Internalname = "EMERGENCIAFECHA_"+sGXsfl_64_fel_idx;
         cmbEmergenciaEstado_Internalname = "EMERGENCIAESTADO_"+sGXsfl_64_fel_idx;
         edtEmergenciaPaciente_Internalname = "EMERGENCIAPACIENTE_"+sGXsfl_64_fel_idx;
      }

      protected void sendrow_642( )
      {
         SubsflControlProps_642( ) ;
         WB2Z0( ) ;
         if ( ( 10 * 1 == 0 ) || ( nGXsfl_64_idx <= subGrid1_Recordsperpage( ) * 1 ) )
         {
            Grid1Row = GXWebRow.GetNew(context,Grid1Container);
            if ( subGrid1_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid1_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
            else if ( subGrid1_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid1_Backstyle = 0;
               subGrid1_Backcolor = subGrid1_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Uniform";
               }
            }
            else if ( subGrid1_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
               subGrid1_Backcolor = (int)(0x0);
            }
            else if ( subGrid1_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( ((int)((nGXsfl_64_idx) % (2))) == 0 )
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Even";
                  }
               }
               else
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Odd";
                  }
               }
            }
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+"PromptGrid"+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_64_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( A43EmergenciaID.ToString())+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Link", edtavLinkselection_Link, !bGXsfl_64_Refreshing);
            ClassString = "SelectionAttribute";
            StyleString = "";
            AV5LinkSelection_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection))&&String.IsNullOrEmpty(StringUtil.RTrim( AV15Linkselection_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)));
            sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV15Linkselection_GXI : context.PathToRelativeUrl( AV5LinkSelection));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavLinkselection_Internalname,(String)sImgUrl,(String)edtavLinkselection_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"WWActionColumn",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV5LinkSelection_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEmergenciaID_Internalname,A43EmergenciaID.ToString(),A43EmergenciaID.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEmergenciaID_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)64,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "DescriptionAttribute";
            edtEmergenciaFecha_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( A43EmergenciaID.ToString())+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaFecha_Internalname, "Link", edtEmergenciaFecha_Link, !bGXsfl_64_Refreshing);
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEmergenciaFecha_Internalname,context.localUtil.TToC( A44EmergenciaFecha, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A44EmergenciaFecha, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)edtEmergenciaFecha_Link,(String)"",(String)"",(String)"",(String)edtEmergenciaFecha_Jsonclick,(short)0,(String)"DescriptionAttribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)64,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( cmbEmergenciaEstado.ItemCount == 0 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "EMERGENCIAESTADO_" + sGXsfl_64_idx;
               cmbEmergenciaEstado.Name = GXCCtl;
               cmbEmergenciaEstado.WebTags = "";
               cmbEmergenciaEstado.addItem("1", "Activa", 0);
               cmbEmergenciaEstado.addItem("2", "Finalizada", 0);
               if ( cmbEmergenciaEstado.ItemCount > 0 )
               {
                  A45EmergenciaEstado = (short)(NumberUtil.Val( cmbEmergenciaEstado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0))), "."));
               }
            }
            /* ComboBox */
            Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbEmergenciaEstado,(String)cmbEmergenciaEstado_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)),(short)1,(String)cmbEmergenciaEstado_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"WWColumn OptionalColumn",(String)"",(String)"",(String)"",(bool)true});
            cmbEmergenciaEstado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmergenciaEstado_Internalname, "Values", (String)(cmbEmergenciaEstado.ToJavascriptSource()), !bGXsfl_64_Refreshing);
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtEmergenciaPaciente_Internalname,A41EmergenciaPaciente.ToString(),A41EmergenciaPaciente.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtEmergenciaPaciente_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)64,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            send_integrity_lvl_hashes2Z2( ) ;
            Grid1Container.AddRow(Grid1Row);
            nGXsfl_64_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_64_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_64_idx+1));
            sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
            SubsflControlProps_642( ) ;
         }
         /* End function sendrow_642 */
      }

      protected void init_default_properties( )
      {
         lblLblemergenciaidfilter_Internalname = "LBLEMERGENCIAIDFILTER";
         edtavCemergenciaid_Internalname = "vCEMERGENCIAID";
         divEmergenciaidfiltercontainer_Internalname = "EMERGENCIAIDFILTERCONTAINER";
         lblLblemergenciafechafilter_Internalname = "LBLEMERGENCIAFECHAFILTER";
         edtavCemergenciafecha_Internalname = "vCEMERGENCIAFECHA";
         divEmergenciafechafiltercontainer_Internalname = "EMERGENCIAFECHAFILTERCONTAINER";
         lblLblemergenciaestadofilter_Internalname = "LBLEMERGENCIAESTADOFILTER";
         cmbavCemergenciaestado_Internalname = "vCEMERGENCIAESTADO";
         divEmergenciaestadofiltercontainer_Internalname = "EMERGENCIAESTADOFILTERCONTAINER";
         lblLblemergenciapacientefilter_Internalname = "LBLEMERGENCIAPACIENTEFILTER";
         edtavCemergenciapaciente_Internalname = "vCEMERGENCIAPACIENTE";
         divEmergenciapacientefiltercontainer_Internalname = "EMERGENCIAPACIENTEFILTERCONTAINER";
         lblLblemergenciacoachfilter_Internalname = "LBLEMERGENCIACOACHFILTER";
         edtavCemergenciacoach_Internalname = "vCEMERGENCIACOACH";
         divEmergenciacoachfiltercontainer_Internalname = "EMERGENCIACOACHFILTERCONTAINER";
         divAdvancedcontainer_Internalname = "ADVANCEDCONTAINER";
         bttBtntoggle_Internalname = "BTNTOGGLE";
         edtavLinkselection_Internalname = "vLINKSELECTION";
         edtEmergenciaID_Internalname = "EMERGENCIAID";
         edtEmergenciaFecha_Internalname = "EMERGENCIAFECHA";
         cmbEmergenciaEstado_Internalname = "EMERGENCIAESTADO";
         edtEmergenciaPaciente_Internalname = "EMERGENCIAPACIENTE";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         divGridtable_Internalname = "GRIDTABLE";
         divMain_Internalname = "MAIN";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtEmergenciaPaciente_Jsonclick = "";
         cmbEmergenciaEstado_Jsonclick = "";
         edtEmergenciaFecha_Jsonclick = "";
         edtEmergenciaID_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtEmergenciaFecha_Link = "";
         edtavLinkselection_Link = "";
         subGrid1_Class = "PromptGrid";
         subGrid1_Backcolorstyle = 0;
         edtavCemergenciacoach_Jsonclick = "";
         edtavCemergenciacoach_Enabled = 1;
         edtavCemergenciacoach_Visible = 1;
         edtavCemergenciapaciente_Jsonclick = "";
         edtavCemergenciapaciente_Enabled = 1;
         edtavCemergenciapaciente_Visible = 1;
         cmbavCemergenciaestado_Jsonclick = "";
         cmbavCemergenciaestado.Enabled = 1;
         cmbavCemergenciaestado.Visible = 1;
         edtavCemergenciafecha_Jsonclick = "";
         edtavCemergenciafecha_Enabled = 1;
         edtavCemergenciaid_Jsonclick = "";
         edtavCemergenciaid_Enabled = 1;
         edtavCemergenciaid_Visible = 1;
         divEmergenciacoachfiltercontainer_Class = "AdvancedContainerItem";
         divEmergenciapacientefiltercontainer_Class = "AdvancedContainerItem";
         divEmergenciaestadofiltercontainer_Class = "AdvancedContainerItem";
         divEmergenciafechafiltercontainer_Class = "AdvancedContainerItem";
         divEmergenciaidfiltercontainer_Class = "AdvancedContainerItem";
         bttBtntoggle_Class = "BtnToggle";
         divAdvancedcontainer_Class = "AdvancedContainerVisible";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Selection List Emergencia Bitacora";
         subGrid1_Rows = 10;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cEmergenciaID',fld:'vCEMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cEmergenciaFecha',fld:'vCEMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'cmbavCemergenciaestado'},{av:'AV8cEmergenciaEstado',fld:'vCEMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'AV9cEmergenciaPaciente',fld:'vCEMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV10cEmergenciaCoach',fld:'vCEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("'TOGGLE'","{handler:'E162Z1',iparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}],oparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]}");
         setEventMetadata("LBLEMERGENCIAIDFILTER.CLICK","{handler:'E112Z1',iparms:[{av:'divEmergenciaidfiltercontainer_Class',ctrl:'EMERGENCIAIDFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divEmergenciaidfiltercontainer_Class',ctrl:'EMERGENCIAIDFILTERCONTAINER',prop:'Class'},{av:'edtavCemergenciaid_Visible',ctrl:'vCEMERGENCIAID',prop:'Visible'}]}");
         setEventMetadata("LBLEMERGENCIAFECHAFILTER.CLICK","{handler:'E122Z1',iparms:[{av:'divEmergenciafechafiltercontainer_Class',ctrl:'EMERGENCIAFECHAFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divEmergenciafechafiltercontainer_Class',ctrl:'EMERGENCIAFECHAFILTERCONTAINER',prop:'Class'}]}");
         setEventMetadata("LBLEMERGENCIAESTADOFILTER.CLICK","{handler:'E132Z1',iparms:[{av:'divEmergenciaestadofiltercontainer_Class',ctrl:'EMERGENCIAESTADOFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divEmergenciaestadofiltercontainer_Class',ctrl:'EMERGENCIAESTADOFILTERCONTAINER',prop:'Class'},{av:'cmbavCemergenciaestado'}]}");
         setEventMetadata("LBLEMERGENCIAPACIENTEFILTER.CLICK","{handler:'E142Z1',iparms:[{av:'divEmergenciapacientefiltercontainer_Class',ctrl:'EMERGENCIAPACIENTEFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divEmergenciapacientefiltercontainer_Class',ctrl:'EMERGENCIAPACIENTEFILTERCONTAINER',prop:'Class'},{av:'edtavCemergenciapaciente_Visible',ctrl:'vCEMERGENCIAPACIENTE',prop:'Visible'}]}");
         setEventMetadata("LBLEMERGENCIACOACHFILTER.CLICK","{handler:'E152Z1',iparms:[{av:'divEmergenciacoachfiltercontainer_Class',ctrl:'EMERGENCIACOACHFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divEmergenciacoachfiltercontainer_Class',ctrl:'EMERGENCIACOACHFILTERCONTAINER',prop:'Class'},{av:'edtavCemergenciacoach_Visible',ctrl:'vCEMERGENCIACOACH',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E192Z2',iparms:[{av:'A43EmergenciaID',fld:'EMERGENCIAID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV11pEmergenciaID',fld:'vPEMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}]}");
         setEventMetadata("GRID1_FIRSTPAGE","{handler:'subgrid1_firstpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cEmergenciaID',fld:'vCEMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cEmergenciaFecha',fld:'vCEMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'cmbavCemergenciaestado'},{av:'AV8cEmergenciaEstado',fld:'vCEMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'AV9cEmergenciaPaciente',fld:'vCEMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV10cEmergenciaCoach',fld:'vCEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("GRID1_PREVPAGE","{handler:'subgrid1_previouspage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cEmergenciaID',fld:'vCEMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cEmergenciaFecha',fld:'vCEMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'cmbavCemergenciaestado'},{av:'AV8cEmergenciaEstado',fld:'vCEMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'AV9cEmergenciaPaciente',fld:'vCEMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV10cEmergenciaCoach',fld:'vCEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("GRID1_NEXTPAGE","{handler:'subgrid1_nextpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cEmergenciaID',fld:'vCEMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cEmergenciaFecha',fld:'vCEMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'cmbavCemergenciaestado'},{av:'AV8cEmergenciaEstado',fld:'vCEMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'AV9cEmergenciaPaciente',fld:'vCEMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV10cEmergenciaCoach',fld:'vCEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("GRID1_LASTPAGE","{handler:'subgrid1_lastpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cEmergenciaID',fld:'vCEMERGENCIAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cEmergenciaFecha',fld:'vCEMERGENCIAFECHA',pic:'99/99/99 99:99',nv:''},{av:'cmbavCemergenciaestado'},{av:'AV8cEmergenciaEstado',fld:'vCEMERGENCIAESTADO',pic:'ZZZ9',nv:0},{av:'AV9cEmergenciaPaciente',fld:'vCEMERGENCIAPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV10cEmergenciaCoach',fld:'vCEMERGENCIACOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6cEmergenciaID = (Guid)(Guid.Empty);
         AV7cEmergenciaFecha = (DateTime)(DateTime.MinValue);
         AV9cEmergenciaPaciente = (Guid)(Guid.Empty);
         AV10cEmergenciaCoach = (Guid)(Guid.Empty);
         GXKey = "";
         AV11pEmergenciaID = (Guid)(Guid.Empty);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblLblemergenciaidfilter_Jsonclick = "";
         TempTags = "";
         lblLblemergenciafechafilter_Jsonclick = "";
         lblLblemergenciaestadofilter_Jsonclick = "";
         lblLblemergenciapacientefilter_Jsonclick = "";
         lblLblemergenciacoachfilter_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtntoggle_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         AV5LinkSelection = "";
         A43EmergenciaID = (Guid)(Guid.Empty);
         A44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         A41EmergenciaPaciente = (Guid)(Guid.Empty);
         bttBtn_cancel_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Linkselection_GXI = "";
         GXCCtl = "";
         scmdbuf = "";
         A42EmergenciaCoach = (Guid)(Guid.Empty);
         H002Z2_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         H002Z2_n42EmergenciaCoach = new bool[] {false} ;
         H002Z2_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         H002Z2_n41EmergenciaPaciente = new bool[] {false} ;
         H002Z2_A45EmergenciaEstado = new short[1] ;
         H002Z2_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         H002Z2_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         H002Z3_AGRID1_nRecordCount = new long[1] ;
         AV12ADVANCED_LABEL_TEMPLATE = "";
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sImgUrl = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gx0070__default(),
            new Object[][] {
                new Object[] {
               H002Z2_A42EmergenciaCoach, H002Z2_n42EmergenciaCoach, H002Z2_A41EmergenciaPaciente, H002Z2_n41EmergenciaPaciente, H002Z2_A45EmergenciaEstado, H002Z2_A44EmergenciaFecha, H002Z2_A43EmergenciaID
               }
               , new Object[] {
               H002Z3_AGRID1_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_64 ;
      private short nGXsfl_64_idx=1 ;
      private short GRID1_nEOF ;
      private short AV8cEmergenciaEstado ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short A45EmergenciaEstado ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int subGrid1_Rows ;
      private int edtavCemergenciaid_Visible ;
      private int edtavCemergenciaid_Enabled ;
      private int edtavCemergenciafecha_Enabled ;
      private int edtavCemergenciapaciente_Visible ;
      private int edtavCemergenciapaciente_Enabled ;
      private int edtavCemergenciacoach_Visible ;
      private int edtavCemergenciacoach_Enabled ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int subGrid1_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nRecordCount ;
      private String divAdvancedcontainer_Class ;
      private String bttBtntoggle_Class ;
      private String divEmergenciaidfiltercontainer_Class ;
      private String divEmergenciafechafiltercontainer_Class ;
      private String divEmergenciaestadofiltercontainer_Class ;
      private String divEmergenciapacientefiltercontainer_Class ;
      private String divEmergenciacoachfiltercontainer_Class ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_64_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMain_Internalname ;
      private String divAdvancedcontainer_Internalname ;
      private String divEmergenciaidfiltercontainer_Internalname ;
      private String lblLblemergenciaidfilter_Internalname ;
      private String lblLblemergenciaidfilter_Jsonclick ;
      private String edtavCemergenciaid_Internalname ;
      private String TempTags ;
      private String edtavCemergenciaid_Jsonclick ;
      private String divEmergenciafechafiltercontainer_Internalname ;
      private String lblLblemergenciafechafilter_Internalname ;
      private String lblLblemergenciafechafilter_Jsonclick ;
      private String edtavCemergenciafecha_Internalname ;
      private String edtavCemergenciafecha_Jsonclick ;
      private String divEmergenciaestadofiltercontainer_Internalname ;
      private String lblLblemergenciaestadofilter_Internalname ;
      private String lblLblemergenciaestadofilter_Jsonclick ;
      private String cmbavCemergenciaestado_Internalname ;
      private String cmbavCemergenciaestado_Jsonclick ;
      private String divEmergenciapacientefiltercontainer_Internalname ;
      private String lblLblemergenciapacientefilter_Internalname ;
      private String lblLblemergenciapacientefilter_Jsonclick ;
      private String edtavCemergenciapaciente_Internalname ;
      private String edtavCemergenciapaciente_Jsonclick ;
      private String divEmergenciacoachfiltercontainer_Internalname ;
      private String lblLblemergenciacoachfilter_Internalname ;
      private String lblLblemergenciacoachfilter_Jsonclick ;
      private String edtavCemergenciacoach_Internalname ;
      private String edtavCemergenciacoach_Jsonclick ;
      private String divGridtable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtntoggle_Internalname ;
      private String bttBtntoggle_Jsonclick ;
      private String sStyleString ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String edtavLinkselection_Link ;
      private String edtEmergenciaFecha_Link ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLinkselection_Internalname ;
      private String edtEmergenciaID_Internalname ;
      private String edtEmergenciaFecha_Internalname ;
      private String cmbEmergenciaEstado_Internalname ;
      private String edtEmergenciaPaciente_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String AV12ADVANCED_LABEL_TEMPLATE ;
      private String sGXsfl_64_fel_idx="0001" ;
      private String sImgUrl ;
      private String ROClassString ;
      private String edtEmergenciaID_Jsonclick ;
      private String edtEmergenciaFecha_Jsonclick ;
      private String cmbEmergenciaEstado_Jsonclick ;
      private String edtEmergenciaPaciente_Jsonclick ;
      private DateTime AV7cEmergenciaFecha ;
      private DateTime A44EmergenciaFecha ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_64_Refreshing=false ;
      private bool n41EmergenciaPaciente ;
      private bool n42EmergenciaCoach ;
      private bool returnInSub ;
      private bool AV5LinkSelection_IsBlob ;
      private String AV15Linkselection_GXI ;
      private String AV5LinkSelection ;
      private Guid AV6cEmergenciaID ;
      private Guid AV9cEmergenciaPaciente ;
      private Guid AV10cEmergenciaCoach ;
      private Guid AV11pEmergenciaID ;
      private Guid A43EmergenciaID ;
      private Guid A41EmergenciaPaciente ;
      private Guid A42EmergenciaCoach ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavCemergenciaestado ;
      private GXCombobox cmbEmergenciaEstado ;
      private IDataStoreProvider pr_default ;
      private Guid[] H002Z2_A42EmergenciaCoach ;
      private bool[] H002Z2_n42EmergenciaCoach ;
      private Guid[] H002Z2_A41EmergenciaPaciente ;
      private bool[] H002Z2_n41EmergenciaPaciente ;
      private short[] H002Z2_A45EmergenciaEstado ;
      private DateTime[] H002Z2_A44EmergenciaFecha ;
      private Guid[] H002Z2_A43EmergenciaID ;
      private long[] H002Z3_AGRID1_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private Guid aP0_pEmergenciaID ;
      private GXWebForm Form ;
   }

   public class gx0070__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H002Z2( IGxContext context ,
                                             DateTime AV7cEmergenciaFecha ,
                                             short AV8cEmergenciaEstado ,
                                             Guid AV9cEmergenciaPaciente ,
                                             Guid AV10cEmergenciaCoach ,
                                             DateTime A44EmergenciaFecha ,
                                             short A45EmergenciaEstado ,
                                             Guid A41EmergenciaPaciente ,
                                             Guid A42EmergenciaCoach ,
                                             Guid AV6cEmergenciaID )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [EmergenciaCoach], [EmergenciaPaciente], [EmergenciaEstado], [EmergenciaFecha], [EmergenciaID]";
         sFromString = " FROM [EmergenciaBitacora] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([EmergenciaID] >= @AV6cEmergenciaID)";
         if ( ! (DateTime.MinValue==AV7cEmergenciaFecha) )
         {
            sWhereString = sWhereString + " and ([EmergenciaFecha] >= @AV7cEmergenciaFecha)";
         }
         else
         {
            GXv_int1[1] = 1;
         }
         if ( ! (0==AV8cEmergenciaEstado) )
         {
            sWhereString = sWhereString + " and ([EmergenciaEstado] >= @AV8cEmergenciaEstado)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (Guid.Empty==AV9cEmergenciaPaciente) )
         {
            sWhereString = sWhereString + " and ([EmergenciaPaciente] >= @AV9cEmergenciaPaciente)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (Guid.Empty==AV10cEmergenciaCoach) )
         {
            sWhereString = sWhereString + " and ([EmergenciaCoach] >= @AV10cEmergenciaCoach)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         sOrderString = sOrderString + " ORDER BY [EmergenciaID]";
         scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + "" + sOrderString + " OFFSET " + "@GXPagingFrom2" + " ROWS FETCH NEXT CAST((SELECT CASE WHEN " + "@GXPagingTo2" + " > 0 THEN " + "@GXPagingTo2" + " ELSE 1e9 END) AS INTEGER) ROWS ONLY";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H002Z3( IGxContext context ,
                                             DateTime AV7cEmergenciaFecha ,
                                             short AV8cEmergenciaEstado ,
                                             Guid AV9cEmergenciaPaciente ,
                                             Guid AV10cEmergenciaCoach ,
                                             DateTime A44EmergenciaFecha ,
                                             short A45EmergenciaEstado ,
                                             Guid A41EmergenciaPaciente ,
                                             Guid A42EmergenciaCoach ,
                                             Guid AV6cEmergenciaID )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [5] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [EmergenciaBitacora] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([EmergenciaID] >= @AV6cEmergenciaID)";
         if ( ! (DateTime.MinValue==AV7cEmergenciaFecha) )
         {
            sWhereString = sWhereString + " and ([EmergenciaFecha] >= @AV7cEmergenciaFecha)";
         }
         else
         {
            GXv_int3[1] = 1;
         }
         if ( ! (0==AV8cEmergenciaEstado) )
         {
            sWhereString = sWhereString + " and ([EmergenciaEstado] >= @AV8cEmergenciaEstado)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (Guid.Empty==AV9cEmergenciaPaciente) )
         {
            sWhereString = sWhereString + " and ([EmergenciaPaciente] >= @AV9cEmergenciaPaciente)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (Guid.Empty==AV10cEmergenciaCoach) )
         {
            sWhereString = sWhereString + " and ([EmergenciaCoach] >= @AV10cEmergenciaCoach)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H002Z2(context, (DateTime)dynConstraints[0] , (short)dynConstraints[1] , (Guid)dynConstraints[2] , (Guid)dynConstraints[3] , (DateTime)dynConstraints[4] , (short)dynConstraints[5] , (Guid)dynConstraints[6] , (Guid)dynConstraints[7] , (Guid)dynConstraints[8] );
               case 1 :
                     return conditional_H002Z3(context, (DateTime)dynConstraints[0] , (short)dynConstraints[1] , (Guid)dynConstraints[2] , (Guid)dynConstraints[3] , (DateTime)dynConstraints[4] , (short)dynConstraints[5] , (Guid)dynConstraints[6] , (Guid)dynConstraints[7] , (Guid)dynConstraints[8] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH002Z2 ;
          prmH002Z2 = new Object[] {
          new Object[] {"@AV6cEmergenciaID",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV7cEmergenciaFecha",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV8cEmergenciaEstado",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9cEmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV10cEmergenciaCoach",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH002Z3 ;
          prmH002Z3 = new Object[] {
          new Object[] {"@AV6cEmergenciaID",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV7cEmergenciaFecha",SqlDbType.DateTime,8,5} ,
          new Object[] {"@AV8cEmergenciaEstado",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV9cEmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV10cEmergenciaCoach",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H002Z2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002Z2,11,0,false,false )
             ,new CursorDef("H002Z3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH002Z3,1,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((Guid[]) buf[2])[0] = rslt.getGuid(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((short[]) buf[4])[0] = rslt.getShort(3) ;
                ((DateTime[]) buf[5])[0] = rslt.getGXDateTime(4) ;
                ((Guid[]) buf[6])[0] = rslt.getGuid(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameterDatetime(sIdx, (DateTime)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[9]);
                }
                return;
       }
    }

 }

}
