/*
               File: K2BUrlGetProgramName
        Description: K2B Stack Get Program Name
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:35.31
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2burlgetprogramname : GXProcedure
   {
      public k2burlgetprogramname( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2burlgetprogramname( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Url ,
                           out String aP1_ProgramName )
      {
         this.AV8Url = aP0_Url;
         this.AV9ProgramName = "" ;
         initialize();
         executePrivate();
         aP1_ProgramName=this.AV9ProgramName;
      }

      public String executeUdp( String aP0_Url )
      {
         this.AV8Url = aP0_Url;
         this.AV9ProgramName = "" ;
         initialize();
         executePrivate();
         aP1_ProgramName=this.AV9ProgramName;
         return AV9ProgramName ;
      }

      public void executeSubmit( String aP0_Url ,
                                 out String aP1_ProgramName )
      {
         k2burlgetprogramname objk2burlgetprogramname;
         objk2burlgetprogramname = new k2burlgetprogramname();
         objk2burlgetprogramname.AV8Url = aP0_Url;
         objk2burlgetprogramname.AV9ProgramName = "" ;
         objk2burlgetprogramname.context.SetSubmitInitialConfig(context);
         objk2burlgetprogramname.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2burlgetprogramname);
         aP1_ProgramName=this.AV9ProgramName;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2burlgetprogramname)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11endIndex = (short)(StringUtil.StringSearch( AV8Url, "?", 1)-1);
         if ( AV11endIndex > 0 )
         {
            AV9ProgramName = StringUtil.Substring( AV8Url, 1, AV11endIndex);
         }
         else
         {
            AV9ProgramName = AV8Url;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV11endIndex ;
      private String AV8Url ;
      private String AV9ProgramName ;
      private String aP1_ProgramName ;
   }

}
