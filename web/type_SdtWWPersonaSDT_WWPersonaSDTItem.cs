/*
				   File: type_SdtWWPersonaSDT_WWPersonaSDTItem
			Description: WWPersonaSDT
				 Author: Nemo for C# version 15.0.9.121631
		   Generated on: 17/11/2018 15:05:17
		   Program type: Callable routine
			  Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;

namespace GeneXus.Programs{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="WWPersonaSDTItem")]
	[XmlType(TypeName="WWPersonaSDTItem" , Namespace="PACYE2" )]
	[Serializable]
	public class SdtWWPersonaSDT_WWPersonaSDTItem : GxUserType
	{
		public SdtWWPersonaSDT_WWPersonaSDTItem( )
		{
			/* Constructor for serialization */
			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapnombre = "";

			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasnombre = "";

			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapapellido = "";

			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasapellido = "";

			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasexo = "";

			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personadpi = "";

		}

		public SdtWWPersonaSDT_WWPersonaSDTItem(IGxContext context)
		{
			this.context = context;
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override String JsonMap(String value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (String)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("PersonaID", gxTpr_Personaid, false);
			AddObjectProperty("PersonaPNombre", gxTpr_Personapnombre, false);
			AddObjectProperty("PersonaSNombre", gxTpr_Personasnombre, false);
			AddObjectProperty("PersonaPApellido", gxTpr_Personapapellido, false);
			AddObjectProperty("PersonaSApellido", gxTpr_Personasapellido, false);
			AddObjectProperty("PersonaSexo", gxTpr_Personasexo, false);
			AddObjectProperty("PersonaDPI", gxTpr_Personadpi, false);
			AddObjectProperty("PersonaTipo", gxTpr_Personatipo, false);
			sDateCnv = "";
			sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTpr_Personafechanacimiento)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
			sDateCnv = sDateCnv + "-";
			sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTpr_Personafechanacimiento)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
			sDateCnv = sDateCnv + "-";
			sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTpr_Personafechanacimiento)), 10, 0));
			sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
			AddObjectProperty("PersonaFechaNacimiento", sDateCnv, false);
			AddObjectProperty("PersonaPacienteOrigenSordera", gxTpr_Personapacienteorigensordera, false);
			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="PersonaID")]
		[XmlElement(ElementName="PersonaID")]
		public Guid gxTpr_Personaid
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personaid; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personaid = value;
				SetDirty("Personaid");
			}
		}


		[SoapElement(ElementName="PersonaPNombre")]
		[XmlElement(ElementName="PersonaPNombre")]
		public String gxTpr_Personapnombre
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapnombre; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapnombre = value;
				SetDirty("Personapnombre");
			}
		}


		[SoapElement(ElementName="PersonaSNombre")]
		[XmlElement(ElementName="PersonaSNombre")]
		public String gxTpr_Personasnombre
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasnombre; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasnombre = value;
				SetDirty("Personasnombre");
			}
		}


		[SoapElement(ElementName="PersonaPApellido")]
		[XmlElement(ElementName="PersonaPApellido")]
		public String gxTpr_Personapapellido
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapapellido; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapapellido = value;
				SetDirty("Personapapellido");
			}
		}


		[SoapElement(ElementName="PersonaSApellido")]
		[XmlElement(ElementName="PersonaSApellido")]
		public String gxTpr_Personasapellido
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasapellido; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasapellido = value;
				SetDirty("Personasapellido");
			}
		}


		[SoapElement(ElementName="PersonaSexo")]
		[XmlElement(ElementName="PersonaSexo")]
		public String gxTpr_Personasexo
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasexo; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasexo = value;
				SetDirty("Personasexo");
			}
		}


		[SoapElement(ElementName="PersonaDPI")]
		[XmlElement(ElementName="PersonaDPI")]
		public String gxTpr_Personadpi
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personadpi; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personadpi = value;
				SetDirty("Personadpi");
			}
		}


		[SoapElement(ElementName="PersonaTipo")]
		[XmlElement(ElementName="PersonaTipo")]
		public short gxTpr_Personatipo
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personatipo; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personatipo = value;
				SetDirty("Personatipo");
			}
		}


		[SoapElement(ElementName="PersonaFechaNacimiento")]
		[XmlElement(ElementName="PersonaFechaNacimiento" , IsNullable=true)]
		public string gxTpr_Personafechanacimiento_Nullable
		{
			get {
				if ( gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personafechanacimiento == DateTime.MinValue)
					return null;
				return new GxDateString(gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personafechanacimiento).value ;
			}
			set {
				if (value == null || value == GxDateString.NullValue )
					gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personafechanacimiento = DateTime.MinValue;
				else
					gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personafechanacimiento = DateTime.Parse( value);
			}
		}

		[SoapIgnore]
		[XmlIgnore]
		public DateTime gxTpr_Personafechanacimiento
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personafechanacimiento; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personafechanacimiento = value;
				SetDirty("Personafechanacimiento");
			}
		}

		[SoapElement(ElementName="PersonaPacienteOrigenSordera")]
		[XmlElement(ElementName="PersonaPacienteOrigenSordera")]
		public short gxTpr_Personapacienteorigensordera
		{
			get { 
				return gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapacienteorigensordera; 
			}
			set { 
				gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapacienteorigensordera = value;
				SetDirty("Personapacienteorigensordera");
			}
		}


		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapnombre = "";
			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasnombre = "";
			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapapellido = "";
			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasapellido = "";
			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasexo = "";
			gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personadpi = "";



			sDateCnv = "";
			sNumToPad = "";
			return  ;
		}



		#endregion

		#region Declaration

		protected String sDateCnv ;
		protected String sNumToPad ;
		protected Guid gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personaid;
		protected String gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapnombre;
		protected String gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasnombre;
		protected String gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapapellido;
		protected String gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasapellido;
		protected String gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personasexo;
		protected String gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personadpi;
		protected short gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personatipo;
		protected DateTime gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personafechanacimiento;
		protected short gxTv_SdtWWPersonaSDT_WWPersonaSDTItem_Personapacienteorigensordera;



		#endregion
	}
	#region Rest interface
	[DataContract(Name=@"WWPersonaSDTItem", Namespace="PACYE2")]
	public class SdtWWPersonaSDT_WWPersonaSDTItem_RESTInterface : GxGenericCollectionItem<SdtWWPersonaSDT_WWPersonaSDTItem>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtWWPersonaSDT_WWPersonaSDTItem_RESTInterface( ) : base()
		{
		}

		public SdtWWPersonaSDT_WWPersonaSDTItem_RESTInterface( SdtWWPersonaSDT_WWPersonaSDTItem psdt ) : base(psdt)
		{
		}

		#region Rest Properties
		[DataMember(Name="PersonaID", Order=0)]
		public Guid gxTpr_Personaid
		{
			get { 
				return sdt.gxTpr_Personaid;
			}
			set { 
				sdt.gxTpr_Personaid = value;
			}
		}

		[DataMember(Name="PersonaPNombre", Order=1)]
		public String gxTpr_Personapnombre
		{
			get { 
				return sdt.gxTpr_Personapnombre;
			}
			set { 
				sdt.gxTpr_Personapnombre = value;
			}
		}

		[DataMember(Name="PersonaSNombre", Order=2)]
		public String gxTpr_Personasnombre
		{
			get { 
				return sdt.gxTpr_Personasnombre;
			}
			set { 
				sdt.gxTpr_Personasnombre = value;
			}
		}

		[DataMember(Name="PersonaPApellido", Order=3)]
		public String gxTpr_Personapapellido
		{
			get { 
				return sdt.gxTpr_Personapapellido;
			}
			set { 
				sdt.gxTpr_Personapapellido = value;
			}
		}

		[DataMember(Name="PersonaSApellido", Order=4)]
		public String gxTpr_Personasapellido
		{
			get { 
				return sdt.gxTpr_Personasapellido;
			}
			set { 
				sdt.gxTpr_Personasapellido = value;
			}
		}

		[DataMember(Name="PersonaSexo", Order=5)]
		public String gxTpr_Personasexo
		{
			get { 
				return StringUtil.RTrim( sdt.gxTpr_Personasexo);
			}
			set { 
				sdt.gxTpr_Personasexo = value;
			}
		}

		[DataMember(Name="PersonaDPI", Order=6)]
		public String gxTpr_Personadpi
		{
			get { 
				return sdt.gxTpr_Personadpi;
			}
			set { 
				sdt.gxTpr_Personadpi = value;
			}
		}

		[DataMember(Name="PersonaTipo", Order=7)]
		public  Nullable<short> gxTpr_Personatipo
		{
			get { 
				return sdt.gxTpr_Personatipo;
			}
			set { 
				sdt.gxTpr_Personatipo = (short) (value.HasValue ? value.Value : 0);
			}
		}

		[DataMember(Name="PersonaFechaNacimiento", Order=8)]
		public String gxTpr_Personafechanacimiento
		{
			get { 
				return DateTimeUtil.DToC2( sdt.gxTpr_Personafechanacimiento);
			}
			set { 
				sdt.gxTpr_Personafechanacimiento = DateTimeUtil.CToD2(value);
			}
		}

		[DataMember(Name="PersonaPacienteOrigenSordera", Order=9)]
		public  Nullable<short> gxTpr_Personapacienteorigensordera
		{
			get { 
				return sdt.gxTpr_Personapacienteorigensordera;
			}
			set { 
				sdt.gxTpr_Personapacienteorigensordera = (short) (value.HasValue ? value.Value : 0);
			}
		}


		#endregion

		public SdtWWPersonaSDT_WWPersonaSDTItem sdt
		{
			get { 
				return (SdtWWPersonaSDT_WWPersonaSDTItem)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtWWPersonaSDT_WWPersonaSDTItem() ;
			}
		}
	}
	#endregion
}