/*
               File: WWPersona
        Description: Personas
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 12:48:55.62
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wwpersona : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wwpersona( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public wwpersona( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavAtt_personaid_visible = new GXCheckbox();
         chkavAtt_personapnombre_visible = new GXCheckbox();
         chkavAtt_personasnombre_visible = new GXCheckbox();
         chkavAtt_personapapellido_visible = new GXCheckbox();
         chkavAtt_personasapellido_visible = new GXCheckbox();
         chkavAtt_personasexo_visible = new GXCheckbox();
         chkavAtt_personadpi_visible = new GXCheckbox();
         chkavAtt_personatipo_visible = new GXCheckbox();
         chkavAtt_personafechanacimiento_visible = new GXCheckbox();
         chkavAtt_personapacienteorigensordera_visible = new GXCheckbox();
         cmbavGridsettingsorderedby = new GXCombobox();
         cmbavGridsettingsrowsperpagevariable = new GXCombobox();
         chkavCheckall = new GXCheckbox();
         chkavSelected = new GXCheckbox();
         cmbPersonaTipo = new GXCombobox();
         cmbPersonaPacienteOrigenSordera = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_167 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_167_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_167_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               subGrid_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV8PersonaPNombre = GetNextPar( );
               AV10PersonaFechaNacimiento_From = context.localUtil.ParseDateParm( GetNextPar( ));
               AV11PersonaFechaNacimiento_To = context.localUtil.ParseDateParm( GetNextPar( ));
               AV12OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV61Pgmname = GetNextPar( );
               AV44CurrentPage = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV41GridSettingsRowsPerPageVariable = (long)(NumberUtil.Val( GetNextPar( ), "."));
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14GridColumns);
               AV18AttributeName = GetNextPar( );
               AV19ColumnTitle = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV37AllSelectedRows);
               AV38SelectedRowsAmount = (short)(NumberUtil.Val( GetNextPar( ), "."));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "persona_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA1E2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START1E2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111812485633", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("K2BOrderBy/K2BOrderByRender.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("wwpersona.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vPERSONAPNOMBRE", AV8PersonaPNombre);
         GxWebStd.gx_hidden_field( context, "GXH_vPERSONAFECHANACIMIENTO_FROM", context.localUtil.Format(AV10PersonaFechaNacimiento_From, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vPERSONAFECHANACIMIENTO_TO", context.localUtil.Format(AV11PersonaFechaNacimiento_To, "99/99/99"));
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_167", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_167), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDORDERS", AV48GridOrders);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDORDERS", AV48GridOrders);
         }
         GxWebStd.gx_hidden_field( context, "vUC_ORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV50UC_OrderedBy), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV61Pgmname));
         GxWebStd.gx_hidden_field( context, "vCURRENTPAGE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44CurrentPage), 5, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vORDEREDBY", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12OrderedBy), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vGRIDCOLUMNS", AV14GridColumns);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vGRIDCOLUMNS", AV14GridColumns);
         }
         GxWebStd.gx_hidden_field( context, "vATTRIBUTENAME", AV18AttributeName);
         GxWebStd.gx_hidden_field( context, "vCOLUMNTITLE", AV19ColumnTitle);
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vALLSELECTEDROWS", AV37AllSelectedRows);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vALLSELECTEDROWS", AV37AllSelectedRows);
         }
         GxWebStd.gx_hidden_field( context, "vSELECTEDROWSAMOUNT", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV38SelectedRowsAmount), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vSELECTEDROWS", AV34SelectedRows);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vSELECTEDROWS", AV34SelectedRows);
         }
         GxWebStd.gx_hidden_field( context, "vCONFIRMATIONSUBID", StringUtil.RTrim( AV52ConfirmationSubId));
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "K2BORDERBYUSERCONTROL_Gridcontrolname", StringUtil.RTrim( K2borderbyusercontrol_Gridcontrolname));
         GxWebStd.gx_hidden_field( context, "K2BGRIDSETTINGSCONTENTOUTERTABLE_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(divK2bgridsettingscontentoutertable_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE1E2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT1E2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wwpersona.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "WWPersona" ;
      }

      public override String GetPgmdesc( )
      {
         return "Personas" ;
      }

      protected void WB1E0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_MainContentTable", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPgmdescriptortextblock_Internalname, "Personas", "", "", lblPgmdescriptortextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock_Title", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable2_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable7_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_RoundedBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2btoolsfilterscontainer_Internalname, 1, 0, "px", 0, "px", "FilterContainerTable", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFilterattributestable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavPersonapnombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPersonapnombre_Internalname, "PNombre", "col-sm-3 Attribute_FilterLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_167_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPersonapnombre_Internalname, AV8PersonaPNombre, StringUtil.RTrim( context.localUtil.Format( AV8PersonaPNombre, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPersonapnombre_Jsonclick, 0, "Attribute_Filter", "", "", "", "", 1, edtavPersonapnombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPersona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2btoolstable_attributecontainerpersonafechanacimiento_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_AttributeContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblockpersonafechanacimiento_Internalname, "Fecha Nacimiento", "", "", lblTextblockpersonafechanacimiento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Label", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9", "left", "top", "", "", "div");
            wb_table1_34_1E2( true) ;
         }
         else
         {
            wb_table1_34_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table1_34_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGlobalgridtable_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_TopBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable5_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_FullWidth", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridactionsleftcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 col-sm-8", "left", "top", "", "", "div");
            wb_table2_52_1E2( true) ;
         }
         else
         {
            wb_table2_52_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table2_52_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-xs col-sm-4", "left", "top", "", "", "div");
            wb_table3_61_1E2( true) ;
         }
         else
         {
            wb_table3_61_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table3_61_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaingridcontainerresponsivetablegrid1_Internalname, 1, 0, "px", 0, "px", "Section_Grid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table4_161_1E2( true) ;
         }
         else
         {
            wb_table4_161_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table4_161_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table5_181_1E2( true) ;
         }
         else
         {
            wb_table5_181_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table5_181_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable4_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table6_190_1E2( true) ;
         }
         else
         {
            wb_table6_190_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table6_190_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table7_212_1E2( true) ;
         }
         else
         {
            wb_table7_212_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table7_212_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table8_218_1E2( true) ;
         }
         else
         {
            wb_table8_218_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table8_218_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"K2BCONTROLBEAUTIFY1Container"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START1E2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Personas", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1E0( ) ;
      }

      protected void WS1E2( )
      {
         START1E2( ) ;
         EVT1E2( ) ;
      }

      protected void EVT1E2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOINSERT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoInsert' */
                              E111E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOEXPORT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoExport' */
                              E121E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "K2BORDERBYUSERCONTROL.ORDERBYCHANGED") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              E131E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOFIRST'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoFirst' */
                              E141E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOPREVIOUS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoPrevious' */
                              E151E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DONEXT'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoNext' */
                              E161E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOLAST'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoLast' */
                              E171E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DOUPDATE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoUpdate' */
                              E181E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'DODELETE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'DoDelete' */
                              E191E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VCHECKALL.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              E201E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'SAVEGRIDSETTINGS'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'SaveGridSettings' */
                              E211E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'CONFIRMYES'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'ConfirmYes' */
                              E221E2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "GRID.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "PERSONAPNOMBRE.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 15), "VSELECTED.CLICK") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 20), "PERSONAPNOMBRE.CLICK") == 0 ) )
                           {
                              nGXsfl_167_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
                              SubsflControlProps_1672( ) ;
                              AV33Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV33Selected);
                              A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
                              A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
                              A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
                              n3PersonaSNombre = false;
                              A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
                              A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
                              n5PersonaSApellido = false;
                              A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
                              n6PersonaSexo = false;
                              A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
                              n7PersonaDPI = false;
                              cmbPersonaTipo.Name = cmbPersonaTipo_Internalname;
                              cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
                              A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
                              A9PersonaFechaNacimiento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPersonaFechaNacimiento_Internalname), 0));
                              n9PersonaFechaNacimiento = false;
                              cmbPersonaPacienteOrigenSordera.Name = cmbPersonaPacienteOrigenSordera_Internalname;
                              cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
                              A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
                              n10PersonaPacienteOrigenSordera = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E231E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Refresh */
                                    E241E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E251E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E261E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "PERSONAPNOMBRE.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E271E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "VSELECTED.CLICK") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E281E2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Personapnombre Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vPERSONAPNOMBRE"), AV8PersonaPNombre) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Personafechanacimiento_from Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPERSONAFECHANACIMIENTO_FROM"), 0) != AV10PersonaFechaNacimiento_From )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Personafechanacimiento_to Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vPERSONAFECHANACIMIENTO_TO"), 0) != AV11PersonaFechaNacimiento_To )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1E2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA1E2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavAtt_personaid_visible.Name = "vATT_PERSONAID_VISIBLE";
            chkavAtt_personaid_visible.WebTags = "";
            chkavAtt_personaid_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personaid_visible_Internalname, "TitleCaption", chkavAtt_personaid_visible.Caption, true);
            chkavAtt_personaid_visible.CheckedValue = "false";
            chkavAtt_personapnombre_visible.Name = "vATT_PERSONAPNOMBRE_VISIBLE";
            chkavAtt_personapnombre_visible.WebTags = "";
            chkavAtt_personapnombre_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personapnombre_visible_Internalname, "TitleCaption", chkavAtt_personapnombre_visible.Caption, true);
            chkavAtt_personapnombre_visible.CheckedValue = "false";
            chkavAtt_personasnombre_visible.Name = "vATT_PERSONASNOMBRE_VISIBLE";
            chkavAtt_personasnombre_visible.WebTags = "";
            chkavAtt_personasnombre_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personasnombre_visible_Internalname, "TitleCaption", chkavAtt_personasnombre_visible.Caption, true);
            chkavAtt_personasnombre_visible.CheckedValue = "false";
            chkavAtt_personapapellido_visible.Name = "vATT_PERSONAPAPELLIDO_VISIBLE";
            chkavAtt_personapapellido_visible.WebTags = "";
            chkavAtt_personapapellido_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personapapellido_visible_Internalname, "TitleCaption", chkavAtt_personapapellido_visible.Caption, true);
            chkavAtt_personapapellido_visible.CheckedValue = "false";
            chkavAtt_personasapellido_visible.Name = "vATT_PERSONASAPELLIDO_VISIBLE";
            chkavAtt_personasapellido_visible.WebTags = "";
            chkavAtt_personasapellido_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personasapellido_visible_Internalname, "TitleCaption", chkavAtt_personasapellido_visible.Caption, true);
            chkavAtt_personasapellido_visible.CheckedValue = "false";
            chkavAtt_personasexo_visible.Name = "vATT_PERSONASEXO_VISIBLE";
            chkavAtt_personasexo_visible.WebTags = "";
            chkavAtt_personasexo_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personasexo_visible_Internalname, "TitleCaption", chkavAtt_personasexo_visible.Caption, true);
            chkavAtt_personasexo_visible.CheckedValue = "false";
            chkavAtt_personadpi_visible.Name = "vATT_PERSONADPI_VISIBLE";
            chkavAtt_personadpi_visible.WebTags = "";
            chkavAtt_personadpi_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personadpi_visible_Internalname, "TitleCaption", chkavAtt_personadpi_visible.Caption, true);
            chkavAtt_personadpi_visible.CheckedValue = "false";
            chkavAtt_personatipo_visible.Name = "vATT_PERSONATIPO_VISIBLE";
            chkavAtt_personatipo_visible.WebTags = "";
            chkavAtt_personatipo_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personatipo_visible_Internalname, "TitleCaption", chkavAtt_personatipo_visible.Caption, true);
            chkavAtt_personatipo_visible.CheckedValue = "false";
            chkavAtt_personafechanacimiento_visible.Name = "vATT_PERSONAFECHANACIMIENTO_VISIBLE";
            chkavAtt_personafechanacimiento_visible.WebTags = "";
            chkavAtt_personafechanacimiento_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personafechanacimiento_visible_Internalname, "TitleCaption", chkavAtt_personafechanacimiento_visible.Caption, true);
            chkavAtt_personafechanacimiento_visible.CheckedValue = "false";
            chkavAtt_personapacienteorigensordera_visible.Name = "vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE";
            chkavAtt_personapacienteorigensordera_visible.WebTags = "";
            chkavAtt_personapacienteorigensordera_visible.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAtt_personapacienteorigensordera_visible_Internalname, "TitleCaption", chkavAtt_personapacienteorigensordera_visible.Caption, true);
            chkavAtt_personapacienteorigensordera_visible.CheckedValue = "false";
            cmbavGridsettingsorderedby.Name = "vGRIDSETTINGSORDEREDBY";
            cmbavGridsettingsorderedby.WebTags = "";
            cmbavGridsettingsorderedby.addItem("0", "Persona ID (1 � 10)", 0);
            cmbavGridsettingsorderedby.addItem("1", "Persona ID (10 � 1)", 0);
            cmbavGridsettingsorderedby.addItem("2", "Persona PNombre (1 � 10)", 0);
            cmbavGridsettingsorderedby.addItem("3", "Persona PNombre (10 � 1)", 0);
            if ( cmbavGridsettingsorderedby.ItemCount > 0 )
            {
               AV13GridSettingsOrderedBy = (short)(NumberUtil.Val( cmbavGridsettingsorderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13GridSettingsOrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0)));
            }
            cmbavGridsettingsrowsperpagevariable.Name = "vGRIDSETTINGSROWSPERPAGEVARIABLE";
            cmbavGridsettingsrowsperpagevariable.WebTags = "";
            cmbavGridsettingsrowsperpagevariable.addItem("10", "10", 0);
            cmbavGridsettingsrowsperpagevariable.addItem("20", "20", 0);
            cmbavGridsettingsrowsperpagevariable.addItem("50", "50", 0);
            cmbavGridsettingsrowsperpagevariable.addItem("100", "100", 0);
            cmbavGridsettingsrowsperpagevariable.addItem("200", "200", 0);
            if ( cmbavGridsettingsrowsperpagevariable.ItemCount > 0 )
            {
               AV41GridSettingsRowsPerPageVariable = (long)(NumberUtil.Val( cmbavGridsettingsrowsperpagevariable.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridSettingsRowsPerPageVariable", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0)));
            }
            chkavCheckall.Name = "vCHECKALL";
            chkavCheckall.WebTags = "";
            chkavCheckall.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavCheckall_Internalname, "TitleCaption", chkavCheckall.Caption, true);
            chkavCheckall.CheckedValue = "false";
            GXCCtl = "vSELECTED_" + sGXsfl_167_idx;
            chkavSelected.Name = GXCCtl;
            chkavSelected.WebTags = "";
            chkavSelected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSelected_Internalname, "TitleCaption", chkavSelected.Caption, !bGXsfl_167_Refreshing);
            chkavSelected.CheckedValue = "false";
            GXCCtl = "PERSONATIPO_" + sGXsfl_167_idx;
            cmbPersonaTipo.Name = GXCCtl;
            cmbPersonaTipo.WebTags = "";
            cmbPersonaTipo.addItem("1", "COACH", 0);
            cmbPersonaTipo.addItem("2", "Paciente", 0);
            if ( cmbPersonaTipo.ItemCount > 0 )
            {
               A8PersonaTipo = (short)(NumberUtil.Val( cmbPersonaTipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0))), "."));
            }
            GXCCtl = "PERSONAPACIENTEORIGENSORDERA_" + sGXsfl_167_idx;
            cmbPersonaPacienteOrigenSordera.Name = GXCCtl;
            cmbPersonaPacienteOrigenSordera.WebTags = "";
            cmbPersonaPacienteOrigenSordera.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Ninguno)", 0);
            cmbPersonaPacienteOrigenSordera.addItem("1", "Congenito", 0);
            cmbPersonaPacienteOrigenSordera.addItem("2", "Accidental", 0);
            cmbPersonaPacienteOrigenSordera.addItem("3", "Otros", 0);
            if ( cmbPersonaPacienteOrigenSordera.ItemCount > 0 )
            {
               A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cmbPersonaPacienteOrigenSordera.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0))), "."));
               n10PersonaPacienteOrigenSordera = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPersonapnombre_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_1672( ) ;
         while ( nGXsfl_167_idx <= nRC_GXsfl_167 )
         {
            sendrow_1672( ) ;
            nGXsfl_167_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_idx+1));
            sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
            SubsflControlProps_1672( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( GridContainer));
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( int subGrid_Rows ,
                                       String AV8PersonaPNombre ,
                                       DateTime AV10PersonaFechaNacimiento_From ,
                                       DateTime AV11PersonaFechaNacimiento_To ,
                                       short AV12OrderedBy ,
                                       String AV61Pgmname ,
                                       int AV44CurrentPage ,
                                       long AV41GridSettingsRowsPerPageVariable ,
                                       GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> AV14GridColumns ,
                                       String AV18AttributeName ,
                                       String AV19ColumnTitle ,
                                       GXBaseCollection<SdtWWPersonaSDT_WWPersonaSDTItem> AV37AllSelectedRows ,
                                       short AV38SelectedRowsAmount )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         GRID_nCurrentRecord = 0;
         RF1E2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAID", GetSecureSignedToken( "", A1PersonaID, context));
         GxWebStd.gx_hidden_field( context, "PERSONAID", A1PersonaID.ToString());
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAPNOMBRE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A2PersonaPNombre, "")), context));
         GxWebStd.gx_hidden_field( context, "PERSONAPNOMBRE", A2PersonaPNombre);
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONASNOMBRE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A3PersonaSNombre, "")), context));
         GxWebStd.gx_hidden_field( context, "PERSONASNOMBRE", A3PersonaSNombre);
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAPAPELLIDO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A4PersonaPApellido, "")), context));
         GxWebStd.gx_hidden_field( context, "PERSONAPAPELLIDO", A4PersonaPApellido);
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONASAPELLIDO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A5PersonaSApellido, "")), context));
         GxWebStd.gx_hidden_field( context, "PERSONASAPELLIDO", A5PersonaSApellido);
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONASEXO", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A6PersonaSexo, "")), context));
         GxWebStd.gx_hidden_field( context, "PERSONASEXO", StringUtil.RTrim( A6PersonaSexo));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONADPI", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( A7PersonaDPI, "")), context));
         GxWebStd.gx_hidden_field( context, "PERSONADPI", A7PersonaDPI);
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONATIPO", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A8PersonaTipo), "ZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "PERSONATIPO", StringUtil.LTrim( StringUtil.NToC( (decimal)(A8PersonaTipo), 4, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAFECHANACIMIENTO", GetSecureSignedToken( "", A9PersonaFechaNacimiento, context));
         GxWebStd.gx_hidden_field( context, "PERSONAFECHANACIMIENTO", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAPACIENTEORIGENSORDERA", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(A10PersonaPacienteOrigenSordera), "ZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "PERSONAPACIENTEORIGENSORDERA", StringUtil.LTrim( StringUtil.NToC( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0, ".", "")));
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavGridsettingsorderedby.ItemCount > 0 )
         {
            AV13GridSettingsOrderedBy = (short)(NumberUtil.Val( cmbavGridsettingsorderedby.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13GridSettingsOrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavGridsettingsorderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsorderedby_Internalname, "Values", cmbavGridsettingsorderedby.ToJavascriptSource(), true);
         }
         if ( cmbavGridsettingsrowsperpagevariable.ItemCount > 0 )
         {
            AV41GridSettingsRowsPerPageVariable = (long)(NumberUtil.Val( cmbavGridsettingsrowsperpagevariable.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridSettingsRowsPerPageVariable", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavGridsettingsrowsperpagevariable.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpagevariable_Internalname, "Values", cmbavGridsettingsrowsperpagevariable.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         /* Execute user event: Refresh */
         E241E2 ();
         RF1E2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV61Pgmname = "WWPersona";
         context.Gx_err = 0;
         edtavConfirmmessage_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConfirmmessage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConfirmmessage_Enabled), 5, 0)), true);
      }

      protected void RF1E2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 167;
         E251E2 ();
         nGXsfl_167_idx = 1;
         sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
         SubsflControlProps_1672( ) ;
         bGXsfl_167_Refreshing = true;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid_WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_1672( ) ;
            GXPagingFrom2 = (int)(((subGrid_Rows==0) ? 0 : GRID_nFirstRecordOnPage));
            GXPagingTo2 = ((subGrid_Rows==0) ? 10000 : subGrid_Recordsperpage( )+1);
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV8PersonaPNombre ,
                                                 AV11PersonaFechaNacimiento_To ,
                                                 AV10PersonaFechaNacimiento_From ,
                                                 A2PersonaPNombre ,
                                                 A9PersonaFechaNacimiento ,
                                                 AV12OrderedBy } ,
                                                 new int[]{
                                                 TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                                 }
            } ) ;
            lV8PersonaPNombre = StringUtil.Concat( StringUtil.RTrim( AV8PersonaPNombre), "%", "");
            /* Using cursor H001E2 */
            pr_default.execute(0, new Object[] {lV8PersonaPNombre, AV11PersonaFechaNacimiento_To, AV10PersonaFechaNacimiento_From, GXPagingFrom2, GXPagingTo2, GXPagingTo2});
            nGXsfl_167_idx = 1;
            sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
            SubsflControlProps_1672( ) ;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( subGrid_Rows == 0 ) || ( GRID_nCurrentRecord < subGrid_Recordsperpage( ) ) ) ) )
            {
               A10PersonaPacienteOrigenSordera = H001E2_A10PersonaPacienteOrigenSordera[0];
               n10PersonaPacienteOrigenSordera = H001E2_n10PersonaPacienteOrigenSordera[0];
               A9PersonaFechaNacimiento = H001E2_A9PersonaFechaNacimiento[0];
               n9PersonaFechaNacimiento = H001E2_n9PersonaFechaNacimiento[0];
               A8PersonaTipo = H001E2_A8PersonaTipo[0];
               A7PersonaDPI = H001E2_A7PersonaDPI[0];
               n7PersonaDPI = H001E2_n7PersonaDPI[0];
               A6PersonaSexo = H001E2_A6PersonaSexo[0];
               n6PersonaSexo = H001E2_n6PersonaSexo[0];
               A5PersonaSApellido = H001E2_A5PersonaSApellido[0];
               n5PersonaSApellido = H001E2_n5PersonaSApellido[0];
               A4PersonaPApellido = H001E2_A4PersonaPApellido[0];
               A3PersonaSNombre = H001E2_A3PersonaSNombre[0];
               n3PersonaSNombre = H001E2_n3PersonaSNombre[0];
               A2PersonaPNombre = H001E2_A2PersonaPNombre[0];
               A1PersonaID = (Guid)((Guid)(H001E2_A1PersonaID[0]));
               E261E2 ();
               pr_default.readNext(0);
            }
            GRID_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 167;
            WB1E0( ) ;
         }
         bGXsfl_167_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes1E2( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAID"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, A1PersonaID, context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAPNOMBRE"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, StringUtil.RTrim( context.localUtil.Format( A2PersonaPNombre, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONASNOMBRE"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, StringUtil.RTrim( context.localUtil.Format( A3PersonaSNombre, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAPAPELLIDO"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, StringUtil.RTrim( context.localUtil.Format( A4PersonaPApellido, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONASAPELLIDO"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, StringUtil.RTrim( context.localUtil.Format( A5PersonaSApellido, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONASEXO"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, StringUtil.RTrim( context.localUtil.Format( A6PersonaSexo, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONADPI"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, StringUtil.RTrim( context.localUtil.Format( A7PersonaDPI, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONATIPO"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, context.localUtil.Format( (decimal)(A8PersonaTipo), "ZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAFECHANACIMIENTO"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, A9PersonaFechaNacimiento, context));
         GxWebStd.gx_hidden_field( context, "gxhash_PERSONAPACIENTEORIGENSORDERA"+"_"+sGXsfl_167_idx, GetSecureSignedToken( sGXsfl_167_idx, context.localUtil.Format( (decimal)(A10PersonaPacienteOrigenSordera), "ZZZ9"), context));
      }

      protected int subGrid_Pagecount( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID_nRecordCount/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected int subGrid_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV8PersonaPNombre ,
                                              AV11PersonaFechaNacimiento_To ,
                                              AV10PersonaFechaNacimiento_From ,
                                              A2PersonaPNombre ,
                                              A9PersonaFechaNacimiento ,
                                              AV12OrderedBy } ,
                                              new int[]{
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         } ) ;
         lV8PersonaPNombre = StringUtil.Concat( StringUtil.RTrim( AV8PersonaPNombre), "%", "");
         /* Using cursor H001E3 */
         pr_default.execute(1, new Object[] {lV8PersonaPNombre, AV11PersonaFechaNacimiento_To, AV10PersonaFechaNacimiento_From});
         GRID_nRecordCount = H001E3_AGRID_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID_nRecordCount) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         if ( subGrid_Rows > 0 )
         {
            return subGrid_Rows*1 ;
         }
         else
         {
            return (int)(-1) ;
         }
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID_nFirstRecordOnPage/ (decimal)(subGrid_Recordsperpage( ))))+1) ;
      }

      protected short subgrid_firstpage( )
      {
         GRID_nFirstRecordOnPage = 0;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid_nextpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( ( GRID_nRecordCount >= subGrid_Recordsperpage( ) ) && ( GRID_nEOF == 0 ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage+subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         GridContainer.AddObjectProperty("GRID_nFirstRecordOnPage", GRID_nFirstRecordOnPage);
         return (short)(((GRID_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid_previouspage( )
      {
         if ( GRID_nFirstRecordOnPage >= subGrid_Recordsperpage( ) )
         {
            GRID_nFirstRecordOnPage = (long)(GRID_nFirstRecordOnPage-subGrid_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid_lastpage( )
      {
         GRID_nRecordCount = subGrid_Recordcount( );
         if ( GRID_nRecordCount > subGrid_Recordsperpage( ) )
         {
            if ( ((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))) == 0 )
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-subGrid_Recordsperpage( ));
            }
            else
            {
               GRID_nFirstRecordOnPage = (long)(GRID_nRecordCount-((int)((GRID_nRecordCount) % (subGrid_Recordsperpage( )))));
            }
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected int subgrid_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID_nFirstRecordOnPage = (long)(subGrid_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID_nFirstRecordOnPage), 15, 0, ".", "")));
         return (int)(0) ;
      }

      protected void STRUP1E0( )
      {
         /* Before Start, stand alone formulas. */
         AV61Pgmname = "WWPersona";
         context.Gx_err = 0;
         edtavConfirmmessage_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConfirmmessage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConfirmmessage_Enabled), 5, 0)), true);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E231E2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vGRIDORDERS"), AV48GridOrders);
            /* Read variables values. */
            AV8PersonaPNombre = cgiGet( edtavPersonapnombre_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8PersonaPNombre", AV8PersonaPNombre);
            if ( context.localUtil.VCDateTime( cgiGet( edtavPersonafechanacimiento_from_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Persona Fecha Nacimiento_From"}), 1, "vPERSONAFECHANACIMIENTO_FROM");
               GX_FocusControl = edtavPersonafechanacimiento_from_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10PersonaFechaNacimiento_From = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PersonaFechaNacimiento_From", context.localUtil.Format(AV10PersonaFechaNacimiento_From, "99/99/99"));
            }
            else
            {
               AV10PersonaFechaNacimiento_From = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPersonafechanacimiento_from_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PersonaFechaNacimiento_From", context.localUtil.Format(AV10PersonaFechaNacimiento_From, "99/99/99"));
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavPersonafechanacimiento_to_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Persona Fecha Nacimiento_To"}), 1, "vPERSONAFECHANACIMIENTO_TO");
               GX_FocusControl = edtavPersonafechanacimiento_to_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV11PersonaFechaNacimiento_To = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11PersonaFechaNacimiento_To", context.localUtil.Format(AV11PersonaFechaNacimiento_To, "99/99/99"));
            }
            else
            {
               AV11PersonaFechaNacimiento_To = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavPersonafechanacimiento_to_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11PersonaFechaNacimiento_To", context.localUtil.Format(AV11PersonaFechaNacimiento_To, "99/99/99"));
            }
            AV20Att_PersonaID_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personaid_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Att_PersonaID_Visible", AV20Att_PersonaID_Visible);
            AV21Att_PersonaPNombre_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personapnombre_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Att_PersonaPNombre_Visible", AV21Att_PersonaPNombre_Visible);
            AV22Att_PersonaSNombre_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personasnombre_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Att_PersonaSNombre_Visible", AV22Att_PersonaSNombre_Visible);
            AV23Att_PersonaPApellido_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personapapellido_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Att_PersonaPApellido_Visible", AV23Att_PersonaPApellido_Visible);
            AV24Att_PersonaSApellido_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personasapellido_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Att_PersonaSApellido_Visible", AV24Att_PersonaSApellido_Visible);
            AV25Att_PersonaSexo_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personasexo_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Att_PersonaSexo_Visible", AV25Att_PersonaSexo_Visible);
            AV26Att_PersonaDPI_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personadpi_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Att_PersonaDPI_Visible", AV26Att_PersonaDPI_Visible);
            AV27Att_PersonaTipo_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personatipo_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Att_PersonaTipo_Visible", AV27Att_PersonaTipo_Visible);
            AV28Att_PersonaFechaNacimiento_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personafechanacimiento_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Att_PersonaFechaNacimiento_Visible", AV28Att_PersonaFechaNacimiento_Visible);
            AV29Att_PersonaPacienteOrigenSordera_Visible = StringUtil.StrToBool( cgiGet( chkavAtt_personapacienteorigensordera_visible_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Att_PersonaPacienteOrigenSordera_Visible", AV29Att_PersonaPacienteOrigenSordera_Visible);
            cmbavGridsettingsorderedby.Name = cmbavGridsettingsorderedby_Internalname;
            cmbavGridsettingsorderedby.CurrentValue = cgiGet( cmbavGridsettingsorderedby_Internalname);
            AV13GridSettingsOrderedBy = (short)(NumberUtil.Val( cgiGet( cmbavGridsettingsorderedby_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13GridSettingsOrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0)));
            cmbavGridsettingsrowsperpagevariable.Name = cmbavGridsettingsrowsperpagevariable_Internalname;
            cmbavGridsettingsrowsperpagevariable.CurrentValue = cgiGet( cmbavGridsettingsrowsperpagevariable_Internalname);
            AV41GridSettingsRowsPerPageVariable = (long)(NumberUtil.Val( cgiGet( cmbavGridsettingsrowsperpagevariable_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridSettingsRowsPerPageVariable", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0)));
            AV51CheckAll = StringUtil.StrToBool( cgiGet( chkavCheckall_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51CheckAll", AV51CheckAll);
            AV53ConfirmMessage = cgiGet( edtavConfirmmessage_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ConfirmMessage", AV53ConfirmMessage);
            /* Read saved values. */
            nRC_GXsfl_167 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_167"), ",", "."));
            AV50UC_OrderedBy = (short)(context.localUtil.CToN( cgiGet( "vUC_ORDEREDBY"), ",", "."));
            AV52ConfirmationSubId = cgiGet( "vCONFIRMATIONSUBID");
            AV12OrderedBy = (short)(context.localUtil.CToN( cgiGet( "vORDEREDBY"), ",", "."));
            GRID_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID_nFirstRecordOnPage"), ",", "."));
            GRID_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID_nEOF"), ",", "."));
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            K2borderbyusercontrol_Gridcontrolname = cgiGet( "K2BORDERBYUSERCONTROL_Gridcontrolname");
            subGrid_Rows = (int)(context.localUtil.CToN( cgiGet( "GRID_Rows"), ",", "."));
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrCmp(cgiGet( "GXH_vPERSONAPNOMBRE"), AV8PersonaPNombre) != 0 )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPERSONAFECHANACIMIENTO_FROM"), 0) != AV10PersonaFechaNacimiento_From )
            {
               GRID_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vPERSONAFECHANACIMIENTO_TO"), 0) != AV11PersonaFechaNacimiento_To )
            {
               GRID_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E231E2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E231E2( )
      {
         /* Start Routine */
         new k2bgetcontext(context ).execute( out  AV5Context) ;
         AV10PersonaFechaNacimiento_From = DateTimeUtil.DAdd(DateTimeUtil.ServerDate( context, pr_default),-((int)(30)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PersonaFechaNacimiento_From", context.localUtil.Format(AV10PersonaFechaNacimiento_From, "99/99/99"));
         AV11PersonaFechaNacimiento_To = DateTimeUtil.ResetTime(DateTimeUtil.ServerNow( context, pr_default));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11PersonaFechaNacimiento_To", context.localUtil.Format(AV11PersonaFechaNacimiento_To, "99/99/99"));
         subGrid_Rows = 20;
         GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
         AV41GridSettingsRowsPerPageVariable = 20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridSettingsRowsPerPageVariable", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0)));
         Form.Caption = "Personas";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption, true);
         /* Execute user subroutine: 'LOADGRIDSTATE' */
         S112 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         K2borderbyusercontrol_Gridcontrolname = subGrid_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_uc_prop("", false, K2borderbyusercontrol_Internalname, "GridControlName", K2borderbyusercontrol_Gridcontrolname);
         AV48GridOrders = new GXBaseCollection<SdtK2BGridOrders_K2BGridOrdersItem>( context, "K2BGridOrdersItem", "PACYE2");
         AV49GridOrder = new SdtK2BGridOrders_K2BGridOrdersItem(context);
         AV49GridOrder.gxTpr_Ascendingorder = 0;
         AV49GridOrder.gxTpr_Descendingorder = 1;
         AV49GridOrder.gxTpr_Gridcolumnindex = 1;
         AV48GridOrders.Add(AV49GridOrder, 0);
         AV49GridOrder = new SdtK2BGridOrders_K2BGridOrdersItem(context);
         AV49GridOrder.gxTpr_Ascendingorder = 2;
         AV49GridOrder.gxTpr_Descendingorder = 3;
         AV49GridOrder.gxTpr_Gridcolumnindex = 2;
         AV48GridOrders.Add(AV49GridOrder, 0);
      }

      protected void E241E2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         GXt_objcol_SdtMessages_Message1 = AV39Messages;
         new k2btoolsmessagequeuegetallmessages(context ).execute( out  GXt_objcol_SdtMessages_Message1) ;
         AV39Messages = GXt_objcol_SdtMessages_Message1;
         AV60GXV1 = 1;
         while ( AV60GXV1 <= AV39Messages.Count )
         {
            AV40Message = ((SdtMessages_Message)AV39Messages.Item(AV60GXV1));
            GX_msglist.addItem(AV40Message.gxTpr_Description);
            AV60GXV1 = (int)(AV60GXV1+1);
         }
         /* Execute user subroutine: 'PREPARETRANSACTION' */
         S242 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV46ActivityList = new GXBaseCollection<SdtK2BActivityList_K2BActivityListItem>( context, "K2BActivityListItem", "PACYE2");
         AV47ActivityListItem = new SdtK2BActivityList_K2BActivityListItem(context);
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Entityname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Transactionname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Standardactivitytype = "List";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Useractivitytype = "";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Pgmname = AV61Pgmname;
         AV46ActivityList.Add(AV47ActivityListItem, 0);
         new k2bisauthorizedactivitylist(context ).execute( ref  AV46ActivityList) ;
         if ( ! ((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Isauthorized )
         {
            CallWebObject(formatLink("k2bnotauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim(((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Activity.gxTpr_Entityname)) + "," + UrlEncode(StringUtil.RTrim(((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Activity.gxTpr_Transactionname)) + "," + UrlEncode(StringUtil.RTrim(((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Activity.gxTpr_Standardactivitytype)) + "," + UrlEncode(StringUtil.RTrim(((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Activity.gxTpr_Useractivitytype)) + "," + UrlEncode(StringUtil.RTrim(((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Activity.gxTpr_Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         new k2bgetcontext(context ).execute( out  AV5Context) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         imgReport_Tooltiptext = "PDF Report";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgReport_Internalname, "Tooltiptext", imgReport_Tooltiptext, true);
         imgExport_Tooltiptext = "Export";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExport_Internalname, "Tooltiptext", imgExport_Tooltiptext, true);
         bttInsert_Tooltiptext = "Agregar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttInsert_Internalname, "Tooltiptext", bttInsert_Tooltiptext, true);
         imgUpdate_Tooltiptext = "Modificar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdate_Internalname, "Tooltiptext", imgUpdate_Tooltiptext, true);
         imgDelete_Tooltiptext = "Eliminar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgDelete_Internalname, "Tooltiptext", imgDelete_Tooltiptext, true);
         tblTableconditionalconfirm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTableconditionalconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTableconditionalconfirm_Visible), 5, 0)), true);
         /* Execute user subroutine: 'REFRESHGLOBALRELATEDACTIONS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'HIDESINGLEACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'HIDEMULTIPLEPAGERELATEDACTIONS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV38SelectedRowsAmount = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38SelectedRowsAmount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38SelectedRowsAmount), 4, 0)));
         /* Execute user subroutine: 'HIDESHOWCOLUMNS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subGrid_Backcolorstyle = 3;
         divK2bgridsettingscontentoutertable_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divK2bgridsettingscontentoutertable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divK2bgridsettingscontentoutertable_Visible), 5, 0)), true);
         /* Execute user subroutine: 'DISPLAYPAGINGBUTTONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14GridColumns", AV14GridColumns);
      }

      protected void E251E2( )
      {
         /* Grid_Refresh Routine */
         new k2bgetcontext(context ).execute( out  AV5Context) ;
         imgReport_Tooltiptext = "PDF Report";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgReport_Internalname, "Tooltiptext", imgReport_Tooltiptext, true);
         imgExport_Tooltiptext = "Export";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExport_Internalname, "Tooltiptext", imgExport_Tooltiptext, true);
         bttInsert_Tooltiptext = "Agregar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttInsert_Internalname, "Tooltiptext", bttInsert_Tooltiptext, true);
         imgUpdate_Tooltiptext = "Modificar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdate_Internalname, "Tooltiptext", imgUpdate_Tooltiptext, true);
         imgDelete_Tooltiptext = "Eliminar";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgDelete_Internalname, "Tooltiptext", imgDelete_Tooltiptext, true);
         tblTableconditionalconfirm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTableconditionalconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTableconditionalconfirm_Visible), 5, 0)), true);
         /* Execute user subroutine: 'REFRESHGLOBALRELATEDACTIONS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'HIDESINGLEACTIONS' */
         S142 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'HIDEMULTIPLEPAGERELATEDACTIONS' */
         S152 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV38SelectedRowsAmount = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38SelectedRowsAmount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38SelectedRowsAmount), 4, 0)));
         /* Execute user subroutine: 'DISPLAYPAGINGBUTTONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         tblNoresultsfoundtable_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblNoresultsfoundtable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblNoresultsfoundtable_Visible), 5, 0)), true);
         AV50UC_OrderedBy = AV12OrderedBy;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UC_OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50UC_OrderedBy), 4, 0)));
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'HIDESHOWCOLUMNS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         subGrid_Backcolorstyle = 3;
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14GridColumns", AV14GridColumns);
      }

      private void E261E2( )
      {
         /* Grid_Load Routine */
         tblNoresultsfoundtable_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblNoresultsfoundtable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblNoresultsfoundtable_Visible), 5, 0)), true);
         AV33Selected = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV33Selected);
         AV62GXV2 = 1;
         while ( AV62GXV2 <= AV37AllSelectedRows.Count )
         {
            AV35SelectedRow = ((SdtWWPersonaSDT_WWPersonaSDTItem)AV37AllSelectedRows.Item(AV62GXV2));
            if ( AV35SelectedRow.gxTpr_Personaid == A1PersonaID )
            {
               AV33Selected = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV33Selected);
               AV38SelectedRowsAmount = (short)(AV38SelectedRowsAmount+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38SelectedRowsAmount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38SelectedRowsAmount), 4, 0)));
               if ( AV38SelectedRowsAmount == 1 )
               {
                  /* Execute user subroutine: 'DISPLAYSINGLEACTIONS' */
                  S182 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
                  /* Execute user subroutine: 'DISPLAYMULTIPLEROWSELECTEDACTIONS' */
                  S192 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               if ( AV38SelectedRowsAmount == 2 )
               {
                  /* Execute user subroutine: 'HIDESINGLEACTIONS' */
                  S142 ();
                  if ( returnInSub )
                  {
                     returnInSub = true;
                     if (true) return;
                  }
               }
               if (true) break;
            }
            AV62GXV2 = (int)(AV62GXV2+1);
         }
         /* Load Method */
         if ( wbStart != -1 )
         {
            wbStart = 167;
         }
         sendrow_1672( ) ;
         GRID_nCurrentRecord = (long)(GRID_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ! bGXsfl_167_Refreshing )
         {
            context.DoAjaxLoad(167, GridRow);
         }
         /*  Sending Event outputs  */
      }

      protected void S112( )
      {
         /* 'LOADGRIDSTATE' Routine */
         if ( StringUtil.StrCmp(AV7HTTPRequest.Method, "GET") == 0 )
         {
            AV30GridStateKey = "";
            new k2bloadgridstate(context ).execute(  AV61Pgmname,  AV30GridStateKey, out  AV31GridState) ;
            AV12OrderedBy = AV31GridState.gxTpr_Orderedby;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
            AV13GridSettingsOrderedBy = AV31GridState.gxTpr_Orderedby;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13GridSettingsOrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0)));
            AV50UC_OrderedBy = AV31GridState.gxTpr_Orderedby;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UC_OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50UC_OrderedBy), 4, 0)));
            AV63GXV3 = 1;
            while ( AV63GXV3 <= AV31GridState.gxTpr_Filtervalues.Count )
            {
               AV32GridStateFilterValue = ((SdtK2BGridState_FilterValue)AV31GridState.gxTpr_Filtervalues.Item(AV63GXV3));
               if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Filtername, "PersonaPNombre") == 0 )
               {
                  AV8PersonaPNombre = AV32GridStateFilterValue.gxTpr_Value;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8PersonaPNombre", AV8PersonaPNombre);
               }
               else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Filtername, "PersonaFechaNacimiento:From") == 0 )
               {
                  AV10PersonaFechaNacimiento_From = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10PersonaFechaNacimiento_From", context.localUtil.Format(AV10PersonaFechaNacimiento_From, "99/99/99"));
               }
               else if ( StringUtil.StrCmp(AV32GridStateFilterValue.gxTpr_Filtername, "PersonaFechaNacimiento:To") == 0 )
               {
                  AV11PersonaFechaNacimiento_To = context.localUtil.CToD( AV32GridStateFilterValue.gxTpr_Value, 2);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11PersonaFechaNacimiento_To", context.localUtil.Format(AV11PersonaFechaNacimiento_To, "99/99/99"));
               }
               AV63GXV3 = (int)(AV63GXV3+1);
            }
            if ( ( AV31GridState.gxTpr_Currentpage > 0 ) && ( AV31GridState.gxTpr_Currentpage <= subGrid_Pagecount( ) ) )
            {
               AV44CurrentPage = AV31GridState.gxTpr_Currentpage;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
               subgrid_gotopage( AV44CurrentPage) ;
            }
            if ( AV31GridState.gxTpr_Rowsperpage > 0 )
            {
               AV41GridSettingsRowsPerPageVariable = AV31GridState.gxTpr_Rowsperpage;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41GridSettingsRowsPerPageVariable", StringUtil.LTrim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0)));
               subGrid_Rows = AV31GridState.gxTpr_Rowsperpage;
               GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            }
         }
      }

      protected void S122( )
      {
         /* 'SAVEGRIDSTATE' Routine */
         AV30GridStateKey = "";
         new k2bloadgridstate(context ).execute(  AV61Pgmname,  AV30GridStateKey, out  AV31GridState) ;
         AV31GridState.gxTpr_Currentpage = (short)(AV44CurrentPage);
         AV31GridState.gxTpr_Orderedby = AV12OrderedBy;
         AV31GridState.gxTpr_Rowsperpage = (short)(AV41GridSettingsRowsPerPageVariable);
         AV31GridState.gxTpr_Filtervalues.Clear();
         AV32GridStateFilterValue = new SdtK2BGridState_FilterValue(context);
         AV32GridStateFilterValue.gxTpr_Filtername = "PersonaPNombre";
         AV32GridStateFilterValue.gxTpr_Value = StringUtil.RTrim( context.localUtil.Format( AV8PersonaPNombre, ""));
         AV31GridState.gxTpr_Filtervalues.Add(AV32GridStateFilterValue, 0);
         AV32GridStateFilterValue = new SdtK2BGridState_FilterValue(context);
         AV32GridStateFilterValue.gxTpr_Filtername = "PersonaFechaNacimiento:From";
         AV32GridStateFilterValue.gxTpr_Value = context.localUtil.Format( AV10PersonaFechaNacimiento_From, "99/99/99");
         AV31GridState.gxTpr_Filtervalues.Add(AV32GridStateFilterValue, 0);
         AV32GridStateFilterValue = new SdtK2BGridState_FilterValue(context);
         AV32GridStateFilterValue.gxTpr_Filtername = "PersonaFechaNacimiento:To";
         AV32GridStateFilterValue.gxTpr_Value = context.localUtil.Format( AV11PersonaFechaNacimiento_To, "99/99/99");
         AV31GridState.gxTpr_Filtervalues.Add(AV32GridStateFilterValue, 0);
         new k2bsavegridstate(context ).execute(  AV61Pgmname,  AV30GridStateKey,  AV31GridState) ;
      }

      protected void S242( )
      {
         /* 'PREPARETRANSACTION' Routine */
         AV42TrnContext = new SdtK2BTrnContext(context);
         AV42TrnContext.gxTpr_Callerurl = AV7HTTPRequest.ScriptName+"?"+AV7HTTPRequest.QueryString;
         AV42TrnContext.gxTpr_Returnmode = "Stack";
         AV42TrnContext.gxTpr_Afterinsert = new SdtK2BTrnNavigation(context);
         AV42TrnContext.gxTpr_Afterinsert.gxTpr_Aftertrn = 2;
         AV42TrnContext.gxTpr_Afterupdate = new SdtK2BTrnNavigation(context);
         AV42TrnContext.gxTpr_Afterupdate.gxTpr_Aftertrn = 1;
         AV42TrnContext.gxTpr_Afterdelete = new SdtK2BTrnNavigation(context);
         AV42TrnContext.gxTpr_Afterdelete.gxTpr_Aftertrn = 1;
         new k2bsettrncontextbyname(context ).execute(  "Persona",  AV42TrnContext) ;
      }

      protected void S172( )
      {
         /* 'DISPLAYPAGINGBUTTONS' Routine */
         AV45K2BMaxPages = subGrid_Pagecount( );
         if ( AV45K2BMaxPages == 0 )
         {
            AV44CurrentPage = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
         }
         else
         {
            AV44CurrentPage = subGrid_Currentpage( );
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
         }
         lblFirstpagetextblock_Caption = "1";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblFirstpagetextblock_Internalname, "Caption", lblFirstpagetextblock_Caption, true);
         lblPreviouspagetextblock_Caption = StringUtil.Str( (decimal)(AV44CurrentPage-1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPreviouspagetextblock_Internalname, "Caption", lblPreviouspagetextblock_Caption, true);
         lblCurrentpagetextblock_Caption = StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblCurrentpagetextblock_Internalname, "Caption", lblCurrentpagetextblock_Caption, true);
         lblNextpagetextblock_Caption = StringUtil.Str( (decimal)(AV44CurrentPage+1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNextpagetextblock_Internalname, "Caption", lblNextpagetextblock_Caption, true);
         lblLastpagetextblock_Caption = StringUtil.Str( (decimal)(AV45K2BMaxPages), 6, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLastpagetextblock_Internalname, "Caption", lblLastpagetextblock_Caption, true);
         lblPreviouspagebuttontextblock_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPreviouspagebuttontextblock_Internalname, "Class", lblPreviouspagebuttontextblock_Class, true);
         lblNextpagebuttontextblock_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNextpagebuttontextblock_Internalname, "Class", lblNextpagebuttontextblock_Class, true);
         if ( (0==AV44CurrentPage) || ( AV44CurrentPage <= 1 ) )
         {
            cellFirstpagecell_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellFirstpagecell_Internalname, "Class", cellFirstpagecell_Class, true);
            lblPreviouspagebuttontextblock_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPreviouspagebuttontextblock_Internalname, "Class", lblPreviouspagebuttontextblock_Class, true);
            lblFirstpagetextblock_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblFirstpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblFirstpagetextblock_Visible), 5, 0)), true);
            cellSpacingleftcell_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingleftcell_Internalname, "Class", cellSpacingleftcell_Class, true);
            lblSpacinglefttextblock_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacinglefttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacinglefttextblock_Visible), 5, 0)), true);
            cellPreviouspagecell_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPreviouspagecell_Internalname, "Class", cellPreviouspagecell_Class, true);
            lblPreviouspagetextblock_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPreviouspagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPreviouspagetextblock_Visible), 5, 0)), true);
         }
         else
         {
            cellPreviouspagecell_Class = "K2BToolsCell_PaginationPrevious";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPreviouspagecell_Internalname, "Class", cellPreviouspagecell_Class, true);
            lblPreviouspagetextblock_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPreviouspagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPreviouspagetextblock_Visible), 5, 0)), true);
            if ( AV44CurrentPage == 2 )
            {
               cellFirstpagecell_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellFirstpagecell_Internalname, "Class", cellFirstpagecell_Class, true);
               lblFirstpagetextblock_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblFirstpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblFirstpagetextblock_Visible), 5, 0)), true);
               cellSpacingleftcell_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingleftcell_Internalname, "Class", cellSpacingleftcell_Class, true);
               lblSpacinglefttextblock_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacinglefttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacinglefttextblock_Visible), 5, 0)), true);
            }
            else
            {
               cellFirstpagecell_Class = "K2BToolsCell_PaginationLeft";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellFirstpagecell_Internalname, "Class", cellFirstpagecell_Class, true);
               lblFirstpagetextblock_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblFirstpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblFirstpagetextblock_Visible), 5, 0)), true);
               if ( AV44CurrentPage == 3 )
               {
                  cellSpacingleftcell_Class = "K2BToolsCell_PaginationInvisible";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingleftcell_Internalname, "Class", cellSpacingleftcell_Class, true);
                  lblSpacinglefttextblock_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacinglefttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacinglefttextblock_Visible), 5, 0)), true);
               }
               else
               {
                  cellSpacingleftcell_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingleftcell_Internalname, "Class", cellSpacingleftcell_Class, true);
                  lblSpacinglefttextblock_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacinglefttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacinglefttextblock_Visible), 5, 0)), true);
               }
            }
         }
         if ( AV44CurrentPage == AV45K2BMaxPages )
         {
            cellLastpagecell_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellLastpagecell_Internalname, "Class", cellLastpagecell_Class, true);
            lblNextpagebuttontextblock_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNextpagebuttontextblock_Internalname, "Class", lblNextpagebuttontextblock_Class, true);
            lblLastpagetextblock_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLastpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblLastpagetextblock_Visible), 5, 0)), true);
            cellSpacingrightcell_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingrightcell_Internalname, "Class", cellSpacingrightcell_Class, true);
            lblSpacingrighttextblock_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacingrighttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacingrighttextblock_Visible), 5, 0)), true);
            cellNextpagecell_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellNextpagecell_Internalname, "Class", cellNextpagecell_Class, true);
            lblNextpagetextblock_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNextpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblNextpagetextblock_Visible), 5, 0)), true);
         }
         else
         {
            cellNextpagecell_Class = "K2BToolsCell_PaginationNext";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellNextpagecell_Internalname, "Class", cellNextpagecell_Class, true);
            lblNextpagetextblock_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblNextpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblNextpagetextblock_Visible), 5, 0)), true);
            if ( AV44CurrentPage == AV45K2BMaxPages - 1 )
            {
               cellLastpagecell_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellLastpagecell_Internalname, "Class", cellLastpagecell_Class, true);
               lblLastpagetextblock_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLastpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblLastpagetextblock_Visible), 5, 0)), true);
               cellSpacingrightcell_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingrightcell_Internalname, "Class", cellSpacingrightcell_Class, true);
               lblSpacingrighttextblock_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacingrighttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacingrighttextblock_Visible), 5, 0)), true);
            }
            else
            {
               cellLastpagecell_Class = "K2BToolsCell_PaginationRight";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellLastpagecell_Internalname, "Class", cellLastpagecell_Class, true);
               lblLastpagetextblock_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblLastpagetextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblLastpagetextblock_Visible), 5, 0)), true);
               if ( AV44CurrentPage == AV45K2BMaxPages - 2 )
               {
                  cellSpacingrightcell_Class = "K2BToolsCell_PaginationInvisible";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingrightcell_Internalname, "Class", cellSpacingrightcell_Class, true);
                  lblSpacingrighttextblock_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacingrighttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacingrighttextblock_Visible), 5, 0)), true);
               }
               else
               {
                  cellSpacingrightcell_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellSpacingrightcell_Internalname, "Class", cellSpacingrightcell_Class, true);
                  lblSpacingrighttextblock_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblSpacingrighttextblock_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblSpacingrighttextblock_Visible), 5, 0)), true);
               }
            }
         }
         if ( ( AV44CurrentPage <= 1 ) && ( AV45K2BMaxPages <= 1 ) )
         {
            tblK2btoolspagingcontainertable_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblK2btoolspagingcontainertable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblK2btoolspagingcontainertable_Visible), 5, 0)), true);
         }
         else
         {
            tblK2btoolspagingcontainertable_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblK2btoolspagingcontainertable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblK2btoolspagingcontainertable_Visible), 5, 0)), true);
         }
      }

      protected void E141E2( )
      {
         /* 'DoFirst' Routine */
         AV44CurrentPage = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
         subgrid_firstpage( ) ;
         /* Execute user subroutine: 'DISPLAYPAGINGBUTTONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /*  Sending Event outputs  */
      }

      protected void E151E2( )
      {
         /* 'DoPrevious' Routine */
         if ( AV44CurrentPage > 1 )
         {
            AV44CurrentPage = (int)(AV44CurrentPage-1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
            subgrid_previouspage( ) ;
            /* Execute user subroutine: 'DISPLAYPAGINGBUTTONS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /*  Sending Event outputs  */
      }

      protected void E161E2( )
      {
         /* 'DoNext' Routine */
         if ( AV44CurrentPage != subGrid_Pagecount( ) )
         {
            AV44CurrentPage = (int)(AV44CurrentPage+1);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
            subgrid_nextpage( ) ;
            /* Execute user subroutine: 'DISPLAYPAGINGBUTTONS' */
            S172 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /*  Sending Event outputs  */
      }

      protected void E171E2( )
      {
         /* 'DoLast' Routine */
         AV44CurrentPage = subGrid_Pagecount( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
         subgrid_lastpage( ) ;
         /* Execute user subroutine: 'DISPLAYPAGINGBUTTONS' */
         S172 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /*  Sending Event outputs  */
      }

      protected void E111E2( )
      {
         /* 'DoInsert' Routine */
         if ( new k2bisauthorizedactivityname(context).executeUdp(  "Persona",  "Persona",  "Insert",  "",  "EntityManagerPersona") )
         {
            CallWebObject(formatLink("entitymanagerpersona.aspx") + "?" + UrlEncode(StringUtil.RTrim("INS")) + "," + UrlEncode(Guid.Empty.ToString()) + "," + UrlEncode(StringUtil.RTrim("")));
            context.wjLocDisableFrm = 1;
         }
      }

      protected void E181E2( )
      {
         /* 'DoUpdate' Routine */
         if ( new k2bisauthorizedactivityname(context).executeUdp(  "Persona",  "Persona",  "Update",  "",  "EntityManagerPersona") )
         {
            /* Execute user subroutine: 'LOADSELECTEDROWS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV34SelectedRows.Count < 1 )
            {
               GX_msglist.addItem("Error: Debe seleccionar alguna l�nea");
            }
            else
            {
               if ( AV34SelectedRows.Count <= 1 )
               {
                  CallWebObject(formatLink("entitymanagerpersona.aspx") + "?" + UrlEncode(StringUtil.RTrim("UPD")) + "," + UrlEncode(((SdtWWPersonaSDT_WWPersonaSDTItem)AV34SelectedRows.Item(1)).gxTpr_Personaid.ToString()) + "," + UrlEncode(StringUtil.RTrim("")));
                  context.wjLocDisableFrm = 1;
               }
               else
               {
                  GX_msglist.addItem("Error: Debe seleccionar solo una l�nea");
               }
            }
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34SelectedRows", AV34SelectedRows);
      }

      protected void E191E2( )
      {
         /* 'DoDelete' Routine */
         if ( new k2bisauthorizedactivityname(context).executeUdp(  "Persona",  "Persona",  "Delete",  "",  "") )
         {
            /* Execute user subroutine: 'LOADSELECTEDROWS' */
            S202 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV34SelectedRows.Count > 0 )
            {
               if ( AV34SelectedRows.Count == 1 )
               {
                  CallWebObject(formatLink("entitymanagerpersona.aspx") + "?" + UrlEncode(StringUtil.RTrim("DLT")) + "," + UrlEncode(((SdtWWPersonaSDT_WWPersonaSDTItem)AV34SelectedRows.Item(1)).gxTpr_Personaid.ToString()) + "," + UrlEncode(StringUtil.RTrim("")));
                  context.wjLocDisableFrm = 1;
               }
               else
               {
                  AV53ConfirmMessage = StringUtil.Format( "�Est� seguro que quiere eliminar %1 personas?", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34SelectedRows.Count), 9, 0)), "", "", "", "", "", "", "", "");
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV53ConfirmMessage", AV53ConfirmMessage);
                  AV52ConfirmationSubId = "DoMultipleDelete";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV52ConfirmationSubId", AV52ConfirmationSubId);
                  tblTableconditionalconfirm_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTableconditionalconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTableconditionalconfirm_Visible), 5, 0)), true);
               }
            }
            else
            {
               GX_msglist.addItem("Error: Debe seleccionar alguna l�nea");
            }
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV34SelectedRows", AV34SelectedRows);
      }

      protected void E271E2( )
      {
         /* PersonaPNombre_Click Routine */
         if ( new k2bisauthorizedactivityname(context).executeUdp(  "Persona",  "Persona",  "Display",  "",  "EntityManagerPersona") )
         {
            CallWebObject(formatLink("entitymanagerpersona.aspx") + "?" + UrlEncode(StringUtil.RTrim("DSP")) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim("")));
            context.wjLocDisableFrm = 1;
         }
      }

      protected void S202( )
      {
         /* 'LOADSELECTEDROWS' Routine */
         AV34SelectedRows = new GXBaseCollection<SdtWWPersonaSDT_WWPersonaSDTItem>( context, "WWPersonaSDTItem", "PACYE2");
         /* Start For Each Line in Grid */
         nRC_GXsfl_167 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_167"), ",", "."));
         nGXsfl_167_fel_idx = 0;
         while ( nGXsfl_167_fel_idx < nRC_GXsfl_167 )
         {
            nGXsfl_167_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_fel_idx+1));
            sGXsfl_167_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_1672( ) ;
            AV33Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
            A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
            A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
            n3PersonaSNombre = false;
            A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
            A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
            n5PersonaSApellido = false;
            A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
            n6PersonaSexo = false;
            A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
            n7PersonaDPI = false;
            cmbPersonaTipo.Name = cmbPersonaTipo_Internalname;
            cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
            A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
            A9PersonaFechaNacimiento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPersonaFechaNacimiento_Internalname), 0));
            n9PersonaFechaNacimiento = false;
            cmbPersonaPacienteOrigenSordera.Name = cmbPersonaPacienteOrigenSordera_Internalname;
            cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
            A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
            n10PersonaPacienteOrigenSordera = false;
            if ( AV33Selected )
            {
               AV35SelectedRow = new SdtWWPersonaSDT_WWPersonaSDTItem(context);
               AV35SelectedRow.gxTpr_Personaid = (Guid)(A1PersonaID);
               AV35SelectedRow.gxTpr_Personapnombre = A2PersonaPNombre;
               AV35SelectedRow.gxTpr_Personasnombre = A3PersonaSNombre;
               AV35SelectedRow.gxTpr_Personapapellido = A4PersonaPApellido;
               AV35SelectedRow.gxTpr_Personasapellido = A5PersonaSApellido;
               AV35SelectedRow.gxTpr_Personasexo = A6PersonaSexo;
               AV35SelectedRow.gxTpr_Personadpi = A7PersonaDPI;
               AV35SelectedRow.gxTpr_Personatipo = A8PersonaTipo;
               AV35SelectedRow.gxTpr_Personafechanacimiento = A9PersonaFechaNacimiento;
               AV35SelectedRow.gxTpr_Personapacienteorigensordera = A10PersonaPacienteOrigenSordera;
               AV34SelectedRows.Add(AV35SelectedRow, 0);
            }
            /* End For Each Line */
         }
         if ( nGXsfl_167_fel_idx == 0 )
         {
            nGXsfl_167_idx = 1;
            sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
            SubsflControlProps_1672( ) ;
         }
         nGXsfl_167_fel_idx = 1;
      }

      protected void E121E2( )
      {
         /* 'DoExport' Routine */
         CallWebObject(formatLink("aexportwwpersona.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV8PersonaPNombre)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV10PersonaFechaNacimiento_From)) + "," + UrlEncode(DateTimeUtil.FormatDateParm(AV11PersonaFechaNacimiento_To)) + "," + UrlEncode("" +AV12OrderedBy));
         context.wjLocDisableFrm = 0;
      }

      protected void S162( )
      {
         /* 'HIDESHOWCOLUMNS' Routine */
         new k2bloadgridcolumns(context ).execute(  AV61Pgmname,  "Grid", out  AV14GridColumns) ;
         AV18AttributeName = "PersonaID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "ID";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaID_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaID_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaID_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV20Att_PersonaID_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Att_PersonaID_Visible", AV20Att_PersonaID_Visible);
         AV18AttributeName = "PersonaPNombre";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "PNombre";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaPNombre_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaPNombre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPNombre_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV21Att_PersonaPNombre_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Att_PersonaPNombre_Visible", AV21Att_PersonaPNombre_Visible);
         AV18AttributeName = "PersonaSNombre";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "SNombre";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaSNombre_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaSNombre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSNombre_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV22Att_PersonaSNombre_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Att_PersonaSNombre_Visible", AV22Att_PersonaSNombre_Visible);
         AV18AttributeName = "PersonaPApellido";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "PApellido";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaPApellido_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaPApellido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPApellido_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV23Att_PersonaPApellido_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Att_PersonaPApellido_Visible", AV23Att_PersonaPApellido_Visible);
         AV18AttributeName = "PersonaSApellido";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "SApellido";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaSApellido_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaSApellido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSApellido_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV24Att_PersonaSApellido_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Att_PersonaSApellido_Visible", AV24Att_PersonaSApellido_Visible);
         AV18AttributeName = "PersonaSexo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "Sexo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaSexo_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaSexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSexo_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV25Att_PersonaSexo_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Att_PersonaSexo_Visible", AV25Att_PersonaSexo_Visible);
         AV18AttributeName = "PersonaDPI";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "DPI";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaDPI_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaDPI_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaDPI_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV26Att_PersonaDPI_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Att_PersonaDPI_Visible", AV26Att_PersonaDPI_Visible);
         AV18AttributeName = "PersonaTipo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "Tipo";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbPersonaTipo.Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPersonaTipo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaTipo.Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV27Att_PersonaTipo_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Att_PersonaTipo_Visible", AV27Att_PersonaTipo_Visible);
         AV18AttributeName = "PersonaFechaNacimiento";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "Fecha Nacimiento";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         edtPersonaFechaNacimiento_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaFechaNacimiento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaFechaNacimiento_Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV28Att_PersonaFechaNacimiento_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Att_PersonaFechaNacimiento_Visible", AV28Att_PersonaFechaNacimiento_Visible);
         AV18AttributeName = "PersonaPacienteOrigenSordera";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18AttributeName", AV18AttributeName);
         AV19ColumnTitle = "Origen Sordera";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ColumnTitle", AV19ColumnTitle);
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S252 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         cmbPersonaPacienteOrigenSordera.Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPersonaPacienteOrigenSordera_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaPacienteOrigenSordera.Visible), 5, 0)), !bGXsfl_167_Refreshing);
         AV29Att_PersonaPacienteOrigenSordera_Visible = true;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Att_PersonaPacienteOrigenSordera_Visible", AV29Att_PersonaPacienteOrigenSordera_Visible);
         new k2bsavegridcolumns(context ).execute(  AV61Pgmname,  "Grid",  AV14GridColumns) ;
         AV65GXV4 = 1;
         while ( AV65GXV4 <= AV14GridColumns.Count )
         {
            AV15GridColumn = ((SdtK2BGridColumns_K2BGridColumnsItem)AV14GridColumns.Item(AV65GXV4));
            if ( ! AV15GridColumn.gxTpr_Showattribute )
            {
               if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaID") == 0 )
               {
                  edtPersonaID_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaID_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaID_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV20Att_PersonaID_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Att_PersonaID_Visible", AV20Att_PersonaID_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaPNombre") == 0 )
               {
                  edtPersonaPNombre_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaPNombre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPNombre_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV21Att_PersonaPNombre_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Att_PersonaPNombre_Visible", AV21Att_PersonaPNombre_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaSNombre") == 0 )
               {
                  edtPersonaSNombre_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaSNombre_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSNombre_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV22Att_PersonaSNombre_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Att_PersonaSNombre_Visible", AV22Att_PersonaSNombre_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaPApellido") == 0 )
               {
                  edtPersonaPApellido_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaPApellido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPApellido_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV23Att_PersonaPApellido_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Att_PersonaPApellido_Visible", AV23Att_PersonaPApellido_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaSApellido") == 0 )
               {
                  edtPersonaSApellido_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaSApellido_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSApellido_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV24Att_PersonaSApellido_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24Att_PersonaSApellido_Visible", AV24Att_PersonaSApellido_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaSexo") == 0 )
               {
                  edtPersonaSexo_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaSexo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSexo_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV25Att_PersonaSexo_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Att_PersonaSexo_Visible", AV25Att_PersonaSexo_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaDPI") == 0 )
               {
                  edtPersonaDPI_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaDPI_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaDPI_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV26Att_PersonaDPI_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26Att_PersonaDPI_Visible", AV26Att_PersonaDPI_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaTipo") == 0 )
               {
                  cmbPersonaTipo.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPersonaTipo_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaTipo.Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV27Att_PersonaTipo_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27Att_PersonaTipo_Visible", AV27Att_PersonaTipo_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaFechaNacimiento") == 0 )
               {
                  edtPersonaFechaNacimiento_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtPersonaFechaNacimiento_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaFechaNacimiento_Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV28Att_PersonaFechaNacimiento_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28Att_PersonaFechaNacimiento_Visible", AV28Att_PersonaFechaNacimiento_Visible);
               }
               else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaPacienteOrigenSordera") == 0 )
               {
                  cmbPersonaPacienteOrigenSordera.Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPersonaPacienteOrigenSordera_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaPacienteOrigenSordera.Visible), 5, 0)), !bGXsfl_167_Refreshing);
                  AV29Att_PersonaPacienteOrigenSordera_Visible = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Att_PersonaPacienteOrigenSordera_Visible", AV29Att_PersonaPacienteOrigenSordera_Visible);
               }
            }
            AV65GXV4 = (int)(AV65GXV4+1);
         }
      }

      protected void S252( )
      {
         /* 'ADDCOLUMNIFNOTEXISTS' Routine */
         AV16FoundColumn = false;
         AV17CurrentColumn = 1;
         while ( ( AV17CurrentColumn <= AV14GridColumns.Count ) && ! AV16FoundColumn )
         {
            if ( StringUtil.StrCmp(((SdtK2BGridColumns_K2BGridColumnsItem)AV14GridColumns.Item(AV17CurrentColumn)).gxTpr_Attributename, AV18AttributeName) == 0 )
            {
               AV16FoundColumn = true;
            }
            AV17CurrentColumn = (short)(AV17CurrentColumn+1);
         }
         if ( ! AV16FoundColumn )
         {
            AV15GridColumn = new SdtK2BGridColumns_K2BGridColumnsItem(context);
            AV15GridColumn.gxTpr_Attributename = AV18AttributeName;
            AV15GridColumn.gxTpr_Columntitle = AV19ColumnTitle;
            AV15GridColumn.gxTpr_Showattribute = true;
            AV14GridColumns.Add(AV15GridColumn, 0);
         }
      }

      protected void S212( )
      {
         /* 'UPDATESELECTION' Routine */
         AV36Index = 1;
         while ( AV36Index <= AV37AllSelectedRows.Count )
         {
            if ( ((SdtWWPersonaSDT_WWPersonaSDTItem)AV37AllSelectedRows.Item(AV36Index)).gxTpr_Personaid == A1PersonaID )
            {
               AV37AllSelectedRows.RemoveItem(AV36Index);
            }
            else
            {
               AV36Index = (short)(AV36Index+1);
            }
         }
         if ( AV33Selected )
         {
            AV35SelectedRow = new SdtWWPersonaSDT_WWPersonaSDTItem(context);
            AV35SelectedRow.gxTpr_Personaid = (Guid)(A1PersonaID);
            AV35SelectedRow.gxTpr_Personapnombre = A2PersonaPNombre;
            AV35SelectedRow.gxTpr_Personasnombre = A3PersonaSNombre;
            AV35SelectedRow.gxTpr_Personapapellido = A4PersonaPApellido;
            AV35SelectedRow.gxTpr_Personasapellido = A5PersonaSApellido;
            AV35SelectedRow.gxTpr_Personasexo = A6PersonaSexo;
            AV35SelectedRow.gxTpr_Personadpi = A7PersonaDPI;
            AV35SelectedRow.gxTpr_Personatipo = A8PersonaTipo;
            AV35SelectedRow.gxTpr_Personafechanacimiento = A9PersonaFechaNacimiento;
            AV35SelectedRow.gxTpr_Personapacienteorigensordera = A10PersonaPacienteOrigenSordera;
            AV37AllSelectedRows.Add(AV35SelectedRow, 0);
         }
      }

      protected void S132( )
      {
         /* 'REFRESHGLOBALRELATEDACTIONS' Routine */
         /* Execute user subroutine: 'DISPLAYPERSISTENTACTIONS' */
         S262 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S222( )
      {
         /* 'REFRESHPAGERELATEDACTIONS' Routine */
         AV38SelectedRowsAmount = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38SelectedRowsAmount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38SelectedRowsAmount), 4, 0)));
         /* Start For Each Line in Grid */
         nRC_GXsfl_167 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_167"), ",", "."));
         nGXsfl_167_fel_idx = 0;
         while ( nGXsfl_167_fel_idx < nRC_GXsfl_167 )
         {
            nGXsfl_167_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_fel_idx+1));
            sGXsfl_167_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_1672( ) ;
            AV33Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
            A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
            A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
            n3PersonaSNombre = false;
            A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
            A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
            n5PersonaSApellido = false;
            A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
            n6PersonaSexo = false;
            A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
            n7PersonaDPI = false;
            cmbPersonaTipo.Name = cmbPersonaTipo_Internalname;
            cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
            A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
            A9PersonaFechaNacimiento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPersonaFechaNacimiento_Internalname), 0));
            n9PersonaFechaNacimiento = false;
            cmbPersonaPacienteOrigenSordera.Name = cmbPersonaPacienteOrigenSordera_Internalname;
            cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
            A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
            n10PersonaPacienteOrigenSordera = false;
            if ( AV33Selected )
            {
               AV38SelectedRowsAmount = (short)(AV38SelectedRowsAmount+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38SelectedRowsAmount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38SelectedRowsAmount), 4, 0)));
            }
            /* End For Each Line */
         }
         if ( nGXsfl_167_fel_idx == 0 )
         {
            nGXsfl_167_idx = 1;
            sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
            SubsflControlProps_1672( ) ;
         }
         nGXsfl_167_fel_idx = 1;
         if ( AV38SelectedRowsAmount > 0 )
         {
            /* Execute user subroutine: 'DISPLAYMULTIPLEROWSELECTEDACTIONS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV38SelectedRowsAmount == 1 )
            {
               /* Start For Each Line in Grid */
               nRC_GXsfl_167 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_167"), ",", "."));
               nGXsfl_167_fel_idx = 0;
               while ( nGXsfl_167_fel_idx < nRC_GXsfl_167 )
               {
                  nGXsfl_167_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_fel_idx+1));
                  sGXsfl_167_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_fel_idx), 4, 0)), 4, "0");
                  SubsflControlProps_fel_1672( ) ;
                  AV33Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                  A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
                  A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
                  A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
                  n3PersonaSNombre = false;
                  A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
                  A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
                  n5PersonaSApellido = false;
                  A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
                  n6PersonaSexo = false;
                  A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
                  n7PersonaDPI = false;
                  cmbPersonaTipo.Name = cmbPersonaTipo_Internalname;
                  cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
                  A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
                  A9PersonaFechaNacimiento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPersonaFechaNacimiento_Internalname), 0));
                  n9PersonaFechaNacimiento = false;
                  cmbPersonaPacienteOrigenSordera.Name = cmbPersonaPacienteOrigenSordera_Internalname;
                  cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
                  A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
                  n10PersonaPacienteOrigenSordera = false;
                  if ( AV33Selected )
                  {
                     /* Execute user subroutine: 'DISPLAYSINGLEACTIONS' */
                     S182 ();
                     if ( returnInSub )
                     {
                        returnInSub = true;
                        if (true) return;
                     }
                  }
                  /* End For Each Line */
               }
               if ( nGXsfl_167_fel_idx == 0 )
               {
                  nGXsfl_167_idx = 1;
                  sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
                  SubsflControlProps_1672( ) ;
               }
               nGXsfl_167_fel_idx = 1;
            }
            else
            {
               /* Execute user subroutine: 'HIDESINGLEACTIONS' */
               S142 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'HIDEMULTIPLEPAGERELATEDACTIONS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'HIDESINGLEACTIONS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void E281E2( )
      {
         /* Selected_Click Routine */
         /* Start For Each Line in Grid */
         nRC_GXsfl_167 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_167"), ",", "."));
         nGXsfl_167_fel_idx = 0;
         while ( nGXsfl_167_fel_idx < nRC_GXsfl_167 )
         {
            nGXsfl_167_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_fel_idx+1));
            sGXsfl_167_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_1672( ) ;
            AV33Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
            A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
            A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
            n3PersonaSNombre = false;
            A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
            A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
            n5PersonaSApellido = false;
            A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
            n6PersonaSexo = false;
            A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
            n7PersonaDPI = false;
            cmbPersonaTipo.Name = cmbPersonaTipo_Internalname;
            cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
            A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
            A9PersonaFechaNacimiento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPersonaFechaNacimiento_Internalname), 0));
            n9PersonaFechaNacimiento = false;
            cmbPersonaPacienteOrigenSordera.Name = cmbPersonaPacienteOrigenSordera_Internalname;
            cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
            A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
            n10PersonaPacienteOrigenSordera = false;
            /* Execute user subroutine: 'UPDATESELECTION' */
            S212 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* End For Each Line */
         }
         if ( nGXsfl_167_fel_idx == 0 )
         {
            nGXsfl_167_idx = 1;
            sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
            SubsflControlProps_1672( ) ;
         }
         nGXsfl_167_fel_idx = 1;
         AV51CheckAll = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51CheckAll", AV51CheckAll);
         /* Execute user subroutine: 'REFRESHGLOBALRELATEDACTIONS' */
         S132 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /* Execute user subroutine: 'REFRESHPAGERELATEDACTIONS' */
         S222 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37AllSelectedRows", AV37AllSelectedRows);
      }

      protected void E201E2( )
      {
         /* Checkall_Click Routine */
         AV38SelectedRowsAmount = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38SelectedRowsAmount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38SelectedRowsAmount), 4, 0)));
         /* Start For Each Line in Grid */
         nRC_GXsfl_167 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_167"), ",", "."));
         nGXsfl_167_fel_idx = 0;
         while ( nGXsfl_167_fel_idx < nRC_GXsfl_167 )
         {
            nGXsfl_167_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_fel_idx+1));
            sGXsfl_167_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_1672( ) ;
            AV33Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
            A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
            A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
            A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
            n3PersonaSNombre = false;
            A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
            A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
            n5PersonaSApellido = false;
            A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
            n6PersonaSexo = false;
            A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
            n7PersonaDPI = false;
            cmbPersonaTipo.Name = cmbPersonaTipo_Internalname;
            cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
            A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
            A9PersonaFechaNacimiento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPersonaFechaNacimiento_Internalname), 0));
            n9PersonaFechaNacimiento = false;
            cmbPersonaPacienteOrigenSordera.Name = cmbPersonaPacienteOrigenSordera_Internalname;
            cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
            A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
            n10PersonaPacienteOrigenSordera = false;
            if ( AV33Selected != AV51CheckAll )
            {
               AV33Selected = AV51CheckAll;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavSelected_Internalname, AV33Selected);
               /* Execute user subroutine: 'UPDATESELECTION' */
               S212 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
            if ( AV51CheckAll )
            {
               AV38SelectedRowsAmount = (short)(AV38SelectedRowsAmount+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38SelectedRowsAmount", StringUtil.LTrim( StringUtil.Str( (decimal)(AV38SelectedRowsAmount), 4, 0)));
            }
            /* End For Each Line */
         }
         if ( nGXsfl_167_fel_idx == 0 )
         {
            nGXsfl_167_idx = 1;
            sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
            SubsflControlProps_1672( ) ;
         }
         nGXsfl_167_fel_idx = 1;
         if ( AV38SelectedRowsAmount > 0 )
         {
            /* Execute user subroutine: 'DISPLAYMULTIPLEROWSELECTEDACTIONS' */
            S192 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            if ( AV38SelectedRowsAmount == 1 )
            {
               /* Start For Each Line in Grid */
               nRC_GXsfl_167 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_167"), ",", "."));
               nGXsfl_167_fel_idx = 0;
               while ( nGXsfl_167_fel_idx < nRC_GXsfl_167 )
               {
                  nGXsfl_167_fel_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_fel_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_fel_idx+1));
                  sGXsfl_167_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_fel_idx), 4, 0)), 4, "0");
                  SubsflControlProps_fel_1672( ) ;
                  AV33Selected = StringUtil.StrToBool( cgiGet( chkavSelected_Internalname));
                  A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
                  A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
                  A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
                  n3PersonaSNombre = false;
                  A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
                  A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
                  n5PersonaSApellido = false;
                  A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
                  n6PersonaSexo = false;
                  A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
                  n7PersonaDPI = false;
                  cmbPersonaTipo.Name = cmbPersonaTipo_Internalname;
                  cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
                  A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
                  A9PersonaFechaNacimiento = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtPersonaFechaNacimiento_Internalname), 0));
                  n9PersonaFechaNacimiento = false;
                  cmbPersonaPacienteOrigenSordera.Name = cmbPersonaPacienteOrigenSordera_Internalname;
                  cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
                  A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
                  n10PersonaPacienteOrigenSordera = false;
                  if ( AV33Selected )
                  {
                     /* Execute user subroutine: 'DISPLAYSINGLEACTIONS' */
                     S182 ();
                     if ( returnInSub )
                     {
                        returnInSub = true;
                        if (true) return;
                     }
                  }
                  /* End For Each Line */
               }
               if ( nGXsfl_167_fel_idx == 0 )
               {
                  nGXsfl_167_idx = 1;
                  sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
                  SubsflControlProps_1672( ) ;
               }
               nGXsfl_167_fel_idx = 1;
            }
            else
            {
               /* Execute user subroutine: 'HIDESINGLEACTIONS' */
               S142 ();
               if ( returnInSub )
               {
                  returnInSub = true;
                  if (true) return;
               }
            }
         }
         else
         {
            /* Execute user subroutine: 'HIDEMULTIPLEPAGERELATEDACTIONS' */
            S152 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
            /* Execute user subroutine: 'HIDESINGLEACTIONS' */
            S142 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV37AllSelectedRows", AV37AllSelectedRows);
      }

      protected void S142( )
      {
         /* 'HIDESINGLEACTIONS' Routine */
         imgUpdate_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgUpdate_Visible), 5, 0)), true);
      }

      protected void S152( )
      {
         /* 'HIDEMULTIPLEPAGERELATEDACTIONS' Routine */
         imgDelete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgDelete_Visible), 5, 0)), true);
      }

      protected void S192( )
      {
         /* 'DISPLAYMULTIPLEROWSELECTEDACTIONS' Routine */
         AV46ActivityList = new GXBaseCollection<SdtK2BActivityList_K2BActivityListItem>( context, "K2BActivityListItem", "PACYE2");
         AV47ActivityListItem = new SdtK2BActivityList_K2BActivityListItem(context);
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Standardactivitytype = "Delete";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Useractivitytype = "";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Entityname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Transactionname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Pgmname = "";
         AV46ActivityList.Add(AV47ActivityListItem, 0);
         new k2bisauthorizedactivitylist(context ).execute( ref  AV46ActivityList) ;
         if ( ((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Isauthorized )
         {
            imgDelete_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgDelete_Visible), 5, 0)), true);
         }
         else
         {
            imgDelete_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgDelete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgDelete_Visible), 5, 0)), true);
         }
      }

      protected void S182( )
      {
         /* 'DISPLAYSINGLEACTIONS' Routine */
         AV46ActivityList = new GXBaseCollection<SdtK2BActivityList_K2BActivityListItem>( context, "K2BActivityListItem", "PACYE2");
         AV47ActivityListItem = new SdtK2BActivityList_K2BActivityListItem(context);
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Standardactivitytype = "Update";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Useractivitytype = "";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Entityname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Transactionname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Pgmname = "EntityManagerPersona";
         AV46ActivityList.Add(AV47ActivityListItem, 0);
         new k2bisauthorizedactivitylist(context ).execute( ref  AV46ActivityList) ;
         if ( ((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Isauthorized )
         {
            imgUpdate_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgUpdate_Visible), 5, 0)), true);
         }
         else
         {
            imgUpdate_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgUpdate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgUpdate_Visible), 5, 0)), true);
         }
      }

      protected void S262( )
      {
         /* 'DISPLAYPERSISTENTACTIONS' Routine */
         AV46ActivityList = new GXBaseCollection<SdtK2BActivityList_K2BActivityListItem>( context, "K2BActivityListItem", "PACYE2");
         AV47ActivityListItem = new SdtK2BActivityList_K2BActivityListItem(context);
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Standardactivitytype = "List";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Useractivitytype = "";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Entityname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Transactionname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Pgmname = "ReportWWPersona";
         AV46ActivityList.Add(AV47ActivityListItem, 0);
         AV47ActivityListItem = new SdtK2BActivityList_K2BActivityListItem(context);
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Standardactivitytype = "List";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Useractivitytype = "";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Entityname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Transactionname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Pgmname = "ExportWWPersona";
         AV46ActivityList.Add(AV47ActivityListItem, 0);
         AV47ActivityListItem = new SdtK2BActivityList_K2BActivityListItem(context);
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Standardactivitytype = "Insert";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Useractivitytype = "";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Entityname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Transactionname = "Persona";
         AV47ActivityListItem.gxTpr_Activity.gxTpr_Pgmname = "EntityManagerPersona";
         AV46ActivityList.Add(AV47ActivityListItem, 0);
         new k2bisauthorizedactivitylist(context ).execute( ref  AV46ActivityList) ;
         if ( ((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(1)).gxTpr_Isauthorized )
         {
            imgReport_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgReport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgReport_Visible), 5, 0)), true);
         }
         else
         {
            imgReport_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgReport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgReport_Visible), 5, 0)), true);
         }
         if ( ((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(2)).gxTpr_Isauthorized )
         {
            imgExport_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExport_Visible), 5, 0)), true);
         }
         else
         {
            imgExport_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgExport_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgExport_Visible), 5, 0)), true);
         }
         if ( ((SdtK2BActivityList_K2BActivityListItem)AV46ActivityList.Item(3)).gxTpr_Isauthorized )
         {
            bttInsert_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttInsert_Visible), 5, 0)), true);
         }
         else
         {
            bttInsert_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttInsert_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttInsert_Visible), 5, 0)), true);
         }
      }

      protected void E211E2( )
      {
         /* 'SaveGridSettings' Routine */
         new k2bloadgridcolumns(context ).execute(  AV61Pgmname,  "Grid", out  AV14GridColumns) ;
         AV71GXV5 = 1;
         while ( AV71GXV5 <= AV14GridColumns.Count )
         {
            AV15GridColumn = ((SdtK2BGridColumns_K2BGridColumnsItem)AV14GridColumns.Item(AV71GXV5));
            if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaID") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV20Att_PersonaID_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaPNombre") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV21Att_PersonaPNombre_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaSNombre") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV22Att_PersonaSNombre_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaPApellido") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV23Att_PersonaPApellido_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaSApellido") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV24Att_PersonaSApellido_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaSexo") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV25Att_PersonaSexo_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaDPI") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV26Att_PersonaDPI_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaTipo") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV27Att_PersonaTipo_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaFechaNacimiento") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV28Att_PersonaFechaNacimiento_Visible;
            }
            else if ( StringUtil.StrCmp(AV15GridColumn.gxTpr_Attributename, "PersonaPacienteOrigenSordera") == 0 )
            {
               AV15GridColumn.gxTpr_Showattribute = AV29Att_PersonaPacienteOrigenSordera_Visible;
            }
            AV71GXV5 = (int)(AV71GXV5+1);
         }
         new k2bsavegridcolumns(context ).execute(  AV61Pgmname,  "Grid",  AV14GridColumns) ;
         /* Execute user subroutine: 'HIDESHOWCOLUMNS' */
         S162 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         AV12OrderedBy = AV13GridSettingsOrderedBy;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         AV50UC_OrderedBy = AV12OrderedBy;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UC_OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV50UC_OrderedBy), 4, 0)));
         if ( subGrid_Rows != AV41GridSettingsRowsPerPageVariable )
         {
            subGrid_Rows = (int)(AV41GridSettingsRowsPerPageVariable);
            GxWebStd.gx_hidden_field( context, "GRID_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Rows), 6, 0, ".", "")));
            AV44CurrentPage = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44CurrentPage", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44CurrentPage), 5, 0)));
            subgrid_firstpage( ) ;
         }
         /* Execute user subroutine: 'SAVEGRIDSTATE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
         gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
         divK2bgridsettingscontentoutertable_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divK2bgridsettingscontentoutertable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divK2bgridsettingscontentoutertable_Visible), 5, 0)), true);
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14GridColumns", AV14GridColumns);
      }

      protected void E221E2( )
      {
         /* 'ConfirmYes' Routine */
         tblTableconditionalconfirm_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblTableconditionalconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblTableconditionalconfirm_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(AV52ConfirmationSubId, "DoMultipleDelete") == 0 )
         {
            /* Execute user subroutine: 'DOMULTIPLEDELETE' */
            S232 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14GridColumns", AV14GridColumns);
      }

      protected void S232( )
      {
         /* 'DOMULTIPLEDELETE' Routine */
         AV54DeletedCount = 0;
         AV55ErrorCount = 0;
         AV72GXV6 = 1;
         while ( AV72GXV6 <= AV34SelectedRows.Count )
         {
            AV35SelectedRow = ((SdtWWPersonaSDT_WWPersonaSDTItem)AV34SelectedRows.Item(AV72GXV6));
            AV56MultipleDelete_BC.Load(AV35SelectedRow.gxTpr_Personaid);
            AV56MultipleDelete_BC.Delete();
            if ( AV56MultipleDelete_BC.Success() )
            {
               AV54DeletedCount = (short)(AV54DeletedCount+1);
            }
            else
            {
               AV55ErrorCount = (short)(AV55ErrorCount+1);
               AV57ErrorMessage = StringUtil.Format( "Persona \"%1\" no pudo ser eliminado por causa de estos errores:", AV56MultipleDelete_BC.gxTpr_Personapnombre, "", "", "", "", "", "", "", "");
               AV74GXV8 = 1;
               AV73GXV7 = AV56MultipleDelete_BC.GetMessages();
               while ( AV74GXV8 <= AV73GXV7.Count )
               {
                  AV40Message = ((SdtMessages_Message)AV73GXV7.Item(AV74GXV8));
                  AV57ErrorMessage = AV57ErrorMessage + StringUtil.NewLine( ) + " - " + AV40Message.gxTpr_Description;
                  AV74GXV8 = (int)(AV74GXV8+1);
               }
               GX_msglist.addItem(AV57ErrorMessage);
            }
            AV72GXV6 = (int)(AV72GXV6+1);
         }
         if ( AV55ErrorCount > 0 )
         {
            pr_gam.rollback( "WWPersona");
            pr_default.rollback( "WWPersona");
            GX_msglist.addItem("Ning�n Persona fue eliminado");
         }
         else
         {
            pr_gam.commit( "WWPersona");
            pr_default.commit( "WWPersona");
            GX_msglist.addItem(StringUtil.Format( "%1 personas eliminados", StringUtil.LTrim( StringUtil.Str( (decimal)(AV54DeletedCount), 4, 0)), "", "", "", "", "", "", "", ""));
         }
         gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
      }

      protected void E131E2( )
      {
         /* K2borderbyusercontrol_Orderbychanged Routine */
         AV12OrderedBy = AV50UC_OrderedBy;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12OrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12OrderedBy), 4, 0)));
         AV13GridSettingsOrderedBy = AV12OrderedBy;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13GridSettingsOrderedBy", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0)));
         gxgrGrid_refresh( subGrid_Rows, AV8PersonaPNombre, AV10PersonaFechaNacimiento_From, AV11PersonaFechaNacimiento_To, AV12OrderedBy, AV61Pgmname, AV44CurrentPage, AV41GridSettingsRowsPerPageVariable, AV14GridColumns, AV18AttributeName, AV19ColumnTitle, AV37AllSelectedRows, AV38SelectedRowsAmount) ;
         /*  Sending Event outputs  */
         cmbavGridsettingsorderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsorderedby_Internalname, "Values", cmbavGridsettingsorderedby.ToJavascriptSource(), true);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14GridColumns", AV14GridColumns);
      }

      protected void wb_table8_218_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblTableconditionalconfirm_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblTableconditionalconfirm_Internalname, tblTableconditionalconfirm_Internalname, "", "Table_ConditionalConfirm", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table9_221_1E2( true) ;
         }
         else
         {
            wb_table9_221_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table9_221_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table8_218_1E2e( true) ;
         }
         else
         {
            wb_table8_218_1E2e( false) ;
         }
      }

      protected void wb_table9_221_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblSection_condconf_dialog_Internalname, tblSection_condconf_dialog_Internalname, "", "Section_CondConf_Dialog", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table10_224_1E2( true) ;
         }
         else
         {
            wb_table10_224_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table10_224_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table9_221_1E2e( true) ;
         }
         else
         {
            wb_table9_221_1E2e( false) ;
         }
      }

      protected void wb_table10_224_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblSection_condconf_dialog_inner_Internalname, tblSection_condconf_dialog_inner_Internalname, "", "Section_CondConf_DialogInner", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavConfirmmessage_Internalname, "ConfirmMessage", "col-sm-3 Attribute_ConditionalConfirmLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 228,'',false,'" + sGXsfl_167_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConfirmmessage_Internalname, StringUtil.RTrim( AV53ConfirmMessage), StringUtil.RTrim( context.localUtil.Format( AV53ConfirmMessage, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,228);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConfirmmessage_Jsonclick, 0, "Attribute_ConditionalConfirm", "", "", "", "", 1, edtavConfirmmessage_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_WWPersona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table11_231_1E2( true) ;
         }
         else
         {
            wb_table11_231_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table11_231_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table10_224_1E2e( true) ;
         }
         else
         {
            wb_table10_224_1E2e( false) ;
         }
      }

      protected void wb_table11_231_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblConfirm_hidden_actionstable_Internalname, tblConfirm_hidden_actionstable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 234,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction_Confirm";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButtonconfirmyes_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(167), 3, 0)+","+"null"+");", "Aceptar", bttButtonconfirmyes_Jsonclick, 5, "Aceptar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'CONFIRMYES\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 236,'',false,'',0)\"";
            ClassString = "K2BToolsButton_GreyAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButtonconfirmno_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(167), 3, 0)+","+"null"+");", "Cancelar", bttButtonconfirmno_Jsonclick, 7, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e291e1_client"+"'", TempTags, "", 2, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table11_231_1E2e( true) ;
         }
         else
         {
            wb_table11_231_1E2e( false) ;
         }
      }

      protected void wb_table7_212_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblK2btoolsabstracthiddenitemsgrid_Internalname, tblK2btoolsabstracthiddenitemsgrid_Internalname, "", "", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"K2BORDERBYUSERCONTROLContainer"+"\"></div>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table7_212_1E2e( true) ;
         }
         else
         {
            wb_table7_212_1E2e( false) ;
         }
      }

      protected void wb_table6_190_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblK2btoolspagingcontainertable_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblK2btoolspagingcontainertable_Internalname, tblK2btoolspagingcontainertable_Internalname, "", "K2BToolsTable_PaginationContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellPreviouspagebuttoncell_Internalname+"\"  class='K2BToolsCell_PaginationFirst'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPreviouspagebuttontextblock_Internalname, "&laquo;", "", "", lblPreviouspagebuttontextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOPREVIOUS\\'."+"'", "", lblPreviouspagebuttontextblock_Class, 5, "", 1, 1, 1, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellFirstpagecell_Internalname+"\"  class='"+cellFirstpagecell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFirstpagetextblock_Internalname, lblFirstpagetextblock_Caption, "", "", lblFirstpagetextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOFIRST\\'."+"'", "", "K2BToolsTextBlock_PaginationNormal", 5, "", lblFirstpagetextblock_Visible, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellSpacingleftcell_Internalname+"\"  class='"+cellSpacingleftcell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSpacinglefttextblock_Internalname, "...", "", "", lblSpacinglefttextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblSpacinglefttextblock_Visible, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPreviouspagecell_Internalname+"\"  class='"+cellPreviouspagecell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPreviouspagetextblock_Internalname, lblPreviouspagetextblock_Caption, "", "", lblPreviouspagetextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOPREVIOUS\\'."+"'", "", "K2BToolsTextBlock_PaginationNormal", 5, "", lblPreviouspagetextblock_Visible, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellCurrentpagecell_Internalname+"\"  class='K2BToolsCell_PaginationCurrentPage'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblCurrentpagetextblock_Internalname, lblCurrentpagetextblock_Caption, "", "", lblCurrentpagetextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationCurrent", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellNextpagecell_Internalname+"\"  class='"+cellNextpagecell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNextpagetextblock_Internalname, lblNextpagetextblock_Caption, "", "", lblNextpagetextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DONEXT\\'."+"'", "", "K2BToolsTextBlock_PaginationNormal", 5, "", lblNextpagetextblock_Visible, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellSpacingrightcell_Internalname+"\"  class='"+cellSpacingrightcell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSpacingrighttextblock_Internalname, "...", "", "", lblSpacingrighttextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblSpacingrighttextblock_Visible, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellLastpagecell_Internalname+"\"  class='"+cellLastpagecell_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLastpagetextblock_Internalname, lblLastpagetextblock_Caption, "", "", lblLastpagetextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOLAST\\'."+"'", "", "K2BToolsTextBlock_PaginationNormal", 5, "", lblLastpagetextblock_Visible, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellNextpagebuttoncell_Internalname+"\"  class='K2BToolsCell_PaginationLast'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNextpagebuttontextblock_Internalname, "&raquo;", "", "", lblNextpagebuttontextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DONEXT\\'."+"'", "", lblNextpagebuttontextblock_Class, 5, "", 1, 1, 1, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table6_190_1E2e( true) ;
         }
         else
         {
            wb_table6_190_1E2e( false) ;
         }
      }

      protected void wb_table5_181_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblNoresultsfoundtable_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblNoresultsfoundtable_Internalname, tblNoresultsfoundtable_Internalname, "", "K2BToolsTable_NoResultsFound", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNoresultsfoundtextblock_Internalname, "No hay resultados", "", "", lblNoresultsfoundtextblock_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_NoResultsFound", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_181_1E2e( true) ;
         }
         else
         {
            wb_table5_181_1E2e( false) ;
         }
      }

      protected void wb_table4_161_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable_gridcontainergrid1_Internalname, tblTable_gridcontainergrid1_Internalname, "", "K2BToolsTable_GridContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 165,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "K2BTools_CheckAllGrid";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavCheckall_Internalname, StringUtil.BoolToStr( AV51CheckAll), "", "", 1, chkavCheckall.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onblur=\""+""+";gx.evt.onblur(this,165);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"167\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid_WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(20), 4, 0))+"px"+" class=\""+"CheckBoxInGrid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(298), 4, 0))+"px"+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaID_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "ID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaPNombre_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "PNombre") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaSNombre_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "SNombre") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaPApellido_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "PApellido") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaSApellido_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "SApellido") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaSexo_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Sexo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaDPI_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "DPI") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((cmbPersonaTipo.Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Tipo") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((edtPersonaFechaNacimiento_Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Fecha Nacimiento") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+((cmbPersonaPacienteOrigenSordera.Visible==0) ? "display:none;" : "")+""+"\" "+">") ;
               context.SendWebValue( "Origen Sordera") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  GridContainer = new GXWebGrid( context);
               }
               else
               {
                  GridContainer.Clear();
               }
               GridContainer.SetWrapped(nGXWrapped);
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid_WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.BoolToStr( AV33Selected));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A1PersonaID.ToString());
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaID_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A2PersonaPNombre);
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaPNombre_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A3PersonaSNombre);
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaSNombre_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A4PersonaPApellido);
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaPApellido_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A5PersonaSApellido);
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaSApellido_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.RTrim( A6PersonaSexo));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaSexo_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", A7PersonaDPI);
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaDPI_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A8PersonaTipo), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbPersonaTipo.Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtPersonaFechaNacimiento_Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0, ".", "")));
               GridColumn.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbPersonaPacienteOrigenSordera.Visible), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 167 )
         {
            wbEnd = 0;
            nRC_GXsfl_167 = (short)(nGXsfl_167_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_161_1E2e( true) ;
         }
         else
         {
            wb_table4_161_1E2e( false) ;
         }
      }

      protected void wb_table3_61_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable6_Internalname, tblTable6_Internalname, "", "K2BToolsTable_FloatRight", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bgridsettingstable_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_GridSettingsContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblK2bgridsettingslabel_Internalname, "Config. tabla &#x25BC;", "", "", lblK2bgridsettingslabel_Jsonclick, "'"+""+"'"+",false,"+"'"+"e301e1_client"+"'", "", "K2BToolsTextBlock_GridSettingsTitle", 7, "", 1, 1, 1, "HLP_WWPersona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bgridsettingscontentoutertable_Internalname, divK2bgridsettingscontentoutertable_Visible, 0, "px", 0, "px", "K2BToolsTable_GridSettings", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table12_73_1E2( true) ;
         }
         else
         {
            wb_table12_73_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table12_73_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttK2bgridsettingssave_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(167), 3, 0)+","+"null"+");", "Guardar", bttK2bgridsettingssave_Jsonclick, 5, "Guardar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SAVEGRIDSETTINGS\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWPersona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table13_150_1E2( true) ;
         }
         else
         {
            wb_table13_150_1E2( false) ;
         }
         return  ;
      }

      protected void wb_table13_150_1E2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_61_1E2e( true) ;
         }
         else
         {
            wb_table3_61_1E2e( false) ;
         }
      }

      protected void wb_table13_150_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblK2btablegridactionsrightcontainer_Internalname, tblK2btablegridactionsrightcontainer_Internalname, "", "Table_ActionsContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='K2BToolsTableCell_ActionContainer'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'',0)\"";
            ClassString = "Image_Action";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "bc80c103-5523-45a1-b084-18e21da26d5e", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgReport_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgReport_Visible, 1, "Reporte", imgReport_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 7, imgReport_Jsonclick, "'"+""+"'"+",false,"+"'"+"e311e1_client"+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='K2BToolsTableCell_ActionContainer'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 155,'',false,'',0)\"";
            ClassString = "Image_Action InvisibleInExtraSmallImage InvisibleInSmallImage";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "e1d29e24-01c9-4adf-a617-2236e5e52470", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgExport_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgExport_Visible, 1, "Exportar", imgExport_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgExport_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOEXPORT\\'."+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table13_150_1E2e( true) ;
         }
         else
         {
            wb_table13_150_1E2e( false) ;
         }
      }

      protected void wb_table12_73_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblGridsettingstable_content_Internalname, tblGridsettingstable_content_Internalname, "", "K2BToolsTable_GridSettingsContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonaidruntimecolumnatt_Internalname, "ID", "", "", lblPersonaidruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personaid_visible_Internalname, StringUtil.BoolToStr( AV20Att_PersonaID_Visible), "", "", 1, chkavAtt_personaid_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(79, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonapnombreruntimecolumnatt_Internalname, "PNombre", "", "", lblPersonapnombreruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 85,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personapnombre_visible_Internalname, StringUtil.BoolToStr( AV21Att_PersonaPNombre_Visible), "", "", 1, chkavAtt_personapnombre_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(85, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,85);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonasnombreruntimecolumnatt_Internalname, "SNombre", "", "", lblPersonasnombreruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personasnombre_visible_Internalname, StringUtil.BoolToStr( AV22Att_PersonaSNombre_Visible), "", "", 1, chkavAtt_personasnombre_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(91, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonapapellidoruntimecolumnatt_Internalname, "PApellido", "", "", lblPersonapapellidoruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 97,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personapapellido_visible_Internalname, StringUtil.BoolToStr( AV23Att_PersonaPApellido_Visible), "", "", 1, chkavAtt_personapapellido_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(97, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,97);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonasapellidoruntimecolumnatt_Internalname, "SApellido", "", "", lblPersonasapellidoruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personasapellido_visible_Internalname, StringUtil.BoolToStr( AV24Att_PersonaSApellido_Visible), "", "", 1, chkavAtt_personasapellido_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(103, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonasexoruntimecolumnatt_Internalname, "Sexo", "", "", lblPersonasexoruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personasexo_visible_Internalname, StringUtil.BoolToStr( AV25Att_PersonaSexo_Visible), "", "", 1, chkavAtt_personasexo_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonadpiruntimecolumnatt_Internalname, "DPI", "", "", lblPersonadpiruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 115,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personadpi_visible_Internalname, StringUtil.BoolToStr( AV26Att_PersonaDPI_Visible), "", "", 1, chkavAtt_personadpi_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(115, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,115);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonatiporuntimecolumnatt_Internalname, "Tipo", "", "", lblPersonatiporuntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personatipo_visible_Internalname, StringUtil.BoolToStr( AV27Att_PersonaTipo_Visible), "", "", 1, chkavAtt_personatipo_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(121, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonafechanacimientoruntimecolumnatt_Internalname, "Fecha Nacimiento", "", "", lblPersonafechanacimientoruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 127,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personafechanacimiento_visible_Internalname, StringUtil.BoolToStr( AV28Att_PersonaFechaNacimiento_Visible), "", "", 1, chkavAtt_personafechanacimiento_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(127, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,127);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPersonapacienteorigensorderaruntimecolumnatt_Internalname, "Origen Sordera", "", "", lblPersonapacienteorigensorderaruntimecolumnatt_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'" + sGXsfl_167_idx + "',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAtt_personapacienteorigensordera_visible_Internalname, StringUtil.BoolToStr( AV29Att_PersonaPacienteOrigenSordera_Visible), "", "", 1, chkavAtt_personapacienteorigensordera_visible.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(133, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblOrderbytext_Internalname, "Ordenado por", "", "", lblOrderbytext_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavGridsettingsorderedby_Internalname, "Grid Settings Ordered By", "col-sm-3 K2BToolsEnhancedComboLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'" + sGXsfl_167_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGridsettingsorderedby, cmbavGridsettingsorderedby_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0)), 1, cmbavGridsettingsorderedby_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "Ordenado por", 1, cmbavGridsettingsorderedby.Enabled, 0, 0, 0, "em", 0, "", "", "K2BToolsEnhancedCombo", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,139);\"", "", true, "HLP_WWPersona.htm");
            cmbavGridsettingsorderedby.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13GridSettingsOrderedBy), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsorderedby_Internalname, "Values", (String)(cmbavGridsettingsorderedby.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblRowsperpagegrid_Internalname, "Filas por p�gina", "", "", lblRowsperpagegrid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavGridsettingsrowsperpagevariable_Internalname, "GridSettingsRowsPerPageVariable", "col-sm-3 K2BToolsEnhancedComboLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 145,'',false,'" + sGXsfl_167_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGridsettingsrowsperpagevariable, cmbavGridsettingsrowsperpagevariable_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0)), 1, cmbavGridsettingsrowsperpagevariable_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "Filas por p�gina", 1, cmbavGridsettingsrowsperpagevariable.Enabled, 0, 0, 0, "em", 0, "", "", "K2BToolsEnhancedCombo", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,145);\"", "", true, "HLP_WWPersona.htm");
            cmbavGridsettingsrowsperpagevariable.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV41GridSettingsRowsPerPageVariable), 10, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpagevariable_Internalname, "Values", (String)(cmbavGridsettingsrowsperpagevariable.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table12_73_1E2e( true) ;
         }
         else
         {
            wb_table12_73_1E2e( false) ;
         }
      }

      protected void wb_table2_52_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblK2btablegridactionsleftcontainer_Internalname, tblK2btablegridactionsleftcontainer_Internalname, "", "Table_ActionsContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='K2BToolsTableCell_ActionContainer'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttInsert_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(167), 3, 0)+","+"null"+");", "Nuevo", bttInsert_Jsonclick, 5, bttInsert_Tooltiptext, "", StyleString, ClassString, bttInsert_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'DOINSERT\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='K2BToolsTableCell_ActionContainer'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "Image_Action";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "788f9b72-f982-49f9-99e4-c0374e31a85a", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgUpdate_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgUpdate_Visible, 1, "Actualizar", imgUpdate_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgUpdate_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DOUPDATE\\'."+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='K2BToolsTableCell_ActionContainer'>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "Image_Action";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "3e4a9f50-2c57-41b6-9da5-ebe49bca33c0", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgDelete_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgDelete_Visible, 1, "Eliminar", imgDelete_Tooltiptext, 0, 0, 0, "px", 0, "px", 0, 0, 5, imgDelete_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'DODELETE\\'."+"'", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" "+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_52_1E2e( true) ;
         }
         else
         {
            wb_table2_52_1E2e( false) ;
         }
      }

      protected void wb_table1_34_1E2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblDaterangefiltermaintable_personafechanacimiento_Internalname, tblDaterangefiltermaintable_personafechanacimiento_Internalname, "", "K2BToolsTable_SideTextContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPersonafechanacimiento_from_Internalname, "Persona Fecha Nacimiento_From", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'" + sGXsfl_167_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPersonafechanacimiento_from_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPersonafechanacimiento_from_Internalname, context.localUtil.Format(AV10PersonaFechaNacimiento_From, "99/99/99"), context.localUtil.Format( AV10PersonaFechaNacimiento_From, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPersonafechanacimiento_from_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavPersonafechanacimiento_from_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPersona.htm");
            GxWebStd.gx_bitmap( context, edtavPersonafechanacimiento_from_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavPersonafechanacimiento_from_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_WWPersona.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDaterangeseparator_personafechanacimiento_Internalname, "-", "", "", lblDaterangeseparator_personafechanacimiento_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "SideLabel", 0, "", 1, 1, 0, "HLP_WWPersona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPersonafechanacimiento_to_Internalname, "Persona Fecha Nacimiento_To", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_167_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavPersonafechanacimiento_to_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavPersonafechanacimiento_to_Internalname, context.localUtil.Format(AV11PersonaFechaNacimiento_To, "99/99/99"), context.localUtil.Format( AV11PersonaFechaNacimiento_To, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavPersonafechanacimiento_to_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavPersonafechanacimiento_to_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_WWPersona.htm");
            GxWebStd.gx_bitmap( context, edtavPersonafechanacimiento_to_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavPersonafechanacimiento_to_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_WWPersona.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_34_1E2e( true) ;
         }
         else
         {
            wb_table1_34_1E2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1E2( ) ;
         WS1E2( ) ;
         WE1E2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("K2BControlBeautify/montrezorro-bootstrap-checkbox/css/bootstrap-checkbox.css", "");
         AddStyleSheetFile("K2BControlBeautify/silviomoreto-bootstrap-select/dist/css/bootstrap-select.css", "");
         AddStyleSheetFile("K2BControlBeautify/toastr-master/toastr.min.css", "");
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20181118124933", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("wwpersona.js", "?20181118124933", false);
         context.AddJavascriptSource("K2BOrderBy/K2BOrderByRender.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_1672( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_167_idx;
         edtPersonaID_Internalname = "PERSONAID_"+sGXsfl_167_idx;
         edtPersonaPNombre_Internalname = "PERSONAPNOMBRE_"+sGXsfl_167_idx;
         edtPersonaSNombre_Internalname = "PERSONASNOMBRE_"+sGXsfl_167_idx;
         edtPersonaPApellido_Internalname = "PERSONAPAPELLIDO_"+sGXsfl_167_idx;
         edtPersonaSApellido_Internalname = "PERSONASAPELLIDO_"+sGXsfl_167_idx;
         edtPersonaSexo_Internalname = "PERSONASEXO_"+sGXsfl_167_idx;
         edtPersonaDPI_Internalname = "PERSONADPI_"+sGXsfl_167_idx;
         cmbPersonaTipo_Internalname = "PERSONATIPO_"+sGXsfl_167_idx;
         edtPersonaFechaNacimiento_Internalname = "PERSONAFECHANACIMIENTO_"+sGXsfl_167_idx;
         cmbPersonaPacienteOrigenSordera_Internalname = "PERSONAPACIENTEORIGENSORDERA_"+sGXsfl_167_idx;
      }

      protected void SubsflControlProps_fel_1672( )
      {
         chkavSelected_Internalname = "vSELECTED_"+sGXsfl_167_fel_idx;
         edtPersonaID_Internalname = "PERSONAID_"+sGXsfl_167_fel_idx;
         edtPersonaPNombre_Internalname = "PERSONAPNOMBRE_"+sGXsfl_167_fel_idx;
         edtPersonaSNombre_Internalname = "PERSONASNOMBRE_"+sGXsfl_167_fel_idx;
         edtPersonaPApellido_Internalname = "PERSONAPAPELLIDO_"+sGXsfl_167_fel_idx;
         edtPersonaSApellido_Internalname = "PERSONASAPELLIDO_"+sGXsfl_167_fel_idx;
         edtPersonaSexo_Internalname = "PERSONASEXO_"+sGXsfl_167_fel_idx;
         edtPersonaDPI_Internalname = "PERSONADPI_"+sGXsfl_167_fel_idx;
         cmbPersonaTipo_Internalname = "PERSONATIPO_"+sGXsfl_167_fel_idx;
         edtPersonaFechaNacimiento_Internalname = "PERSONAFECHANACIMIENTO_"+sGXsfl_167_fel_idx;
         cmbPersonaPacienteOrigenSordera_Internalname = "PERSONAPACIENTEORIGENSORDERA_"+sGXsfl_167_fel_idx;
      }

      protected void sendrow_1672( )
      {
         SubsflControlProps_1672( ) ;
         WB1E0( ) ;
         if ( ( subGrid_Rows * 1 == 0 ) || ( nGXsfl_167_idx <= subGrid_Recordsperpage( ) * 1 ) )
         {
            GridRow = GXWebRow.GetNew(context,GridContainer);
            if ( subGrid_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
            else if ( subGrid_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid_Backstyle = 0;
               subGrid_Backcolor = subGrid_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Uniform";
               }
            }
            else if ( subGrid_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
               subGrid_Backcolor = (int)(0x0);
            }
            else if ( subGrid_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid_Backstyle = 1;
               if ( ((int)((nGXsfl_167_idx) % (2))) == 0 )
               {
                  subGrid_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Even";
                  }
               }
               else
               {
                  subGrid_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Odd";
                  }
               }
            }
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+"Grid_WorkWith"+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_167_idx+"\">") ;
            }
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Check box */
            TempTags = " " + ((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 168,'',false,'"+sGXsfl_167_idx+"',167)\"" : " ");
            ClassString = "CheckBoxInGrid";
            StyleString = "";
            GridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavSelected_Internalname,StringUtil.BoolToStr( AV33Selected),(String)"",(String)"",(short)-1,(short)1,(String)"true",(String)"",(String)StyleString,(String)ClassString,(String)"K2BToolsCheckBoxColumn",(String)"",TempTags+((chkavSelected.Enabled!=0)&&(chkavSelected.Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,168);\"" : " ")});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+((edtPersonaID_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaID_Internalname,A1PersonaID.ToString(),A1PersonaID.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaID_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn K2BToolsSortableColumn InvisibleInExtraSmallColumn",(String)"",(int)edtPersonaID_Visible,(short)0,(short)0,(String)"text",(String)"",(short)298,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)167,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtPersonaPNombre_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaPNombre_Internalname,(String)A2PersonaPNombre,(String)"",(String)"","'"+""+"'"+",false,"+"'"+"EPERSONAPNOMBRE.CLICK."+sGXsfl_167_idx+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaPNombre_Jsonclick,(short)5,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn K2BToolsSortableColumn",(String)"",(int)edtPersonaPNombre_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)167,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtPersonaSNombre_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaSNombre_Internalname,(String)A3PersonaSNombre,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaSNombre_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(int)edtPersonaSNombre_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)167,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtPersonaPApellido_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaPApellido_Internalname,(String)A4PersonaPApellido,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaPApellido_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(int)edtPersonaPApellido_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)167,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtPersonaSApellido_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaSApellido_Internalname,(String)A5PersonaSApellido,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaSApellido_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(int)edtPersonaSApellido_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)167,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtPersonaSexo_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaSexo_Internalname,StringUtil.RTrim( A6PersonaSexo),(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaSexo_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(int)edtPersonaSexo_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)1,(short)0,(short)0,(short)167,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+((edtPersonaDPI_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaDPI_Internalname,(String)A7PersonaDPI,(String)"",(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaDPI_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(int)edtPersonaDPI_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)40,(short)0,(short)0,(short)167,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((cmbPersonaTipo.Visible==0) ? "display:none;" : "")+"\">") ;
            }
            if ( ( cmbPersonaTipo.ItemCount == 0 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PERSONATIPO_" + sGXsfl_167_idx;
               cmbPersonaTipo.Name = GXCCtl;
               cmbPersonaTipo.WebTags = "";
               cmbPersonaTipo.addItem("1", "COACH", 0);
               cmbPersonaTipo.addItem("2", "Paciente", 0);
               if ( cmbPersonaTipo.ItemCount > 0 )
               {
                  A8PersonaTipo = (short)(NumberUtil.Val( cmbPersonaTipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0))), "."));
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbPersonaTipo,(String)cmbPersonaTipo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)),(short)1,(String)cmbPersonaTipo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",cmbPersonaTipo.Visible,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute_Grid",(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(String)"",(String)"",(bool)true});
            cmbPersonaTipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPersonaTipo_Internalname, "Values", (String)(cmbPersonaTipo.ToJavascriptSource()), !bGXsfl_167_Refreshing);
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((edtPersonaFechaNacimiento_Visible==0) ? "display:none;" : "")+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute_Grid";
            GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtPersonaFechaNacimiento_Internalname,context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"),context.localUtil.Format( A9PersonaFechaNacimiento, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtPersonaFechaNacimiento_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(int)edtPersonaFechaNacimiento_Visible,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)167,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            /* Subfile cell */
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+((cmbPersonaPacienteOrigenSordera.Visible==0) ? "display:none;" : "")+"\">") ;
            }
            if ( ( cmbPersonaPacienteOrigenSordera.ItemCount == 0 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "PERSONAPACIENTEORIGENSORDERA_" + sGXsfl_167_idx;
               cmbPersonaPacienteOrigenSordera.Name = GXCCtl;
               cmbPersonaPacienteOrigenSordera.WebTags = "";
               cmbPersonaPacienteOrigenSordera.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Ninguno)", 0);
               cmbPersonaPacienteOrigenSordera.addItem("1", "Congenito", 0);
               cmbPersonaPacienteOrigenSordera.addItem("2", "Accidental", 0);
               cmbPersonaPacienteOrigenSordera.addItem("3", "Otros", 0);
               if ( cmbPersonaPacienteOrigenSordera.ItemCount > 0 )
               {
                  A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cmbPersonaPacienteOrigenSordera.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0))), "."));
                  n10PersonaPacienteOrigenSordera = false;
               }
            }
            /* ComboBox */
            GridRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbPersonaPacienteOrigenSordera,(String)cmbPersonaPacienteOrigenSordera_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)),(short)1,(String)cmbPersonaPacienteOrigenSordera_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",cmbPersonaPacienteOrigenSordera.Visible,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute_Grid",(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(String)"",(String)"",(bool)true});
            cmbPersonaPacienteOrigenSordera.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbPersonaPacienteOrigenSordera_Internalname, "Values", (String)(cmbPersonaPacienteOrigenSordera.ToJavascriptSource()), !bGXsfl_167_Refreshing);
            send_integrity_lvl_hashes1E2( ) ;
            GridContainer.AddRow(GridRow);
            nGXsfl_167_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_167_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_167_idx+1));
            sGXsfl_167_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_167_idx), 4, 0)), 4, "0");
            SubsflControlProps_1672( ) ;
         }
         /* End function sendrow_1672 */
      }

      protected void init_default_properties( )
      {
         lblPgmdescriptortextblock_Internalname = "PGMDESCRIPTORTEXTBLOCK";
         edtavPersonapnombre_Internalname = "vPERSONAPNOMBRE";
         lblTextblockpersonafechanacimiento_Internalname = "TEXTBLOCKPERSONAFECHANACIMIENTO";
         edtavPersonafechanacimiento_from_Internalname = "vPERSONAFECHANACIMIENTO_FROM";
         lblDaterangeseparator_personafechanacimiento_Internalname = "DATERANGESEPARATOR_PERSONAFECHANACIMIENTO";
         edtavPersonafechanacimiento_to_Internalname = "vPERSONAFECHANACIMIENTO_TO";
         tblDaterangefiltermaintable_personafechanacimiento_Internalname = "DATERANGEFILTERMAINTABLE_PERSONAFECHANACIMIENTO";
         divK2btoolstable_attributecontainerpersonafechanacimiento_Internalname = "K2BTOOLSTABLE_ATTRIBUTECONTAINERPERSONAFECHANACIMIENTO";
         divFilterattributestable_Internalname = "FILTERATTRIBUTESTABLE";
         divK2btoolsfilterscontainer_Internalname = "K2BTOOLSFILTERSCONTAINER";
         bttInsert_Internalname = "INSERT";
         imgUpdate_Internalname = "UPDATE";
         imgDelete_Internalname = "DELETE";
         tblK2btablegridactionsleftcontainer_Internalname = "K2BTABLEGRIDACTIONSLEFTCONTAINER";
         divGridactionsleftcell_Internalname = "GRIDACTIONSLEFTCELL";
         lblK2bgridsettingslabel_Internalname = "K2BGRIDSETTINGSLABEL";
         lblPersonaidruntimecolumnatt_Internalname = "PERSONAIDRUNTIMECOLUMNATT";
         chkavAtt_personaid_visible_Internalname = "vATT_PERSONAID_VISIBLE";
         lblPersonapnombreruntimecolumnatt_Internalname = "PERSONAPNOMBRERUNTIMECOLUMNATT";
         chkavAtt_personapnombre_visible_Internalname = "vATT_PERSONAPNOMBRE_VISIBLE";
         lblPersonasnombreruntimecolumnatt_Internalname = "PERSONASNOMBRERUNTIMECOLUMNATT";
         chkavAtt_personasnombre_visible_Internalname = "vATT_PERSONASNOMBRE_VISIBLE";
         lblPersonapapellidoruntimecolumnatt_Internalname = "PERSONAPAPELLIDORUNTIMECOLUMNATT";
         chkavAtt_personapapellido_visible_Internalname = "vATT_PERSONAPAPELLIDO_VISIBLE";
         lblPersonasapellidoruntimecolumnatt_Internalname = "PERSONASAPELLIDORUNTIMECOLUMNATT";
         chkavAtt_personasapellido_visible_Internalname = "vATT_PERSONASAPELLIDO_VISIBLE";
         lblPersonasexoruntimecolumnatt_Internalname = "PERSONASEXORUNTIMECOLUMNATT";
         chkavAtt_personasexo_visible_Internalname = "vATT_PERSONASEXO_VISIBLE";
         lblPersonadpiruntimecolumnatt_Internalname = "PERSONADPIRUNTIMECOLUMNATT";
         chkavAtt_personadpi_visible_Internalname = "vATT_PERSONADPI_VISIBLE";
         lblPersonatiporuntimecolumnatt_Internalname = "PERSONATIPORUNTIMECOLUMNATT";
         chkavAtt_personatipo_visible_Internalname = "vATT_PERSONATIPO_VISIBLE";
         lblPersonafechanacimientoruntimecolumnatt_Internalname = "PERSONAFECHANACIMIENTORUNTIMECOLUMNATT";
         chkavAtt_personafechanacimiento_visible_Internalname = "vATT_PERSONAFECHANACIMIENTO_VISIBLE";
         lblPersonapacienteorigensorderaruntimecolumnatt_Internalname = "PERSONAPACIENTEORIGENSORDERARUNTIMECOLUMNATT";
         chkavAtt_personapacienteorigensordera_visible_Internalname = "vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE";
         lblOrderbytext_Internalname = "ORDERBYTEXT";
         cmbavGridsettingsorderedby_Internalname = "vGRIDSETTINGSORDEREDBY";
         lblRowsperpagegrid_Internalname = "ROWSPERPAGEGRID";
         cmbavGridsettingsrowsperpagevariable_Internalname = "vGRIDSETTINGSROWSPERPAGEVARIABLE";
         tblGridsettingstable_content_Internalname = "GRIDSETTINGSTABLE_CONTENT";
         bttK2bgridsettingssave_Internalname = "K2BGRIDSETTINGSSAVE";
         divK2bgridsettingscontentoutertable_Internalname = "K2BGRIDSETTINGSCONTENTOUTERTABLE";
         divK2bgridsettingstable_Internalname = "K2BGRIDSETTINGSTABLE";
         imgReport_Internalname = "REPORT";
         imgExport_Internalname = "EXPORT";
         tblK2btablegridactionsrightcontainer_Internalname = "K2BTABLEGRIDACTIONSRIGHTCONTAINER";
         tblTable6_Internalname = "TABLE6";
         divTable5_Internalname = "TABLE5";
         chkavCheckall_Internalname = "vCHECKALL";
         chkavSelected_Internalname = "vSELECTED";
         edtPersonaID_Internalname = "PERSONAID";
         edtPersonaPNombre_Internalname = "PERSONAPNOMBRE";
         edtPersonaSNombre_Internalname = "PERSONASNOMBRE";
         edtPersonaPApellido_Internalname = "PERSONAPAPELLIDO";
         edtPersonaSApellido_Internalname = "PERSONASAPELLIDO";
         edtPersonaSexo_Internalname = "PERSONASEXO";
         edtPersonaDPI_Internalname = "PERSONADPI";
         cmbPersonaTipo_Internalname = "PERSONATIPO";
         edtPersonaFechaNacimiento_Internalname = "PERSONAFECHANACIMIENTO";
         cmbPersonaPacienteOrigenSordera_Internalname = "PERSONAPACIENTEORIGENSORDERA";
         tblTable_gridcontainergrid1_Internalname = "TABLE_GRIDCONTAINERGRID1";
         lblNoresultsfoundtextblock_Internalname = "NORESULTSFOUNDTEXTBLOCK";
         tblNoresultsfoundtable_Internalname = "NORESULTSFOUNDTABLE";
         divMaingridcontainerresponsivetablegrid1_Internalname = "MAINGRIDCONTAINERRESPONSIVETABLEGRID1";
         lblPreviouspagebuttontextblock_Internalname = "PREVIOUSPAGEBUTTONTEXTBLOCK";
         cellPreviouspagebuttoncell_Internalname = "PREVIOUSPAGEBUTTONCELL";
         lblFirstpagetextblock_Internalname = "FIRSTPAGETEXTBLOCK";
         cellFirstpagecell_Internalname = "FIRSTPAGECELL";
         lblSpacinglefttextblock_Internalname = "SPACINGLEFTTEXTBLOCK";
         cellSpacingleftcell_Internalname = "SPACINGLEFTCELL";
         lblPreviouspagetextblock_Internalname = "PREVIOUSPAGETEXTBLOCK";
         cellPreviouspagecell_Internalname = "PREVIOUSPAGECELL";
         lblCurrentpagetextblock_Internalname = "CURRENTPAGETEXTBLOCK";
         cellCurrentpagecell_Internalname = "CURRENTPAGECELL";
         lblNextpagetextblock_Internalname = "NEXTPAGETEXTBLOCK";
         cellNextpagecell_Internalname = "NEXTPAGECELL";
         lblSpacingrighttextblock_Internalname = "SPACINGRIGHTTEXTBLOCK";
         cellSpacingrightcell_Internalname = "SPACINGRIGHTCELL";
         lblLastpagetextblock_Internalname = "LASTPAGETEXTBLOCK";
         cellLastpagecell_Internalname = "LASTPAGECELL";
         lblNextpagebuttontextblock_Internalname = "NEXTPAGEBUTTONTEXTBLOCK";
         cellNextpagebuttoncell_Internalname = "NEXTPAGEBUTTONCELL";
         tblK2btoolspagingcontainertable_Internalname = "K2BTOOLSPAGINGCONTAINERTABLE";
         divTable4_Internalname = "TABLE4";
         divGlobalgridtable_Internalname = "GLOBALGRIDTABLE";
         divTable7_Internalname = "TABLE7";
         divTable2_Internalname = "TABLE2";
         K2borderbyusercontrol_Internalname = "K2BORDERBYUSERCONTROL";
         tblK2btoolsabstracthiddenitemsgrid_Internalname = "K2BTOOLSABSTRACTHIDDENITEMSGRID";
         edtavConfirmmessage_Internalname = "vCONFIRMMESSAGE";
         bttButtonconfirmyes_Internalname = "BUTTONCONFIRMYES";
         bttButtonconfirmno_Internalname = "BUTTONCONFIRMNO";
         tblConfirm_hidden_actionstable_Internalname = "CONFIRM_HIDDEN_ACTIONSTABLE";
         tblSection_condconf_dialog_inner_Internalname = "SECTION_CONDCONF_DIALOG_INNER";
         tblSection_condconf_dialog_Internalname = "SECTION_CONDCONF_DIALOG";
         tblTableconditionalconfirm_Internalname = "TABLECONDITIONALCONFIRM";
         K2bcontrolbeautify1_Internalname = "K2BCONTROLBEAUTIFY1";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         cmbPersonaPacienteOrigenSordera_Jsonclick = "";
         edtPersonaFechaNacimiento_Jsonclick = "";
         cmbPersonaTipo_Jsonclick = "";
         edtPersonaDPI_Jsonclick = "";
         edtPersonaSexo_Jsonclick = "";
         edtPersonaSApellido_Jsonclick = "";
         edtPersonaPApellido_Jsonclick = "";
         edtPersonaSNombre_Jsonclick = "";
         edtPersonaPNombre_Jsonclick = "";
         edtPersonaID_Jsonclick = "";
         chkavSelected.Visible = -1;
         chkavSelected.Enabled = 1;
         edtavPersonafechanacimiento_to_Jsonclick = "";
         edtavPersonafechanacimiento_to_Enabled = 1;
         edtavPersonafechanacimiento_from_Jsonclick = "";
         edtavPersonafechanacimiento_from_Enabled = 1;
         imgDelete_Visible = 1;
         imgUpdate_Visible = 1;
         bttInsert_Visible = 1;
         cmbavGridsettingsrowsperpagevariable_Jsonclick = "";
         cmbavGridsettingsrowsperpagevariable.Enabled = 1;
         cmbavGridsettingsorderedby_Jsonclick = "";
         cmbavGridsettingsorderedby.Enabled = 1;
         chkavAtt_personapacienteorigensordera_visible.Enabled = 1;
         chkavAtt_personafechanacimiento_visible.Enabled = 1;
         chkavAtt_personatipo_visible.Enabled = 1;
         chkavAtt_personadpi_visible.Enabled = 1;
         chkavAtt_personasexo_visible.Enabled = 1;
         chkavAtt_personasapellido_visible.Enabled = 1;
         chkavAtt_personapapellido_visible.Enabled = 1;
         chkavAtt_personasnombre_visible.Enabled = 1;
         chkavAtt_personapnombre_visible.Enabled = 1;
         chkavAtt_personaid_visible.Enabled = 1;
         imgExport_Visible = 1;
         imgReport_Visible = 1;
         divK2bgridsettingscontentoutertable_Visible = 1;
         subGrid_Allowcollapsing = 0;
         subGrid_Allowselection = 0;
         subGrid_Class = "Grid_WorkWith";
         subGrid_Backcolorstyle = 0;
         chkavCheckall.Enabled = 1;
         lblLastpagetextblock_Visible = 1;
         lblSpacingrighttextblock_Visible = 1;
         lblNextpagetextblock_Visible = 1;
         lblPreviouspagetextblock_Visible = 1;
         lblSpacinglefttextblock_Visible = 1;
         lblFirstpagetextblock_Visible = 1;
         edtavConfirmmessage_Jsonclick = "";
         edtavConfirmmessage_Enabled = 1;
         cmbPersonaPacienteOrigenSordera.Visible = -1;
         edtPersonaFechaNacimiento_Visible = -1;
         cmbPersonaTipo.Visible = -1;
         edtPersonaDPI_Visible = -1;
         edtPersonaSexo_Visible = -1;
         edtPersonaSApellido_Visible = -1;
         edtPersonaPApellido_Visible = -1;
         edtPersonaSNombre_Visible = -1;
         edtPersonaPNombre_Visible = -1;
         edtPersonaID_Visible = -1;
         tblK2btoolspagingcontainertable_Visible = 1;
         cellNextpagecell_Class = "K2BToolsCell_PaginationNext";
         cellSpacingrightcell_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
         cellLastpagecell_Class = "K2BToolsCell_PaginationRight";
         cellPreviouspagecell_Class = "K2BToolsCell_PaginationPrevious";
         cellSpacingleftcell_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
         cellFirstpagecell_Class = "K2BToolsCell_PaginationLeft";
         lblNextpagebuttontextblock_Class = "K2BToolsTextBlock_PaginationNormal";
         lblPreviouspagebuttontextblock_Class = "K2BToolsTextBlock_PaginationNormal";
         lblLastpagetextblock_Caption = "1";
         lblNextpagetextblock_Caption = "#";
         lblCurrentpagetextblock_Caption = "#";
         lblPreviouspagetextblock_Caption = "#";
         lblFirstpagetextblock_Caption = "1";
         tblNoresultsfoundtable_Visible = 1;
         tblTableconditionalconfirm_Visible = 1;
         imgDelete_Tooltiptext = "Eliminar";
         imgUpdate_Tooltiptext = "Modificar";
         bttInsert_Tooltiptext = "Agregar";
         imgExport_Tooltiptext = "Export";
         imgReport_Tooltiptext = "PDF Report";
         subGrid_Sortable = 0;
         chkavSelected.Caption = "";
         chkavCheckall.Caption = "";
         chkavAtt_personapacienteorigensordera_visible.Caption = "";
         chkavAtt_personafechanacimiento_visible.Caption = "";
         chkavAtt_personatipo_visible.Caption = "";
         chkavAtt_personadpi_visible.Caption = "";
         chkavAtt_personasexo_visible.Caption = "";
         chkavAtt_personasapellido_visible.Caption = "";
         chkavAtt_personapapellido_visible.Caption = "";
         chkavAtt_personasnombre_visible.Caption = "";
         chkavAtt_personapnombre_visible.Caption = "";
         chkavAtt_personaid_visible.Caption = "";
         edtavPersonapnombre_Jsonclick = "";
         edtavPersonapnombre_Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Personas";
         subGrid_Rows = 0;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''}],oparms:[{av:'imgReport_Tooltiptext',ctrl:'REPORT',prop:'Tooltiptext'},{av:'imgExport_Tooltiptext',ctrl:'EXPORT',prop:'Tooltiptext'},{ctrl:'INSERT',prop:'Tooltiptext'},{av:'imgUpdate_Tooltiptext',ctrl:'UPDATE',prop:'Tooltiptext'},{av:'imgDelete_Tooltiptext',ctrl:'DELETE',prop:'Tooltiptext'},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'divK2bgridsettingscontentoutertable_Visible',ctrl:'K2BGRIDSETTINGSCONTENTOUTERTABLE',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'edtPersonaID_Visible',ctrl:'PERSONAID',prop:'Visible'},{av:'AV20Att_PersonaID_Visible',fld:'vATT_PERSONAID_VISIBLE',pic:'',nv:false},{av:'edtPersonaPNombre_Visible',ctrl:'PERSONAPNOMBRE',prop:'Visible'},{av:'AV21Att_PersonaPNombre_Visible',fld:'vATT_PERSONAPNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaSNombre_Visible',ctrl:'PERSONASNOMBRE',prop:'Visible'},{av:'AV22Att_PersonaSNombre_Visible',fld:'vATT_PERSONASNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaPApellido_Visible',ctrl:'PERSONAPAPELLIDO',prop:'Visible'},{av:'AV23Att_PersonaPApellido_Visible',fld:'vATT_PERSONAPAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSApellido_Visible',ctrl:'PERSONASAPELLIDO',prop:'Visible'},{av:'AV24Att_PersonaSApellido_Visible',fld:'vATT_PERSONASAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSexo_Visible',ctrl:'PERSONASEXO',prop:'Visible'},{av:'AV25Att_PersonaSexo_Visible',fld:'vATT_PERSONASEXO_VISIBLE',pic:'',nv:false},{av:'edtPersonaDPI_Visible',ctrl:'PERSONADPI',prop:'Visible'},{av:'AV26Att_PersonaDPI_Visible',fld:'vATT_PERSONADPI_VISIBLE',pic:'',nv:false},{av:'cmbPersonaTipo'},{av:'AV27Att_PersonaTipo_Visible',fld:'vATT_PERSONATIPO_VISIBLE',pic:'',nv:false},{av:'edtPersonaFechaNacimiento_Visible',ctrl:'PERSONAFECHANACIMIENTO',prop:'Visible'},{av:'AV28Att_PersonaFechaNacimiento_Visible',fld:'vATT_PERSONAFECHANACIMIENTO_VISIBLE',pic:'',nv:false},{av:'cmbPersonaPacienteOrigenSordera'},{av:'AV29Att_PersonaPacienteOrigenSordera_Visible',fld:'vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE',pic:'',nv:false},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'},{av:'imgReport_Visible',ctrl:'REPORT',prop:'Visible'},{av:'imgExport_Visible',ctrl:'EXPORT',prop:'Visible'},{ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("GRID.REFRESH","{handler:'E251E2',iparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''}],oparms:[{av:'imgReport_Tooltiptext',ctrl:'REPORT',prop:'Tooltiptext'},{av:'imgExport_Tooltiptext',ctrl:'EXPORT',prop:'Tooltiptext'},{ctrl:'INSERT',prop:'Tooltiptext'},{av:'imgUpdate_Tooltiptext',ctrl:'UPDATE',prop:'Tooltiptext'},{av:'imgDelete_Tooltiptext',ctrl:'DELETE',prop:'Tooltiptext'},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'tblNoresultsfoundtable_Visible',ctrl:'NORESULTSFOUNDTABLE',prop:'Visible'},{av:'AV50UC_OrderedBy',fld:'vUC_ORDEREDBY',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'edtPersonaID_Visible',ctrl:'PERSONAID',prop:'Visible'},{av:'AV20Att_PersonaID_Visible',fld:'vATT_PERSONAID_VISIBLE',pic:'',nv:false},{av:'edtPersonaPNombre_Visible',ctrl:'PERSONAPNOMBRE',prop:'Visible'},{av:'AV21Att_PersonaPNombre_Visible',fld:'vATT_PERSONAPNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaSNombre_Visible',ctrl:'PERSONASNOMBRE',prop:'Visible'},{av:'AV22Att_PersonaSNombre_Visible',fld:'vATT_PERSONASNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaPApellido_Visible',ctrl:'PERSONAPAPELLIDO',prop:'Visible'},{av:'AV23Att_PersonaPApellido_Visible',fld:'vATT_PERSONAPAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSApellido_Visible',ctrl:'PERSONASAPELLIDO',prop:'Visible'},{av:'AV24Att_PersonaSApellido_Visible',fld:'vATT_PERSONASAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSexo_Visible',ctrl:'PERSONASEXO',prop:'Visible'},{av:'AV25Att_PersonaSexo_Visible',fld:'vATT_PERSONASEXO_VISIBLE',pic:'',nv:false},{av:'edtPersonaDPI_Visible',ctrl:'PERSONADPI',prop:'Visible'},{av:'AV26Att_PersonaDPI_Visible',fld:'vATT_PERSONADPI_VISIBLE',pic:'',nv:false},{av:'cmbPersonaTipo'},{av:'AV27Att_PersonaTipo_Visible',fld:'vATT_PERSONATIPO_VISIBLE',pic:'',nv:false},{av:'edtPersonaFechaNacimiento_Visible',ctrl:'PERSONAFECHANACIMIENTO',prop:'Visible'},{av:'AV28Att_PersonaFechaNacimiento_Visible',fld:'vATT_PERSONAFECHANACIMIENTO_VISIBLE',pic:'',nv:false},{av:'cmbPersonaPacienteOrigenSordera'},{av:'AV29Att_PersonaPacienteOrigenSordera_Visible',fld:'vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE',pic:'',nv:false},{av:'imgReport_Visible',ctrl:'REPORT',prop:'Visible'},{av:'imgExport_Visible',ctrl:'EXPORT',prop:'Visible'},{ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E261E2',iparms:[{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'A1PersonaID',fld:'PERSONAID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0}],oparms:[{av:'tblNoresultsfoundtable_Visible',ctrl:'NORESULTSFOUNDTABLE',prop:'Visible'},{av:'AV33Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'}]}");
         setEventMetadata("'DOFIRST'","{handler:'E141E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0}],oparms:[{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'}]}");
         setEventMetadata("'DOPREVIOUS'","{handler:'E151E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0}],oparms:[{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'}]}");
         setEventMetadata("'DONEXT'","{handler:'E161E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0}],oparms:[{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'}]}");
         setEventMetadata("'DOLAST'","{handler:'E171E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0}],oparms:[{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'}]}");
         setEventMetadata("'DOINSERT'","{handler:'E111E2',iparms:[{av:'A1PersonaID',fld:'PERSONAID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("'DOUPDATE'","{handler:'E181E2',iparms:[{av:'AV34SelectedRows',fld:'vSELECTEDROWS',pic:'',nv:null},{av:'AV33Selected',fld:'vSELECTED',grid:167,pic:'',nv:false},{av:'A1PersonaID',fld:'PERSONAID',grid:167,pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A3PersonaSNombre',fld:'PERSONASNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A5PersonaSApellido',fld:'PERSONASAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A6PersonaSexo',fld:'PERSONASEXO',grid:167,pic:'',hsh:true,nv:''},{av:'A7PersonaDPI',fld:'PERSONADPI',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaTipo'},{av:'A8PersonaTipo',fld:'PERSONATIPO',grid:167,pic:'ZZZ9',hsh:true,nv:0},{av:'A9PersonaFechaNacimiento',fld:'PERSONAFECHANACIMIENTO',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaPacienteOrigenSordera'},{av:'A10PersonaPacienteOrigenSordera',fld:'PERSONAPACIENTEORIGENSORDERA',grid:167,pic:'ZZZ9',hsh:true,nv:0}],oparms:[{av:'AV34SelectedRows',fld:'vSELECTEDROWS',pic:'',nv:null}]}");
         setEventMetadata("'DODELETE'","{handler:'E191E2',iparms:[{av:'AV34SelectedRows',fld:'vSELECTEDROWS',pic:'',nv:null},{av:'AV33Selected',fld:'vSELECTED',grid:167,pic:'',nv:false},{av:'A1PersonaID',fld:'PERSONAID',grid:167,pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A3PersonaSNombre',fld:'PERSONASNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A5PersonaSApellido',fld:'PERSONASAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A6PersonaSexo',fld:'PERSONASEXO',grid:167,pic:'',hsh:true,nv:''},{av:'A7PersonaDPI',fld:'PERSONADPI',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaTipo'},{av:'A8PersonaTipo',fld:'PERSONATIPO',grid:167,pic:'ZZZ9',hsh:true,nv:0},{av:'A9PersonaFechaNacimiento',fld:'PERSONAFECHANACIMIENTO',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaPacienteOrigenSordera'},{av:'A10PersonaPacienteOrigenSordera',fld:'PERSONAPACIENTEORIGENSORDERA',grid:167,pic:'ZZZ9',hsh:true,nv:0}],oparms:[{av:'AV53ConfirmMessage',fld:'vCONFIRMMESSAGE',pic:'',nv:''},{av:'AV52ConfirmationSubId',fld:'vCONFIRMATIONSUBID',pic:'',nv:''},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'AV34SelectedRows',fld:'vSELECTEDROWS',pic:'',nv:null}]}");
         setEventMetadata("PERSONAPNOMBRE.CLICK","{handler:'E271E2',iparms:[{av:'A1PersonaID',fld:'PERSONAID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("'DOEXPORT'","{handler:'E121E2',iparms:[{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("'DOREPORT'","{handler:'E311E1',iparms:[{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0}],oparms:[]}");
         setEventMetadata("VSELECTED.CLICK","{handler:'E281E2',iparms:[{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'A1PersonaID',fld:'PERSONAID',grid:167,pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'AV33Selected',fld:'vSELECTED',grid:167,pic:'',nv:false},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A3PersonaSNombre',fld:'PERSONASNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A5PersonaSApellido',fld:'PERSONASAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A6PersonaSexo',fld:'PERSONASEXO',grid:167,pic:'',hsh:true,nv:''},{av:'A7PersonaDPI',fld:'PERSONADPI',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaTipo'},{av:'A8PersonaTipo',fld:'PERSONATIPO',grid:167,pic:'ZZZ9',hsh:true,nv:0},{av:'A9PersonaFechaNacimiento',fld:'PERSONAFECHANACIMIENTO',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaPacienteOrigenSordera'},{av:'A10PersonaPacienteOrigenSordera',fld:'PERSONAPACIENTEORIGENSORDERA',grid:167,pic:'ZZZ9',hsh:true,nv:0}],oparms:[{av:'AV51CheckAll',fld:'vCHECKALL',pic:'',nv:false},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'imgReport_Visible',ctrl:'REPORT',prop:'Visible'},{av:'imgExport_Visible',ctrl:'EXPORT',prop:'Visible'},{ctrl:'INSERT',prop:'Visible'},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'}]}");
         setEventMetadata("VCHECKALL.CLICK","{handler:'E201E2',iparms:[{av:'AV33Selected',fld:'vSELECTED',grid:167,pic:'',nv:false},{av:'AV51CheckAll',fld:'vCHECKALL',pic:'',nv:false},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'A1PersonaID',fld:'PERSONAID',grid:167,pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A3PersonaSNombre',fld:'PERSONASNOMBRE',grid:167,pic:'',hsh:true,nv:''},{av:'A4PersonaPApellido',fld:'PERSONAPAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A5PersonaSApellido',fld:'PERSONASAPELLIDO',grid:167,pic:'',hsh:true,nv:''},{av:'A6PersonaSexo',fld:'PERSONASEXO',grid:167,pic:'',hsh:true,nv:''},{av:'A7PersonaDPI',fld:'PERSONADPI',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaTipo'},{av:'A8PersonaTipo',fld:'PERSONATIPO',grid:167,pic:'ZZZ9',hsh:true,nv:0},{av:'A9PersonaFechaNacimiento',fld:'PERSONAFECHANACIMIENTO',grid:167,pic:'',hsh:true,nv:''},{av:'cmbPersonaPacienteOrigenSordera'},{av:'A10PersonaPacienteOrigenSordera',fld:'PERSONAPACIENTEORIGENSORDERA',grid:167,pic:'ZZZ9',hsh:true,nv:0}],oparms:[{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'AV33Selected',fld:'vSELECTED',pic:'',nv:false},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'}]}");
         setEventMetadata("'TOGGLEGRIDSETTINGSTABLE'","{handler:'E301E1',iparms:[{av:'divK2bgridsettingscontentoutertable_Visible',ctrl:'K2BGRIDSETTINGSCONTENTOUTERTABLE',prop:'Visible'}],oparms:[{av:'divK2bgridsettingscontentoutertable_Visible',ctrl:'K2BGRIDSETTINGSCONTENTOUTERTABLE',prop:'Visible'}]}");
         setEventMetadata("'SAVEGRIDSETTINGS'","{handler:'E211E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'AV20Att_PersonaID_Visible',fld:'vATT_PERSONAID_VISIBLE',pic:'',nv:false},{av:'AV21Att_PersonaPNombre_Visible',fld:'vATT_PERSONAPNOMBRE_VISIBLE',pic:'',nv:false},{av:'AV22Att_PersonaSNombre_Visible',fld:'vATT_PERSONASNOMBRE_VISIBLE',pic:'',nv:false},{av:'AV23Att_PersonaPApellido_Visible',fld:'vATT_PERSONAPAPELLIDO_VISIBLE',pic:'',nv:false},{av:'AV24Att_PersonaSApellido_Visible',fld:'vATT_PERSONASAPELLIDO_VISIBLE',pic:'',nv:false},{av:'AV25Att_PersonaSexo_Visible',fld:'vATT_PERSONASEXO_VISIBLE',pic:'',nv:false},{av:'AV26Att_PersonaDPI_Visible',fld:'vATT_PERSONADPI_VISIBLE',pic:'',nv:false},{av:'AV27Att_PersonaTipo_Visible',fld:'vATT_PERSONATIPO_VISIBLE',pic:'',nv:false},{av:'AV28Att_PersonaFechaNacimiento_Visible',fld:'vATT_PERSONAFECHANACIMIENTO_VISIBLE',pic:'',nv:false},{av:'AV29Att_PersonaPacienteOrigenSordera_Visible',fld:'vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE',pic:'',nv:false},{av:'cmbavGridsettingsorderedby'},{av:'AV13GridSettingsOrderedBy',fld:'vGRIDSETTINGSORDEREDBY',pic:'ZZZ9',nv:0},{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'}],oparms:[{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV50UC_OrderedBy',fld:'vUC_ORDEREDBY',pic:'ZZZ9',nv:0},{av:'subGrid_Rows',ctrl:'GRID',prop:'Rows'},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'divK2bgridsettingscontentoutertable_Visible',ctrl:'K2BGRIDSETTINGSCONTENTOUTERTABLE',prop:'Visible'},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'edtPersonaID_Visible',ctrl:'PERSONAID',prop:'Visible'},{av:'AV20Att_PersonaID_Visible',fld:'vATT_PERSONAID_VISIBLE',pic:'',nv:false},{av:'edtPersonaPNombre_Visible',ctrl:'PERSONAPNOMBRE',prop:'Visible'},{av:'AV21Att_PersonaPNombre_Visible',fld:'vATT_PERSONAPNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaSNombre_Visible',ctrl:'PERSONASNOMBRE',prop:'Visible'},{av:'AV22Att_PersonaSNombre_Visible',fld:'vATT_PERSONASNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaPApellido_Visible',ctrl:'PERSONAPAPELLIDO',prop:'Visible'},{av:'AV23Att_PersonaPApellido_Visible',fld:'vATT_PERSONAPAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSApellido_Visible',ctrl:'PERSONASAPELLIDO',prop:'Visible'},{av:'AV24Att_PersonaSApellido_Visible',fld:'vATT_PERSONASAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSexo_Visible',ctrl:'PERSONASEXO',prop:'Visible'},{av:'AV25Att_PersonaSexo_Visible',fld:'vATT_PERSONASEXO_VISIBLE',pic:'',nv:false},{av:'edtPersonaDPI_Visible',ctrl:'PERSONADPI',prop:'Visible'},{av:'AV26Att_PersonaDPI_Visible',fld:'vATT_PERSONADPI_VISIBLE',pic:'',nv:false},{av:'cmbPersonaTipo'},{av:'AV27Att_PersonaTipo_Visible',fld:'vATT_PERSONATIPO_VISIBLE',pic:'',nv:false},{av:'edtPersonaFechaNacimiento_Visible',ctrl:'PERSONAFECHANACIMIENTO',prop:'Visible'},{av:'AV28Att_PersonaFechaNacimiento_Visible',fld:'vATT_PERSONAFECHANACIMIENTO_VISIBLE',pic:'',nv:false},{av:'cmbPersonaPacienteOrigenSordera'},{av:'AV29Att_PersonaPacienteOrigenSordera_Visible',fld:'vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE',pic:'',nv:false},{av:'imgReport_Tooltiptext',ctrl:'REPORT',prop:'Tooltiptext'},{av:'imgExport_Tooltiptext',ctrl:'EXPORT',prop:'Tooltiptext'},{ctrl:'INSERT',prop:'Tooltiptext'},{av:'imgUpdate_Tooltiptext',ctrl:'UPDATE',prop:'Tooltiptext'},{av:'imgDelete_Tooltiptext',ctrl:'DELETE',prop:'Tooltiptext'},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'},{av:'imgReport_Visible',ctrl:'REPORT',prop:'Visible'},{av:'imgExport_Visible',ctrl:'EXPORT',prop:'Visible'},{ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("'CONFIRMNO'","{handler:'E291E1',iparms:[],oparms:[{av:'AV52ConfirmationSubId',fld:'vCONFIRMATIONSUBID',pic:'',nv:''},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'}]}");
         setEventMetadata("'CONFIRMYES'","{handler:'E221E2',iparms:[{av:'AV52ConfirmationSubId',fld:'vCONFIRMATIONSUBID',pic:'',nv:''},{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'AV34SelectedRows',fld:'vSELECTEDROWS',pic:'',nv:null}],oparms:[{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'imgReport_Tooltiptext',ctrl:'REPORT',prop:'Tooltiptext'},{av:'imgExport_Tooltiptext',ctrl:'EXPORT',prop:'Tooltiptext'},{ctrl:'INSERT',prop:'Tooltiptext'},{av:'imgUpdate_Tooltiptext',ctrl:'UPDATE',prop:'Tooltiptext'},{av:'imgDelete_Tooltiptext',ctrl:'DELETE',prop:'Tooltiptext'},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'divK2bgridsettingscontentoutertable_Visible',ctrl:'K2BGRIDSETTINGSCONTENTOUTERTABLE',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'edtPersonaID_Visible',ctrl:'PERSONAID',prop:'Visible'},{av:'AV20Att_PersonaID_Visible',fld:'vATT_PERSONAID_VISIBLE',pic:'',nv:false},{av:'edtPersonaPNombre_Visible',ctrl:'PERSONAPNOMBRE',prop:'Visible'},{av:'AV21Att_PersonaPNombre_Visible',fld:'vATT_PERSONAPNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaSNombre_Visible',ctrl:'PERSONASNOMBRE',prop:'Visible'},{av:'AV22Att_PersonaSNombre_Visible',fld:'vATT_PERSONASNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaPApellido_Visible',ctrl:'PERSONAPAPELLIDO',prop:'Visible'},{av:'AV23Att_PersonaPApellido_Visible',fld:'vATT_PERSONAPAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSApellido_Visible',ctrl:'PERSONASAPELLIDO',prop:'Visible'},{av:'AV24Att_PersonaSApellido_Visible',fld:'vATT_PERSONASAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSexo_Visible',ctrl:'PERSONASEXO',prop:'Visible'},{av:'AV25Att_PersonaSexo_Visible',fld:'vATT_PERSONASEXO_VISIBLE',pic:'',nv:false},{av:'edtPersonaDPI_Visible',ctrl:'PERSONADPI',prop:'Visible'},{av:'AV26Att_PersonaDPI_Visible',fld:'vATT_PERSONADPI_VISIBLE',pic:'',nv:false},{av:'cmbPersonaTipo'},{av:'AV27Att_PersonaTipo_Visible',fld:'vATT_PERSONATIPO_VISIBLE',pic:'',nv:false},{av:'edtPersonaFechaNacimiento_Visible',ctrl:'PERSONAFECHANACIMIENTO',prop:'Visible'},{av:'AV28Att_PersonaFechaNacimiento_Visible',fld:'vATT_PERSONAFECHANACIMIENTO_VISIBLE',pic:'',nv:false},{av:'cmbPersonaPacienteOrigenSordera'},{av:'AV29Att_PersonaPacienteOrigenSordera_Visible',fld:'vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE',pic:'',nv:false},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'},{av:'imgReport_Visible',ctrl:'REPORT',prop:'Visible'},{av:'imgExport_Visible',ctrl:'EXPORT',prop:'Visible'},{ctrl:'INSERT',prop:'Visible'}]}");
         setEventMetadata("K2BORDERBYUSERCONTROL.ORDERBYCHANGED","{handler:'E131E2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'subGrid_Rows',nv:0},{av:'AV8PersonaPNombre',fld:'vPERSONAPNOMBRE',pic:'',nv:''},{av:'AV10PersonaFechaNacimiento_From',fld:'vPERSONAFECHANACIMIENTO_FROM',pic:'',nv:''},{av:'AV11PersonaFechaNacimiento_To',fld:'vPERSONAFECHANACIMIENTO_TO',pic:'',nv:''},{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'AV61Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'cmbavGridsettingsrowsperpagevariable'},{av:'AV41GridSettingsRowsPerPageVariable',fld:'vGRIDSETTINGSROWSPERPAGEVARIABLE',pic:'ZZZZZZZZZ9',nv:0},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'AV37AllSelectedRows',fld:'vALLSELECTEDROWS',pic:'',nv:null},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'AV50UC_OrderedBy',fld:'vUC_ORDEREDBY',pic:'ZZZ9',nv:0}],oparms:[{av:'AV12OrderedBy',fld:'vORDEREDBY',pic:'ZZZ9',nv:0},{av:'cmbavGridsettingsorderedby'},{av:'AV13GridSettingsOrderedBy',fld:'vGRIDSETTINGSORDEREDBY',pic:'ZZZ9',nv:0},{av:'imgReport_Tooltiptext',ctrl:'REPORT',prop:'Tooltiptext'},{av:'imgExport_Tooltiptext',ctrl:'EXPORT',prop:'Tooltiptext'},{ctrl:'INSERT',prop:'Tooltiptext'},{av:'imgUpdate_Tooltiptext',ctrl:'UPDATE',prop:'Tooltiptext'},{av:'imgDelete_Tooltiptext',ctrl:'DELETE',prop:'Tooltiptext'},{av:'tblTableconditionalconfirm_Visible',ctrl:'TABLECONDITIONALCONFIRM',prop:'Visible'},{av:'AV38SelectedRowsAmount',fld:'vSELECTEDROWSAMOUNT',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'divK2bgridsettingscontentoutertable_Visible',ctrl:'K2BGRIDSETTINGSCONTENTOUTERTABLE',prop:'Visible'},{av:'imgUpdate_Visible',ctrl:'UPDATE',prop:'Visible'},{av:'imgDelete_Visible',ctrl:'DELETE',prop:'Visible'},{av:'AV14GridColumns',fld:'vGRIDCOLUMNS',pic:'',nv:null},{av:'AV18AttributeName',fld:'vATTRIBUTENAME',pic:'',nv:''},{av:'AV19ColumnTitle',fld:'vCOLUMNTITLE',pic:'',nv:''},{av:'edtPersonaID_Visible',ctrl:'PERSONAID',prop:'Visible'},{av:'AV20Att_PersonaID_Visible',fld:'vATT_PERSONAID_VISIBLE',pic:'',nv:false},{av:'edtPersonaPNombre_Visible',ctrl:'PERSONAPNOMBRE',prop:'Visible'},{av:'AV21Att_PersonaPNombre_Visible',fld:'vATT_PERSONAPNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaSNombre_Visible',ctrl:'PERSONASNOMBRE',prop:'Visible'},{av:'AV22Att_PersonaSNombre_Visible',fld:'vATT_PERSONASNOMBRE_VISIBLE',pic:'',nv:false},{av:'edtPersonaPApellido_Visible',ctrl:'PERSONAPAPELLIDO',prop:'Visible'},{av:'AV23Att_PersonaPApellido_Visible',fld:'vATT_PERSONAPAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSApellido_Visible',ctrl:'PERSONASAPELLIDO',prop:'Visible'},{av:'AV24Att_PersonaSApellido_Visible',fld:'vATT_PERSONASAPELLIDO_VISIBLE',pic:'',nv:false},{av:'edtPersonaSexo_Visible',ctrl:'PERSONASEXO',prop:'Visible'},{av:'AV25Att_PersonaSexo_Visible',fld:'vATT_PERSONASEXO_VISIBLE',pic:'',nv:false},{av:'edtPersonaDPI_Visible',ctrl:'PERSONADPI',prop:'Visible'},{av:'AV26Att_PersonaDPI_Visible',fld:'vATT_PERSONADPI_VISIBLE',pic:'',nv:false},{av:'cmbPersonaTipo'},{av:'AV27Att_PersonaTipo_Visible',fld:'vATT_PERSONATIPO_VISIBLE',pic:'',nv:false},{av:'edtPersonaFechaNacimiento_Visible',ctrl:'PERSONAFECHANACIMIENTO',prop:'Visible'},{av:'AV28Att_PersonaFechaNacimiento_Visible',fld:'vATT_PERSONAFECHANACIMIENTO_VISIBLE',pic:'',nv:false},{av:'cmbPersonaPacienteOrigenSordera'},{av:'AV29Att_PersonaPacienteOrigenSordera_Visible',fld:'vATT_PERSONAPACIENTEORIGENSORDERA_VISIBLE',pic:'',nv:false},{av:'AV44CurrentPage',fld:'vCURRENTPAGE',pic:'ZZZZ9',nv:0},{av:'lblFirstpagetextblock_Caption',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagetextblock_Caption',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Caption'},{av:'lblCurrentpagetextblock_Caption',ctrl:'CURRENTPAGETEXTBLOCK',prop:'Caption'},{av:'lblNextpagetextblock_Caption',ctrl:'NEXTPAGETEXTBLOCK',prop:'Caption'},{av:'lblLastpagetextblock_Caption',ctrl:'LASTPAGETEXTBLOCK',prop:'Caption'},{av:'lblPreviouspagebuttontextblock_Class',ctrl:'PREVIOUSPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'lblNextpagebuttontextblock_Class',ctrl:'NEXTPAGEBUTTONTEXTBLOCK',prop:'Class'},{av:'cellFirstpagecell_Class',ctrl:'FIRSTPAGECELL',prop:'Class'},{av:'lblFirstpagetextblock_Visible',ctrl:'FIRSTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingleftcell_Class',ctrl:'SPACINGLEFTCELL',prop:'Class'},{av:'lblSpacinglefttextblock_Visible',ctrl:'SPACINGLEFTTEXTBLOCK',prop:'Visible'},{av:'cellPreviouspagecell_Class',ctrl:'PREVIOUSPAGECELL',prop:'Class'},{av:'lblPreviouspagetextblock_Visible',ctrl:'PREVIOUSPAGETEXTBLOCK',prop:'Visible'},{av:'cellLastpagecell_Class',ctrl:'LASTPAGECELL',prop:'Class'},{av:'lblLastpagetextblock_Visible',ctrl:'LASTPAGETEXTBLOCK',prop:'Visible'},{av:'cellSpacingrightcell_Class',ctrl:'SPACINGRIGHTCELL',prop:'Class'},{av:'lblSpacingrighttextblock_Visible',ctrl:'SPACINGRIGHTTEXTBLOCK',prop:'Visible'},{av:'cellNextpagecell_Class',ctrl:'NEXTPAGECELL',prop:'Class'},{av:'lblNextpagetextblock_Visible',ctrl:'NEXTPAGETEXTBLOCK',prop:'Visible'},{av:'tblK2btoolspagingcontainertable_Visible',ctrl:'K2BTOOLSPAGINGCONTAINERTABLE',prop:'Visible'},{av:'imgReport_Visible',ctrl:'REPORT',prop:'Visible'},{av:'imgExport_Visible',ctrl:'EXPORT',prop:'Visible'},{ctrl:'INSERT',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV8PersonaPNombre = "";
         AV10PersonaFechaNacimiento_From = DateTime.MinValue;
         AV11PersonaFechaNacimiento_To = DateTime.MinValue;
         AV61Pgmname = "";
         AV14GridColumns = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2");
         AV18AttributeName = "";
         AV19ColumnTitle = "";
         AV37AllSelectedRows = new GXBaseCollection<SdtWWPersonaSDT_WWPersonaSDTItem>( context, "WWPersonaSDTItem", "PACYE2");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         AV48GridOrders = new GXBaseCollection<SdtK2BGridOrders_K2BGridOrdersItem>( context, "K2BGridOrdersItem", "PACYE2");
         AV34SelectedRows = new GXBaseCollection<SdtWWPersonaSDT_WWPersonaSDTItem>( context, "WWPersonaSDTItem", "PACYE2");
         AV52ConfirmationSubId = "";
         K2borderbyusercontrol_Gridcontrolname = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblPgmdescriptortextblock_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         lblTextblockpersonafechanacimiento_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         A1PersonaID = (Guid)(Guid.Empty);
         A2PersonaPNombre = "";
         A3PersonaSNombre = "";
         A4PersonaPApellido = "";
         A5PersonaSApellido = "";
         A6PersonaSexo = "";
         A7PersonaDPI = "";
         A9PersonaFechaNacimiento = DateTime.MinValue;
         GXCCtl = "";
         GridContainer = new GXWebGrid( context);
         scmdbuf = "";
         lV8PersonaPNombre = "";
         H001E2_A10PersonaPacienteOrigenSordera = new short[1] ;
         H001E2_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         H001E2_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         H001E2_n9PersonaFechaNacimiento = new bool[] {false} ;
         H001E2_A8PersonaTipo = new short[1] ;
         H001E2_A7PersonaDPI = new String[] {""} ;
         H001E2_n7PersonaDPI = new bool[] {false} ;
         H001E2_A6PersonaSexo = new String[] {""} ;
         H001E2_n6PersonaSexo = new bool[] {false} ;
         H001E2_A5PersonaSApellido = new String[] {""} ;
         H001E2_n5PersonaSApellido = new bool[] {false} ;
         H001E2_A4PersonaPApellido = new String[] {""} ;
         H001E2_A3PersonaSNombre = new String[] {""} ;
         H001E2_n3PersonaSNombre = new bool[] {false} ;
         H001E2_A2PersonaPNombre = new String[] {""} ;
         H001E2_A1PersonaID = new Guid[] {Guid.Empty} ;
         H001E3_AGRID_nRecordCount = new long[1] ;
         AV53ConfirmMessage = "";
         AV5Context = new SdtK2BContext(context);
         AV49GridOrder = new SdtK2BGridOrders_K2BGridOrdersItem(context);
         AV39Messages = new GXBaseCollection<SdtMessages_Message>( context, "Message", "GeneXus");
         GXt_objcol_SdtMessages_Message1 = new GXBaseCollection<SdtMessages_Message>( context, "Message", "GeneXus");
         AV40Message = new SdtMessages_Message(context);
         AV46ActivityList = new GXBaseCollection<SdtK2BActivityList_K2BActivityListItem>( context, "K2BActivityListItem", "PACYE2");
         AV47ActivityListItem = new SdtK2BActivityList_K2BActivityListItem(context);
         AV35SelectedRow = new SdtWWPersonaSDT_WWPersonaSDTItem(context);
         GridRow = new GXWebRow();
         AV7HTTPRequest = new GxHttpRequest( context);
         AV30GridStateKey = "";
         AV31GridState = new SdtK2BGridState(context);
         AV32GridStateFilterValue = new SdtK2BGridState_FilterValue(context);
         AV42TrnContext = new SdtK2BTrnContext(context);
         AV15GridColumn = new SdtK2BGridColumns_K2BGridColumnsItem(context);
         AV56MultipleDelete_BC = new SdtPersona(context);
         AV57ErrorMessage = "";
         AV73GXV7 = new GXBaseCollection<SdtMessages_Message>( context, "Message", "GeneXus");
         sStyleString = "";
         bttButtonconfirmyes_Jsonclick = "";
         bttButtonconfirmno_Jsonclick = "";
         lblPreviouspagebuttontextblock_Jsonclick = "";
         lblFirstpagetextblock_Jsonclick = "";
         lblSpacinglefttextblock_Jsonclick = "";
         lblPreviouspagetextblock_Jsonclick = "";
         lblCurrentpagetextblock_Jsonclick = "";
         lblNextpagetextblock_Jsonclick = "";
         lblSpacingrighttextblock_Jsonclick = "";
         lblLastpagetextblock_Jsonclick = "";
         lblNextpagebuttontextblock_Jsonclick = "";
         lblNoresultsfoundtextblock_Jsonclick = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         lblK2bgridsettingslabel_Jsonclick = "";
         bttK2bgridsettingssave_Jsonclick = "";
         sImgUrl = "";
         imgReport_Jsonclick = "";
         imgExport_Jsonclick = "";
         lblPersonaidruntimecolumnatt_Jsonclick = "";
         lblPersonapnombreruntimecolumnatt_Jsonclick = "";
         lblPersonasnombreruntimecolumnatt_Jsonclick = "";
         lblPersonapapellidoruntimecolumnatt_Jsonclick = "";
         lblPersonasapellidoruntimecolumnatt_Jsonclick = "";
         lblPersonasexoruntimecolumnatt_Jsonclick = "";
         lblPersonadpiruntimecolumnatt_Jsonclick = "";
         lblPersonatiporuntimecolumnatt_Jsonclick = "";
         lblPersonafechanacimientoruntimecolumnatt_Jsonclick = "";
         lblPersonapacienteorigensorderaruntimecolumnatt_Jsonclick = "";
         lblOrderbytext_Jsonclick = "";
         lblRowsperpagegrid_Jsonclick = "";
         bttInsert_Jsonclick = "";
         imgUpdate_Jsonclick = "";
         imgDelete_Jsonclick = "";
         lblDaterangeseparator_personafechanacimiento_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.wwpersona__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wwpersona__default(),
            new Object[][] {
                new Object[] {
               H001E2_A10PersonaPacienteOrigenSordera, H001E2_n10PersonaPacienteOrigenSordera, H001E2_A9PersonaFechaNacimiento, H001E2_n9PersonaFechaNacimiento, H001E2_A8PersonaTipo, H001E2_A7PersonaDPI, H001E2_n7PersonaDPI, H001E2_A6PersonaSexo, H001E2_n6PersonaSexo, H001E2_A5PersonaSApellido,
               H001E2_n5PersonaSApellido, H001E2_A4PersonaPApellido, H001E2_A3PersonaSNombre, H001E2_n3PersonaSNombre, H001E2_A2PersonaPNombre, H001E2_A1PersonaID
               }
               , new Object[] {
               H001E3_AGRID_nRecordCount
               }
            }
         );
         AV61Pgmname = "WWPersona";
         /* GeneXus formulas. */
         AV61Pgmname = "WWPersona";
         context.Gx_err = 0;
         edtavConfirmmessage_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_167 ;
      private short nGXsfl_167_idx=1 ;
      private short GRID_nEOF ;
      private short AV12OrderedBy ;
      private short AV38SelectedRowsAmount ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short AV50UC_OrderedBy ;
      private short wbEnd ;
      private short wbStart ;
      private short A8PersonaTipo ;
      private short A10PersonaPacienteOrigenSordera ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV13GridSettingsOrderedBy ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Sortable ;
      private short nGXsfl_167_fel_idx=1 ;
      private short AV17CurrentColumn ;
      private short AV36Index ;
      private short AV54DeletedCount ;
      private short AV55ErrorCount ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int divK2bgridsettingscontentoutertable_Visible ;
      private int subGrid_Rows ;
      private int AV44CurrentPage ;
      private int edtavPersonapnombre_Enabled ;
      private int subGrid_Islastpage ;
      private int edtavConfirmmessage_Enabled ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int AV60GXV1 ;
      private int tblTableconditionalconfirm_Visible ;
      private int tblNoresultsfoundtable_Visible ;
      private int AV62GXV2 ;
      private int AV63GXV3 ;
      private int AV45K2BMaxPages ;
      private int lblFirstpagetextblock_Visible ;
      private int lblSpacinglefttextblock_Visible ;
      private int lblPreviouspagetextblock_Visible ;
      private int lblLastpagetextblock_Visible ;
      private int lblSpacingrighttextblock_Visible ;
      private int lblNextpagetextblock_Visible ;
      private int tblK2btoolspagingcontainertable_Visible ;
      private int edtPersonaID_Visible ;
      private int edtPersonaPNombre_Visible ;
      private int edtPersonaSNombre_Visible ;
      private int edtPersonaPApellido_Visible ;
      private int edtPersonaSApellido_Visible ;
      private int edtPersonaSexo_Visible ;
      private int edtPersonaDPI_Visible ;
      private int edtPersonaFechaNacimiento_Visible ;
      private int AV65GXV4 ;
      private int imgUpdate_Visible ;
      private int imgDelete_Visible ;
      private int imgReport_Visible ;
      private int imgExport_Visible ;
      private int bttInsert_Visible ;
      private int AV71GXV5 ;
      private int AV72GXV6 ;
      private int AV74GXV8 ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int edtavPersonafechanacimiento_from_Enabled ;
      private int edtavPersonafechanacimiento_to_Enabled ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nFirstRecordOnPage ;
      private long AV41GridSettingsRowsPerPageVariable ;
      private long GRID_nCurrentRecord ;
      private long GRID_nRecordCount ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_167_idx="0001" ;
      private String AV61Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String AV52ConfirmationSubId ;
      private String K2borderbyusercontrol_Gridcontrolname ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String lblPgmdescriptortextblock_Internalname ;
      private String lblPgmdescriptortextblock_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divTable2_Internalname ;
      private String divTable7_Internalname ;
      private String divK2btoolsfilterscontainer_Internalname ;
      private String divFilterattributestable_Internalname ;
      private String edtavPersonapnombre_Internalname ;
      private String TempTags ;
      private String edtavPersonapnombre_Jsonclick ;
      private String divK2btoolstable_attributecontainerpersonafechanacimiento_Internalname ;
      private String lblTextblockpersonafechanacimiento_Internalname ;
      private String lblTextblockpersonafechanacimiento_Jsonclick ;
      private String divGlobalgridtable_Internalname ;
      private String divTable5_Internalname ;
      private String divGridactionsleftcell_Internalname ;
      private String divMaingridcontainerresponsivetablegrid1_Internalname ;
      private String divTable4_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavSelected_Internalname ;
      private String edtPersonaID_Internalname ;
      private String edtPersonaPNombre_Internalname ;
      private String edtPersonaSNombre_Internalname ;
      private String edtPersonaPApellido_Internalname ;
      private String edtPersonaSApellido_Internalname ;
      private String A6PersonaSexo ;
      private String edtPersonaSexo_Internalname ;
      private String edtPersonaDPI_Internalname ;
      private String cmbPersonaTipo_Internalname ;
      private String edtPersonaFechaNacimiento_Internalname ;
      private String cmbPersonaPacienteOrigenSordera_Internalname ;
      private String chkavAtt_personaid_visible_Internalname ;
      private String chkavAtt_personapnombre_visible_Internalname ;
      private String chkavAtt_personasnombre_visible_Internalname ;
      private String chkavAtt_personapapellido_visible_Internalname ;
      private String chkavAtt_personasapellido_visible_Internalname ;
      private String chkavAtt_personasexo_visible_Internalname ;
      private String chkavAtt_personadpi_visible_Internalname ;
      private String chkavAtt_personatipo_visible_Internalname ;
      private String chkavAtt_personafechanacimiento_visible_Internalname ;
      private String chkavAtt_personapacienteorigensordera_visible_Internalname ;
      private String chkavCheckall_Internalname ;
      private String GXCCtl ;
      private String cmbavGridsettingsorderedby_Internalname ;
      private String cmbavGridsettingsrowsperpagevariable_Internalname ;
      private String edtavConfirmmessage_Internalname ;
      private String scmdbuf ;
      private String edtavPersonafechanacimiento_from_Internalname ;
      private String edtavPersonafechanacimiento_to_Internalname ;
      private String AV53ConfirmMessage ;
      private String subGrid_Internalname ;
      private String K2borderbyusercontrol_Internalname ;
      private String imgReport_Tooltiptext ;
      private String imgReport_Internalname ;
      private String imgExport_Tooltiptext ;
      private String imgExport_Internalname ;
      private String bttInsert_Tooltiptext ;
      private String bttInsert_Internalname ;
      private String imgUpdate_Tooltiptext ;
      private String imgUpdate_Internalname ;
      private String imgDelete_Tooltiptext ;
      private String imgDelete_Internalname ;
      private String tblTableconditionalconfirm_Internalname ;
      private String divK2bgridsettingscontentoutertable_Internalname ;
      private String tblNoresultsfoundtable_Internalname ;
      private String lblFirstpagetextblock_Caption ;
      private String lblFirstpagetextblock_Internalname ;
      private String lblPreviouspagetextblock_Caption ;
      private String lblPreviouspagetextblock_Internalname ;
      private String lblCurrentpagetextblock_Caption ;
      private String lblCurrentpagetextblock_Internalname ;
      private String lblNextpagetextblock_Caption ;
      private String lblNextpagetextblock_Internalname ;
      private String lblLastpagetextblock_Caption ;
      private String lblLastpagetextblock_Internalname ;
      private String lblPreviouspagebuttontextblock_Class ;
      private String lblPreviouspagebuttontextblock_Internalname ;
      private String lblNextpagebuttontextblock_Class ;
      private String lblNextpagebuttontextblock_Internalname ;
      private String cellFirstpagecell_Class ;
      private String cellFirstpagecell_Internalname ;
      private String cellSpacingleftcell_Class ;
      private String cellSpacingleftcell_Internalname ;
      private String lblSpacinglefttextblock_Internalname ;
      private String cellPreviouspagecell_Class ;
      private String cellPreviouspagecell_Internalname ;
      private String cellLastpagecell_Class ;
      private String cellLastpagecell_Internalname ;
      private String cellSpacingrightcell_Class ;
      private String cellSpacingrightcell_Internalname ;
      private String lblSpacingrighttextblock_Internalname ;
      private String cellNextpagecell_Class ;
      private String cellNextpagecell_Internalname ;
      private String tblK2btoolspagingcontainertable_Internalname ;
      private String sGXsfl_167_fel_idx="0001" ;
      private String AV57ErrorMessage ;
      private String sStyleString ;
      private String tblSection_condconf_dialog_Internalname ;
      private String tblSection_condconf_dialog_inner_Internalname ;
      private String edtavConfirmmessage_Jsonclick ;
      private String tblConfirm_hidden_actionstable_Internalname ;
      private String bttButtonconfirmyes_Internalname ;
      private String bttButtonconfirmyes_Jsonclick ;
      private String bttButtonconfirmno_Internalname ;
      private String bttButtonconfirmno_Jsonclick ;
      private String tblK2btoolsabstracthiddenitemsgrid_Internalname ;
      private String cellPreviouspagebuttoncell_Internalname ;
      private String lblPreviouspagebuttontextblock_Jsonclick ;
      private String lblFirstpagetextblock_Jsonclick ;
      private String lblSpacinglefttextblock_Jsonclick ;
      private String lblPreviouspagetextblock_Jsonclick ;
      private String cellCurrentpagecell_Internalname ;
      private String lblCurrentpagetextblock_Jsonclick ;
      private String lblNextpagetextblock_Jsonclick ;
      private String lblSpacingrighttextblock_Jsonclick ;
      private String lblLastpagetextblock_Jsonclick ;
      private String cellNextpagebuttoncell_Internalname ;
      private String lblNextpagebuttontextblock_Jsonclick ;
      private String lblNoresultsfoundtextblock_Internalname ;
      private String lblNoresultsfoundtextblock_Jsonclick ;
      private String tblTable_gridcontainergrid1_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String tblTable6_Internalname ;
      private String divK2bgridsettingstable_Internalname ;
      private String lblK2bgridsettingslabel_Internalname ;
      private String lblK2bgridsettingslabel_Jsonclick ;
      private String bttK2bgridsettingssave_Internalname ;
      private String bttK2bgridsettingssave_Jsonclick ;
      private String tblK2btablegridactionsrightcontainer_Internalname ;
      private String sImgUrl ;
      private String imgReport_Jsonclick ;
      private String imgExport_Jsonclick ;
      private String tblGridsettingstable_content_Internalname ;
      private String lblPersonaidruntimecolumnatt_Internalname ;
      private String lblPersonaidruntimecolumnatt_Jsonclick ;
      private String lblPersonapnombreruntimecolumnatt_Internalname ;
      private String lblPersonapnombreruntimecolumnatt_Jsonclick ;
      private String lblPersonasnombreruntimecolumnatt_Internalname ;
      private String lblPersonasnombreruntimecolumnatt_Jsonclick ;
      private String lblPersonapapellidoruntimecolumnatt_Internalname ;
      private String lblPersonapapellidoruntimecolumnatt_Jsonclick ;
      private String lblPersonasapellidoruntimecolumnatt_Internalname ;
      private String lblPersonasapellidoruntimecolumnatt_Jsonclick ;
      private String lblPersonasexoruntimecolumnatt_Internalname ;
      private String lblPersonasexoruntimecolumnatt_Jsonclick ;
      private String lblPersonadpiruntimecolumnatt_Internalname ;
      private String lblPersonadpiruntimecolumnatt_Jsonclick ;
      private String lblPersonatiporuntimecolumnatt_Internalname ;
      private String lblPersonatiporuntimecolumnatt_Jsonclick ;
      private String lblPersonafechanacimientoruntimecolumnatt_Internalname ;
      private String lblPersonafechanacimientoruntimecolumnatt_Jsonclick ;
      private String lblPersonapacienteorigensorderaruntimecolumnatt_Internalname ;
      private String lblPersonapacienteorigensorderaruntimecolumnatt_Jsonclick ;
      private String lblOrderbytext_Internalname ;
      private String lblOrderbytext_Jsonclick ;
      private String cmbavGridsettingsorderedby_Jsonclick ;
      private String lblRowsperpagegrid_Internalname ;
      private String lblRowsperpagegrid_Jsonclick ;
      private String cmbavGridsettingsrowsperpagevariable_Jsonclick ;
      private String tblK2btablegridactionsleftcontainer_Internalname ;
      private String bttInsert_Jsonclick ;
      private String imgUpdate_Jsonclick ;
      private String imgDelete_Jsonclick ;
      private String tblDaterangefiltermaintable_personafechanacimiento_Internalname ;
      private String edtavPersonafechanacimiento_from_Jsonclick ;
      private String lblDaterangeseparator_personafechanacimiento_Internalname ;
      private String lblDaterangeseparator_personafechanacimiento_Jsonclick ;
      private String edtavPersonafechanacimiento_to_Jsonclick ;
      private String ROClassString ;
      private String edtPersonaID_Jsonclick ;
      private String edtPersonaPNombre_Jsonclick ;
      private String edtPersonaSNombre_Jsonclick ;
      private String edtPersonaPApellido_Jsonclick ;
      private String edtPersonaSApellido_Jsonclick ;
      private String edtPersonaSexo_Jsonclick ;
      private String edtPersonaDPI_Jsonclick ;
      private String cmbPersonaTipo_Jsonclick ;
      private String edtPersonaFechaNacimiento_Jsonclick ;
      private String cmbPersonaPacienteOrigenSordera_Jsonclick ;
      private String K2bcontrolbeautify1_Internalname ;
      private DateTime AV10PersonaFechaNacimiento_From ;
      private DateTime AV11PersonaFechaNacimiento_To ;
      private DateTime A9PersonaFechaNacimiento ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool AV33Selected ;
      private bool n3PersonaSNombre ;
      private bool n5PersonaSApellido ;
      private bool n6PersonaSexo ;
      private bool n7PersonaDPI ;
      private bool n9PersonaFechaNacimiento ;
      private bool n10PersonaPacienteOrigenSordera ;
      private bool bGXsfl_167_Refreshing=false ;
      private bool AV20Att_PersonaID_Visible ;
      private bool AV21Att_PersonaPNombre_Visible ;
      private bool AV22Att_PersonaSNombre_Visible ;
      private bool AV23Att_PersonaPApellido_Visible ;
      private bool AV24Att_PersonaSApellido_Visible ;
      private bool AV25Att_PersonaSexo_Visible ;
      private bool AV26Att_PersonaDPI_Visible ;
      private bool AV27Att_PersonaTipo_Visible ;
      private bool AV28Att_PersonaFechaNacimiento_Visible ;
      private bool AV29Att_PersonaPacienteOrigenSordera_Visible ;
      private bool AV51CheckAll ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV16FoundColumn ;
      private String AV8PersonaPNombre ;
      private String AV18AttributeName ;
      private String AV19ColumnTitle ;
      private String A2PersonaPNombre ;
      private String A3PersonaSNombre ;
      private String A4PersonaPApellido ;
      private String A5PersonaSApellido ;
      private String A7PersonaDPI ;
      private String lV8PersonaPNombre ;
      private String AV30GridStateKey ;
      private Guid A1PersonaID ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavAtt_personaid_visible ;
      private GXCheckbox chkavAtt_personapnombre_visible ;
      private GXCheckbox chkavAtt_personasnombre_visible ;
      private GXCheckbox chkavAtt_personapapellido_visible ;
      private GXCheckbox chkavAtt_personasapellido_visible ;
      private GXCheckbox chkavAtt_personasexo_visible ;
      private GXCheckbox chkavAtt_personadpi_visible ;
      private GXCheckbox chkavAtt_personatipo_visible ;
      private GXCheckbox chkavAtt_personafechanacimiento_visible ;
      private GXCheckbox chkavAtt_personapacienteorigensordera_visible ;
      private GXCombobox cmbavGridsettingsorderedby ;
      private GXCombobox cmbavGridsettingsrowsperpagevariable ;
      private GXCheckbox chkavCheckall ;
      private GXCheckbox chkavSelected ;
      private GXCombobox cmbPersonaTipo ;
      private GXCombobox cmbPersonaPacienteOrigenSordera ;
      private IDataStoreProvider pr_default ;
      private short[] H001E2_A10PersonaPacienteOrigenSordera ;
      private bool[] H001E2_n10PersonaPacienteOrigenSordera ;
      private DateTime[] H001E2_A9PersonaFechaNacimiento ;
      private bool[] H001E2_n9PersonaFechaNacimiento ;
      private short[] H001E2_A8PersonaTipo ;
      private String[] H001E2_A7PersonaDPI ;
      private bool[] H001E2_n7PersonaDPI ;
      private String[] H001E2_A6PersonaSexo ;
      private bool[] H001E2_n6PersonaSexo ;
      private String[] H001E2_A5PersonaSApellido ;
      private bool[] H001E2_n5PersonaSApellido ;
      private String[] H001E2_A4PersonaPApellido ;
      private String[] H001E2_A3PersonaSNombre ;
      private bool[] H001E2_n3PersonaSNombre ;
      private String[] H001E2_A2PersonaPNombre ;
      private Guid[] H001E2_A1PersonaID ;
      private long[] H001E3_AGRID_nRecordCount ;
      private IDataStoreProvider pr_gam ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV7HTTPRequest ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> AV14GridColumns ;
      private GXBaseCollection<SdtWWPersonaSDT_WWPersonaSDTItem> AV37AllSelectedRows ;
      private GXBaseCollection<SdtWWPersonaSDT_WWPersonaSDTItem> AV34SelectedRows ;
      private GXBaseCollection<SdtMessages_Message> AV39Messages ;
      private GXBaseCollection<SdtMessages_Message> GXt_objcol_SdtMessages_Message1 ;
      private GXBaseCollection<SdtMessages_Message> AV73GXV7 ;
      private GXBaseCollection<SdtK2BActivityList_K2BActivityListItem> AV46ActivityList ;
      private GXBaseCollection<SdtK2BGridOrders_K2BGridOrdersItem> AV48GridOrders ;
      private GXWebForm Form ;
      private SdtK2BContext AV5Context ;
      private SdtK2BGridColumns_K2BGridColumnsItem AV15GridColumn ;
      private SdtK2BGridState AV31GridState ;
      private SdtK2BGridState_FilterValue AV32GridStateFilterValue ;
      private SdtWWPersonaSDT_WWPersonaSDTItem AV35SelectedRow ;
      private SdtMessages_Message AV40Message ;
      private SdtK2BTrnContext AV42TrnContext ;
      private SdtK2BActivityList_K2BActivityListItem AV47ActivityListItem ;
      private SdtPersona AV56MultipleDelete_BC ;
      private SdtK2BGridOrders_K2BGridOrdersItem AV49GridOrder ;
   }

   public class wwpersona__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class wwpersona__default : DataStoreHelperBase, IDataStoreHelper
 {
    protected Object[] conditional_H001E2( IGxContext context ,
                                           String AV8PersonaPNombre ,
                                           DateTime AV11PersonaFechaNacimiento_To ,
                                           DateTime AV10PersonaFechaNacimiento_From ,
                                           String A2PersonaPNombre ,
                                           DateTime A9PersonaFechaNacimiento ,
                                           short AV12OrderedBy )
    {
       String sWhereString = "" ;
       String scmdbuf ;
       short[] GXv_int2 ;
       GXv_int2 = new short [6] ;
       Object[] GXv_Object3 ;
       GXv_Object3 = new Object [2] ;
       String sSelectString ;
       String sFromString ;
       String sOrderString ;
       sSelectString = " [PersonaPacienteOrigenSordera], [PersonaFechaNacimiento], [PersonaTipo], [PersonaDPI], [PersonaSexo], [PersonaSApellido], [PersonaPApellido], [PersonaSNombre], [PersonaPNombre], [PersonaID]";
       sFromString = " FROM [Persona] WITH (NOLOCK)";
       sOrderString = "";
       if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8PersonaPNombre)) )
       {
          if ( StringUtil.StrCmp("", sWhereString) != 0 )
          {
             sWhereString = sWhereString + " and ([PersonaPNombre] like @lV8PersonaPNombre)";
          }
          else
          {
             sWhereString = sWhereString + " ([PersonaPNombre] like @lV8PersonaPNombre)";
          }
       }
       else
       {
          GXv_int2[0] = 1;
       }
       if ( ! (DateTime.MinValue==AV11PersonaFechaNacimiento_To) )
       {
          if ( StringUtil.StrCmp("", sWhereString) != 0 )
          {
             sWhereString = sWhereString + " and ([PersonaFechaNacimiento] <= @AV11PersonaFechaNacimiento_To)";
          }
          else
          {
             sWhereString = sWhereString + " ([PersonaFechaNacimiento] <= @AV11PersonaFechaNacimiento_To)";
          }
       }
       else
       {
          GXv_int2[1] = 1;
       }
       if ( ! (DateTime.MinValue==AV10PersonaFechaNacimiento_From) )
       {
          if ( StringUtil.StrCmp("", sWhereString) != 0 )
          {
             sWhereString = sWhereString + " and ([PersonaFechaNacimiento] >= @AV10PersonaFechaNacimiento_From)";
          }
          else
          {
             sWhereString = sWhereString + " ([PersonaFechaNacimiento] >= @AV10PersonaFechaNacimiento_From)";
          }
       }
       else
       {
          GXv_int2[2] = 1;
       }
       if ( StringUtil.StrCmp("", sWhereString) != 0 )
       {
          sWhereString = " WHERE" + sWhereString;
       }
       if ( AV12OrderedBy == 0 )
       {
          sOrderString = sOrderString + " ORDER BY [PersonaID]";
       }
       else if ( AV12OrderedBy == 1 )
       {
          sOrderString = sOrderString + " ORDER BY [PersonaID] DESC";
       }
       else if ( AV12OrderedBy == 2 )
       {
          sOrderString = sOrderString + " ORDER BY [PersonaPNombre]";
       }
       else if ( AV12OrderedBy == 3 )
       {
          sOrderString = sOrderString + " ORDER BY [PersonaPNombre] DESC";
       }
       else if ( true )
       {
          sOrderString = sOrderString + " ORDER BY [PersonaID]";
       }
       scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + "" + sOrderString + " OFFSET " + "@GXPagingFrom2" + " ROWS FETCH NEXT CAST((SELECT CASE WHEN " + "@GXPagingTo2" + " > 0 THEN " + "@GXPagingTo2" + " ELSE 1e9 END) AS INTEGER) ROWS ONLY";
       GXv_Object3[0] = scmdbuf;
       GXv_Object3[1] = GXv_int2;
       return GXv_Object3 ;
    }

    protected Object[] conditional_H001E3( IGxContext context ,
                                           String AV8PersonaPNombre ,
                                           DateTime AV11PersonaFechaNacimiento_To ,
                                           DateTime AV10PersonaFechaNacimiento_From ,
                                           String A2PersonaPNombre ,
                                           DateTime A9PersonaFechaNacimiento ,
                                           short AV12OrderedBy )
    {
       String sWhereString = "" ;
       String scmdbuf ;
       short[] GXv_int4 ;
       GXv_int4 = new short [3] ;
       Object[] GXv_Object5 ;
       GXv_Object5 = new Object [2] ;
       scmdbuf = "SELECT COUNT(*) FROM [Persona] WITH (NOLOCK)";
       if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV8PersonaPNombre)) )
       {
          if ( StringUtil.StrCmp("", sWhereString) != 0 )
          {
             sWhereString = sWhereString + " and ([PersonaPNombre] like @lV8PersonaPNombre)";
          }
          else
          {
             sWhereString = sWhereString + " ([PersonaPNombre] like @lV8PersonaPNombre)";
          }
       }
       else
       {
          GXv_int4[0] = 1;
       }
       if ( ! (DateTime.MinValue==AV11PersonaFechaNacimiento_To) )
       {
          if ( StringUtil.StrCmp("", sWhereString) != 0 )
          {
             sWhereString = sWhereString + " and ([PersonaFechaNacimiento] <= @AV11PersonaFechaNacimiento_To)";
          }
          else
          {
             sWhereString = sWhereString + " ([PersonaFechaNacimiento] <= @AV11PersonaFechaNacimiento_To)";
          }
       }
       else
       {
          GXv_int4[1] = 1;
       }
       if ( ! (DateTime.MinValue==AV10PersonaFechaNacimiento_From) )
       {
          if ( StringUtil.StrCmp("", sWhereString) != 0 )
          {
             sWhereString = sWhereString + " and ([PersonaFechaNacimiento] >= @AV10PersonaFechaNacimiento_From)";
          }
          else
          {
             sWhereString = sWhereString + " ([PersonaFechaNacimiento] >= @AV10PersonaFechaNacimiento_From)";
          }
       }
       else
       {
          GXv_int4[2] = 1;
       }
       if ( StringUtil.StrCmp("", sWhereString) != 0 )
       {
          scmdbuf = scmdbuf + " WHERE" + sWhereString;
       }
       if ( AV12OrderedBy == 0 )
       {
          scmdbuf = scmdbuf + "";
       }
       else if ( AV12OrderedBy == 1 )
       {
          scmdbuf = scmdbuf + "";
       }
       else if ( AV12OrderedBy == 2 )
       {
          scmdbuf = scmdbuf + "";
       }
       else if ( AV12OrderedBy == 3 )
       {
          scmdbuf = scmdbuf + "";
       }
       else if ( true )
       {
          scmdbuf = scmdbuf + "";
       }
       GXv_Object5[0] = scmdbuf;
       GXv_Object5[1] = GXv_int4;
       return GXv_Object5 ;
    }

    public override Object [] getDynamicStatement( int cursor ,
                                                   IGxContext context ,
                                                   Object [] dynConstraints )
    {
       switch ( cursor )
       {
             case 0 :
                   return conditional_H001E2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (short)dynConstraints[5] );
             case 1 :
                   return conditional_H001E3(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (short)dynConstraints[5] );
       }
       return base.getDynamicStatement(cursor, context, dynConstraints);
    }

    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmH001E2 ;
        prmH001E2 = new Object[] {
        new Object[] {"@lV8PersonaPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@AV11PersonaFechaNacimiento_To",SqlDbType.DateTime,8,0} ,
        new Object[] {"@AV10PersonaFechaNacimiento_From",SqlDbType.DateTime,8,0} ,
        new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
        new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
        new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
        } ;
        Object[] prmH001E3 ;
        prmH001E3 = new Object[] {
        new Object[] {"@lV8PersonaPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@AV11PersonaFechaNacimiento_To",SqlDbType.DateTime,8,0} ,
        new Object[] {"@AV10PersonaFechaNacimiento_From",SqlDbType.DateTime,8,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("H001E2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001E2,11,0,true,false )
           ,new CursorDef("H001E3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001E3,1,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((short[]) buf[0])[0] = rslt.getShort(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              ((DateTime[]) buf[2])[0] = rslt.getGXDate(2) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(2);
              ((short[]) buf[4])[0] = rslt.getShort(3) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(4);
              ((String[]) buf[7])[0] = rslt.getString(5, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(5);
              ((String[]) buf[9])[0] = rslt.getVarchar(6) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(6);
              ((String[]) buf[11])[0] = rslt.getVarchar(7) ;
              ((String[]) buf[12])[0] = rslt.getVarchar(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((Guid[]) buf[15])[0] = rslt.getGuid(10) ;
              return;
           case 1 :
              ((long[]) buf[0])[0] = rslt.getLong(1) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     short sIdx ;
     switch ( cursor )
     {
           case 0 :
              sIdx = 0;
              if ( (short)parms[0] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (String)parms[6]);
              }
              if ( (short)parms[1] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (DateTime)parms[7]);
              }
              if ( (short)parms[2] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (DateTime)parms[8]);
              }
              if ( (short)parms[3] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (int)parms[9]);
              }
              if ( (short)parms[4] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (int)parms[10]);
              }
              if ( (short)parms[5] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (int)parms[11]);
              }
              return;
           case 1 :
              sIdx = 0;
              if ( (short)parms[0] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (String)parms[3]);
              }
              if ( (short)parms[1] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (DateTime)parms[4]);
              }
              if ( (short)parms[2] == 0 )
              {
                 sIdx = (short)(sIdx+1);
                 stmt.SetParameter(sIdx, (DateTime)parms[5]);
              }
              return;
     }
  }

}

}
