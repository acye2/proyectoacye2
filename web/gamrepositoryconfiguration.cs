/*
               File: GAMRepositoryConfiguration
        Description: Repository configuration
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:3:19.29
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamrepositoryconfiguration : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamrepositoryconfiguration( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gamrepositoryconfiguration( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( long aP0_pId )
      {
         this.AV31pId = aP0_pId;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavDefaultauthtypename = new GXCombobox();
         chkavSessionexpiresonipchange = new GXCheckbox();
         chkavAllowoauthaccess = new GXCheckbox();
         cmbavDefaultsecuritypolicyid = new GXCombobox();
         cmbavLogoutbehavior = new GXCombobox();
         cmbavDefaultroleid = new GXCombobox();
         cmbavEnabletracing = new GXCombobox();
         cmbavUseridentification = new GXCombobox();
         cmbavUseractivationmethod = new GXCombobox();
         chkavUseremailisunique = new GXCheckbox();
         chkavRequiredemail = new GXCheckbox();
         chkavRequiredpassword = new GXCheckbox();
         chkavRequiredfirstname = new GXCheckbox();
         chkavRequiredlastname = new GXCheckbox();
         cmbavGeneratesessionstatistics = new GXCombobox();
         chkavGiveanonymoussession = new GXCheckbox();
         cmbavUserremembermetype = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV31pId = (long)(NumberUtil.Val( gxfirstwebparm, "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31pId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31pId), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV31pId), "ZZZZZZZZZZZ9"), context));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "gamrepositoryconfiguration_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpage", "GeneXus.Programs.gammasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA222( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START222( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171531959", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManager.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/json2005.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/rsh.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManagerCreate.js", "", false);
         context.AddJavascriptSource("Tab/TabRender.js", "", false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gamrepositoryconfiguration.aspx") + "?" + UrlEncode("" +AV31pId)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = "hsh" + "GAMRepositoryConfiguration";
         forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9");
         GxWebStd.gx_hidden_field( context, "hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("gamrepositoryconfiguration:[SendSecurityCheck value for]"+"Id:"+context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "vSECURITYADMINISTRATOREMAIL", AV40SecurityAdministratorEmail);
         GxWebStd.gx_hidden_field( context, "gxhash_vSECURITYADMINISTRATOREMAIL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV40SecurityAdministratorEmail, "")), context));
         GxWebStd.gx_boolean_hidden_field( context, "vCANREGISTERUSERS", AV8CanRegisterUsers);
         GxWebStd.gx_hidden_field( context, "gxhash_vCANREGISTERUSERS", GetSecureSignedToken( "", AV8CanRegisterUsers, context));
         GxWebStd.gx_hidden_field( context, "vPID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV31pId), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV31pId), "ZZZZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "TAB1_Class", StringUtil.RTrim( Tab1_Class));
         GxWebStd.gx_hidden_field( context, "TAB1_Pagecount", StringUtil.LTrim( StringUtil.NToC( (decimal)(Tab1_Pagecount), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TAB1_Historymanagement", StringUtil.BoolToStr( Tab1_Historymanagement));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE222( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT222( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamrepositoryconfiguration.aspx") + "?" + UrlEncode("" +AV31pId) ;
      }

      public override String GetPgmname( )
      {
         return "GAMRepositoryConfiguration" ;
      }

      public override String GetPgmdesc( )
      {
         return "Repository configuration " ;
      }

      protected void WB220( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "BodyContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable1_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-5 col-sm-6 col-md-offset-1 col-lg-3 col-lg-offset-2", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Repository Configuration", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-4 col-sm-6 col-md-3", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"TAB1Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB1Container"+"title1"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGeneral_title_Internalname, "General", "", "", lblGeneral_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "General") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB1Container"+"panel1"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabpage1table_Internalname, 1, 0, "px", 0, "px", "TabsFormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavId_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavId_Internalname, "Id", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 24,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22Id), 12, 0, ",", "")), ((edtavId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,24);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavId_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavGuid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavGuid_Internalname, "GUID", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 29,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV21GUID), StringUtil.RTrim( context.localUtil.Format( AV21GUID, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,29);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavGuid_Enabled, 0, "text", "", 32, "chr", 1, "row", 32, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavNamespace_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavNamespace_Internalname, "Namespace", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 34,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNamespace_Internalname, StringUtil.RTrim( AV30NameSpace), StringUtil.RTrim( context.localUtil.Format( AV30NameSpace, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,34);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavNamespace_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavNamespace_Enabled, 0, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, 0, true, "GAMRepositoryNameSpace", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavName_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavName_Internalname, "Name", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 39,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV29Name), StringUtil.RTrim( context.localUtil.Format( AV29Name, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,39);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavName_Enabled, 0, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavDsc_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavDsc_Internalname, "Description", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 44,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV12Dsc), StringUtil.RTrim( context.localUtil.Format( AV12Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavDsc_Enabled, 0, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divDefaultauthtypenamecell_Internalname, divDefaultauthtypenamecell_Visible, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavDefaultauthtypename_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavDefaultauthtypename_Internalname, "Default authentication type", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 49,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDefaultauthtypename, cmbavDefaultauthtypename_Internalname, StringUtil.RTrim( AV9DefaultAuthTypeName), 1, cmbavDefaultauthtypename_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavDefaultauthtypename.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavDefaultauthtypename.CurrentValue = StringUtil.RTrim( AV9DefaultAuthTypeName);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultauthtypename_Internalname, "Values", (String)(cmbavDefaultauthtypename.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavSessionexpiresonipchange_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavSessionexpiresonipchange_Internalname, "GAM Session expires on IP change?", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 54,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavSessionexpiresonipchange_Internalname, StringUtil.BoolToStr( AV43SessionExpiresOnIPChange), "", "GAM Session expires on IP change?", 1, chkavSessionexpiresonipchange.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(54, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,54);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavAllowoauthaccess_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavAllowoauthaccess_Internalname, "Allow oauth access (Smart Devices)", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAllowoauthaccess_Internalname, StringUtil.BoolToStr( AV5AllowOauthAccess), "", "Allow oauth access (Smart Devices)", 1, chkavAllowoauthaccess.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(59, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divDefaultsecuritypolicyidcell_Internalname, divDefaultsecuritypolicyidcell_Visible, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavDefaultsecuritypolicyid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavDefaultsecuritypolicyid_Internalname, "Repository default security policy", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDefaultsecuritypolicyid, cmbavDefaultsecuritypolicyid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)), 1, cmbavDefaultsecuritypolicyid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavDefaultsecuritypolicyid.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavDefaultsecuritypolicyid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultsecuritypolicyid_Internalname, "Values", (String)(cmbavDefaultsecuritypolicyid.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSsobehaviorcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavLogoutbehavior_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavLogoutbehavior_Internalname, "GAM Remote logout behavior", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavLogoutbehavior, cmbavLogoutbehavior_Internalname, StringUtil.RTrim( AV27LogoutBehavior), 1, cmbavLogoutbehavior_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavLogoutbehavior.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavLogoutbehavior.CurrentValue = StringUtil.RTrim( AV27LogoutBehavior);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogoutbehavior_Internalname, "Values", (String)(cmbavLogoutbehavior.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divDefaultroleidcell_Internalname, divDefaultroleidcell_Visible, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavDefaultroleid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavDefaultroleid_Internalname, "Repository default role", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 74,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavDefaultroleid, cmbavDefaultroleid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)), 1, cmbavDefaultroleid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavDefaultroleid.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,74);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavDefaultroleid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultroleid_Internalname, "Values", (String)(cmbavDefaultroleid.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellSimple", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavEnabletracing_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavEnabletracing_Internalname, "Enable tracing", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 79,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavEnabletracing, cmbavEnabletracing_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0)), 1, cmbavEnabletracing_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavEnabletracing.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,79);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavEnabletracing.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEnabletracing_Internalname, "Values", (String)(cmbavEnabletracing.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB1Container"+"title2"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblUsers_title_Internalname, "Users", "", "", lblUsers_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "Users") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB1Container"+"panel2"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabpage2table_Internalname, 1, 0, "px", 0, "px", "TabsFormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavUseridentification_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavUseridentification_Internalname, "User identification by", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 89,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUseridentification, cmbavUseridentification_Internalname, StringUtil.RTrim( AV47UserIdentification), 1, cmbavUseridentification_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavUseridentification.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,89);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavUseridentification.CurrentValue = StringUtil.RTrim( AV47UserIdentification);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUseridentification_Internalname, "Values", (String)(cmbavUseridentification.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavUseractivationmethod_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavUseractivationmethod_Internalname, "User activation method", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 94,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUseractivationmethod, cmbavUseractivationmethod_Internalname, StringUtil.RTrim( AV44UserActivationMethod), 1, cmbavUseractivationmethod_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavUseractivationmethod.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,94);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavUseractivationmethod.CurrentValue = StringUtil.RTrim( AV44UserActivationMethod);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUseractivationmethod_Internalname, "Values", (String)(cmbavUseractivationmethod.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavUserautomaticactivationtimeout_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavUserautomaticactivationtimeout_Internalname, "User automatic activation timeout  (hours)", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 99,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserautomaticactivationtimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV45UserAutomaticActivationTimeout), 4, 0, ",", "")), ((edtavUserautomaticactivationtimeout_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV45UserAutomaticActivationTimeout), "ZZZ9")) : context.localUtil.Format( (decimal)(AV45UserAutomaticActivationTimeout), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,99);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserautomaticactivationtimeout_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavUserautomaticactivationtimeout_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavUseremailisunique_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavUseremailisunique_Internalname, "User email is unique?", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 104,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUseremailisunique_Internalname, StringUtil.BoolToStr( AV46UserEmailisUnique), "", "User email is unique?", 1, chkavUseremailisunique.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(104, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,104);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavRequiredemail_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavRequiredemail_Internalname, "Required email?", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 109,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredemail_Internalname, StringUtil.BoolToStr( AV34RequiredEmail), "", "Required email?", 1, chkavRequiredemail.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(109, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,109);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavRequiredpassword_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavRequiredpassword_Internalname, "Required password?", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 114,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredpassword_Internalname, StringUtil.BoolToStr( AV37RequiredPassword), "", "Required password?", 1, chkavRequiredpassword.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(114, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,114);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavRequiredfirstname_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavRequiredfirstname_Internalname, "Required first name?", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 119,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredfirstname_Internalname, StringUtil.BoolToStr( AV35RequiredFirstName), "", "Required first name?", 1, chkavRequiredfirstname.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(119, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,119);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellSimple", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavRequiredlastname_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavRequiredlastname_Internalname, "Required last name?", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 124,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavRequiredlastname_Internalname, StringUtil.BoolToStr( AV36RequiredLastName), "", "Required last name?", 1, chkavRequiredlastname.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(124, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,124);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB1Container"+"title3"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSession_title_Internalname, "Session", "", "", lblSession_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GAMRepositoryConfiguration.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "Session") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB1Container"+"panel3"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabpage3table_Internalname, 1, 0, "px", 0, "px", "TabsFormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavGeneratesessionstatistics_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavGeneratesessionstatistics_Internalname, "Generate session statistics?", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 134,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGeneratesessionstatistics, cmbavGeneratesessionstatistics_Internalname, StringUtil.RTrim( AV19GenerateSessionStatistics), 1, cmbavGeneratesessionstatistics_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavGeneratesessionstatistics.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,134);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavGeneratesessionstatistics.CurrentValue = StringUtil.RTrim( AV19GenerateSessionStatistics);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGeneratesessionstatistics_Internalname, "Values", (String)(cmbavGeneratesessionstatistics.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavUsersessioncachetimeout_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavUsersessioncachetimeout_Internalname, "User session cache timeout (seconds)", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 139,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUsersessioncachetimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV51UserSessionCacheTimeout), 6, 0, ",", "")), ((edtavUsersessioncachetimeout_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV51UserSessionCacheTimeout), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV51UserSessionCacheTimeout), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,139);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUsersessioncachetimeout_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavUsersessioncachetimeout_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavGiveanonymoussession_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavGiveanonymoussession_Internalname, "Give anonymous session?", "col-sm-5 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 144,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavGiveanonymoussession_Internalname, StringUtil.BoolToStr( AV20GiveAnonymousSession), "", "Give anonymous session?", 1, chkavGiveanonymoussession.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(144, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,144);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavLoginattemptstolocksession_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavLoginattemptstolocksession_Internalname, "Login attempts to lock session", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 149,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLoginattemptstolocksession_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25LoginAttemptsToLockSession), 2, 0, ",", "")), ((edtavLoginattemptstolocksession_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25LoginAttemptsToLockSession), "Z9")) : context.localUtil.Format( (decimal)(AV25LoginAttemptsToLockSession), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,149);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLoginattemptstolocksession_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavLoginattemptstolocksession_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavGamunblockusertimeout_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavGamunblockusertimeout_Internalname, "Unblock user timeout (minutes)", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 154,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGamunblockusertimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18GAMUnblockUserTimeout), 4, 0, ",", "")), ((edtavGamunblockusertimeout_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV18GAMUnblockUserTimeout), "ZZZ9")) : context.localUtil.Format( (decimal)(AV18GAMUnblockUserTimeout), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,154);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGamunblockusertimeout_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavGamunblockusertimeout_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavLoginattemptstolockuser_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavLoginattemptstolockuser_Internalname, "Login retries to lock user", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLoginattemptstolockuser_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV26LoginAttemptsToLockUser), 2, 0, ",", "")), ((edtavLoginattemptstolockuser_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV26LoginAttemptsToLockUser), "Z9")) : context.localUtil.Format( (decimal)(AV26LoginAttemptsToLockUser), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,159);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLoginattemptstolockuser_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavLoginattemptstolockuser_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavMinimumamountcharactersinlogin_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavMinimumamountcharactersinlogin_Internalname, "Minimum amount characters in login", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavMinimumamountcharactersinlogin_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV28MinimumAmountCharactersInLogin), 2, 0, ",", "")), ((edtavMinimumamountcharactersinlogin_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV28MinimumAmountCharactersInLogin), "Z9")) : context.localUtil.Format( (decimal)(AV28MinimumAmountCharactersInLogin), "Z9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,164);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavMinimumamountcharactersinlogin_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavMinimumamountcharactersinlogin_Enabled, 0, "text", "", 2, "chr", 1, "row", 2, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavUserrecoverypasswordkeytimeout_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavUserrecoverypasswordkeytimeout_Internalname, "User recovery password key timeout (minutes)", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserrecoverypasswordkeytimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV48UserRecoveryPasswordKeyTimeOut), 4, 0, ",", "")), ((edtavUserrecoverypasswordkeytimeout_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV48UserRecoveryPasswordKeyTimeOut), "ZZZ9")) : context.localUtil.Format( (decimal)(AV48UserRecoveryPasswordKeyTimeOut), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,169);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserrecoverypasswordkeytimeout_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavUserrecoverypasswordkeytimeout_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavUserremembermetimeout_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavUserremembermetimeout_Internalname, "User remember me timeout (days)", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 174,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavUserremembermetimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV49UserRememberMeTimeOut), 4, 0, ",", "")), ((edtavUserremembermetimeout_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV49UserRememberMeTimeOut), "ZZZ9")) : context.localUtil.Format( (decimal)(AV49UserRememberMeTimeOut), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,174);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavUserremembermetimeout_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavUserremembermetimeout_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavUserremembermetype_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavUserremembermetype_Internalname, "User remember me type", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 179,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavUserremembermetype, cmbavUserremembermetype_Internalname, StringUtil.RTrim( AV50UserRememberMeType), 1, cmbavUserremembermetype_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavUserremembermetype.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,179);\"", "", true, "HLP_GAMRepositoryConfiguration.htm");
            cmbavUserremembermetype.CurrentValue = StringUtil.RTrim( AV50UserRememberMeType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUserremembermetype_Internalname, "Values", (String)(cmbavUserremembermetype.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellSimple", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavRepositorycachetimeout_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavRepositorycachetimeout_Internalname, "Repository cache timeout (minutes)", "col-sm-5 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-7 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavRepositorycachetimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV33RepositoryCacheTimeout), 6, 0, ",", "")), ((edtavRepositorycachetimeout_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV33RepositoryCacheTimeout), "ZZZZZ9")) : context.localUtil.Format( (decimal)(AV33RepositoryCacheTimeout), "ZZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,184);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavRepositorycachetimeout_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavRepositorycachetimeout_Enabled, 0, "text", "", 6, "chr", 1, "row", 6, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 189,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirm_Internalname, "", "Confirmar", bttConfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMRepositoryConfiguration.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START222( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Repository configuration ", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP220( ) ;
      }

      protected void WS222( )
      {
         START222( ) ;
         EVT222( ) ;
      }

      protected void EVT222( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Start */
                              E11222 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: Enter */
                                    E12222 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Load */
                              E13222 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE222( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA222( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavDefaultauthtypename.Name = "vDEFAULTAUTHTYPENAME";
            cmbavDefaultauthtypename.WebTags = "";
            if ( cmbavDefaultauthtypename.ItemCount > 0 )
            {
               AV9DefaultAuthTypeName = cmbavDefaultauthtypename.getValidValue(AV9DefaultAuthTypeName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
            }
            chkavSessionexpiresonipchange.Name = "vSESSIONEXPIRESONIPCHANGE";
            chkavSessionexpiresonipchange.WebTags = "";
            chkavSessionexpiresonipchange.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavSessionexpiresonipchange_Internalname, "TitleCaption", chkavSessionexpiresonipchange.Caption, true);
            chkavSessionexpiresonipchange.CheckedValue = "false";
            chkavAllowoauthaccess.Name = "vALLOWOAUTHACCESS";
            chkavAllowoauthaccess.WebTags = "";
            chkavAllowoauthaccess.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAllowoauthaccess_Internalname, "TitleCaption", chkavAllowoauthaccess.Caption, true);
            chkavAllowoauthaccess.CheckedValue = "false";
            cmbavDefaultsecuritypolicyid.Name = "vDEFAULTSECURITYPOLICYID";
            cmbavDefaultsecuritypolicyid.WebTags = "";
            if ( cmbavDefaultsecuritypolicyid.ItemCount > 0 )
            {
               AV11DefaultSecurityPolicyId = (int)(NumberUtil.Val( cmbavDefaultsecuritypolicyid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
            }
            cmbavLogoutbehavior.Name = "vLOGOUTBEHAVIOR";
            cmbavLogoutbehavior.WebTags = "";
            cmbavLogoutbehavior.addItem("clionl", "Client only", 0);
            cmbavLogoutbehavior.addItem("cliip", "Identity Provider and Client", 0);
            cmbavLogoutbehavior.addItem("all", "Identity Provider and all Clients", 0);
            if ( cmbavLogoutbehavior.ItemCount > 0 )
            {
               AV27LogoutBehavior = cmbavLogoutbehavior.getValidValue(AV27LogoutBehavior);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogoutBehavior", AV27LogoutBehavior);
            }
            cmbavDefaultroleid.Name = "vDEFAULTROLEID";
            cmbavDefaultroleid.WebTags = "";
            if ( cmbavDefaultroleid.ItemCount > 0 )
            {
               AV10DefaultRoleId = (long)(NumberUtil.Val( cmbavDefaultroleid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
            }
            cmbavEnabletracing.Name = "vENABLETRACING";
            cmbavEnabletracing.WebTags = "";
            cmbavEnabletracing.addItem("0", "0 - Off", 0);
            cmbavEnabletracing.addItem("1", "1 - Debug", 0);
            if ( cmbavEnabletracing.ItemCount > 0 )
            {
               AV13EnableTracing = (short)(NumberUtil.Val( cmbavEnabletracing.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0)));
            }
            cmbavUseridentification.Name = "vUSERIDENTIFICATION";
            cmbavUseridentification.WebTags = "";
            cmbavUseridentification.addItem("name", "Name", 0);
            cmbavUseridentification.addItem("email", "EMail", 0);
            cmbavUseridentification.addItem("namema", "Name and Email", 0);
            if ( cmbavUseridentification.ItemCount > 0 )
            {
               AV47UserIdentification = cmbavUseridentification.getValidValue(AV47UserIdentification);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47UserIdentification", AV47UserIdentification);
            }
            cmbavUseractivationmethod.Name = "vUSERACTIVATIONMETHOD";
            cmbavUseractivationmethod.WebTags = "";
            cmbavUseractivationmethod.addItem("A", "Automatic", 0);
            cmbavUseractivationmethod.addItem("U", "User", 0);
            cmbavUseractivationmethod.addItem("D", "Administrator", 0);
            if ( cmbavUseractivationmethod.ItemCount > 0 )
            {
               AV44UserActivationMethod = cmbavUseractivationmethod.getValidValue(AV44UserActivationMethod);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44UserActivationMethod", AV44UserActivationMethod);
            }
            chkavUseremailisunique.Name = "vUSEREMAILISUNIQUE";
            chkavUseremailisunique.WebTags = "";
            chkavUseremailisunique.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUseremailisunique_Internalname, "TitleCaption", chkavUseremailisunique.Caption, true);
            chkavUseremailisunique.CheckedValue = "false";
            chkavRequiredemail.Name = "vREQUIREDEMAIL";
            chkavRequiredemail.WebTags = "";
            chkavRequiredemail.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredemail_Internalname, "TitleCaption", chkavRequiredemail.Caption, true);
            chkavRequiredemail.CheckedValue = "false";
            chkavRequiredpassword.Name = "vREQUIREDPASSWORD";
            chkavRequiredpassword.WebTags = "";
            chkavRequiredpassword.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredpassword_Internalname, "TitleCaption", chkavRequiredpassword.Caption, true);
            chkavRequiredpassword.CheckedValue = "false";
            chkavRequiredfirstname.Name = "vREQUIREDFIRSTNAME";
            chkavRequiredfirstname.WebTags = "";
            chkavRequiredfirstname.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredfirstname_Internalname, "TitleCaption", chkavRequiredfirstname.Caption, true);
            chkavRequiredfirstname.CheckedValue = "false";
            chkavRequiredlastname.Name = "vREQUIREDLASTNAME";
            chkavRequiredlastname.WebTags = "";
            chkavRequiredlastname.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavRequiredlastname_Internalname, "TitleCaption", chkavRequiredlastname.Caption, true);
            chkavRequiredlastname.CheckedValue = "false";
            cmbavGeneratesessionstatistics.Name = "vGENERATESESSIONSTATISTICS";
            cmbavGeneratesessionstatistics.WebTags = "";
            cmbavGeneratesessionstatistics.addItem("None", "None", 0);
            cmbavGeneratesessionstatistics.addItem("Minimum", "Minimum (Only authenticated users)", 0);
            cmbavGeneratesessionstatistics.addItem("Detail", "Detail (Authenticated and anonymous users)", 0);
            cmbavGeneratesessionstatistics.addItem("Full", "Full log (Authenticated and anonymous users)", 0);
            if ( cmbavGeneratesessionstatistics.ItemCount > 0 )
            {
               AV19GenerateSessionStatistics = cmbavGeneratesessionstatistics.getValidValue(AV19GenerateSessionStatistics);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GenerateSessionStatistics", AV19GenerateSessionStatistics);
            }
            chkavGiveanonymoussession.Name = "vGIVEANONYMOUSSESSION";
            chkavGiveanonymoussession.WebTags = "";
            chkavGiveanonymoussession.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavGiveanonymoussession_Internalname, "TitleCaption", chkavGiveanonymoussession.Caption, true);
            chkavGiveanonymoussession.CheckedValue = "false";
            cmbavUserremembermetype.Name = "vUSERREMEMBERMETYPE";
            cmbavUserremembermetype.WebTags = "";
            cmbavUserremembermetype.addItem("None", "None", 0);
            cmbavUserremembermetype.addItem("Login", "Login", 0);
            cmbavUserremembermetype.addItem("Auth", "Authentication", 0);
            cmbavUserremembermetype.addItem("Both", "Both", 0);
            if ( cmbavUserremembermetype.ItemCount > 0 )
            {
               AV50UserRememberMeType = cmbavUserremembermetype.getValidValue(AV50UserRememberMeType);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UserRememberMeType", AV50UserRememberMeType);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavDefaultauthtypename.ItemCount > 0 )
         {
            AV9DefaultAuthTypeName = cmbavDefaultauthtypename.getValidValue(AV9DefaultAuthTypeName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavDefaultauthtypename.CurrentValue = StringUtil.RTrim( AV9DefaultAuthTypeName);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultauthtypename_Internalname, "Values", cmbavDefaultauthtypename.ToJavascriptSource(), true);
         }
         if ( cmbavDefaultsecuritypolicyid.ItemCount > 0 )
         {
            AV11DefaultSecurityPolicyId = (int)(NumberUtil.Val( cmbavDefaultsecuritypolicyid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavDefaultsecuritypolicyid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultsecuritypolicyid_Internalname, "Values", cmbavDefaultsecuritypolicyid.ToJavascriptSource(), true);
         }
         if ( cmbavLogoutbehavior.ItemCount > 0 )
         {
            AV27LogoutBehavior = cmbavLogoutbehavior.getValidValue(AV27LogoutBehavior);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogoutBehavior", AV27LogoutBehavior);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavLogoutbehavior.CurrentValue = StringUtil.RTrim( AV27LogoutBehavior);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavLogoutbehavior_Internalname, "Values", cmbavLogoutbehavior.ToJavascriptSource(), true);
         }
         if ( cmbavDefaultroleid.ItemCount > 0 )
         {
            AV10DefaultRoleId = (long)(NumberUtil.Val( cmbavDefaultroleid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavDefaultroleid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavDefaultroleid_Internalname, "Values", cmbavDefaultroleid.ToJavascriptSource(), true);
         }
         if ( cmbavEnabletracing.ItemCount > 0 )
         {
            AV13EnableTracing = (short)(NumberUtil.Val( cmbavEnabletracing.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavEnabletracing.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavEnabletracing_Internalname, "Values", cmbavEnabletracing.ToJavascriptSource(), true);
         }
         if ( cmbavUseridentification.ItemCount > 0 )
         {
            AV47UserIdentification = cmbavUseridentification.getValidValue(AV47UserIdentification);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47UserIdentification", AV47UserIdentification);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavUseridentification.CurrentValue = StringUtil.RTrim( AV47UserIdentification);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUseridentification_Internalname, "Values", cmbavUseridentification.ToJavascriptSource(), true);
         }
         if ( cmbavUseractivationmethod.ItemCount > 0 )
         {
            AV44UserActivationMethod = cmbavUseractivationmethod.getValidValue(AV44UserActivationMethod);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44UserActivationMethod", AV44UserActivationMethod);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavUseractivationmethod.CurrentValue = StringUtil.RTrim( AV44UserActivationMethod);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUseractivationmethod_Internalname, "Values", cmbavUseractivationmethod.ToJavascriptSource(), true);
         }
         if ( cmbavGeneratesessionstatistics.ItemCount > 0 )
         {
            AV19GenerateSessionStatistics = cmbavGeneratesessionstatistics.getValidValue(AV19GenerateSessionStatistics);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GenerateSessionStatistics", AV19GenerateSessionStatistics);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavGeneratesessionstatistics.CurrentValue = StringUtil.RTrim( AV19GenerateSessionStatistics);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGeneratesessionstatistics_Internalname, "Values", cmbavGeneratesessionstatistics.ToJavascriptSource(), true);
         }
         if ( cmbavUserremembermetype.ItemCount > 0 )
         {
            AV50UserRememberMeType = cmbavUserremembermetype.getValidValue(AV50UserRememberMeType);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UserRememberMeType", AV50UserRememberMeType);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavUserremembermetype.CurrentValue = StringUtil.RTrim( AV50UserRememberMeType);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavUserremembermetype_Internalname, "Values", cmbavUserremembermetype.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF222( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)), true);
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)), true);
         edtavNamespace_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNamespace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNamespace_Enabled), 5, 0)), true);
      }

      protected void RF222( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E13222 ();
            WB220( ) ;
         }
      }

      protected void send_integrity_lvl_hashes222( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "vSECURITYADMINISTRATOREMAIL", AV40SecurityAdministratorEmail);
         GxWebStd.gx_hidden_field( context, "gxhash_vSECURITYADMINISTRATOREMAIL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV40SecurityAdministratorEmail, "")), context));
         GxWebStd.gx_boolean_hidden_field( context, "vCANREGISTERUSERS", AV8CanRegisterUsers);
         GxWebStd.gx_hidden_field( context, "gxhash_vCANREGISTERUSERS", GetSecureSignedToken( "", AV8CanRegisterUsers, context));
      }

      protected void STRUP220( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)), true);
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)), true);
         edtavNamespace_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavNamespace_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNamespace_Enabled), 5, 0)), true);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11222 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", ".") > Convert.ToDecimal( 999999999999L )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vID");
               GX_FocusControl = edtavId_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV22Id = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Id), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
            }
            else
            {
               AV22Id = (long)(context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Id), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
            }
            AV21GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GUID", AV21GUID);
            AV30NameSpace = cgiGet( edtavNamespace_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30NameSpace", AV30NameSpace);
            AV29Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Name", AV29Name);
            AV12Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Dsc", AV12Dsc);
            cmbavDefaultauthtypename.CurrentValue = cgiGet( cmbavDefaultauthtypename_Internalname);
            AV9DefaultAuthTypeName = cgiGet( cmbavDefaultauthtypename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
            AV43SessionExpiresOnIPChange = StringUtil.StrToBool( cgiGet( chkavSessionexpiresonipchange_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43SessionExpiresOnIPChange", AV43SessionExpiresOnIPChange);
            AV5AllowOauthAccess = StringUtil.StrToBool( cgiGet( chkavAllowoauthaccess_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowOauthAccess", AV5AllowOauthAccess);
            cmbavDefaultsecuritypolicyid.CurrentValue = cgiGet( cmbavDefaultsecuritypolicyid_Internalname);
            AV11DefaultSecurityPolicyId = (int)(NumberUtil.Val( cgiGet( cmbavDefaultsecuritypolicyid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
            cmbavLogoutbehavior.CurrentValue = cgiGet( cmbavLogoutbehavior_Internalname);
            AV27LogoutBehavior = cgiGet( cmbavLogoutbehavior_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogoutBehavior", AV27LogoutBehavior);
            cmbavDefaultroleid.CurrentValue = cgiGet( cmbavDefaultroleid_Internalname);
            AV10DefaultRoleId = (long)(NumberUtil.Val( cgiGet( cmbavDefaultroleid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
            cmbavEnabletracing.CurrentValue = cgiGet( cmbavEnabletracing_Internalname);
            AV13EnableTracing = (short)(NumberUtil.Val( cgiGet( cmbavEnabletracing_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0)));
            cmbavUseridentification.CurrentValue = cgiGet( cmbavUseridentification_Internalname);
            AV47UserIdentification = cgiGet( cmbavUseridentification_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47UserIdentification", AV47UserIdentification);
            cmbavUseractivationmethod.CurrentValue = cgiGet( cmbavUseractivationmethod_Internalname);
            AV44UserActivationMethod = cgiGet( cmbavUseractivationmethod_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44UserActivationMethod", AV44UserActivationMethod);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUserautomaticactivationtimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUserautomaticactivationtimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERAUTOMATICACTIVATIONTIMEOUT");
               GX_FocusControl = edtavUserautomaticactivationtimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV45UserAutomaticActivationTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45UserAutomaticActivationTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45UserAutomaticActivationTimeout), 4, 0)));
            }
            else
            {
               AV45UserAutomaticActivationTimeout = (short)(context.localUtil.CToN( cgiGet( edtavUserautomaticactivationtimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45UserAutomaticActivationTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45UserAutomaticActivationTimeout), 4, 0)));
            }
            AV46UserEmailisUnique = StringUtil.StrToBool( cgiGet( chkavUseremailisunique_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46UserEmailisUnique", AV46UserEmailisUnique);
            AV34RequiredEmail = StringUtil.StrToBool( cgiGet( chkavRequiredemail_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RequiredEmail", AV34RequiredEmail);
            AV37RequiredPassword = StringUtil.StrToBool( cgiGet( chkavRequiredpassword_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37RequiredPassword", AV37RequiredPassword);
            AV35RequiredFirstName = StringUtil.StrToBool( cgiGet( chkavRequiredfirstname_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35RequiredFirstName", AV35RequiredFirstName);
            AV36RequiredLastName = StringUtil.StrToBool( cgiGet( chkavRequiredlastname_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RequiredLastName", AV36RequiredLastName);
            cmbavGeneratesessionstatistics.CurrentValue = cgiGet( cmbavGeneratesessionstatistics_Internalname);
            AV19GenerateSessionStatistics = cgiGet( cmbavGeneratesessionstatistics_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GenerateSessionStatistics", AV19GenerateSessionStatistics);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUsersessioncachetimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUsersessioncachetimeout_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERSESSIONCACHETIMEOUT");
               GX_FocusControl = edtavUsersessioncachetimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV51UserSessionCacheTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51UserSessionCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51UserSessionCacheTimeout), 6, 0)));
            }
            else
            {
               AV51UserSessionCacheTimeout = (int)(context.localUtil.CToN( cgiGet( edtavUsersessioncachetimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51UserSessionCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51UserSessionCacheTimeout), 6, 0)));
            }
            AV20GiveAnonymousSession = StringUtil.StrToBool( cgiGet( chkavGiveanonymoussession_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GiveAnonymousSession", AV20GiveAnonymousSession);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolocksession_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolocksession_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOGINATTEMPTSTOLOCKSESSION");
               GX_FocusControl = edtavLoginattemptstolocksession_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25LoginAttemptsToLockSession = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25LoginAttemptsToLockSession", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25LoginAttemptsToLockSession), 2, 0)));
            }
            else
            {
               AV25LoginAttemptsToLockSession = (short)(context.localUtil.CToN( cgiGet( edtavLoginattemptstolocksession_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25LoginAttemptsToLockSession", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25LoginAttemptsToLockSession), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavGamunblockusertimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavGamunblockusertimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vGAMUNBLOCKUSERTIMEOUT");
               GX_FocusControl = edtavGamunblockusertimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18GAMUnblockUserTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GAMUnblockUserTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GAMUnblockUserTimeout), 4, 0)));
            }
            else
            {
               AV18GAMUnblockUserTimeout = (short)(context.localUtil.CToN( cgiGet( edtavGamunblockusertimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GAMUnblockUserTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GAMUnblockUserTimeout), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolockuser_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavLoginattemptstolockuser_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vLOGINATTEMPTSTOLOCKUSER");
               GX_FocusControl = edtavLoginattemptstolockuser_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV26LoginAttemptsToLockUser = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26LoginAttemptsToLockUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26LoginAttemptsToLockUser), 2, 0)));
            }
            else
            {
               AV26LoginAttemptsToLockUser = (short)(context.localUtil.CToN( cgiGet( edtavLoginattemptstolockuser_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26LoginAttemptsToLockUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26LoginAttemptsToLockUser), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavMinimumamountcharactersinlogin_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavMinimumamountcharactersinlogin_Internalname), ",", ".") > Convert.ToDecimal( 99 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vMINIMUMAMOUNTCHARACTERSINLOGIN");
               GX_FocusControl = edtavMinimumamountcharactersinlogin_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV28MinimumAmountCharactersInLogin = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28MinimumAmountCharactersInLogin", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28MinimumAmountCharactersInLogin), 2, 0)));
            }
            else
            {
               AV28MinimumAmountCharactersInLogin = (short)(context.localUtil.CToN( cgiGet( edtavMinimumamountcharactersinlogin_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28MinimumAmountCharactersInLogin", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28MinimumAmountCharactersInLogin), 2, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUserrecoverypasswordkeytimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUserrecoverypasswordkeytimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERRECOVERYPASSWORDKEYTIMEOUT");
               GX_FocusControl = edtavUserrecoverypasswordkeytimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV48UserRecoveryPasswordKeyTimeOut = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48UserRecoveryPasswordKeyTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48UserRecoveryPasswordKeyTimeOut), 4, 0)));
            }
            else
            {
               AV48UserRecoveryPasswordKeyTimeOut = (short)(context.localUtil.CToN( cgiGet( edtavUserrecoverypasswordkeytimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48UserRecoveryPasswordKeyTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48UserRecoveryPasswordKeyTimeOut), 4, 0)));
            }
            if ( ( ( context.localUtil.CToN( cgiGet( edtavUserremembermetimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavUserremembermetimeout_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vUSERREMEMBERMETIMEOUT");
               GX_FocusControl = edtavUserremembermetimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV49UserRememberMeTimeOut = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49UserRememberMeTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49UserRememberMeTimeOut), 4, 0)));
            }
            else
            {
               AV49UserRememberMeTimeOut = (short)(context.localUtil.CToN( cgiGet( edtavUserremembermetimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49UserRememberMeTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49UserRememberMeTimeOut), 4, 0)));
            }
            cmbavUserremembermetype.CurrentValue = cgiGet( cmbavUserremembermetype_Internalname);
            AV50UserRememberMeType = cgiGet( cmbavUserremembermetype_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UserRememberMeType", AV50UserRememberMeType);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavRepositorycachetimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavRepositorycachetimeout_Internalname), ",", ".") > Convert.ToDecimal( 999999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vREPOSITORYCACHETIMEOUT");
               GX_FocusControl = edtavRepositorycachetimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV33RepositoryCacheTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33RepositoryCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33RepositoryCacheTimeout), 6, 0)));
            }
            else
            {
               AV33RepositoryCacheTimeout = (int)(context.localUtil.CToN( cgiGet( edtavRepositorycachetimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33RepositoryCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33RepositoryCacheTimeout), 6, 0)));
            }
            /* Read saved values. */
            Tab1_Class = cgiGet( "TAB1_Class");
            Tab1_Pagecount = (int)(context.localUtil.CToN( cgiGet( "TAB1_Pagecount"), ",", "."));
            Tab1_Historymanagement = StringUtil.StrToBool( cgiGet( "TAB1_Historymanagement"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            forbiddenHiddens = "hsh" + "GAMRepositoryConfiguration";
            AV22Id = (long)(context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
            forbiddenHiddens = forbiddenHiddens + context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9");
            hsh = cgiGet( "hsh");
            if ( ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
            {
               GXUtil.WriteLog("gamrepositoryconfiguration:[SecurityCheckFailed value for]"+"Id:"+context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"));
               GxWebError = 1;
               context.HttpContext.Response.StatusDescription = 403.ToString();
               context.HttpContext.Response.StatusCode = 403;
               context.WriteHtmlText( "<title>403 Forbidden</title>") ;
               context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
               context.WriteHtmlText( "<p /><hr />") ;
               GXUtil.WriteLog("send_http_error_code " + 403.ToString());
               return  ;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E11222 ();
         if (returnInSub) return;
      }

      protected void E11222( )
      {
         /* Start Routine */
         AV23isLoginRepositoryAdm = new SdtGAMRepository(context).isgamadministrator(out  AV15Errors);
         divDefaultauthtypenamecell_Visible = (!AV23isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divDefaultauthtypenamecell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divDefaultauthtypenamecell_Visible), 5, 0)), true);
         divDefaultsecuritypolicyidcell_Visible = (!AV23isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divDefaultsecuritypolicyidcell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divDefaultsecuritypolicyidcell_Visible), 5, 0)), true);
         divDefaultroleidcell_Visible = (!AV23isLoginRepositoryAdm ? 1 : 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divDefaultroleidcell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divDefaultroleidcell_Visible), 5, 0)), true);
         AV7AuthenticationTypes = new SdtGAMRepository(context).getenabledauthenticationtypes(AV24Language, out  AV15Errors);
         AV54GXV1 = 1;
         while ( AV54GXV1 <= AV7AuthenticationTypes.Count )
         {
            AV6AuthenticationType = ((SdtGAMAuthenticationTypeSimple)AV7AuthenticationTypes.Item(AV54GXV1));
            cmbavDefaultauthtypename.addItem(AV6AuthenticationType.gxTpr_Name, AV6AuthenticationType.gxTpr_Description, 0);
            AV54GXV1 = (int)(AV54GXV1+1);
         }
         AV41SecurityPolicies = new SdtGAMRepository(context).getsecuritypolicies(AV17FilterSecPol, out  AV15Errors);
         AV55GXV2 = 1;
         while ( AV55GXV2 <= AV41SecurityPolicies.Count )
         {
            AV42SecurityPolicy = ((SdtGAMSecurityPolicy)AV41SecurityPolicies.Item(AV55GXV2));
            cmbavDefaultsecuritypolicyid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV42SecurityPolicy.gxTpr_Id), 9, 0)), AV42SecurityPolicy.gxTpr_Name, 0);
            AV55GXV2 = (int)(AV55GXV2+1);
         }
         AV39Roles = new SdtGAMRepository(context).getroles(AV16FilterRole, out  AV15Errors);
         AV56GXV3 = 1;
         while ( AV56GXV3 <= AV39Roles.Count )
         {
            AV38Role = ((SdtGAMRole)AV39Roles.Item(AV56GXV3));
            cmbavDefaultroleid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV38Role.gxTpr_Id), 12, 0)), AV38Role.gxTpr_Name, 0);
            AV56GXV3 = (int)(AV56GXV3+1);
         }
         if ( (0==AV31pId) )
         {
            AV22Id = new SdtGAMRepository(context).getid();
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
         }
         else
         {
            AV22Id = AV31pId;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
         }
         AV32Repository.load( (int)(AV22Id));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
         AV21GUID = AV32Repository.gxTpr_Guid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21GUID", AV21GUID);
         AV30NameSpace = AV32Repository.gxTpr_Namespace;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30NameSpace", AV30NameSpace);
         AV29Name = AV32Repository.gxTpr_Name;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29Name", AV29Name);
         AV12Dsc = AV32Repository.gxTpr_Description;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12Dsc", AV12Dsc);
         AV9DefaultAuthTypeName = AV32Repository.gxTpr_Defaultauthenticationtypename;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9DefaultAuthTypeName", AV9DefaultAuthTypeName);
         AV47UserIdentification = AV32Repository.gxTpr_Useridentification;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV47UserIdentification", AV47UserIdentification);
         AV19GenerateSessionStatistics = AV32Repository.gxTpr_Generatesessionstatistics;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19GenerateSessionStatistics", AV19GenerateSessionStatistics);
         AV44UserActivationMethod = AV32Repository.gxTpr_Useractivationmethod;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44UserActivationMethod", AV44UserActivationMethod);
         AV45UserAutomaticActivationTimeout = AV32Repository.gxTpr_Userautomaticactivationtimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45UserAutomaticActivationTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV45UserAutomaticActivationTimeout), 4, 0)));
         AV50UserRememberMeType = AV32Repository.gxTpr_Userremembermetype;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV50UserRememberMeType", AV50UserRememberMeType);
         AV49UserRememberMeTimeOut = AV32Repository.gxTpr_Userremembermetimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV49UserRememberMeTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV49UserRememberMeTimeOut), 4, 0)));
         AV48UserRecoveryPasswordKeyTimeOut = AV32Repository.gxTpr_Userrecoverypasswordkeytimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV48UserRecoveryPasswordKeyTimeOut", StringUtil.LTrim( StringUtil.Str( (decimal)(AV48UserRecoveryPasswordKeyTimeOut), 4, 0)));
         AV28MinimumAmountCharactersInLogin = AV32Repository.gxTpr_Minimumamountcharactersinlogin;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28MinimumAmountCharactersInLogin", StringUtil.LTrim( StringUtil.Str( (decimal)(AV28MinimumAmountCharactersInLogin), 2, 0)));
         AV26LoginAttemptsToLockUser = AV32Repository.gxTpr_Loginattemptstolockuser;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26LoginAttemptsToLockUser", StringUtil.LTrim( StringUtil.Str( (decimal)(AV26LoginAttemptsToLockUser), 2, 0)));
         AV18GAMUnblockUserTimeout = AV32Repository.gxTpr_Gamunblockusertimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18GAMUnblockUserTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18GAMUnblockUserTimeout), 4, 0)));
         AV25LoginAttemptsToLockSession = AV32Repository.gxTpr_Loginattemptstolocksession;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25LoginAttemptsToLockSession", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25LoginAttemptsToLockSession), 2, 0)));
         AV51UserSessionCacheTimeout = AV32Repository.gxTpr_Usersessioncachetimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV51UserSessionCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV51UserSessionCacheTimeout), 6, 0)));
         AV33RepositoryCacheTimeout = AV32Repository.gxTpr_Cachetimeout;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33RepositoryCacheTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV33RepositoryCacheTimeout), 6, 0)));
         AV27LogoutBehavior = AV32Repository.gxTpr_Gamremotelogoutbehavior;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27LogoutBehavior", AV27LogoutBehavior);
         AV40SecurityAdministratorEmail = AV32Repository.gxTpr_Securityadministratoremail;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40SecurityAdministratorEmail", AV40SecurityAdministratorEmail);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vSECURITYADMINISTRATOREMAIL", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV40SecurityAdministratorEmail, "")), context));
         AV20GiveAnonymousSession = AV32Repository.gxTpr_Giveanonymoussession;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20GiveAnonymousSession", AV20GiveAnonymousSession);
         AV8CanRegisterUsers = AV32Repository.gxTpr_Canregisterusers;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8CanRegisterUsers", AV8CanRegisterUsers);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCANREGISTERUSERS", GetSecureSignedToken( "", AV8CanRegisterUsers, context));
         AV46UserEmailisUnique = AV32Repository.gxTpr_Useremailisunique;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV46UserEmailisUnique", AV46UserEmailisUnique);
         AV11DefaultSecurityPolicyId = AV32Repository.gxTpr_Defaultsecuritypolicyid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11DefaultSecurityPolicyId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11DefaultSecurityPolicyId), 9, 0)));
         AV10DefaultRoleId = AV32Repository.gxTpr_Defaultroleid;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10DefaultRoleId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10DefaultRoleId), 12, 0)));
         AV13EnableTracing = AV32Repository.gxTpr_Enabletracing;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13EnableTracing", StringUtil.LTrim( StringUtil.Str( (decimal)(AV13EnableTracing), 4, 0)));
         AV5AllowOauthAccess = AV32Repository.gxTpr_Allowoauthaccess;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AllowOauthAccess", AV5AllowOauthAccess);
         AV43SessionExpiresOnIPChange = AV32Repository.gxTpr_Sessionexpiresonipchange;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43SessionExpiresOnIPChange", AV43SessionExpiresOnIPChange);
         AV37RequiredPassword = AV32Repository.gxTpr_Requiredpassword;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37RequiredPassword", AV37RequiredPassword);
         AV34RequiredEmail = AV32Repository.gxTpr_Requiredemail;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RequiredEmail", AV34RequiredEmail);
         AV35RequiredFirstName = AV32Repository.gxTpr_Requiredfirstname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35RequiredFirstName", AV35RequiredFirstName);
         AV36RequiredLastName = AV32Repository.gxTpr_Requiredlastname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36RequiredLastName", AV36RequiredLastName);
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E12222 ();
         if (returnInSub) return;
      }

      protected void E12222( )
      {
         /* Enter Routine */
         AV32Repository.load( (int)(AV22Id));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22Id), "ZZZZZZZZZZZ9"), context));
         AV32Repository.gxTpr_Name = AV29Name;
         AV32Repository.gxTpr_Description = AV12Dsc;
         AV32Repository.gxTpr_Defaultauthenticationtypename = AV9DefaultAuthTypeName;
         AV32Repository.gxTpr_Useridentification = AV47UserIdentification;
         AV32Repository.gxTpr_Generatesessionstatistics = AV19GenerateSessionStatistics;
         AV32Repository.gxTpr_Useractivationmethod = AV44UserActivationMethod;
         AV32Repository.gxTpr_Userautomaticactivationtimeout = AV45UserAutomaticActivationTimeout;
         AV32Repository.gxTpr_Gamunblockusertimeout = AV18GAMUnblockUserTimeout;
         AV32Repository.gxTpr_Userremembermetype = AV50UserRememberMeType;
         AV32Repository.gxTpr_Userremembermetimeout = AV49UserRememberMeTimeOut;
         AV32Repository.gxTpr_Userrecoverypasswordkeytimeout = AV48UserRecoveryPasswordKeyTimeOut;
         AV32Repository.gxTpr_Gamremotelogoutbehavior = AV27LogoutBehavior;
         AV32Repository.gxTpr_Minimumamountcharactersinlogin = AV28MinimumAmountCharactersInLogin;
         AV32Repository.gxTpr_Loginattemptstolockuser = AV26LoginAttemptsToLockUser;
         AV32Repository.gxTpr_Loginattemptstolocksession = AV25LoginAttemptsToLockSession;
         AV32Repository.gxTpr_Usersessioncachetimeout = AV51UserSessionCacheTimeout;
         AV32Repository.gxTpr_Cachetimeout = AV33RepositoryCacheTimeout;
         AV32Repository.gxTpr_Securityadministratoremail = AV40SecurityAdministratorEmail;
         AV32Repository.gxTpr_Giveanonymoussession = AV20GiveAnonymousSession;
         AV32Repository.gxTpr_Canregisterusers = AV8CanRegisterUsers;
         AV32Repository.gxTpr_Useremailisunique = AV46UserEmailisUnique;
         AV32Repository.gxTpr_Defaultsecuritypolicyid = AV11DefaultSecurityPolicyId;
         AV32Repository.gxTpr_Defaultroleid = AV10DefaultRoleId;
         AV32Repository.gxTpr_Enabletracing = AV13EnableTracing;
         AV32Repository.gxTpr_Allowoauthaccess = AV5AllowOauthAccess;
         AV32Repository.gxTpr_Sessionexpiresonipchange = AV43SessionExpiresOnIPChange;
         AV32Repository.gxTpr_Requiredpassword = AV37RequiredPassword;
         AV32Repository.gxTpr_Requiredemail = AV34RequiredEmail;
         AV32Repository.gxTpr_Requiredfirstname = AV35RequiredFirstName;
         AV32Repository.gxTpr_Requiredlastname = AV36RequiredLastName;
         AV32Repository.save();
         if ( AV32Repository.success() )
         {
            pr_gam.commit( "GAMRepositoryConfiguration");
            pr_default.commit( "GAMRepositoryConfiguration");
            GX_msglist.addItem(context.GetMessage( "Los datos han sido actualizados.", ""));
         }
         else
         {
            AV15Errors = AV32Repository.geterrors();
            AV57GXV4 = 1;
            while ( AV57GXV4 <= AV15Errors.Count )
            {
               AV14Error = ((SdtGAMError)AV15Errors.Item(AV57GXV4));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV14Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV57GXV4 = (int)(AV57GXV4+1);
            }
         }
         /*  Sending Event outputs  */
      }

      protected void nextLoad( )
      {
      }

      protected void E13222( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV31pId = Convert.ToInt64(getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31pId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV31pId), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV31pId), "ZZZZZZZZZZZ9"), context));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA222( ) ;
         WS222( ) ;
         WE222( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171533951", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gamrepositoryconfiguration.js", "?201811171533953", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManager.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/json2005.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/rsh.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManagerCreate.js", "", false);
         context.AddJavascriptSource("Tab/TabRender.js", "", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         divTable1_Internalname = "TABLE1";
         lblGeneral_title_Internalname = "GENERAL_TITLE";
         edtavId_Internalname = "vID";
         edtavGuid_Internalname = "vGUID";
         edtavNamespace_Internalname = "vNAMESPACE";
         edtavName_Internalname = "vNAME";
         edtavDsc_Internalname = "vDSC";
         cmbavDefaultauthtypename_Internalname = "vDEFAULTAUTHTYPENAME";
         divDefaultauthtypenamecell_Internalname = "DEFAULTAUTHTYPENAMECELL";
         chkavSessionexpiresonipchange_Internalname = "vSESSIONEXPIRESONIPCHANGE";
         chkavAllowoauthaccess_Internalname = "vALLOWOAUTHACCESS";
         cmbavDefaultsecuritypolicyid_Internalname = "vDEFAULTSECURITYPOLICYID";
         divDefaultsecuritypolicyidcell_Internalname = "DEFAULTSECURITYPOLICYIDCELL";
         cmbavLogoutbehavior_Internalname = "vLOGOUTBEHAVIOR";
         divSsobehaviorcell_Internalname = "SSOBEHAVIORCELL";
         cmbavDefaultroleid_Internalname = "vDEFAULTROLEID";
         divDefaultroleidcell_Internalname = "DEFAULTROLEIDCELL";
         cmbavEnabletracing_Internalname = "vENABLETRACING";
         divTabpage1table_Internalname = "TABPAGE1TABLE";
         lblUsers_title_Internalname = "USERS_TITLE";
         cmbavUseridentification_Internalname = "vUSERIDENTIFICATION";
         cmbavUseractivationmethod_Internalname = "vUSERACTIVATIONMETHOD";
         edtavUserautomaticactivationtimeout_Internalname = "vUSERAUTOMATICACTIVATIONTIMEOUT";
         chkavUseremailisunique_Internalname = "vUSEREMAILISUNIQUE";
         chkavRequiredemail_Internalname = "vREQUIREDEMAIL";
         chkavRequiredpassword_Internalname = "vREQUIREDPASSWORD";
         chkavRequiredfirstname_Internalname = "vREQUIREDFIRSTNAME";
         chkavRequiredlastname_Internalname = "vREQUIREDLASTNAME";
         divTabpage2table_Internalname = "TABPAGE2TABLE";
         lblSession_title_Internalname = "SESSION_TITLE";
         cmbavGeneratesessionstatistics_Internalname = "vGENERATESESSIONSTATISTICS";
         edtavUsersessioncachetimeout_Internalname = "vUSERSESSIONCACHETIMEOUT";
         chkavGiveanonymoussession_Internalname = "vGIVEANONYMOUSSESSION";
         edtavLoginattemptstolocksession_Internalname = "vLOGINATTEMPTSTOLOCKSESSION";
         edtavGamunblockusertimeout_Internalname = "vGAMUNBLOCKUSERTIMEOUT";
         edtavLoginattemptstolockuser_Internalname = "vLOGINATTEMPTSTOLOCKUSER";
         edtavMinimumamountcharactersinlogin_Internalname = "vMINIMUMAMOUNTCHARACTERSINLOGIN";
         edtavUserrecoverypasswordkeytimeout_Internalname = "vUSERRECOVERYPASSWORDKEYTIMEOUT";
         edtavUserremembermetimeout_Internalname = "vUSERREMEMBERMETIMEOUT";
         cmbavUserremembermetype_Internalname = "vUSERREMEMBERMETYPE";
         edtavRepositorycachetimeout_Internalname = "vREPOSITORYCACHETIMEOUT";
         divTabpage3table_Internalname = "TABPAGE3TABLE";
         Tab1_Internalname = "TAB1";
         bttConfirm_Internalname = "CONFIRM";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         chkavGiveanonymoussession.Caption = "Give anonymous session?";
         chkavRequiredlastname.Caption = "Required last name?";
         chkavRequiredfirstname.Caption = "Required first name?";
         chkavRequiredpassword.Caption = "Required password?";
         chkavRequiredemail.Caption = "Required email?";
         chkavUseremailisunique.Caption = "User email is unique?";
         chkavAllowoauthaccess.Caption = "Allow oauth access (Smart Devices)";
         chkavSessionexpiresonipchange.Caption = "GAM Session expires on IP change?";
         edtavRepositorycachetimeout_Jsonclick = "";
         edtavRepositorycachetimeout_Enabled = 1;
         cmbavUserremembermetype_Jsonclick = "";
         cmbavUserremembermetype.Enabled = 1;
         edtavUserremembermetimeout_Jsonclick = "";
         edtavUserremembermetimeout_Enabled = 1;
         edtavUserrecoverypasswordkeytimeout_Jsonclick = "";
         edtavUserrecoverypasswordkeytimeout_Enabled = 1;
         edtavMinimumamountcharactersinlogin_Jsonclick = "";
         edtavMinimumamountcharactersinlogin_Enabled = 1;
         edtavLoginattemptstolockuser_Jsonclick = "";
         edtavLoginattemptstolockuser_Enabled = 1;
         edtavGamunblockusertimeout_Jsonclick = "";
         edtavGamunblockusertimeout_Enabled = 1;
         edtavLoginattemptstolocksession_Jsonclick = "";
         edtavLoginattemptstolocksession_Enabled = 1;
         chkavGiveanonymoussession.Enabled = 1;
         edtavUsersessioncachetimeout_Jsonclick = "";
         edtavUsersessioncachetimeout_Enabled = 1;
         cmbavGeneratesessionstatistics_Jsonclick = "";
         cmbavGeneratesessionstatistics.Enabled = 1;
         chkavRequiredlastname.Enabled = 1;
         chkavRequiredfirstname.Enabled = 1;
         chkavRequiredpassword.Enabled = 1;
         chkavRequiredemail.Enabled = 1;
         chkavUseremailisunique.Enabled = 1;
         edtavUserautomaticactivationtimeout_Jsonclick = "";
         edtavUserautomaticactivationtimeout_Enabled = 1;
         cmbavUseractivationmethod_Jsonclick = "";
         cmbavUseractivationmethod.Enabled = 1;
         cmbavUseridentification_Jsonclick = "";
         cmbavUseridentification.Enabled = 1;
         cmbavEnabletracing_Jsonclick = "";
         cmbavEnabletracing.Enabled = 1;
         cmbavDefaultroleid_Jsonclick = "";
         cmbavDefaultroleid.Enabled = 1;
         divDefaultroleidcell_Visible = 1;
         cmbavLogoutbehavior_Jsonclick = "";
         cmbavLogoutbehavior.Enabled = 1;
         cmbavDefaultsecuritypolicyid_Jsonclick = "";
         cmbavDefaultsecuritypolicyid.Enabled = 1;
         divDefaultsecuritypolicyidcell_Visible = 1;
         chkavAllowoauthaccess.Enabled = 1;
         chkavSessionexpiresonipchange.Enabled = 1;
         cmbavDefaultauthtypename_Jsonclick = "";
         cmbavDefaultauthtypename.Enabled = 1;
         divDefaultauthtypenamecell_Visible = 1;
         edtavDsc_Jsonclick = "";
         edtavDsc_Enabled = 1;
         edtavName_Jsonclick = "";
         edtavName_Enabled = 1;
         edtavNamespace_Jsonclick = "";
         edtavNamespace_Enabled = 1;
         edtavGuid_Jsonclick = "";
         edtavGuid_Enabled = 1;
         edtavId_Jsonclick = "";
         edtavId_Enabled = 1;
         Tab1_Historymanagement = Convert.ToBoolean( 0);
         Tab1_Pagecount = 3;
         Tab1_Class = "Tab";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Repository configuration ";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV31pId',fld:'vPID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV22Id',fld:'vID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV40SecurityAdministratorEmail',fld:'vSECURITYADMINISTRATOREMAIL',pic:'',hsh:true,nv:''},{av:'AV8CanRegisterUsers',fld:'vCANREGISTERUSERS',pic:'',hsh:true,nv:false}],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E12222',iparms:[{av:'AV22Id',fld:'vID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV29Name',fld:'vNAME',pic:'',nv:''},{av:'AV12Dsc',fld:'vDSC',pic:'',nv:''},{av:'cmbavDefaultauthtypename'},{av:'AV9DefaultAuthTypeName',fld:'vDEFAULTAUTHTYPENAME',pic:'',nv:''},{av:'cmbavUseridentification'},{av:'AV47UserIdentification',fld:'vUSERIDENTIFICATION',pic:'',nv:''},{av:'cmbavGeneratesessionstatistics'},{av:'AV19GenerateSessionStatistics',fld:'vGENERATESESSIONSTATISTICS',pic:'',nv:''},{av:'cmbavUseractivationmethod'},{av:'AV44UserActivationMethod',fld:'vUSERACTIVATIONMETHOD',pic:'',nv:''},{av:'AV45UserAutomaticActivationTimeout',fld:'vUSERAUTOMATICACTIVATIONTIMEOUT',pic:'ZZZ9',nv:0},{av:'AV18GAMUnblockUserTimeout',fld:'vGAMUNBLOCKUSERTIMEOUT',pic:'ZZZ9',nv:0},{av:'cmbavUserremembermetype'},{av:'AV50UserRememberMeType',fld:'vUSERREMEMBERMETYPE',pic:'',nv:''},{av:'AV49UserRememberMeTimeOut',fld:'vUSERREMEMBERMETIMEOUT',pic:'ZZZ9',nv:0},{av:'AV48UserRecoveryPasswordKeyTimeOut',fld:'vUSERRECOVERYPASSWORDKEYTIMEOUT',pic:'ZZZ9',nv:0},{av:'cmbavLogoutbehavior'},{av:'AV27LogoutBehavior',fld:'vLOGOUTBEHAVIOR',pic:'',nv:''},{av:'AV28MinimumAmountCharactersInLogin',fld:'vMINIMUMAMOUNTCHARACTERSINLOGIN',pic:'Z9',nv:0},{av:'AV26LoginAttemptsToLockUser',fld:'vLOGINATTEMPTSTOLOCKUSER',pic:'Z9',nv:0},{av:'AV25LoginAttemptsToLockSession',fld:'vLOGINATTEMPTSTOLOCKSESSION',pic:'Z9',nv:0},{av:'AV51UserSessionCacheTimeout',fld:'vUSERSESSIONCACHETIMEOUT',pic:'ZZZZZ9',nv:0},{av:'AV33RepositoryCacheTimeout',fld:'vREPOSITORYCACHETIMEOUT',pic:'ZZZZZ9',nv:0},{av:'AV40SecurityAdministratorEmail',fld:'vSECURITYADMINISTRATOREMAIL',pic:'',hsh:true,nv:''},{av:'AV20GiveAnonymousSession',fld:'vGIVEANONYMOUSSESSION',pic:'',nv:false},{av:'AV8CanRegisterUsers',fld:'vCANREGISTERUSERS',pic:'',hsh:true,nv:false},{av:'AV46UserEmailisUnique',fld:'vUSEREMAILISUNIQUE',pic:'',nv:false},{av:'cmbavDefaultsecuritypolicyid'},{av:'AV11DefaultSecurityPolicyId',fld:'vDEFAULTSECURITYPOLICYID',pic:'ZZZZZZZZ9',nv:0},{av:'cmbavDefaultroleid'},{av:'AV10DefaultRoleId',fld:'vDEFAULTROLEID',pic:'ZZZZZZZZZZZ9',nv:0},{av:'cmbavEnabletracing'},{av:'AV13EnableTracing',fld:'vENABLETRACING',pic:'ZZZ9',nv:0},{av:'AV5AllowOauthAccess',fld:'vALLOWOAUTHACCESS',pic:'',nv:false},{av:'AV43SessionExpiresOnIPChange',fld:'vSESSIONEXPIRESONIPCHANGE',pic:'',nv:false},{av:'AV37RequiredPassword',fld:'vREQUIREDPASSWORD',pic:'',nv:false},{av:'AV34RequiredEmail',fld:'vREQUIREDEMAIL',pic:'',nv:false},{av:'AV35RequiredFirstName',fld:'vREQUIREDFIRSTNAME',pic:'',nv:false},{av:'AV36RequiredLastName',fld:'vREQUIREDLASTNAME',pic:'',nv:false}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         forbiddenHiddens = "";
         AV40SecurityAdministratorEmail = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTextblock1_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         lblGeneral_title_Jsonclick = "";
         TempTags = "";
         AV21GUID = "";
         AV30NameSpace = "";
         AV29Name = "";
         AV12Dsc = "";
         AV9DefaultAuthTypeName = "";
         AV27LogoutBehavior = "";
         lblUsers_title_Jsonclick = "";
         AV47UserIdentification = "";
         AV44UserActivationMethod = "";
         lblSession_title_Jsonclick = "";
         AV19GenerateSessionStatistics = "";
         AV50UserRememberMeType = "";
         bttConfirm_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         hsh = "";
         AV15Errors = new GXExternalCollection<SdtGAMError>( context, "SdtGAMError", "GeneXus.Programs");
         AV7AuthenticationTypes = new GXExternalCollection<SdtGAMAuthenticationTypeSimple>( context, "SdtGAMAuthenticationTypeSimple", "GeneXus.Programs");
         AV24Language = "";
         AV6AuthenticationType = new SdtGAMAuthenticationTypeSimple(context);
         AV41SecurityPolicies = new GXExternalCollection<SdtGAMSecurityPolicy>( context, "SdtGAMSecurityPolicy", "GeneXus.Programs");
         AV17FilterSecPol = new SdtGAMSecurityPolicyFilter(context);
         AV42SecurityPolicy = new SdtGAMSecurityPolicy(context);
         AV39Roles = new GXExternalCollection<SdtGAMRole>( context, "SdtGAMRole", "GeneXus.Programs");
         AV16FilterRole = new SdtGAMRoleFilter(context);
         AV38Role = new SdtGAMRole(context);
         AV32Repository = new SdtGAMRepository(context);
         AV14Error = new SdtGAMError(context);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.gamrepositoryconfiguration__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamrepositoryconfiguration__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
         edtavNamespace_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV13EnableTracing ;
      private short AV45UserAutomaticActivationTimeout ;
      private short AV25LoginAttemptsToLockSession ;
      private short AV18GAMUnblockUserTimeout ;
      private short AV26LoginAttemptsToLockUser ;
      private short AV28MinimumAmountCharactersInLogin ;
      private short AV48UserRecoveryPasswordKeyTimeOut ;
      private short AV49UserRememberMeTimeOut ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int Tab1_Pagecount ;
      private int edtavId_Enabled ;
      private int edtavGuid_Enabled ;
      private int edtavNamespace_Enabled ;
      private int edtavName_Enabled ;
      private int edtavDsc_Enabled ;
      private int divDefaultauthtypenamecell_Visible ;
      private int divDefaultsecuritypolicyidcell_Visible ;
      private int AV11DefaultSecurityPolicyId ;
      private int divDefaultroleidcell_Visible ;
      private int edtavUserautomaticactivationtimeout_Enabled ;
      private int AV51UserSessionCacheTimeout ;
      private int edtavUsersessioncachetimeout_Enabled ;
      private int edtavLoginattemptstolocksession_Enabled ;
      private int edtavGamunblockusertimeout_Enabled ;
      private int edtavLoginattemptstolockuser_Enabled ;
      private int edtavMinimumamountcharactersinlogin_Enabled ;
      private int edtavUserrecoverypasswordkeytimeout_Enabled ;
      private int edtavUserremembermetimeout_Enabled ;
      private int AV33RepositoryCacheTimeout ;
      private int edtavRepositorycachetimeout_Enabled ;
      private int AV54GXV1 ;
      private int AV55GXV2 ;
      private int AV56GXV3 ;
      private int AV57GXV4 ;
      private int idxLst ;
      private long AV31pId ;
      private long wcpOAV31pId ;
      private long AV22Id ;
      private long AV10DefaultRoleId ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String forbiddenHiddens ;
      private String Tab1_Class ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String divTable1_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String lblGeneral_title_Internalname ;
      private String lblGeneral_title_Jsonclick ;
      private String divTabpage1table_Internalname ;
      private String edtavId_Internalname ;
      private String TempTags ;
      private String edtavId_Jsonclick ;
      private String edtavGuid_Internalname ;
      private String AV21GUID ;
      private String edtavGuid_Jsonclick ;
      private String edtavNamespace_Internalname ;
      private String AV30NameSpace ;
      private String edtavNamespace_Jsonclick ;
      private String edtavName_Internalname ;
      private String AV29Name ;
      private String edtavName_Jsonclick ;
      private String edtavDsc_Internalname ;
      private String AV12Dsc ;
      private String edtavDsc_Jsonclick ;
      private String divDefaultauthtypenamecell_Internalname ;
      private String cmbavDefaultauthtypename_Internalname ;
      private String AV9DefaultAuthTypeName ;
      private String cmbavDefaultauthtypename_Jsonclick ;
      private String chkavSessionexpiresonipchange_Internalname ;
      private String chkavAllowoauthaccess_Internalname ;
      private String divDefaultsecuritypolicyidcell_Internalname ;
      private String cmbavDefaultsecuritypolicyid_Internalname ;
      private String cmbavDefaultsecuritypolicyid_Jsonclick ;
      private String divSsobehaviorcell_Internalname ;
      private String cmbavLogoutbehavior_Internalname ;
      private String AV27LogoutBehavior ;
      private String cmbavLogoutbehavior_Jsonclick ;
      private String divDefaultroleidcell_Internalname ;
      private String cmbavDefaultroleid_Internalname ;
      private String cmbavDefaultroleid_Jsonclick ;
      private String cmbavEnabletracing_Internalname ;
      private String cmbavEnabletracing_Jsonclick ;
      private String lblUsers_title_Internalname ;
      private String lblUsers_title_Jsonclick ;
      private String divTabpage2table_Internalname ;
      private String cmbavUseridentification_Internalname ;
      private String AV47UserIdentification ;
      private String cmbavUseridentification_Jsonclick ;
      private String cmbavUseractivationmethod_Internalname ;
      private String AV44UserActivationMethod ;
      private String cmbavUseractivationmethod_Jsonclick ;
      private String edtavUserautomaticactivationtimeout_Internalname ;
      private String edtavUserautomaticactivationtimeout_Jsonclick ;
      private String chkavUseremailisunique_Internalname ;
      private String chkavRequiredemail_Internalname ;
      private String chkavRequiredpassword_Internalname ;
      private String chkavRequiredfirstname_Internalname ;
      private String chkavRequiredlastname_Internalname ;
      private String lblSession_title_Internalname ;
      private String lblSession_title_Jsonclick ;
      private String divTabpage3table_Internalname ;
      private String cmbavGeneratesessionstatistics_Internalname ;
      private String AV19GenerateSessionStatistics ;
      private String cmbavGeneratesessionstatistics_Jsonclick ;
      private String edtavUsersessioncachetimeout_Internalname ;
      private String edtavUsersessioncachetimeout_Jsonclick ;
      private String chkavGiveanonymoussession_Internalname ;
      private String edtavLoginattemptstolocksession_Internalname ;
      private String edtavLoginattemptstolocksession_Jsonclick ;
      private String edtavGamunblockusertimeout_Internalname ;
      private String edtavGamunblockusertimeout_Jsonclick ;
      private String edtavLoginattemptstolockuser_Internalname ;
      private String edtavLoginattemptstolockuser_Jsonclick ;
      private String edtavMinimumamountcharactersinlogin_Internalname ;
      private String edtavMinimumamountcharactersinlogin_Jsonclick ;
      private String edtavUserrecoverypasswordkeytimeout_Internalname ;
      private String edtavUserrecoverypasswordkeytimeout_Jsonclick ;
      private String edtavUserremembermetimeout_Internalname ;
      private String edtavUserremembermetimeout_Jsonclick ;
      private String cmbavUserremembermetype_Internalname ;
      private String AV50UserRememberMeType ;
      private String cmbavUserremembermetype_Jsonclick ;
      private String edtavRepositorycachetimeout_Internalname ;
      private String edtavRepositorycachetimeout_Jsonclick ;
      private String bttConfirm_Internalname ;
      private String bttConfirm_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String hsh ;
      private String AV24Language ;
      private String Tab1_Internalname ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV8CanRegisterUsers ;
      private bool Tab1_Historymanagement ;
      private bool wbLoad ;
      private bool AV43SessionExpiresOnIPChange ;
      private bool AV5AllowOauthAccess ;
      private bool AV46UserEmailisUnique ;
      private bool AV34RequiredEmail ;
      private bool AV37RequiredPassword ;
      private bool AV35RequiredFirstName ;
      private bool AV36RequiredLastName ;
      private bool AV20GiveAnonymousSession ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV23isLoginRepositoryAdm ;
      private String AV40SecurityAdministratorEmail ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavDefaultauthtypename ;
      private GXCheckbox chkavSessionexpiresonipchange ;
      private GXCheckbox chkavAllowoauthaccess ;
      private GXCombobox cmbavDefaultsecuritypolicyid ;
      private GXCombobox cmbavLogoutbehavior ;
      private GXCombobox cmbavDefaultroleid ;
      private GXCombobox cmbavEnabletracing ;
      private GXCombobox cmbavUseridentification ;
      private GXCombobox cmbavUseractivationmethod ;
      private GXCheckbox chkavUseremailisunique ;
      private GXCheckbox chkavRequiredemail ;
      private GXCheckbox chkavRequiredpassword ;
      private GXCheckbox chkavRequiredfirstname ;
      private GXCheckbox chkavRequiredlastname ;
      private GXCombobox cmbavGeneratesessionstatistics ;
      private GXCheckbox chkavGiveanonymoussession ;
      private GXCombobox cmbavUserremembermetype ;
      private IDataStoreProvider pr_gam ;
      private IDataStoreProvider pr_default ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXExternalCollection<SdtGAMAuthenticationTypeSimple> AV7AuthenticationTypes ;
      private GXExternalCollection<SdtGAMError> AV15Errors ;
      private GXExternalCollection<SdtGAMSecurityPolicy> AV41SecurityPolicies ;
      private GXExternalCollection<SdtGAMRole> AV39Roles ;
      private GXWebForm Form ;
      private SdtGAMAuthenticationTypeSimple AV6AuthenticationType ;
      private SdtGAMError AV14Error ;
      private SdtGAMRoleFilter AV16FilterRole ;
      private SdtGAMSecurityPolicyFilter AV17FilterSecPol ;
      private SdtGAMRepository AV32Repository ;
      private SdtGAMSecurityPolicy AV42SecurityPolicy ;
      private SdtGAMRole AV38Role ;
   }

   public class gamrepositoryconfiguration__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class gamrepositoryconfiguration__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
