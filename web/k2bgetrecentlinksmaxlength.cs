/*
               File: K2BGetRecentLinksMaxLength
        Description: Get Max Recent Links To Show
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:34.41
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bgetrecentlinksmaxlength : GXProcedure
   {
      public k2bgetrecentlinksmaxlength( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bgetrecentlinksmaxlength( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out int aP0_MaxLength )
      {
         this.AV8MaxLength = 0 ;
         initialize();
         executePrivate();
         aP0_MaxLength=this.AV8MaxLength;
      }

      public int executeUdp( )
      {
         this.AV8MaxLength = 0 ;
         initialize();
         executePrivate();
         aP0_MaxLength=this.AV8MaxLength;
         return AV8MaxLength ;
      }

      public void executeSubmit( out int aP0_MaxLength )
      {
         k2bgetrecentlinksmaxlength objk2bgetrecentlinksmaxlength;
         objk2bgetrecentlinksmaxlength = new k2bgetrecentlinksmaxlength();
         objk2bgetrecentlinksmaxlength.AV8MaxLength = 0 ;
         objk2bgetrecentlinksmaxlength.context.SetSubmitInitialConfig(context);
         objk2bgetrecentlinksmaxlength.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bgetrecentlinksmaxlength);
         aP0_MaxLength=this.AV8MaxLength;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bgetrecentlinksmaxlength)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV8MaxLength = 3;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV8MaxLength ;
      private int aP0_MaxLength ;
   }

}
