/*
               File: K2BStackPush
        Description: Push
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:35.48
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bstackpush : GXProcedure
   {
      public k2bstackpush( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bstackpush( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack ,
                           String aP1_FormCaption )
      {
         this.AV8Stack = aP0_Stack;
         this.AV10FormCaption = aP1_FormCaption;
         initialize();
         executePrivate();
      }

      public void executeSubmit( GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack ,
                                 String aP1_FormCaption )
      {
         k2bstackpush objk2bstackpush;
         objk2bstackpush = new k2bstackpush();
         objk2bstackpush.AV8Stack = aP0_Stack;
         objk2bstackpush.AV10FormCaption = aP1_FormCaption;
         objk2bstackpush.context.SetSubmitInitialConfig(context);
         objk2bstackpush.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bstackpush);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bstackpush)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV11stackItem = new SdtK2BStack_K2BStackItem(context);
         AV15AuxFormCaption = StringUtil.Trim( AV10FormCaption);
         GXt_char1 = "";
         new k2bfixrecentlinkscaption(context ).execute(  AV10FormCaption, out  GXt_char1) ;
         AV11stackItem.gxTpr_Caption = GXt_char1;
         GXt_char1 = "";
         new k2blinkto(context ).execute(  AV12Request, out  GXt_char1) ;
         AV11stackItem.gxTpr_Url = GXt_char1;
         AV8Stack.Add(AV11stackItem, 0);
         new k2bsetstack(context ).execute(  AV8Stack) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV11stackItem = new SdtK2BStack_K2BStackItem(context);
         AV15AuxFormCaption = "";
         GXt_char1 = "";
         AV12Request = new GxHttpRequest( context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV15AuxFormCaption ;
      private String GXt_char1 ;
      private String AV10FormCaption ;
      private GxHttpRequest AV12Request ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> AV8Stack ;
      private SdtK2BStack_K2BStackItem AV11stackItem ;
   }

}
