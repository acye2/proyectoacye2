/*
               File: Gx0020
        Description: Selection List Coach Paciente
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:35.59
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gx0020 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gx0020( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public gx0020( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( out Guid aP0_pCoachPaciente_Coach ,
                           out Guid aP1_pCoachPaciente_Paciente )
      {
         this.AV10pCoachPaciente_Coach = Guid.Empty ;
         this.AV11pCoachPaciente_Paciente = Guid.Empty ;
         executePrivate();
         aP0_pCoachPaciente_Coach=this.AV10pCoachPaciente_Coach;
         aP1_pCoachPaciente_Paciente=this.AV11pCoachPaciente_Paciente;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_64 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_64_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_64_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               subGrid1_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV6cCoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV7cCoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV8cCoachPaciente_FechaAsignacion = context.localUtil.ParseDateParm( GetNextPar( ));
               AV9cCoachPaciente_DispositivoID = GetNextPar( );
               AV13cCoachPaciente_Token = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( subGrid1_Rows, AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, AV9cCoachPaciente_DispositivoID, AV13cCoachPaciente_Token) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV10pCoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( gxfirstwebparm));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10pCoachPaciente_Coach", AV10pCoachPaciente_Coach.ToString());
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV11pCoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11pCoachPaciente_Paciente", AV11pCoachPaciente_Paciente.ToString());
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "gx0020_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdpromptmasterpage", "GeneXus.Programs.rwdpromptmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,true);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA1H2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START1H2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111816263565", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gx0020.aspx") + "?" + UrlEncode(AV10pCoachPaciente_Coach.ToString()) + "," + UrlEncode(AV11pCoachPaciente_Paciente.ToString())+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCCOACHPACIENTE_COACH", AV6cCoachPaciente_Coach.ToString());
         GxWebStd.gx_hidden_field( context, "GXH_vCCOACHPACIENTE_PACIENTE", AV7cCoachPaciente_Paciente.ToString());
         GxWebStd.gx_hidden_field( context, "GXH_vCCOACHPACIENTE_FECHAASIGNACION", context.localUtil.Format(AV8cCoachPaciente_FechaAsignacion, "99/99/99"));
         GxWebStd.gx_hidden_field( context, "GXH_vCCOACHPACIENTE_DISPOSITIVOID", AV9cCoachPaciente_DispositivoID);
         GxWebStd.gx_hidden_field( context, "GXH_vCCOACHPACIENTE_TOKEN", AV13cCoachPaciente_Token);
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_64", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_64), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPCOACHPACIENTE_COACH", AV10pCoachPaciente_Coach.ToString());
         GxWebStd.gx_hidden_field( context, "vPCOACHPACIENTE_PACIENTE", AV11pCoachPaciente_Paciente.ToString());
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ADVANCEDCONTAINER_Class", StringUtil.RTrim( divAdvancedcontainer_Class));
         GxWebStd.gx_hidden_field( context, "BTNTOGGLE_Class", StringUtil.RTrim( bttBtntoggle_Class));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_COACHFILTERCONTAINER_Class", StringUtil.RTrim( divCoachpaciente_coachfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_PACIENTEFILTERCONTAINER_Class", StringUtil.RTrim( divCoachpaciente_pacientefiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_FECHAASIGNACIONFILTERCONTAINER_Class", StringUtil.RTrim( divCoachpaciente_fechaasignacionfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_DISPOSITIVOIDFILTERCONTAINER_Class", StringUtil.RTrim( divCoachpaciente_dispositivoidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_TOKENFILTERCONTAINER_Class", StringUtil.RTrim( divCoachpaciente_tokenfiltercontainer_Class));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", "notset");
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE1H2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT1H2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gx0020.aspx") + "?" + UrlEncode(AV10pCoachPaciente_Coach.ToString()) + "," + UrlEncode(AV11pCoachPaciente_Paciente.ToString()) ;
      }

      public override String GetPgmname( )
      {
         return "Gx0020" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selection List Coach Paciente" ;
      }

      protected void WB1H0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMain_Internalname, 1, 0, "px", 0, "px", "ContainerFluid PromptContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 PromptAdvancedBarCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAdvancedcontainer_Internalname, 1, 0, "px", 0, "px", divAdvancedcontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divCoachpaciente_coachfiltercontainer_Internalname, 1, 0, "px", 0, "px", divCoachpaciente_coachfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblcoachpaciente_coachfilter_Internalname, "Coach Paciente_Coach", "", "", lblLblcoachpaciente_coachfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e111h1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCcoachpaciente_coach_Internalname, "Coach Paciente_Coach", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_64_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCcoachpaciente_coach_Internalname, AV6cCoachPaciente_Coach.ToString(), AV6cCoachPaciente_Coach.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCcoachpaciente_coach_Jsonclick, 0, "Attribute", "", "", "", "", edtavCcoachpaciente_coach_Visible, edtavCcoachpaciente_coach_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divCoachpaciente_pacientefiltercontainer_Internalname, 1, 0, "px", 0, "px", divCoachpaciente_pacientefiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblcoachpaciente_pacientefilter_Internalname, "Coach Paciente_Paciente", "", "", lblLblcoachpaciente_pacientefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e121h1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCcoachpaciente_paciente_Internalname, "Coach Paciente_Paciente", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_64_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCcoachpaciente_paciente_Internalname, AV7cCoachPaciente_Paciente.ToString(), AV7cCoachPaciente_Paciente.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCcoachpaciente_paciente_Jsonclick, 0, "Attribute", "", "", "", "", edtavCcoachpaciente_paciente_Visible, edtavCcoachpaciente_paciente_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divCoachpaciente_fechaasignacionfiltercontainer_Internalname, 1, 0, "px", 0, "px", divCoachpaciente_fechaasignacionfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblcoachpaciente_fechaasignacionfilter_Internalname, "Coach Paciente_Fecha Asignacion", "", "", lblLblcoachpaciente_fechaasignacionfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e131h1_client"+"'", "", "WWAdvancedLabel WWDateFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCcoachpaciente_fechaasignacion_Internalname, "Coach Paciente_Fecha Asignacion", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_64_idx + "',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavCcoachpaciente_fechaasignacion_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavCcoachpaciente_fechaasignacion_Internalname, context.localUtil.Format(AV8cCoachPaciente_FechaAsignacion, "99/99/99"), context.localUtil.Format( AV8cCoachPaciente_FechaAsignacion, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCcoachpaciente_fechaasignacion_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCcoachpaciente_fechaasignacion_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Gx0020.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divCoachpaciente_dispositivoidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divCoachpaciente_dispositivoidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblcoachpaciente_dispositivoidfilter_Internalname, "Coach Paciente_Dispositivo ID", "", "", lblLblcoachpaciente_dispositivoidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e141h1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCcoachpaciente_dispositivoid_Internalname, "Coach Paciente_Dispositivo ID", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_64_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCcoachpaciente_dispositivoid_Internalname, AV9cCoachPaciente_DispositivoID, StringUtil.RTrim( context.localUtil.Format( AV9cCoachPaciente_DispositivoID, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCcoachpaciente_dispositivoid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCcoachpaciente_dispositivoid_Visible, edtavCcoachpaciente_dispositivoid_Enabled, 0, "text", "", 75, "chr", 1, "row", 75, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divCoachpaciente_tokenfiltercontainer_Internalname, 1, 0, "px", 0, "px", divCoachpaciente_tokenfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblcoachpaciente_tokenfilter_Internalname, "Coach Paciente_Token", "", "", lblLblcoachpaciente_tokenfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e151h1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCcoachpaciente_token_Internalname, "Coach Paciente_Token", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_64_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCcoachpaciente_token_Internalname, AV13cCoachPaciente_Token, StringUtil.RTrim( context.localUtil.Format( AV13cCoachPaciente_Token, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCcoachpaciente_token_Jsonclick, 0, "Attribute", "", "", "", "", edtavCcoachpaciente_token_Visible, edtavCcoachpaciente_token_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 WWGridCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridtable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-sm hidden-md hidden-lg ToggleCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 61,'',false,'',0)\"";
            ClassString = bttBtntoggle_Class;
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntoggle_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(64), 2, 0)+","+"null"+");", "|||", bttBtntoggle_Jsonclick, 7, "|||", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e161h1_client"+"'", TempTags, "", 2, "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"64\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "PromptGrid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"SelectionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Paciente_Coach") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Paciente_Paciente") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Paciente_Fecha Asignacion") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  Grid1Container = new GXWebGrid( context);
               }
               else
               {
                  Grid1Container.Clear();
               }
               Grid1Container.SetWrapped(nGXWrapped);
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "PromptGrid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV5LinkSelection));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavLinkselection_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A12CoachPaciente_Coach.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A13CoachPaciente_Paciente.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 64 )
         {
            wbEnd = 0;
            nRC_GXsfl_64 = (short)(nGXsfl_64_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
               Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(64), 2, 0)+","+"null"+");", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gx0020.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START1H2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Selection List Coach Paciente", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1H0( ) ;
      }

      protected void WS1H2( )
      {
         START1H2( ) ;
         EVT1H2( ) ;
      }

      protected void EVT1H2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRID1PAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRID1PAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid1_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid1_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid1_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid1_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_64_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
                              SubsflControlProps_642( ) ;
                              AV5LinkSelection = cgiGet( edtavLinkselection_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV16Linkselection_GXI : context.convertURL( context.PathToRelativeUrl( AV5LinkSelection))), !bGXsfl_64_Refreshing);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "SrcSet", context.GetImageSrcSet( AV5LinkSelection), true);
                              A12CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( cgiGet( edtCoachPaciente_Coach_Internalname)));
                              A13CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtCoachPaciente_Paciente_Internalname)));
                              A18CoachPaciente_FechaAsignacion = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtCoachPaciente_FechaAsignacion_Internalname), 0));
                              n18CoachPaciente_FechaAsignacion = false;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E171H2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E181H2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Ccoachpaciente_coach Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCCOACHPACIENTE_COACH")) != AV6cCoachPaciente_Coach )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ccoachpaciente_paciente Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCCOACHPACIENTE_PACIENTE")) != AV7cCoachPaciente_Paciente )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ccoachpaciente_fechaasignacion Changed */
                                       if ( context.localUtil.CToT( cgiGet( "GXH_vCCOACHPACIENTE_FECHAASIGNACION"), 0) != AV8cCoachPaciente_FechaAsignacion )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ccoachpaciente_dispositivoid Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCCOACHPACIENTE_DISPOSITIVOID"), AV9cCoachPaciente_DispositivoID) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ccoachpaciente_token Changed */
                                       if ( StringUtil.StrCmp(cgiGet( "GXH_vCCOACHPACIENTE_TOKEN"), AV13cCoachPaciente_Token) != 0 )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: Enter */
                                          E191H2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1H2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA1H2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_642( ) ;
         while ( nGXsfl_64_idx <= nRC_GXsfl_64 )
         {
            sendrow_642( ) ;
            nGXsfl_64_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_64_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_64_idx+1));
            sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
            SubsflControlProps_642( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Grid1Container));
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int subGrid1_Rows ,
                                        Guid AV6cCoachPaciente_Coach ,
                                        Guid AV7cCoachPaciente_Paciente ,
                                        DateTime AV8cCoachPaciente_FechaAsignacion ,
                                        String AV9cCoachPaciente_DispositivoID ,
                                        String AV13cCoachPaciente_Token )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         GRID1_nCurrentRecord = 0;
         RF1H2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_COACHPACIENTE_COACH", GetSecureSignedToken( "", A12CoachPaciente_Coach, context));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_COACH", A12CoachPaciente_Coach.ToString());
         GxWebStd.gx_hidden_field( context, "gxhash_COACHPACIENTE_PACIENTE", GetSecureSignedToken( "", A13CoachPaciente_Paciente, context));
         GxWebStd.gx_hidden_field( context, "COACHPACIENTE_PACIENTE", A13CoachPaciente_Paciente.ToString());
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1H2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF1H2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 64;
         nGXsfl_64_idx = 1;
         sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
         SubsflControlProps_642( ) ;
         bGXsfl_64_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "PromptGrid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_642( ) ;
            GXPagingFrom2 = (int)(((10==0) ? 0 : GRID1_nFirstRecordOnPage));
            GXPagingTo2 = ((10==0) ? 10000 : subGrid1_Recordsperpage( )+1);
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV8cCoachPaciente_FechaAsignacion ,
                                                 AV9cCoachPaciente_DispositivoID ,
                                                 AV13cCoachPaciente_Token ,
                                                 A18CoachPaciente_FechaAsignacion ,
                                                 A19CoachPaciente_DispositivoID ,
                                                 A20CoachPaciente_Token ,
                                                 AV6cCoachPaciente_Coach ,
                                                 AV7cCoachPaciente_Paciente } ,
                                                 new int[]{
                                                 TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                                 }
            } ) ;
            lV9cCoachPaciente_DispositivoID = StringUtil.Concat( StringUtil.RTrim( AV9cCoachPaciente_DispositivoID), "%", "");
            lV13cCoachPaciente_Token = StringUtil.Concat( StringUtil.RTrim( AV13cCoachPaciente_Token), "%", "");
            /* Using cursor H001H2 */
            pr_default.execute(0, new Object[] {AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, lV9cCoachPaciente_DispositivoID, lV13cCoachPaciente_Token, GXPagingFrom2, GXPagingTo2, GXPagingTo2});
            nGXsfl_64_idx = 1;
            sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
            SubsflControlProps_642( ) ;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( 10 == 0 ) || ( GRID1_nCurrentRecord < subGrid1_Recordsperpage( ) ) ) ) )
            {
               A20CoachPaciente_Token = H001H2_A20CoachPaciente_Token[0];
               n20CoachPaciente_Token = H001H2_n20CoachPaciente_Token[0];
               A19CoachPaciente_DispositivoID = H001H2_A19CoachPaciente_DispositivoID[0];
               n19CoachPaciente_DispositivoID = H001H2_n19CoachPaciente_DispositivoID[0];
               A18CoachPaciente_FechaAsignacion = H001H2_A18CoachPaciente_FechaAsignacion[0];
               n18CoachPaciente_FechaAsignacion = H001H2_n18CoachPaciente_FechaAsignacion[0];
               A13CoachPaciente_Paciente = (Guid)((Guid)(H001H2_A13CoachPaciente_Paciente[0]));
               A12CoachPaciente_Coach = (Guid)((Guid)(H001H2_A12CoachPaciente_Coach[0]));
               /* Execute user event: Load */
               E181H2 ();
               pr_default.readNext(0);
            }
            GRID1_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 64;
            WB1H0( ) ;
         }
         bGXsfl_64_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes1H2( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_COACHPACIENTE_COACH"+"_"+sGXsfl_64_idx, GetSecureSignedToken( sGXsfl_64_idx, A12CoachPaciente_Coach, context));
         GxWebStd.gx_hidden_field( context, "gxhash_COACHPACIENTE_PACIENTE"+"_"+sGXsfl_64_idx, GetSecureSignedToken( sGXsfl_64_idx, A13CoachPaciente_Paciente, context));
      }

      protected int subGrid1_Pagecount( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV8cCoachPaciente_FechaAsignacion ,
                                              AV9cCoachPaciente_DispositivoID ,
                                              AV13cCoachPaciente_Token ,
                                              A18CoachPaciente_FechaAsignacion ,
                                              A19CoachPaciente_DispositivoID ,
                                              A20CoachPaciente_Token ,
                                              AV6cCoachPaciente_Coach ,
                                              AV7cCoachPaciente_Paciente } ,
                                              new int[]{
                                              TypeConstants.DATE, TypeConstants.STRING, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN, TypeConstants.STRING, TypeConstants.BOOLEAN
                                              }
         } ) ;
         lV9cCoachPaciente_DispositivoID = StringUtil.Concat( StringUtil.RTrim( AV9cCoachPaciente_DispositivoID), "%", "");
         lV13cCoachPaciente_Token = StringUtil.Concat( StringUtil.RTrim( AV13cCoachPaciente_Token), "%", "");
         /* Using cursor H001H3 */
         pr_default.execute(1, new Object[] {AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, lV9cCoachPaciente_DispositivoID, lV13cCoachPaciente_Token});
         GRID1_nRecordCount = H001H3_AGRID1_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID1_nRecordCount) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(10*1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID1_nFirstRecordOnPage/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected short subgrid1_firstpage( )
      {
         GRID1_nFirstRecordOnPage = 0;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, AV9cCoachPaciente_DispositivoID, AV13cCoachPaciente_Token) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid1_nextpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ( GRID1_nRecordCount >= subGrid1_Recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, AV9cCoachPaciente_DispositivoID, AV13cCoachPaciente_Token) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
         return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid1_previouspage( )
      {
         if ( GRID1_nFirstRecordOnPage >= subGrid1_Recordsperpage( ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, AV9cCoachPaciente_DispositivoID, AV13cCoachPaciente_Token) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid1_lastpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( GRID1_nRecordCount > subGrid1_Recordsperpage( ) )
         {
            if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-subGrid1_Recordsperpage( ));
            }
            else
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))));
            }
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, AV9cCoachPaciente_DispositivoID, AV13cCoachPaciente_Token) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected int subgrid1_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(subGrid1_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cCoachPaciente_Coach, AV7cCoachPaciente_Paciente, AV8cCoachPaciente_FechaAsignacion, AV9cCoachPaciente_DispositivoID, AV13cCoachPaciente_Token) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return (int)(0) ;
      }

      protected void STRUP1H0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E171H2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( StringUtil.StrCmp(cgiGet( edtavCcoachpaciente_coach_Internalname), "") == 0 )
            {
               AV6cCoachPaciente_Coach = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cCoachPaciente_Coach", AV6cCoachPaciente_Coach.ToString());
            }
            else
            {
               try
               {
                  AV6cCoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCcoachpaciente_coach_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cCoachPaciente_Coach", AV6cCoachPaciente_Coach.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCCOACHPACIENTE_COACH");
                  GX_FocusControl = edtavCcoachpaciente_coach_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            if ( StringUtil.StrCmp(cgiGet( edtavCcoachpaciente_paciente_Internalname), "") == 0 )
            {
               AV7cCoachPaciente_Paciente = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cCoachPaciente_Paciente", AV7cCoachPaciente_Paciente.ToString());
            }
            else
            {
               try
               {
                  AV7cCoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCcoachpaciente_paciente_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cCoachPaciente_Paciente", AV7cCoachPaciente_Paciente.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCCOACHPACIENTE_PACIENTE");
                  GX_FocusControl = edtavCcoachpaciente_paciente_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            if ( context.localUtil.VCDateTime( cgiGet( edtavCcoachpaciente_fechaasignacion_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Coach Paciente_Fecha Asignacion"}), 1, "vCCOACHPACIENTE_FECHAASIGNACION");
               GX_FocusControl = edtavCcoachpaciente_fechaasignacion_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV8cCoachPaciente_FechaAsignacion = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cCoachPaciente_FechaAsignacion", context.localUtil.Format(AV8cCoachPaciente_FechaAsignacion, "99/99/99"));
            }
            else
            {
               AV8cCoachPaciente_FechaAsignacion = DateTimeUtil.ResetTime(context.localUtil.CToT( cgiGet( edtavCcoachpaciente_fechaasignacion_Internalname), 0));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cCoachPaciente_FechaAsignacion", context.localUtil.Format(AV8cCoachPaciente_FechaAsignacion, "99/99/99"));
            }
            AV9cCoachPaciente_DispositivoID = cgiGet( edtavCcoachpaciente_dispositivoid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cCoachPaciente_DispositivoID", AV9cCoachPaciente_DispositivoID);
            AV13cCoachPaciente_Token = cgiGet( edtavCcoachpaciente_token_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13cCoachPaciente_Token", AV13cCoachPaciente_Token);
            /* Read saved values. */
            nRC_GXsfl_64 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_64"), ",", "."));
            GRID1_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID1_nFirstRecordOnPage"), ",", "."));
            GRID1_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID1_nEOF"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCCOACHPACIENTE_COACH")) != AV6cCoachPaciente_Coach )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCCOACHPACIENTE_PACIENTE")) != AV7cCoachPaciente_Paciente )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( context.localUtil.CToT( cgiGet( "GXH_vCCOACHPACIENTE_FECHAASIGNACION"), 0) != AV8cCoachPaciente_FechaAsignacion )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCCOACHPACIENTE_DISPOSITIVOID"), AV9cCoachPaciente_DispositivoID) != 0 )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrCmp(cgiGet( "GXH_vCCOACHPACIENTE_TOKEN"), AV13cCoachPaciente_Token) != 0 )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E171H2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E171H2( )
      {
         /* Start Routine */
         Form.Caption = StringUtil.Format( "Lista de Selecci�n %1", "Coach Paciente", "", "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption, true);
         AV12ADVANCED_LABEL_TEMPLATE = "%1 <strong>%2</strong>";
      }

      private void E181H2( )
      {
         /* Load Routine */
         AV5LinkSelection = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLinkselection_Internalname, AV5LinkSelection);
         AV16Linkselection_GXI = GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         sendrow_642( ) ;
         GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ! bGXsfl_64_Refreshing )
         {
            context.DoAjaxLoad(64, Grid1Row);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E191H2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E191H2( )
      {
         /* Enter Routine */
         AV10pCoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10pCoachPaciente_Coach", AV10pCoachPaciente_Coach.ToString());
         AV11pCoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11pCoachPaciente_Paciente", AV11pCoachPaciente_Paciente.ToString());
         context.setWebReturnParms(new Object[] {(Guid)AV10pCoachPaciente_Coach,(Guid)AV11pCoachPaciente_Paciente});
         context.setWebReturnParmsMetadata(new Object[] {"AV10pCoachPaciente_Coach","AV11pCoachPaciente_Paciente"});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         /*  Sending Event outputs  */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV10pCoachPaciente_Coach = (Guid)((Guid)getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10pCoachPaciente_Coach", AV10pCoachPaciente_Coach.ToString());
         AV11pCoachPaciente_Paciente = (Guid)((Guid)getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11pCoachPaciente_Paciente", AV11pCoachPaciente_Paciente.ToString());
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1H2( ) ;
         WS1H2( ) ;
         WE1H2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2018111816263643", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gx0020.js", "?2018111816263643", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_642( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_64_idx;
         edtCoachPaciente_Coach_Internalname = "COACHPACIENTE_COACH_"+sGXsfl_64_idx;
         edtCoachPaciente_Paciente_Internalname = "COACHPACIENTE_PACIENTE_"+sGXsfl_64_idx;
         edtCoachPaciente_FechaAsignacion_Internalname = "COACHPACIENTE_FECHAASIGNACION_"+sGXsfl_64_idx;
      }

      protected void SubsflControlProps_fel_642( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_64_fel_idx;
         edtCoachPaciente_Coach_Internalname = "COACHPACIENTE_COACH_"+sGXsfl_64_fel_idx;
         edtCoachPaciente_Paciente_Internalname = "COACHPACIENTE_PACIENTE_"+sGXsfl_64_fel_idx;
         edtCoachPaciente_FechaAsignacion_Internalname = "COACHPACIENTE_FECHAASIGNACION_"+sGXsfl_64_fel_idx;
      }

      protected void sendrow_642( )
      {
         SubsflControlProps_642( ) ;
         WB1H0( ) ;
         if ( ( 10 * 1 == 0 ) || ( nGXsfl_64_idx <= subGrid1_Recordsperpage( ) * 1 ) )
         {
            Grid1Row = GXWebRow.GetNew(context,Grid1Container);
            if ( subGrid1_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid1_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
            else if ( subGrid1_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid1_Backstyle = 0;
               subGrid1_Backcolor = subGrid1_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Uniform";
               }
            }
            else if ( subGrid1_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
               subGrid1_Backcolor = (int)(0x0);
            }
            else if ( subGrid1_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( ((int)((nGXsfl_64_idx) % (2))) == 0 )
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Even";
                  }
               }
               else
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Odd";
                  }
               }
            }
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+"PromptGrid"+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_64_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( A12CoachPaciente_Coach.ToString())+"'"+", "+"'"+GXUtil.EncodeJSConstant( A13CoachPaciente_Paciente.ToString())+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Link", edtavLinkselection_Link, !bGXsfl_64_Refreshing);
            ClassString = "SelectionAttribute";
            StyleString = "";
            AV5LinkSelection_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection))&&String.IsNullOrEmpty(StringUtil.RTrim( AV16Linkselection_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)));
            sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV16Linkselection_GXI : context.PathToRelativeUrl( AV5LinkSelection));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavLinkselection_Internalname,(String)sImgUrl,(String)edtavLinkselection_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"WWActionColumn",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV5LinkSelection_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCoachPaciente_Coach_Internalname,A12CoachPaciente_Coach.ToString(),A12CoachPaciente_Coach.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCoachPaciente_Coach_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)64,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCoachPaciente_Paciente_Internalname,A13CoachPaciente_Paciente.ToString(),A13CoachPaciente_Paciente.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCoachPaciente_Paciente_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)64,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtCoachPaciente_FechaAsignacion_Internalname,context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"),context.localUtil.Format( A18CoachPaciente_FechaAsignacion, "99/99/99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtCoachPaciente_FechaAsignacion_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)8,(short)0,(short)0,(short)64,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
            send_integrity_lvl_hashes1H2( ) ;
            Grid1Container.AddRow(Grid1Row);
            nGXsfl_64_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_64_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_64_idx+1));
            sGXsfl_64_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_64_idx), 4, 0)), 4, "0");
            SubsflControlProps_642( ) ;
         }
         /* End function sendrow_642 */
      }

      protected void init_default_properties( )
      {
         lblLblcoachpaciente_coachfilter_Internalname = "LBLCOACHPACIENTE_COACHFILTER";
         edtavCcoachpaciente_coach_Internalname = "vCCOACHPACIENTE_COACH";
         divCoachpaciente_coachfiltercontainer_Internalname = "COACHPACIENTE_COACHFILTERCONTAINER";
         lblLblcoachpaciente_pacientefilter_Internalname = "LBLCOACHPACIENTE_PACIENTEFILTER";
         edtavCcoachpaciente_paciente_Internalname = "vCCOACHPACIENTE_PACIENTE";
         divCoachpaciente_pacientefiltercontainer_Internalname = "COACHPACIENTE_PACIENTEFILTERCONTAINER";
         lblLblcoachpaciente_fechaasignacionfilter_Internalname = "LBLCOACHPACIENTE_FECHAASIGNACIONFILTER";
         edtavCcoachpaciente_fechaasignacion_Internalname = "vCCOACHPACIENTE_FECHAASIGNACION";
         divCoachpaciente_fechaasignacionfiltercontainer_Internalname = "COACHPACIENTE_FECHAASIGNACIONFILTERCONTAINER";
         lblLblcoachpaciente_dispositivoidfilter_Internalname = "LBLCOACHPACIENTE_DISPOSITIVOIDFILTER";
         edtavCcoachpaciente_dispositivoid_Internalname = "vCCOACHPACIENTE_DISPOSITIVOID";
         divCoachpaciente_dispositivoidfiltercontainer_Internalname = "COACHPACIENTE_DISPOSITIVOIDFILTERCONTAINER";
         lblLblcoachpaciente_tokenfilter_Internalname = "LBLCOACHPACIENTE_TOKENFILTER";
         edtavCcoachpaciente_token_Internalname = "vCCOACHPACIENTE_TOKEN";
         divCoachpaciente_tokenfiltercontainer_Internalname = "COACHPACIENTE_TOKENFILTERCONTAINER";
         divAdvancedcontainer_Internalname = "ADVANCEDCONTAINER";
         bttBtntoggle_Internalname = "BTNTOGGLE";
         edtavLinkselection_Internalname = "vLINKSELECTION";
         edtCoachPaciente_Coach_Internalname = "COACHPACIENTE_COACH";
         edtCoachPaciente_Paciente_Internalname = "COACHPACIENTE_PACIENTE";
         edtCoachPaciente_FechaAsignacion_Internalname = "COACHPACIENTE_FECHAASIGNACION";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         divGridtable_Internalname = "GRIDTABLE";
         divMain_Internalname = "MAIN";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtCoachPaciente_FechaAsignacion_Jsonclick = "";
         edtCoachPaciente_Paciente_Jsonclick = "";
         edtCoachPaciente_Coach_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         edtavLinkselection_Link = "";
         subGrid1_Class = "PromptGrid";
         subGrid1_Backcolorstyle = 0;
         edtavCcoachpaciente_token_Jsonclick = "";
         edtavCcoachpaciente_token_Enabled = 1;
         edtavCcoachpaciente_token_Visible = 1;
         edtavCcoachpaciente_dispositivoid_Jsonclick = "";
         edtavCcoachpaciente_dispositivoid_Enabled = 1;
         edtavCcoachpaciente_dispositivoid_Visible = 1;
         edtavCcoachpaciente_fechaasignacion_Jsonclick = "";
         edtavCcoachpaciente_fechaasignacion_Enabled = 1;
         edtavCcoachpaciente_paciente_Jsonclick = "";
         edtavCcoachpaciente_paciente_Enabled = 1;
         edtavCcoachpaciente_paciente_Visible = 1;
         edtavCcoachpaciente_coach_Jsonclick = "";
         edtavCcoachpaciente_coach_Enabled = 1;
         edtavCcoachpaciente_coach_Visible = 1;
         divCoachpaciente_tokenfiltercontainer_Class = "AdvancedContainerItem";
         divCoachpaciente_dispositivoidfiltercontainer_Class = "AdvancedContainerItem";
         divCoachpaciente_fechaasignacionfiltercontainer_Class = "AdvancedContainerItem";
         divCoachpaciente_pacientefiltercontainer_Class = "AdvancedContainerItem";
         divCoachpaciente_coachfiltercontainer_Class = "AdvancedContainerItem";
         bttBtntoggle_Class = "BtnToggle";
         divAdvancedcontainer_Class = "AdvancedContainerVisible";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Selection List Coach Paciente";
         subGrid1_Rows = 10;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cCoachPaciente_Coach',fld:'vCCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cCoachPaciente_Paciente',fld:'vCCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cCoachPaciente_FechaAsignacion',fld:'vCCOACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'AV9cCoachPaciente_DispositivoID',fld:'vCCOACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'AV13cCoachPaciente_Token',fld:'vCCOACHPACIENTE_TOKEN',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'TOGGLE'","{handler:'E161H1',iparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}],oparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]}");
         setEventMetadata("LBLCOACHPACIENTE_COACHFILTER.CLICK","{handler:'E111H1',iparms:[{av:'divCoachpaciente_coachfiltercontainer_Class',ctrl:'COACHPACIENTE_COACHFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divCoachpaciente_coachfiltercontainer_Class',ctrl:'COACHPACIENTE_COACHFILTERCONTAINER',prop:'Class'},{av:'edtavCcoachpaciente_coach_Visible',ctrl:'vCCOACHPACIENTE_COACH',prop:'Visible'}]}");
         setEventMetadata("LBLCOACHPACIENTE_PACIENTEFILTER.CLICK","{handler:'E121H1',iparms:[{av:'divCoachpaciente_pacientefiltercontainer_Class',ctrl:'COACHPACIENTE_PACIENTEFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divCoachpaciente_pacientefiltercontainer_Class',ctrl:'COACHPACIENTE_PACIENTEFILTERCONTAINER',prop:'Class'},{av:'edtavCcoachpaciente_paciente_Visible',ctrl:'vCCOACHPACIENTE_PACIENTE',prop:'Visible'}]}");
         setEventMetadata("LBLCOACHPACIENTE_FECHAASIGNACIONFILTER.CLICK","{handler:'E131H1',iparms:[{av:'divCoachpaciente_fechaasignacionfiltercontainer_Class',ctrl:'COACHPACIENTE_FECHAASIGNACIONFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divCoachpaciente_fechaasignacionfiltercontainer_Class',ctrl:'COACHPACIENTE_FECHAASIGNACIONFILTERCONTAINER',prop:'Class'}]}");
         setEventMetadata("LBLCOACHPACIENTE_DISPOSITIVOIDFILTER.CLICK","{handler:'E141H1',iparms:[{av:'divCoachpaciente_dispositivoidfiltercontainer_Class',ctrl:'COACHPACIENTE_DISPOSITIVOIDFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divCoachpaciente_dispositivoidfiltercontainer_Class',ctrl:'COACHPACIENTE_DISPOSITIVOIDFILTERCONTAINER',prop:'Class'},{av:'edtavCcoachpaciente_dispositivoid_Visible',ctrl:'vCCOACHPACIENTE_DISPOSITIVOID',prop:'Visible'}]}");
         setEventMetadata("LBLCOACHPACIENTE_TOKENFILTER.CLICK","{handler:'E151H1',iparms:[{av:'divCoachpaciente_tokenfiltercontainer_Class',ctrl:'COACHPACIENTE_TOKENFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divCoachpaciente_tokenfiltercontainer_Class',ctrl:'COACHPACIENTE_TOKENFILTERCONTAINER',prop:'Class'},{av:'edtavCcoachpaciente_token_Visible',ctrl:'vCCOACHPACIENTE_TOKEN',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E191H2',iparms:[{av:'A12CoachPaciente_Coach',fld:'COACHPACIENTE_COACH',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'A13CoachPaciente_Paciente',fld:'COACHPACIENTE_PACIENTE',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV10pCoachPaciente_Coach',fld:'vPCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV11pCoachPaciente_Paciente',fld:'vPCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'}]}");
         setEventMetadata("GRID1_FIRSTPAGE","{handler:'subgrid1_firstpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cCoachPaciente_Coach',fld:'vCCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cCoachPaciente_Paciente',fld:'vCCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cCoachPaciente_FechaAsignacion',fld:'vCCOACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'AV9cCoachPaciente_DispositivoID',fld:'vCCOACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'AV13cCoachPaciente_Token',fld:'vCCOACHPACIENTE_TOKEN',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("GRID1_PREVPAGE","{handler:'subgrid1_previouspage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cCoachPaciente_Coach',fld:'vCCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cCoachPaciente_Paciente',fld:'vCCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cCoachPaciente_FechaAsignacion',fld:'vCCOACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'AV9cCoachPaciente_DispositivoID',fld:'vCCOACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'AV13cCoachPaciente_Token',fld:'vCCOACHPACIENTE_TOKEN',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("GRID1_NEXTPAGE","{handler:'subgrid1_nextpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cCoachPaciente_Coach',fld:'vCCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cCoachPaciente_Paciente',fld:'vCCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cCoachPaciente_FechaAsignacion',fld:'vCCOACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'AV9cCoachPaciente_DispositivoID',fld:'vCCOACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'AV13cCoachPaciente_Token',fld:'vCCOACHPACIENTE_TOKEN',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("GRID1_LASTPAGE","{handler:'subgrid1_lastpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cCoachPaciente_Coach',fld:'vCCOACHPACIENTE_COACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cCoachPaciente_Paciente',fld:'vCCOACHPACIENTE_PACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cCoachPaciente_FechaAsignacion',fld:'vCCOACHPACIENTE_FECHAASIGNACION',pic:'',nv:''},{av:'AV9cCoachPaciente_DispositivoID',fld:'vCCOACHPACIENTE_DISPOSITIVOID',pic:'',nv:''},{av:'AV13cCoachPaciente_Token',fld:'vCCOACHPACIENTE_TOKEN',pic:'',nv:''}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6cCoachPaciente_Coach = (Guid)(Guid.Empty);
         AV7cCoachPaciente_Paciente = (Guid)(Guid.Empty);
         AV8cCoachPaciente_FechaAsignacion = DateTime.MinValue;
         AV9cCoachPaciente_DispositivoID = "";
         AV13cCoachPaciente_Token = "";
         GXKey = "";
         AV10pCoachPaciente_Coach = (Guid)(Guid.Empty);
         AV11pCoachPaciente_Paciente = (Guid)(Guid.Empty);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblLblcoachpaciente_coachfilter_Jsonclick = "";
         TempTags = "";
         lblLblcoachpaciente_pacientefilter_Jsonclick = "";
         lblLblcoachpaciente_fechaasignacionfilter_Jsonclick = "";
         lblLblcoachpaciente_dispositivoidfilter_Jsonclick = "";
         lblLblcoachpaciente_tokenfilter_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtntoggle_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         AV5LinkSelection = "";
         A12CoachPaciente_Coach = (Guid)(Guid.Empty);
         A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         bttBtn_cancel_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16Linkselection_GXI = "";
         scmdbuf = "";
         lV9cCoachPaciente_DispositivoID = "";
         lV13cCoachPaciente_Token = "";
         A19CoachPaciente_DispositivoID = "";
         A20CoachPaciente_Token = "";
         H001H2_A20CoachPaciente_Token = new String[] {""} ;
         H001H2_n20CoachPaciente_Token = new bool[] {false} ;
         H001H2_A19CoachPaciente_DispositivoID = new String[] {""} ;
         H001H2_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         H001H2_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         H001H2_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         H001H2_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         H001H2_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         H001H3_AGRID1_nRecordCount = new long[1] ;
         AV12ADVANCED_LABEL_TEMPLATE = "";
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sImgUrl = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gx0020__default(),
            new Object[][] {
                new Object[] {
               H001H2_A20CoachPaciente_Token, H001H2_n20CoachPaciente_Token, H001H2_A19CoachPaciente_DispositivoID, H001H2_n19CoachPaciente_DispositivoID, H001H2_A18CoachPaciente_FechaAsignacion, H001H2_n18CoachPaciente_FechaAsignacion, H001H2_A13CoachPaciente_Paciente, H001H2_A12CoachPaciente_Coach
               }
               , new Object[] {
               H001H3_AGRID1_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_64 ;
      private short nGXsfl_64_idx=1 ;
      private short GRID1_nEOF ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int subGrid1_Rows ;
      private int edtavCcoachpaciente_coach_Visible ;
      private int edtavCcoachpaciente_coach_Enabled ;
      private int edtavCcoachpaciente_paciente_Visible ;
      private int edtavCcoachpaciente_paciente_Enabled ;
      private int edtavCcoachpaciente_fechaasignacion_Enabled ;
      private int edtavCcoachpaciente_dispositivoid_Visible ;
      private int edtavCcoachpaciente_dispositivoid_Enabled ;
      private int edtavCcoachpaciente_token_Visible ;
      private int edtavCcoachpaciente_token_Enabled ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int subGrid1_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nRecordCount ;
      private String divAdvancedcontainer_Class ;
      private String bttBtntoggle_Class ;
      private String divCoachpaciente_coachfiltercontainer_Class ;
      private String divCoachpaciente_pacientefiltercontainer_Class ;
      private String divCoachpaciente_fechaasignacionfiltercontainer_Class ;
      private String divCoachpaciente_dispositivoidfiltercontainer_Class ;
      private String divCoachpaciente_tokenfiltercontainer_Class ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_64_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMain_Internalname ;
      private String divAdvancedcontainer_Internalname ;
      private String divCoachpaciente_coachfiltercontainer_Internalname ;
      private String lblLblcoachpaciente_coachfilter_Internalname ;
      private String lblLblcoachpaciente_coachfilter_Jsonclick ;
      private String edtavCcoachpaciente_coach_Internalname ;
      private String TempTags ;
      private String edtavCcoachpaciente_coach_Jsonclick ;
      private String divCoachpaciente_pacientefiltercontainer_Internalname ;
      private String lblLblcoachpaciente_pacientefilter_Internalname ;
      private String lblLblcoachpaciente_pacientefilter_Jsonclick ;
      private String edtavCcoachpaciente_paciente_Internalname ;
      private String edtavCcoachpaciente_paciente_Jsonclick ;
      private String divCoachpaciente_fechaasignacionfiltercontainer_Internalname ;
      private String lblLblcoachpaciente_fechaasignacionfilter_Internalname ;
      private String lblLblcoachpaciente_fechaasignacionfilter_Jsonclick ;
      private String edtavCcoachpaciente_fechaasignacion_Internalname ;
      private String edtavCcoachpaciente_fechaasignacion_Jsonclick ;
      private String divCoachpaciente_dispositivoidfiltercontainer_Internalname ;
      private String lblLblcoachpaciente_dispositivoidfilter_Internalname ;
      private String lblLblcoachpaciente_dispositivoidfilter_Jsonclick ;
      private String edtavCcoachpaciente_dispositivoid_Internalname ;
      private String edtavCcoachpaciente_dispositivoid_Jsonclick ;
      private String divCoachpaciente_tokenfiltercontainer_Internalname ;
      private String lblLblcoachpaciente_tokenfilter_Internalname ;
      private String lblLblcoachpaciente_tokenfilter_Jsonclick ;
      private String edtavCcoachpaciente_token_Internalname ;
      private String edtavCcoachpaciente_token_Jsonclick ;
      private String divGridtable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtntoggle_Internalname ;
      private String bttBtntoggle_Jsonclick ;
      private String sStyleString ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String edtavLinkselection_Link ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLinkselection_Internalname ;
      private String edtCoachPaciente_Coach_Internalname ;
      private String edtCoachPaciente_Paciente_Internalname ;
      private String edtCoachPaciente_FechaAsignacion_Internalname ;
      private String scmdbuf ;
      private String AV12ADVANCED_LABEL_TEMPLATE ;
      private String sGXsfl_64_fel_idx="0001" ;
      private String sImgUrl ;
      private String ROClassString ;
      private String edtCoachPaciente_Coach_Jsonclick ;
      private String edtCoachPaciente_Paciente_Jsonclick ;
      private String edtCoachPaciente_FechaAsignacion_Jsonclick ;
      private DateTime AV8cCoachPaciente_FechaAsignacion ;
      private DateTime A18CoachPaciente_FechaAsignacion ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_64_Refreshing=false ;
      private bool n18CoachPaciente_FechaAsignacion ;
      private bool n20CoachPaciente_Token ;
      private bool n19CoachPaciente_DispositivoID ;
      private bool returnInSub ;
      private bool AV5LinkSelection_IsBlob ;
      private String AV9cCoachPaciente_DispositivoID ;
      private String AV13cCoachPaciente_Token ;
      private String AV16Linkselection_GXI ;
      private String lV9cCoachPaciente_DispositivoID ;
      private String lV13cCoachPaciente_Token ;
      private String A19CoachPaciente_DispositivoID ;
      private String A20CoachPaciente_Token ;
      private String AV5LinkSelection ;
      private Guid AV6cCoachPaciente_Coach ;
      private Guid AV7cCoachPaciente_Paciente ;
      private Guid AV10pCoachPaciente_Coach ;
      private Guid AV11pCoachPaciente_Paciente ;
      private Guid A12CoachPaciente_Coach ;
      private Guid A13CoachPaciente_Paciente ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] H001H2_A20CoachPaciente_Token ;
      private bool[] H001H2_n20CoachPaciente_Token ;
      private String[] H001H2_A19CoachPaciente_DispositivoID ;
      private bool[] H001H2_n19CoachPaciente_DispositivoID ;
      private DateTime[] H001H2_A18CoachPaciente_FechaAsignacion ;
      private bool[] H001H2_n18CoachPaciente_FechaAsignacion ;
      private Guid[] H001H2_A13CoachPaciente_Paciente ;
      private Guid[] H001H2_A12CoachPaciente_Coach ;
      private long[] H001H3_AGRID1_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private Guid aP0_pCoachPaciente_Coach ;
      private Guid aP1_pCoachPaciente_Paciente ;
      private GXWebForm Form ;
   }

   public class gx0020__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H001H2( IGxContext context ,
                                             DateTime AV8cCoachPaciente_FechaAsignacion ,
                                             String AV9cCoachPaciente_DispositivoID ,
                                             String AV13cCoachPaciente_Token ,
                                             DateTime A18CoachPaciente_FechaAsignacion ,
                                             String A19CoachPaciente_DispositivoID ,
                                             String A20CoachPaciente_Token ,
                                             Guid AV6cCoachPaciente_Coach ,
                                             Guid AV7cCoachPaciente_Paciente )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [8] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [CoachPaciente_Token], [CoachPaciente_DispositivoID], [CoachPaciente_FechaAsignacion], [CoachPaciente_Paciente], [CoachPaciente_Coach]";
         sFromString = " FROM [CoachPaciente] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([CoachPaciente_Coach] >= @AV6cCoachPaciente_Coach and [CoachPaciente_Paciente] >= @AV7cCoachPaciente_Paciente)";
         if ( ! (DateTime.MinValue==AV8cCoachPaciente_FechaAsignacion) )
         {
            sWhereString = sWhereString + " and ([CoachPaciente_FechaAsignacion] >= @AV8cCoachPaciente_FechaAsignacion)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9cCoachPaciente_DispositivoID)) )
         {
            sWhereString = sWhereString + " and ([CoachPaciente_DispositivoID] like @lV9cCoachPaciente_DispositivoID)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13cCoachPaciente_Token)) )
         {
            sWhereString = sWhereString + " and ([CoachPaciente_Token] like @lV13cCoachPaciente_Token)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         sOrderString = sOrderString + " ORDER BY [CoachPaciente_Coach], [CoachPaciente_Paciente]";
         scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + "" + sOrderString + " OFFSET " + "@GXPagingFrom2" + " ROWS FETCH NEXT CAST((SELECT CASE WHEN " + "@GXPagingTo2" + " > 0 THEN " + "@GXPagingTo2" + " ELSE 1e9 END) AS INTEGER) ROWS ONLY";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H001H3( IGxContext context ,
                                             DateTime AV8cCoachPaciente_FechaAsignacion ,
                                             String AV9cCoachPaciente_DispositivoID ,
                                             String AV13cCoachPaciente_Token ,
                                             DateTime A18CoachPaciente_FechaAsignacion ,
                                             String A19CoachPaciente_DispositivoID ,
                                             String A20CoachPaciente_Token ,
                                             Guid AV6cCoachPaciente_Coach ,
                                             Guid AV7cCoachPaciente_Paciente )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [5] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [CoachPaciente] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([CoachPaciente_Coach] >= @AV6cCoachPaciente_Coach and [CoachPaciente_Paciente] >= @AV7cCoachPaciente_Paciente)";
         if ( ! (DateTime.MinValue==AV8cCoachPaciente_FechaAsignacion) )
         {
            sWhereString = sWhereString + " and ([CoachPaciente_FechaAsignacion] >= @AV8cCoachPaciente_FechaAsignacion)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV9cCoachPaciente_DispositivoID)) )
         {
            sWhereString = sWhereString + " and ([CoachPaciente_DispositivoID] like @lV9cCoachPaciente_DispositivoID)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13cCoachPaciente_Token)) )
         {
            sWhereString = sWhereString + " and ([CoachPaciente_Token] like @lV13cCoachPaciente_Token)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H001H2(context, (DateTime)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (Guid)dynConstraints[6] , (Guid)dynConstraints[7] );
               case 1 :
                     return conditional_H001H3(context, (DateTime)dynConstraints[0] , (String)dynConstraints[1] , (String)dynConstraints[2] , (DateTime)dynConstraints[3] , (String)dynConstraints[4] , (String)dynConstraints[5] , (Guid)dynConstraints[6] , (Guid)dynConstraints[7] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001H2 ;
          prmH001H2 = new Object[] {
          new Object[] {"@AV6cCoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV7cCoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV8cCoachPaciente_FechaAsignacion",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV9cCoachPaciente_DispositivoID",SqlDbType.VarChar,75,0} ,
          new Object[] {"@lV13cCoachPaciente_Token",SqlDbType.VarChar,40,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH001H3 ;
          prmH001H3 = new Object[] {
          new Object[] {"@AV6cCoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV7cCoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV8cCoachPaciente_FechaAsignacion",SqlDbType.DateTime,8,0} ,
          new Object[] {"@lV9cCoachPaciente_DispositivoID",SqlDbType.VarChar,75,0} ,
          new Object[] {"@lV13cCoachPaciente_Token",SqlDbType.VarChar,40,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H001H2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001H2,11,0,false,false )
             ,new CursorDef("H001H3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001H3,1,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((DateTime[]) buf[4])[0] = rslt.getGXDate(3) ;
                ((bool[]) buf[5])[0] = rslt.wasNull(3);
                ((Guid[]) buf[6])[0] = rslt.getGuid(4) ;
                ((Guid[]) buf[7])[0] = rslt.getGuid(5) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[8]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[9]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[10]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[11]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[12]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[13]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[14]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[15]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[5]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[6]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[7]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[8]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[9]);
                }
                return;
       }
    }

 }

}
