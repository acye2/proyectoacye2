using System;
using GeneXus.Builder;
using System.IO;
public class bldDevelopermenu : GxBaseBuilder
{
   string cs_path = "." ;
   public bldDevelopermenu( ) : base()
   {
   }

   public override int BeforeCompile( )
   {
      return 0 ;
   }

   public override int AfterCompile( )
   {
      int ErrCode ;
      ErrCode = 0;
      if ( ! File.Exists(@"bin\client.exe.config") || checkTime(@"bin\client.exe.config",cs_path + @"\client.exe.config") )
      {
         File.Copy( cs_path + @"\client.exe.config", @"bin\client.exe.config", true);
      }
      return ErrCode ;
   }

   static public int Main( string[] args )
   {
      bldDevelopermenu x = new bldDevelopermenu() ;
      x.SetMainSourceFile( "bldDevelopermenu.cs");
      x.LoadVariables( args);
      return x.CompileAll( );
   }

   public override ItemCollection GetSortedBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\GeneXus.Programs.Common.dll", cs_path + @"\genexus.programs.common.rsp");
      return sc ;
   }

   public override TargetCollection GetRuntimeBuildList( )
   {
      TargetCollection sc = new TargetCollection() ;
      sc.Add( @"aexportwwpersona", "dll");
      sc.Add( @"areportwwpersona", "dll");
      sc.Add( @"awsregistrarecorrido", "dll");
      sc.Add( @"ws_registrarecorrido", "dll");
      sc.Add( @"aregistrarrecorrido", "dll");
      sc.Add( @"appmasterpage", "dll");
      sc.Add( @"recentlinks", "dll");
      sc.Add( @"promptmasterpage", "dll");
      sc.Add( @"rwdmasterpage", "dll");
      sc.Add( @"rwdrecentlinks", "dll");
      sc.Add( @"rwdpromptmasterpage", "dll");
      sc.Add( @"gx0010", "dll");
      sc.Add( @"webpanelbuilderlayout", "dll");
      sc.Add( @"k2blogout", "dll");
      sc.Add( @"k2bchangepassword", "dll");
      sc.Add( @"k2bnotauthorized", "dll");
      sc.Add( @"k2bpromptmasterpage", "dll");
      sc.Add( @"k2bheadermodern", "dll");
      sc.Add( @"k2brecentlinksmodern", "dll");
      sc.Add( @"k2bwwmasterpagemodern", "dll");
      sc.Add( @"k2bwwmasterpagemoderndesigner", "dll");
      sc.Add( @"k2bwwmasterpageflat", "dll");
      sc.Add( @"k2bwwmasterpageflatcompact", "dll");
      sc.Add( @"k2btoolscomponentplaceholder", "dll");
      sc.Add( @"k2btabbedviewforlayoutmodern", "dll");
      sc.Add( @"k2btabbedviewforlayoutflat", "dll");
      sc.Add( @"k2btoolstabstitlecomponent", "dll");
      sc.Add( @"k2bmenu", "dll");
      sc.Add( @"k2bselectcolumns", "dll");
      sc.Add( @"promptlayoutmodern", "dll");
      sc.Add( @"k2bmenumodern", "dll");
      sc.Add( @"k2bheadermodernsearch", "dll");
      sc.Add( @"k2btoolssearchresult", "dll");
      sc.Add( @"k2btabbedviewforlayoutmodernsearch", "dll");
      sc.Add( @"k2btoolssearchresultentitywc", "dll");
      sc.Add( @"k2btoolsindexadmin", "dll");
      sc.Add( @"k2btoolssearchresultflat", "dll");
      sc.Add( @"k2btabbedviewforlayoutflatsearch", "dll");
      sc.Add( @"k2btoolssearchresultentitywcflat", "dll");
      sc.Add( @"entitymanagerpersona", "dll");
      sc.Add( @"wwpersona", "dll");
      sc.Add( @"personageneral", "dll");
      sc.Add( @"principal", "dll");
      sc.Add( @"gx0020", "dll");
      sc.Add( @"gx0030", "dll");
      sc.Add( @"gx0051", "dll");
      sc.Add( @"gamlogout", "dll");
      sc.Add( @"gamexamplelogin", "dll");
      sc.Add( @"gamexampleuserentry", "dll");
      sc.Add( @"gammasterpage", "dll");
      sc.Add( @"gamexamplechangeyourpassword", "dll");
      sc.Add( @"gamhome", "dll");
      sc.Add( @"gamexamplewwroles", "dll");
      sc.Add( @"gamexampleroleentry", "dll");
      sc.Add( @"gamexamplewwusers", "dll");
      sc.Add( @"gamexamplewwsecuritypolicies", "dll");
      sc.Add( @"gamexamplesecuritypolicyentry", "dll");
      sc.Add( @"gamexamplewwrolepermissions", "dll");
      sc.Add( @"gamexamplewwroleroles", "dll");
      sc.Add( @"gamexamplewwuserroles", "dll");
      sc.Add( @"gamexamplesetpassword", "dll");
      sc.Add( @"gamexamplewwrepositories", "dll");
      sc.Add( @"gamexamplerepositoryentry", "dll");
      sc.Add( @"gamrepositoryconfiguration", "dll");
      sc.Add( @"gamexamplewwauthtypes", "dll");
      sc.Add( @"gamexampleauthenticationtypeentry", "dll");
      sc.Add( @"gamexampletestexternallogin", "dll");
      sc.Add( @"gamexamplewwconnections", "dll");
      sc.Add( @"gamexampleconnectionentry", "dll");
      sc.Add( @"gamexamplechangepassword", "dll");
      sc.Add( @"gamexampleupdateregisteruser", "dll");
      sc.Add( @"gamexamplerecoverpasswordstep1", "dll");
      sc.Add( @"gamexampleregisteruser", "dll");
      sc.Add( @"gamheader", "dll");
      sc.Add( @"gamexamplenotauthorized", "dll");
      sc.Add( @"gamexamplerecoverpasswordstep2", "dll");
      sc.Add( @"gamexamplewwapplications", "dll");
      sc.Add( @"gamexampleapplicationentry", "dll");
      sc.Add( @"gamexampleuserroleselect", "dll");
      sc.Add( @"gamexamplewwapppermissions", "dll");
      sc.Add( @"gamexamplewwappmenus", "dll");
      sc.Add( @"gamexamplerolepermissionselect", "dll");
      sc.Add( @"gamexampleapppermissionentry", "dll");
      sc.Add( @"gamexampleapppermissionchildren", "dll");
      sc.Add( @"gamexampleapppermissionselect", "dll");
      sc.Add( @"gamexamplechangerepository", "dll");
      sc.Add( @"gamexampleroleselect", "dll");
      sc.Add( @"gamremotelogin", "dll");
      sc.Add( @"gamssologin", "dll");
      sc.Add( @"gamexamplewweventsubscriptions", "dll");
      sc.Add( @"gamexampleeventsubscriptionentry", "dll");
      sc.Add( @"gamexampleappmenuentry", "dll");
      sc.Add( @"gamexamplewwappmenuoptions", "dll");
      sc.Add( @"gamexampleappmenuoptionentry", "dll");
      sc.Add( @"gammenu", "dll");
      sc.Add( @"gx0060", "dll");
      sc.Add( @"gx0070", "dll");
      sc.Add( @"webpaneldesignerlayoutmodern", "dll");
      sc.Add( @"webpaneldesignerlayoutflat", "dll");
      sc.Add( @"pacienteagregar", "dll");
      sc.Add( @"wprecorridos", "dll");
      sc.Add( @"wcmapa", "dll");
      sc.Add( @"persona", "dll");
      sc.Add( @"coachpaciente", "dll");
      sc.Add( @"recorrido", "dll");
      sc.Add( @"obstaculo", "dll");
      sc.Add( @"emergenciabitacora", "dll");
      return sc ;
   }

   public override ItemCollection GetResBuildList( )
   {
      ItemCollection sc = new ItemCollection() ;
      sc.Add( @"bin\messages.spa.dll", cs_path + @"\messages.spa.txt");
      return sc ;
   }

   public override bool ToBuild( String obj )
   {
      if (checkTime(obj, cs_path + @"\bin\GxClasses.dll" ))
         return true;
      if ( obj == @"bin\GeneXus.Programs.Common.dll" )
      {
         if (checkTime(obj, cs_path + @"\GxObjectCollection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\SoapParm.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxWebStd.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxFullTextSearchReindexer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GxModelInfoProvider.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\genexus.programs.sdt.rsp" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtObstaculo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtCoachPaciente.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRecorrido.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtRecorrido_Detalle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtPersona.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtEmergenciaBitacora.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtK2BToolsGXPreferences.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMDescription.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMProperty.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMError.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeSimple.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMCountry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMCountryLanguages.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepository.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUpdateRepositoryConfiguration.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUpdateRepositoryConfigurationApplicationsToImport.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMLoginAdditionalParameters.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSecurityPolicy.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserAttribute.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserAttributeMultiValues.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRole.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRoleFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplication.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationEnvironment.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationDelegateAuthorization.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationPermissionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationPermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationToken.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationTokenElement.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationMenuFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationMenu.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationMenuOptionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationMenuOption.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMMenuAdditionalParameters.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMMenuOptionList.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMPermissionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMPermission.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserRepositoryFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUserRepository.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLogFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLog.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLogLoginRetry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionFullLog.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSecurityPolicyFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnection.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionAddressList.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMApplicationFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSession.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionLoginRetry.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionRole.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMEventSubscriptionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMEventSubscription.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMUsersCountFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMSessionsCountFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationFacebook.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeFacebook.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeLocal.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeWebService.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationWebService.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationWebServiceServer.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTwitter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeTwitter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAM.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryCreate.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMImportRepositoryConfiguration.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMRepositoryConnectionFileFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionInfo.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMConnectionInfoProperties.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMCleanSessionLogFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationGoogle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeGoogle.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationCustom.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeCustom.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationGAMRemote.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\type_SdtGAMAuthenticationTypeGAMRemote.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAuthenticationTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMBrowser.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMErrorMessages.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMGenerateAuditory.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMGenerateSessionStatistics.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMIdentificatorKey.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMMenuOptionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionAccessType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRecoveryPasswordTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRememberUserTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryConnectionTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAPiMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryUserIdentifications.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMApplicationAuthorizarionRequestType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMApplicationType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtOAuthVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtOpenIdVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAutExtWebServiceVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMExternalAuthentication.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMVersion.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserListOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAllowMultipleConcurrentSessions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionAccessTypeDefault.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMPermissionTypeFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMBooleanFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMExternalAuthorizationVersions.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionLogListOrder.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMTracing.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMInternalGUIDs.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAPIModes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRemoteLogoutBehaviors.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMEvents.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMEventSubscriptionStatus.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMSessionLogType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMOptionFilter.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINActionTexts.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEmergenciaEstadoDomain.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BAfterTrnNavigation.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BBtnToolTip.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BComponentType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BPage.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BSesItem.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BStandardActivityType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BSymbols.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BThemeClasses.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BTrnMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BTrnReturnMode.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BHttpMethod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINK2BFilterType.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoSenalDomain.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINTipoObstaculoDomain.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINIncidenteDomain.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINEmergenciaDomain.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserActivationMethod.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMUserGender.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMRepositoryRememberUserTypes.cs" ))
            return true;
         if (checkTime(obj, cs_path + @"\GXDOMAINGAMAuthenticationFunctions.cs" ))
            return true;
      }
      if ( obj == @"bin\messages.spa.dll" )
      {
         if (checkTime(obj, cs_path + @"\messages.spa.txt" ))
            return true;
      }
      return false ;
   }

}

