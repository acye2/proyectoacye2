/*
               File: K2BStackPop
        Description: Pop
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:35.38
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bstackpop : GXProcedure
   {
      public k2bstackpop( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bstackpop( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack )
      {
         this.AV8Stack = aP0_Stack;
         initialize();
         executePrivate();
      }

      public void executeSubmit( GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack )
      {
         k2bstackpop objk2bstackpop;
         objk2bstackpop = new k2bstackpop();
         objk2bstackpop.AV8Stack = aP0_Stack;
         objk2bstackpop.context.SetSubmitInitialConfig(context);
         objk2bstackpop.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bstackpop);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bstackpop)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV8Stack.Count > 0 )
         {
            AV8Stack.RemoveItem(AV8Stack.Count);
         }
         new k2bsetstack(context ).execute(  AV8Stack) ;
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private GXBaseCollection<SdtK2BStack_K2BStackItem> AV8Stack ;
   }

}
