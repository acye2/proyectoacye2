/*
               File: K2BStackManagement
        Description: Manage Stack
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:35.64
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bstackmanagement : GXProcedure
   {
      public k2bstackmanagement( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bstackmanagement( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_FormCaption ,
                           bool aP1_includeInStack ,
                           out GXBaseCollection<SdtK2BStack_K2BStackItem> aP2_Stack )
      {
         this.AV9FormCaption = aP0_FormCaption;
         this.AV13includeInStack = aP1_includeInStack;
         this.AV10Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2") ;
         initialize();
         executePrivate();
         aP2_Stack=this.AV10Stack;
      }

      public GXBaseCollection<SdtK2BStack_K2BStackItem> executeUdp( String aP0_FormCaption ,
                                                                    bool aP1_includeInStack )
      {
         this.AV9FormCaption = aP0_FormCaption;
         this.AV13includeInStack = aP1_includeInStack;
         this.AV10Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2") ;
         initialize();
         executePrivate();
         aP2_Stack=this.AV10Stack;
         return AV10Stack ;
      }

      public void executeSubmit( String aP0_FormCaption ,
                                 bool aP1_includeInStack ,
                                 out GXBaseCollection<SdtK2BStack_K2BStackItem> aP2_Stack )
      {
         k2bstackmanagement objk2bstackmanagement;
         objk2bstackmanagement = new k2bstackmanagement();
         objk2bstackmanagement.AV9FormCaption = aP0_FormCaption;
         objk2bstackmanagement.AV13includeInStack = aP1_includeInStack;
         objk2bstackmanagement.AV10Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2") ;
         objk2bstackmanagement.context.SetSubmitInitialConfig(context);
         objk2bstackmanagement.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bstackmanagement);
         aP2_Stack=this.AV10Stack;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bstackmanagement)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new k2bgetstack(context ).execute( out  AV10Stack) ;
         if ( StringUtil.StrCmp(AV12Request.Method, "GET") == 0 )
         {
            if ( AV13includeInStack )
            {
               AV14StackCount = AV10Stack.Count;
               if ( AV14StackCount > 0 )
               {
                  GXt_char1 = AV11lastPgmName;
                  new k2burlgetprogramname(context ).execute(  ((SdtK2BStack_K2BStackItem)AV10Stack.Item(AV14StackCount)).gxTpr_Url, out  GXt_char1) ;
                  AV11lastPgmName = GXt_char1;
                  if ( StringUtil.StrCmp(StringUtil.Upper( AV11lastPgmName), StringUtil.Trim( StringUtil.Upper( AV12Request.ScriptName))) == 0 )
                  {
                     GXt_char1 = "";
                     new k2blinkto(context ).execute(  AV12Request, out  GXt_char1) ;
                     ((SdtK2BStack_K2BStackItem)AV10Stack.Item(AV14StackCount)).gxTpr_Url = GXt_char1;
                     GXt_char1 = "";
                     new k2bfixrecentlinkscaption(context ).execute(  AV9FormCaption, out  GXt_char1) ;
                     ((SdtK2BStack_K2BStackItem)AV10Stack.Item(AV14StackCount)).gxTpr_Caption = GXt_char1;
                     new k2bsetstack(context ).execute(  AV10Stack) ;
                  }
                  else
                  {
                     new k2bstackpush(context ).execute(  AV10Stack,  AV9FormCaption) ;
                  }
               }
               else
               {
                  new k2bstackpush(context ).execute(  AV10Stack,  AV9FormCaption) ;
               }
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12Request = new GxHttpRequest( context);
         AV11lastPgmName = "";
         GXt_char1 = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV14StackCount ;
      private String GXt_char1 ;
      private bool AV13includeInStack ;
      private String AV9FormCaption ;
      private String AV11lastPgmName ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> aP2_Stack ;
      private GxHttpRequest AV12Request ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> AV10Stack ;
   }

}
