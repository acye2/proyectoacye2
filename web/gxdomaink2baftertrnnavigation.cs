/*
               File: K2BAfterTrnNavigation
        Description: K2BAfterTrnNavigation
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:17.51
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using GeneXus.Reorg;
using System.Threading;
using GeneXus.Programs;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
namespace GeneXus.Programs {
   public class gxdomaink2baftertrnnavigation
   {
      private static Hashtable domain = new Hashtable();
      private static Hashtable domainMap;
      static gxdomaink2baftertrnnavigation ()
      {
         domain[(short)1] = "Return To Caller";
         domain[(short)2] = "Entity Manager";
         domain[(short)3] = "Dynamic Link";
         domain[(short)4] = "Deprecated";
         domain[(short)5] = "None";
      }

      public static string getDescription( IGxContext context ,
                                           short key )
      {
         return (string)domain[key] ;
      }

      public static GxSimpleCollection<short> getValues( )
      {
         GxSimpleCollection<short> value = new GxSimpleCollection<short>();
         ArrayList aKeys = new ArrayList(domain.Keys);
         aKeys.Sort();
         foreach (short key in aKeys)
         {
            value.Add(key);
         }
         return value;
      }

      [System.Runtime.CompilerServices.MethodImpl(System.Runtime.CompilerServices.MethodImplOptions.Synchronized)]
      public static short getValue( string key )
      {
         if(domainMap == null)
         {
            domainMap = new Hashtable();
            domainMap["ReturnToCaller"] = (short)1;
            domainMap["EntityManager"] = (short)2;
            domainMap["DynamicLink"] = (short)3;
            domainMap["EntityManger"] = (short)4;
            domainMap["None"] = (short)5;
         }
         return (short)domainMap[key] ;
      }

   }

}
