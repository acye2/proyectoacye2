/*
				   File: type_SdtK2BStack_K2BStackItem
			Description: K2BStack
				 Author: Nemo for C# version 15.0.9.121631
		   Generated on: 17/11/2018 15:05:16
		   Program type: Callable routine
			  Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;

namespace GeneXus.Programs{
	[XmlSerializerFormat]
	[XmlRoot(ElementName="K2BStackItem")]
	[XmlType(TypeName="K2BStackItem" , Namespace="PACYE2" )]
	[Serializable]
	public class SdtK2BStack_K2BStackItem : GxUserType
	{
		public SdtK2BStack_K2BStackItem( )
		{
			/* Constructor for serialization */
			gxTv_SdtK2BStack_K2BStackItem_Caption = "";

			gxTv_SdtK2BStack_K2BStackItem_Url = "";

		}

		public SdtK2BStack_K2BStackItem(IGxContext context)
		{
			this.context = context;
			initialize();
		}

		#region Json
		private static Hashtable mapper;
		public override String JsonMap(String value)
		{
			if (mapper == null)
			{
				mapper = new Hashtable();
			}
			return (String)mapper[value]; ;
		}

		public override void ToJSON()
		{
			ToJSON(true) ;
			return;
		}

		public override void ToJSON(bool includeState)
		{
			AddObjectProperty("Caption", gxTpr_Caption, false);
			AddObjectProperty("Url", gxTpr_Url, false);
			return;
		}
		#endregion

		#region Properties

		[SoapElement(ElementName="Caption")]
		[XmlElement(ElementName="Caption")]
		public String gxTpr_Caption
		{
			get { 
				return gxTv_SdtK2BStack_K2BStackItem_Caption; 
			}
			set { 
				gxTv_SdtK2BStack_K2BStackItem_Caption = value;
				SetDirty("Caption");
			}
		}


		[SoapElement(ElementName="Url")]
		[XmlElement(ElementName="Url")]
		public String gxTpr_Url
		{
			get { 
				return gxTv_SdtK2BStack_K2BStackItem_Url; 
			}
			set { 
				gxTv_SdtK2BStack_K2BStackItem_Url = value;
				SetDirty("Url");
			}
		}


		#endregion

		#region Initialization

		public void initialize( )
		{
			gxTv_SdtK2BStack_K2BStackItem_Caption = "";
			gxTv_SdtK2BStack_K2BStackItem_Url = "";
			return  ;
		}



		#endregion

		#region Declaration

		protected String gxTv_SdtK2BStack_K2BStackItem_Caption;
		protected String gxTv_SdtK2BStack_K2BStackItem_Url;



		#endregion
	}
	#region Rest interface
	[DataContract(Name=@"K2BStackItem", Namespace="PACYE2")]
	public class SdtK2BStack_K2BStackItem_RESTInterface : GxGenericCollectionItem<SdtK2BStack_K2BStackItem>, System.Web.SessionState.IRequiresSessionState
	{
		public SdtK2BStack_K2BStackItem_RESTInterface( ) : base()
		{
		}

		public SdtK2BStack_K2BStackItem_RESTInterface( SdtK2BStack_K2BStackItem psdt ) : base(psdt)
		{
		}

		#region Rest Properties
		[DataMember(Name="Caption", Order=0)]
		public String gxTpr_Caption
		{
			get { 
				return sdt.gxTpr_Caption;
			}
			set { 
				sdt.gxTpr_Caption = value;
			}
		}

		[DataMember(Name="Url", Order=1)]
		public String gxTpr_Url
		{
			get { 
				return sdt.gxTpr_Url;
			}
			set { 
				sdt.gxTpr_Url = value;
			}
		}


		#endregion

		public SdtK2BStack_K2BStackItem sdt
		{
			get { 
				return (SdtK2BStack_K2BStackItem)Sdt;
			}
			set { 
				Sdt = value;
			}
		}

		[OnDeserializing]
		void checkSdt( StreamingContext ctx )
		{
			if ( sdt == null )
			{
				sdt = new SdtK2BStack_K2BStackItem() ;
			}
		}
	}
	#endregion
}