/*
               File: K2BWWMasterPageModern
        Description: Application master page
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:46.72
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bwwmasterpagemodern : GXMasterPage, System.Web.SessionState.IRequiresSessionState
   {
      public k2bwwmasterpagemodern( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public k2bwwmasterpagemodern( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            PA0F2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS0F2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE0F2( ) ;
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlHeaders();
         }
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlOpenForm();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "FORM_MPAGE_Caption", StringUtil.RTrim( (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption));
         GxWebStd.gx_hidden_field( context, "FORM_MPAGE_Caption", StringUtil.RTrim( (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption));
      }

      protected void RenderHtmlCloseForm0F2( )
      {
         SendCloseFormHiddens( ) ;
         SendSecurityToken((String)(sPrefix));
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlCloseForm();
         }
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( ! ( WebComp_Wcheader == null ) )
         {
            WebComp_Wcheader.componentjscripts();
         }
         if ( ! ( WebComp_Wcleftside == null ) )
         {
            WebComp_Wcleftside.componentjscripts();
         }
         if ( ! ( WebComp_Recentlinkscomponent == null ) )
         {
            WebComp_Recentlinkscomponent.componentjscripts();
         }
         if ( ! ( WebComp_Wcrightside == null ) )
         {
            WebComp_Wcrightside.componentjscripts();
         }
         if ( ! ( WebComp_Wcfooter == null ) )
         {
            WebComp_Wcfooter.componentjscripts();
         }
         context.AddJavascriptSource("k2bwwmasterpagemodern.js", "?201811171524674", false);
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "K2BWWMasterPageModern" ;
      }

      public override String GetPgmdesc( )
      {
         return "Application master page" ;
      }

      protected void WB0F0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            if ( ! ShowMPWhenPopUp( ) && context.isPopUpObject( ) )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableOutput();
               }
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               /* Content placeholder */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gx-content-placeholder");
               context.WriteHtmlText( ">") ;
               if ( ! isFullAjaxMode( ) )
               {
                  getDataAreaObject().RenderHtmlContent();
               }
               context.WriteHtmlText( "</div>") ;
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
               wbLoad = true;
               return  ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, divAllcontainer_Internalname, 1, 0, "px", 100, "%", "Section_PageBackground", "left", "top", "", "", "div");
            wb_table1_3_0F2( true) ;
         }
         else
         {
            wb_table1_3_0F2( false) ;
         }
         return  ;
      }

      protected void wb_table1_3_0F2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START0F2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0F0( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( getDataAreaObject().ExecuteStartEvent() != 0 )
            {
               setAjaxCallMode();
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      protected void WS0F2( )
      {
         START0F2( ) ;
         EVT0F2( ) ;
      }

      protected void EVT0F2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E110F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Refresh */
                           E120F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Load */
                           E130F2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                              }
                              dynload_actions( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  else if ( StringUtil.StrCmp(sEvtType, "M") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-2));
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-6));
                     nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                     if ( nCmpId == 6 )
                     {
                        OldWcheader = cgiGet( "MPW0006");
                        if ( ( StringUtil.Len( OldWcheader) == 0 ) || ( StringUtil.StrCmp(OldWcheader, WebComp_Wcheader_Component) != 0 ) )
                        {
                           WebComp_Wcheader = getWebComponent(GetType(), "GeneXus.Programs", OldWcheader, new Object[] {context} );
                           WebComp_Wcheader.ComponentInit();
                           WebComp_Wcheader.Name = "OldWcheader";
                           WebComp_Wcheader_Component = OldWcheader;
                        }
                        if ( StringUtil.Len( WebComp_Wcheader_Component) != 0 )
                        {
                           WebComp_Wcheader.componentprocess("MPW0006", "", sEvt);
                        }
                        WebComp_Wcheader_Component = OldWcheader;
                     }
                     else if ( nCmpId == 9 )
                     {
                        OldWcleftside = cgiGet( "MPW0009");
                        if ( ( StringUtil.Len( OldWcleftside) == 0 ) || ( StringUtil.StrCmp(OldWcleftside, WebComp_Wcleftside_Component) != 0 ) )
                        {
                           WebComp_Wcleftside = getWebComponent(GetType(), "GeneXus.Programs", OldWcleftside, new Object[] {context} );
                           WebComp_Wcleftside.ComponentInit();
                           WebComp_Wcleftside.Name = "OldWcleftside";
                           WebComp_Wcleftside_Component = OldWcleftside;
                        }
                        if ( StringUtil.Len( WebComp_Wcleftside_Component) != 0 )
                        {
                           WebComp_Wcleftside.componentprocess("MPW0009", "", sEvt);
                        }
                        WebComp_Wcleftside_Component = OldWcleftside;
                     }
                     else if ( nCmpId == 12 )
                     {
                        OldRecentlinkscomponent = cgiGet( "MPW0012");
                        if ( ( StringUtil.Len( OldRecentlinkscomponent) == 0 ) || ( StringUtil.StrCmp(OldRecentlinkscomponent, WebComp_Recentlinkscomponent_Component) != 0 ) )
                        {
                           WebComp_Recentlinkscomponent = getWebComponent(GetType(), "GeneXus.Programs", OldRecentlinkscomponent, new Object[] {context} );
                           WebComp_Recentlinkscomponent.ComponentInit();
                           WebComp_Recentlinkscomponent.Name = "OldRecentlinkscomponent";
                           WebComp_Recentlinkscomponent_Component = OldRecentlinkscomponent;
                        }
                        if ( StringUtil.Len( WebComp_Recentlinkscomponent_Component) != 0 )
                        {
                           WebComp_Recentlinkscomponent.componentprocess("MPW0012", "", sEvt);
                        }
                        WebComp_Recentlinkscomponent_Component = OldRecentlinkscomponent;
                     }
                     else if ( nCmpId == 14 )
                     {
                        OldWcrightside = cgiGet( "MPW0014");
                        if ( ( StringUtil.Len( OldWcrightside) == 0 ) || ( StringUtil.StrCmp(OldWcrightside, WebComp_Wcrightside_Component) != 0 ) )
                        {
                           WebComp_Wcrightside = getWebComponent(GetType(), "GeneXus.Programs", OldWcrightside, new Object[] {context} );
                           WebComp_Wcrightside.ComponentInit();
                           WebComp_Wcrightside.Name = "OldWcrightside";
                           WebComp_Wcrightside_Component = OldWcrightside;
                        }
                        WebComp_Wcrightside.componentprocess("MPW0014", "", sEvt);
                        WebComp_Wcrightside_Component = OldWcrightside;
                     }
                     else if ( nCmpId == 24 )
                     {
                        OldWcfooter = cgiGet( "MPW0024");
                        if ( ( StringUtil.Len( OldWcfooter) == 0 ) || ( StringUtil.StrCmp(OldWcfooter, WebComp_Wcfooter_Component) != 0 ) )
                        {
                           WebComp_Wcfooter = getWebComponent(GetType(), "GeneXus.Programs", OldWcfooter, new Object[] {context} );
                           WebComp_Wcfooter.ComponentInit();
                           WebComp_Wcfooter.Name = "OldWcfooter";
                           WebComp_Wcfooter_Component = OldWcfooter;
                        }
                        WebComp_Wcfooter.componentprocess("MPW0024", "", sEvt);
                        WebComp_Wcfooter_Component = OldWcfooter;
                     }
                  }
                  if ( context.wbHandled == 0 )
                  {
                     getDataAreaObject().DispatchEvents();
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE0F2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0F2( ) ;
            }
         }
      }

      protected void PA0F2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0F2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0F2( )
      {
         initialize_formulas( ) ;
         if ( ShowMPWhenPopUp( ) || ! context.isPopUpObject( ) )
         {
            /* Execute user event: Refresh */
            E120F2 ();
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
            {
               if ( 1 != 0 )
               {
                  if ( StringUtil.Len( WebComp_Wcheader_Component) != 0 )
                  {
                     WebComp_Wcheader.componentstart();
                  }
               }
            }
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
            {
               if ( 1 != 0 )
               {
                  if ( StringUtil.Len( WebComp_Wcleftside_Component) != 0 )
                  {
                     WebComp_Wcleftside.componentstart();
                  }
               }
            }
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
            {
               if ( 1 != 0 )
               {
                  if ( StringUtil.Len( WebComp_Recentlinkscomponent_Component) != 0 )
                  {
                     WebComp_Recentlinkscomponent.componentstart();
                  }
               }
            }
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
            {
               if ( 1 != 0 )
               {
                  WebComp_Wcrightside.componentstart();
               }
            }
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
            {
               if ( 1 != 0 )
               {
                  WebComp_Wcfooter.componentstart();
               }
            }
            fix_multi_value_controls( ) ;
         }
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E130F2 ();
            WB0F0( ) ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
      }

      protected void send_integrity_lvl_hashes0F2( )
      {
      }

      protected void STRUP0F0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E110F2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption = cgiGet( "FORM_MPAGE_Caption");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E110F2 ();
         if (returnInSub) return;
      }

      protected void E110F2( )
      {
         /* Start Routine */
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Add("jquery.js") ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Add("K2BTools.js") ;
      }

      protected void E120F2( )
      {
         /* Refresh Routine */
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcheader_Component), StringUtil.Lower( "K2BHeaderModernSearch")) != 0 )
         {
            WebComp_Wcheader = getWebComponent(GetType(), "GeneXus.Programs", "k2bheadermodernsearch", new Object[] {context} );
            WebComp_Wcheader.ComponentInit();
            WebComp_Wcheader.Name = "K2BHeaderModernSearch";
            WebComp_Wcheader_Component = "K2BHeaderModernSearch";
         }
         if ( StringUtil.Len( WebComp_Wcheader_Component) != 0 )
         {
            WebComp_Wcheader.setjustcreated();
            WebComp_Wcheader.componentprepare(new Object[] {(String)"MPW0006",(String)"",Contentholder.Pgmname,Contentholder.Pgmdesc,(getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption,(bool)true});
            WebComp_Wcheader.componentbind(new Object[] {(String)"",(String)"",(String)"",(String)""});
         }
         if ( isFullAjaxMode( ) )
         {
            context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0006"+"");
            WebComp_Wcheader.componentdraw();
            context.httpAjaxContext.ajax_rspEndCmp();
         }
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Recentlinkscomponent_Component), StringUtil.Lower( "K2BRecentLinksModern")) != 0 )
         {
            WebComp_Recentlinkscomponent = getWebComponent(GetType(), "GeneXus.Programs", "k2brecentlinksmodern", new Object[] {context} );
            WebComp_Recentlinkscomponent.ComponentInit();
            WebComp_Recentlinkscomponent.Name = "K2BRecentLinksModern";
            WebComp_Recentlinkscomponent_Component = "K2BRecentLinksModern";
         }
         if ( StringUtil.Len( WebComp_Recentlinkscomponent_Component) != 0 )
         {
            WebComp_Recentlinkscomponent.setjustcreated();
            WebComp_Recentlinkscomponent.componentprepare(new Object[] {(String)"MPW0012",(String)"",(getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Caption,(bool)true});
            WebComp_Recentlinkscomponent.componentbind(new Object[] {(String)"",(String)""});
         }
         if ( isFullAjaxMode( ) )
         {
            context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0012"+"");
            WebComp_Recentlinkscomponent.componentdraw();
            context.httpAjaxContext.ajax_rspEndCmp();
         }
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcleftside_Component), StringUtil.Lower( "K2BMenuModern")) != 0 )
         {
            WebComp_Wcleftside = getWebComponent(GetType(), "GeneXus.Programs", "k2bmenumodern", new Object[] {context} );
            WebComp_Wcleftside.ComponentInit();
            WebComp_Wcleftside.Name = "K2BMenuModern";
            WebComp_Wcleftside_Component = "K2BMenuModern";
         }
         if ( StringUtil.Len( WebComp_Wcleftside_Component) != 0 )
         {
            WebComp_Wcleftside.setjustcreated();
            WebComp_Wcleftside.componentprepare(new Object[] {(String)"MPW0009",(String)""});
            WebComp_Wcleftside.componentbind(new Object[] {});
         }
         if ( isFullAjaxMode( ) )
         {
            context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0009"+"");
            WebComp_Wcleftside.componentdraw();
            context.httpAjaxContext.ajax_rspEndCmp();
         }
         /*  Sending Event outputs  */
      }

      protected void nextLoad( )
      {
      }

      protected void E130F2( )
      {
         /* Load Routine */
      }

      protected void wb_table1_3_0F2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + "background-color: " + context.BuildHTMLColor( (int)(0xFFFFFF)) + ";";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(100), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table_GlobalContainer", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"5\"  style=\""+CSSHelper.Prettify( "height:13px")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "MPW0006"+"", StringUtil.RTrim( WebComp_Wcheader_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0006"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcheader_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcheader), StringUtil.Lower( WebComp_Wcheader_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0006"+"");
                  }
                  WebComp_Wcheader.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcheader), StringUtil.Lower( WebComp_Wcheader_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  style=\""+CSSHelper.Prettify( "vertical-align:top;height:39px;width:60px")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "MPW0009"+"", StringUtil.RTrim( WebComp_Wcleftside_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0009"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcleftside_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcleftside), StringUtil.Lower( WebComp_Wcleftside_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0009"+"");
                  }
                  WebComp_Wcleftside.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcleftside), StringUtil.Lower( WebComp_Wcleftside_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td bgcolor=\"#FFFFFF\"  style=\""+CSSHelper.Prettify( "vertical-align:top;height:19px;width:100%")+"\" class='Cell_BorderBottom'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection1_Internalname, 1, 0, "px", 0, "px", "Section_Basic_Margin_Left_10px", "left", "top", "", "", "div");
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "MPW0012"+"", StringUtil.RTrim( WebComp_Recentlinkscomponent_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0012"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Recentlinkscomponent_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldRecentlinkscomponent), StringUtil.Lower( WebComp_Recentlinkscomponent_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0012"+"");
                  }
                  WebComp_Recentlinkscomponent.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldRecentlinkscomponent), StringUtil.Lower( WebComp_Recentlinkscomponent_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td rowspan=\"3\"  style=\""+CSSHelper.Prettify( "width:0px")+"\">") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "MPW0014"+"", StringUtil.RTrim( WebComp_Wcrightside_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0014"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWcrightside), StringUtil.Lower( WebComp_Wcrightside_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0014"+"");
               }
               WebComp_Wcrightside.componentdraw();
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWcrightside), StringUtil.Lower( WebComp_Wcrightside_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellMaincontent_Internalname+"\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "height:26px")+"\" class='Cell_MainContent'>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            /* Content placeholder */
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-content-placeholder");
            context.WriteHtmlText( ">") ;
            if ( ! isFullAjaxMode( ) )
            {
               getDataAreaObject().RenderHtmlContent();
            }
            context.WriteHtmlText( "</div>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellCell_footermessage_Internalname+"\" colspan=\"3\"  style=\""+CSSHelper.Prettify( "height:26px")+"\" class='Cell_BorderTop'>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divSection_footermessage_Internalname, 1, 0, "px", 0, "px", "Section_Basic_FloatRight", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblFooter_text_Internalname, "� Copyright 2014, K2B - All rights reserved", "", "", lblFooter_text_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_K2BWWMasterPageModern.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td colspan=\"5\" >") ;
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "MPW0024"+"", StringUtil.RTrim( WebComp_Wcfooter_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpMPW0024"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWcfooter), StringUtil.Lower( WebComp_Wcfooter_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpMPW0024"+"");
               }
               WebComp_Wcfooter.componentdraw();
               if ( StringUtil.StrCmp(StringUtil.Lower( OldWcfooter), StringUtil.Lower( WebComp_Wcfooter_Component)) != 0 )
               {
                  context.httpAjaxContext.ajax_rspEndCmp();
               }
               context.WriteHtmlText( "</div>") ;
            }
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_3_0F2e( true) ;
         }
         else
         {
            wb_table1_3_0F2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0F2( ) ;
         WS0F2( ) ;
         WE0F2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void master_styles( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), false);
         if ( ! ( WebComp_Wcheader == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcheader_Component) != 0 )
            {
               WebComp_Wcheader.componentthemes();
            }
         }
         if ( ! ( WebComp_Wcleftside == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcleftside_Component) != 0 )
            {
               WebComp_Wcleftside.componentthemes();
            }
         }
         if ( ! ( WebComp_Recentlinkscomponent == null ) )
         {
            if ( StringUtil.Len( WebComp_Recentlinkscomponent_Component) != 0 )
            {
               WebComp_Recentlinkscomponent.componentthemes();
            }
         }
         if ( ! ( WebComp_Wcrightside == null ) )
         {
            WebComp_Wcrightside.componentthemes();
         }
         if ( ! ( WebComp_Wcfooter == null ) )
         {
            WebComp_Wcfooter.componentthemes();
         }
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)(getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Item(idxLst))), "?201811171524684", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("k2bwwmasterpagemodern.js", "?201811171524685", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         divSection1_Internalname = "SECTION1_MPAGE";
         cellMaincontent_Internalname = "MAINCONTENT_MPAGE";
         lblFooter_text_Internalname = "FOOTER_TEXT_MPAGE";
         divSection_footermessage_Internalname = "SECTION_FOOTERMESSAGE_MPAGE";
         cellCell_footermessage_Internalname = "CELL_FOOTERMESSAGE_MPAGE";
         tblTable1_Internalname = "TABLE1_MPAGE";
         divAllcontainer_Internalname = "ALLCONTAINER_MPAGE";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Internalname = "FORM_MPAGE";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Contentholder.setDataArea(getDataAreaObject());
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH_MPAGE","{handler:'Refresh',iparms:[{ctrl:'FORM_MPAGE',prop:'Caption'}],oparms:[{ctrl:'WCHEADER_MPAGE'},{ctrl:'RECENTLINKSCOMPONENT_MPAGE'},{ctrl:'WCLEFTSIDE_MPAGE'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Contentholder = new GXDataAreaControl();
         GXKey = "";
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         OldWcheader = "";
         WebComp_Wcheader_Component = "";
         OldWcleftside = "";
         WebComp_Wcleftside_Component = "";
         OldRecentlinkscomponent = "";
         WebComp_Recentlinkscomponent_Component = "";
         OldWcrightside = "";
         WebComp_Wcrightside_Component = "";
         OldWcfooter = "";
         WebComp_Wcfooter_Component = "";
         sStyleString = "";
         lblFooter_text_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sDynURL = "";
         Form = new GXWebForm();
         WebComp_Wcheader = new GeneXus.Http.GXNullWebComponent();
         WebComp_Wcleftside = new GeneXus.Http.GXNullWebComponent();
         WebComp_Recentlinkscomponent = new GeneXus.Http.GXNullWebComponent();
         WebComp_Wcrightside = new GeneXus.Http.GXNullWebComponent();
         WebComp_Wcfooter = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short initialized ;
      private short GxWebError ;
      private short wbEnd ;
      private short wbStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGotPars ;
      private short nGXWrapped ;
      private int idxLst ;
      private String GXKey ;
      private String sPrefix ;
      private String divAllcontainer_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String OldWcheader ;
      private String WebComp_Wcheader_Component ;
      private String OldWcleftside ;
      private String WebComp_Wcleftside_Component ;
      private String OldRecentlinkscomponent ;
      private String WebComp_Recentlinkscomponent_Component ;
      private String OldWcrightside ;
      private String WebComp_Wcrightside_Component ;
      private String OldWcfooter ;
      private String WebComp_Wcfooter_Component ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String divSection1_Internalname ;
      private String cellMaincontent_Internalname ;
      private String cellCell_footermessage_Internalname ;
      private String divSection_footermessage_Internalname ;
      private String lblFooter_text_Internalname ;
      private String lblFooter_text_Jsonclick ;
      private String sDynURL ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool toggleJsOutput ;
      private bool returnInSub ;
      private GXWebComponent WebComp_Wcheader ;
      private GXWebComponent WebComp_Wcleftside ;
      private GXWebComponent WebComp_Recentlinkscomponent ;
      private GXWebComponent WebComp_Wcrightside ;
      private GXWebComponent WebComp_Wcfooter ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXDataAreaControl Contentholder ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
   }

}
