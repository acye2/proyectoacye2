/*
               File: K2BLoadGridColumns
        Description: K2B Load Grid State
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:37.23
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bloadgridcolumns : GXProcedure
   {
      public k2bloadgridcolumns( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bloadgridcolumns( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_ProgramName ,
                           String aP1_GridName ,
                           out GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> aP2_GridColumns )
      {
         this.AV10ProgramName = aP0_ProgramName;
         this.AV9GridName = aP1_GridName;
         this.AV8GridColumns = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2") ;
         initialize();
         executePrivate();
         aP2_GridColumns=this.AV8GridColumns;
      }

      public GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> executeUdp( String aP0_ProgramName ,
                                                                                String aP1_GridName )
      {
         this.AV10ProgramName = aP0_ProgramName;
         this.AV9GridName = aP1_GridName;
         this.AV8GridColumns = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2") ;
         initialize();
         executePrivate();
         aP2_GridColumns=this.AV8GridColumns;
         return AV8GridColumns ;
      }

      public void executeSubmit( String aP0_ProgramName ,
                                 String aP1_GridName ,
                                 out GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> aP2_GridColumns )
      {
         k2bloadgridcolumns objk2bloadgridcolumns;
         objk2bloadgridcolumns = new k2bloadgridcolumns();
         objk2bloadgridcolumns.AV10ProgramName = aP0_ProgramName;
         objk2bloadgridcolumns.AV9GridName = aP1_GridName;
         objk2bloadgridcolumns.AV8GridColumns = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2") ;
         objk2bloadgridcolumns.context.SetSubmitInitialConfig(context);
         objk2bloadgridcolumns.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bloadgridcolumns);
         aP2_GridColumns=this.AV8GridColumns;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bloadgridcolumns)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV12SessionString = AV11Session.Get(StringUtil.Trim( AV10ProgramName)+StringUtil.Trim( AV9GridName)+"GridColumns");
         if ( StringUtil.StrCmp(AV12SessionString, "") != 0 )
         {
            AV8GridColumns.FromXml(AV12SessionString, null, "K2BGridColumns", "PACYE2");
         }
         else
         {
            GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1 = AV8GridColumns;
            new k2bretrievegridcolumns(context ).execute(  AV10ProgramName,  AV9GridName, out  GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1) ;
            AV8GridColumns = GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV12SessionString = "";
         AV11Session = context.GetSession();
         GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1 = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2");
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV10ProgramName ;
      private String AV9GridName ;
      private String AV12SessionString ;
      private IGxSession AV11Session ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> aP2_GridColumns ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> AV8GridColumns ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1 ;
   }

}
