/*
               File: K2BGetStack
        Description: K2B Gets Stack
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:34.60
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bgetstack : GXProcedure
   {
      public k2bgetstack( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bgetstack( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack )
      {
         this.AV8Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2") ;
         initialize();
         executePrivate();
         aP0_Stack=this.AV8Stack;
      }

      public GXBaseCollection<SdtK2BStack_K2BStackItem> executeUdp( )
      {
         this.AV8Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2") ;
         initialize();
         executePrivate();
         aP0_Stack=this.AV8Stack;
         return AV8Stack ;
      }

      public void executeSubmit( out GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack )
      {
         k2bgetstack objk2bgetstack;
         objk2bgetstack = new k2bgetstack();
         objk2bgetstack.AV8Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2") ;
         objk2bgetstack.context.SetSubmitInitialConfig(context);
         objk2bgetstack.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bgetstack);
         aP0_Stack=this.AV8Stack;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bgetstack)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         GXt_char1 = AV9xml;
         new k2bsessionget(context ).execute(  "Stack", out  GXt_char1) ;
         AV9xml = GXt_char1;
         AV8Stack.FromXml(AV9xml, null, "K2BStack", "PACYE2");
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9xml = "";
         GXt_char1 = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String GXt_char1 ;
      private String AV9xml ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> AV8Stack ;
   }

}
