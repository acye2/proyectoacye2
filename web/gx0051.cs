/*
               File: Gx0051
        Description: Selection List Detalle
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:4:25.44
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gx0051 : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gx0051( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public gx0051( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( Guid aP0_pRecorridoID ,
                           out Guid aP1_pRecorridoDetalleID )
      {
         this.AV12pRecorridoID = aP0_pRecorridoID;
         this.AV13pRecorridoDetalleID = Guid.Empty ;
         executePrivate();
         aP1_pRecorridoDetalleID=this.AV13pRecorridoDetalleID;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavCtiposenal = new GXCombobox();
         cmbavCtipoobstaculo = new GXCombobox();
         cmbavCincidente = new GXCombobox();
         cmbTipoSenal = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid1") == 0 )
            {
               nRC_GXsfl_74 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_74_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_74_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid1_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid1") == 0 )
            {
               subGrid1_Rows = (int)(NumberUtil.Val( GetNextPar( ), "."));
               AV6cRecorridoDetalleID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV7cRecorridoDetallePaciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV8cIDCoach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               AV9cTipoSenal = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV10cTipoObstaculo = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV11cIncidente = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV12pRecorridoID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid1_refresh( subGrid1_Rows, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente, AV12pRecorridoID) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV12pRecorridoID = (Guid)(StringUtil.StrToGuid( gxfirstwebparm));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12pRecorridoID", AV12pRecorridoID.ToString());
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV13pRecorridoDetalleID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pRecorridoDetalleID", AV13pRecorridoDetalleID.ToString());
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "gx0051_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("rwdpromptmasterpage", "GeneXus.Programs.rwdpromptmasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,true);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA1K2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START1K2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171542551", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gx0051.aspx") + "?" + UrlEncode(AV12pRecorridoID.ToString()) + "," + UrlEncode(AV13pRecorridoDetalleID.ToString())+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         GxWebStd.gx_hidden_field( context, "GXH_vCRECORRIDODETALLEID", AV6cRecorridoDetalleID.ToString());
         GxWebStd.gx_hidden_field( context, "GXH_vCRECORRIDODETALLEPACIENTE", AV7cRecorridoDetallePaciente.ToString());
         GxWebStd.gx_hidden_field( context, "GXH_vCIDCOACH", AV8cIDCoach.ToString());
         GxWebStd.gx_hidden_field( context, "GXH_vCTIPOSENAL", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9cTipoSenal), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCTIPOOBSTACULO", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10cTipoObstaculo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GXH_vCINCIDENTE", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV11cIncidente), 4, 0, ",", "")));
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_74", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_74), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPRECORRIDOID", AV12pRecorridoID.ToString());
         GxWebStd.gx_hidden_field( context, "vPRECORRIDODETALLEID", AV13pRecorridoDetalleID.ToString());
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "ADVANCEDCONTAINER_Class", StringUtil.RTrim( divAdvancedcontainer_Class));
         GxWebStd.gx_hidden_field( context, "BTNTOGGLE_Class", StringUtil.RTrim( bttBtntoggle_Class));
         GxWebStd.gx_hidden_field( context, "RECORRIDODETALLEIDFILTERCONTAINER_Class", StringUtil.RTrim( divRecorridodetalleidfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "RECORRIDODETALLEPACIENTEFILTERCONTAINER_Class", StringUtil.RTrim( divRecorridodetallepacientefiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "IDCOACHFILTERCONTAINER_Class", StringUtil.RTrim( divIdcoachfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "TIPOSENALFILTERCONTAINER_Class", StringUtil.RTrim( divTiposenalfiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "TIPOOBSTACULOFILTERCONTAINER_Class", StringUtil.RTrim( divTipoobstaculofiltercontainer_Class));
         GxWebStd.gx_hidden_field( context, "INCIDENTEFILTERCONTAINER_Class", StringUtil.RTrim( divIncidentefiltercontainer_Class));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", "notset");
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE1K2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT1K2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gx0051.aspx") + "?" + UrlEncode(AV12pRecorridoID.ToString()) + "," + UrlEncode(AV13pRecorridoDetalleID.ToString()) ;
      }

      public override String GetPgmname( )
      {
         return "Gx0051" ;
      }

      public override String GetPgmdesc( )
      {
         return "Selection List Detalle" ;
      }

      protected void WB1K0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMain_Internalname, 1, 0, "px", 0, "px", "ContainerFluid PromptContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 PromptAdvancedBarCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAdvancedcontainer_Internalname, 1, 0, "px", 0, "px", divAdvancedcontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divRecorridodetalleidfiltercontainer_Internalname, 1, 0, "px", 0, "px", divRecorridodetalleidfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblrecorridodetalleidfilter_Internalname, "Recorrido Detalle ID", "", "", lblLblrecorridodetalleidfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e111k1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCrecorridodetalleid_Internalname, "Recorrido Detalle ID", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'" + sGXsfl_74_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCrecorridodetalleid_Internalname, AV6cRecorridoDetalleID.ToString(), AV6cRecorridoDetalleID.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,16);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCrecorridodetalleid_Jsonclick, 0, "Attribute", "", "", "", "", edtavCrecorridodetalleid_Visible, edtavCrecorridodetalleid_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divRecorridodetallepacientefiltercontainer_Internalname, 1, 0, "px", 0, "px", divRecorridodetallepacientefiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblrecorridodetallepacientefilter_Internalname, "Recorrido Detalle Paciente", "", "", lblLblrecorridodetallepacientefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e121k1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCrecorridodetallepaciente_Internalname, "Recorrido Detalle Paciente", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 26,'',false,'" + sGXsfl_74_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCrecorridodetallepaciente_Internalname, AV7cRecorridoDetallePaciente.ToString(), AV7cRecorridoDetallePaciente.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,26);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCrecorridodetallepaciente_Jsonclick, 0, "Attribute", "", "", "", "", edtavCrecorridodetallepaciente_Visible, edtavCrecorridodetallepaciente_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divIdcoachfiltercontainer_Internalname, 1, 0, "px", 0, "px", divIdcoachfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblidcoachfilter_Internalname, "IDCoach", "", "", lblLblidcoachfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e131k1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCidcoach_Internalname, "IDCoach", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 36,'',false,'" + sGXsfl_74_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCidcoach_Internalname, AV8cIDCoach.ToString(), AV8cIDCoach.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,36);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCidcoach_Jsonclick, 0, "Attribute", "", "", "", "", edtavCidcoach_Visible, edtavCidcoach_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTiposenalfiltercontainer_Internalname, 1, 0, "px", 0, "px", divTiposenalfiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLbltiposenalfilter_Internalname, "Tipo Senal", "", "", lblLbltiposenalfilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e141k1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavCtiposenal_Internalname, "Tipo Senal", "col-sm-3 AttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_74_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavCtiposenal, cmbavCtiposenal_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0)), 1, cmbavCtiposenal_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavCtiposenal.Visible, cmbavCtiposenal.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "", true, "HLP_Gx0051.htm");
            cmbavCtiposenal.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtiposenal_Internalname, "Values", (String)(cmbavCtiposenal.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTipoobstaculofiltercontainer_Internalname, 1, 0, "px", 0, "px", divTipoobstaculofiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLbltipoobstaculofilter_Internalname, "Tipo Obstaculo", "", "", lblLbltipoobstaculofilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e151k1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavCtipoobstaculo_Internalname, "Tipo Obstaculo", "col-sm-3 AttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'',false,'" + sGXsfl_74_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavCtipoobstaculo, cmbavCtipoobstaculo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0)), 1, cmbavCtipoobstaculo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavCtipoobstaculo.Visible, cmbavCtipoobstaculo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,56);\"", "", true, "HLP_Gx0051.htm");
            cmbavCtipoobstaculo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtipoobstaculo_Internalname, "Values", (String)(cmbavCtipoobstaculo.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divIncidentefiltercontainer_Internalname, 1, 0, "px", 0, "px", divIncidentefiltercontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblLblincidentefilter_Internalname, "Incidente", "", "", lblLblincidentefilter_Jsonclick, "'"+""+"'"+",false,"+"'"+"e161k1_client"+"'", "", "WWAdvancedLabel WWFilterLabel", 7, "", 1, 1, 1, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 WWFiltersCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavCincidente_Internalname, "Incidente", "col-sm-3 AttributeLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'" + sGXsfl_74_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavCincidente, cmbavCincidente_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0)), 1, cmbavCincidente_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", cmbavCincidente.Visible, cmbavCincidente.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "", true, "HLP_Gx0051.htm");
            cmbavCincidente.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCincidente_Internalname, "Values", (String)(cmbavCincidente.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 WWGridCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridtable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 hidden-sm hidden-md hidden-lg ToggleCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            ClassString = bttBtntoggle_Class;
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntoggle_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(74), 2, 0)+","+"null"+");", "|||", bttBtntoggle_Jsonclick, 7, "|||", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e171k1_client"+"'", TempTags, "", 2, "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            Grid1Container.SetWrapped(nGXWrapped);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"DivS\" data-gxgridid=\"74\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid1_Internalname, subGrid1_Internalname, "", "PromptGrid", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid1_Backcolorstyle == 0 )
               {
                  subGrid1_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid1_Class) > 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Title";
                  }
               }
               else
               {
                  subGrid1_Titlebackstyle = 1;
                  if ( subGrid1_Backcolorstyle == 1 )
                  {
                     subGrid1_Titlebackcolor = subGrid1_Allbackcolor;
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid1_Class) > 0 )
                     {
                        subGrid1_Linesclass = subGrid1_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"SelectionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Detalle ID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Detalle Paciente") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "IDCoach") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"DescriptionAttribute"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Senal") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute"+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Recorrido ID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               Grid1Container.AddObjectProperty("GridName", "Grid1");
            }
            else
            {
               if ( isAjaxCallMode( ) )
               {
                  Grid1Container = new GXWebGrid( context);
               }
               else
               {
                  Grid1Container.Clear();
               }
               Grid1Container.SetWrapped(nGXWrapped);
               Grid1Container.AddObjectProperty("GridName", "Grid1");
               Grid1Container.AddObjectProperty("Class", "PromptGrid");
               Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("CmpContext", "");
               Grid1Container.AddObjectProperty("InMasterPage", "false");
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", context.convertURL( AV5LinkSelection));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( edtavLinkselection_Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A32RecorridoDetalleID.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A21RecorridoDetallePaciente.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A22IDCoach.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25TipoSenal), 4, 0, ".", "")));
               Grid1Column.AddObjectProperty("Link", StringUtil.RTrim( cmbTipoSenal.Link));
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Column = GXWebColumn.GetNew(isAjaxCallMode( ));
               Grid1Column.AddObjectProperty("Value", A23RecorridoID.ToString());
               Grid1Container.AddColumnProperties(Grid1Column);
               Grid1Container.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowselection), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Selectioncolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowhovering), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Hoveringcolor), 9, 0, ".", "")));
               Grid1Container.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Allowcollapsing), 1, 0, ".", "")));
               Grid1Container.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 74 )
         {
            wbEnd = 0;
            nRC_GXsfl_74 = (short)(nGXsfl_74_idx-1);
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               Grid1Container.AddObjectProperty("GRID1_nEOF", GRID1_nEOF);
               Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"Grid1Container"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid1", Grid1Container);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData", Grid1Container.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "Grid1ContainerData"+"V", Grid1Container.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"Grid1ContainerData"+"V"+"\" value='"+Grid1Container.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 83,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(74), 2, 0)+","+"null"+");", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Gx0051.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START1K2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Selection List Detalle", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP1K0( ) ;
      }

      protected void WS1K2( )
      {
         START1K2( ) ;
         EVT1K2( ) ;
      }

      protected void EVT1K2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "GRID1PAGING") == 0 )
                           {
                              context.wbHandled = 1;
                              sEvt = cgiGet( "GRID1PAGING");
                              if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                              {
                                 subgrid1_firstpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "PREV") == 0 )
                              {
                                 subgrid1_previouspage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                              {
                                 subgrid1_nextpage( ) ;
                              }
                              else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                              {
                                 subgrid1_lastpage( ) ;
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 4), "LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) )
                           {
                              nGXsfl_74_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_74_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_74_idx), 4, 0)), 4, "0");
                              SubsflControlProps_742( ) ;
                              AV5LinkSelection = cgiGet( edtavLinkselection_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Bitmap", (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV17Linkselection_GXI : context.convertURL( context.PathToRelativeUrl( AV5LinkSelection))), !bGXsfl_74_Refreshing);
                              context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "SrcSet", context.GetImageSrcSet( AV5LinkSelection), true);
                              A32RecorridoDetalleID = (Guid)(StringUtil.StrToGuid( cgiGet( edtRecorridoDetalleID_Internalname)));
                              A21RecorridoDetallePaciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtRecorridoDetallePaciente_Internalname)));
                              n21RecorridoDetallePaciente = false;
                              A22IDCoach = (Guid)(StringUtil.StrToGuid( cgiGet( edtIDCoach_Internalname)));
                              n22IDCoach = false;
                              cmbTipoSenal.Name = cmbTipoSenal_Internalname;
                              cmbTipoSenal.CurrentValue = cgiGet( cmbTipoSenal_Internalname);
                              A25TipoSenal = (short)(NumberUtil.Val( cgiGet( cmbTipoSenal_Internalname), "."));
                              n25TipoSenal = false;
                              A23RecorridoID = (Guid)(StringUtil.StrToGuid( cgiGet( edtRecorridoID_Internalname)));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E181K2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E191K2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       /* Set Refresh If Crecorridodetalleid Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCRECORRIDODETALLEID")) != AV6cRecorridoDetalleID )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Crecorridodetallepaciente Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCRECORRIDODETALLEPACIENTE")) != AV7cRecorridoDetallePaciente )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cidcoach Changed */
                                       if ( StringUtil.StrToGuid( cgiGet( "GXH_vCIDCOACH")) != AV8cIDCoach )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ctiposenal Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCTIPOSENAL"), ",", ".") != Convert.ToDecimal( AV9cTipoSenal )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Ctipoobstaculo Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCTIPOOBSTACULO"), ",", ".") != Convert.ToDecimal( AV10cTipoObstaculo )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       /* Set Refresh If Cincidente Changed */
                                       if ( ( context.localUtil.CToN( cgiGet( "GXH_vCINCIDENTE"), ",", ".") != Convert.ToDecimal( AV11cIncidente )) )
                                       {
                                          Rfr0gs = true;
                                       }
                                       if ( ! Rfr0gs )
                                       {
                                          /* Execute user event: Enter */
                                          E201K2 ();
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1K2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA1K2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavCtiposenal.Name = "vCTIPOSENAL";
            cmbavCtiposenal.WebTags = "";
            cmbavCtiposenal.addItem("1", "Inicio", 0);
            cmbavCtiposenal.addItem("2", "Progreso", 0);
            cmbavCtiposenal.addItem("3", "Fin", 0);
            if ( cmbavCtiposenal.ItemCount > 0 )
            {
               AV9cTipoSenal = (short)(NumberUtil.Val( cmbavCtiposenal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cTipoSenal", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0)));
            }
            cmbavCtipoobstaculo.Name = "vCTIPOOBSTACULO";
            cmbavCtipoobstaculo.WebTags = "";
            cmbavCtipoobstaculo.addItem("0", "Ninguno", 0);
            cmbavCtipoobstaculo.addItem("1", "Agujero", 0);
            cmbavCtipoobstaculo.addItem("2", "Gradas", 0);
            cmbavCtipoobstaculo.addItem("3", "Otros", 0);
            if ( cmbavCtipoobstaculo.ItemCount > 0 )
            {
               AV10cTipoObstaculo = (short)(NumberUtil.Val( cmbavCtipoobstaculo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cTipoObstaculo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0)));
            }
            cmbavCincidente.Name = "vCINCIDENTE";
            cmbavCincidente.WebTags = "";
            cmbavCincidente.addItem("0", "Nada", 0);
            cmbavCincidente.addItem("1", "Registrar Obstaculo", 0);
            if ( cmbavCincidente.ItemCount > 0 )
            {
               AV11cIncidente = (short)(NumberUtil.Val( cmbavCincidente.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11cIncidente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0)));
            }
            GXCCtl = "TIPOSENAL_" + sGXsfl_74_idx;
            cmbTipoSenal.Name = GXCCtl;
            cmbTipoSenal.WebTags = "";
            cmbTipoSenal.addItem("1", "Inicio", 0);
            cmbTipoSenal.addItem("2", "Progreso", 0);
            cmbTipoSenal.addItem("3", "Fin", 0);
            if ( cmbTipoSenal.ItemCount > 0 )
            {
               A25TipoSenal = (short)(NumberUtil.Val( cmbTipoSenal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0))), "."));
               n25TipoSenal = false;
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid1_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_742( ) ;
         while ( nGXsfl_74_idx <= nRC_GXsfl_74 )
         {
            sendrow_742( ) ;
            nGXsfl_74_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_74_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_74_idx+1));
            sGXsfl_74_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_74_idx), 4, 0)), 4, "0");
            SubsflControlProps_742( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Grid1Container));
         /* End function gxnrGrid1_newrow */
      }

      protected void gxgrGrid1_refresh( int subGrid1_Rows ,
                                        Guid AV6cRecorridoDetalleID ,
                                        Guid AV7cRecorridoDetallePaciente ,
                                        Guid AV8cIDCoach ,
                                        short AV9cTipoSenal ,
                                        short AV10cTipoObstaculo ,
                                        short AV11cIncidente ,
                                        Guid AV12pRecorridoID )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GxWebStd.gx_hidden_field( context, "GRID1_Rows", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Rows), 6, 0, ".", "")));
         GRID1_nCurrentRecord = 0;
         RF1K2( ) ;
         /* End function gxgrGrid1_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_RECORRIDODETALLEID", GetSecureSignedToken( "", A32RecorridoDetalleID, context));
         GxWebStd.gx_hidden_field( context, "RECORRIDODETALLEID", A32RecorridoDetalleID.ToString());
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavCtiposenal.ItemCount > 0 )
         {
            AV9cTipoSenal = (short)(NumberUtil.Val( cmbavCtiposenal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cTipoSenal", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavCtiposenal.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtiposenal_Internalname, "Values", cmbavCtiposenal.ToJavascriptSource(), true);
         }
         if ( cmbavCtipoobstaculo.ItemCount > 0 )
         {
            AV10cTipoObstaculo = (short)(NumberUtil.Val( cmbavCtipoobstaculo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cTipoObstaculo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavCtipoobstaculo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCtipoobstaculo_Internalname, "Values", cmbavCtipoobstaculo.ToJavascriptSource(), true);
         }
         if ( cmbavCincidente.ItemCount > 0 )
         {
            AV11cIncidente = (short)(NumberUtil.Val( cmbavCincidente.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11cIncidente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavCincidente.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCincidente_Internalname, "Values", cmbavCincidente.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1K2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF1K2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            Grid1Container.ClearRows();
         }
         wbStart = 74;
         nGXsfl_74_idx = 1;
         sGXsfl_74_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_74_idx), 4, 0)), 4, "0");
         SubsflControlProps_742( ) ;
         bGXsfl_74_Refreshing = true;
         Grid1Container.AddObjectProperty("GridName", "Grid1");
         Grid1Container.AddObjectProperty("CmpContext", "");
         Grid1Container.AddObjectProperty("InMasterPage", "false");
         Grid1Container.AddObjectProperty("Class", "PromptGrid");
         Grid1Container.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Grid1Container.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid1_Backcolorstyle), 1, 0, ".", "")));
         Grid1Container.PageSize = subGrid1_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_742( ) ;
            GXPagingFrom2 = (int)(((10==0) ? 0 : GRID1_nFirstRecordOnPage));
            GXPagingTo2 = ((10==0) ? 10000 : subGrid1_Recordsperpage( )+1);
            pr_default.dynParam(0, new Object[]{ new Object[]{
                                                 AV7cRecorridoDetallePaciente ,
                                                 AV8cIDCoach ,
                                                 AV9cTipoSenal ,
                                                 AV10cTipoObstaculo ,
                                                 AV11cIncidente ,
                                                 A21RecorridoDetallePaciente ,
                                                 A22IDCoach ,
                                                 A25TipoSenal ,
                                                 A28TipoObstaculo ,
                                                 A29Incidente ,
                                                 AV12pRecorridoID ,
                                                 AV6cRecorridoDetalleID ,
                                                 A23RecorridoID } ,
                                                 new int[]{
                                                 TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                                 TypeConstants.BOOLEAN
                                                 }
            } ) ;
            /* Using cursor H001K2 */
            pr_default.execute(0, new Object[] {AV12pRecorridoID, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente, GXPagingFrom2, GXPagingTo2, GXPagingTo2});
            nGXsfl_74_idx = 1;
            sGXsfl_74_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_74_idx), 4, 0)), 4, "0");
            SubsflControlProps_742( ) ;
            while ( ( (pr_default.getStatus(0) != 101) ) && ( ( ( 10 == 0 ) || ( GRID1_nCurrentRecord < subGrid1_Recordsperpage( ) ) ) ) )
            {
               A29Incidente = H001K2_A29Incidente[0];
               n29Incidente = H001K2_n29Incidente[0];
               A28TipoObstaculo = H001K2_A28TipoObstaculo[0];
               n28TipoObstaculo = H001K2_n28TipoObstaculo[0];
               A23RecorridoID = (Guid)((Guid)(H001K2_A23RecorridoID[0]));
               A25TipoSenal = H001K2_A25TipoSenal[0];
               n25TipoSenal = H001K2_n25TipoSenal[0];
               A22IDCoach = (Guid)((Guid)(H001K2_A22IDCoach[0]));
               n22IDCoach = H001K2_n22IDCoach[0];
               A21RecorridoDetallePaciente = (Guid)((Guid)(H001K2_A21RecorridoDetallePaciente[0]));
               n21RecorridoDetallePaciente = H001K2_n21RecorridoDetallePaciente[0];
               A32RecorridoDetalleID = (Guid)((Guid)(H001K2_A32RecorridoDetalleID[0]));
               /* Execute user event: Load */
               E191K2 ();
               pr_default.readNext(0);
            }
            GRID1_nEOF = (short)(((pr_default.getStatus(0) == 101) ? 1 : 0));
            GxWebStd.gx_hidden_field( context, "GRID1_nEOF", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nEOF), 1, 0, ".", "")));
            pr_default.close(0);
            wbEnd = 74;
            WB1K0( ) ;
         }
         bGXsfl_74_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes1K2( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_RECORRIDODETALLEID"+"_"+sGXsfl_74_idx, GetSecureSignedToken( sGXsfl_74_idx, A32RecorridoDetalleID, context));
      }

      protected int subGrid1_Pagecount( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
         {
            return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))) ;
         }
         return (int)(NumberUtil.Int( (long)(GRID1_nRecordCount/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected int subGrid1_Recordcount( )
      {
         pr_default.dynParam(1, new Object[]{ new Object[]{
                                              AV7cRecorridoDetallePaciente ,
                                              AV8cIDCoach ,
                                              AV9cTipoSenal ,
                                              AV10cTipoObstaculo ,
                                              AV11cIncidente ,
                                              A21RecorridoDetallePaciente ,
                                              A22IDCoach ,
                                              A25TipoSenal ,
                                              A28TipoObstaculo ,
                                              A29Incidente ,
                                              AV12pRecorridoID ,
                                              AV6cRecorridoDetalleID ,
                                              A23RecorridoID } ,
                                              new int[]{
                                              TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT, TypeConstants.BOOLEAN, TypeConstants.SHORT,
                                              TypeConstants.BOOLEAN
                                              }
         } ) ;
         /* Using cursor H001K3 */
         pr_default.execute(1, new Object[] {AV12pRecorridoID, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente});
         GRID1_nRecordCount = H001K3_AGRID1_nRecordCount[0];
         pr_default.close(1);
         return (int)(GRID1_nRecordCount) ;
      }

      protected int subGrid1_Recordsperpage( )
      {
         return (int)(10*1) ;
      }

      protected int subGrid1_Currentpage( )
      {
         return (int)(NumberUtil.Int( (long)(GRID1_nFirstRecordOnPage/ (decimal)(subGrid1_Recordsperpage( ))))+1) ;
      }

      protected short subgrid1_firstpage( )
      {
         GRID1_nFirstRecordOnPage = 0;
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente, AV12pRecorridoID) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid1_nextpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( ( GRID1_nRecordCount >= subGrid1_Recordsperpage( ) ) && ( GRID1_nEOF == 0 ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage+subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente, AV12pRecorridoID) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         Grid1Container.AddObjectProperty("GRID1_nFirstRecordOnPage", GRID1_nFirstRecordOnPage);
         return (short)(((GRID1_nEOF==0) ? 0 : 2)) ;
      }

      protected short subgrid1_previouspage( )
      {
         if ( GRID1_nFirstRecordOnPage >= subGrid1_Recordsperpage( ) )
         {
            GRID1_nFirstRecordOnPage = (long)(GRID1_nFirstRecordOnPage-subGrid1_Recordsperpage( ));
         }
         else
         {
            return 2 ;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente, AV12pRecorridoID) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected short subgrid1_lastpage( )
      {
         GRID1_nRecordCount = subGrid1_Recordcount( );
         if ( GRID1_nRecordCount > subGrid1_Recordsperpage( ) )
         {
            if ( ((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))) == 0 )
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-subGrid1_Recordsperpage( ));
            }
            else
            {
               GRID1_nFirstRecordOnPage = (long)(GRID1_nRecordCount-((int)((GRID1_nRecordCount) % (subGrid1_Recordsperpage( )))));
            }
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente, AV12pRecorridoID) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return 0 ;
      }

      protected int subgrid1_gotopage( int nPageNo )
      {
         if ( nPageNo > 0 )
         {
            GRID1_nFirstRecordOnPage = (long)(subGrid1_Recordsperpage( )*(nPageNo-1));
         }
         else
         {
            GRID1_nFirstRecordOnPage = 0;
         }
         if ( isFullAjaxMode( ) )
         {
            gxgrGrid1_refresh( subGrid1_Rows, AV6cRecorridoDetalleID, AV7cRecorridoDetallePaciente, AV8cIDCoach, AV9cTipoSenal, AV10cTipoObstaculo, AV11cIncidente, AV12pRecorridoID) ;
         }
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "GRID1_nFirstRecordOnPage", StringUtil.LTrim( StringUtil.NToC( (decimal)(GRID1_nFirstRecordOnPage), 15, 0, ".", "")));
         return (int)(0) ;
      }

      protected void STRUP1K0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E181K2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( StringUtil.StrCmp(cgiGet( edtavCrecorridodetalleid_Internalname), "") == 0 )
            {
               AV6cRecorridoDetalleID = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cRecorridoDetalleID", AV6cRecorridoDetalleID.ToString());
            }
            else
            {
               try
               {
                  AV6cRecorridoDetalleID = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCrecorridodetalleid_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6cRecorridoDetalleID", AV6cRecorridoDetalleID.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCRECORRIDODETALLEID");
                  GX_FocusControl = edtavCrecorridodetalleid_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            if ( StringUtil.StrCmp(cgiGet( edtavCrecorridodetallepaciente_Internalname), "") == 0 )
            {
               AV7cRecorridoDetallePaciente = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cRecorridoDetallePaciente", AV7cRecorridoDetallePaciente.ToString());
            }
            else
            {
               try
               {
                  AV7cRecorridoDetallePaciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCrecorridodetallepaciente_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7cRecorridoDetallePaciente", AV7cRecorridoDetallePaciente.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCRECORRIDODETALLEPACIENTE");
                  GX_FocusControl = edtavCrecorridodetallepaciente_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            if ( StringUtil.StrCmp(cgiGet( edtavCidcoach_Internalname), "") == 0 )
            {
               AV8cIDCoach = (Guid)(Guid.Empty);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cIDCoach", AV8cIDCoach.ToString());
            }
            else
            {
               try
               {
                  AV8cIDCoach = (Guid)(StringUtil.StrToGuid( cgiGet( edtavCidcoach_Internalname)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8cIDCoach", AV8cIDCoach.ToString());
               }
               catch ( Exception e )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "vCIDCOACH");
                  GX_FocusControl = edtavCidcoach_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
               }
            }
            cmbavCtiposenal.Name = cmbavCtiposenal_Internalname;
            cmbavCtiposenal.CurrentValue = cgiGet( cmbavCtiposenal_Internalname);
            AV9cTipoSenal = (short)(NumberUtil.Val( cgiGet( cmbavCtiposenal_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9cTipoSenal", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9cTipoSenal), 4, 0)));
            cmbavCtipoobstaculo.Name = cmbavCtipoobstaculo_Internalname;
            cmbavCtipoobstaculo.CurrentValue = cgiGet( cmbavCtipoobstaculo_Internalname);
            AV10cTipoObstaculo = (short)(NumberUtil.Val( cgiGet( cmbavCtipoobstaculo_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10cTipoObstaculo", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10cTipoObstaculo), 4, 0)));
            cmbavCincidente.Name = cmbavCincidente_Internalname;
            cmbavCincidente.CurrentValue = cgiGet( cmbavCincidente_Internalname);
            AV11cIncidente = (short)(NumberUtil.Val( cgiGet( cmbavCincidente_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11cIncidente", StringUtil.LTrim( StringUtil.Str( (decimal)(AV11cIncidente), 4, 0)));
            /* Read saved values. */
            nRC_GXsfl_74 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_74"), ",", "."));
            GRID1_nFirstRecordOnPage = (long)(context.localUtil.CToN( cgiGet( "GRID1_nFirstRecordOnPage"), ",", "."));
            GRID1_nEOF = (short)(context.localUtil.CToN( cgiGet( "GRID1_nEOF"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            /* Check if conditions changed and reset current page numbers */
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCRECORRIDODETALLEID")) != AV6cRecorridoDetalleID )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCRECORRIDODETALLEPACIENTE")) != AV7cRecorridoDetallePaciente )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( StringUtil.StrToGuid( cgiGet( "GXH_vCIDCOACH")) != AV8cIDCoach )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCTIPOSENAL"), ",", ".") != Convert.ToDecimal( AV9cTipoSenal )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCTIPOOBSTACULO"), ",", ".") != Convert.ToDecimal( AV10cTipoObstaculo )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
            if ( ( context.localUtil.CToN( cgiGet( "GXH_vCINCIDENTE"), ",", ".") != Convert.ToDecimal( AV11cIncidente )) )
            {
               GRID1_nFirstRecordOnPage = 0;
            }
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E181K2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E181K2( )
      {
         /* Start Routine */
         Form.Caption = StringUtil.Format( "Lista de Selecci�n %1", "Detalle", "", "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Caption", Form.Caption, true);
         AV14ADVANCED_LABEL_TEMPLATE = "%1 <strong>%2</strong>";
      }

      private void E191K2( )
      {
         /* Load Routine */
         AV5LinkSelection = context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavLinkselection_Internalname, AV5LinkSelection);
         AV17Linkselection_GXI = GXDbFile.PathToUrl( context.GetImagePath( "3914535b-0c03-44c5-9538-906a99cdd2bc", "", context.GetTheme( )));
         sendrow_742( ) ;
         GRID1_nCurrentRecord = (long)(GRID1_nCurrentRecord+1);
         if ( isFullAjaxMode( ) && ! bGXsfl_74_Refreshing )
         {
            context.DoAjaxLoad(74, Grid1Row);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E201K2 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void E201K2( )
      {
         /* Enter Routine */
         AV13pRecorridoDetalleID = (Guid)(A32RecorridoDetalleID);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pRecorridoDetalleID", AV13pRecorridoDetalleID.ToString());
         context.setWebReturnParms(new Object[] {(Guid)AV13pRecorridoDetalleID});
         context.setWebReturnParmsMetadata(new Object[] {"AV13pRecorridoDetalleID"});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
         /*  Sending Event outputs  */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV12pRecorridoID = (Guid)((Guid)getParm(obj,0));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12pRecorridoID", AV12pRecorridoID.ToString());
         AV13pRecorridoDetalleID = (Guid)((Guid)getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13pRecorridoDetalleID", AV13pRecorridoDetalleID.ToString());
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1K2( ) ;
         WS1K2( ) ;
         WE1K2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171542689", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gx0051.js", "?201811171542689", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_742( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_74_idx;
         edtRecorridoDetalleID_Internalname = "RECORRIDODETALLEID_"+sGXsfl_74_idx;
         edtRecorridoDetallePaciente_Internalname = "RECORRIDODETALLEPACIENTE_"+sGXsfl_74_idx;
         edtIDCoach_Internalname = "IDCOACH_"+sGXsfl_74_idx;
         cmbTipoSenal_Internalname = "TIPOSENAL_"+sGXsfl_74_idx;
         edtRecorridoID_Internalname = "RECORRIDOID_"+sGXsfl_74_idx;
      }

      protected void SubsflControlProps_fel_742( )
      {
         edtavLinkselection_Internalname = "vLINKSELECTION_"+sGXsfl_74_fel_idx;
         edtRecorridoDetalleID_Internalname = "RECORRIDODETALLEID_"+sGXsfl_74_fel_idx;
         edtRecorridoDetallePaciente_Internalname = "RECORRIDODETALLEPACIENTE_"+sGXsfl_74_fel_idx;
         edtIDCoach_Internalname = "IDCOACH_"+sGXsfl_74_fel_idx;
         cmbTipoSenal_Internalname = "TIPOSENAL_"+sGXsfl_74_fel_idx;
         edtRecorridoID_Internalname = "RECORRIDOID_"+sGXsfl_74_fel_idx;
      }

      protected void sendrow_742( )
      {
         SubsflControlProps_742( ) ;
         WB1K0( ) ;
         if ( ( 10 * 1 == 0 ) || ( nGXsfl_74_idx <= subGrid1_Recordsperpage( ) * 1 ) )
         {
            Grid1Row = GXWebRow.GetNew(context,Grid1Container);
            if ( subGrid1_Backcolorstyle == 0 )
            {
               /* None style subfile background logic. */
               subGrid1_Backstyle = 0;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
            }
            else if ( subGrid1_Backcolorstyle == 1 )
            {
               /* Uniform style subfile background logic. */
               subGrid1_Backstyle = 0;
               subGrid1_Backcolor = subGrid1_Allbackcolor;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Uniform";
               }
            }
            else if ( subGrid1_Backcolorstyle == 2 )
            {
               /* Header style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
               {
                  subGrid1_Linesclass = subGrid1_Class+"Odd";
               }
               subGrid1_Backcolor = (int)(0x0);
            }
            else if ( subGrid1_Backcolorstyle == 3 )
            {
               /* Report style subfile background logic. */
               subGrid1_Backstyle = 1;
               if ( ((int)((nGXsfl_74_idx) % (2))) == 0 )
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Even";
                  }
               }
               else
               {
                  subGrid1_Backcolor = (int)(0x0);
                  if ( StringUtil.StrCmp(subGrid1_Class, "") != 0 )
                  {
                     subGrid1_Linesclass = subGrid1_Class+"Odd";
                  }
               }
            }
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<tr ") ;
               context.WriteHtmlText( " class=\""+"PromptGrid"+"\" style=\""+""+"\"") ;
               context.WriteHtmlText( " gxrow=\""+sGXsfl_74_idx+"\">") ;
            }
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Static Bitmap Variable */
            edtavLinkselection_Link = "javascript:gx.popup.gxReturn(["+"'"+GXUtil.EncodeJSConstant( A32RecorridoDetalleID.ToString())+"'"+"]);";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLinkselection_Internalname, "Link", edtavLinkselection_Link, !bGXsfl_74_Refreshing);
            ClassString = "SelectionAttribute";
            StyleString = "";
            AV5LinkSelection_IsBlob = (bool)((String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection))&&String.IsNullOrEmpty(StringUtil.RTrim( AV17Linkselection_GXI)))||!String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)));
            sImgUrl = (String.IsNullOrEmpty(StringUtil.RTrim( AV5LinkSelection)) ? AV17Linkselection_GXI : context.PathToRelativeUrl( AV5LinkSelection));
            Grid1Row.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavLinkselection_Internalname,(String)sImgUrl,(String)edtavLinkselection_Link,(String)"",(String)"",context.GetTheme( ),(short)-1,(short)1,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"px",(short)0,(String)"px",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"WWActionColumn",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)AV5LinkSelection_IsBlob,(bool)false,context.GetImageSrcSet( sImgUrl)});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRecorridoDetalleID_Internalname,A32RecorridoDetalleID.ToString(),A32RecorridoDetalleID.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRecorridoDetalleID_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)74,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRecorridoDetallePaciente_Internalname,A21RecorridoDetallePaciente.ToString(),A21RecorridoDetallePaciente.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRecorridoDetallePaciente_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)74,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIDCoach_Internalname,A22IDCoach.ToString(),A22IDCoach.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIDCoach_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"WWColumn OptionalColumn",(String)"",(short)-1,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)74,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
            }
            if ( ( cmbTipoSenal.ItemCount == 0 ) && isAjaxCallMode( ) )
            {
               GXCCtl = "TIPOSENAL_" + sGXsfl_74_idx;
               cmbTipoSenal.Name = GXCCtl;
               cmbTipoSenal.WebTags = "";
               cmbTipoSenal.addItem("1", "Inicio", 0);
               cmbTipoSenal.addItem("2", "Progreso", 0);
               cmbTipoSenal.addItem("3", "Fin", 0);
               if ( cmbTipoSenal.ItemCount > 0 )
               {
                  A25TipoSenal = (short)(NumberUtil.Val( cmbTipoSenal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0))), "."));
                  n25TipoSenal = false;
               }
            }
            /* ComboBox */
            Grid1Row.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTipoSenal,(String)cmbTipoSenal_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0)),(short)1,(String)cmbTipoSenal_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,(short)0,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"DescriptionAttribute",(String)"WWColumn",(String)"",(String)"",(String)"",(bool)true});
            cmbTipoSenal.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoSenal_Internalname, "Values", (String)(cmbTipoSenal.ToJavascriptSource()), !bGXsfl_74_Refreshing);
            /* Subfile cell */
            if ( Grid1Container.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+"display:none;"+"\">") ;
            }
            /* Single line edit */
            ROClassString = "Attribute";
            Grid1Row.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRecorridoID_Internalname,A23RecorridoID.ToString(),A23RecorridoID.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRecorridoID_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)74,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
            send_integrity_lvl_hashes1K2( ) ;
            Grid1Container.AddRow(Grid1Row);
            nGXsfl_74_idx = (short)(((subGrid1_Islastpage==1)&&(nGXsfl_74_idx+1>subGrid1_Recordsperpage( )) ? 1 : nGXsfl_74_idx+1));
            sGXsfl_74_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_74_idx), 4, 0)), 4, "0");
            SubsflControlProps_742( ) ;
         }
         /* End function sendrow_742 */
      }

      protected void init_default_properties( )
      {
         lblLblrecorridodetalleidfilter_Internalname = "LBLRECORRIDODETALLEIDFILTER";
         edtavCrecorridodetalleid_Internalname = "vCRECORRIDODETALLEID";
         divRecorridodetalleidfiltercontainer_Internalname = "RECORRIDODETALLEIDFILTERCONTAINER";
         lblLblrecorridodetallepacientefilter_Internalname = "LBLRECORRIDODETALLEPACIENTEFILTER";
         edtavCrecorridodetallepaciente_Internalname = "vCRECORRIDODETALLEPACIENTE";
         divRecorridodetallepacientefiltercontainer_Internalname = "RECORRIDODETALLEPACIENTEFILTERCONTAINER";
         lblLblidcoachfilter_Internalname = "LBLIDCOACHFILTER";
         edtavCidcoach_Internalname = "vCIDCOACH";
         divIdcoachfiltercontainer_Internalname = "IDCOACHFILTERCONTAINER";
         lblLbltiposenalfilter_Internalname = "LBLTIPOSENALFILTER";
         cmbavCtiposenal_Internalname = "vCTIPOSENAL";
         divTiposenalfiltercontainer_Internalname = "TIPOSENALFILTERCONTAINER";
         lblLbltipoobstaculofilter_Internalname = "LBLTIPOOBSTACULOFILTER";
         cmbavCtipoobstaculo_Internalname = "vCTIPOOBSTACULO";
         divTipoobstaculofiltercontainer_Internalname = "TIPOOBSTACULOFILTERCONTAINER";
         lblLblincidentefilter_Internalname = "LBLINCIDENTEFILTER";
         cmbavCincidente_Internalname = "vCINCIDENTE";
         divIncidentefiltercontainer_Internalname = "INCIDENTEFILTERCONTAINER";
         divAdvancedcontainer_Internalname = "ADVANCEDCONTAINER";
         bttBtntoggle_Internalname = "BTNTOGGLE";
         edtavLinkselection_Internalname = "vLINKSELECTION";
         edtRecorridoDetalleID_Internalname = "RECORRIDODETALLEID";
         edtRecorridoDetallePaciente_Internalname = "RECORRIDODETALLEPACIENTE";
         edtIDCoach_Internalname = "IDCOACH";
         cmbTipoSenal_Internalname = "TIPOSENAL";
         edtRecorridoID_Internalname = "RECORRIDOID";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         divGridtable_Internalname = "GRIDTABLE";
         divMain_Internalname = "MAIN";
         Form.Internalname = "FORM";
         subGrid1_Internalname = "GRID1";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtRecorridoID_Jsonclick = "";
         cmbTipoSenal_Jsonclick = "";
         edtIDCoach_Jsonclick = "";
         edtRecorridoDetallePaciente_Jsonclick = "";
         edtRecorridoDetalleID_Jsonclick = "";
         subGrid1_Allowcollapsing = 0;
         subGrid1_Allowselection = 0;
         cmbTipoSenal.Link = "";
         edtavLinkselection_Link = "";
         subGrid1_Class = "PromptGrid";
         subGrid1_Backcolorstyle = 0;
         cmbavCincidente_Jsonclick = "";
         cmbavCincidente.Enabled = 1;
         cmbavCincidente.Visible = 1;
         cmbavCtipoobstaculo_Jsonclick = "";
         cmbavCtipoobstaculo.Enabled = 1;
         cmbavCtipoobstaculo.Visible = 1;
         cmbavCtiposenal_Jsonclick = "";
         cmbavCtiposenal.Enabled = 1;
         cmbavCtiposenal.Visible = 1;
         edtavCidcoach_Jsonclick = "";
         edtavCidcoach_Enabled = 1;
         edtavCidcoach_Visible = 1;
         edtavCrecorridodetallepaciente_Jsonclick = "";
         edtavCrecorridodetallepaciente_Enabled = 1;
         edtavCrecorridodetallepaciente_Visible = 1;
         edtavCrecorridodetalleid_Jsonclick = "";
         edtavCrecorridodetalleid_Enabled = 1;
         edtavCrecorridodetalleid_Visible = 1;
         divIncidentefiltercontainer_Class = "AdvancedContainerItem";
         divTipoobstaculofiltercontainer_Class = "AdvancedContainerItem";
         divTiposenalfiltercontainer_Class = "AdvancedContainerItem";
         divIdcoachfiltercontainer_Class = "AdvancedContainerItem";
         divRecorridodetallepacientefiltercontainer_Class = "AdvancedContainerItem";
         divRecorridodetalleidfiltercontainer_Class = "AdvancedContainerItem";
         bttBtntoggle_Class = "BtnToggle";
         divAdvancedcontainer_Class = "AdvancedContainerVisible";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Selection List Detalle";
         subGrid1_Rows = 10;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cRecorridoDetalleID',fld:'vCRECORRIDODETALLEID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cRecorridoDetallePaciente',fld:'vCRECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cIDCoach',fld:'vCIDCOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'cmbavCtiposenal'},{av:'AV9cTipoSenal',fld:'vCTIPOSENAL',pic:'ZZZ9',nv:0},{av:'cmbavCtipoobstaculo'},{av:'AV10cTipoObstaculo',fld:'vCTIPOOBSTACULO',pic:'ZZZ9',nv:0},{av:'cmbavCincidente'},{av:'AV11cIncidente',fld:'vCINCIDENTE',pic:'ZZZ9',nv:0},{av:'AV12pRecorridoID',fld:'vPRECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("'TOGGLE'","{handler:'E171K1',iparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}],oparms:[{av:'divAdvancedcontainer_Class',ctrl:'ADVANCEDCONTAINER',prop:'Class'},{ctrl:'BTNTOGGLE',prop:'Class'}]}");
         setEventMetadata("LBLRECORRIDODETALLEIDFILTER.CLICK","{handler:'E111K1',iparms:[{av:'divRecorridodetalleidfiltercontainer_Class',ctrl:'RECORRIDODETALLEIDFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divRecorridodetalleidfiltercontainer_Class',ctrl:'RECORRIDODETALLEIDFILTERCONTAINER',prop:'Class'},{av:'edtavCrecorridodetalleid_Visible',ctrl:'vCRECORRIDODETALLEID',prop:'Visible'}]}");
         setEventMetadata("LBLRECORRIDODETALLEPACIENTEFILTER.CLICK","{handler:'E121K1',iparms:[{av:'divRecorridodetallepacientefiltercontainer_Class',ctrl:'RECORRIDODETALLEPACIENTEFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divRecorridodetallepacientefiltercontainer_Class',ctrl:'RECORRIDODETALLEPACIENTEFILTERCONTAINER',prop:'Class'},{av:'edtavCrecorridodetallepaciente_Visible',ctrl:'vCRECORRIDODETALLEPACIENTE',prop:'Visible'}]}");
         setEventMetadata("LBLIDCOACHFILTER.CLICK","{handler:'E131K1',iparms:[{av:'divIdcoachfiltercontainer_Class',ctrl:'IDCOACHFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divIdcoachfiltercontainer_Class',ctrl:'IDCOACHFILTERCONTAINER',prop:'Class'},{av:'edtavCidcoach_Visible',ctrl:'vCIDCOACH',prop:'Visible'}]}");
         setEventMetadata("LBLTIPOSENALFILTER.CLICK","{handler:'E141K1',iparms:[{av:'divTiposenalfiltercontainer_Class',ctrl:'TIPOSENALFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divTiposenalfiltercontainer_Class',ctrl:'TIPOSENALFILTERCONTAINER',prop:'Class'},{av:'cmbavCtiposenal'}]}");
         setEventMetadata("LBLTIPOOBSTACULOFILTER.CLICK","{handler:'E151K1',iparms:[{av:'divTipoobstaculofiltercontainer_Class',ctrl:'TIPOOBSTACULOFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divTipoobstaculofiltercontainer_Class',ctrl:'TIPOOBSTACULOFILTERCONTAINER',prop:'Class'},{av:'cmbavCtipoobstaculo'}]}");
         setEventMetadata("LBLINCIDENTEFILTER.CLICK","{handler:'E161K1',iparms:[{av:'divIncidentefiltercontainer_Class',ctrl:'INCIDENTEFILTERCONTAINER',prop:'Class'}],oparms:[{av:'divIncidentefiltercontainer_Class',ctrl:'INCIDENTEFILTERCONTAINER',prop:'Class'},{av:'cmbavCincidente'}]}");
         setEventMetadata("ENTER","{handler:'E201K2',iparms:[{av:'A32RecorridoDetalleID',fld:'RECORRIDODETALLEID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{av:'AV13pRecorridoDetalleID',fld:'vPRECORRIDODETALLEID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}]}");
         setEventMetadata("GRID1_FIRSTPAGE","{handler:'subgrid1_firstpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cRecorridoDetalleID',fld:'vCRECORRIDODETALLEID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cRecorridoDetallePaciente',fld:'vCRECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cIDCoach',fld:'vCIDCOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'cmbavCtiposenal'},{av:'AV9cTipoSenal',fld:'vCTIPOSENAL',pic:'ZZZ9',nv:0},{av:'cmbavCtipoobstaculo'},{av:'AV10cTipoObstaculo',fld:'vCTIPOOBSTACULO',pic:'ZZZ9',nv:0},{av:'cmbavCincidente'},{av:'AV11cIncidente',fld:'vCINCIDENTE',pic:'ZZZ9',nv:0},{av:'AV12pRecorridoID',fld:'vPRECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("GRID1_PREVPAGE","{handler:'subgrid1_previouspage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cRecorridoDetalleID',fld:'vCRECORRIDODETALLEID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cRecorridoDetallePaciente',fld:'vCRECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cIDCoach',fld:'vCIDCOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'cmbavCtiposenal'},{av:'AV9cTipoSenal',fld:'vCTIPOSENAL',pic:'ZZZ9',nv:0},{av:'cmbavCtipoobstaculo'},{av:'AV10cTipoObstaculo',fld:'vCTIPOOBSTACULO',pic:'ZZZ9',nv:0},{av:'cmbavCincidente'},{av:'AV11cIncidente',fld:'vCINCIDENTE',pic:'ZZZ9',nv:0},{av:'AV12pRecorridoID',fld:'vPRECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("GRID1_NEXTPAGE","{handler:'subgrid1_nextpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cRecorridoDetalleID',fld:'vCRECORRIDODETALLEID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cRecorridoDetallePaciente',fld:'vCRECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cIDCoach',fld:'vCIDCOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'cmbavCtiposenal'},{av:'AV9cTipoSenal',fld:'vCTIPOSENAL',pic:'ZZZ9',nv:0},{av:'cmbavCtipoobstaculo'},{av:'AV10cTipoObstaculo',fld:'vCTIPOOBSTACULO',pic:'ZZZ9',nv:0},{av:'cmbavCincidente'},{av:'AV11cIncidente',fld:'vCINCIDENTE',pic:'ZZZ9',nv:0},{av:'AV12pRecorridoID',fld:'vPRECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("GRID1_LASTPAGE","{handler:'subgrid1_lastpage',iparms:[{av:'GRID1_nFirstRecordOnPage',nv:0},{av:'GRID1_nEOF',nv:0},{av:'subGrid1_Rows',nv:0},{av:'AV6cRecorridoDetalleID',fld:'vCRECORRIDODETALLEID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV7cRecorridoDetallePaciente',fld:'vCRECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV8cIDCoach',fld:'vCIDCOACH',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'cmbavCtiposenal'},{av:'AV9cTipoSenal',fld:'vCTIPOSENAL',pic:'ZZZ9',nv:0},{av:'cmbavCtipoobstaculo'},{av:'AV10cTipoObstaculo',fld:'vCTIPOOBSTACULO',pic:'ZZZ9',nv:0},{av:'cmbavCincidente'},{av:'AV11cIncidente',fld:'vCINCIDENTE',pic:'ZZZ9',nv:0},{av:'AV12pRecorridoID',fld:'vPRECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV12pRecorridoID = (Guid)(Guid.Empty);
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV6cRecorridoDetalleID = (Guid)(Guid.Empty);
         AV7cRecorridoDetallePaciente = (Guid)(Guid.Empty);
         AV8cIDCoach = (Guid)(Guid.Empty);
         GXKey = "";
         AV13pRecorridoDetalleID = (Guid)(Guid.Empty);
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblLblrecorridodetalleidfilter_Jsonclick = "";
         TempTags = "";
         lblLblrecorridodetallepacientefilter_Jsonclick = "";
         lblLblidcoachfilter_Jsonclick = "";
         lblLbltiposenalfilter_Jsonclick = "";
         lblLbltipoobstaculofilter_Jsonclick = "";
         lblLblincidentefilter_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         bttBtntoggle_Jsonclick = "";
         Grid1Container = new GXWebGrid( context);
         sStyleString = "";
         subGrid1_Linesclass = "";
         Grid1Column = new GXWebColumn();
         AV5LinkSelection = "";
         A32RecorridoDetalleID = (Guid)(Guid.Empty);
         A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         A22IDCoach = (Guid)(Guid.Empty);
         A23RecorridoID = (Guid)(Guid.Empty);
         bttBtn_cancel_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17Linkselection_GXI = "";
         GXCCtl = "";
         scmdbuf = "";
         H001K2_A29Incidente = new short[1] ;
         H001K2_n29Incidente = new bool[] {false} ;
         H001K2_A28TipoObstaculo = new short[1] ;
         H001K2_n28TipoObstaculo = new bool[] {false} ;
         H001K2_A23RecorridoID = new Guid[] {Guid.Empty} ;
         H001K2_A25TipoSenal = new short[1] ;
         H001K2_n25TipoSenal = new bool[] {false} ;
         H001K2_A22IDCoach = new Guid[] {Guid.Empty} ;
         H001K2_n22IDCoach = new bool[] {false} ;
         H001K2_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         H001K2_n21RecorridoDetallePaciente = new bool[] {false} ;
         H001K2_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         H001K3_AGRID1_nRecordCount = new long[1] ;
         AV14ADVANCED_LABEL_TEMPLATE = "";
         Grid1Row = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sImgUrl = "";
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gx0051__default(),
            new Object[][] {
                new Object[] {
               H001K2_A29Incidente, H001K2_n29Incidente, H001K2_A28TipoObstaculo, H001K2_n28TipoObstaculo, H001K2_A23RecorridoID, H001K2_A25TipoSenal, H001K2_n25TipoSenal, H001K2_A22IDCoach, H001K2_n22IDCoach, H001K2_A21RecorridoDetallePaciente,
               H001K2_n21RecorridoDetallePaciente, H001K2_A32RecorridoDetalleID
               }
               , new Object[] {
               H001K3_AGRID1_nRecordCount
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_74 ;
      private short nGXsfl_74_idx=1 ;
      private short GRID1_nEOF ;
      private short AV9cTipoSenal ;
      private short AV10cTipoObstaculo ;
      private short AV11cIncidente ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid1_Backcolorstyle ;
      private short subGrid1_Titlebackstyle ;
      private short A25TipoSenal ;
      private short subGrid1_Allowselection ;
      private short subGrid1_Allowhovering ;
      private short subGrid1_Allowcollapsing ;
      private short subGrid1_Collapsed ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short A28TipoObstaculo ;
      private short A29Incidente ;
      private short nGXWrapped ;
      private short subGrid1_Backstyle ;
      private int subGrid1_Rows ;
      private int edtavCrecorridodetalleid_Visible ;
      private int edtavCrecorridodetalleid_Enabled ;
      private int edtavCrecorridodetallepaciente_Visible ;
      private int edtavCrecorridodetallepaciente_Enabled ;
      private int edtavCidcoach_Visible ;
      private int edtavCidcoach_Enabled ;
      private int subGrid1_Titlebackcolor ;
      private int subGrid1_Allbackcolor ;
      private int subGrid1_Selectioncolor ;
      private int subGrid1_Hoveringcolor ;
      private int subGrid1_Islastpage ;
      private int GXPagingFrom2 ;
      private int GXPagingTo2 ;
      private int idxLst ;
      private int subGrid1_Backcolor ;
      private long GRID1_nFirstRecordOnPage ;
      private long GRID1_nCurrentRecord ;
      private long GRID1_nRecordCount ;
      private String divAdvancedcontainer_Class ;
      private String bttBtntoggle_Class ;
      private String divRecorridodetalleidfiltercontainer_Class ;
      private String divRecorridodetallepacientefiltercontainer_Class ;
      private String divIdcoachfiltercontainer_Class ;
      private String divTiposenalfiltercontainer_Class ;
      private String divTipoobstaculofiltercontainer_Class ;
      private String divIncidentefiltercontainer_Class ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_74_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMain_Internalname ;
      private String divAdvancedcontainer_Internalname ;
      private String divRecorridodetalleidfiltercontainer_Internalname ;
      private String lblLblrecorridodetalleidfilter_Internalname ;
      private String lblLblrecorridodetalleidfilter_Jsonclick ;
      private String edtavCrecorridodetalleid_Internalname ;
      private String TempTags ;
      private String edtavCrecorridodetalleid_Jsonclick ;
      private String divRecorridodetallepacientefiltercontainer_Internalname ;
      private String lblLblrecorridodetallepacientefilter_Internalname ;
      private String lblLblrecorridodetallepacientefilter_Jsonclick ;
      private String edtavCrecorridodetallepaciente_Internalname ;
      private String edtavCrecorridodetallepaciente_Jsonclick ;
      private String divIdcoachfiltercontainer_Internalname ;
      private String lblLblidcoachfilter_Internalname ;
      private String lblLblidcoachfilter_Jsonclick ;
      private String edtavCidcoach_Internalname ;
      private String edtavCidcoach_Jsonclick ;
      private String divTiposenalfiltercontainer_Internalname ;
      private String lblLbltiposenalfilter_Internalname ;
      private String lblLbltiposenalfilter_Jsonclick ;
      private String cmbavCtiposenal_Internalname ;
      private String cmbavCtiposenal_Jsonclick ;
      private String divTipoobstaculofiltercontainer_Internalname ;
      private String lblLbltipoobstaculofilter_Internalname ;
      private String lblLbltipoobstaculofilter_Jsonclick ;
      private String cmbavCtipoobstaculo_Internalname ;
      private String cmbavCtipoobstaculo_Jsonclick ;
      private String divIncidentefiltercontainer_Internalname ;
      private String lblLblincidentefilter_Internalname ;
      private String lblLblincidentefilter_Jsonclick ;
      private String cmbavCincidente_Internalname ;
      private String cmbavCincidente_Jsonclick ;
      private String divGridtable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtntoggle_Internalname ;
      private String bttBtntoggle_Jsonclick ;
      private String sStyleString ;
      private String subGrid1_Internalname ;
      private String subGrid1_Class ;
      private String subGrid1_Linesclass ;
      private String edtavLinkselection_Link ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavLinkselection_Internalname ;
      private String edtRecorridoDetalleID_Internalname ;
      private String edtRecorridoDetallePaciente_Internalname ;
      private String edtIDCoach_Internalname ;
      private String cmbTipoSenal_Internalname ;
      private String edtRecorridoID_Internalname ;
      private String GXCCtl ;
      private String scmdbuf ;
      private String AV14ADVANCED_LABEL_TEMPLATE ;
      private String sGXsfl_74_fel_idx="0001" ;
      private String sImgUrl ;
      private String ROClassString ;
      private String edtRecorridoDetalleID_Jsonclick ;
      private String edtRecorridoDetallePaciente_Jsonclick ;
      private String edtIDCoach_Jsonclick ;
      private String cmbTipoSenal_Jsonclick ;
      private String edtRecorridoID_Jsonclick ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_74_Refreshing=false ;
      private bool n21RecorridoDetallePaciente ;
      private bool n22IDCoach ;
      private bool n25TipoSenal ;
      private bool n29Incidente ;
      private bool n28TipoObstaculo ;
      private bool returnInSub ;
      private bool AV5LinkSelection_IsBlob ;
      private String AV17Linkselection_GXI ;
      private String AV5LinkSelection ;
      private Guid AV12pRecorridoID ;
      private Guid wcpOAV12pRecorridoID ;
      private Guid AV6cRecorridoDetalleID ;
      private Guid AV7cRecorridoDetallePaciente ;
      private Guid AV8cIDCoach ;
      private Guid AV13pRecorridoDetalleID ;
      private Guid A32RecorridoDetalleID ;
      private Guid A21RecorridoDetallePaciente ;
      private Guid A22IDCoach ;
      private Guid A23RecorridoID ;
      private GXWebGrid Grid1Container ;
      private GXWebRow Grid1Row ;
      private GXWebColumn Grid1Column ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbavCtiposenal ;
      private GXCombobox cmbavCtipoobstaculo ;
      private GXCombobox cmbavCincidente ;
      private GXCombobox cmbTipoSenal ;
      private IDataStoreProvider pr_default ;
      private short[] H001K2_A29Incidente ;
      private bool[] H001K2_n29Incidente ;
      private short[] H001K2_A28TipoObstaculo ;
      private bool[] H001K2_n28TipoObstaculo ;
      private Guid[] H001K2_A23RecorridoID ;
      private short[] H001K2_A25TipoSenal ;
      private bool[] H001K2_n25TipoSenal ;
      private Guid[] H001K2_A22IDCoach ;
      private bool[] H001K2_n22IDCoach ;
      private Guid[] H001K2_A21RecorridoDetallePaciente ;
      private bool[] H001K2_n21RecorridoDetallePaciente ;
      private Guid[] H001K2_A32RecorridoDetalleID ;
      private long[] H001K3_AGRID1_nRecordCount ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private Guid aP1_pRecorridoDetalleID ;
      private GXWebForm Form ;
   }

   public class gx0051__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_H001K2( IGxContext context ,
                                             Guid AV7cRecorridoDetallePaciente ,
                                             Guid AV8cIDCoach ,
                                             short AV9cTipoSenal ,
                                             short AV10cTipoObstaculo ,
                                             short AV11cIncidente ,
                                             Guid A21RecorridoDetallePaciente ,
                                             Guid A22IDCoach ,
                                             short A25TipoSenal ,
                                             short A28TipoObstaculo ,
                                             short A29Incidente ,
                                             Guid AV12pRecorridoID ,
                                             Guid AV6cRecorridoDetalleID ,
                                             Guid A23RecorridoID )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int1 ;
         GXv_int1 = new short [10] ;
         Object[] GXv_Object2 ;
         GXv_Object2 = new Object [2] ;
         String sSelectString ;
         String sFromString ;
         String sOrderString ;
         sSelectString = " [Incidente], [TipoObstaculo], [RecorridoID], [TipoSenal], [IDCoach], [RecorridoDetallePaciente], [RecorridoDetalleID]";
         sFromString = " FROM [RecorridoDetalle] WITH (NOLOCK)";
         sOrderString = "";
         sWhereString = sWhereString + " WHERE ([RecorridoID] = @AV12pRecorridoID and [RecorridoDetalleID] >= @AV6cRecorridoDetalleID)";
         if ( ! (Guid.Empty==AV7cRecorridoDetallePaciente) )
         {
            sWhereString = sWhereString + " and ([RecorridoDetallePaciente] >= @AV7cRecorridoDetallePaciente)";
         }
         else
         {
            GXv_int1[2] = 1;
         }
         if ( ! (Guid.Empty==AV8cIDCoach) )
         {
            sWhereString = sWhereString + " and ([IDCoach] >= @AV8cIDCoach)";
         }
         else
         {
            GXv_int1[3] = 1;
         }
         if ( ! (0==AV9cTipoSenal) )
         {
            sWhereString = sWhereString + " and ([TipoSenal] >= @AV9cTipoSenal)";
         }
         else
         {
            GXv_int1[4] = 1;
         }
         if ( ! (0==AV10cTipoObstaculo) )
         {
            sWhereString = sWhereString + " and ([TipoObstaculo] >= @AV10cTipoObstaculo)";
         }
         else
         {
            GXv_int1[5] = 1;
         }
         if ( ! (0==AV11cIncidente) )
         {
            sWhereString = sWhereString + " and ([Incidente] >= @AV11cIncidente)";
         }
         else
         {
            GXv_int1[6] = 1;
         }
         sOrderString = sOrderString + " ORDER BY [RecorridoID], [RecorridoDetalleID]";
         scmdbuf = "SELECT " + sSelectString + sFromString + sWhereString + "" + sOrderString + " OFFSET " + "@GXPagingFrom2" + " ROWS FETCH NEXT CAST((SELECT CASE WHEN " + "@GXPagingTo2" + " > 0 THEN " + "@GXPagingTo2" + " ELSE 1e9 END) AS INTEGER) ROWS ONLY";
         GXv_Object2[0] = scmdbuf;
         GXv_Object2[1] = GXv_int1;
         return GXv_Object2 ;
      }

      protected Object[] conditional_H001K3( IGxContext context ,
                                             Guid AV7cRecorridoDetallePaciente ,
                                             Guid AV8cIDCoach ,
                                             short AV9cTipoSenal ,
                                             short AV10cTipoObstaculo ,
                                             short AV11cIncidente ,
                                             Guid A21RecorridoDetallePaciente ,
                                             Guid A22IDCoach ,
                                             short A25TipoSenal ,
                                             short A28TipoObstaculo ,
                                             short A29Incidente ,
                                             Guid AV12pRecorridoID ,
                                             Guid AV6cRecorridoDetalleID ,
                                             Guid A23RecorridoID )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int3 ;
         GXv_int3 = new short [7] ;
         Object[] GXv_Object4 ;
         GXv_Object4 = new Object [2] ;
         scmdbuf = "SELECT COUNT(*) FROM [RecorridoDetalle] WITH (NOLOCK)";
         scmdbuf = scmdbuf + " WHERE ([RecorridoID] = @AV12pRecorridoID and [RecorridoDetalleID] >= @AV6cRecorridoDetalleID)";
         if ( ! (Guid.Empty==AV7cRecorridoDetallePaciente) )
         {
            sWhereString = sWhereString + " and ([RecorridoDetallePaciente] >= @AV7cRecorridoDetallePaciente)";
         }
         else
         {
            GXv_int3[2] = 1;
         }
         if ( ! (Guid.Empty==AV8cIDCoach) )
         {
            sWhereString = sWhereString + " and ([IDCoach] >= @AV8cIDCoach)";
         }
         else
         {
            GXv_int3[3] = 1;
         }
         if ( ! (0==AV9cTipoSenal) )
         {
            sWhereString = sWhereString + " and ([TipoSenal] >= @AV9cTipoSenal)";
         }
         else
         {
            GXv_int3[4] = 1;
         }
         if ( ! (0==AV10cTipoObstaculo) )
         {
            sWhereString = sWhereString + " and ([TipoObstaculo] >= @AV10cTipoObstaculo)";
         }
         else
         {
            GXv_int3[5] = 1;
         }
         if ( ! (0==AV11cIncidente) )
         {
            sWhereString = sWhereString + " and ([Incidente] >= @AV11cIncidente)";
         }
         else
         {
            GXv_int3[6] = 1;
         }
         scmdbuf = scmdbuf + sWhereString;
         scmdbuf = scmdbuf + "";
         GXv_Object4[0] = scmdbuf;
         GXv_Object4[1] = GXv_int3;
         return GXv_Object4 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_H001K2(context, (Guid)dynConstraints[0] , (Guid)dynConstraints[1] , (short)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (Guid)dynConstraints[5] , (Guid)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (Guid)dynConstraints[10] , (Guid)dynConstraints[11] , (Guid)dynConstraints[12] );
               case 1 :
                     return conditional_H001K3(context, (Guid)dynConstraints[0] , (Guid)dynConstraints[1] , (short)dynConstraints[2] , (short)dynConstraints[3] , (short)dynConstraints[4] , (Guid)dynConstraints[5] , (Guid)dynConstraints[6] , (short)dynConstraints[7] , (short)dynConstraints[8] , (short)dynConstraints[9] , (Guid)dynConstraints[10] , (Guid)dynConstraints[11] , (Guid)dynConstraints[12] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
         ,new ForEachCursor(def[1])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH001K2 ;
          prmH001K2 = new Object[] {
          new Object[] {"@AV12pRecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV6cRecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV7cRecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV8cIDCoach",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV9cTipoSenal",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV10cTipoObstaculo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV11cIncidente",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@GXPagingFrom2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0} ,
          new Object[] {"@GXPagingTo2",SqlDbType.Int,9,0}
          } ;
          Object[] prmH001K3 ;
          prmH001K3 = new Object[] {
          new Object[] {"@AV12pRecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV6cRecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV7cRecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV8cIDCoach",SqlDbType.UniqueIdentifier,4,0} ,
          new Object[] {"@AV9cTipoSenal",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV10cTipoObstaculo",SqlDbType.SmallInt,4,0} ,
          new Object[] {"@AV11cIncidente",SqlDbType.SmallInt,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H001K2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001K2,11,0,false,false )
             ,new CursorDef("H001K3", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH001K3,1,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((short[]) buf[0])[0] = rslt.getShort(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((short[]) buf[2])[0] = rslt.getShort(2) ;
                ((bool[]) buf[3])[0] = rslt.wasNull(2);
                ((Guid[]) buf[4])[0] = rslt.getGuid(3) ;
                ((short[]) buf[5])[0] = rslt.getShort(4) ;
                ((bool[]) buf[6])[0] = rslt.wasNull(4);
                ((Guid[]) buf[7])[0] = rslt.getGuid(5) ;
                ((bool[]) buf[8])[0] = rslt.wasNull(5);
                ((Guid[]) buf[9])[0] = rslt.getGuid(6) ;
                ((bool[]) buf[10])[0] = rslt.wasNull(6);
                ((Guid[]) buf[11])[0] = rslt.getGuid(7) ;
                return;
             case 1 :
                ((long[]) buf[0])[0] = rslt.getLong(1) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[10]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[11]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[12]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[13]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[14]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[15]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[16]);
                }
                if ( (short)parms[7] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[17]);
                }
                if ( (short)parms[8] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[18]);
                }
                if ( (short)parms[9] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (int)parms[19]);
                }
                return;
             case 1 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[7]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[8]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[9]);
                }
                if ( (short)parms[3] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (Guid)parms[10]);
                }
                if ( (short)parms[4] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[11]);
                }
                if ( (short)parms[5] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[12]);
                }
                if ( (short)parms[6] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (short)parms[13]);
                }
                return;
       }
    }

 }

}
