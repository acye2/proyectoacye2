/*
               File: Persona_BC
        Description: Persona
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:45.98
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class persona_bc : GXHttpHandler, IGxSilentTrn
   {
      public persona_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public persona_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow011( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey011( ) ;
         standaloneModal( ) ;
         AddRow011( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E11012 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z1PersonaID = (Guid)(A1PersonaID);
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_010( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls011( ) ;
            }
            else
            {
               CheckExtendedTable011( ) ;
               if ( AnyError == 0 )
               {
               }
               CloseExtendedTableCursors011( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void E12012( )
      {
         /* Start Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV16StandardActivityType = "Insert";
            AV17UserActivityType = "";
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV16StandardActivityType = "Update";
            AV17UserActivityType = "";
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV16StandardActivityType = "Delete";
            AV17UserActivityType = "";
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            AV16StandardActivityType = "Display";
            AV17UserActivityType = "";
         }
      }

      protected void E11012( )
      {
         /* After Trn Routine */
      }

      protected void ZM011( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z2PersonaPNombre = A2PersonaPNombre;
            Z3PersonaSNombre = A3PersonaSNombre;
            Z4PersonaPApellido = A4PersonaPApellido;
            Z5PersonaSApellido = A5PersonaSApellido;
            Z6PersonaSexo = A6PersonaSexo;
            Z7PersonaDPI = A7PersonaDPI;
            Z8PersonaTipo = A8PersonaTipo;
            Z9PersonaFechaNacimiento = A9PersonaFechaNacimiento;
            Z10PersonaPacienteOrigenSordera = A10PersonaPacienteOrigenSordera;
            Z11PersonaEMail = A11PersonaEMail;
         }
         if ( GX_JID == -6 )
         {
            Z1PersonaID = (Guid)(A1PersonaID);
            Z2PersonaPNombre = A2PersonaPNombre;
            Z3PersonaSNombre = A3PersonaSNombre;
            Z4PersonaPApellido = A4PersonaPApellido;
            Z5PersonaSApellido = A5PersonaSApellido;
            Z6PersonaSexo = A6PersonaSexo;
            Z7PersonaDPI = A7PersonaDPI;
            Z8PersonaTipo = A8PersonaTipo;
            Z9PersonaFechaNacimiento = A9PersonaFechaNacimiento;
            Z10PersonaPacienteOrigenSordera = A10PersonaPacienteOrigenSordera;
            Z11PersonaEMail = A11PersonaEMail;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A1PersonaID) )
         {
            A1PersonaID = (Guid)(Guid.NewGuid( ));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load011( )
      {
         /* Using cursor BC00014 */
         pr_default.execute(2, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound1 = 1;
            A2PersonaPNombre = BC00014_A2PersonaPNombre[0];
            A3PersonaSNombre = BC00014_A3PersonaSNombre[0];
            n3PersonaSNombre = BC00014_n3PersonaSNombre[0];
            A4PersonaPApellido = BC00014_A4PersonaPApellido[0];
            A5PersonaSApellido = BC00014_A5PersonaSApellido[0];
            n5PersonaSApellido = BC00014_n5PersonaSApellido[0];
            A6PersonaSexo = BC00014_A6PersonaSexo[0];
            n6PersonaSexo = BC00014_n6PersonaSexo[0];
            A7PersonaDPI = BC00014_A7PersonaDPI[0];
            n7PersonaDPI = BC00014_n7PersonaDPI[0];
            A8PersonaTipo = BC00014_A8PersonaTipo[0];
            A9PersonaFechaNacimiento = BC00014_A9PersonaFechaNacimiento[0];
            n9PersonaFechaNacimiento = BC00014_n9PersonaFechaNacimiento[0];
            A10PersonaPacienteOrigenSordera = BC00014_A10PersonaPacienteOrigenSordera[0];
            n10PersonaPacienteOrigenSordera = BC00014_n10PersonaPacienteOrigenSordera[0];
            A11PersonaEMail = BC00014_A11PersonaEMail[0];
            n11PersonaEMail = BC00014_n11PersonaEMail[0];
            ZM011( -6) ;
         }
         pr_default.close(2);
         OnLoadActions011( ) ;
      }

      protected void OnLoadActions011( )
      {
      }

      protected void CheckExtendedTable011( )
      {
         standaloneModal( ) ;
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A2PersonaPNombre)) )
         {
            GX_msglist.addItem("PNombre es obligatorio", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A9PersonaFechaNacimiento) || ( A9PersonaFechaNacimiento >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Persona Fecha Nacimiento fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( GxRegex.IsMatch(A11PersonaEMail,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A11PersonaEMail)) ) )
         {
            GX_msglist.addItem("El valor de Persona EMail no coincide con el patr�n especificado", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors011( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey011( )
      {
         /* Using cursor BC00015 */
         pr_default.execute(3, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound1 = 1;
         }
         else
         {
            RcdFound1 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00013 */
         pr_default.execute(1, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM011( 6) ;
            RcdFound1 = 1;
            A1PersonaID = (Guid)((Guid)(BC00013_A1PersonaID[0]));
            A2PersonaPNombre = BC00013_A2PersonaPNombre[0];
            A3PersonaSNombre = BC00013_A3PersonaSNombre[0];
            n3PersonaSNombre = BC00013_n3PersonaSNombre[0];
            A4PersonaPApellido = BC00013_A4PersonaPApellido[0];
            A5PersonaSApellido = BC00013_A5PersonaSApellido[0];
            n5PersonaSApellido = BC00013_n5PersonaSApellido[0];
            A6PersonaSexo = BC00013_A6PersonaSexo[0];
            n6PersonaSexo = BC00013_n6PersonaSexo[0];
            A7PersonaDPI = BC00013_A7PersonaDPI[0];
            n7PersonaDPI = BC00013_n7PersonaDPI[0];
            A8PersonaTipo = BC00013_A8PersonaTipo[0];
            A9PersonaFechaNacimiento = BC00013_A9PersonaFechaNacimiento[0];
            n9PersonaFechaNacimiento = BC00013_n9PersonaFechaNacimiento[0];
            A10PersonaPacienteOrigenSordera = BC00013_A10PersonaPacienteOrigenSordera[0];
            n10PersonaPacienteOrigenSordera = BC00013_n10PersonaPacienteOrigenSordera[0];
            A11PersonaEMail = BC00013_A11PersonaEMail[0];
            n11PersonaEMail = BC00013_n11PersonaEMail[0];
            Z1PersonaID = (Guid)(A1PersonaID);
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load011( ) ;
            if ( AnyError == 1 )
            {
               RcdFound1 = 0;
               InitializeNonKey011( ) ;
            }
            Gx_mode = sMode1;
         }
         else
         {
            RcdFound1 = 0;
            InitializeNonKey011( ) ;
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode1;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey011( ) ;
         if ( RcdFound1 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_010( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00012 */
            pr_default.execute(0, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Persona"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2PersonaPNombre, BC00012_A2PersonaPNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z3PersonaSNombre, BC00012_A3PersonaSNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z4PersonaPApellido, BC00012_A4PersonaPApellido[0]) != 0 ) || ( StringUtil.StrCmp(Z5PersonaSApellido, BC00012_A5PersonaSApellido[0]) != 0 ) || ( StringUtil.StrCmp(Z6PersonaSexo, BC00012_A6PersonaSexo[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z7PersonaDPI, BC00012_A7PersonaDPI[0]) != 0 ) || ( Z8PersonaTipo != BC00012_A8PersonaTipo[0] ) || ( Z9PersonaFechaNacimiento != BC00012_A9PersonaFechaNacimiento[0] ) || ( Z10PersonaPacienteOrigenSordera != BC00012_A10PersonaPacienteOrigenSordera[0] ) || ( StringUtil.StrCmp(Z11PersonaEMail, BC00012_A11PersonaEMail[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Persona"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert011( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM011( 0) ;
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00016 */
                     pr_default.execute(4, new Object[] {A2PersonaPNombre, n3PersonaSNombre, A3PersonaSNombre, A4PersonaPApellido, n5PersonaSApellido, A5PersonaSApellido, n6PersonaSexo, A6PersonaSexo, n7PersonaDPI, A7PersonaDPI, A8PersonaTipo, n9PersonaFechaNacimiento, A9PersonaFechaNacimiento, n10PersonaPacienteOrigenSordera, A10PersonaPacienteOrigenSordera, n11PersonaEMail, A11PersonaEMail, A1PersonaID});
                     pr_default.close(4);
                     dsDefault.SmartCacheProvider.SetUpdated("Persona") ;
                     if ( (pr_default.getStatus(4) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load011( ) ;
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void Update011( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00017 */
                     pr_default.execute(5, new Object[] {A2PersonaPNombre, n3PersonaSNombre, A3PersonaSNombre, A4PersonaPApellido, n5PersonaSApellido, A5PersonaSApellido, n6PersonaSexo, A6PersonaSexo, n7PersonaDPI, A7PersonaDPI, A8PersonaTipo, n9PersonaFechaNacimiento, A9PersonaFechaNacimiento, n10PersonaPacienteOrigenSordera, A10PersonaPacienteOrigenSordera, n11PersonaEMail, A11PersonaEMail, A1PersonaID});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Persona") ;
                     if ( (pr_default.getStatus(5) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Persona"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate011( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void DeferredUpdate011( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls011( ) ;
            AfterConfirm011( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete011( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00018 */
                  pr_default.execute(6, new Object[] {A1PersonaID});
                  pr_default.close(6);
                  dsDefault.SmartCacheProvider.SetUpdated("Persona") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode1 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel011( ) ;
         Gx_mode = sMode1;
      }

      protected void OnDeleteControls011( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
         if ( AnyError == 0 )
         {
            /* Using cursor BC00019 */
            pr_default.execute(7, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(7) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Emergencia Bitacora"+" ("+"Emergencia Coach ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(7);
            /* Using cursor BC000110 */
            pr_default.execute(8, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(8) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Emergencia Bitacora"+" ("+"Emergencia Paciente ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(8);
            /* Using cursor BC000111 */
            pr_default.execute(9, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Obstaculo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor BC000112 */
            pr_default.execute(10, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Detalle"+" ("+"Recorrido Detalle Coach ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor BC000113 */
            pr_default.execute(11, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Detalle"+" ("+"Recorrido Paciente ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC000114 */
            pr_default.execute(12, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Coach Paciente"+" ("+"Coach Paciente Paciente ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor BC000115 */
            pr_default.execute(13, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Coach Paciente"+" ("+"Coach Paciente Coach ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
         }
      }

      protected void EndLevel011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete011( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart011( )
      {
         /* Scan By routine */
         /* Using cursor BC000116 */
         pr_default.execute(14, new Object[] {A1PersonaID});
         RcdFound1 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound1 = 1;
            A1PersonaID = (Guid)((Guid)(BC000116_A1PersonaID[0]));
            A2PersonaPNombre = BC000116_A2PersonaPNombre[0];
            A3PersonaSNombre = BC000116_A3PersonaSNombre[0];
            n3PersonaSNombre = BC000116_n3PersonaSNombre[0];
            A4PersonaPApellido = BC000116_A4PersonaPApellido[0];
            A5PersonaSApellido = BC000116_A5PersonaSApellido[0];
            n5PersonaSApellido = BC000116_n5PersonaSApellido[0];
            A6PersonaSexo = BC000116_A6PersonaSexo[0];
            n6PersonaSexo = BC000116_n6PersonaSexo[0];
            A7PersonaDPI = BC000116_A7PersonaDPI[0];
            n7PersonaDPI = BC000116_n7PersonaDPI[0];
            A8PersonaTipo = BC000116_A8PersonaTipo[0];
            A9PersonaFechaNacimiento = BC000116_A9PersonaFechaNacimiento[0];
            n9PersonaFechaNacimiento = BC000116_n9PersonaFechaNacimiento[0];
            A10PersonaPacienteOrigenSordera = BC000116_A10PersonaPacienteOrigenSordera[0];
            n10PersonaPacienteOrigenSordera = BC000116_n10PersonaPacienteOrigenSordera[0];
            A11PersonaEMail = BC000116_A11PersonaEMail[0];
            n11PersonaEMail = BC000116_n11PersonaEMail[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext011( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound1 = 0;
         ScanKeyLoad011( ) ;
      }

      protected void ScanKeyLoad011( )
      {
         sMode1 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound1 = 1;
            A1PersonaID = (Guid)((Guid)(BC000116_A1PersonaID[0]));
            A2PersonaPNombre = BC000116_A2PersonaPNombre[0];
            A3PersonaSNombre = BC000116_A3PersonaSNombre[0];
            n3PersonaSNombre = BC000116_n3PersonaSNombre[0];
            A4PersonaPApellido = BC000116_A4PersonaPApellido[0];
            A5PersonaSApellido = BC000116_A5PersonaSApellido[0];
            n5PersonaSApellido = BC000116_n5PersonaSApellido[0];
            A6PersonaSexo = BC000116_A6PersonaSexo[0];
            n6PersonaSexo = BC000116_n6PersonaSexo[0];
            A7PersonaDPI = BC000116_A7PersonaDPI[0];
            n7PersonaDPI = BC000116_n7PersonaDPI[0];
            A8PersonaTipo = BC000116_A8PersonaTipo[0];
            A9PersonaFechaNacimiento = BC000116_A9PersonaFechaNacimiento[0];
            n9PersonaFechaNacimiento = BC000116_n9PersonaFechaNacimiento[0];
            A10PersonaPacienteOrigenSordera = BC000116_A10PersonaPacienteOrigenSordera[0];
            n10PersonaPacienteOrigenSordera = BC000116_n10PersonaPacienteOrigenSordera[0];
            A11PersonaEMail = BC000116_A11PersonaEMail[0];
            n11PersonaEMail = BC000116_n11PersonaEMail[0];
         }
         Gx_mode = sMode1;
      }

      protected void ScanKeyEnd011( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm011( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert011( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate011( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete011( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete011( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate011( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes011( )
      {
      }

      protected void send_integrity_lvl_hashes011( )
      {
      }

      protected void AddRow011( )
      {
         VarsToRow1( bcPersona) ;
      }

      protected void ReadRow011( )
      {
         RowToVars1( bcPersona, 1) ;
      }

      protected void InitializeNonKey011( )
      {
         A2PersonaPNombre = "";
         A3PersonaSNombre = "";
         n3PersonaSNombre = false;
         A4PersonaPApellido = "";
         A5PersonaSApellido = "";
         n5PersonaSApellido = false;
         A6PersonaSexo = "";
         n6PersonaSexo = false;
         A7PersonaDPI = "";
         n7PersonaDPI = false;
         A8PersonaTipo = 0;
         A9PersonaFechaNacimiento = DateTime.MinValue;
         n9PersonaFechaNacimiento = false;
         A10PersonaPacienteOrigenSordera = 0;
         n10PersonaPacienteOrigenSordera = false;
         A11PersonaEMail = "";
         n11PersonaEMail = false;
         Z2PersonaPNombre = "";
         Z3PersonaSNombre = "";
         Z4PersonaPApellido = "";
         Z5PersonaSApellido = "";
         Z6PersonaSexo = "";
         Z7PersonaDPI = "";
         Z8PersonaTipo = 0;
         Z9PersonaFechaNacimiento = DateTime.MinValue;
         Z10PersonaPacienteOrigenSordera = 0;
         Z11PersonaEMail = "";
      }

      protected void InitAll011( )
      {
         A1PersonaID = (Guid)(Guid.NewGuid( ));
         InitializeNonKey011( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow1( SdtPersona obj1 )
      {
         obj1.gxTpr_Mode = Gx_mode;
         obj1.gxTpr_Personapnombre = A2PersonaPNombre;
         obj1.gxTpr_Personasnombre = A3PersonaSNombre;
         obj1.gxTpr_Personapapellido = A4PersonaPApellido;
         obj1.gxTpr_Personasapellido = A5PersonaSApellido;
         obj1.gxTpr_Personasexo = A6PersonaSexo;
         obj1.gxTpr_Personadpi = A7PersonaDPI;
         obj1.gxTpr_Personatipo = A8PersonaTipo;
         obj1.gxTpr_Personafechanacimiento = A9PersonaFechaNacimiento;
         obj1.gxTpr_Personapacienteorigensordera = A10PersonaPacienteOrigenSordera;
         obj1.gxTpr_Personaemail = A11PersonaEMail;
         obj1.gxTpr_Personaid = (Guid)(A1PersonaID);
         obj1.gxTpr_Personaid_Z = (Guid)(Z1PersonaID);
         obj1.gxTpr_Personapnombre_Z = Z2PersonaPNombre;
         obj1.gxTpr_Personasnombre_Z = Z3PersonaSNombre;
         obj1.gxTpr_Personapapellido_Z = Z4PersonaPApellido;
         obj1.gxTpr_Personasapellido_Z = Z5PersonaSApellido;
         obj1.gxTpr_Personasexo_Z = Z6PersonaSexo;
         obj1.gxTpr_Personadpi_Z = Z7PersonaDPI;
         obj1.gxTpr_Personatipo_Z = Z8PersonaTipo;
         obj1.gxTpr_Personafechanacimiento_Z = Z9PersonaFechaNacimiento;
         obj1.gxTpr_Personapacienteorigensordera_Z = Z10PersonaPacienteOrigenSordera;
         obj1.gxTpr_Personaemail_Z = Z11PersonaEMail;
         obj1.gxTpr_Personasnombre_N = (short)(Convert.ToInt16(n3PersonaSNombre));
         obj1.gxTpr_Personasapellido_N = (short)(Convert.ToInt16(n5PersonaSApellido));
         obj1.gxTpr_Personasexo_N = (short)(Convert.ToInt16(n6PersonaSexo));
         obj1.gxTpr_Personadpi_N = (short)(Convert.ToInt16(n7PersonaDPI));
         obj1.gxTpr_Personafechanacimiento_N = (short)(Convert.ToInt16(n9PersonaFechaNacimiento));
         obj1.gxTpr_Personapacienteorigensordera_N = (short)(Convert.ToInt16(n10PersonaPacienteOrigenSordera));
         obj1.gxTpr_Personaemail_N = (short)(Convert.ToInt16(n11PersonaEMail));
         obj1.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow1( SdtPersona obj1 )
      {
         obj1.gxTpr_Personaid = (Guid)(A1PersonaID);
         return  ;
      }

      public void RowToVars1( SdtPersona obj1 ,
                              int forceLoad )
      {
         Gx_mode = obj1.gxTpr_Mode;
         A2PersonaPNombre = obj1.gxTpr_Personapnombre;
         A3PersonaSNombre = obj1.gxTpr_Personasnombre;
         n3PersonaSNombre = false;
         A4PersonaPApellido = obj1.gxTpr_Personapapellido;
         A5PersonaSApellido = obj1.gxTpr_Personasapellido;
         n5PersonaSApellido = false;
         A6PersonaSexo = obj1.gxTpr_Personasexo;
         n6PersonaSexo = false;
         A7PersonaDPI = obj1.gxTpr_Personadpi;
         n7PersonaDPI = false;
         A8PersonaTipo = obj1.gxTpr_Personatipo;
         A9PersonaFechaNacimiento = obj1.gxTpr_Personafechanacimiento;
         n9PersonaFechaNacimiento = false;
         A10PersonaPacienteOrigenSordera = obj1.gxTpr_Personapacienteorigensordera;
         n10PersonaPacienteOrigenSordera = false;
         A11PersonaEMail = obj1.gxTpr_Personaemail;
         n11PersonaEMail = false;
         A1PersonaID = (Guid)(obj1.gxTpr_Personaid);
         Z1PersonaID = (Guid)(obj1.gxTpr_Personaid_Z);
         Z2PersonaPNombre = obj1.gxTpr_Personapnombre_Z;
         Z3PersonaSNombre = obj1.gxTpr_Personasnombre_Z;
         Z4PersonaPApellido = obj1.gxTpr_Personapapellido_Z;
         Z5PersonaSApellido = obj1.gxTpr_Personasapellido_Z;
         Z6PersonaSexo = obj1.gxTpr_Personasexo_Z;
         Z7PersonaDPI = obj1.gxTpr_Personadpi_Z;
         Z8PersonaTipo = obj1.gxTpr_Personatipo_Z;
         Z9PersonaFechaNacimiento = obj1.gxTpr_Personafechanacimiento_Z;
         Z10PersonaPacienteOrigenSordera = obj1.gxTpr_Personapacienteorigensordera_Z;
         Z11PersonaEMail = obj1.gxTpr_Personaemail_Z;
         n3PersonaSNombre = (bool)(Convert.ToBoolean(obj1.gxTpr_Personasnombre_N));
         n5PersonaSApellido = (bool)(Convert.ToBoolean(obj1.gxTpr_Personasapellido_N));
         n6PersonaSexo = (bool)(Convert.ToBoolean(obj1.gxTpr_Personasexo_N));
         n7PersonaDPI = (bool)(Convert.ToBoolean(obj1.gxTpr_Personadpi_N));
         n9PersonaFechaNacimiento = (bool)(Convert.ToBoolean(obj1.gxTpr_Personafechanacimiento_N));
         n10PersonaPacienteOrigenSordera = (bool)(Convert.ToBoolean(obj1.gxTpr_Personapacienteorigensordera_N));
         n11PersonaEMail = (bool)(Convert.ToBoolean(obj1.gxTpr_Personaemail_N));
         Gx_mode = obj1.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A1PersonaID = (Guid)((Guid)getParm(obj,0));
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey011( ) ;
         ScanKeyStart011( ) ;
         if ( RcdFound1 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1PersonaID = (Guid)(A1PersonaID);
         }
         ZM011( -6) ;
         OnLoadActions011( ) ;
         AddRow011( ) ;
         ScanKeyEnd011( ) ;
         if ( RcdFound1 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars1( bcPersona, 0) ;
         ScanKeyStart011( ) ;
         if ( RcdFound1 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z1PersonaID = (Guid)(A1PersonaID);
         }
         ZM011( -6) ;
         OnLoadActions011( ) ;
         AddRow011( ) ;
         ScanKeyEnd011( ) ;
         if ( RcdFound1 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      protected void SaveImpl( )
      {
         nKeyPressed = 1;
         GetKey011( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert011( ) ;
         }
         else
         {
            if ( RcdFound1 == 1 )
            {
               if ( A1PersonaID != Z1PersonaID )
               {
                  A1PersonaID = (Guid)(Z1PersonaID);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update011( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A1PersonaID != Z1PersonaID )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert011( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert011( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars1( bcPersona, 0) ;
         SaveImpl( ) ;
         VarsToRow1( bcPersona) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public bool Insert( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars1( bcPersona, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert011( ) ;
         AfterTrn( ) ;
         VarsToRow1( bcPersona) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      protected void UpdateImpl( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            SaveImpl( ) ;
         }
         else
         {
            SdtPersona auxBC = new SdtPersona(context) ;
            auxBC.Load(A1PersonaID);
            auxBC.UpdateDirties(bcPersona);
            auxBC.Save();
            IGxSilentTrn auxTrn = auxBC.getTransaction() ;
            LclMsgLst = (msglist)(auxTrn.GetMessages());
            AnyError = (short)(auxTrn.Errors());
            Gx_mode = auxTrn.GetMode();
            context.GX_msglist = LclMsgLst;
            AfterTrn( ) ;
         }
      }

      public bool Update( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars1( bcPersona, 0) ;
         UpdateImpl( ) ;
         VarsToRow1( bcPersona) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public bool InsertOrUpdate( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars1( bcPersona, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert011( ) ;
         if ( AnyError == 1 )
         {
            AnyError = 0;
            context.GX_msglist.removeAllItems();
            UpdateImpl( ) ;
         }
         else
         {
            AfterTrn( ) ;
         }
         VarsToRow1( bcPersona) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars1( bcPersona, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey011( ) ;
         if ( RcdFound1 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A1PersonaID != Z1PersonaID )
            {
               A1PersonaID = (Guid)(Z1PersonaID);
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A1PersonaID != Z1PersonaID )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_gam.rollback( "Persona_BC");
         pr_default.rollback( "Persona_BC");
         VarsToRow1( bcPersona) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcPersona.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcPersona.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcPersona )
         {
            bcPersona = (SdtPersona)(sdt);
            if ( StringUtil.StrCmp(bcPersona.gxTpr_Mode, "") == 0 )
            {
               bcPersona.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow1( bcPersona) ;
            }
            else
            {
               RowToVars1( bcPersona, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcPersona.gxTpr_Mode, "") == 0 )
            {
               bcPersona.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars1( bcPersona, 1) ;
         return  ;
      }

      public SdtPersona Persona_BC
      {
         get {
            return bcPersona ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "persona_Execute" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z1PersonaID = (Guid)(Guid.Empty);
         A1PersonaID = (Guid)(Guid.Empty);
         AV16StandardActivityType = "";
         AV17UserActivityType = "";
         Z2PersonaPNombre = "";
         A2PersonaPNombre = "";
         Z3PersonaSNombre = "";
         A3PersonaSNombre = "";
         Z4PersonaPApellido = "";
         A4PersonaPApellido = "";
         Z5PersonaSApellido = "";
         A5PersonaSApellido = "";
         Z6PersonaSexo = "";
         A6PersonaSexo = "";
         Z7PersonaDPI = "";
         A7PersonaDPI = "";
         Z9PersonaFechaNacimiento = DateTime.MinValue;
         A9PersonaFechaNacimiento = DateTime.MinValue;
         Z11PersonaEMail = "";
         A11PersonaEMail = "";
         BC00014_A1PersonaID = new Guid[] {Guid.Empty} ;
         BC00014_A2PersonaPNombre = new String[] {""} ;
         BC00014_A3PersonaSNombre = new String[] {""} ;
         BC00014_n3PersonaSNombre = new bool[] {false} ;
         BC00014_A4PersonaPApellido = new String[] {""} ;
         BC00014_A5PersonaSApellido = new String[] {""} ;
         BC00014_n5PersonaSApellido = new bool[] {false} ;
         BC00014_A6PersonaSexo = new String[] {""} ;
         BC00014_n6PersonaSexo = new bool[] {false} ;
         BC00014_A7PersonaDPI = new String[] {""} ;
         BC00014_n7PersonaDPI = new bool[] {false} ;
         BC00014_A8PersonaTipo = new short[1] ;
         BC00014_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         BC00014_n9PersonaFechaNacimiento = new bool[] {false} ;
         BC00014_A10PersonaPacienteOrigenSordera = new short[1] ;
         BC00014_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         BC00014_A11PersonaEMail = new String[] {""} ;
         BC00014_n11PersonaEMail = new bool[] {false} ;
         BC00015_A1PersonaID = new Guid[] {Guid.Empty} ;
         BC00013_A1PersonaID = new Guid[] {Guid.Empty} ;
         BC00013_A2PersonaPNombre = new String[] {""} ;
         BC00013_A3PersonaSNombre = new String[] {""} ;
         BC00013_n3PersonaSNombre = new bool[] {false} ;
         BC00013_A4PersonaPApellido = new String[] {""} ;
         BC00013_A5PersonaSApellido = new String[] {""} ;
         BC00013_n5PersonaSApellido = new bool[] {false} ;
         BC00013_A6PersonaSexo = new String[] {""} ;
         BC00013_n6PersonaSexo = new bool[] {false} ;
         BC00013_A7PersonaDPI = new String[] {""} ;
         BC00013_n7PersonaDPI = new bool[] {false} ;
         BC00013_A8PersonaTipo = new short[1] ;
         BC00013_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         BC00013_n9PersonaFechaNacimiento = new bool[] {false} ;
         BC00013_A10PersonaPacienteOrigenSordera = new short[1] ;
         BC00013_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         BC00013_A11PersonaEMail = new String[] {""} ;
         BC00013_n11PersonaEMail = new bool[] {false} ;
         sMode1 = "";
         BC00012_A1PersonaID = new Guid[] {Guid.Empty} ;
         BC00012_A2PersonaPNombre = new String[] {""} ;
         BC00012_A3PersonaSNombre = new String[] {""} ;
         BC00012_n3PersonaSNombre = new bool[] {false} ;
         BC00012_A4PersonaPApellido = new String[] {""} ;
         BC00012_A5PersonaSApellido = new String[] {""} ;
         BC00012_n5PersonaSApellido = new bool[] {false} ;
         BC00012_A6PersonaSexo = new String[] {""} ;
         BC00012_n6PersonaSexo = new bool[] {false} ;
         BC00012_A7PersonaDPI = new String[] {""} ;
         BC00012_n7PersonaDPI = new bool[] {false} ;
         BC00012_A8PersonaTipo = new short[1] ;
         BC00012_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         BC00012_n9PersonaFechaNacimiento = new bool[] {false} ;
         BC00012_A10PersonaPacienteOrigenSordera = new short[1] ;
         BC00012_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         BC00012_A11PersonaEMail = new String[] {""} ;
         BC00012_n11PersonaEMail = new bool[] {false} ;
         BC00019_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         BC000110_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         BC000111_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         BC000112_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC000112_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         BC000113_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC000113_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         BC000114_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC000114_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         BC000115_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC000115_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         BC000116_A1PersonaID = new Guid[] {Guid.Empty} ;
         BC000116_A2PersonaPNombre = new String[] {""} ;
         BC000116_A3PersonaSNombre = new String[] {""} ;
         BC000116_n3PersonaSNombre = new bool[] {false} ;
         BC000116_A4PersonaPApellido = new String[] {""} ;
         BC000116_A5PersonaSApellido = new String[] {""} ;
         BC000116_n5PersonaSApellido = new bool[] {false} ;
         BC000116_A6PersonaSexo = new String[] {""} ;
         BC000116_n6PersonaSexo = new bool[] {false} ;
         BC000116_A7PersonaDPI = new String[] {""} ;
         BC000116_n7PersonaDPI = new bool[] {false} ;
         BC000116_A8PersonaTipo = new short[1] ;
         BC000116_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         BC000116_n9PersonaFechaNacimiento = new bool[] {false} ;
         BC000116_A10PersonaPacienteOrigenSordera = new short[1] ;
         BC000116_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         BC000116_A11PersonaEMail = new String[] {""} ;
         BC000116_n11PersonaEMail = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.persona_bc__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.persona_bc__default(),
            new Object[][] {
                new Object[] {
               BC00012_A1PersonaID, BC00012_A2PersonaPNombre, BC00012_A3PersonaSNombre, BC00012_n3PersonaSNombre, BC00012_A4PersonaPApellido, BC00012_A5PersonaSApellido, BC00012_n5PersonaSApellido, BC00012_A6PersonaSexo, BC00012_n6PersonaSexo, BC00012_A7PersonaDPI,
               BC00012_n7PersonaDPI, BC00012_A8PersonaTipo, BC00012_A9PersonaFechaNacimiento, BC00012_n9PersonaFechaNacimiento, BC00012_A10PersonaPacienteOrigenSordera, BC00012_n10PersonaPacienteOrigenSordera, BC00012_A11PersonaEMail, BC00012_n11PersonaEMail
               }
               , new Object[] {
               BC00013_A1PersonaID, BC00013_A2PersonaPNombre, BC00013_A3PersonaSNombre, BC00013_n3PersonaSNombre, BC00013_A4PersonaPApellido, BC00013_A5PersonaSApellido, BC00013_n5PersonaSApellido, BC00013_A6PersonaSexo, BC00013_n6PersonaSexo, BC00013_A7PersonaDPI,
               BC00013_n7PersonaDPI, BC00013_A8PersonaTipo, BC00013_A9PersonaFechaNacimiento, BC00013_n9PersonaFechaNacimiento, BC00013_A10PersonaPacienteOrigenSordera, BC00013_n10PersonaPacienteOrigenSordera, BC00013_A11PersonaEMail, BC00013_n11PersonaEMail
               }
               , new Object[] {
               BC00014_A1PersonaID, BC00014_A2PersonaPNombre, BC00014_A3PersonaSNombre, BC00014_n3PersonaSNombre, BC00014_A4PersonaPApellido, BC00014_A5PersonaSApellido, BC00014_n5PersonaSApellido, BC00014_A6PersonaSexo, BC00014_n6PersonaSexo, BC00014_A7PersonaDPI,
               BC00014_n7PersonaDPI, BC00014_A8PersonaTipo, BC00014_A9PersonaFechaNacimiento, BC00014_n9PersonaFechaNacimiento, BC00014_A10PersonaPacienteOrigenSordera, BC00014_n10PersonaPacienteOrigenSordera, BC00014_A11PersonaEMail, BC00014_n11PersonaEMail
               }
               , new Object[] {
               BC00015_A1PersonaID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC00019_A43EmergenciaID
               }
               , new Object[] {
               BC000110_A43EmergenciaID
               }
               , new Object[] {
               BC000111_A35ObstaculoID
               }
               , new Object[] {
               BC000112_A23RecorridoID, BC000112_A32RecorridoDetalleID
               }
               , new Object[] {
               BC000113_A23RecorridoID, BC000113_A32RecorridoDetalleID
               }
               , new Object[] {
               BC000114_A12CoachPaciente_Coach, BC000114_A13CoachPaciente_Paciente
               }
               , new Object[] {
               BC000115_A12CoachPaciente_Coach, BC000115_A13CoachPaciente_Paciente
               }
               , new Object[] {
               BC000116_A1PersonaID, BC000116_A2PersonaPNombre, BC000116_A3PersonaSNombre, BC000116_n3PersonaSNombre, BC000116_A4PersonaPApellido, BC000116_A5PersonaSApellido, BC000116_n5PersonaSApellido, BC000116_A6PersonaSexo, BC000116_n6PersonaSexo, BC000116_A7PersonaDPI,
               BC000116_n7PersonaDPI, BC000116_A8PersonaTipo, BC000116_A9PersonaFechaNacimiento, BC000116_n9PersonaFechaNacimiento, BC000116_A10PersonaPacienteOrigenSordera, BC000116_n10PersonaPacienteOrigenSordera, BC000116_A11PersonaEMail, BC000116_n11PersonaEMail
               }
            }
         );
         Z1PersonaID = (Guid)(Guid.NewGuid( ));
         A1PersonaID = (Guid)(Guid.NewGuid( ));
         INITTRN();
         /* Execute Start event if defined. */
         /* Execute user event: Start */
         E12012 ();
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z8PersonaTipo ;
      private short A8PersonaTipo ;
      private short Z10PersonaPacienteOrigenSordera ;
      private short A10PersonaPacienteOrigenSordera ;
      private short Gx_BScreen ;
      private short RcdFound1 ;
      private int trnEnded ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String AV16StandardActivityType ;
      private String Z6PersonaSexo ;
      private String A6PersonaSexo ;
      private String sMode1 ;
      private DateTime Z9PersonaFechaNacimiento ;
      private DateTime A9PersonaFechaNacimiento ;
      private bool n3PersonaSNombre ;
      private bool n5PersonaSApellido ;
      private bool n6PersonaSexo ;
      private bool n7PersonaDPI ;
      private bool n9PersonaFechaNacimiento ;
      private bool n10PersonaPacienteOrigenSordera ;
      private bool n11PersonaEMail ;
      private bool Gx_longc ;
      private String AV17UserActivityType ;
      private String Z2PersonaPNombre ;
      private String A2PersonaPNombre ;
      private String Z3PersonaSNombre ;
      private String A3PersonaSNombre ;
      private String Z4PersonaPApellido ;
      private String A4PersonaPApellido ;
      private String Z5PersonaSApellido ;
      private String A5PersonaSApellido ;
      private String Z7PersonaDPI ;
      private String A7PersonaDPI ;
      private String Z11PersonaEMail ;
      private String A11PersonaEMail ;
      private Guid Z1PersonaID ;
      private Guid A1PersonaID ;
      private SdtPersona bcPersona ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] BC00014_A1PersonaID ;
      private String[] BC00014_A2PersonaPNombre ;
      private String[] BC00014_A3PersonaSNombre ;
      private bool[] BC00014_n3PersonaSNombre ;
      private String[] BC00014_A4PersonaPApellido ;
      private String[] BC00014_A5PersonaSApellido ;
      private bool[] BC00014_n5PersonaSApellido ;
      private String[] BC00014_A6PersonaSexo ;
      private bool[] BC00014_n6PersonaSexo ;
      private String[] BC00014_A7PersonaDPI ;
      private bool[] BC00014_n7PersonaDPI ;
      private short[] BC00014_A8PersonaTipo ;
      private DateTime[] BC00014_A9PersonaFechaNacimiento ;
      private bool[] BC00014_n9PersonaFechaNacimiento ;
      private short[] BC00014_A10PersonaPacienteOrigenSordera ;
      private bool[] BC00014_n10PersonaPacienteOrigenSordera ;
      private String[] BC00014_A11PersonaEMail ;
      private bool[] BC00014_n11PersonaEMail ;
      private Guid[] BC00015_A1PersonaID ;
      private Guid[] BC00013_A1PersonaID ;
      private String[] BC00013_A2PersonaPNombre ;
      private String[] BC00013_A3PersonaSNombre ;
      private bool[] BC00013_n3PersonaSNombre ;
      private String[] BC00013_A4PersonaPApellido ;
      private String[] BC00013_A5PersonaSApellido ;
      private bool[] BC00013_n5PersonaSApellido ;
      private String[] BC00013_A6PersonaSexo ;
      private bool[] BC00013_n6PersonaSexo ;
      private String[] BC00013_A7PersonaDPI ;
      private bool[] BC00013_n7PersonaDPI ;
      private short[] BC00013_A8PersonaTipo ;
      private DateTime[] BC00013_A9PersonaFechaNacimiento ;
      private bool[] BC00013_n9PersonaFechaNacimiento ;
      private short[] BC00013_A10PersonaPacienteOrigenSordera ;
      private bool[] BC00013_n10PersonaPacienteOrigenSordera ;
      private String[] BC00013_A11PersonaEMail ;
      private bool[] BC00013_n11PersonaEMail ;
      private Guid[] BC00012_A1PersonaID ;
      private String[] BC00012_A2PersonaPNombre ;
      private String[] BC00012_A3PersonaSNombre ;
      private bool[] BC00012_n3PersonaSNombre ;
      private String[] BC00012_A4PersonaPApellido ;
      private String[] BC00012_A5PersonaSApellido ;
      private bool[] BC00012_n5PersonaSApellido ;
      private String[] BC00012_A6PersonaSexo ;
      private bool[] BC00012_n6PersonaSexo ;
      private String[] BC00012_A7PersonaDPI ;
      private bool[] BC00012_n7PersonaDPI ;
      private short[] BC00012_A8PersonaTipo ;
      private DateTime[] BC00012_A9PersonaFechaNacimiento ;
      private bool[] BC00012_n9PersonaFechaNacimiento ;
      private short[] BC00012_A10PersonaPacienteOrigenSordera ;
      private bool[] BC00012_n10PersonaPacienteOrigenSordera ;
      private String[] BC00012_A11PersonaEMail ;
      private bool[] BC00012_n11PersonaEMail ;
      private Guid[] BC00019_A43EmergenciaID ;
      private Guid[] BC000110_A43EmergenciaID ;
      private Guid[] BC000111_A35ObstaculoID ;
      private Guid[] BC000112_A23RecorridoID ;
      private Guid[] BC000112_A32RecorridoDetalleID ;
      private Guid[] BC000113_A23RecorridoID ;
      private Guid[] BC000113_A32RecorridoDetalleID ;
      private Guid[] BC000114_A12CoachPaciente_Coach ;
      private Guid[] BC000114_A13CoachPaciente_Paciente ;
      private Guid[] BC000115_A12CoachPaciente_Coach ;
      private Guid[] BC000115_A13CoachPaciente_Paciente ;
      private Guid[] BC000116_A1PersonaID ;
      private String[] BC000116_A2PersonaPNombre ;
      private String[] BC000116_A3PersonaSNombre ;
      private bool[] BC000116_n3PersonaSNombre ;
      private String[] BC000116_A4PersonaPApellido ;
      private String[] BC000116_A5PersonaSApellido ;
      private bool[] BC000116_n5PersonaSApellido ;
      private String[] BC000116_A6PersonaSexo ;
      private bool[] BC000116_n6PersonaSexo ;
      private String[] BC000116_A7PersonaDPI ;
      private bool[] BC000116_n7PersonaDPI ;
      private short[] BC000116_A8PersonaTipo ;
      private DateTime[] BC000116_A9PersonaFechaNacimiento ;
      private bool[] BC000116_n9PersonaFechaNacimiento ;
      private short[] BC000116_A10PersonaPacienteOrigenSordera ;
      private bool[] BC000116_n10PersonaPacienteOrigenSordera ;
      private String[] BC000116_A11PersonaEMail ;
      private bool[] BC000116_n11PersonaEMail ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_gam ;
   }

   public class persona_bc__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class persona_bc__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new UpdateCursor(def[4])
       ,new UpdateCursor(def[5])
       ,new UpdateCursor(def[6])
       ,new ForEachCursor(def[7])
       ,new ForEachCursor(def[8])
       ,new ForEachCursor(def[9])
       ,new ForEachCursor(def[10])
       ,new ForEachCursor(def[11])
       ,new ForEachCursor(def[12])
       ,new ForEachCursor(def[13])
       ,new ForEachCursor(def[14])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmBC00014 ;
        prmBC00014 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00015 ;
        prmBC00015 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00013 ;
        prmBC00013 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00012 ;
        prmBC00012 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00016 ;
        prmBC00016 = new Object[] {
        new Object[] {"@PersonaPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSexo",SqlDbType.Char,1,0} ,
        new Object[] {"@PersonaDPI",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaFechaNacimiento",SqlDbType.DateTime,8,0} ,
        new Object[] {"@PersonaPacienteOrigenSordera",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaEMail",SqlDbType.VarChar,100,0} ,
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00017 ;
        prmBC00017 = new Object[] {
        new Object[] {"@PersonaPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSexo",SqlDbType.Char,1,0} ,
        new Object[] {"@PersonaDPI",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaFechaNacimiento",SqlDbType.DateTime,8,0} ,
        new Object[] {"@PersonaPacienteOrigenSordera",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaEMail",SqlDbType.VarChar,100,0} ,
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00018 ;
        prmBC00018 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00019 ;
        prmBC00019 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000110 ;
        prmBC000110 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000111 ;
        prmBC000111 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000112 ;
        prmBC000112 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000113 ;
        prmBC000113 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000114 ;
        prmBC000114 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000115 ;
        prmBC000115 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000116 ;
        prmBC000116 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("BC00012", "SELECT [PersonaID], [PersonaPNombre], [PersonaSNombre], [PersonaPApellido], [PersonaSApellido], [PersonaSexo], [PersonaDPI], [PersonaTipo], [PersonaFechaNacimiento], [PersonaPacienteOrigenSordera], [PersonaEMail] FROM [Persona] WITH (UPDLOCK) WHERE [PersonaID] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00012,1,0,true,false )
           ,new CursorDef("BC00013", "SELECT [PersonaID], [PersonaPNombre], [PersonaSNombre], [PersonaPApellido], [PersonaSApellido], [PersonaSexo], [PersonaDPI], [PersonaTipo], [PersonaFechaNacimiento], [PersonaPacienteOrigenSordera], [PersonaEMail] FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00013,1,0,true,false )
           ,new CursorDef("BC00014", "SELECT TM1.[PersonaID], TM1.[PersonaPNombre], TM1.[PersonaSNombre], TM1.[PersonaPApellido], TM1.[PersonaSApellido], TM1.[PersonaSexo], TM1.[PersonaDPI], TM1.[PersonaTipo], TM1.[PersonaFechaNacimiento], TM1.[PersonaPacienteOrigenSordera], TM1.[PersonaEMail] FROM [Persona] TM1 WITH (NOLOCK) WHERE TM1.[PersonaID] = @PersonaID ORDER BY TM1.[PersonaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00014,100,0,true,false )
           ,new CursorDef("BC00015", "SELECT [PersonaID] FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @PersonaID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00015,1,0,true,false )
           ,new CursorDef("BC00016", "INSERT INTO [Persona]([PersonaPNombre], [PersonaSNombre], [PersonaPApellido], [PersonaSApellido], [PersonaSexo], [PersonaDPI], [PersonaTipo], [PersonaFechaNacimiento], [PersonaPacienteOrigenSordera], [PersonaEMail], [PersonaID]) VALUES(@PersonaPNombre, @PersonaSNombre, @PersonaPApellido, @PersonaSApellido, @PersonaSexo, @PersonaDPI, @PersonaTipo, @PersonaFechaNacimiento, @PersonaPacienteOrigenSordera, @PersonaEMail, @PersonaID)", GxErrorMask.GX_NOMASK,prmBC00016)
           ,new CursorDef("BC00017", "UPDATE [Persona] SET [PersonaPNombre]=@PersonaPNombre, [PersonaSNombre]=@PersonaSNombre, [PersonaPApellido]=@PersonaPApellido, [PersonaSApellido]=@PersonaSApellido, [PersonaSexo]=@PersonaSexo, [PersonaDPI]=@PersonaDPI, [PersonaTipo]=@PersonaTipo, [PersonaFechaNacimiento]=@PersonaFechaNacimiento, [PersonaPacienteOrigenSordera]=@PersonaPacienteOrigenSordera, [PersonaEMail]=@PersonaEMail  WHERE [PersonaID] = @PersonaID", GxErrorMask.GX_NOMASK,prmBC00017)
           ,new CursorDef("BC00018", "DELETE FROM [Persona]  WHERE [PersonaID] = @PersonaID", GxErrorMask.GX_NOMASK,prmBC00018)
           ,new CursorDef("BC00019", "SELECT TOP 1 [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaCoach] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00019,1,0,true,true )
           ,new CursorDef("BC000110", "SELECT TOP 1 [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaPaciente] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000110,1,0,true,true )
           ,new CursorDef("BC000111", "SELECT TOP 1 [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) WHERE [ObstaculoPacienteRegistra] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000111,1,0,true,true )
           ,new CursorDef("BC000112", "SELECT TOP 1 [RecorridoID], [RecorridoDetalleID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [IDCoach] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000112,1,0,true,true )
           ,new CursorDef("BC000113", "SELECT TOP 1 [RecorridoID], [RecorridoDetalleID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoDetallePaciente] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000113,1,0,true,true )
           ,new CursorDef("BC000114", "SELECT TOP 1 [CoachPaciente_Coach], [CoachPaciente_Paciente] FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Paciente] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000114,1,0,true,true )
           ,new CursorDef("BC000115", "SELECT TOP 1 [CoachPaciente_Coach], [CoachPaciente_Paciente] FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Coach] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000115,1,0,true,true )
           ,new CursorDef("BC000116", "SELECT TM1.[PersonaID], TM1.[PersonaPNombre], TM1.[PersonaSNombre], TM1.[PersonaPApellido], TM1.[PersonaSApellido], TM1.[PersonaSexo], TM1.[PersonaDPI], TM1.[PersonaTipo], TM1.[PersonaFechaNacimiento], TM1.[PersonaPacienteOrigenSordera], TM1.[PersonaEMail] FROM [Persona] TM1 WITH (NOLOCK) WHERE TM1.[PersonaID] = @PersonaID ORDER BY TM1.[PersonaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000116,100,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((short[]) buf[11])[0] = rslt.getShort(8) ;
              ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(9);
              ((short[]) buf[14])[0] = rslt.getShort(10) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(10);
              ((String[]) buf[16])[0] = rslt.getVarchar(11) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(11);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((short[]) buf[11])[0] = rslt.getShort(8) ;
              ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(9);
              ((short[]) buf[14])[0] = rslt.getShort(10) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(10);
              ((String[]) buf[16])[0] = rslt.getVarchar(11) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(11);
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((short[]) buf[11])[0] = rslt.getShort(8) ;
              ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(9);
              ((short[]) buf[14])[0] = rslt.getShort(10) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(10);
              ((String[]) buf[16])[0] = rslt.getVarchar(11) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(11);
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 7 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 8 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 9 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 10 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 11 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 12 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 13 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 14 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((short[]) buf[11])[0] = rslt.getShort(8) ;
              ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(9);
              ((short[]) buf[14])[0] = rslt.getShort(10) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(10);
              ((String[]) buf[16])[0] = rslt.getVarchar(11) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(11);
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 2 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 3 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 4 :
              stmt.SetParameter(1, (String)parms[0]);
              if ( (bool)parms[1] )
              {
                 stmt.setNull( 2 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(2, (String)parms[2]);
              }
              stmt.SetParameter(3, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.Char );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(6, (String)parms[9]);
              }
              stmt.SetParameter(7, (short)parms[10]);
              if ( (bool)parms[11] )
              {
                 stmt.setNull( 8 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(8, (DateTime)parms[12]);
              }
              if ( (bool)parms[13] )
              {
                 stmt.setNull( 9 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(9, (short)parms[14]);
              }
              if ( (bool)parms[15] )
              {
                 stmt.setNull( 10 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(10, (String)parms[16]);
              }
              stmt.SetParameter(11, (Guid)parms[17]);
              return;
           case 5 :
              stmt.SetParameter(1, (String)parms[0]);
              if ( (bool)parms[1] )
              {
                 stmt.setNull( 2 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(2, (String)parms[2]);
              }
              stmt.SetParameter(3, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.Char );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(6, (String)parms[9]);
              }
              stmt.SetParameter(7, (short)parms[10]);
              if ( (bool)parms[11] )
              {
                 stmt.setNull( 8 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(8, (DateTime)parms[12]);
              }
              if ( (bool)parms[13] )
              {
                 stmt.setNull( 9 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(9, (short)parms[14]);
              }
              if ( (bool)parms[15] )
              {
                 stmt.setNull( 10 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(10, (String)parms[16]);
              }
              stmt.SetParameter(11, (Guid)parms[17]);
              return;
           case 6 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 7 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 9 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 10 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 11 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 12 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 13 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 14 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
