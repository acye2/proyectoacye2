/*
               File: registrarrecorrido
        Description: Stub for registrarrecorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:30:18.76
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using System.Web.Services;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class registrarrecorrido : GXProcedure
   {
      public registrarrecorrido( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public registrarrecorrido( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_IDSesion ,
                           short aP1_TipoSenal ,
                           ref String aP2_Longitud ,
                           ref String aP3_Latitud ,
                           ref short aP4_TipoObstaculo ,
                           short aP5_Incidente ,
                           short aP6_Emergencia ,
                           ref String aP7_IMEI ,
                           ref String aP8_IDCoach ,
                           ref String aP9_IDPaciente )
      {
         this.AV2IDSesion = aP0_IDSesion;
         this.AV3TipoSenal = aP1_TipoSenal;
         this.AV4Longitud = aP2_Longitud;
         this.AV5Latitud = aP3_Latitud;
         this.AV6TipoObstaculo = aP4_TipoObstaculo;
         this.AV7Incidente = aP5_Incidente;
         this.AV8Emergencia = aP6_Emergencia;
         this.AV9IMEI = aP7_IMEI;
         this.AV10IDCoach = aP8_IDCoach;
         this.AV11IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV2IDSesion;
         aP2_Longitud=this.AV4Longitud;
         aP3_Latitud=this.AV5Latitud;
         aP4_TipoObstaculo=this.AV6TipoObstaculo;
         aP7_IMEI=this.AV9IMEI;
         aP8_IDCoach=this.AV10IDCoach;
         aP9_IDPaciente=this.AV11IDPaciente;
      }

      public String executeUdp( ref String aP0_IDSesion ,
                                short aP1_TipoSenal ,
                                ref String aP2_Longitud ,
                                ref String aP3_Latitud ,
                                ref short aP4_TipoObstaculo ,
                                short aP5_Incidente ,
                                short aP6_Emergencia ,
                                ref String aP7_IMEI ,
                                ref String aP8_IDCoach )
      {
         this.AV2IDSesion = aP0_IDSesion;
         this.AV3TipoSenal = aP1_TipoSenal;
         this.AV4Longitud = aP2_Longitud;
         this.AV5Latitud = aP3_Latitud;
         this.AV6TipoObstaculo = aP4_TipoObstaculo;
         this.AV7Incidente = aP5_Incidente;
         this.AV8Emergencia = aP6_Emergencia;
         this.AV9IMEI = aP7_IMEI;
         this.AV10IDCoach = aP8_IDCoach;
         this.AV11IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV2IDSesion;
         aP2_Longitud=this.AV4Longitud;
         aP3_Latitud=this.AV5Latitud;
         aP4_TipoObstaculo=this.AV6TipoObstaculo;
         aP7_IMEI=this.AV9IMEI;
         aP8_IDCoach=this.AV10IDCoach;
         aP9_IDPaciente=this.AV11IDPaciente;
         return AV11IDPaciente ;
      }

      public void executeSubmit( ref String aP0_IDSesion ,
                                 short aP1_TipoSenal ,
                                 ref String aP2_Longitud ,
                                 ref String aP3_Latitud ,
                                 ref short aP4_TipoObstaculo ,
                                 short aP5_Incidente ,
                                 short aP6_Emergencia ,
                                 ref String aP7_IMEI ,
                                 ref String aP8_IDCoach ,
                                 ref String aP9_IDPaciente )
      {
         registrarrecorrido objregistrarrecorrido;
         objregistrarrecorrido = new registrarrecorrido();
         objregistrarrecorrido.AV2IDSesion = aP0_IDSesion;
         objregistrarrecorrido.AV3TipoSenal = aP1_TipoSenal;
         objregistrarrecorrido.AV4Longitud = aP2_Longitud;
         objregistrarrecorrido.AV5Latitud = aP3_Latitud;
         objregistrarrecorrido.AV6TipoObstaculo = aP4_TipoObstaculo;
         objregistrarrecorrido.AV7Incidente = aP5_Incidente;
         objregistrarrecorrido.AV8Emergencia = aP6_Emergencia;
         objregistrarrecorrido.AV9IMEI = aP7_IMEI;
         objregistrarrecorrido.AV10IDCoach = aP8_IDCoach;
         objregistrarrecorrido.AV11IDPaciente = aP9_IDPaciente;
         objregistrarrecorrido.context.SetSubmitInitialConfig(context);
         objregistrarrecorrido.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objregistrarrecorrido);
         aP0_IDSesion=this.AV2IDSesion;
         aP2_Longitud=this.AV4Longitud;
         aP3_Latitud=this.AV5Latitud;
         aP4_TipoObstaculo=this.AV6TipoObstaculo;
         aP7_IMEI=this.AV9IMEI;
         aP8_IDCoach=this.AV10IDCoach;
         aP9_IDPaciente=this.AV11IDPaciente;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((registrarrecorrido)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         args = new Object[] {(String)AV2IDSesion,(short)AV3TipoSenal,(String)AV4Longitud,(String)AV5Latitud,(short)AV6TipoObstaculo,(short)AV7Incidente,(short)AV8Emergencia,(String)AV9IMEI,(String)AV10IDCoach,(String)AV11IDPaciente} ;
         ClassLoader.Execute("aregistrarrecorrido","GeneXus.Programs","aregistrarrecorrido", new Object[] {context }, "execute", args);
         if ( ( args != null ) && ( args.Length == 10 ) )
         {
            AV2IDSesion = (String)(args[0]) ;
            AV4Longitud = (String)(args[2]) ;
            AV5Latitud = (String)(args[3]) ;
            AV6TipoObstaculo = (short)(args[4]) ;
            AV9IMEI = (String)(args[7]) ;
            AV10IDCoach = (String)(args[8]) ;
            AV11IDPaciente = (String)(args[9]) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV3TipoSenal ;
      private short AV6TipoObstaculo ;
      private short AV7Incidente ;
      private short AV8Emergencia ;
      private String AV2IDSesion ;
      private String AV10IDCoach ;
      private String AV11IDPaciente ;
      private String AV4Longitud ;
      private String AV5Latitud ;
      private String AV9IMEI ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_IDSesion ;
      private String aP2_Longitud ;
      private String aP3_Latitud ;
      private short aP4_TipoObstaculo ;
      private String aP7_IMEI ;
      private String aP8_IDCoach ;
      private String aP9_IDPaciente ;
      private Object[] args ;
   }

}
