/*
               File: ws_registrarecorrido
        Description: ws_registrarecorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:40.80
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class ws_registrarecorrido : GXProcedure
   {
      public ws_registrarecorrido( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public ws_registrarecorrido( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_IDSesion ,
                           short aP1_TipoSenal ,
                           ref String aP2_Longitud ,
                           ref String aP3_Latitud ,
                           ref short aP4_TipoObstaculo ,
                           short aP5_Incidente ,
                           short aP6_Emergencia ,
                           ref String aP7_IMEI ,
                           ref String aP8_IDCoach ,
                           ref String aP9_IDPaciente )
      {
         this.AV12IDSesion = aP0_IDSesion;
         this.AV19TipoSenal = aP1_TipoSenal;
         this.AV16Longitud = aP2_Longitud;
         this.AV15Latitud = aP3_Latitud;
         this.AV18TipoObstaculo = aP4_TipoObstaculo;
         this.AV14Incidente = aP5_Incidente;
         this.AV8Emergencia = aP6_Emergencia;
         this.AV13IMEI = aP7_IMEI;
         this.AV10IDCoach = aP8_IDCoach;
         this.AV11IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV12IDSesion;
         aP2_Longitud=this.AV16Longitud;
         aP3_Latitud=this.AV15Latitud;
         aP4_TipoObstaculo=this.AV18TipoObstaculo;
         aP7_IMEI=this.AV13IMEI;
         aP8_IDCoach=this.AV10IDCoach;
         aP9_IDPaciente=this.AV11IDPaciente;
      }

      public String executeUdp( ref String aP0_IDSesion ,
                                short aP1_TipoSenal ,
                                ref String aP2_Longitud ,
                                ref String aP3_Latitud ,
                                ref short aP4_TipoObstaculo ,
                                short aP5_Incidente ,
                                short aP6_Emergencia ,
                                ref String aP7_IMEI ,
                                ref String aP8_IDCoach )
      {
         this.AV12IDSesion = aP0_IDSesion;
         this.AV19TipoSenal = aP1_TipoSenal;
         this.AV16Longitud = aP2_Longitud;
         this.AV15Latitud = aP3_Latitud;
         this.AV18TipoObstaculo = aP4_TipoObstaculo;
         this.AV14Incidente = aP5_Incidente;
         this.AV8Emergencia = aP6_Emergencia;
         this.AV13IMEI = aP7_IMEI;
         this.AV10IDCoach = aP8_IDCoach;
         this.AV11IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV12IDSesion;
         aP2_Longitud=this.AV16Longitud;
         aP3_Latitud=this.AV15Latitud;
         aP4_TipoObstaculo=this.AV18TipoObstaculo;
         aP7_IMEI=this.AV13IMEI;
         aP8_IDCoach=this.AV10IDCoach;
         aP9_IDPaciente=this.AV11IDPaciente;
         return AV11IDPaciente ;
      }

      public void executeSubmit( ref String aP0_IDSesion ,
                                 short aP1_TipoSenal ,
                                 ref String aP2_Longitud ,
                                 ref String aP3_Latitud ,
                                 ref short aP4_TipoObstaculo ,
                                 short aP5_Incidente ,
                                 short aP6_Emergencia ,
                                 ref String aP7_IMEI ,
                                 ref String aP8_IDCoach ,
                                 ref String aP9_IDPaciente )
      {
         ws_registrarecorrido objws_registrarecorrido;
         objws_registrarecorrido = new ws_registrarecorrido();
         objws_registrarecorrido.AV12IDSesion = aP0_IDSesion;
         objws_registrarecorrido.AV19TipoSenal = aP1_TipoSenal;
         objws_registrarecorrido.AV16Longitud = aP2_Longitud;
         objws_registrarecorrido.AV15Latitud = aP3_Latitud;
         objws_registrarecorrido.AV18TipoObstaculo = aP4_TipoObstaculo;
         objws_registrarecorrido.AV14Incidente = aP5_Incidente;
         objws_registrarecorrido.AV8Emergencia = aP6_Emergencia;
         objws_registrarecorrido.AV13IMEI = aP7_IMEI;
         objws_registrarecorrido.AV10IDCoach = aP8_IDCoach;
         objws_registrarecorrido.AV11IDPaciente = aP9_IDPaciente;
         objws_registrarecorrido.context.SetSubmitInitialConfig(context);
         objws_registrarecorrido.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objws_registrarecorrido);
         aP0_IDSesion=this.AV12IDSesion;
         aP2_Longitud=this.AV16Longitud;
         aP3_Latitud=this.AV15Latitud;
         aP4_TipoObstaculo=this.AV18TipoObstaculo;
         aP7_IMEI=this.AV13IMEI;
         aP8_IDCoach=this.AV10IDCoach;
         aP9_IDPaciente=this.AV11IDPaciente;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((ws_registrarecorrido)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV19TipoSenal == 1 )
         {
            AV9GUID = (Guid)(Guid.NewGuid( ));
            AV17Recorrido = new SdtRecorrido(context);
            AV17Recorrido.gxTpr_Recorridoid = (Guid)(AV9GUID);
            AV17Recorrido.gxTpr_Recorridofecha = DateTimeUtil.ServerNow( context, pr_default);
            /* Execute user subroutine: 'GUARDAR' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV12IDSesion = StringUtil.Trim( AV9GUID.ToString());
         }
         else if ( AV19TipoSenal == 2 )
         {
            AV9GUID = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV12IDSesion)));
            AV17Recorrido.Load(AV9GUID);
            /* Execute user subroutine: 'GUARDAR' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( AV19TipoSenal == 3 )
         {
            AV9GUID = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV12IDSesion)));
            AV17Recorrido.Load(AV9GUID);
            /* Execute user subroutine: 'DETALLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'GUARDAR' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'DETALLE' Routine */
         AV20Detalle = new SdtRecorrido_Detalle(context);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV11IDPaciente)) )
         {
            AV20Detalle.gxTpr_Recorridodetallepaciente = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV11IDPaciente)));
         }
         else
         {
            AV20Detalle.gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_SetNull();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV10IDCoach)) )
         {
            AV20Detalle.gxTpr_Idcoach = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV10IDCoach)));
         }
         else
         {
            AV20Detalle.gxTv_SdtRecorrido_Detalle_Idcoach_SetNull();
         }
         AV20Detalle.gxTpr_Tiposenal = AV19TipoSenal;
         AV20Detalle.gxTpr_Longitud = AV16Longitud;
         AV20Detalle.gxTpr_Latitud = AV15Latitud;
         AV20Detalle.gxTpr_Tipoobstaculo = AV18TipoObstaculo;
         AV20Detalle.gxTpr_Incidente = AV14Incidente;
         AV20Detalle.gxTpr_Emergencia = AV8Emergencia;
         AV20Detalle.gxTpr_Imei = AV13IMEI;
         AV20Detalle.gxTpr_Timestamp = DateTimeUtil.ServerNow( context, pr_default);
         AV17Recorrido.gxTpr_Detalle.Add(AV20Detalle, 0);
      }

      protected void S121( )
      {
         /* 'GUARDAR' Routine */
         AV17Recorrido.Save();
         if ( AV17Recorrido.Success() )
         {
            pr_gam.commit( "ws_registrarecorrido");
            pr_default.commit( "ws_registrarecorrido");
         }
         else
         {
            pr_gam.rollback( "ws_registrarecorrido");
            pr_default.rollback( "ws_registrarecorrido");
            AV25GXV2 = 1;
            AV24GXV1 = AV17Recorrido.GetMessages();
            while ( AV25GXV2 <= AV24GXV1.Count )
            {
               AV21Message = ((SdtMessages_Message)AV24GXV1.Item(AV25GXV2));
               GX_msglist.addItem(AV21Message.gxTpr_Description);
               AV25GXV2 = (int)(AV25GXV2+1);
            }
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV9GUID = (Guid)(Guid.Empty);
         AV17Recorrido = new SdtRecorrido(context);
         AV20Detalle = new SdtRecorrido_Detalle(context);
         AV24GXV1 = new GXBaseCollection<SdtMessages_Message>( context, "Message", "GeneXus");
         AV21Message = new SdtMessages_Message(context);
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.ws_registrarecorrido__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.ws_registrarecorrido__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV19TipoSenal ;
      private short AV18TipoObstaculo ;
      private short AV14Incidente ;
      private short AV8Emergencia ;
      private int AV25GXV2 ;
      private String AV12IDSesion ;
      private String AV10IDCoach ;
      private String AV11IDPaciente ;
      private bool returnInSub ;
      private String AV16Longitud ;
      private String AV15Latitud ;
      private String AV13IMEI ;
      private Guid AV9GUID ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_IDSesion ;
      private String aP2_Longitud ;
      private String aP3_Latitud ;
      private short aP4_TipoObstaculo ;
      private String aP7_IMEI ;
      private String aP8_IDCoach ;
      private String aP9_IDPaciente ;
      private IDataStoreProvider pr_default ;
      private IDataStoreProvider pr_gam ;
      private GXBaseCollection<SdtMessages_Message> AV24GXV1 ;
      private SdtRecorrido AV17Recorrido ;
      private SdtRecorrido_Detalle AV20Detalle ;
      private SdtMessages_Message AV21Message ;
   }

   public class ws_registrarecorrido__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class ws_registrarecorrido__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

[ServiceContract(Namespace = "GeneXus.Programs.ws_registrarecorrido_services")]
[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class ws_registrarecorrido_services : GxRestService
{
   [OperationContract]
   [WebInvoke(Method =  "POST" ,
   	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
   	ResponseFormat = WebMessageFormat.Json,
   	UriTemplate = "/")]
   public void execute( ref String IDSesion ,
                        short TipoSenal ,
                        ref String Longitud ,
                        ref String Latitud ,
                        ref short TipoObstaculo ,
                        short Incidente ,
                        short Emergencia ,
                        ref String IMEI ,
                        ref String IDCoach ,
                        ref String IDPaciente )
   {
      try
      {
         if ( ! ProcessHeaders("ws_registrarecorrido") )
         {
            return  ;
         }
         ws_registrarecorrido worker = new ws_registrarecorrido(context) ;
         worker.IsMain = RunAsMain ;
         worker.execute(ref IDSesion,TipoSenal,ref Longitud,ref Latitud,ref TipoObstaculo,Incidente,Emergencia,ref IMEI,ref IDCoach,ref IDPaciente );
         worker.cleanup( );
      }
      catch ( Exception e )
      {
         WebException(e);
      }
      finally
      {
         Cleanup();
      }
   }

}

}
