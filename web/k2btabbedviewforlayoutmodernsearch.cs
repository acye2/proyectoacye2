/*
               File: K2BTabbedViewForLayoutModernSearch
        Description: K2 BTabbed View For Layout3
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:26.97
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2btabbedviewforlayoutmodernsearch : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public k2btabbedviewforlayoutmodernsearch( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
      }

      public k2btabbedviewforlayoutmodernsearch( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           GXBaseCollection<SdtK2BTabOptions_K2BTabOptionsItem> aP1_Tabs ,
                           String aP2_TabCode )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV19Tabs = aP1_Tabs;
         this.AV16TabCode = aP2_TabCode;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  Gx_mode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV19Tabs);
                  AV16TabCode = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TabCode", AV16TabCode);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(String)Gx_mode,(GXBaseCollection<SdtK2BTabOptions_K2BTabOptionsItem>)AV19Tabs,(String)AV16TabCode});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA162( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS162( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, false);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "K2 BTabbed View For Layout3") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?20181117152275", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body ") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" data-gx-class=\"Form\" novalidate action=\""+formatLink("k2btabbedviewforlayoutmodernsearch.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV16TabCode))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Class", "Form", true);
         }
         else
         {
            bool toggleHtmlOutput = isOutputEnabled( ) ;
            if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
            }
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            if ( toggleHtmlOutput )
            {
               if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableOutput();
                  }
               }
            }
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOGx_mode", StringUtil.RTrim( wcpOGx_mode));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV16TabCode", StringUtil.RTrim( wcpOAV16TabCode));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFIRSTTAB", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV6FirstTab), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vLASTTAB", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV12LastTab), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTABS", AV19Tabs);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTABS", AV19Tabs);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABCODE", StringUtil.RTrim( AV16TabCode));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSELECTEDTAB", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV14SelectedTab), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vINDEX", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9Index), 4, 0, ",", "")));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vTAB", AV15Tab);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vTAB", AV15Tab);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABSMARKUP", StringUtil.RTrim( AV20TabsMarkup));
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABTEMPLATE", StringUtil.RTrim( AV21TabTemplate));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMODE", StringUtil.RTrim( Gx_mode));
      }

      protected void RenderHtmlCloseForm162( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && ( context.isAjaxRequest( ) || context.isSpaRequest( ) ) )
         {
            context.AddJavascriptSource("k2btabbedviewforlayoutmodernsearch.js", "?20181117152277", false);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            if ( ! ( WebComp_Component == null ) )
            {
               WebComp_Component.componentjscripts();
            }
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            SendWebComponentState();
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "K2BTabbedViewForLayoutModernSearch" ;
      }

      public override String GetPgmdesc( )
      {
         return "K2 BTabbed View For Layout3" ;
      }

      protected void WB160( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "k2btabbedviewforlayoutmodernsearch.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabcontainer_Internalname, 1, 0, "px", 0, "px", "TabContainer", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTabs_Internalname, lblTabs_Caption, "", "", lblTabs_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 1, "HLP_K2BTabbedViewForLayoutModernSearch.htm");
            /* Static images/pictures */
            ClassString = "ImageTabPaging";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "ab84de32-929a-4946-a007-1b4c50d1e214", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgTabprevious_Internalname, sImgUrl, imgTabprevious_Link, "", "", context.GetTheme( ), imgTabprevious_Visible, 1, "", "Previous Tab", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_K2BTabbedViewForLayoutModernSearch.htm");
            /* Static images/pictures */
            ClassString = "ImageTabPaging";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "e67e1229-b202-42dc-b64c-894bf5de2fcd", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgTabnext_Internalname, sImgUrl, imgTabnext_Link, "", "", context.GetTheme( ), imgTabnext_Visible, 1, "", "Next Tab", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_K2BTabbedViewForLayoutModernSearch.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section_TabbedViewSearch", "left", "top", "", "", "div");
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, sPrefix+"W0007"+"", StringUtil.RTrim( WebComp_Component_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+sPrefix+"gxHTMLWrpW0007"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Component_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldComponent), StringUtil.Lower( WebComp_Component_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp(sPrefix+"gxHTMLWrpW0007"+"");
                  }
                  WebComp_Component.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldComponent), StringUtil.Lower( WebComp_Component_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START162( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
               Form.Meta.addItem("description", "K2 BTabbed View For Layout3", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP160( ) ;
            }
         }
      }

      protected void WS162( )
      {
         START162( ) ;
         EVT162( ) ;
      }

      protected void EVT162( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP160( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP160( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E11162 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP160( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Refresh */
                                    E12162 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP160( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E13162 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP160( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP160( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 7 )
                        {
                           OldComponent = cgiGet( sPrefix+"W0007");
                           if ( ( StringUtil.Len( OldComponent) == 0 ) || ( StringUtil.StrCmp(OldComponent, WebComp_Component_Component) != 0 ) )
                           {
                              WebComp_Component = getWebComponent(GetType(), "GeneXus.Programs", OldComponent, new Object[] {context} );
                              WebComp_Component.ComponentInit();
                              WebComp_Component.Name = "OldComponent";
                              WebComp_Component_Component = OldComponent;
                           }
                           if ( StringUtil.Len( WebComp_Component_Component) != 0 )
                           {
                              WebComp_Component.componentprocess(sPrefix+"W0007", "", sEvt);
                           }
                           WebComp_Component_Component = OldComponent;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE162( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm162( ) ;
            }
         }
      }

      protected void PA162( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF162( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF162( )
      {
         initialize_formulas( ) ;
         /* Execute user event: Refresh */
         E12162 ();
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Component_Component) != 0 )
               {
                  WebComp_Component.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E13162 ();
            WB160( ) ;
         }
      }

      protected void send_integrity_lvl_hashes162( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"vTABTEMPLATE", StringUtil.RTrim( AV21TabTemplate));
         GxWebStd.gx_hidden_field( context, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
      }

      protected void STRUP160( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11162 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            wcpOGx_mode = cgiGet( sPrefix+"wcpOGx_mode");
            wcpOAV16TabCode = cgiGet( sPrefix+"wcpOAV16TabCode");
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E11162 ();
         if (returnInSub) return;
      }

      protected void E11162( )
      {
         /* Start Routine */
         /* Execute user subroutine: 'INIT' */
         S112 ();
         if (returnInSub) return;
      }

      protected void E12162( )
      {
         /* Refresh Routine */
         /* Execute user subroutine: 'FINDTABINDEX' */
         S122 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'SCROLLTABS' */
         S132 ();
         if (returnInSub) return;
         AV10IsFirstTab = true;
         AV20TabsMarkup = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TabsMarkup", AV20TabsMarkup);
         AV9Index = AV6FirstTab;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Index), 4, 0)));
         while ( AV9Index <= AV12LastTab )
         {
            AV15Tab = ((SdtK2BTabOptions_K2BTabOptionsItem)AV19Tabs.Item(AV9Index));
            /* Execute user subroutine: 'LOADITEM' */
            S142 ();
            if (returnInSub) return;
            AV10IsFirstTab = false;
            AV9Index = (short)(AV9Index+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Index), 4, 0)));
         }
         lblTabs_Caption = StringUtil.Format( "<ul class=\"SearchTabs\">%1</ul>", AV20TabsMarkup, "", "", "", "", "", "", "", "");
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, lblTabs_Internalname, "Caption", lblTabs_Caption, true);
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV15Tab", AV15Tab);
      }

      protected void S112( )
      {
         /* 'INIT' Routine */
         AV21TabTemplate = "<li class=\"%1\">";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "<a id=\"%2Tab\" %3%7 class=\"%4\">";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "<span class=\"%5\">";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "<span class=\"TabBackground\">";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "<span class=\"TabText\">%6</span>";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "</span>";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "</span>";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "</a>";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
         AV21TabTemplate = AV21TabTemplate + "</li>";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21TabTemplate", AV21TabTemplate);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, sPrefix+"gxhash_vTABTEMPLATE", GetSecureSignedToken( sPrefix, StringUtil.RTrim( context.localUtil.Format( AV21TabTemplate, "")), context));
      }

      protected void S122( )
      {
         /* 'FINDTABINDEX' Routine */
         AV7Found = false;
         AV9Index = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Index), 4, 0)));
         while ( AV9Index <= AV19Tabs.Count )
         {
            if ( StringUtil.StrCmp(((SdtK2BTabOptions_K2BTabOptionsItem)AV19Tabs.Item(AV9Index)).gxTpr_Code, AV16TabCode) == 0 )
            {
               AV14SelectedTab = AV9Index;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14SelectedTab", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SelectedTab), 4, 0)));
               AV7Found = true;
               if (true) break;
            }
            AV9Index = (short)(AV9Index+1);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9Index", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9Index), 4, 0)));
         }
         if ( ! AV7Found && ( AV19Tabs.Count > 0 ) )
         {
            AV14SelectedTab = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14SelectedTab", StringUtil.LTrim( StringUtil.Str( (decimal)(AV14SelectedTab), 4, 0)));
         }
      }

      protected void S132( )
      {
         /* 'SCROLLTABS' Routine */
         AV22TotalTabs = 8;
         AV5CurrentPage = (short)((AV14SelectedTab-1)/ (decimal)(AV22TotalTabs));
         AV6FirstTab = (short)(AV5CurrentPage*AV22TotalTabs+1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV6FirstTab", StringUtil.LTrim( StringUtil.Str( (decimal)(AV6FirstTab), 4, 0)));
         AV12LastTab = (short)(AV6FirstTab+AV22TotalTabs-1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12LastTab", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12LastTab), 4, 0)));
         if ( AV12LastTab > AV19Tabs.Count )
         {
            AV12LastTab = (short)(AV19Tabs.Count);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV12LastTab", StringUtil.LTrim( StringUtil.Str( (decimal)(AV12LastTab), 4, 0)));
         }
         if ( AV6FirstTab <= AV22TotalTabs )
         {
            imgTabprevious_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgTabprevious_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTabprevious_Visible), 5, 0)), true);
         }
         else
         {
            imgTabprevious_Link = formatLink(((SdtK2BTabOptions_K2BTabOptionsItem)AV19Tabs.Item(AV6FirstTab-AV22TotalTabs)).gxTpr_Link) ;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgTabprevious_Internalname, "Link", imgTabprevious_Link, true);
         }
         if ( AV12LastTab >= AV19Tabs.Count )
         {
            imgTabnext_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgTabnext_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgTabnext_Visible), 5, 0)), true);
         }
         else
         {
            imgTabnext_Link = formatLink(((SdtK2BTabOptions_K2BTabOptionsItem)AV19Tabs.Item(AV6FirstTab+AV22TotalTabs)).gxTpr_Link) ;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, imgTabnext_Internalname, "Link", imgTabnext_Link, true);
         }
      }

      protected void S142( )
      {
         /* 'LOADITEM' Routine */
         AV17TabLeftCls = "TabLeft";
         if ( AV9Index == AV6FirstTab )
         {
            AV17TabLeftCls = "TabFirst";
         }
         AV18TabRightCls = "TabRight";
         if ( AV9Index == AV12LastTab )
         {
            AV18TabRightCls = "TabLast";
         }
         if ( AV9Index == AV14SelectedTab )
         {
            AV20TabsMarkup = AV20TabsMarkup + StringUtil.Format( AV21TabTemplate, "Tab TabSelected", AV15Tab.gxTpr_Code, "", AV17TabLeftCls, AV18TabRightCls, AV15Tab.gxTpr_Description, "", "", "");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TabsMarkup", AV20TabsMarkup);
            AV24WebComponentUrl = AV15Tab.gxTpr_Webcomponent;
            /* Object Property */
            gxDynCompUrl = AV24WebComponentUrl;
            if ( ! IsSameComponent( WebComp_Component_Component, gxDynCompUrl) )
            {
               WebComp_Component = getWebComponent(GetType(), "GeneXus.Programs", gxDynCompUrl, new Object[] {context} );
               WebComp_Component.ComponentInit();
               WebComp_Component.Name = "gxDynCompUrl";
               WebComp_Component_Component = gxDynCompUrl;
            }
            else
            {
               WebComp_Component.setparmsfromurl(gxDynCompUrl);
            }
            if ( StringUtil.Len( WebComp_Component_Component) != 0 )
            {
               WebComp_Component.setjustcreated();
               WebComp_Component.componentprepare(new Object[] {(String)sPrefix+"W0007",(String)""});
               WebComp_Component.componentbind(new Object[] {});
            }
            if ( isFullAjaxMode( ) )
            {
               context.httpAjaxContext.ajax_rspStartCmp(sPrefix+"gxHTMLWrpW0007"+"");
               WebComp_Component.componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
            }
         }
         else
         {
            if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
            {
               AV20TabsMarkup = AV20TabsMarkup + StringUtil.Format( AV21TabTemplate, "Tab TabDisabled", AV15Tab.gxTpr_Code, "", AV17TabLeftCls, AV18TabRightCls, AV15Tab.gxTpr_Description, "", "", "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TabsMarkup", AV20TabsMarkup);
            }
            else
            {
               AV13OnClickTemplate = "";
               AV20TabsMarkup = AV20TabsMarkup + StringUtil.Format( AV21TabTemplate, "Tab", AV15Tab.gxTpr_Code, StringUtil.Format( "href=\"%1\"", AV15Tab.gxTpr_Link, "", "", "", "", "", "", "", ""), AV17TabLeftCls, AV18TabRightCls, AV15Tab.gxTpr_Description, StringUtil.Format( AV13OnClickTemplate, AV15Tab.gxTpr_Code, "", "", "", "", "", "", "", ""), "", "");
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20TabsMarkup", AV20TabsMarkup);
            }
         }
      }

      protected void nextLoad( )
      {
      }

      protected void E13162( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         AV19Tabs = (GXBaseCollection<SdtK2BTabOptions_K2BTabOptionsItem>)getParm(obj,1);
         AV16TabCode = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TabCode", AV16TabCode);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA162( ) ;
         WS162( ) ;
         WE162( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlGx_mode = (String)((String)getParm(obj,0));
         sCtrlAV19Tabs = (String)((String)getParm(obj,1));
         sCtrlAV16TabCode = (String)((String)getParm(obj,2));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA162( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "k2btabbedviewforlayoutmodernsearch", GetJustCreated( ));
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA162( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            Gx_mode = (String)getParm(obj,2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
            AV19Tabs = (GXBaseCollection<SdtK2BTabOptions_K2BTabOptionsItem>)getParm(obj,3);
            AV16TabCode = (String)getParm(obj,4);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TabCode", AV16TabCode);
         }
         wcpOGx_mode = cgiGet( sPrefix+"wcpOGx_mode");
         wcpOAV16TabCode = cgiGet( sPrefix+"wcpOAV16TabCode");
         if ( ! GetJustCreated( ) && ( ( StringUtil.StrCmp(Gx_mode, wcpOGx_mode) != 0 ) || ( StringUtil.StrCmp(AV16TabCode, wcpOAV16TabCode) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOGx_mode = Gx_mode;
         wcpOAV16TabCode = AV16TabCode;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlGx_mode = cgiGet( sPrefix+"Gx_mode_CTRL");
         if ( StringUtil.Len( sCtrlGx_mode) > 0 )
         {
            Gx_mode = cgiGet( sCtrlGx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = cgiGet( sPrefix+"Gx_mode_PARM");
         }
         sCtrlAV19Tabs = cgiGet( sPrefix+"AV19Tabs_CTRL");
         if ( StringUtil.Len( sCtrlAV19Tabs) > 0 )
         {
            AV19Tabs = new GXBaseCollection<SdtK2BTabOptions_K2BTabOptionsItem>();
         }
         else
         {
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"AV19Tabs_PARM"), AV19Tabs);
         }
         sCtrlAV16TabCode = cgiGet( sPrefix+"AV16TabCode_CTRL");
         if ( StringUtil.Len( sCtrlAV16TabCode) > 0 )
         {
            AV16TabCode = cgiGet( sCtrlAV16TabCode);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16TabCode", AV16TabCode);
         }
         else
         {
            AV16TabCode = cgiGet( sPrefix+"AV16TabCode_PARM");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA162( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS162( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS162( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"Gx_mode_PARM", StringUtil.RTrim( Gx_mode));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlGx_mode)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"Gx_mode_CTRL", StringUtil.RTrim( sCtrlGx_mode));
         }
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"AV19Tabs_PARM", AV19Tabs);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"AV19Tabs_PARM", AV19Tabs);
         }
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV19Tabs)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV19Tabs_CTRL", StringUtil.RTrim( sCtrlAV19Tabs));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV16TabCode_PARM", StringUtil.RTrim( AV16TabCode));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV16TabCode)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV16TabCode_CTRL", StringUtil.RTrim( sCtrlAV16TabCode));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE162( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
         if ( ! ( WebComp_Component == null ) )
         {
            WebComp_Component.componentjscripts();
         }
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), false);
         if ( ! ( WebComp_Component == null ) )
         {
            if ( StringUtil.Len( WebComp_Component_Component) != 0 )
            {
               WebComp_Component.componentthemes();
            }
         }
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171522755", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("k2btabbedviewforlayoutmodernsearch.js", "?201811171522755", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTabs_Internalname = sPrefix+"TABS";
         imgTabprevious_Internalname = sPrefix+"TABPREVIOUS";
         imgTabnext_Internalname = sPrefix+"TABNEXT";
         divTabcontainer_Internalname = sPrefix+"TABCONTAINER";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         imgTabnext_Visible = 1;
         imgTabnext_Link = "";
         imgTabprevious_Visible = 1;
         imgTabprevious_Link = "";
         lblTabs_Jsonclick = "";
         lblTabs_Caption = "Tabs";
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV6FirstTab',fld:'vFIRSTTAB',pic:'ZZZ9',nv:0},{av:'AV12LastTab',fld:'vLASTTAB',pic:'ZZZ9',nv:0},{av:'AV19Tabs',fld:'vTABS',pic:'',nv:null},{av:'AV16TabCode',fld:'vTABCODE',pic:'',nv:''},{av:'AV14SelectedTab',fld:'vSELECTEDTAB',pic:'ZZZ9',nv:0},{av:'AV9Index',fld:'vINDEX',pic:'ZZZ9',nv:0},{av:'AV15Tab',fld:'vTAB',pic:'',nv:null},{av:'AV20TabsMarkup',fld:'vTABSMARKUP',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',nv:''},{av:'AV21TabTemplate',fld:'vTABTEMPLATE',pic:'',hsh:true,nv:''}],oparms:[{av:'AV20TabsMarkup',fld:'vTABSMARKUP',pic:'',nv:''},{av:'AV9Index',fld:'vINDEX',pic:'ZZZ9',nv:0},{av:'AV15Tab',fld:'vTAB',pic:'',nv:null},{av:'lblTabs_Caption',ctrl:'TABS',prop:'Caption'},{av:'AV14SelectedTab',fld:'vSELECTEDTAB',pic:'ZZZ9',nv:0},{av:'AV6FirstTab',fld:'vFIRSTTAB',pic:'ZZZ9',nv:0},{av:'AV12LastTab',fld:'vLASTTAB',pic:'ZZZ9',nv:0},{av:'imgTabprevious_Visible',ctrl:'TABPREVIOUS',prop:'Visible'},{av:'imgTabprevious_Link',ctrl:'TABPREVIOUS',prop:'Link'},{av:'imgTabnext_Visible',ctrl:'TABNEXT',prop:'Visible'},{av:'imgTabnext_Link',ctrl:'TABNEXT',prop:'Link'},{ctrl:'COMPONENT'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV19Tabs = new GXBaseCollection<SdtK2BTabOptions_K2BTabOptionsItem>( context, "K2BTabOptionsItem", "PACYE2");
         wcpOGx_mode = "";
         wcpOAV16TabCode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         AV15Tab = new SdtK2BTabOptions_K2BTabOptionsItem(context);
         AV20TabsMarkup = "";
         AV21TabTemplate = "";
         GX_FocusControl = "";
         ClassString = "";
         StyleString = "";
         sImgUrl = "";
         WebComp_Component_Component = "";
         gxDynCompUrl = "";
         OldComponent = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17TabLeftCls = "";
         AV18TabRightCls = "";
         AV24WebComponentUrl = "";
         AV13OnClickTemplate = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlGx_mode = "";
         sCtrlAV19Tabs = "";
         sCtrlAV16TabCode = "";
         WebComp_Component = new GeneXus.Http.GXNullWebComponent();
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short AV6FirstTab ;
      private short AV12LastTab ;
      private short AV14SelectedTab ;
      private short AV9Index ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV22TotalTabs ;
      private short AV5CurrentPage ;
      private short nGXWrapped ;
      private int imgTabprevious_Visible ;
      private int imgTabnext_Visible ;
      private int idxLst ;
      private String Gx_mode ;
      private String AV16TabCode ;
      private String wcpOGx_mode ;
      private String wcpOAV16TabCode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String AV20TabsMarkup ;
      private String AV21TabTemplate ;
      private String GX_FocusControl ;
      private String divTabcontainer_Internalname ;
      private String lblTabs_Internalname ;
      private String lblTabs_Caption ;
      private String lblTabs_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String sImgUrl ;
      private String imgTabprevious_Internalname ;
      private String imgTabprevious_Link ;
      private String imgTabnext_Internalname ;
      private String imgTabnext_Link ;
      private String WebComp_Component_Component ;
      private String gxDynCompUrl ;
      private String OldComponent ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV17TabLeftCls ;
      private String AV18TabRightCls ;
      private String AV13OnClickTemplate ;
      private String sCtrlGx_mode ;
      private String sCtrlAV19Tabs ;
      private String sCtrlAV16TabCode ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV10IsFirstTab ;
      private bool AV7Found ;
      private String AV24WebComponentUrl ;
      private GXWebComponent WebComp_Component ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXBaseCollection<SdtK2BTabOptions_K2BTabOptionsItem> AV19Tabs ;
      private SdtK2BTabOptions_K2BTabOptionsItem AV15Tab ;
   }

}
