/*
               File: EmergenciaBitacora_BC
        Description: Emergencia Bitacora
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:34:9.18
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class emergenciabitacora_bc : GXHttpHandler, IGxSilentTrn
   {
      public emergenciabitacora_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public emergenciabitacora_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow057( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey057( ) ;
         standaloneModal( ) ;
         AddRow057( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z43EmergenciaID = (Guid)(A43EmergenciaID);
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_050( )
      {
         BeforeValidate057( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls057( ) ;
            }
            else
            {
               CheckExtendedTable057( ) ;
               if ( AnyError == 0 )
               {
                  ZM057( 8) ;
                  ZM057( 9) ;
               }
               CloseExtendedTableCursors057( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM057( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            Z44EmergenciaFecha = A44EmergenciaFecha;
            Z45EmergenciaEstado = A45EmergenciaEstado;
            Z41EmergenciaPaciente = (Guid)(A41EmergenciaPaciente);
            Z42EmergenciaCoach = (Guid)(A42EmergenciaCoach);
         }
         if ( ( GX_JID == 8 ) || ( GX_JID == 0 ) )
         {
            Z49EmergenciaPacienteNombre = A49EmergenciaPacienteNombre;
         }
         if ( ( GX_JID == 9 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -7 )
         {
            Z43EmergenciaID = (Guid)(A43EmergenciaID);
            Z44EmergenciaFecha = A44EmergenciaFecha;
            Z45EmergenciaEstado = A45EmergenciaEstado;
            Z46EmergenciaLongitud = A46EmergenciaLongitud;
            Z47EmergenciaLatitud = A47EmergenciaLatitud;
            Z48EmergenciaMensaje = A48EmergenciaMensaje;
            Z41EmergenciaPaciente = (Guid)(A41EmergenciaPaciente);
            Z42EmergenciaCoach = (Guid)(A42EmergenciaCoach);
            Z49EmergenciaPacienteNombre = A49EmergenciaPacienteNombre;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A43EmergenciaID) )
         {
            A43EmergenciaID = (Guid)(Guid.NewGuid( ));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load057( )
      {
         /* Using cursor BC00056 */
         pr_default.execute(4, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound7 = 1;
            A44EmergenciaFecha = BC00056_A44EmergenciaFecha[0];
            A45EmergenciaEstado = BC00056_A45EmergenciaEstado[0];
            A49EmergenciaPacienteNombre = BC00056_A49EmergenciaPacienteNombre[0];
            n49EmergenciaPacienteNombre = BC00056_n49EmergenciaPacienteNombre[0];
            A46EmergenciaLongitud = BC00056_A46EmergenciaLongitud[0];
            n46EmergenciaLongitud = BC00056_n46EmergenciaLongitud[0];
            A47EmergenciaLatitud = BC00056_A47EmergenciaLatitud[0];
            n47EmergenciaLatitud = BC00056_n47EmergenciaLatitud[0];
            A48EmergenciaMensaje = BC00056_A48EmergenciaMensaje[0];
            n48EmergenciaMensaje = BC00056_n48EmergenciaMensaje[0];
            A41EmergenciaPaciente = (Guid)((Guid)(BC00056_A41EmergenciaPaciente[0]));
            n41EmergenciaPaciente = BC00056_n41EmergenciaPaciente[0];
            A42EmergenciaCoach = (Guid)((Guid)(BC00056_A42EmergenciaCoach[0]));
            n42EmergenciaCoach = BC00056_n42EmergenciaCoach[0];
            ZM057( -7) ;
         }
         pr_default.close(4);
         OnLoadActions057( ) ;
      }

      protected void OnLoadActions057( )
      {
      }

      protected void CheckExtendedTable057( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A44EmergenciaFecha) || ( A44EmergenciaFecha >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Emergencia Fecha fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A45EmergenciaEstado == 1 ) || ( A45EmergenciaEstado == 2 ) ) )
         {
            GX_msglist.addItem("Campo Emergencia Estado fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00054 */
         pr_default.execute(2, new Object[] {n41EmergenciaPaciente, A41EmergenciaPaciente});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (Guid.Empty==A41EmergenciaPaciente) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Paciente ST'.", "ForeignKeyNotFound", 1, "EMERGENCIAPACIENTE");
               AnyError = 1;
            }
         }
         A49EmergenciaPacienteNombre = BC00054_A49EmergenciaPacienteNombre[0];
         n49EmergenciaPacienteNombre = BC00054_n49EmergenciaPacienteNombre[0];
         pr_default.close(2);
         /* Using cursor BC00055 */
         pr_default.execute(3, new Object[] {n42EmergenciaCoach, A42EmergenciaCoach});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (Guid.Empty==A42EmergenciaCoach) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Coach ST'.", "ForeignKeyNotFound", 1, "EMERGENCIACOACH");
               AnyError = 1;
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors057( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey057( )
      {
         /* Using cursor BC00057 */
         pr_default.execute(5, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound7 = 1;
         }
         else
         {
            RcdFound7 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00053 */
         pr_default.execute(1, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM057( 7) ;
            RcdFound7 = 1;
            A43EmergenciaID = (Guid)((Guid)(BC00053_A43EmergenciaID[0]));
            A44EmergenciaFecha = BC00053_A44EmergenciaFecha[0];
            A45EmergenciaEstado = BC00053_A45EmergenciaEstado[0];
            A46EmergenciaLongitud = BC00053_A46EmergenciaLongitud[0];
            n46EmergenciaLongitud = BC00053_n46EmergenciaLongitud[0];
            A47EmergenciaLatitud = BC00053_A47EmergenciaLatitud[0];
            n47EmergenciaLatitud = BC00053_n47EmergenciaLatitud[0];
            A48EmergenciaMensaje = BC00053_A48EmergenciaMensaje[0];
            n48EmergenciaMensaje = BC00053_n48EmergenciaMensaje[0];
            A41EmergenciaPaciente = (Guid)((Guid)(BC00053_A41EmergenciaPaciente[0]));
            n41EmergenciaPaciente = BC00053_n41EmergenciaPaciente[0];
            A42EmergenciaCoach = (Guid)((Guid)(BC00053_A42EmergenciaCoach[0]));
            n42EmergenciaCoach = BC00053_n42EmergenciaCoach[0];
            Z43EmergenciaID = (Guid)(A43EmergenciaID);
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load057( ) ;
            if ( AnyError == 1 )
            {
               RcdFound7 = 0;
               InitializeNonKey057( ) ;
            }
            Gx_mode = sMode7;
         }
         else
         {
            RcdFound7 = 0;
            InitializeNonKey057( ) ;
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode7;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey057( ) ;
         if ( RcdFound7 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_050( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency057( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00052 */
            pr_default.execute(0, new Object[] {A43EmergenciaID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"EmergenciaBitacora"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z44EmergenciaFecha != BC00052_A44EmergenciaFecha[0] ) || ( Z45EmergenciaEstado != BC00052_A45EmergenciaEstado[0] ) || ( Z41EmergenciaPaciente != BC00052_A41EmergenciaPaciente[0] ) || ( Z42EmergenciaCoach != BC00052_A42EmergenciaCoach[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"EmergenciaBitacora"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert057( )
      {
         BeforeValidate057( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable057( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM057( 0) ;
            CheckOptimisticConcurrency057( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm057( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert057( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00058 */
                     pr_default.execute(6, new Object[] {A44EmergenciaFecha, A45EmergenciaEstado, n46EmergenciaLongitud, A46EmergenciaLongitud, n47EmergenciaLatitud, A47EmergenciaLatitud, n48EmergenciaMensaje, A48EmergenciaMensaje, n41EmergenciaPaciente, A41EmergenciaPaciente, n42EmergenciaCoach, A42EmergenciaCoach, A43EmergenciaID});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("EmergenciaBitacora") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load057( ) ;
            }
            EndLevel057( ) ;
         }
         CloseExtendedTableCursors057( ) ;
      }

      protected void Update057( )
      {
         BeforeValidate057( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable057( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency057( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm057( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate057( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00059 */
                     pr_default.execute(7, new Object[] {A44EmergenciaFecha, A45EmergenciaEstado, n46EmergenciaLongitud, A46EmergenciaLongitud, n47EmergenciaLatitud, A47EmergenciaLatitud, n48EmergenciaMensaje, A48EmergenciaMensaje, n41EmergenciaPaciente, A41EmergenciaPaciente, n42EmergenciaCoach, A42EmergenciaCoach, A43EmergenciaID});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("EmergenciaBitacora") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"EmergenciaBitacora"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate057( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel057( ) ;
         }
         CloseExtendedTableCursors057( ) ;
      }

      protected void DeferredUpdate057( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate057( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency057( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls057( ) ;
            AfterConfirm057( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete057( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000510 */
                  pr_default.execute(8, new Object[] {A43EmergenciaID});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("EmergenciaBitacora") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode7 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel057( ) ;
         Gx_mode = sMode7;
      }

      protected void OnDeleteControls057( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000511 */
            pr_default.execute(9, new Object[] {n41EmergenciaPaciente, A41EmergenciaPaciente});
            A49EmergenciaPacienteNombre = BC000511_A49EmergenciaPacienteNombre[0];
            n49EmergenciaPacienteNombre = BC000511_n49EmergenciaPacienteNombre[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel057( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete057( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart057( )
      {
         /* Using cursor BC000512 */
         pr_default.execute(10, new Object[] {A43EmergenciaID});
         RcdFound7 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound7 = 1;
            A43EmergenciaID = (Guid)((Guid)(BC000512_A43EmergenciaID[0]));
            A44EmergenciaFecha = BC000512_A44EmergenciaFecha[0];
            A45EmergenciaEstado = BC000512_A45EmergenciaEstado[0];
            A49EmergenciaPacienteNombre = BC000512_A49EmergenciaPacienteNombre[0];
            n49EmergenciaPacienteNombre = BC000512_n49EmergenciaPacienteNombre[0];
            A46EmergenciaLongitud = BC000512_A46EmergenciaLongitud[0];
            n46EmergenciaLongitud = BC000512_n46EmergenciaLongitud[0];
            A47EmergenciaLatitud = BC000512_A47EmergenciaLatitud[0];
            n47EmergenciaLatitud = BC000512_n47EmergenciaLatitud[0];
            A48EmergenciaMensaje = BC000512_A48EmergenciaMensaje[0];
            n48EmergenciaMensaje = BC000512_n48EmergenciaMensaje[0];
            A41EmergenciaPaciente = (Guid)((Guid)(BC000512_A41EmergenciaPaciente[0]));
            n41EmergenciaPaciente = BC000512_n41EmergenciaPaciente[0];
            A42EmergenciaCoach = (Guid)((Guid)(BC000512_A42EmergenciaCoach[0]));
            n42EmergenciaCoach = BC000512_n42EmergenciaCoach[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext057( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound7 = 0;
         ScanKeyLoad057( ) ;
      }

      protected void ScanKeyLoad057( )
      {
         sMode7 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound7 = 1;
            A43EmergenciaID = (Guid)((Guid)(BC000512_A43EmergenciaID[0]));
            A44EmergenciaFecha = BC000512_A44EmergenciaFecha[0];
            A45EmergenciaEstado = BC000512_A45EmergenciaEstado[0];
            A49EmergenciaPacienteNombre = BC000512_A49EmergenciaPacienteNombre[0];
            n49EmergenciaPacienteNombre = BC000512_n49EmergenciaPacienteNombre[0];
            A46EmergenciaLongitud = BC000512_A46EmergenciaLongitud[0];
            n46EmergenciaLongitud = BC000512_n46EmergenciaLongitud[0];
            A47EmergenciaLatitud = BC000512_A47EmergenciaLatitud[0];
            n47EmergenciaLatitud = BC000512_n47EmergenciaLatitud[0];
            A48EmergenciaMensaje = BC000512_A48EmergenciaMensaje[0];
            n48EmergenciaMensaje = BC000512_n48EmergenciaMensaje[0];
            A41EmergenciaPaciente = (Guid)((Guid)(BC000512_A41EmergenciaPaciente[0]));
            n41EmergenciaPaciente = BC000512_n41EmergenciaPaciente[0];
            A42EmergenciaCoach = (Guid)((Guid)(BC000512_A42EmergenciaCoach[0]));
            n42EmergenciaCoach = BC000512_n42EmergenciaCoach[0];
         }
         Gx_mode = sMode7;
      }

      protected void ScanKeyEnd057( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm057( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert057( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate057( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete057( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete057( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate057( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes057( )
      {
      }

      protected void send_integrity_lvl_hashes057( )
      {
      }

      protected void AddRow057( )
      {
         VarsToRow7( bcEmergenciaBitacora) ;
      }

      protected void ReadRow057( )
      {
         RowToVars7( bcEmergenciaBitacora, 1) ;
      }

      protected void InitializeNonKey057( )
      {
         A44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         A45EmergenciaEstado = 0;
         A41EmergenciaPaciente = (Guid)(Guid.Empty);
         n41EmergenciaPaciente = false;
         A49EmergenciaPacienteNombre = "";
         n49EmergenciaPacienteNombre = false;
         A42EmergenciaCoach = (Guid)(Guid.Empty);
         n42EmergenciaCoach = false;
         A46EmergenciaLongitud = "";
         n46EmergenciaLongitud = false;
         A47EmergenciaLatitud = "";
         n47EmergenciaLatitud = false;
         A48EmergenciaMensaje = "";
         n48EmergenciaMensaje = false;
         Z44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         Z45EmergenciaEstado = 0;
         Z41EmergenciaPaciente = (Guid)(Guid.Empty);
         Z42EmergenciaCoach = (Guid)(Guid.Empty);
      }

      protected void InitAll057( )
      {
         A43EmergenciaID = (Guid)(Guid.NewGuid( ));
         InitializeNonKey057( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow7( SdtEmergenciaBitacora obj7 )
      {
         obj7.gxTpr_Mode = Gx_mode;
         obj7.gxTpr_Emergenciafecha = A44EmergenciaFecha;
         obj7.gxTpr_Emergenciaestado = A45EmergenciaEstado;
         obj7.gxTpr_Emergenciapaciente = (Guid)(A41EmergenciaPaciente);
         obj7.gxTpr_Emergenciapacientenombre = A49EmergenciaPacienteNombre;
         obj7.gxTpr_Emergenciacoach = (Guid)(A42EmergenciaCoach);
         obj7.gxTpr_Emergencialongitud = A46EmergenciaLongitud;
         obj7.gxTpr_Emergencialatitud = A47EmergenciaLatitud;
         obj7.gxTpr_Emergenciamensaje = A48EmergenciaMensaje;
         obj7.gxTpr_Emergenciaid = (Guid)(A43EmergenciaID);
         obj7.gxTpr_Emergenciaid_Z = (Guid)(Z43EmergenciaID);
         obj7.gxTpr_Emergenciafecha_Z = Z44EmergenciaFecha;
         obj7.gxTpr_Emergenciaestado_Z = Z45EmergenciaEstado;
         obj7.gxTpr_Emergenciapaciente_Z = (Guid)(Z41EmergenciaPaciente);
         obj7.gxTpr_Emergenciapacientenombre_Z = Z49EmergenciaPacienteNombre;
         obj7.gxTpr_Emergenciacoach_Z = (Guid)(Z42EmergenciaCoach);
         obj7.gxTpr_Emergenciapaciente_N = (short)(Convert.ToInt16(n41EmergenciaPaciente));
         obj7.gxTpr_Emergenciapacientenombre_N = (short)(Convert.ToInt16(n49EmergenciaPacienteNombre));
         obj7.gxTpr_Emergenciacoach_N = (short)(Convert.ToInt16(n42EmergenciaCoach));
         obj7.gxTpr_Emergencialongitud_N = (short)(Convert.ToInt16(n46EmergenciaLongitud));
         obj7.gxTpr_Emergencialatitud_N = (short)(Convert.ToInt16(n47EmergenciaLatitud));
         obj7.gxTpr_Emergenciamensaje_N = (short)(Convert.ToInt16(n48EmergenciaMensaje));
         obj7.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow7( SdtEmergenciaBitacora obj7 )
      {
         obj7.gxTpr_Emergenciaid = (Guid)(A43EmergenciaID);
         return  ;
      }

      public void RowToVars7( SdtEmergenciaBitacora obj7 ,
                              int forceLoad )
      {
         Gx_mode = obj7.gxTpr_Mode;
         A44EmergenciaFecha = obj7.gxTpr_Emergenciafecha;
         A45EmergenciaEstado = obj7.gxTpr_Emergenciaestado;
         A41EmergenciaPaciente = (Guid)(obj7.gxTpr_Emergenciapaciente);
         n41EmergenciaPaciente = false;
         A49EmergenciaPacienteNombre = obj7.gxTpr_Emergenciapacientenombre;
         n49EmergenciaPacienteNombre = false;
         A42EmergenciaCoach = (Guid)(obj7.gxTpr_Emergenciacoach);
         n42EmergenciaCoach = false;
         A46EmergenciaLongitud = obj7.gxTpr_Emergencialongitud;
         n46EmergenciaLongitud = false;
         A47EmergenciaLatitud = obj7.gxTpr_Emergencialatitud;
         n47EmergenciaLatitud = false;
         A48EmergenciaMensaje = obj7.gxTpr_Emergenciamensaje;
         n48EmergenciaMensaje = false;
         A43EmergenciaID = (Guid)(obj7.gxTpr_Emergenciaid);
         Z43EmergenciaID = (Guid)(obj7.gxTpr_Emergenciaid_Z);
         Z44EmergenciaFecha = obj7.gxTpr_Emergenciafecha_Z;
         Z45EmergenciaEstado = obj7.gxTpr_Emergenciaestado_Z;
         Z41EmergenciaPaciente = (Guid)(obj7.gxTpr_Emergenciapaciente_Z);
         Z49EmergenciaPacienteNombre = obj7.gxTpr_Emergenciapacientenombre_Z;
         Z42EmergenciaCoach = (Guid)(obj7.gxTpr_Emergenciacoach_Z);
         n41EmergenciaPaciente = (bool)(Convert.ToBoolean(obj7.gxTpr_Emergenciapaciente_N));
         n49EmergenciaPacienteNombre = (bool)(Convert.ToBoolean(obj7.gxTpr_Emergenciapacientenombre_N));
         n42EmergenciaCoach = (bool)(Convert.ToBoolean(obj7.gxTpr_Emergenciacoach_N));
         n46EmergenciaLongitud = (bool)(Convert.ToBoolean(obj7.gxTpr_Emergencialongitud_N));
         n47EmergenciaLatitud = (bool)(Convert.ToBoolean(obj7.gxTpr_Emergencialatitud_N));
         n48EmergenciaMensaje = (bool)(Convert.ToBoolean(obj7.gxTpr_Emergenciamensaje_N));
         Gx_mode = obj7.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A43EmergenciaID = (Guid)((Guid)getParm(obj,0));
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey057( ) ;
         ScanKeyStart057( ) ;
         if ( RcdFound7 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z43EmergenciaID = (Guid)(A43EmergenciaID);
         }
         ZM057( -7) ;
         OnLoadActions057( ) ;
         AddRow057( ) ;
         ScanKeyEnd057( ) ;
         if ( RcdFound7 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars7( bcEmergenciaBitacora, 0) ;
         ScanKeyStart057( ) ;
         if ( RcdFound7 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z43EmergenciaID = (Guid)(A43EmergenciaID);
         }
         ZM057( -7) ;
         OnLoadActions057( ) ;
         AddRow057( ) ;
         ScanKeyEnd057( ) ;
         if ( RcdFound7 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      protected void SaveImpl( )
      {
         nKeyPressed = 1;
         GetKey057( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert057( ) ;
         }
         else
         {
            if ( RcdFound7 == 1 )
            {
               if ( A43EmergenciaID != Z43EmergenciaID )
               {
                  A43EmergenciaID = (Guid)(Z43EmergenciaID);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update057( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A43EmergenciaID != Z43EmergenciaID )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert057( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert057( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars7( bcEmergenciaBitacora, 0) ;
         SaveImpl( ) ;
         VarsToRow7( bcEmergenciaBitacora) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public bool Insert( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars7( bcEmergenciaBitacora, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert057( ) ;
         AfterTrn( ) ;
         VarsToRow7( bcEmergenciaBitacora) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      protected void UpdateImpl( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            SaveImpl( ) ;
         }
         else
         {
            SdtEmergenciaBitacora auxBC = new SdtEmergenciaBitacora(context) ;
            auxBC.Load(A43EmergenciaID);
            auxBC.UpdateDirties(bcEmergenciaBitacora);
            auxBC.Save();
            IGxSilentTrn auxTrn = auxBC.getTransaction() ;
            LclMsgLst = (msglist)(auxTrn.GetMessages());
            AnyError = (short)(auxTrn.Errors());
            Gx_mode = auxTrn.GetMode();
            context.GX_msglist = LclMsgLst;
            AfterTrn( ) ;
         }
      }

      public bool Update( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars7( bcEmergenciaBitacora, 0) ;
         UpdateImpl( ) ;
         VarsToRow7( bcEmergenciaBitacora) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public bool InsertOrUpdate( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars7( bcEmergenciaBitacora, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert057( ) ;
         if ( AnyError == 1 )
         {
            AnyError = 0;
            context.GX_msglist.removeAllItems();
            UpdateImpl( ) ;
         }
         else
         {
            AfterTrn( ) ;
         }
         VarsToRow7( bcEmergenciaBitacora) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars7( bcEmergenciaBitacora, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey057( ) ;
         if ( RcdFound7 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A43EmergenciaID != Z43EmergenciaID )
            {
               A43EmergenciaID = (Guid)(Z43EmergenciaID);
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A43EmergenciaID != Z43EmergenciaID )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(9);
         pr_gam.rollback( "EmergenciaBitacora_BC");
         pr_default.rollback( "EmergenciaBitacora_BC");
         VarsToRow7( bcEmergenciaBitacora) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcEmergenciaBitacora.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcEmergenciaBitacora.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcEmergenciaBitacora )
         {
            bcEmergenciaBitacora = (SdtEmergenciaBitacora)(sdt);
            if ( StringUtil.StrCmp(bcEmergenciaBitacora.gxTpr_Mode, "") == 0 )
            {
               bcEmergenciaBitacora.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow7( bcEmergenciaBitacora) ;
            }
            else
            {
               RowToVars7( bcEmergenciaBitacora, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcEmergenciaBitacora.gxTpr_Mode, "") == 0 )
            {
               bcEmergenciaBitacora.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars7( bcEmergenciaBitacora, 1) ;
         return  ;
      }

      public SdtEmergenciaBitacora EmergenciaBitacora_BC
      {
         get {
            return bcEmergenciaBitacora ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "emergenciabitacora_Execute" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z43EmergenciaID = (Guid)(Guid.Empty);
         A43EmergenciaID = (Guid)(Guid.Empty);
         Z44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         A44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         Z41EmergenciaPaciente = (Guid)(Guid.Empty);
         A41EmergenciaPaciente = (Guid)(Guid.Empty);
         Z42EmergenciaCoach = (Guid)(Guid.Empty);
         A42EmergenciaCoach = (Guid)(Guid.Empty);
         Z49EmergenciaPacienteNombre = "";
         A49EmergenciaPacienteNombre = "";
         Z46EmergenciaLongitud = "";
         A46EmergenciaLongitud = "";
         Z47EmergenciaLatitud = "";
         A47EmergenciaLatitud = "";
         Z48EmergenciaMensaje = "";
         A48EmergenciaMensaje = "";
         BC00056_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         BC00056_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         BC00056_A45EmergenciaEstado = new short[1] ;
         BC00056_A49EmergenciaPacienteNombre = new String[] {""} ;
         BC00056_n49EmergenciaPacienteNombre = new bool[] {false} ;
         BC00056_A46EmergenciaLongitud = new String[] {""} ;
         BC00056_n46EmergenciaLongitud = new bool[] {false} ;
         BC00056_A47EmergenciaLatitud = new String[] {""} ;
         BC00056_n47EmergenciaLatitud = new bool[] {false} ;
         BC00056_A48EmergenciaMensaje = new String[] {""} ;
         BC00056_n48EmergenciaMensaje = new bool[] {false} ;
         BC00056_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         BC00056_n41EmergenciaPaciente = new bool[] {false} ;
         BC00056_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         BC00056_n42EmergenciaCoach = new bool[] {false} ;
         BC00054_A49EmergenciaPacienteNombre = new String[] {""} ;
         BC00054_n49EmergenciaPacienteNombre = new bool[] {false} ;
         BC00055_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         BC00055_n42EmergenciaCoach = new bool[] {false} ;
         BC00057_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         BC00053_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         BC00053_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         BC00053_A45EmergenciaEstado = new short[1] ;
         BC00053_A46EmergenciaLongitud = new String[] {""} ;
         BC00053_n46EmergenciaLongitud = new bool[] {false} ;
         BC00053_A47EmergenciaLatitud = new String[] {""} ;
         BC00053_n47EmergenciaLatitud = new bool[] {false} ;
         BC00053_A48EmergenciaMensaje = new String[] {""} ;
         BC00053_n48EmergenciaMensaje = new bool[] {false} ;
         BC00053_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         BC00053_n41EmergenciaPaciente = new bool[] {false} ;
         BC00053_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         BC00053_n42EmergenciaCoach = new bool[] {false} ;
         sMode7 = "";
         BC00052_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         BC00052_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         BC00052_A45EmergenciaEstado = new short[1] ;
         BC00052_A46EmergenciaLongitud = new String[] {""} ;
         BC00052_n46EmergenciaLongitud = new bool[] {false} ;
         BC00052_A47EmergenciaLatitud = new String[] {""} ;
         BC00052_n47EmergenciaLatitud = new bool[] {false} ;
         BC00052_A48EmergenciaMensaje = new String[] {""} ;
         BC00052_n48EmergenciaMensaje = new bool[] {false} ;
         BC00052_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         BC00052_n41EmergenciaPaciente = new bool[] {false} ;
         BC00052_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         BC00052_n42EmergenciaCoach = new bool[] {false} ;
         BC000511_A49EmergenciaPacienteNombre = new String[] {""} ;
         BC000511_n49EmergenciaPacienteNombre = new bool[] {false} ;
         BC000512_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         BC000512_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         BC000512_A45EmergenciaEstado = new short[1] ;
         BC000512_A49EmergenciaPacienteNombre = new String[] {""} ;
         BC000512_n49EmergenciaPacienteNombre = new bool[] {false} ;
         BC000512_A46EmergenciaLongitud = new String[] {""} ;
         BC000512_n46EmergenciaLongitud = new bool[] {false} ;
         BC000512_A47EmergenciaLatitud = new String[] {""} ;
         BC000512_n47EmergenciaLatitud = new bool[] {false} ;
         BC000512_A48EmergenciaMensaje = new String[] {""} ;
         BC000512_n48EmergenciaMensaje = new bool[] {false} ;
         BC000512_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         BC000512_n41EmergenciaPaciente = new bool[] {false} ;
         BC000512_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         BC000512_n42EmergenciaCoach = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.emergenciabitacora_bc__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.emergenciabitacora_bc__default(),
            new Object[][] {
                new Object[] {
               BC00052_A43EmergenciaID, BC00052_A44EmergenciaFecha, BC00052_A45EmergenciaEstado, BC00052_A46EmergenciaLongitud, BC00052_n46EmergenciaLongitud, BC00052_A47EmergenciaLatitud, BC00052_n47EmergenciaLatitud, BC00052_A48EmergenciaMensaje, BC00052_n48EmergenciaMensaje, BC00052_A41EmergenciaPaciente,
               BC00052_n41EmergenciaPaciente, BC00052_A42EmergenciaCoach, BC00052_n42EmergenciaCoach
               }
               , new Object[] {
               BC00053_A43EmergenciaID, BC00053_A44EmergenciaFecha, BC00053_A45EmergenciaEstado, BC00053_A46EmergenciaLongitud, BC00053_n46EmergenciaLongitud, BC00053_A47EmergenciaLatitud, BC00053_n47EmergenciaLatitud, BC00053_A48EmergenciaMensaje, BC00053_n48EmergenciaMensaje, BC00053_A41EmergenciaPaciente,
               BC00053_n41EmergenciaPaciente, BC00053_A42EmergenciaCoach, BC00053_n42EmergenciaCoach
               }
               , new Object[] {
               BC00054_A49EmergenciaPacienteNombre, BC00054_n49EmergenciaPacienteNombre
               }
               , new Object[] {
               BC00055_A42EmergenciaCoach
               }
               , new Object[] {
               BC00056_A43EmergenciaID, BC00056_A44EmergenciaFecha, BC00056_A45EmergenciaEstado, BC00056_A49EmergenciaPacienteNombre, BC00056_n49EmergenciaPacienteNombre, BC00056_A46EmergenciaLongitud, BC00056_n46EmergenciaLongitud, BC00056_A47EmergenciaLatitud, BC00056_n47EmergenciaLatitud, BC00056_A48EmergenciaMensaje,
               BC00056_n48EmergenciaMensaje, BC00056_A41EmergenciaPaciente, BC00056_n41EmergenciaPaciente, BC00056_A42EmergenciaCoach, BC00056_n42EmergenciaCoach
               }
               , new Object[] {
               BC00057_A43EmergenciaID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000511_A49EmergenciaPacienteNombre, BC000511_n49EmergenciaPacienteNombre
               }
               , new Object[] {
               BC000512_A43EmergenciaID, BC000512_A44EmergenciaFecha, BC000512_A45EmergenciaEstado, BC000512_A49EmergenciaPacienteNombre, BC000512_n49EmergenciaPacienteNombre, BC000512_A46EmergenciaLongitud, BC000512_n46EmergenciaLongitud, BC000512_A47EmergenciaLatitud, BC000512_n47EmergenciaLatitud, BC000512_A48EmergenciaMensaje,
               BC000512_n48EmergenciaMensaje, BC000512_A41EmergenciaPaciente, BC000512_n41EmergenciaPaciente, BC000512_A42EmergenciaCoach, BC000512_n42EmergenciaCoach
               }
            }
         );
         Z43EmergenciaID = (Guid)(Guid.NewGuid( ));
         A43EmergenciaID = (Guid)(Guid.NewGuid( ));
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z45EmergenciaEstado ;
      private short A45EmergenciaEstado ;
      private short Gx_BScreen ;
      private short RcdFound7 ;
      private int trnEnded ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode7 ;
      private DateTime Z44EmergenciaFecha ;
      private DateTime A44EmergenciaFecha ;
      private bool n49EmergenciaPacienteNombre ;
      private bool n46EmergenciaLongitud ;
      private bool n47EmergenciaLatitud ;
      private bool n48EmergenciaMensaje ;
      private bool n41EmergenciaPaciente ;
      private bool n42EmergenciaCoach ;
      private String Z46EmergenciaLongitud ;
      private String A46EmergenciaLongitud ;
      private String Z47EmergenciaLatitud ;
      private String A47EmergenciaLatitud ;
      private String Z48EmergenciaMensaje ;
      private String A48EmergenciaMensaje ;
      private String Z49EmergenciaPacienteNombre ;
      private String A49EmergenciaPacienteNombre ;
      private Guid Z43EmergenciaID ;
      private Guid A43EmergenciaID ;
      private Guid Z41EmergenciaPaciente ;
      private Guid A41EmergenciaPaciente ;
      private Guid Z42EmergenciaCoach ;
      private Guid A42EmergenciaCoach ;
      private SdtEmergenciaBitacora bcEmergenciaBitacora ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] BC00056_A43EmergenciaID ;
      private DateTime[] BC00056_A44EmergenciaFecha ;
      private short[] BC00056_A45EmergenciaEstado ;
      private String[] BC00056_A49EmergenciaPacienteNombre ;
      private bool[] BC00056_n49EmergenciaPacienteNombre ;
      private String[] BC00056_A46EmergenciaLongitud ;
      private bool[] BC00056_n46EmergenciaLongitud ;
      private String[] BC00056_A47EmergenciaLatitud ;
      private bool[] BC00056_n47EmergenciaLatitud ;
      private String[] BC00056_A48EmergenciaMensaje ;
      private bool[] BC00056_n48EmergenciaMensaje ;
      private Guid[] BC00056_A41EmergenciaPaciente ;
      private bool[] BC00056_n41EmergenciaPaciente ;
      private Guid[] BC00056_A42EmergenciaCoach ;
      private bool[] BC00056_n42EmergenciaCoach ;
      private String[] BC00054_A49EmergenciaPacienteNombre ;
      private bool[] BC00054_n49EmergenciaPacienteNombre ;
      private Guid[] BC00055_A42EmergenciaCoach ;
      private bool[] BC00055_n42EmergenciaCoach ;
      private Guid[] BC00057_A43EmergenciaID ;
      private Guid[] BC00053_A43EmergenciaID ;
      private DateTime[] BC00053_A44EmergenciaFecha ;
      private short[] BC00053_A45EmergenciaEstado ;
      private String[] BC00053_A46EmergenciaLongitud ;
      private bool[] BC00053_n46EmergenciaLongitud ;
      private String[] BC00053_A47EmergenciaLatitud ;
      private bool[] BC00053_n47EmergenciaLatitud ;
      private String[] BC00053_A48EmergenciaMensaje ;
      private bool[] BC00053_n48EmergenciaMensaje ;
      private Guid[] BC00053_A41EmergenciaPaciente ;
      private bool[] BC00053_n41EmergenciaPaciente ;
      private Guid[] BC00053_A42EmergenciaCoach ;
      private bool[] BC00053_n42EmergenciaCoach ;
      private Guid[] BC00052_A43EmergenciaID ;
      private DateTime[] BC00052_A44EmergenciaFecha ;
      private short[] BC00052_A45EmergenciaEstado ;
      private String[] BC00052_A46EmergenciaLongitud ;
      private bool[] BC00052_n46EmergenciaLongitud ;
      private String[] BC00052_A47EmergenciaLatitud ;
      private bool[] BC00052_n47EmergenciaLatitud ;
      private String[] BC00052_A48EmergenciaMensaje ;
      private bool[] BC00052_n48EmergenciaMensaje ;
      private Guid[] BC00052_A41EmergenciaPaciente ;
      private bool[] BC00052_n41EmergenciaPaciente ;
      private Guid[] BC00052_A42EmergenciaCoach ;
      private bool[] BC00052_n42EmergenciaCoach ;
      private String[] BC000511_A49EmergenciaPacienteNombre ;
      private bool[] BC000511_n49EmergenciaPacienteNombre ;
      private Guid[] BC000512_A43EmergenciaID ;
      private DateTime[] BC000512_A44EmergenciaFecha ;
      private short[] BC000512_A45EmergenciaEstado ;
      private String[] BC000512_A49EmergenciaPacienteNombre ;
      private bool[] BC000512_n49EmergenciaPacienteNombre ;
      private String[] BC000512_A46EmergenciaLongitud ;
      private bool[] BC000512_n46EmergenciaLongitud ;
      private String[] BC000512_A47EmergenciaLatitud ;
      private bool[] BC000512_n47EmergenciaLatitud ;
      private String[] BC000512_A48EmergenciaMensaje ;
      private bool[] BC000512_n48EmergenciaMensaje ;
      private Guid[] BC000512_A41EmergenciaPaciente ;
      private bool[] BC000512_n41EmergenciaPaciente ;
      private Guid[] BC000512_A42EmergenciaCoach ;
      private bool[] BC000512_n42EmergenciaCoach ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_gam ;
   }

   public class emergenciabitacora_bc__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class emergenciabitacora_bc__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new UpdateCursor(def[6])
       ,new UpdateCursor(def[7])
       ,new UpdateCursor(def[8])
       ,new ForEachCursor(def[9])
       ,new ForEachCursor(def[10])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmBC00056 ;
        prmBC00056 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00054 ;
        prmBC00054 = new Object[] {
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00055 ;
        prmBC00055 = new Object[] {
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00057 ;
        prmBC00057 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00053 ;
        prmBC00053 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00052 ;
        prmBC00052 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00058 ;
        prmBC00058 = new Object[] {
        new Object[] {"@EmergenciaFecha",SqlDbType.DateTime,8,5} ,
        new Object[] {"@EmergenciaEstado",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@EmergenciaLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaMensaje",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00059 ;
        prmBC00059 = new Object[] {
        new Object[] {"@EmergenciaFecha",SqlDbType.DateTime,8,5} ,
        new Object[] {"@EmergenciaEstado",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@EmergenciaLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaMensaje",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000510 ;
        prmBC000510 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000511 ;
        prmBC000511 = new Object[] {
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000512 ;
        prmBC000512 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("BC00052", "SELECT [EmergenciaID], [EmergenciaFecha], [EmergenciaEstado], [EmergenciaLongitud], [EmergenciaLatitud], [EmergenciaMensaje], [EmergenciaPaciente] AS EmergenciaPaciente, [EmergenciaCoach] AS EmergenciaCoach FROM [EmergenciaBitacora] WITH (UPDLOCK) WHERE [EmergenciaID] = @EmergenciaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00052,1,0,true,false )
           ,new CursorDef("BC00053", "SELECT [EmergenciaID], [EmergenciaFecha], [EmergenciaEstado], [EmergenciaLongitud], [EmergenciaLatitud], [EmergenciaMensaje], [EmergenciaPaciente] AS EmergenciaPaciente, [EmergenciaCoach] AS EmergenciaCoach FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaID] = @EmergenciaID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00053,1,0,true,false )
           ,new CursorDef("BC00054", "SELECT [PersonaPNombre] AS EmergenciaPacienteNombre FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaPaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00054,1,0,true,false )
           ,new CursorDef("BC00055", "SELECT [PersonaID] AS EmergenciaCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00055,1,0,true,false )
           ,new CursorDef("BC00056", "SELECT TM1.[EmergenciaID], TM1.[EmergenciaFecha], TM1.[EmergenciaEstado], T2.[PersonaPNombre] AS EmergenciaPacienteNombre, TM1.[EmergenciaLongitud], TM1.[EmergenciaLatitud], TM1.[EmergenciaMensaje], TM1.[EmergenciaPaciente] AS EmergenciaPaciente, TM1.[EmergenciaCoach] AS EmergenciaCoach FROM ([EmergenciaBitacora] TM1 WITH (NOLOCK) LEFT JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = TM1.[EmergenciaPaciente]) WHERE TM1.[EmergenciaID] = @EmergenciaID ORDER BY TM1.[EmergenciaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00056,100,0,true,false )
           ,new CursorDef("BC00057", "SELECT [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaID] = @EmergenciaID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00057,1,0,true,false )
           ,new CursorDef("BC00058", "INSERT INTO [EmergenciaBitacora]([EmergenciaFecha], [EmergenciaEstado], [EmergenciaLongitud], [EmergenciaLatitud], [EmergenciaMensaje], [EmergenciaPaciente], [EmergenciaCoach], [EmergenciaID]) VALUES(@EmergenciaFecha, @EmergenciaEstado, @EmergenciaLongitud, @EmergenciaLatitud, @EmergenciaMensaje, @EmergenciaPaciente, @EmergenciaCoach, @EmergenciaID)", GxErrorMask.GX_NOMASK,prmBC00058)
           ,new CursorDef("BC00059", "UPDATE [EmergenciaBitacora] SET [EmergenciaFecha]=@EmergenciaFecha, [EmergenciaEstado]=@EmergenciaEstado, [EmergenciaLongitud]=@EmergenciaLongitud, [EmergenciaLatitud]=@EmergenciaLatitud, [EmergenciaMensaje]=@EmergenciaMensaje, [EmergenciaPaciente]=@EmergenciaPaciente, [EmergenciaCoach]=@EmergenciaCoach  WHERE [EmergenciaID] = @EmergenciaID", GxErrorMask.GX_NOMASK,prmBC00059)
           ,new CursorDef("BC000510", "DELETE FROM [EmergenciaBitacora]  WHERE [EmergenciaID] = @EmergenciaID", GxErrorMask.GX_NOMASK,prmBC000510)
           ,new CursorDef("BC000511", "SELECT [PersonaPNombre] AS EmergenciaPacienteNombre FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaPaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000511,1,0,true,false )
           ,new CursorDef("BC000512", "SELECT TM1.[EmergenciaID], TM1.[EmergenciaFecha], TM1.[EmergenciaEstado], T2.[PersonaPNombre] AS EmergenciaPacienteNombre, TM1.[EmergenciaLongitud], TM1.[EmergenciaLatitud], TM1.[EmergenciaMensaje], TM1.[EmergenciaPaciente] AS EmergenciaPaciente, TM1.[EmergenciaCoach] AS EmergenciaCoach FROM ([EmergenciaBitacora] TM1 WITH (NOLOCK) LEFT JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = TM1.[EmergenciaPaciente]) WHERE TM1.[EmergenciaID] = @EmergenciaID ORDER BY TM1.[EmergenciaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000512,100,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[4])[0] = rslt.wasNull(4);
              ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((Guid[]) buf[11])[0] = rslt.getGuid(8) ;
              ((bool[]) buf[12])[0] = rslt.wasNull(8);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[4])[0] = rslt.wasNull(4);
              ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((Guid[]) buf[11])[0] = rslt.getGuid(8) ;
              ((bool[]) buf[12])[0] = rslt.wasNull(8);
              return;
           case 2 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 4 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[4])[0] = rslt.wasNull(4);
              ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((Guid[]) buf[11])[0] = rslt.getGuid(8) ;
              ((bool[]) buf[12])[0] = rslt.wasNull(8);
              ((Guid[]) buf[13])[0] = rslt.getGuid(9) ;
              ((bool[]) buf[14])[0] = rslt.wasNull(9);
              return;
           case 5 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 9 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              return;
           case 10 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[4])[0] = rslt.wasNull(4);
              ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((Guid[]) buf[11])[0] = rslt.getGuid(8) ;
              ((bool[]) buf[12])[0] = rslt.wasNull(8);
              ((Guid[]) buf[13])[0] = rslt.getGuid(9) ;
              ((bool[]) buf[14])[0] = rslt.wasNull(9);
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 2 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 3 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 5 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 6 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[9]);
              }
              if ( (bool)parms[10] )
              {
                 stmt.setNull( 7 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(7, (Guid)parms[11]);
              }
              stmt.SetParameter(8, (Guid)parms[12]);
              return;
           case 7 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[9]);
              }
              if ( (bool)parms[10] )
              {
                 stmt.setNull( 7 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(7, (Guid)parms[11]);
              }
              stmt.SetParameter(8, (Guid)parms[12]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 9 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 10 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
