/*
               File: GAMExampleAppMenuOptionEntry
        Description: GAMExample App Menu Option Entry
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:4:22.99
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleappmenuoptionentry : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleappmenuoptionentry( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gamexampleappmenuoptionentry( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref long aP1_ApplicationId ,
                           ref long aP2_MenuId ,
                           ref long aP3_Id )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV10ApplicationId = aP1_ApplicationId;
         this.AV22MenuId = aP2_MenuId;
         this.AV18Id = aP3_Id;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_ApplicationId=this.AV10ApplicationId;
         aP2_MenuId=this.AV22MenuId;
         aP3_Id=this.AV18Id;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavType = new GXCombobox();
         cmbavMenusid = new GXCombobox();
         cmbavRelresid = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV10ApplicationId = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ApplicationId), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
                  AV22MenuId = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22MenuId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22MenuId), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMENUID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22MenuId), "ZZZZZZZZZZZ9"), context));
                  AV18Id = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Id), 12, 0)));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Id), "ZZZZZZZZZZZ9"), context));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "gamexampleappmenuoptionentry_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpage", "GeneXus.Programs.gammasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA2W2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START2W2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171542315", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gamexampleappmenuoptionentry.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV10ApplicationId) + "," + UrlEncode("" +AV22MenuId) + "," + UrlEncode("" +AV18Id)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "vAPPLICATIONID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10ApplicationId), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "vMENUID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22MenuId), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMENUID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22MenuId), "ZZZZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "vID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Id), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Id), "ZZZZZZZZZZZ9"), context));
         GxWebStd.gx_boolean_hidden_field( context, "vISOK", AV6isOK);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE2W2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT2W2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleappmenuoptionentry.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV10ApplicationId) + "," + UrlEncode("" +AV22MenuId) + "," + UrlEncode("" +AV18Id) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleAppMenuOptionEntry" ;
      }

      public override String GetPgmdesc( )
      {
         return "GAMExample App Menu Option Entry" ;
      }

      protected void WB2W0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "BodyContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable3_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-5 col-md-3 col-lg-4", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "MenuOption", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable2_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlname_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlname_Internalname, "Application", "col-sm-4 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavCtlname_Internalname, StringUtil.RTrim( AV5GAMApplication.gxTpr_Name), StringUtil.RTrim( context.localUtil.Format( AV5GAMApplication.gxTpr_Name, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCtlname_Jsonclick, 0, "ReadonlyAttribute", "", "", "", "", 1, edtavCtlname_Enabled, 0, "text", "", 0, "px", 1, " row", 254, 0, 0, 0, 1, 0, -1, true, "", "", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCtlnamemenu_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCtlnamemenu_Internalname, "Menu", "col-sm-4 ReadonlyAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            ClassString = "ReadonlyAttribute";
            StyleString = "";
            ClassString = "ReadonlyAttribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtavCtlnamemenu_Internalname, StringUtil.RTrim( AV11ApplicationMenu.gxTpr_Name), "", "", 0, 1, edtavCtlnamemenu_Enabled, 0, 80, "chr", 4, "row", StyleString, ClassString, "", "", "254", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavGuid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavGuid_Internalname, "GUID", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 30,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV17GUID), StringUtil.RTrim( context.localUtil.Format( AV17GUID, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,30);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavGuid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavName_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavName_Internalname, "Name", "col-sm-4 RequiredAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV25Name), StringUtil.RTrim( context.localUtil.Format( AV25Name, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,35);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "RequiredAttribute", "", "", "", "", 1, edtavName_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavDsc_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavDsc_Internalname, "Description", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 40,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV13Dsc), StringUtil.RTrim( context.localUtil.Format( AV13Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavDsc_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavType_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavType_Internalname, "Option Type", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavType, cmbavType_Internalname, StringUtil.RTrim( AV37Type), 1, cmbavType_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVTYPE.CLICK."+"'", "char", "", 1, cmbavType.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            cmbavType.CurrentValue = StringUtil.RTrim( AV37Type);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavType_Internalname, "Values", (String)(cmbavType.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMenuscell_Internalname, divMenuscell_Visible, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavMenusid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavMenusid_Internalname, "SubMenu", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 50,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavMenusid, cmbavMenusid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0)), 1, cmbavMenusid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavMenusid.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,50);\"", "", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            cmbavMenusid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMenusid_Internalname, "Values", (String)(cmbavMenusid.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divPermissioncell_Internalname, divPermissioncell_Visible, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavRelresid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavRelresid_Internalname, "Permission", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavRelresid, cmbavRelresid_Internalname, StringUtil.RTrim( AV32RelResId), 1, cmbavRelresid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavRelresid.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,55);\"", "", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            cmbavRelresid.CurrentValue = StringUtil.RTrim( AV32RelResId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRelresid_Internalname, "Values", (String)(cmbavRelresid.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divResourcecell_Internalname, divResourcecell_Visible, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavResource_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavResource_Internalname, "Resource", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 60,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavResource_Internalname, AV34Resource, StringUtil.RTrim( context.localUtil.Format( AV34Resource, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,60);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavResource_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavResource_Enabled, 0, "text", "", 600, "px", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divResourceparameterscell_Internalname, divResourceparameterscell_Visible, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavResourceparameters_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavResourceparameters_Internalname, "Resource Parameters", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavResourceparameters_Internalname, AV35ResourceParameters, StringUtil.RTrim( context.localUtil.Format( AV35ResourceParameters, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,65);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavResourceparameters_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavResourceparameters_Enabled, 0, "text", "", 600, "px", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 70,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBrncancel_Internalname, "", "Cancelar", bttBrncancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 72,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleAppMenuOptionEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START2W2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "GAMExample App Menu Option Entry", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2W0( ) ;
      }

      protected void WS2W2( )
      {
         START2W2( ) ;
         EVT2W2( ) ;
      }

      protected void EVT2W2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Start */
                              E112W2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VTYPE.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              E122W2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: Enter */
                                    E132W2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VRELRESID.ISVALID") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              E142W2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Load */
                              E152W2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE2W2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA2W2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavType.Name = "vTYPE";
            cmbavType.WebTags = "";
            cmbavType.addItem("S", "Simple", 0);
            cmbavType.addItem("M", "Menu", 0);
            if ( cmbavType.ItemCount > 0 )
            {
               AV37Type = cmbavType.getValidValue(AV37Type);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Type", AV37Type);
            }
            cmbavMenusid.Name = "vMENUSID";
            cmbavMenusid.WebTags = "";
            if ( cmbavMenusid.ItemCount > 0 )
            {
               AV24MenusId = (long)(NumberUtil.Val( cmbavMenusid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24MenusId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0)));
            }
            cmbavRelresid.Name = "vRELRESID";
            cmbavRelresid.WebTags = "";
            if ( cmbavRelresid.ItemCount > 0 )
            {
               AV32RelResId = cmbavRelresid.getValidValue(AV32RelResId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32RelResId", AV32RelResId);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavCtlname_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavType.ItemCount > 0 )
         {
            AV37Type = cmbavType.getValidValue(AV37Type);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Type", AV37Type);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavType.CurrentValue = StringUtil.RTrim( AV37Type);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavType_Internalname, "Values", cmbavType.ToJavascriptSource(), true);
         }
         if ( cmbavMenusid.ItemCount > 0 )
         {
            AV24MenusId = (long)(NumberUtil.Val( cmbavMenusid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24MenusId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavMenusid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMenusid_Internalname, "Values", cmbavMenusid.ToJavascriptSource(), true);
         }
         if ( cmbavRelresid.ItemCount > 0 )
         {
            AV32RelResId = cmbavRelresid.getValidValue(AV32RelResId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32RelResId", AV32RelResId);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavRelresid.CurrentValue = StringUtil.RTrim( AV32RelResId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRelresid_Internalname, "Values", cmbavRelresid.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2W2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)), true);
         edtavCtlnamemenu_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlnamemenu_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlnamemenu_Enabled), 5, 0)), true);
      }

      protected void RF2W2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E152W2 ();
            WB2W0( ) ;
         }
      }

      protected void send_integrity_lvl_hashes2W2( )
      {
         GxWebStd.gx_hidden_field( context, "vAPPLICATIONID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10ApplicationId), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "vMENUID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV22MenuId), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vMENUID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22MenuId), "ZZZZZZZZZZZ9"), context));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "vID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18Id), 12, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Id), "ZZZZZZZZZZZ9"), context));
      }

      protected void STRUP2W0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlname_Enabled), 5, 0)), true);
         edtavCtlnamemenu_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCtlnamemenu_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlnamemenu_Enabled), 5, 0)), true);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E112W2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV5GAMApplication.gxTpr_Name = cgiGet( edtavCtlname_Internalname);
            AV11ApplicationMenu.gxTpr_Name = cgiGet( edtavCtlnamemenu_Internalname);
            AV17GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GUID", AV17GUID);
            AV25Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Name", AV25Name);
            AV13Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Dsc", AV13Dsc);
            cmbavType.CurrentValue = cgiGet( cmbavType_Internalname);
            AV37Type = cgiGet( cmbavType_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Type", AV37Type);
            cmbavMenusid.CurrentValue = cgiGet( cmbavMenusid_Internalname);
            AV24MenusId = (long)(NumberUtil.Val( cgiGet( cmbavMenusid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24MenusId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0)));
            cmbavRelresid.CurrentValue = cgiGet( cmbavRelresid_Internalname);
            AV32RelResId = cgiGet( cmbavRelresid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32RelResId", AV32RelResId);
            AV34Resource = cgiGet( edtavResource_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Resource", AV34Resource);
            AV35ResourceParameters = cgiGet( edtavResourceparameters_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ResourceParameters", AV35ResourceParameters);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E112W2 ();
         if (returnInSub) return;
      }

      protected void E112W2( )
      {
         /* Start Routine */
         AV5GAMApplication.load( AV10ApplicationId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ApplicationId), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
         AV11ApplicationMenu = AV5GAMApplication.getmenu(AV22MenuId, out  AV15Errors);
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            AV12ApplicationMenuOption = AV11ApplicationMenu.getmenuoptionbyid(AV10ApplicationId, AV18Id, out  AV15Errors);
            AV18Id = AV12ApplicationMenuOption.gxTpr_Id;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Id), 12, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Id), "ZZZZZZZZZZZ9"), context));
            AV25Name = AV12ApplicationMenuOption.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25Name", AV25Name);
            AV13Dsc = AV12ApplicationMenuOption.gxTpr_Description;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13Dsc", AV13Dsc);
            AV17GUID = AV12ApplicationMenuOption.gxTpr_Guid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GUID", AV17GUID);
            AV37Type = AV12ApplicationMenuOption.gxTpr_Type;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37Type", AV37Type);
            AV24MenusId = AV12ApplicationMenuOption.gxTpr_Submenuid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24MenusId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0)));
            AV32RelResId = AV12ApplicationMenuOption.gxTpr_Permissionresourceguid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32RelResId", AV32RelResId);
            AV34Resource = AV12ApplicationMenuOption.gxTpr_Resource;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Resource", AV34Resource);
            AV35ResourceParameters = AV12ApplicationMenuOption.gxTpr_Resourceparameters;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35ResourceParameters", AV35ResourceParameters);
            edtavGuid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)), true);
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
            {
               if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
               {
                  bttBtnconfirm_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)), true);
               }
               edtavName_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)), true);
               edtavDsc_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDsc_Enabled), 5, 0)), true);
               cmbavType.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavType_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavType.Enabled), 5, 0)), true);
               cmbavMenusid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMenusid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMenusid.Enabled), 5, 0)), true);
               cmbavRelresid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRelresid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavRelresid.Enabled), 5, 0)), true);
               bttBtnconfirm_Caption = "Delete";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption, true);
            }
         }
         if ( StringUtil.StrCmp(AV37Type, "S") == 0 )
         {
            AV44GXV4 = 1;
            AV43GXV3 = AV5GAMApplication.getpermissions(AV28PermissionFilter, out  AV15Errors);
            while ( AV44GXV4 <= AV43GXV3.Count )
            {
               AV27Permission = ((SdtGAMApplicationPermission)AV43GXV3.Item(AV44GXV4));
               cmbavRelresid.addItem(AV27Permission.gxTpr_Guid, AV27Permission.gxTpr_Name, 0);
               AV44GXV4 = (int)(AV44GXV4+1);
            }
            divMenuscell_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divMenuscell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divMenuscell_Visible), 5, 0)), true);
            divResourcecell_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divResourcecell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divResourcecell_Visible), 5, 0)), true);
            divResourceparameterscell_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divResourceparameterscell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divResourceparameterscell_Visible), 5, 0)), true);
            divPermissioncell_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divPermissioncell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divPermissioncell_Visible), 5, 0)), true);
         }
         else
         {
            AV46GXV6 = 1;
            AV45GXV5 = AV5GAMApplication.getsubmenus(AV22MenuId, out  AV15Errors);
            while ( AV46GXV6 <= AV45GXV5.Count )
            {
               AV20Menu = ((SdtGAMApplicationMenu)AV45GXV5.Item(AV46GXV6));
               cmbavMenusid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Menu.gxTpr_Id), 12, 0)), AV20Menu.gxTpr_Name, 0);
               AV46GXV6 = (int)(AV46GXV6+1);
            }
            divMenuscell_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divMenuscell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divMenuscell_Visible), 5, 0)), true);
            divResourcecell_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divResourcecell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divResourcecell_Visible), 5, 0)), true);
            divResourceparameterscell_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divResourceparameterscell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divResourceparameterscell_Visible), 5, 0)), true);
            divPermissioncell_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divPermissioncell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divPermissioncell_Visible), 5, 0)), true);
         }
      }

      protected void E122W2( )
      {
         /* Type_Click Routine */
         if ( StringUtil.StrCmp(AV37Type, "S") == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( AV32RelResId)) )
            {
               AV5GAMApplication.load( AV10ApplicationId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ApplicationId), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
               AV48GXV8 = 1;
               AV47GXV7 = AV5GAMApplication.getpermissions(AV28PermissionFilter, out  AV15Errors);
               while ( AV48GXV8 <= AV47GXV7.Count )
               {
                  AV27Permission = ((SdtGAMApplicationPermission)AV47GXV7.Item(AV48GXV8));
                  cmbavRelresid.addItem(AV27Permission.gxTpr_Guid, AV27Permission.gxTpr_Name, 0);
                  AV48GXV8 = (int)(AV48GXV8+1);
               }
            }
            divMenuscell_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divMenuscell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divMenuscell_Visible), 5, 0)), true);
            divResourcecell_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divResourcecell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divResourcecell_Visible), 5, 0)), true);
         }
         else
         {
            if ( (0==AV24MenusId) )
            {
               AV5GAMApplication.load( AV10ApplicationId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ApplicationId), 12, 0)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
               AV50GXV10 = 1;
               AV49GXV9 = AV5GAMApplication.getsubmenus(AV22MenuId, out  AV15Errors);
               while ( AV50GXV10 <= AV49GXV9.Count )
               {
                  AV20Menu = ((SdtGAMApplicationMenu)AV49GXV9.Item(AV50GXV10));
                  cmbavMenusid.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV20Menu.gxTpr_Id), 12, 0)), AV20Menu.gxTpr_Name, 0);
                  AV50GXV10 = (int)(AV50GXV10+1);
               }
            }
            divMenuscell_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divMenuscell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divMenuscell_Visible), 5, 0)), true);
            divResourcecell_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divResourcecell_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divResourcecell_Visible), 5, 0)), true);
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5GAMApplication", AV5GAMApplication);
         cmbavRelresid.CurrentValue = StringUtil.RTrim( AV32RelResId);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavRelresid_Internalname, "Values", cmbavRelresid.ToJavascriptSource(), true);
         cmbavMenusid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV24MenusId), 12, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMenusid_Internalname, "Values", cmbavMenusid.ToJavascriptSource(), true);
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E132W2 ();
         if (returnInSub) return;
      }

      protected void E132W2( )
      {
         /* Enter Routine */
         AV5GAMApplication.load( AV10ApplicationId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ApplicationId), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25Name)) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               /* Execute user subroutine: 'LOAD_APPLICATIONMENUOPTION' */
               S112 ();
               if (returnInSub) return;
               AV6isOK = AV5GAMApplication.addmenuoption(AV22MenuId, AV12ApplicationMenuOption, out  AV15Errors);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6isOK", AV6isOK);
            }
            else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
            {
               AV12ApplicationMenuOption = AV5GAMApplication.getmenuoption(AV22MenuId, AV18Id, out  AV15Errors);
               /* Execute user subroutine: 'LOAD_APPLICATIONMENUOPTION' */
               S112 ();
               if (returnInSub) return;
               AV6isOK = AV5GAMApplication.updatemenuoption(AV22MenuId, AV12ApplicationMenuOption, out  AV15Errors);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6isOK", AV6isOK);
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               AV12ApplicationMenuOption = AV5GAMApplication.getmenuoption(AV22MenuId, AV18Id, out  AV15Errors);
               AV6isOK = AV5GAMApplication.deletemenuoption(AV22MenuId, AV12ApplicationMenuOption, out  AV15Errors);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6isOK", AV6isOK);
            }
         }
         else
         {
            AV6isOK = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV6isOK", AV6isOK);
            AV14Error = new SdtGAMError(context);
            AV14Error.gxTpr_Code = 239;
            AV14Error.gxTpr_Message = gxdomaingamerrormessages.getDescription(context,AV14Error.gxTpr_Code);
            AV15Errors.Add(AV14Error, 0);
         }
         if ( AV6isOK )
         {
            pr_gam.commit( "GAMExampleAppMenuOptionEntry");
            pr_default.commit( "GAMExampleAppMenuOptionEntry");
            context.setWebReturnParms(new Object[] {(String)Gx_mode,(long)AV10ApplicationId,(long)AV22MenuId,(long)AV18Id});
            context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV10ApplicationId","AV22MenuId","AV18Id"});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            AV51GXV11 = 1;
            while ( AV51GXV11 <= AV15Errors.Count )
            {
               AV14Error = ((SdtGAMError)AV15Errors.Item(AV51GXV11));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV14Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV14Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV51GXV11 = (int)(AV51GXV11+1);
            }
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5GAMApplication", AV5GAMApplication);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12ApplicationMenuOption", AV12ApplicationMenuOption);
      }

      protected void E142W2( )
      {
         /* Relresid_Isvalid Routine */
         AV5GAMApplication.load( AV10ApplicationId);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ApplicationId), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
         AV27Permission = AV5GAMApplication.getpermissionbyguid(AV32RelResId, out  AV15Errors);
         AV34Resource = AV27Permission.gxTpr_Resource;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Resource", AV34Resource);
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV5GAMApplication", AV5GAMApplication);
      }

      protected void S112( )
      {
         /* 'LOAD_APPLICATIONMENUOPTION' Routine */
         AV12ApplicationMenuOption.gxTpr_Guid = AV17GUID;
         AV12ApplicationMenuOption.gxTpr_Name = AV25Name;
         AV12ApplicationMenuOption.gxTpr_Description = AV13Dsc;
         AV12ApplicationMenuOption.gxTpr_Type = AV37Type;
         AV12ApplicationMenuOption.gxTpr_Submenuid = AV24MenusId;
         AV12ApplicationMenuOption.gxTpr_Permissionresourceguid = AV32RelResId;
         AV12ApplicationMenuOption.gxTpr_Resource = AV34Resource;
         AV12ApplicationMenuOption.gxTpr_Resourceparameters = AV35ResourceParameters;
      }

      protected void nextLoad( )
      {
      }

      protected void E152W2( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         AV10ApplicationId = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ApplicationId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10ApplicationId), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vAPPLICATIONID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV10ApplicationId), "ZZZZZZZZZZZ9"), context));
         AV22MenuId = Convert.ToInt64(getParm(obj,2));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22MenuId", StringUtil.LTrim( StringUtil.Str( (decimal)(AV22MenuId), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMENUID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV22MenuId), "ZZZZZZZZZZZ9"), context));
         AV18Id = Convert.ToInt64(getParm(obj,3));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18Id), 12, 0)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vID", GetSecureSignedToken( "", context.localUtil.Format( (decimal)(AV18Id), "ZZZZZZZZZZZ9"), context));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2W2( ) ;
         WS2W2( ) ;
         WE2W2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171542945", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gamexampleappmenuoptionentry.js", "?201811171542946", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         divTable3_Internalname = "TABLE3";
         edtavCtlname_Internalname = "CTLNAME";
         edtavCtlnamemenu_Internalname = "CTLNAMEMENU";
         edtavGuid_Internalname = "vGUID";
         edtavName_Internalname = "vNAME";
         edtavDsc_Internalname = "vDSC";
         cmbavType_Internalname = "vTYPE";
         cmbavMenusid_Internalname = "vMENUSID";
         divMenuscell_Internalname = "MENUSCELL";
         cmbavRelresid_Internalname = "vRELRESID";
         divPermissioncell_Internalname = "PERMISSIONCELL";
         edtavResource_Internalname = "vRESOURCE";
         divResourcecell_Internalname = "RESOURCECELL";
         edtavResourceparameters_Internalname = "vRESOURCEPARAMETERS";
         divResourceparameterscell_Internalname = "RESOURCEPARAMETERSCELL";
         divTable2_Internalname = "TABLE2";
         bttBrncancel_Internalname = "BRNCANCEL";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavCtlnamemenu_Enabled = -1;
         edtavCtlname_Enabled = -1;
         bttBtnconfirm_Caption = "Confirmar";
         bttBtnconfirm_Visible = 1;
         edtavResourceparameters_Jsonclick = "";
         edtavResourceparameters_Enabled = 1;
         divResourceparameterscell_Visible = 1;
         edtavResource_Jsonclick = "";
         edtavResource_Enabled = 1;
         divResourcecell_Visible = 1;
         cmbavRelresid_Jsonclick = "";
         cmbavRelresid.Enabled = 1;
         divPermissioncell_Visible = 1;
         cmbavMenusid_Jsonclick = "";
         cmbavMenusid.Enabled = 1;
         divMenuscell_Visible = 1;
         cmbavType_Jsonclick = "";
         cmbavType.Enabled = 1;
         edtavDsc_Jsonclick = "";
         edtavDsc_Enabled = 1;
         edtavName_Jsonclick = "";
         edtavName_Enabled = 1;
         edtavGuid_Jsonclick = "";
         edtavGuid_Enabled = 1;
         edtavCtlnamemenu_Enabled = 0;
         edtavCtlname_Jsonclick = "";
         edtavCtlname_Enabled = 0;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "GAMExample App Menu Option Entry";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'AV10ApplicationId',fld:'vAPPLICATIONID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV22MenuId',fld:'vMENUID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV18Id',fld:'vID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0}],oparms:[]}");
         setEventMetadata("VTYPE.CLICK","{handler:'E122W2',iparms:[{av:'cmbavType'},{av:'AV37Type',fld:'vTYPE',pic:'',nv:''},{av:'cmbavRelresid'},{av:'AV32RelResId',fld:'vRELRESID',pic:'',nv:''},{av:'AV10ApplicationId',fld:'vAPPLICATIONID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'cmbavMenusid'},{av:'AV24MenusId',fld:'vMENUSID',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV22MenuId',fld:'vMENUID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0}],oparms:[{av:'cmbavRelresid'},{av:'AV32RelResId',fld:'vRELRESID',pic:'',nv:''},{av:'cmbavMenusid'},{av:'AV24MenusId',fld:'vMENUSID',pic:'ZZZZZZZZZZZ9',nv:0},{av:'divMenuscell_Visible',ctrl:'MENUSCELL',prop:'Visible'},{av:'divResourcecell_Visible',ctrl:'RESOURCECELL',prop:'Visible'}]}");
         setEventMetadata("ENTER","{handler:'E132W2',iparms:[{av:'AV10ApplicationId',fld:'vAPPLICATIONID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV25Name',fld:'vNAME',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV22MenuId',fld:'vMENUID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV18Id',fld:'vID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'AV6isOK',fld:'vISOK',pic:'',nv:false},{av:'AV17GUID',fld:'vGUID',pic:'',nv:''},{av:'AV13Dsc',fld:'vDSC',pic:'',nv:''},{av:'cmbavType'},{av:'AV37Type',fld:'vTYPE',pic:'',nv:''},{av:'cmbavMenusid'},{av:'AV24MenusId',fld:'vMENUSID',pic:'ZZZZZZZZZZZ9',nv:0},{av:'cmbavRelresid'},{av:'AV32RelResId',fld:'vRELRESID',pic:'',nv:''},{av:'AV34Resource',fld:'vRESOURCE',pic:'',nv:''},{av:'AV35ResourceParameters',fld:'vRESOURCEPARAMETERS',pic:'',nv:''}],oparms:[{av:'AV6isOK',fld:'vISOK',pic:'',nv:false}]}");
         setEventMetadata("VRELRESID.ISVALID","{handler:'E142W2',iparms:[{av:'AV10ApplicationId',fld:'vAPPLICATIONID',pic:'ZZZZZZZZZZZ9',hsh:true,nv:0},{av:'cmbavRelresid'},{av:'AV32RelResId',fld:'vRELRESID',pic:'',nv:''}],oparms:[{av:'AV34Resource',fld:'vRESOURCE',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTextblock1_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         AV5GAMApplication = new SdtGAMApplication(context);
         AV11ApplicationMenu = new SdtGAMApplicationMenu(context);
         TempTags = "";
         AV17GUID = "";
         AV25Name = "";
         AV13Dsc = "";
         AV37Type = "";
         AV32RelResId = "";
         AV34Resource = "";
         AV35ResourceParameters = "";
         bttBrncancel_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV15Errors = new GXExternalCollection<SdtGAMError>( context, "SdtGAMError", "GeneXus.Programs");
         AV12ApplicationMenuOption = new SdtGAMApplicationMenuOption(context);
         AV43GXV3 = new GXExternalCollection<SdtGAMApplicationPermission>( context, "SdtGAMApplicationPermission", "GeneXus.Programs");
         AV28PermissionFilter = new SdtGAMApplicationPermissionFilter(context);
         AV27Permission = new SdtGAMApplicationPermission(context);
         AV45GXV5 = new GXExternalCollection<SdtGAMApplicationMenu>( context, "SdtGAMApplicationMenu", "GeneXus.Programs");
         AV20Menu = new SdtGAMApplicationMenu(context);
         AV47GXV7 = new GXExternalCollection<SdtGAMApplicationPermission>( context, "SdtGAMApplicationPermission", "GeneXus.Programs");
         AV49GXV9 = new GXExternalCollection<SdtGAMApplicationMenu>( context, "SdtGAMApplicationMenu", "GeneXus.Programs");
         AV14Error = new SdtGAMError(context);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.gamexampleappmenuoptionentry__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleappmenuoptionentry__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavCtlname_Enabled = 0;
         edtavCtlnamemenu_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavCtlname_Enabled ;
      private int edtavCtlnamemenu_Enabled ;
      private int edtavGuid_Enabled ;
      private int edtavName_Enabled ;
      private int edtavDsc_Enabled ;
      private int divMenuscell_Visible ;
      private int divPermissioncell_Visible ;
      private int divResourcecell_Visible ;
      private int edtavResource_Enabled ;
      private int divResourceparameterscell_Visible ;
      private int edtavResourceparameters_Enabled ;
      private int bttBtnconfirm_Visible ;
      private int AV44GXV4 ;
      private int AV46GXV6 ;
      private int AV48GXV8 ;
      private int AV50GXV10 ;
      private int AV51GXV11 ;
      private int idxLst ;
      private long AV10ApplicationId ;
      private long AV22MenuId ;
      private long AV18Id ;
      private long wcpOAV10ApplicationId ;
      private long wcpOAV22MenuId ;
      private long wcpOAV18Id ;
      private long AV24MenusId ;
      private String Gx_mode ;
      private String wcpOGx_mode ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String divTable3_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divTable2_Internalname ;
      private String edtavCtlname_Internalname ;
      private String edtavCtlname_Jsonclick ;
      private String edtavCtlnamemenu_Internalname ;
      private String edtavGuid_Internalname ;
      private String TempTags ;
      private String AV17GUID ;
      private String edtavGuid_Jsonclick ;
      private String edtavName_Internalname ;
      private String AV25Name ;
      private String edtavName_Jsonclick ;
      private String edtavDsc_Internalname ;
      private String AV13Dsc ;
      private String edtavDsc_Jsonclick ;
      private String cmbavType_Internalname ;
      private String AV37Type ;
      private String cmbavType_Jsonclick ;
      private String divMenuscell_Internalname ;
      private String cmbavMenusid_Internalname ;
      private String cmbavMenusid_Jsonclick ;
      private String divPermissioncell_Internalname ;
      private String cmbavRelresid_Internalname ;
      private String AV32RelResId ;
      private String cmbavRelresid_Jsonclick ;
      private String divResourcecell_Internalname ;
      private String edtavResource_Internalname ;
      private String edtavResource_Jsonclick ;
      private String divResourceparameterscell_Internalname ;
      private String edtavResourceparameters_Internalname ;
      private String edtavResourceparameters_Jsonclick ;
      private String bttBrncancel_Internalname ;
      private String bttBrncancel_Jsonclick ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String bttBtnconfirm_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool AV6isOK ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV34Resource ;
      private String AV35ResourceParameters ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private long aP1_ApplicationId ;
      private long aP2_MenuId ;
      private long aP3_Id ;
      private GXCombobox cmbavType ;
      private GXCombobox cmbavMenusid ;
      private GXCombobox cmbavRelresid ;
      private IDataStoreProvider pr_gam ;
      private IDataStoreProvider pr_default ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXExternalCollection<SdtGAMError> AV15Errors ;
      private GXExternalCollection<SdtGAMApplicationPermission> AV43GXV3 ;
      private GXExternalCollection<SdtGAMApplicationPermission> AV47GXV7 ;
      private GXExternalCollection<SdtGAMApplicationMenu> AV45GXV5 ;
      private GXExternalCollection<SdtGAMApplicationMenu> AV49GXV9 ;
      private GXWebForm Form ;
      private SdtGAMApplication AV5GAMApplication ;
      private SdtGAMError AV14Error ;
      private SdtGAMApplicationPermission AV27Permission ;
      private SdtGAMApplicationPermissionFilter AV28PermissionFilter ;
      private SdtGAMApplicationMenu AV11ApplicationMenu ;
      private SdtGAMApplicationMenu AV20Menu ;
      private SdtGAMApplicationMenuOption AV12ApplicationMenuOption ;
   }

   public class gamexampleappmenuoptionentry__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class gamexampleappmenuoptionentry__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
