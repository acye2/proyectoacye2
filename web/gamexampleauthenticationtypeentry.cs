/*
               File: GAMExampleAuthenticationTypeEntry
        Description: Authentication type
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:3:39.2
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleauthenticationtypeentry : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleauthenticationtypeentry( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gamexampleauthenticationtypeentry( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref String aP1_Name ,
                           ref String aP2_TypeIdDsp )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV32Name = aP1_Name;
         this.AV35TypeIdDsp = aP2_TypeIdDsp;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_Name=this.AV32Name;
         aP2_TypeIdDsp=this.AV35TypeIdDsp;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavTypeid = new GXCombobox();
         cmbavFunctionid = new GXCombobox();
         chkavIsenable = new GXCheckbox();
         cmbavWsversion = new GXCombobox();
         cmbavWsserversecureprotocol = new GXCombobox();
         cmbavCusversion = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV32Name = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
                  AV35TypeIdDsp = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TypeIdDsp", AV35TypeIdDsp);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTYPEIDDSP", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35TypeIdDsp, "")), context));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "gamexampleauthenticationtypeentry_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpage", "GeneXus.Programs.gammasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA242( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START242( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171533970", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gamexampleauthenticationtypeentry.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV32Name)) + "," + UrlEncode(StringUtil.RTrim(AV35TypeIdDsp))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "vTYPEIDDSP", StringUtil.RTrim( AV35TypeIdDsp));
         GxWebStd.gx_hidden_field( context, "gxhash_vTYPEIDDSP", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35TypeIdDsp, "")), context));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE242( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT242( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleauthenticationtypeentry.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(StringUtil.RTrim(AV32Name)) + "," + UrlEncode(StringUtil.RTrim(AV35TypeIdDsp)) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleAuthenticationTypeEntry" ;
      }

      public override String GetPgmdesc( )
      {
         return "Authentication type" ;
      }

      protected void WB240( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "BodyContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable3_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-5 col-sm-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Authentication Type", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable2_Internalname, 1, 0, "px", 0, "px", "FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTbldata_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavTypeid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavTypeid_Internalname, "Type", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavTypeid, cmbavTypeid_Internalname, StringUtil.RTrim( AV34TypeId), 1, cmbavTypeid_Jsonclick, 5, "'"+""+"'"+",false,"+"'"+"EVTYPEID.CLICK."+"'", "char", "", 1, cmbavTypeid.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            cmbavTypeid.CurrentValue = StringUtil.RTrim( AV34TypeId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Values", (String)(cmbavTypeid.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavName_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavName_Internalname, "Name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV32Name), StringUtil.RTrim( context.localUtil.Format( AV32Name, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavName_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavFunctionid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavFunctionid_Internalname, "Function", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavFunctionid, cmbavFunctionid_Internalname, StringUtil.RTrim( AV26FunctionId), 1, cmbavFunctionid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavFunctionid.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            cmbavFunctionid.CurrentValue = StringUtil.RTrim( AV26FunctionId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Values", (String)(cmbavFunctionid.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavIsenable_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavIsenable_Internalname, "Enabled?", "col-sm-3 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavIsenable_Internalname, StringUtil.BoolToStr( AV31IsEnable), "", "Enabled?", 1, chkavIsenable.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(38, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavDsc_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavDsc_Internalname, "Description", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV23Dsc), StringUtil.RTrim( context.localUtil.Format( AV23Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavDsc_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTblimpersonate_Internalname, divTblimpersonate_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavImpersonate_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavImpersonate_Internalname, "Impersonate", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavImpersonate_Internalname, StringUtil.RTrim( AV30Impersonate), StringUtil.RTrim( context.localUtil.Format( AV30Impersonate, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavImpersonate_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavImpersonate_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMAuthenticationTypeName", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTblfacebook_Internalname, divTblfacebook_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientid_Internalname, "Client Id.", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientid_Internalname, AV14ClientId, StringUtil.RTrim( context.localUtil.Format( AV14ClientId, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,59);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavClientid_Enabled, 1, "text", "", 80, "%", 1, "row", 400, 0, 0, 0, 1, -1, 0, true, "GAMPropertyValue", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientsecret_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientsecret_Internalname, "Client Secret", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 64,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientsecret_Internalname, AV15ClientSecret, StringUtil.RTrim( context.localUtil.Format( AV15ClientSecret, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,64);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientsecret_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavClientsecret_Enabled, 1, "text", "", 80, "%", 1, "row", 400, 0, 0, 0, 1, -1, 0, true, "GAMPropertyValue", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavSiteurl_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavSiteurl_Internalname, "Local site URL", "col-sm-3 URLAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 69,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSiteurl_Internalname, AV33SiteURL, StringUtil.RTrim( context.localUtil.Format( AV33SiteURL, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,69);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSiteurl_Jsonclick, 0, "URLAttribute", "", "", "", "", 1, edtavSiteurl_Enabled, 1, "text", "", 80, "%", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTbltwitter_Internalname, divTbltwitter_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavConsumerkey_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavConsumerkey_Internalname, "Consumer Key", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConsumerkey_Internalname, StringUtil.RTrim( AV16ConsumerKey), StringUtil.RTrim( context.localUtil.Format( AV16ConsumerKey, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,77);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConsumerkey_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavConsumerkey_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavConsumersecret_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavConsumersecret_Internalname, "Consumer Secret", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 82,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavConsumersecret_Internalname, StringUtil.RTrim( AV17ConsumerSecret), StringUtil.RTrim( context.localUtil.Format( AV17ConsumerSecret, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,82);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavConsumersecret_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavConsumersecret_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCallbackurl_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCallbackurl_Internalname, "Callback URL", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 87,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCallbackurl_Internalname, AV13CallbackURL, StringUtil.RTrim( context.localUtil.Format( AV13CallbackURL, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,87);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCallbackurl_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCallbackurl_Enabled, 1, "text", "", 80, "%", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTblcommonadditional_Internalname, divTblcommonadditional_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavAdditionalscope_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavAdditionalscope_Internalname, "Additional Scope", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 95,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavAdditionalscope_Internalname, AV5AdditionalScope, StringUtil.RTrim( context.localUtil.Format( AV5AdditionalScope, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,95);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavAdditionalscope_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavAdditionalscope_Enabled, 1, "text", "", 80, "%", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTblserverhost_Internalname, divTblserverhost_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavGamrserverurl_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavGamrserverurl_Internalname, "Remote server URL", "col-sm-3 URLAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 103,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGamrserverurl_Internalname, AV29GAMRServerURL, StringUtil.RTrim( context.localUtil.Format( AV29GAMRServerURL, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,103);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGamrserverurl_Jsonclick, 0, "URLAttribute", "", "", "", "", 1, edtavGamrserverurl_Enabled, 1, "text", "", 80, "%", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavGamrprivateencryptkey_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavGamrprivateencryptkey_Internalname, "Private Encription Key", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 108,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGamrprivateencryptkey_Internalname, StringUtil.RTrim( AV27GAMRPrivateEncryptKey), StringUtil.RTrim( context.localUtil.Format( AV27GAMRPrivateEncryptKey, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,108);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGamrprivateencryptkey_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavGamrprivateencryptkey_Enabled, 1, "text", "", 32, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divFormcell_Internalname, 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavGamrrepositoryguid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavGamrrepositoryguid_Internalname, "Repository GUID", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 113,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGamrrepositoryguid_Internalname, StringUtil.RTrim( AV28GAMRRepositoryGUID), StringUtil.RTrim( context.localUtil.Format( AV28GAMRRepositoryGUID, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,113);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGamrrepositoryguid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavGamrrepositoryguid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTblwebservice_Internalname, divTblwebservice_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavWsversion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavWsversion_Internalname, "Web service version", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 121,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavWsversion, cmbavWsversion_Internalname, StringUtil.RTrim( AV45WSVersion), 1, cmbavWsversion_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavWsversion.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,121);\"", "", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            cmbavWsversion.CurrentValue = StringUtil.RTrim( AV45WSVersion);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsversion_Internalname, "Values", (String)(cmbavWsversion.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWsprivateencryptkey_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWsprivateencryptkey_Internalname, "Private Encription Key", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsprivateencryptkey_Internalname, StringUtil.RTrim( AV39WSPrivateEncryptKey), StringUtil.RTrim( context.localUtil.Format( AV39WSPrivateEncryptKey, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsprivateencryptkey_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWsprivateencryptkey_Enabled, 1, "text", "", 32, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 FormCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 128,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtngenkey_Internalname, "", "Generate Key", bttBtngenkey_Jsonclick, 5, "Generate Key", "", StyleString, ClassString, bttBtngenkey_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GENERATEKEY\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWsservername_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWsservername_Internalname, "Server name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 133,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsservername_Internalname, StringUtil.RTrim( AV41WSServerName), StringUtil.RTrim( context.localUtil.Format( AV41WSServerName, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,133);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsservername_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWsservername_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWsserverport_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWsserverport_Internalname, "Server port", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 138,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsserverport_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV42WSServerPort), 5, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV42WSServerPort), "ZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,138);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsserverport_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWsserverport_Enabled, 1, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWsserverbaseurl_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWsserverbaseurl_Internalname, "Base URL", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 143,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsserverbaseurl_Internalname, StringUtil.RTrim( AV40WSServerBaseURL), StringUtil.RTrim( context.localUtil.Format( AV40WSServerBaseURL, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,143);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsserverbaseurl_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWsserverbaseurl_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavWsserversecureprotocol_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavWsserversecureprotocol_Internalname, "Secure protocol", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 148,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavWsserversecureprotocol, cmbavWsserversecureprotocol_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0)), 1, cmbavWsserversecureprotocol_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavWsserversecureprotocol.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,148);\"", "", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            cmbavWsserversecureprotocol.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsserversecureprotocol_Internalname, "Values", (String)(cmbavWsserversecureprotocol.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWstimeout_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWstimeout_Internalname, "Timeout (Seconds)", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 153,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWstimeout_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV44WSTimeout), 5, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV44WSTimeout), "ZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,153);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWstimeout_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWstimeout_Enabled, 1, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWspackage_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWspackage_Internalname, "Package", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 158,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWspackage_Internalname, StringUtil.RTrim( AV38WSPackage), StringUtil.RTrim( context.localUtil.Format( AV38WSPackage, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,158);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWspackage_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWspackage_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWsname_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWsname_Internalname, "Name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 163,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsname_Internalname, StringUtil.RTrim( AV37WSName), StringUtil.RTrim( context.localUtil.Format( AV37WSName, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,163);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsname_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWsname_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavWsextension_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavWsextension_Internalname, "Extension", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 168,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavWsextension_Internalname, StringUtil.RTrim( AV36WSExtension), StringUtil.RTrim( context.localUtil.Format( AV36WSExtension, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,168);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavWsextension_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavWsextension_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTblexternal_Internalname, divTblexternal_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavCusversion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavCusversion_Internalname, "JSON version", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 176,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavCusversion, cmbavCusversion_Internalname, StringUtil.RTrim( AV22CusVersion), 1, cmbavCusversion_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "char", "", 1, cmbavCusversion.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,176);\"", "", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            cmbavCusversion.CurrentValue = StringUtil.RTrim( AV22CusVersion);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCusversion_Internalname, "Values", (String)(cmbavCusversion.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCusprivateencryptkey_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCusprivateencryptkey_Internalname, "Private encription key", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 181,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCusprivateencryptkey_Internalname, StringUtil.RTrim( AV21CusPrivateEncryptKey), StringUtil.RTrim( context.localUtil.Format( AV21CusPrivateEncryptKey, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,181);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCusprivateencryptkey_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCusprivateencryptkey_Enabled, 1, "text", "", 32, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 FormCell", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 183,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtngenkeycustom_Internalname, "", "Generate Key Custom", bttBtngenkeycustom_Jsonclick, 5, "Generate Key Custom", "", StyleString, ClassString, bttBtngenkeycustom_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GENERATEKEYCUSTOM\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCusfilename_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCusfilename_Internalname, "File name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 188,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCusfilename_Internalname, StringUtil.RTrim( AV19CusFileName), StringUtil.RTrim( context.localUtil.Format( AV19CusFileName, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,188);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCusfilename_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCusfilename_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCuspackage_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCuspackage_Internalname, "Package", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 193,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCuspackage_Internalname, StringUtil.RTrim( AV20CusPackage), StringUtil.RTrim( context.localUtil.Format( AV20CusPackage, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,193);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCuspackage_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCuspackage_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCusclassname_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCusclassname_Internalname, "Class name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 198,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCusclassname_Internalname, StringUtil.RTrim( AV18CusClassName), StringUtil.RTrim( context.localUtil.Format( AV18CusClassName, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,198);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCusclassname_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCusclassname_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 203,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncancel_Internalname, "", "Cancelar", bttBtncancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 205,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleAuthenticationTypeEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START242( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Authentication type", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP240( ) ;
      }

      protected void WS242( )
      {
         START242( ) ;
         EVT242( ) ;
      }

      protected void EVT242( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Start */
                              E11242 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Refresh */
                              E12242 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VTYPEID.CLICK") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              E13242 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: Enter */
                                    E14242 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GENERATEKEY'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'GenerateKey' */
                              E15242 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GENERATEKEYCUSTOM'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'GenerateKeyCustom' */
                              E16242 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Load */
                              E17242 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE242( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA242( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavTypeid.Name = "vTYPEID";
            cmbavTypeid.WebTags = "";
            cmbavTypeid.addItem("Custom", "Custom", 0);
            cmbavTypeid.addItem("ExternalWebService", "External Web Service", 0);
            cmbavTypeid.addItem("Facebook", "Facebook", 0);
            cmbavTypeid.addItem("GAMLocal", "GAM Local", 0);
            cmbavTypeid.addItem("GAMRemote", "GAM Remote", 0);
            cmbavTypeid.addItem("Google", "Google", 0);
            cmbavTypeid.addItem("Twitter", "Twitter", 0);
            if ( cmbavTypeid.ItemCount > 0 )
            {
               AV34TypeId = cmbavTypeid.getValidValue(AV34TypeId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TypeId", AV34TypeId);
            }
            cmbavFunctionid.Name = "vFUNCTIONID";
            cmbavFunctionid.WebTags = "";
            cmbavFunctionid.addItem("AuthenticationAndRoles", "Authentication and Roles", 0);
            cmbavFunctionid.addItem("OnlyAuthentication", "Only Authentication", 0);
            if ( cmbavFunctionid.ItemCount > 0 )
            {
               AV26FunctionId = cmbavFunctionid.getValidValue(AV26FunctionId);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
            }
            chkavIsenable.Name = "vISENABLE";
            chkavIsenable.WebTags = "";
            chkavIsenable.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenable_Internalname, "TitleCaption", chkavIsenable.Caption, true);
            chkavIsenable.CheckedValue = "false";
            cmbavWsversion.Name = "vWSVERSION";
            cmbavWsversion.WebTags = "";
            cmbavWsversion.addItem("GAM10", "Version 1.0", 0);
            cmbavWsversion.addItem("GAM20", "Version 2.0", 0);
            if ( cmbavWsversion.ItemCount > 0 )
            {
               AV45WSVersion = cmbavWsversion.getValidValue(AV45WSVersion);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45WSVersion", AV45WSVersion);
            }
            cmbavWsserversecureprotocol.Name = "vWSSERVERSECUREPROTOCOL";
            cmbavWsserversecureprotocol.WebTags = "";
            cmbavWsserversecureprotocol.addItem("0", "No", 0);
            cmbavWsserversecureprotocol.addItem("1", "Yes", 0);
            if ( cmbavWsserversecureprotocol.ItemCount > 0 )
            {
               AV43WSServerSecureProtocol = (short)(NumberUtil.Val( cmbavWsserversecureprotocol.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43WSServerSecureProtocol", StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0));
            }
            cmbavCusversion.Name = "vCUSVERSION";
            cmbavCusversion.WebTags = "";
            cmbavCusversion.addItem("GAM10", "Version 1.0", 0);
            cmbavCusversion.addItem("GAM20", "Version 2.0", 0);
            if ( cmbavCusversion.ItemCount > 0 )
            {
               AV22CusVersion = cmbavCusversion.getValidValue(AV22CusVersion);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CusVersion", AV22CusVersion);
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavTypeid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavTypeid.ItemCount > 0 )
         {
            AV34TypeId = cmbavTypeid.getValidValue(AV34TypeId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TypeId", AV34TypeId);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavTypeid.CurrentValue = StringUtil.RTrim( AV34TypeId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Values", cmbavTypeid.ToJavascriptSource(), true);
         }
         if ( cmbavFunctionid.ItemCount > 0 )
         {
            AV26FunctionId = cmbavFunctionid.getValidValue(AV26FunctionId);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavFunctionid.CurrentValue = StringUtil.RTrim( AV26FunctionId);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Values", cmbavFunctionid.ToJavascriptSource(), true);
         }
         if ( cmbavWsversion.ItemCount > 0 )
         {
            AV45WSVersion = cmbavWsversion.getValidValue(AV45WSVersion);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45WSVersion", AV45WSVersion);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavWsversion.CurrentValue = StringUtil.RTrim( AV45WSVersion);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsversion_Internalname, "Values", cmbavWsversion.ToJavascriptSource(), true);
         }
         if ( cmbavWsserversecureprotocol.ItemCount > 0 )
         {
            AV43WSServerSecureProtocol = (short)(NumberUtil.Val( cmbavWsserversecureprotocol.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43WSServerSecureProtocol", StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavWsserversecureprotocol.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsserversecureprotocol_Internalname, "Values", cmbavWsserversecureprotocol.ToJavascriptSource(), true);
         }
         if ( cmbavCusversion.ItemCount > 0 )
         {
            AV22CusVersion = cmbavCusversion.getValidValue(AV22CusVersion);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CusVersion", AV22CusVersion);
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavCusversion.CurrentValue = StringUtil.RTrim( AV22CusVersion);
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCusversion_Internalname, "Values", cmbavCusversion.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF242( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF242( )
      {
         initialize_formulas( ) ;
         /* Execute user event: Refresh */
         E12242 ();
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E17242 ();
            WB240( ) ;
         }
      }

      protected void send_integrity_lvl_hashes242( )
      {
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "vTYPEIDDSP", StringUtil.RTrim( AV35TypeIdDsp));
         GxWebStd.gx_hidden_field( context, "gxhash_vTYPEIDDSP", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35TypeIdDsp, "")), context));
      }

      protected void STRUP240( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11242 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavTypeid.CurrentValue = cgiGet( cmbavTypeid_Internalname);
            AV34TypeId = cgiGet( cmbavTypeid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TypeId", AV34TypeId);
            AV32Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
            cmbavFunctionid.CurrentValue = cgiGet( cmbavFunctionid_Internalname);
            AV26FunctionId = cgiGet( cmbavFunctionid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
            AV31IsEnable = StringUtil.StrToBool( cgiGet( chkavIsenable_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
            AV23Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
            AV30Impersonate = cgiGet( edtavImpersonate_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Impersonate", AV30Impersonate);
            AV14ClientId = cgiGet( edtavClientid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientId", AV14ClientId);
            AV15ClientSecret = cgiGet( edtavClientsecret_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientSecret", AV15ClientSecret);
            AV33SiteURL = cgiGet( edtavSiteurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33SiteURL", AV33SiteURL);
            AV16ConsumerKey = cgiGet( edtavConsumerkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ConsumerKey", AV16ConsumerKey);
            AV17ConsumerSecret = cgiGet( edtavConsumersecret_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ConsumerSecret", AV17ConsumerSecret);
            AV13CallbackURL = cgiGet( edtavCallbackurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13CallbackURL", AV13CallbackURL);
            AV5AdditionalScope = cgiGet( edtavAdditionalscope_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            AV29GAMRServerURL = cgiGet( edtavGamrserverurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GAMRServerURL", AV29GAMRServerURL);
            AV27GAMRPrivateEncryptKey = cgiGet( edtavGamrprivateencryptkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27GAMRPrivateEncryptKey", AV27GAMRPrivateEncryptKey);
            AV28GAMRRepositoryGUID = cgiGet( edtavGamrrepositoryguid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GAMRRepositoryGUID", AV28GAMRRepositoryGUID);
            cmbavWsversion.CurrentValue = cgiGet( cmbavWsversion_Internalname);
            AV45WSVersion = cgiGet( cmbavWsversion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45WSVersion", AV45WSVersion);
            AV39WSPrivateEncryptKey = cgiGet( edtavWsprivateencryptkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39WSPrivateEncryptKey", AV39WSPrivateEncryptKey);
            AV41WSServerName = cgiGet( edtavWsservername_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41WSServerName", AV41WSServerName);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavWsserverport_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavWsserverport_Internalname), ",", ".") > Convert.ToDecimal( 99999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vWSSERVERPORT");
               GX_FocusControl = edtavWsserverport_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV42WSServerPort = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42WSServerPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42WSServerPort), 5, 0)));
            }
            else
            {
               AV42WSServerPort = (int)(context.localUtil.CToN( cgiGet( edtavWsserverport_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42WSServerPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42WSServerPort), 5, 0)));
            }
            AV40WSServerBaseURL = cgiGet( edtavWsserverbaseurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40WSServerBaseURL", AV40WSServerBaseURL);
            cmbavWsserversecureprotocol.CurrentValue = cgiGet( cmbavWsserversecureprotocol_Internalname);
            AV43WSServerSecureProtocol = (short)(NumberUtil.Val( cgiGet( cmbavWsserversecureprotocol_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43WSServerSecureProtocol", StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0));
            if ( ( ( context.localUtil.CToN( cgiGet( edtavWstimeout_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavWstimeout_Internalname), ",", ".") > Convert.ToDecimal( 99999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vWSTIMEOUT");
               GX_FocusControl = edtavWstimeout_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV44WSTimeout = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44WSTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44WSTimeout), 5, 0)));
            }
            else
            {
               AV44WSTimeout = (int)(context.localUtil.CToN( cgiGet( edtavWstimeout_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44WSTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44WSTimeout), 5, 0)));
            }
            AV38WSPackage = cgiGet( edtavWspackage_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38WSPackage", AV38WSPackage);
            AV37WSName = cgiGet( edtavWsname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37WSName", AV37WSName);
            AV36WSExtension = cgiGet( edtavWsextension_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36WSExtension", AV36WSExtension);
            cmbavCusversion.CurrentValue = cgiGet( cmbavCusversion_Internalname);
            AV22CusVersion = cgiGet( cmbavCusversion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CusVersion", AV22CusVersion);
            AV21CusPrivateEncryptKey = cgiGet( edtavCusprivateencryptkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CusPrivateEncryptKey", AV21CusPrivateEncryptKey);
            AV19CusFileName = cgiGet( edtavCusfilename_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19CusFileName", AV19CusFileName);
            AV20CusPackage = cgiGet( edtavCuspackage_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CusPackage", AV20CusPackage);
            AV18CusClassName = cgiGet( edtavCusclassname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18CusClassName", AV18CusClassName);
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E11242 ();
         if (returnInSub) return;
      }

      protected void E11242( )
      {
         /* Start Routine */
         AV34TypeId = AV35TypeIdDsp;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34TypeId", AV34TypeId);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            cmbavTypeid.Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTypeid.Enabled), 5, 0)), true);
            edtavName_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)), true);
         }
         else
         {
            cmbavTypeid.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTypeid.Enabled), 5, 0)), true);
            edtavName_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)), true);
            AV26FunctionId = "OnlyAuthentication";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
            if ( StringUtil.StrCmp(AV34TypeId, "GAMLocal") == 0 )
            {
               AV10AuthenticationTypeLocal.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV32Name = AV10AuthenticationTypeLocal.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV26FunctionId = AV10AuthenticationTypeLocal.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
               AV31IsEnable = AV10AuthenticationTypeLocal.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
               AV23Dsc = AV10AuthenticationTypeLocal.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Facebook") == 0 )
            {
               cmbavFunctionid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV7AuthenticationTypeFacebook.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV32Name = AV7AuthenticationTypeFacebook.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV31IsEnable = AV7AuthenticationTypeFacebook.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
               AV23Dsc = AV7AuthenticationTypeFacebook.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
               AV14ClientId = AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientId", AV14ClientId);
               AV15ClientSecret = AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientsecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientSecret", AV15ClientSecret);
               AV33SiteURL = AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Siteurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33SiteURL", AV33SiteURL);
               AV5AdditionalScope = AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Google") == 0 )
            {
               cmbavFunctionid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV9AuthenticationTypeGoogle.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV32Name = AV9AuthenticationTypeGoogle.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV31IsEnable = AV9AuthenticationTypeGoogle.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
               AV23Dsc = AV9AuthenticationTypeGoogle.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
               AV14ClientId = AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientId", AV14ClientId);
               AV15ClientSecret = AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientsecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientSecret", AV15ClientSecret);
               AV33SiteURL = AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Siteurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33SiteURL", AV33SiteURL);
               AV5AdditionalScope = AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "GAMRemote") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV8AuthenticationTypeGAMRemote.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV32Name = AV8AuthenticationTypeGAMRemote.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV26FunctionId = AV8AuthenticationTypeGAMRemote.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
               AV31IsEnable = AV8AuthenticationTypeGAMRemote.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
               AV23Dsc = AV8AuthenticationTypeGAMRemote.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
               AV30Impersonate = AV8AuthenticationTypeGAMRemote.gxTpr_Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Impersonate", AV30Impersonate);
               AV14ClientId = AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientId", AV14ClientId);
               AV15ClientSecret = AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientsecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientSecret", AV15ClientSecret);
               AV33SiteURL = AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Siteurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33SiteURL", AV33SiteURL);
               AV5AdditionalScope = AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
               AV29GAMRServerURL = AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29GAMRServerURL", AV29GAMRServerURL);
               AV27GAMRPrivateEncryptKey = AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27GAMRPrivateEncryptKey", AV27GAMRPrivateEncryptKey);
               AV28GAMRRepositoryGUID = AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoterepositoryguid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28GAMRRepositoryGUID", AV28GAMRRepositoryGUID);
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Twitter") == 0 )
            {
               cmbavFunctionid.Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV11AuthenticationTypeTwitter.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV32Name = AV11AuthenticationTypeTwitter.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV31IsEnable = AV11AuthenticationTypeTwitter.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
               AV23Dsc = AV11AuthenticationTypeTwitter.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
               AV16ConsumerKey = AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumerkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ConsumerKey", AV16ConsumerKey);
               AV17ConsumerSecret = AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumersecret;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17ConsumerSecret", AV17ConsumerSecret);
               AV13CallbackURL = AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Callbackurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13CallbackURL", AV13CallbackURL);
               AV5AdditionalScope = AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Additionalscope;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AdditionalScope", AV5AdditionalScope);
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "ExternalWebService") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV12AuthenticationTypeWebService.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV32Name = AV12AuthenticationTypeWebService.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV26FunctionId = AV12AuthenticationTypeWebService.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
               AV31IsEnable = AV12AuthenticationTypeWebService.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
               AV23Dsc = AV12AuthenticationTypeWebService.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
               AV30Impersonate = AV12AuthenticationTypeWebService.gxTpr_Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Impersonate", AV30Impersonate);
               AV45WSVersion = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Version;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV45WSVersion", AV45WSVersion);
               AV39WSPrivateEncryptKey = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Privateencryptkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39WSPrivateEncryptKey", AV39WSPrivateEncryptKey);
               AV41WSServerName = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41WSServerName", AV41WSServerName);
               AV42WSServerPort = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Port;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42WSServerPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV42WSServerPort), 5, 0)));
               AV40WSServerBaseURL = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Baseurl;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV40WSServerBaseURL", AV40WSServerBaseURL);
               AV43WSServerSecureProtocol = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Secureprotocol;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43WSServerSecureProtocol", StringUtil.Str( (decimal)(AV43WSServerSecureProtocol), 1, 0));
               AV44WSTimeout = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Timeout;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV44WSTimeout", StringUtil.LTrim( StringUtil.Str( (decimal)(AV44WSTimeout), 5, 0)));
               AV38WSPackage = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Package;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV38WSPackage", AV38WSPackage);
               AV37WSName = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV37WSName", AV37WSName);
               AV36WSExtension = AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Extension;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36WSExtension", AV36WSExtension);
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Custom") == 0 )
            {
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV6AuthenticationTypeCustom.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV32Name = AV6AuthenticationTypeCustom.gxTpr_Name;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV26FunctionId = AV6AuthenticationTypeCustom.gxTpr_Functionid;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26FunctionId", AV26FunctionId);
               AV31IsEnable = AV6AuthenticationTypeCustom.gxTpr_Isenable;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV31IsEnable", AV31IsEnable);
               AV23Dsc = AV6AuthenticationTypeCustom.gxTpr_Description;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Dsc", AV23Dsc);
               AV30Impersonate = AV6AuthenticationTypeCustom.gxTpr_Impersonate;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV30Impersonate", AV30Impersonate);
               AV22CusVersion = AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Version;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22CusVersion", AV22CusVersion);
               AV21CusPrivateEncryptKey = AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Privateencryptkey;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CusPrivateEncryptKey", AV21CusPrivateEncryptKey);
               AV19CusFileName = AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Filename;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19CusFileName", AV19CusFileName);
               AV20CusPackage = AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Package;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20CusPackage", AV20CusPackage);
               AV18CusClassName = AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Classname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18CusClassName", AV18CusClassName);
            }
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            bttBtngenkey_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtngenkey_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtngenkey_Visible), 5, 0)), true);
            bttBtngenkeycustom_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtngenkeycustom_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtngenkeycustom_Visible), 5, 0)), true);
            cmbavTypeid.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavTypeid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavTypeid.Enabled), 5, 0)), true);
            cmbavFunctionid.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
            chkavIsenable.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavIsenable_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavIsenable.Enabled), 5, 0)), true);
            edtavDsc_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDsc_Enabled), 5, 0)), true);
            edtavImpersonate_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavImpersonate_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavImpersonate_Enabled), 5, 0)), true);
            cmbavWsversion.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsversion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavWsversion.Enabled), 5, 0)), true);
            edtavWsprivateencryptkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsprivateencryptkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsprivateencryptkey_Enabled), 5, 0)), true);
            edtavWsservername_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsservername_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsservername_Enabled), 5, 0)), true);
            edtavWsserverport_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsserverport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsserverport_Enabled), 5, 0)), true);
            edtavWsserverbaseurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsserverbaseurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsserverbaseurl_Enabled), 5, 0)), true);
            cmbavWsserversecureprotocol.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavWsserversecureprotocol_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavWsserversecureprotocol.Enabled), 5, 0)), true);
            edtavWstimeout_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWstimeout_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWstimeout_Enabled), 5, 0)), true);
            edtavWspackage_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWspackage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWspackage_Enabled), 5, 0)), true);
            edtavWsname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsname_Enabled), 5, 0)), true);
            edtavWsextension_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavWsextension_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavWsextension_Enabled), 5, 0)), true);
            edtavClientid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientid_Enabled), 5, 0)), true);
            edtavClientsecret_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientsecret_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientsecret_Enabled), 5, 0)), true);
            edtavSiteurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavSiteurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSiteurl_Enabled), 5, 0)), true);
            edtavAdditionalscope_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavAdditionalscope_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavAdditionalscope_Enabled), 5, 0)), true);
            edtavConsumerkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConsumerkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConsumerkey_Enabled), 5, 0)), true);
            edtavConsumersecret_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavConsumersecret_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavConsumersecret_Enabled), 5, 0)), true);
            edtavCallbackurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCallbackurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCallbackurl_Enabled), 5, 0)), true);
            cmbavCusversion.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavCusversion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavCusversion.Enabled), 5, 0)), true);
            edtavCusprivateencryptkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCusprivateencryptkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCusprivateencryptkey_Enabled), 5, 0)), true);
            edtavCusfilename_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCusfilename_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCusfilename_Enabled), 5, 0)), true);
            edtavCuspackage_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCuspackage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCuspackage_Enabled), 5, 0)), true);
            edtavCusclassname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCusclassname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCusclassname_Enabled), 5, 0)), true);
            edtavGamrserverurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGamrserverurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamrserverurl_Enabled), 5, 0)), true);
            edtavGamrprivateencryptkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGamrprivateencryptkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamrprivateencryptkey_Enabled), 5, 0)), true);
            edtavGamrrepositoryguid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGamrrepositoryguid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGamrrepositoryguid_Enabled), 5, 0)), true);
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption, true);
         }
      }

      protected void E12242( )
      {
         /* Refresh Routine */
         divTblimpersonate_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblimpersonate_Visible), 5, 0)), true);
         divTblfacebook_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblfacebook_Visible), 5, 0)), true);
         divTblcommonadditional_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblcommonadditional_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblcommonadditional_Visible), 5, 0)), true);
         divTblserverhost_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblserverhost_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblserverhost_Visible), 5, 0)), true);
         divTbltwitter_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTbltwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTbltwitter_Visible), 5, 0)), true);
         divTblwebservice_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblwebservice_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblwebservice_Visible), 5, 0)), true);
         divTblexternal_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblexternal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblexternal_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(AV34TypeId, "GAMLocal") == 0 )
         {
         }
         else if ( ( StringUtil.StrCmp(AV34TypeId, "Facebook") == 0 ) || ( StringUtil.StrCmp(AV34TypeId, "Google") == 0 ) || ( StringUtil.StrCmp(AV34TypeId, "GAMRemote") == 0 ) )
         {
            divTblfacebook_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblfacebook_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblfacebook_Visible), 5, 0)), true);
            divTblcommonadditional_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblcommonadditional_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblcommonadditional_Visible), 5, 0)), true);
            if ( StringUtil.StrCmp(AV34TypeId, "GAMRemote") == 0 )
            {
               divTblimpersonate_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblimpersonate_Visible), 5, 0)), true);
               divTblserverhost_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblserverhost_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblserverhost_Visible), 5, 0)), true);
            }
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "Twitter") == 0 )
         {
            divTbltwitter_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTbltwitter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTbltwitter_Visible), 5, 0)), true);
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "ExternalWebService") == 0 )
         {
            divTblimpersonate_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblimpersonate_Visible), 5, 0)), true);
            divTblwebservice_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblwebservice_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblwebservice_Visible), 5, 0)), true);
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "Custom") == 0 )
         {
            divTblimpersonate_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblimpersonate_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblimpersonate_Visible), 5, 0)), true);
            divTblexternal_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblexternal_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblexternal_Visible), 5, 0)), true);
         }
         /*  Sending Event outputs  */
      }

      protected void E13242( )
      {
         /* Typeid_Click Routine */
         context.DoAjaxRefresh();
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E14242 ();
         if (returnInSub) return;
      }

      protected void E14242( )
      {
         /* Enter Routine */
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            if ( StringUtil.StrCmp(AV34TypeId, "GAMLocal") == 0 )
            {
               AV10AuthenticationTypeLocal.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV10AuthenticationTypeLocal.gxTpr_Name = AV32Name;
               AV10AuthenticationTypeLocal.gxTpr_Functionid = AV26FunctionId;
               AV10AuthenticationTypeLocal.gxTpr_Isenable = AV31IsEnable;
               AV10AuthenticationTypeLocal.gxTpr_Description = AV23Dsc;
               AV10AuthenticationTypeLocal.save();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Facebook") == 0 )
            {
               AV7AuthenticationTypeFacebook.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV7AuthenticationTypeFacebook.gxTpr_Name = AV32Name;
               AV7AuthenticationTypeFacebook.gxTpr_Isenable = AV31IsEnable;
               AV7AuthenticationTypeFacebook.gxTpr_Description = AV23Dsc;
               AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientid = AV14ClientId;
               AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Clientsecret = AV15ClientSecret;
               AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Siteurl = AV33SiteURL;
               AV7AuthenticationTypeFacebook.gxTpr_Facebook.gxTpr_Additionalscope = AV5AdditionalScope;
               AV7AuthenticationTypeFacebook.save();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Google") == 0 )
            {
               AV9AuthenticationTypeGoogle.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV9AuthenticationTypeGoogle.gxTpr_Name = AV32Name;
               AV9AuthenticationTypeGoogle.gxTpr_Isenable = AV31IsEnable;
               AV9AuthenticationTypeGoogle.gxTpr_Description = AV23Dsc;
               AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientid = AV14ClientId;
               AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Clientsecret = AV15ClientSecret;
               AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Siteurl = AV33SiteURL;
               AV9AuthenticationTypeGoogle.gxTpr_Google.gxTpr_Additionalscope = AV5AdditionalScope;
               AV9AuthenticationTypeGoogle.save();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "GAMRemote") == 0 )
            {
               AV8AuthenticationTypeGAMRemote.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV8AuthenticationTypeGAMRemote.gxTpr_Name = AV32Name;
               AV8AuthenticationTypeGAMRemote.gxTpr_Functionid = AV26FunctionId;
               AV8AuthenticationTypeGAMRemote.gxTpr_Isenable = AV31IsEnable;
               AV8AuthenticationTypeGAMRemote.gxTpr_Description = AV23Dsc;
               AV8AuthenticationTypeGAMRemote.gxTpr_Impersonate = AV30Impersonate;
               AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientid = AV14ClientId;
               AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Clientsecret = AV15ClientSecret;
               AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Siteurl = AV33SiteURL;
               AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Additionalscope = AV5AdditionalScope;
               AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverurl = AV29GAMRServerURL;
               AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoteserverkey = AV27GAMRPrivateEncryptKey;
               AV8AuthenticationTypeGAMRemote.gxTpr_Gamremote.gxTpr_Remoterepositoryguid = AV28GAMRRepositoryGUID;
               AV8AuthenticationTypeGAMRemote.save();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Twitter") == 0 )
            {
               AV11AuthenticationTypeTwitter.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV11AuthenticationTypeTwitter.gxTpr_Name = AV32Name;
               AV11AuthenticationTypeTwitter.gxTpr_Isenable = AV31IsEnable;
               AV11AuthenticationTypeTwitter.gxTpr_Description = AV23Dsc;
               AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumerkey = AV16ConsumerKey;
               AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Consumersecret = AV17ConsumerSecret;
               AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Callbackurl = AV13CallbackURL;
               AV11AuthenticationTypeTwitter.gxTpr_Twitter.gxTpr_Additionalscope = AV5AdditionalScope;
               AV11AuthenticationTypeTwitter.save();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "ExternalWebService") == 0 )
            {
               AV12AuthenticationTypeWebService.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV12AuthenticationTypeWebService.gxTpr_Name = AV32Name;
               AV12AuthenticationTypeWebService.gxTpr_Functionid = AV26FunctionId;
               AV12AuthenticationTypeWebService.gxTpr_Isenable = AV31IsEnable;
               AV12AuthenticationTypeWebService.gxTpr_Description = AV23Dsc;
               AV12AuthenticationTypeWebService.gxTpr_Impersonate = AV30Impersonate;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Version = AV45WSVersion;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Privateencryptkey = AV39WSPrivateEncryptKey;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Timeout = AV44WSTimeout;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Package = AV38WSPackage;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Name = AV37WSName;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Extension = AV36WSExtension;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Name = AV41WSServerName;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Port = AV42WSServerPort;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Baseurl = AV40WSServerBaseURL;
               AV12AuthenticationTypeWebService.gxTpr_Webservice.gxTpr_Server.gxTpr_Secureprotocol = AV43WSServerSecureProtocol;
               AV12AuthenticationTypeWebService.save();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Custom") == 0 )
            {
               AV6AuthenticationTypeCustom.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               cmbavFunctionid.Enabled = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavFunctionid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavFunctionid.Enabled), 5, 0)), true);
               AV6AuthenticationTypeCustom.gxTpr_Name = AV32Name;
               AV6AuthenticationTypeCustom.gxTpr_Functionid = AV26FunctionId;
               AV6AuthenticationTypeCustom.gxTpr_Isenable = AV31IsEnable;
               AV6AuthenticationTypeCustom.gxTpr_Description = AV23Dsc;
               AV6AuthenticationTypeCustom.gxTpr_Impersonate = AV30Impersonate;
               AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Version = AV22CusVersion;
               AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Privateencryptkey = AV21CusPrivateEncryptKey;
               AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Filename = AV19CusFileName;
               AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Package = AV20CusPackage;
               AV6AuthenticationTypeCustom.gxTpr_Custom.gxTpr_Classname = AV18CusClassName;
               AV6AuthenticationTypeCustom.save();
            }
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            if ( StringUtil.StrCmp(AV34TypeId, "GAMLocal") == 0 )
            {
               AV10AuthenticationTypeLocal.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV10AuthenticationTypeLocal.delete();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Facebook") == 0 )
            {
               AV7AuthenticationTypeFacebook.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV7AuthenticationTypeFacebook.delete();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Google") == 0 )
            {
               AV9AuthenticationTypeGoogle.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV9AuthenticationTypeGoogle.delete();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "GAMRemote") == 0 )
            {
               AV8AuthenticationTypeGAMRemote.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV8AuthenticationTypeGAMRemote.delete();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Twitter") == 0 )
            {
               AV11AuthenticationTypeTwitter.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV11AuthenticationTypeTwitter.delete();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "ExternalWebService") == 0 )
            {
               AV12AuthenticationTypeWebService.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV12AuthenticationTypeWebService.delete();
            }
            else if ( StringUtil.StrCmp(AV34TypeId, "Custom") == 0 )
            {
               AV6AuthenticationTypeCustom.load( AV32Name);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
               AV6AuthenticationTypeCustom.delete();
            }
         }
         if ( StringUtil.StrCmp(AV34TypeId, "GAMLocal") == 0 )
         {
            if ( AV10AuthenticationTypeLocal.success() )
            {
               pr_gam.commit( "GAMExampleAuthenticationTypeEntry");
               pr_default.commit( "GAMExampleAuthenticationTypeEntry");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV32Name,(String)AV35TypeIdDsp});
               context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV32Name","AV35TypeIdDsp"});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "Facebook") == 0 )
         {
            if ( AV7AuthenticationTypeFacebook.success() )
            {
               pr_gam.commit( "GAMExampleAuthenticationTypeEntry");
               pr_default.commit( "GAMExampleAuthenticationTypeEntry");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV32Name,(String)AV35TypeIdDsp});
               context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV32Name","AV35TypeIdDsp"});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "Google") == 0 )
         {
            if ( AV9AuthenticationTypeGoogle.success() )
            {
               pr_gam.commit( "GAMExampleAuthenticationTypeEntry");
               pr_default.commit( "GAMExampleAuthenticationTypeEntry");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV32Name,(String)AV35TypeIdDsp});
               context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV32Name","AV35TypeIdDsp"});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "GAMRemote") == 0 )
         {
            if ( AV8AuthenticationTypeGAMRemote.success() )
            {
               pr_gam.commit( "GAMExampleAuthenticationTypeEntry");
               pr_default.commit( "GAMExampleAuthenticationTypeEntry");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV32Name,(String)AV35TypeIdDsp});
               context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV32Name","AV35TypeIdDsp"});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "Twitter") == 0 )
         {
            if ( AV11AuthenticationTypeTwitter.success() )
            {
               pr_gam.commit( "GAMExampleAuthenticationTypeEntry");
               pr_default.commit( "GAMExampleAuthenticationTypeEntry");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV32Name,(String)AV35TypeIdDsp});
               context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV32Name","AV35TypeIdDsp"});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "ExternalWebService") == 0 )
         {
            if ( AV12AuthenticationTypeWebService.success() )
            {
               pr_gam.commit( "GAMExampleAuthenticationTypeEntry");
               pr_default.commit( "GAMExampleAuthenticationTypeEntry");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV32Name,(String)AV35TypeIdDsp});
               context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV32Name","AV35TypeIdDsp"});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(AV34TypeId, "Custom") == 0 )
         {
            if ( AV6AuthenticationTypeCustom.success() )
            {
               pr_gam.commit( "GAMExampleAuthenticationTypeEntry");
               pr_default.commit( "GAMExampleAuthenticationTypeEntry");
               context.setWebReturnParms(new Object[] {(String)Gx_mode,(String)AV32Name,(String)AV35TypeIdDsp});
               context.setWebReturnParmsMetadata(new Object[] {"Gx_mode","AV32Name","AV35TypeIdDsp"});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
            else
            {
               AV25Errors = AV6AuthenticationTypeCustom.geterrors();
            }
         }
         AV25Errors = new SdtGAMRepository(context).getlasterrors();
         AV49GXV1 = 1;
         while ( AV49GXV1 <= AV25Errors.Count )
         {
            AV24Error = ((SdtGAMError)AV25Errors.Item(AV49GXV1));
            GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV24Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV24Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
            AV49GXV1 = (int)(AV49GXV1+1);
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV6AuthenticationTypeCustom", AV6AuthenticationTypeCustom);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV12AuthenticationTypeWebService", AV12AuthenticationTypeWebService);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV11AuthenticationTypeTwitter", AV11AuthenticationTypeTwitter);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV8AuthenticationTypeGAMRemote", AV8AuthenticationTypeGAMRemote);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV9AuthenticationTypeGoogle", AV9AuthenticationTypeGoogle);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV7AuthenticationTypeFacebook", AV7AuthenticationTypeFacebook);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV10AuthenticationTypeLocal", AV10AuthenticationTypeLocal);
      }

      protected void E15242( )
      {
         /* 'GenerateKey' Routine */
         AV39WSPrivateEncryptKey = Crypto.GetEncryptionKey( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39WSPrivateEncryptKey", AV39WSPrivateEncryptKey);
         /*  Sending Event outputs  */
      }

      protected void E16242( )
      {
         /* 'GenerateKeyCustom' Routine */
         AV21CusPrivateEncryptKey = Crypto.GetEncryptionKey( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21CusPrivateEncryptKey", AV21CusPrivateEncryptKey);
         /*  Sending Event outputs  */
      }

      protected void nextLoad( )
      {
      }

      protected void E17242( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         AV32Name = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32Name", AV32Name);
         AV35TypeIdDsp = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35TypeIdDsp", AV35TypeIdDsp);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vTYPEIDDSP", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV35TypeIdDsp, "")), context));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA242( ) ;
         WS242( ) ;
         WE242( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171543375", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gamexampleauthenticationtypeentry.js", "?201811171543378", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock1_Internalname = "TEXTBLOCK1";
         divTable3_Internalname = "TABLE3";
         cmbavTypeid_Internalname = "vTYPEID";
         edtavName_Internalname = "vNAME";
         cmbavFunctionid_Internalname = "vFUNCTIONID";
         chkavIsenable_Internalname = "vISENABLE";
         edtavDsc_Internalname = "vDSC";
         divTbldata_Internalname = "TBLDATA";
         edtavImpersonate_Internalname = "vIMPERSONATE";
         divTblimpersonate_Internalname = "TBLIMPERSONATE";
         edtavClientid_Internalname = "vCLIENTID";
         edtavClientsecret_Internalname = "vCLIENTSECRET";
         edtavSiteurl_Internalname = "vSITEURL";
         divTblfacebook_Internalname = "TBLFACEBOOK";
         edtavConsumerkey_Internalname = "vCONSUMERKEY";
         edtavConsumersecret_Internalname = "vCONSUMERSECRET";
         edtavCallbackurl_Internalname = "vCALLBACKURL";
         divTbltwitter_Internalname = "TBLTWITTER";
         edtavAdditionalscope_Internalname = "vADDITIONALSCOPE";
         divTblcommonadditional_Internalname = "TBLCOMMONADDITIONAL";
         edtavGamrserverurl_Internalname = "vGAMRSERVERURL";
         edtavGamrprivateencryptkey_Internalname = "vGAMRPRIVATEENCRYPTKEY";
         edtavGamrrepositoryguid_Internalname = "vGAMRREPOSITORYGUID";
         divFormcell_Internalname = "FORMCELL";
         divTblserverhost_Internalname = "TBLSERVERHOST";
         cmbavWsversion_Internalname = "vWSVERSION";
         edtavWsprivateencryptkey_Internalname = "vWSPRIVATEENCRYPTKEY";
         bttBtngenkey_Internalname = "BTNGENKEY";
         edtavWsservername_Internalname = "vWSSERVERNAME";
         edtavWsserverport_Internalname = "vWSSERVERPORT";
         edtavWsserverbaseurl_Internalname = "vWSSERVERBASEURL";
         cmbavWsserversecureprotocol_Internalname = "vWSSERVERSECUREPROTOCOL";
         edtavWstimeout_Internalname = "vWSTIMEOUT";
         edtavWspackage_Internalname = "vWSPACKAGE";
         edtavWsname_Internalname = "vWSNAME";
         edtavWsextension_Internalname = "vWSEXTENSION";
         divTblwebservice_Internalname = "TBLWEBSERVICE";
         cmbavCusversion_Internalname = "vCUSVERSION";
         edtavCusprivateencryptkey_Internalname = "vCUSPRIVATEENCRYPTKEY";
         bttBtngenkeycustom_Internalname = "BTNGENKEYCUSTOM";
         edtavCusfilename_Internalname = "vCUSFILENAME";
         edtavCuspackage_Internalname = "vCUSPACKAGE";
         edtavCusclassname_Internalname = "vCUSCLASSNAME";
         divTblexternal_Internalname = "TBLEXTERNAL";
         divTable2_Internalname = "TABLE2";
         bttBtncancel_Internalname = "BTNCANCEL";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         chkavIsenable.Caption = "Enabled?";
         bttBtnconfirm_Caption = "Confirmar";
         bttBtnconfirm_Visible = 1;
         edtavCusclassname_Jsonclick = "";
         edtavCusclassname_Enabled = 1;
         edtavCuspackage_Jsonclick = "";
         edtavCuspackage_Enabled = 1;
         edtavCusfilename_Jsonclick = "";
         edtavCusfilename_Enabled = 1;
         bttBtngenkeycustom_Visible = 1;
         edtavCusprivateencryptkey_Jsonclick = "";
         edtavCusprivateencryptkey_Enabled = 1;
         cmbavCusversion_Jsonclick = "";
         cmbavCusversion.Enabled = 1;
         divTblexternal_Visible = 1;
         edtavWsextension_Jsonclick = "";
         edtavWsextension_Enabled = 1;
         edtavWsname_Jsonclick = "";
         edtavWsname_Enabled = 1;
         edtavWspackage_Jsonclick = "";
         edtavWspackage_Enabled = 1;
         edtavWstimeout_Jsonclick = "";
         edtavWstimeout_Enabled = 1;
         cmbavWsserversecureprotocol_Jsonclick = "";
         cmbavWsserversecureprotocol.Enabled = 1;
         edtavWsserverbaseurl_Jsonclick = "";
         edtavWsserverbaseurl_Enabled = 1;
         edtavWsserverport_Jsonclick = "";
         edtavWsserverport_Enabled = 1;
         edtavWsservername_Jsonclick = "";
         edtavWsservername_Enabled = 1;
         bttBtngenkey_Visible = 1;
         edtavWsprivateencryptkey_Jsonclick = "";
         edtavWsprivateencryptkey_Enabled = 1;
         cmbavWsversion_Jsonclick = "";
         cmbavWsversion.Enabled = 1;
         divTblwebservice_Visible = 1;
         edtavGamrrepositoryguid_Jsonclick = "";
         edtavGamrrepositoryguid_Enabled = 1;
         edtavGamrprivateencryptkey_Jsonclick = "";
         edtavGamrprivateencryptkey_Enabled = 1;
         edtavGamrserverurl_Jsonclick = "";
         edtavGamrserverurl_Enabled = 1;
         divTblserverhost_Visible = 1;
         edtavAdditionalscope_Jsonclick = "";
         edtavAdditionalscope_Enabled = 1;
         divTblcommonadditional_Visible = 1;
         edtavCallbackurl_Jsonclick = "";
         edtavCallbackurl_Enabled = 1;
         edtavConsumersecret_Jsonclick = "";
         edtavConsumersecret_Enabled = 1;
         edtavConsumerkey_Jsonclick = "";
         edtavConsumerkey_Enabled = 1;
         divTbltwitter_Visible = 1;
         edtavSiteurl_Jsonclick = "";
         edtavSiteurl_Enabled = 1;
         edtavClientsecret_Jsonclick = "";
         edtavClientsecret_Enabled = 1;
         edtavClientid_Jsonclick = "";
         edtavClientid_Enabled = 1;
         divTblfacebook_Visible = 1;
         edtavImpersonate_Jsonclick = "";
         edtavImpersonate_Enabled = 1;
         divTblimpersonate_Visible = 1;
         edtavDsc_Jsonclick = "";
         edtavDsc_Enabled = 1;
         chkavIsenable.Enabled = 1;
         cmbavFunctionid_Jsonclick = "";
         cmbavFunctionid.Enabled = 1;
         edtavName_Jsonclick = "";
         edtavName_Enabled = 0;
         cmbavTypeid_Jsonclick = "";
         cmbavTypeid.Enabled = 1;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Authentication type";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'cmbavTypeid'},{av:'AV34TypeId',fld:'vTYPEID',pic:'',nv:''},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV35TypeIdDsp',fld:'vTYPEIDDSP',pic:'',hsh:true,nv:''}],oparms:[{av:'divTblimpersonate_Visible',ctrl:'TBLIMPERSONATE',prop:'Visible'},{av:'divTblfacebook_Visible',ctrl:'TBLFACEBOOK',prop:'Visible'},{av:'divTblcommonadditional_Visible',ctrl:'TBLCOMMONADDITIONAL',prop:'Visible'},{av:'divTblserverhost_Visible',ctrl:'TBLSERVERHOST',prop:'Visible'},{av:'divTbltwitter_Visible',ctrl:'TBLTWITTER',prop:'Visible'},{av:'divTblwebservice_Visible',ctrl:'TBLWEBSERVICE',prop:'Visible'},{av:'divTblexternal_Visible',ctrl:'TBLEXTERNAL',prop:'Visible'}]}");
         setEventMetadata("VTYPEID.CLICK","{handler:'E13242',iparms:[],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E14242',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV28GAMRRepositoryGUID',fld:'vGAMRREPOSITORYGUID',pic:'',nv:''},{av:'AV27GAMRPrivateEncryptKey',fld:'vGAMRPRIVATEENCRYPTKEY',pic:'',nv:''},{av:'AV29GAMRServerURL',fld:'vGAMRSERVERURL',pic:'',nv:''},{av:'AV33SiteURL',fld:'vSITEURL',pic:'',nv:''},{av:'AV15ClientSecret',fld:'vCLIENTSECRET',pic:'',nv:''},{av:'AV14ClientId',fld:'vCLIENTID',pic:'',nv:''},{av:'AV5AdditionalScope',fld:'vADDITIONALSCOPE',pic:'',nv:''},{av:'AV13CallbackURL',fld:'vCALLBACKURL',pic:'',nv:''},{av:'AV17ConsumerSecret',fld:'vCONSUMERSECRET',pic:'',nv:''},{av:'AV16ConsumerKey',fld:'vCONSUMERKEY',pic:'',nv:''},{av:'cmbavWsserversecureprotocol'},{av:'AV43WSServerSecureProtocol',fld:'vWSSERVERSECUREPROTOCOL',pic:'9',nv:0},{av:'AV40WSServerBaseURL',fld:'vWSSERVERBASEURL',pic:'',nv:''},{av:'AV42WSServerPort',fld:'vWSSERVERPORT',pic:'ZZZZ9',nv:0},{av:'AV41WSServerName',fld:'vWSSERVERNAME',pic:'',nv:''},{av:'AV36WSExtension',fld:'vWSEXTENSION',pic:'',nv:''},{av:'AV37WSName',fld:'vWSNAME',pic:'',nv:''},{av:'AV38WSPackage',fld:'vWSPACKAGE',pic:'',nv:''},{av:'AV44WSTimeout',fld:'vWSTIMEOUT',pic:'ZZZZ9',nv:0},{av:'AV39WSPrivateEncryptKey',fld:'vWSPRIVATEENCRYPTKEY',pic:'',nv:''},{av:'cmbavWsversion'},{av:'AV45WSVersion',fld:'vWSVERSION',pic:'',nv:''},{av:'AV18CusClassName',fld:'vCUSCLASSNAME',pic:'',nv:''},{av:'AV20CusPackage',fld:'vCUSPACKAGE',pic:'',nv:''},{av:'AV19CusFileName',fld:'vCUSFILENAME',pic:'',nv:''},{av:'AV21CusPrivateEncryptKey',fld:'vCUSPRIVATEENCRYPTKEY',pic:'',nv:''},{av:'cmbavCusversion'},{av:'AV22CusVersion',fld:'vCUSVERSION',pic:'',nv:''},{av:'AV30Impersonate',fld:'vIMPERSONATE',pic:'',nv:''},{av:'AV23Dsc',fld:'vDSC',pic:'',nv:''},{av:'AV31IsEnable',fld:'vISENABLE',pic:'',nv:false},{av:'cmbavFunctionid'},{av:'AV26FunctionId',fld:'vFUNCTIONID',pic:'',nv:''},{av:'AV32Name',fld:'vNAME',pic:'',nv:''},{av:'cmbavTypeid'},{av:'AV34TypeId',fld:'vTYPEID',pic:'',nv:''},{av:'AV35TypeIdDsp',fld:'vTYPEIDDSP',pic:'',hsh:true,nv:''}],oparms:[{av:'cmbavFunctionid'}]}");
         setEventMetadata("'GENERATEKEY'","{handler:'E15242',iparms:[],oparms:[{av:'AV39WSPrivateEncryptKey',fld:'vWSPRIVATEENCRYPTKEY',pic:'',nv:''}]}");
         setEventMetadata("'GENERATEKEYCUSTOM'","{handler:'E16242',iparms:[],oparms:[{av:'AV21CusPrivateEncryptKey',fld:'vCUSPRIVATEENCRYPTKEY',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         wcpOAV32Name = "";
         wcpOAV35TypeIdDsp = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTextblock1_Jsonclick = "";
         ClassString = "";
         StyleString = "";
         TempTags = "";
         AV34TypeId = "";
         AV26FunctionId = "";
         AV23Dsc = "";
         AV30Impersonate = "";
         AV14ClientId = "";
         AV15ClientSecret = "";
         AV33SiteURL = "";
         AV16ConsumerKey = "";
         AV17ConsumerSecret = "";
         AV13CallbackURL = "";
         AV5AdditionalScope = "";
         AV29GAMRServerURL = "";
         AV27GAMRPrivateEncryptKey = "";
         AV28GAMRRepositoryGUID = "";
         AV45WSVersion = "";
         AV39WSPrivateEncryptKey = "";
         bttBtngenkey_Jsonclick = "";
         AV41WSServerName = "";
         AV40WSServerBaseURL = "";
         AV43WSServerSecureProtocol = 0;
         AV38WSPackage = "";
         AV37WSName = "";
         AV36WSExtension = "";
         AV22CusVersion = "";
         AV21CusPrivateEncryptKey = "";
         bttBtngenkeycustom_Jsonclick = "";
         AV19CusFileName = "";
         AV20CusPackage = "";
         AV18CusClassName = "";
         bttBtncancel_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV10AuthenticationTypeLocal = new SdtGAMAuthenticationTypeLocal(context);
         AV7AuthenticationTypeFacebook = new SdtGAMAuthenticationTypeFacebook(context);
         AV9AuthenticationTypeGoogle = new SdtGAMAuthenticationTypeGoogle(context);
         AV8AuthenticationTypeGAMRemote = new SdtGAMAuthenticationTypeGAMRemote(context);
         AV11AuthenticationTypeTwitter = new SdtGAMAuthenticationTypeTwitter(context);
         AV12AuthenticationTypeWebService = new SdtGAMAuthenticationTypeWebService(context);
         AV6AuthenticationTypeCustom = new SdtGAMAuthenticationTypeCustom(context);
         AV25Errors = new GXExternalCollection<SdtGAMError>( context, "SdtGAMError", "GeneXus.Programs");
         AV24Error = new SdtGAMError(context);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.gamexampleauthenticationtypeentry__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleauthenticationtypeentry__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short AV43WSServerSecureProtocol ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int edtavName_Enabled ;
      private int edtavDsc_Enabled ;
      private int divTblimpersonate_Visible ;
      private int edtavImpersonate_Enabled ;
      private int divTblfacebook_Visible ;
      private int edtavClientid_Enabled ;
      private int edtavClientsecret_Enabled ;
      private int edtavSiteurl_Enabled ;
      private int divTbltwitter_Visible ;
      private int edtavConsumerkey_Enabled ;
      private int edtavConsumersecret_Enabled ;
      private int edtavCallbackurl_Enabled ;
      private int divTblcommonadditional_Visible ;
      private int edtavAdditionalscope_Enabled ;
      private int divTblserverhost_Visible ;
      private int edtavGamrserverurl_Enabled ;
      private int edtavGamrprivateencryptkey_Enabled ;
      private int edtavGamrrepositoryguid_Enabled ;
      private int divTblwebservice_Visible ;
      private int edtavWsprivateencryptkey_Enabled ;
      private int bttBtngenkey_Visible ;
      private int edtavWsservername_Enabled ;
      private int AV42WSServerPort ;
      private int edtavWsserverport_Enabled ;
      private int edtavWsserverbaseurl_Enabled ;
      private int AV44WSTimeout ;
      private int edtavWstimeout_Enabled ;
      private int edtavWspackage_Enabled ;
      private int edtavWsname_Enabled ;
      private int edtavWsextension_Enabled ;
      private int divTblexternal_Visible ;
      private int edtavCusprivateencryptkey_Enabled ;
      private int bttBtngenkeycustom_Visible ;
      private int edtavCusfilename_Enabled ;
      private int edtavCuspackage_Enabled ;
      private int edtavCusclassname_Enabled ;
      private int bttBtnconfirm_Visible ;
      private int AV49GXV1 ;
      private int idxLst ;
      private String Gx_mode ;
      private String AV32Name ;
      private String AV35TypeIdDsp ;
      private String wcpOGx_mode ;
      private String wcpOAV32Name ;
      private String wcpOAV35TypeIdDsp ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String divTable3_Internalname ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String divTable2_Internalname ;
      private String divTbldata_Internalname ;
      private String cmbavTypeid_Internalname ;
      private String TempTags ;
      private String AV34TypeId ;
      private String cmbavTypeid_Jsonclick ;
      private String edtavName_Internalname ;
      private String edtavName_Jsonclick ;
      private String cmbavFunctionid_Internalname ;
      private String AV26FunctionId ;
      private String cmbavFunctionid_Jsonclick ;
      private String chkavIsenable_Internalname ;
      private String edtavDsc_Internalname ;
      private String AV23Dsc ;
      private String edtavDsc_Jsonclick ;
      private String divTblimpersonate_Internalname ;
      private String edtavImpersonate_Internalname ;
      private String AV30Impersonate ;
      private String edtavImpersonate_Jsonclick ;
      private String divTblfacebook_Internalname ;
      private String edtavClientid_Internalname ;
      private String edtavClientid_Jsonclick ;
      private String edtavClientsecret_Internalname ;
      private String edtavClientsecret_Jsonclick ;
      private String edtavSiteurl_Internalname ;
      private String edtavSiteurl_Jsonclick ;
      private String divTbltwitter_Internalname ;
      private String edtavConsumerkey_Internalname ;
      private String AV16ConsumerKey ;
      private String edtavConsumerkey_Jsonclick ;
      private String edtavConsumersecret_Internalname ;
      private String AV17ConsumerSecret ;
      private String edtavConsumersecret_Jsonclick ;
      private String edtavCallbackurl_Internalname ;
      private String edtavCallbackurl_Jsonclick ;
      private String divTblcommonadditional_Internalname ;
      private String edtavAdditionalscope_Internalname ;
      private String edtavAdditionalscope_Jsonclick ;
      private String divTblserverhost_Internalname ;
      private String edtavGamrserverurl_Internalname ;
      private String edtavGamrserverurl_Jsonclick ;
      private String edtavGamrprivateencryptkey_Internalname ;
      private String AV27GAMRPrivateEncryptKey ;
      private String edtavGamrprivateencryptkey_Jsonclick ;
      private String divFormcell_Internalname ;
      private String edtavGamrrepositoryguid_Internalname ;
      private String AV28GAMRRepositoryGUID ;
      private String edtavGamrrepositoryguid_Jsonclick ;
      private String divTblwebservice_Internalname ;
      private String cmbavWsversion_Internalname ;
      private String AV45WSVersion ;
      private String cmbavWsversion_Jsonclick ;
      private String edtavWsprivateencryptkey_Internalname ;
      private String AV39WSPrivateEncryptKey ;
      private String edtavWsprivateencryptkey_Jsonclick ;
      private String bttBtngenkey_Internalname ;
      private String bttBtngenkey_Jsonclick ;
      private String edtavWsservername_Internalname ;
      private String AV41WSServerName ;
      private String edtavWsservername_Jsonclick ;
      private String edtavWsserverport_Internalname ;
      private String edtavWsserverport_Jsonclick ;
      private String edtavWsserverbaseurl_Internalname ;
      private String AV40WSServerBaseURL ;
      private String edtavWsserverbaseurl_Jsonclick ;
      private String cmbavWsserversecureprotocol_Internalname ;
      private String cmbavWsserversecureprotocol_Jsonclick ;
      private String edtavWstimeout_Internalname ;
      private String edtavWstimeout_Jsonclick ;
      private String edtavWspackage_Internalname ;
      private String AV38WSPackage ;
      private String edtavWspackage_Jsonclick ;
      private String edtavWsname_Internalname ;
      private String AV37WSName ;
      private String edtavWsname_Jsonclick ;
      private String edtavWsextension_Internalname ;
      private String AV36WSExtension ;
      private String edtavWsextension_Jsonclick ;
      private String divTblexternal_Internalname ;
      private String cmbavCusversion_Internalname ;
      private String AV22CusVersion ;
      private String cmbavCusversion_Jsonclick ;
      private String edtavCusprivateencryptkey_Internalname ;
      private String AV21CusPrivateEncryptKey ;
      private String edtavCusprivateencryptkey_Jsonclick ;
      private String bttBtngenkeycustom_Internalname ;
      private String bttBtngenkeycustom_Jsonclick ;
      private String edtavCusfilename_Internalname ;
      private String AV19CusFileName ;
      private String edtavCusfilename_Jsonclick ;
      private String edtavCuspackage_Internalname ;
      private String AV20CusPackage ;
      private String edtavCuspackage_Jsonclick ;
      private String edtavCusclassname_Internalname ;
      private String AV18CusClassName ;
      private String edtavCusclassname_Jsonclick ;
      private String bttBtncancel_Internalname ;
      private String bttBtncancel_Jsonclick ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String bttBtnconfirm_Jsonclick ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool AV31IsEnable ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV14ClientId ;
      private String AV15ClientSecret ;
      private String AV33SiteURL ;
      private String AV13CallbackURL ;
      private String AV5AdditionalScope ;
      private String AV29GAMRServerURL ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private String aP1_Name ;
      private String aP2_TypeIdDsp ;
      private GXCombobox cmbavTypeid ;
      private GXCombobox cmbavFunctionid ;
      private GXCheckbox chkavIsenable ;
      private GXCombobox cmbavWsversion ;
      private GXCombobox cmbavWsserversecureprotocol ;
      private GXCombobox cmbavCusversion ;
      private IDataStoreProvider pr_gam ;
      private IDataStoreProvider pr_default ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXExternalCollection<SdtGAMError> AV25Errors ;
      private GXWebForm Form ;
      private SdtGAMAuthenticationTypeCustom AV6AuthenticationTypeCustom ;
      private SdtGAMError AV24Error ;
      private SdtGAMAuthenticationTypeFacebook AV7AuthenticationTypeFacebook ;
      private SdtGAMAuthenticationTypeGAMRemote AV8AuthenticationTypeGAMRemote ;
      private SdtGAMAuthenticationTypeGoogle AV9AuthenticationTypeGoogle ;
      private SdtGAMAuthenticationTypeLocal AV10AuthenticationTypeLocal ;
      private SdtGAMAuthenticationTypeTwitter AV11AuthenticationTypeTwitter ;
      private SdtGAMAuthenticationTypeWebService AV12AuthenticationTypeWebService ;
   }

   public class gamexampleauthenticationtypeentry__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class gamexampleauthenticationtypeentry__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
