window.MutationObserver = window.MutationObserver
						|| window.WebKitMutationObserver
						|| window.MozMutationObserver;

/* START - Global constants */
					
var k2btools = k2btools || {};

k2btools.checkboxSelector = 'input[type="checkbox"]:not(.gx-switch-Switch *)';
k2btools.selectSelector = 'select.K2BToolsEnhancedCombo';

k2btools.createdSelects = k2btools.createdSelects || []; // Used to refresh already created selects
k2btools.messagesShown = k2btools.messagesShown || []; // Used to avoid duplicating messages

/* END - Global constants */

/* START - Mutation observers - Event Handling */
k2btools.checkboxRefresh = k2btools.checkboxRefresh || function(){
	k2btools.$(k2btools.checkboxSelector).checkbox("refresh");
	
	k2btools.$(k2btools.checkboxSelector).each(function(i, element){
		k2btools.checkBoxObserver.observe(element, { attributes: true, subtree: false });
	});
}
						
k2btools.checkBoxObserver = k2btools.checkBoxObserver || new MutationObserver(function (mutations) {
	k2btools.checkboxRefresh();
});

k2btools.selectRefresh = k2btools.selectRefresh || function(){
	k2btools.$(k2btools.createdSelects).each(function(i, element){
		k2btools.$(element).selectpicker("refresh");
			
		if(k2btools.$(element).parents("#K2BTABLEACTIONSLEFTCONTAINER, #K2BTABLEACTIONSRIGHTCONTAINER, #K2BTABLEGRIDACTIONSLEFTCONTAINER, #K2BTABLEGRIDACTIONSRIGHTCONTAINER, #K2BTABLEACTIONSTOPCONTAINER, #K2BTABLEACTIONSBOTTOMCONTAINER, .Table_ComboActionsContainer, .Table_ActionsContainer, .K2BToolsTableCell_ActionContainer").length > 0)
		{
			if ((k2btools.$(element).find("[value!=0]").length == 0) || (k2btools.$(element).find("[value!='']").length == 0)){
				k2btools.$(element).selectpicker("hide");
			}else{
				k2btools.$(element).selectpicker("show");
			}
		}
		
		k2btools.selectObserver.observe(element, { attributes: true, subtree: true });
	});
}	

k2btools.selectObserver = k2btools.selectObserver || new MutationObserver(function (mutations) {
	k2btools.selectRefresh();
});

gx.fx.obs.addObserver('gx.onafterevent', this, function () {
	k2btools.checkboxRefresh();
	k2btools.selectRefresh();
}, { single: false });

/* END - Mutation observers - Event Handling */

function K2BControlBeautify(jQuery)
{
	k2btools.$ = jQuery;
	uc = this;
	
	this.show = function(){
		this.RefreshCheckboxes();
		this.RefreshSelects();
		this.RefreshTooltips();
		this.RefreshMenu();
		this.RefreshMessageHandling();
		this.RefreshMenuHide();
		this.RefreshTagsCollectionFocus();
		this.RefreshGridSettingsPosition();
		this.RefreshOnImagePrompt();
	}
	
	this.RefreshGridSettingsPosition = function(){
		k2btools.$(".K2BToolsTable_GridSettings").each(function(i, item){
			var itemWidth = k2btools.$(item).outerWidth();
			var parentWidth = k2btools.$(item).closest(".K2BToolsTable_GridSettingsContainer").outerWidth();
			
			var left = itemWidth - parentWidth;
			k2btools.$(item).css({left: -left});
		}); 
		
		k2btools.$(".K2BToolsMyAccountTable").each(function(i, item){
			var itemWidth = k2btools.$(item).outerWidth();
			var parentWidth = k2btools.$(item).closest(".K2BToolsTable_MyAccountContainer").outerWidth();
			
			var left = itemWidth - parentWidth;
			k2btools.$(item).css({left: -left});
		}); 
	}
	
	this.destroy = function(){
		this.CleanupMenuHide();
		this.CleanupMenu();
	}
	
	this.CleanupMenuHide = function(){
		k2btools.$('html').off('click', this.htmlClickCallback);
	}
	
	this.CleanupMenu = function(){
		k2btools.$("#K2BToolsMenu a").off("click", this.onMenuClick);
	}
	
	this.ParentsCheck = function(element, hops){
		i = 0;
		while(i<hops && element != document.documentElement && element.parentElement!=null){
			element = element.parentElement;
			i++;
		}
		return element.parentElement != null || element == document.documentElement;
	}
	
	// Used to hide menus
	this.htmlClickCallback = function (e) {
		k2btools.$(".K2BToolsTable_GridSettings:visible, .K2BToolsMyAccountTable:visible,  .ControlBeautify_CollapsableTable:visible").each(function(index, element){
			var containerTable = k2btools.$(element).closest(".K2BToolsTable_GridSettingsContainer, .K2BToolsTable_MyAccountContainer, .ControlBeautify_ParentCollapsableTable");
			
			if ((uc.ParentsCheck(e.target,10))&&(k2btools.$(e.target).closest(containerTable).length < 1)) {
				k2btools.$(element).hide();
			}
		});
	}
	
	this.RefreshMenuHide = function(){
		k2btools.$('html').on('click', this.htmlClickCallback);
	}
	
	this.RefreshCheckboxes = function(){
		k2btools.checkboxRefresh();
	}

	this.insideActions = function(item){
		return k2btools.$(item).parents("#K2BTABLEACTIONSLEFTCONTAINER, #K2BTABLEACTIONSRIGHTCONTAINER, #K2BTABLEGRIDACTIONSLEFTCONTAINER, #K2BTABLEGRIDACTIONSRIGHTCONTAINER, #K2BTABLEACTIONSTOPCONTAINER, #K2BTABLEACTIONSBOTTOMCONTAINER, .Table_ComboActionsContainer, .Table_ActionsContainer, .K2BToolsTableCell_ActionContainer").length > 0;
	}
	
	this.RefreshSelects = function(){
		var selects = k2btools.$("select.K2BToolsEnhancedCombo");
		
		selects = selects.filter(function(i, item){
			return gx.fn.isVisible(item, 0) && k2btools.$.inArray(item, k2btools.createdSelects) == -1;
		});
		
		if(selects.length != 0){
			k2btools.$(selects).each(function(i, item){
				if(uc.insideActions(item))
				{
					k2btools.$(item).children("option[value=0]").attr("disabled", "disabled");
				}
			});
			
			k2btools.$(selects).removeClass().addClass("K2BToolsEnhancedCombo").selectpicker();
			
			k2btools.$(selects).each(function(i, item){
				if(uc.insideActions(item))
				{
					if ((k2btools.$(item).find("[value!=0]").length == 0) || (k2btools.$(item).find("[value!='']").length == 0)){
						k2btools.$(item).selectpicker("hide");
					}else{
						k2btools.$(item).selectpicker("show");
					}
				}
			});
			
			k2btools.$(selects).selectpicker("refresh");
			
			k2btools.$(selects).bind("DOMSubtreeModified", function() {
				k2btools.$(this).selectpicker("refresh");
			});
		} 
		
		k2btools.createdSelects = k2btools.$.extend(true, k2btools.createdSelects, selects);
		k2btools.selectRefresh();
	}

	this.RefreshTooltips = function(){	
		k2btools.$(".Image_Action, .K2BImage_ContextHelp, .K2BContextHelp").tooltip({delay: {show: 500, hide: 500}, container:'body'});
	}

	this.RefreshMenu = function(){
		var urlComps = window.location.pathname.split("/");
		var scriptName = urlComps[urlComps.length-1];
		
		if(!this.IsPostBack){
			var currentWPinMenu = false;
			k2btools.$("#K2BToolsMenu a").each(function(i, item){
				if(k2btools.$(item).attr("href") == scriptName){
					currentWPinMenu = true;
				}
			});
			
			if(currentWPinMenu){
				k2btools.$("#K2BToolsMenu a").removeClass("activeOption");
				k2btools.$("#K2BToolsMenu a").each(function(i, item){
					if(k2btools.$(item).attr("href") == scriptName){
						k2btools.$(item).addClass("activeOption");
					}
				});
			}
		}
		
		k2btools.$("#K2BToolsMenu a").on("click", this.onMenuClick);
	}
	
	this.onMenuClick = function(){ 
		k2btools.$("#K2BToolsMenu a").removeClass("activeOption");
		k2btools.$(this).addClass("activeOption")
	}

	this.messages_handler_added = false;
	
	this.RefreshMessageHandling = function(){
		if(!this.IsPostBack){
			if(!this.messages_handler_added){				
				this.messages_handler_added = true;
				
				toastr.options = {
				  "closeButton": true,
				  "debug": false,
				  "positionClass": "toast-position",
				  "onclick": null,
				  "showDuration": "1000",
				  "hideDuration": "1000",
				  "timeOut": "8000",
				  "extendedTimeOut": "8000",
				  "showEasing": "swing",
				  "hideEasing": "linear",
				  "showMethod": "fadeIn",
				  "hideMethod": "fadeOut"
				}
				
				gx.fx.obs.addObserver('gx.onmessages', this, function(messages){
					k2btools.$.each(messages, function(i, msgs){
						k2btools.$.each(msgs, uc.showMessage);
					});
				});
			}
			
			var pendingMessages = k2btools.$(".gx_ev").first().children().map(function(){ return {text:k2btools.$(this).html()}; });
			k2btools.$.each(pendingMessages, uc.showMessage);
		}
		
		k2btools.$(".gx_ev").hide();
	}
	
	this.showMessage = function(i, msg){
		if (!(Object.prototype.toString.call(msg) === '[object Array]')) {
			// Refresh shown messages - take those generated in the last second
			k2btools.messagesShown = k2btools.$(k2btools.messagesShown).filter(function(index, element){
				return (new Date().getTime() - element.timestamp) < 1000;
			});
			
			if(k2btools.$(k2btools.messagesShown).filter(function(index, element){ 
				return element.message.text === msg.text; 
			}).length == 0){
				k2btools.messagesShown.push({timestamp: new Date().getTime(), message:msg});
				if((msg.type === 1) || (msg.value===1)){
					toastr.error(msg.text);
				}else{
					toastr.success(msg.text);
				}
			}
		}
	}
	
	this.RefreshTagsCollectionFocus = function(){
		k2btools.$(".K2BToolsAttribute_BorderlessFilter").on("focusin", function(){ k2btools.$(this).closest(".K2BToolsTable_FieldBorder").addClass("K2BToolsTable_FieldBorderFocus") });
		k2btools.$(".K2BToolsAttribute_BorderlessFilter").on("focusout", function(){ k2btools.$(this).closest(".K2BToolsTable_FieldBorderFocus").removeClass("K2BToolsTable_FieldBorderFocus") });
	}
	
	this.RefreshOnImagePrompt = function(){
		k2btools.$(".K2BToolsOnImagePrompt").closest("A").addClass("btn btn-default");
		k2btools.$(".K2BToolsSection_PromptImageAndFieldContainer").on("focusin", function(){ k2btools.$(this).addClass("K2BToolsSection_PromptImageAndFieldContainerFocus") });
		k2btools.$(".K2BToolsSection_PromptImageAndFieldContainer").on("focusout", function(){ k2btools.$(this).removeClass("K2BToolsSection_PromptImageAndFieldContainerFocus") });
	}
}

/* Old checkall code */

function checkall(ev) {
    checkallgrid(ev, 'vSEL');
}

function getEventTarget(e) {
  e = e || window.event;
  return e.target || e.srcElement;
}

function checkallgrid(e, checksNamePart) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

	var target = getEventTarget(e);
	
    for (var i = 0; i < document.MAINFORM.elements.length-1; i++) {
        var c = document.MAINFORM.elements[i];

        if (((c.type == 'checkbox') && (c.name != 'allbox')) && (c.name.toUpperCase().indexOf(checksNamePart.toUpperCase()) >= 0)) {
            
			if (c.checked != target.checked) {
				k2btools.$(c).trigger("click");
				k2btools.$(c).prop("checked", target.checked);
				k2btools.$(c).checkbox("refresh");
			}
        }
    }
}

/* End old checkall code */