/*
               File: Recorrido
        Description: Recorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:38:56.46
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class recorrido : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_14") == 0 )
         {
            A21RecorridoDetallePaciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            n21RecorridoDetallePaciente = false;
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_14( A21RecorridoDetallePaciente) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_15") == 0 )
         {
            A22IDCoach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            n22IDCoach = false;
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_15( A22IDCoach) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Gridrecorrido_detalle") == 0 )
         {
            nRC_GXsfl_39 = (short)(NumberUtil.Val( GetNextPar( ), "."));
            nGXsfl_39_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
            sGXsfl_39_idx = GetNextPar( );
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxnrGridrecorrido_detalle_newrow( ) ;
            return  ;
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         GXCCtl = "TIPOSENAL_" + sGXsfl_39_idx;
         cmbTipoSenal.Name = GXCCtl;
         cmbTipoSenal.WebTags = "";
         cmbTipoSenal.addItem("1", "Inicio", 0);
         cmbTipoSenal.addItem("2", "Progreso", 0);
         cmbTipoSenal.addItem("3", "Fin", 0);
         if ( cmbTipoSenal.ItemCount > 0 )
         {
            A25TipoSenal = (short)(NumberUtil.Val( cmbTipoSenal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0))), "."));
            n25TipoSenal = false;
         }
         GXCCtl = "TIPOOBSTACULO_" + sGXsfl_39_idx;
         cmbTipoObstaculo.Name = GXCCtl;
         cmbTipoObstaculo.WebTags = "";
         cmbTipoObstaculo.addItem("0", "Ninguno", 0);
         cmbTipoObstaculo.addItem("1", "Agujero", 0);
         cmbTipoObstaculo.addItem("2", "Gradas", 0);
         cmbTipoObstaculo.addItem("3", "Otros", 0);
         if ( cmbTipoObstaculo.ItemCount > 0 )
         {
            A28TipoObstaculo = (short)(NumberUtil.Val( cmbTipoObstaculo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A28TipoObstaculo), 4, 0))), "."));
            n28TipoObstaculo = false;
         }
         GXCCtl = "INCIDENTE_" + sGXsfl_39_idx;
         cmbIncidente.Name = GXCCtl;
         cmbIncidente.WebTags = "";
         cmbIncidente.addItem("0", "Nada", 0);
         cmbIncidente.addItem("1", "Registrar Obstaculo", 0);
         if ( cmbIncidente.ItemCount > 0 )
         {
            A29Incidente = (short)(NumberUtil.Val( cmbIncidente.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A29Incidente), 4, 0))), "."));
            n29Incidente = false;
         }
         GXCCtl = "EMERGENCIA_" + sGXsfl_39_idx;
         cmbEmergencia.Name = GXCCtl;
         cmbEmergencia.WebTags = "";
         cmbEmergencia.addItem("0", "Nada", 0);
         cmbEmergencia.addItem("1", "Emergencia", 0);
         if ( cmbEmergencia.ItemCount > 0 )
         {
            A30Emergencia = (short)(NumberUtil.Val( cmbEmergencia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A30Emergencia), 4, 0))), "."));
            n30Emergencia = false;
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Recorrido", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtRecorridoID_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("K2BFlat");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public recorrido( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public recorrido( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbTipoSenal = new GXCombobox();
         cmbTipoObstaculo = new GXCombobox();
         cmbIncidente = new GXCombobox();
         cmbEmergencia = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "recorrido_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablemain_Internalname, 1, 0, "px", 0, "px", "Container FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Recorrido", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "|<", bttBtn_first_Jsonclick, 5, "|<", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "<", bttBtn_previous_Jsonclick, 5, "<", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", ">", bttBtn_next_Jsonclick, 5, ">", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", ">|", bttBtn_last_Jsonclick, 5, ">|", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0030.aspx"+"',["+"{Ctrl:gx.dom.el('"+"RECORRIDOID"+"'), id:'"+"RECORRIDOID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRecorridoID_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtRecorridoID_Internalname, "ID", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtRecorridoID_Internalname, A23RecorridoID.ToString(), A23RecorridoID.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRecorridoID_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRecorridoID_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtRecorridoFecha_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtRecorridoFecha_Internalname, "Fecha", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtRecorridoFecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtRecorridoFecha_Internalname, context.localUtil.TToC( A24RecorridoFecha, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A24RecorridoFecha, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'spa',false,0);"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtRecorridoFecha_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtRecorridoFecha_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Recorrido.htm");
            GxWebStd.gx_bitmap( context, edtRecorridoFecha_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtRecorridoFecha_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Recorrido.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitledetalle_Internalname, "Detalle", "", "", lblTitledetalle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            gxdraw_Gridrecorrido_detalle( ) ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 55,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 59,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Recorrido.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void gxdraw_Gridrecorrido_detalle( )
      {
         /*  Grid Control  */
         Gridrecorrido_detalleContainer.AddObjectProperty("GridName", "Gridrecorrido_detalle");
         Gridrecorrido_detalleContainer.AddObjectProperty("Class", "Grid");
         Gridrecorrido_detalleContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrecorrido_detalle_Backcolorstyle), 1, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("CmpContext", "");
         Gridrecorrido_detalleContainer.AddObjectProperty("InMasterPage", "false");
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", A32RecorridoDetalleID.ToString());
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", A21RecorridoDetallePaciente.ToString());
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetallePaciente_Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", A22IDCoach.ToString());
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIDCoach_Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A25TipoSenal), 4, 0, ".", "")));
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoSenal.Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", A26Longitud);
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLongitud_Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", A27Latitud);
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLatitud_Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A28TipoObstaculo), 4, 0, ".", "")));
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoObstaculo.Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Incidente), 4, 0, ".", "")));
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbIncidente.Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(A30Emergencia), 4, 0, ".", "")));
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbEmergencia.Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", A31IMEI);
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIMEI_Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
         Gridrecorrido_detalleColumn.AddObjectProperty("Value", context.localUtil.TToC( A33TimeStamp, 10, 8, 0, 3, "/", ":", " "));
         Gridrecorrido_detalleColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTimeStamp_Enabled), 5, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddColumnProperties(Gridrecorrido_detalleColumn);
         Gridrecorrido_detalleContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrecorrido_detalle_Allowselection), 1, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrecorrido_detalle_Selectioncolor), 9, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrecorrido_detalle_Allowhovering), 1, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrecorrido_detalle_Hoveringcolor), 9, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrecorrido_detalle_Allowcollapsing), 1, 0, ".", "")));
         Gridrecorrido_detalleContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGridrecorrido_detalle_Collapsed), 1, 0, ".", "")));
         nGXsfl_39_idx = 0;
         if ( ( nKeyPressed == 1 ) && ( AnyError == 0 ) )
         {
            /* Enter key processing. */
            nBlankRcdCount5 = 5;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               /* Display confirmed (stored) records */
               nRcdExists_5 = 1;
               ScanStart035( ) ;
               while ( RcdFound5 != 0 )
               {
                  init_level_properties5( ) ;
                  getByPrimaryKey035( ) ;
                  AddRow035( ) ;
                  ScanNext035( ) ;
               }
               ScanEnd035( ) ;
               nBlankRcdCount5 = 5;
            }
         }
         else if ( ( nKeyPressed == 3 ) || ( nKeyPressed == 4 ) || ( ( nKeyPressed == 1 ) && ( AnyError != 0 ) ) )
         {
            /* Button check  or addlines. */
            standaloneNotModal035( ) ;
            standaloneModal035( ) ;
            sMode5 = Gx_mode;
            while ( nGXsfl_39_idx < nRC_GXsfl_39 )
            {
               bGXsfl_39_Refreshing = true;
               ReadRow035( ) ;
               edtRecorridoDetalleID_Enabled = (int)(context.localUtil.CToN( cgiGet( "RECORRIDODETALLEID_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoDetalleID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               edtRecorridoDetallePaciente_Enabled = (int)(context.localUtil.CToN( cgiGet( "RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoDetallePaciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoDetallePaciente_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               edtIDCoach_Enabled = (int)(context.localUtil.CToN( cgiGet( "IDCOACH_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIDCoach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIDCoach_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               cmbTipoSenal.Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPOSENAL_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoSenal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTipoSenal.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               edtLongitud_Enabled = (int)(context.localUtil.CToN( cgiGet( "LONGITUD_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLongitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLongitud_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               edtLatitud_Enabled = (int)(context.localUtil.CToN( cgiGet( "LATITUD_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLatitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLatitud_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               cmbTipoObstaculo.Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPOOBSTACULO_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoObstaculo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTipoObstaculo.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               cmbIncidente.Enabled = (int)(context.localUtil.CToN( cgiGet( "INCIDENTE_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIncidente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbIncidente.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               cmbEmergencia.Enabled = (int)(context.localUtil.CToN( cgiGet( "EMERGENCIA_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbEmergencia.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               edtIMEI_Enabled = (int)(context.localUtil.CToN( cgiGet( "IMEI_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIMEI_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIMEI_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               edtTimeStamp_Enabled = (int)(context.localUtil.CToN( cgiGet( "TIMESTAMP_"+sGXsfl_39_idx+"Enabled"), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTimeStamp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTimeStamp_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
               imgprompt_21_Link = cgiGet( "PROMPT_21_"+sGXsfl_39_idx+"Link");
               imgprompt_21_Link = cgiGet( "PROMPT_22_"+sGXsfl_39_idx+"Link");
               if ( ( nRcdExists_5 == 0 ) && ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal035( ) ;
               }
               SendRow035( ) ;
               bGXsfl_39_Refreshing = false;
            }
            Gx_mode = sMode5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            /* Get or get-alike key processing. */
            nBlankRcdCount5 = 5;
            nRcdExists_5 = 1;
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               ScanStart035( ) ;
               while ( RcdFound5 != 0 )
               {
                  sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx+1), 4, 0)), 4, "0");
                  SubsflControlProps_395( ) ;
                  init_level_properties5( ) ;
                  standaloneNotModal035( ) ;
                  getByPrimaryKey035( ) ;
                  standaloneModal035( ) ;
                  AddRow035( ) ;
                  ScanNext035( ) ;
               }
               ScanEnd035( ) ;
            }
         }
         /* Initialize fields for 'new' records and send them. */
         sMode5 = Gx_mode;
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx+1), 4, 0)), 4, "0");
         SubsflControlProps_395( ) ;
         InitAll035( ) ;
         init_level_properties5( ) ;
         standaloneNotModal035( ) ;
         standaloneModal035( ) ;
         nRcdExists_5 = 0;
         nIsMod_5 = 0;
         nRcdDeleted_5 = 0;
         nBlankRcdCount5 = (short)(nBlankRcdUsr5+nBlankRcdCount5);
         fRowAdded = 0;
         while ( nBlankRcdCount5 > 0 )
         {
            AddRow035( ) ;
            if ( ( nKeyPressed == 4 ) && ( fRowAdded == 0 ) )
            {
               fRowAdded = 1;
               GX_FocusControl = edtRecorridoDetalleID_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nBlankRcdCount5 = (short)(nBlankRcdCount5-1);
         }
         Gx_mode = sMode5;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         sStyleString = "";
         context.WriteHtmlText( "<div id=\""+"Gridrecorrido_detalleContainer"+"Div\" "+sStyleString+">"+"</div>") ;
         context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Gridrecorrido_detalle", Gridrecorrido_detalleContainer);
         if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridrecorrido_detalleContainerData", Gridrecorrido_detalleContainer.ToJavascriptSource());
         }
         if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
         {
            GxWebStd.gx_hidden_field( context, "Gridrecorrido_detalleContainerData"+"V", Gridrecorrido_detalleContainer.GridValuesHidden());
         }
         else
         {
            context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"Gridrecorrido_detalleContainerData"+"V"+"\" value='"+Gridrecorrido_detalleContainer.GridValuesHidden()+"'/>") ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( StringUtil.StrCmp(cgiGet( edtRecorridoID_Internalname), "") == 0 )
               {
                  A23RecorridoID = (Guid)(Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
               }
               else
               {
                  try
                  {
                     A23RecorridoID = (Guid)(StringUtil.StrToGuid( cgiGet( edtRecorridoID_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "RECORRIDOID");
                     AnyError = 1;
                     GX_FocusControl = edtRecorridoID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtRecorridoFecha_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Recorrido Fecha"}), 1, "RECORRIDOFECHA");
                  AnyError = 1;
                  GX_FocusControl = edtRecorridoFecha_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A24RecorridoFecha = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24RecorridoFecha", context.localUtil.TToC( A24RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A24RecorridoFecha = context.localUtil.CToT( cgiGet( edtRecorridoFecha_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24RecorridoFecha", context.localUtil.TToC( A24RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
               }
               /* Read saved values. */
               Z23RecorridoID = (Guid)(StringUtil.StrToGuid( cgiGet( "Z23RecorridoID")));
               Z24RecorridoFecha = context.localUtil.CToT( cgiGet( "Z24RecorridoFecha"), 0);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               nRC_GXsfl_39 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_39"), ",", "."));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               /* Check if conditions changed and reset current page numbers */
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A23RecorridoID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                        sEvtType = StringUtil.Right( sEvt, 4);
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll033( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes033( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_035( )
      {
         nGXsfl_39_idx = 0;
         while ( nGXsfl_39_idx < nRC_GXsfl_39 )
         {
            ReadRow035( ) ;
            if ( ( nRcdExists_5 != 0 ) || ( nIsMod_5 != 0 ) )
            {
               GetKey035( ) ;
               if ( ( nRcdExists_5 == 0 ) && ( nRcdDeleted_5 == 0 ) )
               {
                  if ( RcdFound5 == 0 )
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     BeforeValidate035( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable035( ) ;
                        CloseExtendedTableCursors035( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                        }
                     }
                  }
                  else
                  {
                     GXCCtl = "RECORRIDODETALLEID_" + sGXsfl_39_idx;
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, GXCCtl);
                     AnyError = 1;
                     GX_FocusControl = edtRecorridoDetalleID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( RcdFound5 != 0 )
                  {
                     if ( nRcdDeleted_5 != 0 )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        getByPrimaryKey035( ) ;
                        Load035( ) ;
                        BeforeValidate035( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls035( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_5 != 0 )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           BeforeValidate035( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable035( ) ;
                              CloseExtendedTableCursors035( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_5 == 0 )
                     {
                        GXCCtl = "RECORRIDODETALLEID_" + sGXsfl_39_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtRecorridoDetalleID_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtRecorridoDetalleID_Internalname, A32RecorridoDetalleID.ToString()) ;
            ChangePostValue( edtRecorridoDetallePaciente_Internalname, A21RecorridoDetallePaciente.ToString()) ;
            ChangePostValue( edtIDCoach_Internalname, A22IDCoach.ToString()) ;
            ChangePostValue( cmbTipoSenal_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25TipoSenal), 4, 0, ".", ""))) ;
            ChangePostValue( edtLongitud_Internalname, A26Longitud) ;
            ChangePostValue( edtLatitud_Internalname, A27Latitud) ;
            ChangePostValue( cmbTipoObstaculo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A28TipoObstaculo), 4, 0, ".", ""))) ;
            ChangePostValue( cmbIncidente_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Incidente), 4, 0, ".", ""))) ;
            ChangePostValue( cmbEmergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A30Emergencia), 4, 0, ".", ""))) ;
            ChangePostValue( edtIMEI_Internalname, A31IMEI) ;
            ChangePostValue( edtTimeStamp_Internalname, context.localUtil.TToC( A33TimeStamp, 10, 8, 0, 3, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z32RecorridoDetalleID_"+sGXsfl_39_idx, Z32RecorridoDetalleID.ToString()) ;
            ChangePostValue( "ZT_"+"Z25TipoSenal_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25TipoSenal), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z28TipoObstaculo_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z28TipoObstaculo), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z29Incidente_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29Incidente), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z30Emergencia_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z30Emergencia), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z31IMEI_"+sGXsfl_39_idx, Z31IMEI) ;
            ChangePostValue( "ZT_"+"Z33TimeStamp_"+sGXsfl_39_idx, context.localUtil.TToC( Z33TimeStamp, 10, 8, 0, 0, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z21RecorridoDetallePaciente_"+sGXsfl_39_idx, Z21RecorridoDetallePaciente.ToString()) ;
            ChangePostValue( "ZT_"+"Z22IDCoach_"+sGXsfl_39_idx, Z22IDCoach.ToString()) ;
            ChangePostValue( "nRcdDeleted_5_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_5), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_5_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_5), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_5_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_5), 4, 0, ",", ""))) ;
            if ( nIsMod_5 != 0 )
            {
               ChangePostValue( "RECORRIDODETALLEID_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetallePaciente_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "IDCOACH_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIDCoach_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPOSENAL_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoSenal.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "LONGITUD_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLongitud_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "LATITUD_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLatitud_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPOOBSTACULO_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoObstaculo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "INCIDENTE_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbIncidente.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "EMERGENCIA_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbEmergencia.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "IMEI_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIMEI_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIMESTAMP_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTimeStamp_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ResetCaption030( )
      {
      }

      protected void ZM033( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z24RecorridoFecha = T00037_A24RecorridoFecha[0];
            }
            else
            {
               Z24RecorridoFecha = A24RecorridoFecha;
            }
         }
         if ( GX_JID == -12 )
         {
            Z23RecorridoID = (Guid)(A23RecorridoID);
            Z24RecorridoFecha = A24RecorridoFecha;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load033( )
      {
         /* Using cursor T00038 */
         pr_default.execute(6, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound3 = 1;
            A24RecorridoFecha = T00038_A24RecorridoFecha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24RecorridoFecha", context.localUtil.TToC( A24RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
            ZM033( -12) ;
         }
         pr_default.close(6);
         OnLoadActions033( ) ;
      }

      protected void OnLoadActions033( )
      {
      }

      protected void CheckExtendedTable033( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A24RecorridoFecha) || ( A24RecorridoFecha >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Recorrido Fecha fuera de rango", "OutOfRange", 1, "RECORRIDOFECHA");
            AnyError = 1;
            GX_FocusControl = edtRecorridoFecha_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors033( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey033( )
      {
         /* Using cursor T00039 */
         pr_default.execute(7, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound3 = 1;
         }
         else
         {
            RcdFound3 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00037 */
         pr_default.execute(5, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(5) != 101) )
         {
            ZM033( 12) ;
            RcdFound3 = 1;
            A23RecorridoID = (Guid)((Guid)(T00037_A23RecorridoID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
            A24RecorridoFecha = T00037_A24RecorridoFecha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24RecorridoFecha", context.localUtil.TToC( A24RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
            Z23RecorridoID = (Guid)(A23RecorridoID);
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load033( ) ;
            if ( AnyError == 1 )
            {
               RcdFound3 = 0;
               InitializeNonKey033( ) ;
            }
            Gx_mode = sMode3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound3 = 0;
            InitializeNonKey033( ) ;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode3;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(5);
      }

      protected void getEqualNoModal( )
      {
         GetKey033( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound3 = 0;
         /* Using cursor T000310 */
         pr_default.execute(8, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( GuidUtil.Compare(T000310_A23RecorridoID[0], A23RecorridoID, 1) < 0 ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( GuidUtil.Compare(T000310_A23RecorridoID[0], A23RecorridoID, 1) > 0 ) ) )
            {
               A23RecorridoID = (Guid)((Guid)(T000310_A23RecorridoID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
               RcdFound3 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound3 = 0;
         /* Using cursor T000311 */
         pr_default.execute(9, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( GuidUtil.Compare(T000311_A23RecorridoID[0], A23RecorridoID, 1) > 0 ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( GuidUtil.Compare(T000311_A23RecorridoID[0], A23RecorridoID, 1) < 0 ) ) )
            {
               A23RecorridoID = (Guid)((Guid)(T000311_A23RecorridoID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
               RcdFound3 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey033( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtRecorridoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert033( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound3 == 1 )
            {
               if ( A23RecorridoID != Z23RecorridoID )
               {
                  A23RecorridoID = (Guid)(Z23RecorridoID);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "RECORRIDOID");
                  AnyError = 1;
                  GX_FocusControl = edtRecorridoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtRecorridoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update033( ) ;
                  GX_FocusControl = edtRecorridoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A23RecorridoID != Z23RecorridoID )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtRecorridoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert033( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "RECORRIDOID");
                     AnyError = 1;
                     GX_FocusControl = edtRecorridoID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtRecorridoID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert033( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A23RecorridoID != Z23RecorridoID )
         {
            A23RecorridoID = (Guid)(Z23RecorridoID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "RECORRIDOID");
            AnyError = 1;
            GX_FocusControl = edtRecorridoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtRecorridoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "RECORRIDOID");
            AnyError = 1;
            GX_FocusControl = edtRecorridoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtRecorridoFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart033( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRecorridoFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd033( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRecorridoFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRecorridoFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart033( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound3 != 0 )
            {
               ScanNext033( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtRecorridoFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd033( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency033( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00036 */
            pr_default.execute(4, new Object[] {A23RecorridoID});
            if ( (pr_default.getStatus(4) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Recorrido"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(4) == 101) || ( Z24RecorridoFecha != T00036_A24RecorridoFecha[0] ) )
            {
               if ( Z24RecorridoFecha != T00036_A24RecorridoFecha[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"RecorridoFecha");
                  GXUtil.WriteLogRaw("Old: ",Z24RecorridoFecha);
                  GXUtil.WriteLogRaw("Current: ",T00036_A24RecorridoFecha[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Recorrido"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert033( )
      {
         if ( ! IsAuthorized("recorrido_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable033( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM033( 0) ;
            CheckOptimisticConcurrency033( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm033( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert033( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000312 */
                     pr_default.execute(10, new Object[] {A23RecorridoID, A24RecorridoFecha});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Recorrido") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel033( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                              ResetCaption030( ) ;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load033( ) ;
            }
            EndLevel033( ) ;
         }
         CloseExtendedTableCursors033( ) ;
      }

      protected void Update033( )
      {
         if ( ! IsAuthorized("recorrido_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable033( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency033( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm033( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate033( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000313 */
                     pr_default.execute(11, new Object[] {A24RecorridoFecha, A23RecorridoID});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("Recorrido") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Recorrido"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate033( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel033( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                              ResetCaption030( ) ;
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel033( ) ;
         }
         CloseExtendedTableCursors033( ) ;
      }

      protected void DeferredUpdate033( )
      {
      }

      protected void delete( )
      {
         if ( ! IsAuthorized("recorrido_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency033( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls033( ) ;
            AfterConfirm033( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete033( ) ;
               if ( AnyError == 0 )
               {
                  ScanStart035( ) ;
                  while ( RcdFound5 != 0 )
                  {
                     getByPrimaryKey035( ) ;
                     Delete035( ) ;
                     ScanNext035( ) ;
                  }
                  ScanEnd035( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000314 */
                     pr_default.execute(12, new Object[] {A23RecorridoID});
                     pr_default.close(12);
                     dsDefault.SmartCacheProvider.SetUpdated("Recorrido") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           move_next( ) ;
                           if ( RcdFound3 == 0 )
                           {
                              InitAll033( ) ;
                              Gx_mode = "INS";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           else
                           {
                              getByPrimaryKey( ) ;
                              Gx_mode = "UPD";
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           }
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                           ResetCaption030( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode3 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel033( ) ;
         Gx_mode = sMode3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls033( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void ProcessNestedLevel035( )
      {
         nGXsfl_39_idx = 0;
         while ( nGXsfl_39_idx < nRC_GXsfl_39 )
         {
            ReadRow035( ) ;
            if ( ( nRcdExists_5 != 0 ) || ( nIsMod_5 != 0 ) )
            {
               standaloneNotModal035( ) ;
               GetKey035( ) ;
               if ( ( nRcdExists_5 == 0 ) && ( nRcdDeleted_5 == 0 ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  Insert035( ) ;
               }
               else
               {
                  if ( RcdFound5 != 0 )
                  {
                     if ( ( nRcdDeleted_5 != 0 ) && ( nRcdExists_5 != 0 ) )
                     {
                        Gx_mode = "DLT";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        Delete035( ) ;
                     }
                     else
                     {
                        if ( ( nIsMod_5 != 0 ) && ( nRcdExists_5 != 0 ) )
                        {
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                           Update035( ) ;
                        }
                     }
                  }
                  else
                  {
                     if ( nRcdDeleted_5 == 0 )
                     {
                        GXCCtl = "RECORRIDODETALLEID_" + sGXsfl_39_idx;
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, GXCCtl);
                        AnyError = 1;
                        GX_FocusControl = edtRecorridoDetalleID_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
            ChangePostValue( edtRecorridoDetalleID_Internalname, A32RecorridoDetalleID.ToString()) ;
            ChangePostValue( edtRecorridoDetallePaciente_Internalname, A21RecorridoDetallePaciente.ToString()) ;
            ChangePostValue( edtIDCoach_Internalname, A22IDCoach.ToString()) ;
            ChangePostValue( cmbTipoSenal_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A25TipoSenal), 4, 0, ".", ""))) ;
            ChangePostValue( edtLongitud_Internalname, A26Longitud) ;
            ChangePostValue( edtLatitud_Internalname, A27Latitud) ;
            ChangePostValue( cmbTipoObstaculo_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A28TipoObstaculo), 4, 0, ".", ""))) ;
            ChangePostValue( cmbIncidente_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A29Incidente), 4, 0, ".", ""))) ;
            ChangePostValue( cmbEmergencia_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A30Emergencia), 4, 0, ".", ""))) ;
            ChangePostValue( edtIMEI_Internalname, A31IMEI) ;
            ChangePostValue( edtTimeStamp_Internalname, context.localUtil.TToC( A33TimeStamp, 10, 8, 0, 3, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z32RecorridoDetalleID_"+sGXsfl_39_idx, Z32RecorridoDetalleID.ToString()) ;
            ChangePostValue( "ZT_"+"Z25TipoSenal_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25TipoSenal), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z28TipoObstaculo_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z28TipoObstaculo), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z29Incidente_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29Incidente), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z30Emergencia_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z30Emergencia), 4, 0, ",", ""))) ;
            ChangePostValue( "ZT_"+"Z31IMEI_"+sGXsfl_39_idx, Z31IMEI) ;
            ChangePostValue( "ZT_"+"Z33TimeStamp_"+sGXsfl_39_idx, context.localUtil.TToC( Z33TimeStamp, 10, 8, 0, 0, "/", ":", " ")) ;
            ChangePostValue( "ZT_"+"Z21RecorridoDetallePaciente_"+sGXsfl_39_idx, Z21RecorridoDetallePaciente.ToString()) ;
            ChangePostValue( "ZT_"+"Z22IDCoach_"+sGXsfl_39_idx, Z22IDCoach.ToString()) ;
            ChangePostValue( "nRcdDeleted_5_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_5), 4, 0, ",", ""))) ;
            ChangePostValue( "nRcdExists_5_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_5), 4, 0, ",", ""))) ;
            ChangePostValue( "nIsMod_5_"+sGXsfl_39_idx, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_5), 4, 0, ",", ""))) ;
            if ( nIsMod_5 != 0 )
            {
               ChangePostValue( "RECORRIDODETALLEID_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetallePaciente_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "IDCOACH_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIDCoach_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPOSENAL_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoSenal.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "LONGITUD_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLongitud_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "LATITUD_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLatitud_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIPOOBSTACULO_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoObstaculo.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "INCIDENTE_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbIncidente.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "EMERGENCIA_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbEmergencia.Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "IMEI_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIMEI_Enabled), 5, 0, ".", ""))) ;
               ChangePostValue( "TIMESTAMP_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTimeStamp_Enabled), 5, 0, ".", ""))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll035( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_5 = 0;
         nIsMod_5 = 0;
         nRcdDeleted_5 = 0;
      }

      protected void ProcessLevel033( )
      {
         /* Save parent mode. */
         sMode3 = Gx_mode;
         ProcessNestedLevel035( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode3;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         /* ' Update level parameters */
      }

      protected void EndLevel033( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(4);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete033( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(2);
            pr_default.close(3);
            pr_gam.commit( "Recorrido");
            pr_default.commit( "Recorrido");
            if ( AnyError == 0 )
            {
               ConfirmValues030( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(5);
            pr_default.close(1);
            pr_default.close(0);
            pr_default.close(2);
            pr_default.close(3);
            pr_gam.rollback( "Recorrido");
            pr_default.rollback( "Recorrido");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart033( )
      {
         /* Using cursor T000315 */
         pr_default.execute(13);
         RcdFound3 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound3 = 1;
            A23RecorridoID = (Guid)((Guid)(T000315_A23RecorridoID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext033( )
      {
         /* Scan next routine */
         pr_default.readNext(13);
         RcdFound3 = 0;
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound3 = 1;
            A23RecorridoID = (Guid)((Guid)(T000315_A23RecorridoID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
         }
      }

      protected void ScanEnd033( )
      {
         pr_default.close(13);
      }

      protected void AfterConfirm033( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert033( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate033( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete033( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete033( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate033( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes033( )
      {
         edtRecorridoID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoID_Enabled), 5, 0)), true);
         edtRecorridoFecha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoFecha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoFecha_Enabled), 5, 0)), true);
      }

      protected void ZM035( short GX_JID )
      {
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z25TipoSenal = T00033_A25TipoSenal[0];
               Z28TipoObstaculo = T00033_A28TipoObstaculo[0];
               Z29Incidente = T00033_A29Incidente[0];
               Z30Emergencia = T00033_A30Emergencia[0];
               Z31IMEI = T00033_A31IMEI[0];
               Z33TimeStamp = T00033_A33TimeStamp[0];
               Z21RecorridoDetallePaciente = (Guid)(T00033_A21RecorridoDetallePaciente[0]);
               Z22IDCoach = (Guid)(T00033_A22IDCoach[0]);
            }
            else
            {
               Z25TipoSenal = A25TipoSenal;
               Z28TipoObstaculo = A28TipoObstaculo;
               Z29Incidente = A29Incidente;
               Z30Emergencia = A30Emergencia;
               Z31IMEI = A31IMEI;
               Z33TimeStamp = A33TimeStamp;
               Z21RecorridoDetallePaciente = (Guid)(A21RecorridoDetallePaciente);
               Z22IDCoach = (Guid)(A22IDCoach);
            }
         }
         if ( GX_JID == -13 )
         {
            Z23RecorridoID = (Guid)(A23RecorridoID);
            Z32RecorridoDetalleID = (Guid)(A32RecorridoDetalleID);
            Z25TipoSenal = A25TipoSenal;
            Z26Longitud = A26Longitud;
            Z27Latitud = A27Latitud;
            Z28TipoObstaculo = A28TipoObstaculo;
            Z29Incidente = A29Incidente;
            Z30Emergencia = A30Emergencia;
            Z31IMEI = A31IMEI;
            Z33TimeStamp = A33TimeStamp;
            Z21RecorridoDetallePaciente = (Guid)(A21RecorridoDetallePaciente);
            Z22IDCoach = (Guid)(A22IDCoach);
         }
      }

      protected void standaloneNotModal035( )
      {
      }

      protected void standaloneModal035( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A32RecorridoDetalleID) && ( Gx_BScreen == 0 ) )
         {
            A32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            edtRecorridoDetalleID_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoDetalleID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         }
         else
         {
            edtRecorridoDetalleID_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoDetalleID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load035( )
      {
         /* Using cursor T000316 */
         pr_default.execute(14, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound5 = 1;
            A25TipoSenal = T000316_A25TipoSenal[0];
            n25TipoSenal = T000316_n25TipoSenal[0];
            A26Longitud = T000316_A26Longitud[0];
            n26Longitud = T000316_n26Longitud[0];
            A27Latitud = T000316_A27Latitud[0];
            n27Latitud = T000316_n27Latitud[0];
            A28TipoObstaculo = T000316_A28TipoObstaculo[0];
            n28TipoObstaculo = T000316_n28TipoObstaculo[0];
            A29Incidente = T000316_A29Incidente[0];
            n29Incidente = T000316_n29Incidente[0];
            A30Emergencia = T000316_A30Emergencia[0];
            n30Emergencia = T000316_n30Emergencia[0];
            A31IMEI = T000316_A31IMEI[0];
            n31IMEI = T000316_n31IMEI[0];
            A33TimeStamp = T000316_A33TimeStamp[0];
            n33TimeStamp = T000316_n33TimeStamp[0];
            A21RecorridoDetallePaciente = (Guid)((Guid)(T000316_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = T000316_n21RecorridoDetallePaciente[0];
            A22IDCoach = (Guid)((Guid)(T000316_A22IDCoach[0]));
            n22IDCoach = T000316_n22IDCoach[0];
            ZM035( -13) ;
         }
         pr_default.close(14);
         OnLoadActions035( ) ;
      }

      protected void OnLoadActions035( )
      {
      }

      protected void CheckExtendedTable035( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal035( ) ;
         /* Using cursor T00034 */
         pr_default.execute(2, new Object[] {n21RecorridoDetallePaciente, A21RecorridoDetallePaciente});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (Guid.Empty==A21RecorridoDetallePaciente) ) )
            {
               GXCCtl = "RECORRIDODETALLEPACIENTE_" + sGXsfl_39_idx;
               GX_msglist.addItem("No existe 'Recorrido Paciente ST'.", "ForeignKeyNotFound", 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = edtRecorridoDetallePaciente_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
         /* Using cursor T00035 */
         pr_default.execute(3, new Object[] {n22IDCoach, A22IDCoach});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (Guid.Empty==A22IDCoach) ) )
            {
               GXCCtl = "IDCOACH_" + sGXsfl_39_idx;
               GX_msglist.addItem("No existe 'Recorrido Detalle Coach ST'.", "ForeignKeyNotFound", 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = edtIDCoach_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
         if ( ! ( ( A25TipoSenal == 1 ) || ( A25TipoSenal == 2 ) || ( A25TipoSenal == 3 ) || (0==A25TipoSenal) ) )
         {
            GXCCtl = "TIPOSENAL_" + sGXsfl_39_idx;
            GX_msglist.addItem("Campo Tipo Senal fuera de rango", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbTipoSenal_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A28TipoObstaculo == 0 ) || ( A28TipoObstaculo == 1 ) || ( A28TipoObstaculo == 2 ) || ( A28TipoObstaculo == 3 ) || (0==A28TipoObstaculo) ) )
         {
            GXCCtl = "TIPOOBSTACULO_" + sGXsfl_39_idx;
            GX_msglist.addItem("Campo Tipo Obstaculo fuera de rango", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbTipoObstaculo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A29Incidente == 0 ) || ( A29Incidente == 1 ) || (0==A29Incidente) ) )
         {
            GXCCtl = "INCIDENTE_" + sGXsfl_39_idx;
            GX_msglist.addItem("Campo Incidente fuera de rango", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbIncidente_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A30Emergencia == 0 ) || ( A30Emergencia == 1 ) || (0==A30Emergencia) ) )
         {
            GXCCtl = "EMERGENCIA_" + sGXsfl_39_idx;
            GX_msglist.addItem("Campo Emergencia fuera de rango", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = cmbEmergencia_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A33TimeStamp) || ( A33TimeStamp >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GXCCtl = "TIMESTAMP_" + sGXsfl_39_idx;
            GX_msglist.addItem("Campo Time Stamp fuera de rango", "OutOfRange", 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtTimeStamp_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors035( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable035( )
      {
      }

      protected void gxLoad_14( Guid A21RecorridoDetallePaciente )
      {
         /* Using cursor T000317 */
         pr_default.execute(15, new Object[] {n21RecorridoDetallePaciente, A21RecorridoDetallePaciente});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (Guid.Empty==A21RecorridoDetallePaciente) ) )
            {
               GXCCtl = "RECORRIDODETALLEPACIENTE_" + sGXsfl_39_idx;
               GX_msglist.addItem("No existe 'Recorrido Paciente ST'.", "ForeignKeyNotFound", 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = edtRecorridoDetallePaciente_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(15) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(15);
      }

      protected void gxLoad_15( Guid A22IDCoach )
      {
         /* Using cursor T000318 */
         pr_default.execute(16, new Object[] {n22IDCoach, A22IDCoach});
         if ( (pr_default.getStatus(16) == 101) )
         {
            if ( ! ( (Guid.Empty==A22IDCoach) ) )
            {
               GXCCtl = "IDCOACH_" + sGXsfl_39_idx;
               GX_msglist.addItem("No existe 'Recorrido Detalle Coach ST'.", "ForeignKeyNotFound", 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = edtIDCoach_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(16) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(16);
      }

      protected void GetKey035( )
      {
         /* Using cursor T000319 */
         pr_default.execute(17, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound5 = 1;
         }
         else
         {
            RcdFound5 = 0;
         }
         pr_default.close(17);
      }

      protected void getByPrimaryKey035( )
      {
         /* Using cursor T00033 */
         pr_default.execute(1, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM035( 13) ;
            RcdFound5 = 1;
            InitializeNonKey035( ) ;
            A32RecorridoDetalleID = (Guid)((Guid)(T00033_A32RecorridoDetalleID[0]));
            A25TipoSenal = T00033_A25TipoSenal[0];
            n25TipoSenal = T00033_n25TipoSenal[0];
            A26Longitud = T00033_A26Longitud[0];
            n26Longitud = T00033_n26Longitud[0];
            A27Latitud = T00033_A27Latitud[0];
            n27Latitud = T00033_n27Latitud[0];
            A28TipoObstaculo = T00033_A28TipoObstaculo[0];
            n28TipoObstaculo = T00033_n28TipoObstaculo[0];
            A29Incidente = T00033_A29Incidente[0];
            n29Incidente = T00033_n29Incidente[0];
            A30Emergencia = T00033_A30Emergencia[0];
            n30Emergencia = T00033_n30Emergencia[0];
            A31IMEI = T00033_A31IMEI[0];
            n31IMEI = T00033_n31IMEI[0];
            A33TimeStamp = T00033_A33TimeStamp[0];
            n33TimeStamp = T00033_n33TimeStamp[0];
            A21RecorridoDetallePaciente = (Guid)((Guid)(T00033_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = T00033_n21RecorridoDetallePaciente[0];
            A22IDCoach = (Guid)((Guid)(T00033_A22IDCoach[0]));
            n22IDCoach = T00033_n22IDCoach[0];
            Z23RecorridoID = (Guid)(A23RecorridoID);
            Z32RecorridoDetalleID = (Guid)(A32RecorridoDetalleID);
            sMode5 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal035( ) ;
            Load035( ) ;
            Gx_mode = sMode5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound5 = 0;
            InitializeNonKey035( ) ;
            sMode5 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal035( ) ;
            Gx_mode = sMode5;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes035( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency035( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00032 */
            pr_default.execute(0, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RecorridoDetalle"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z25TipoSenal != T00032_A25TipoSenal[0] ) || ( Z28TipoObstaculo != T00032_A28TipoObstaculo[0] ) || ( Z29Incidente != T00032_A29Incidente[0] ) || ( Z30Emergencia != T00032_A30Emergencia[0] ) || ( StringUtil.StrCmp(Z31IMEI, T00032_A31IMEI[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z33TimeStamp != T00032_A33TimeStamp[0] ) || ( Z21RecorridoDetallePaciente != T00032_A21RecorridoDetallePaciente[0] ) || ( Z22IDCoach != T00032_A22IDCoach[0] ) )
            {
               if ( Z25TipoSenal != T00032_A25TipoSenal[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"TipoSenal");
                  GXUtil.WriteLogRaw("Old: ",Z25TipoSenal);
                  GXUtil.WriteLogRaw("Current: ",T00032_A25TipoSenal[0]);
               }
               if ( Z28TipoObstaculo != T00032_A28TipoObstaculo[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"TipoObstaculo");
                  GXUtil.WriteLogRaw("Old: ",Z28TipoObstaculo);
                  GXUtil.WriteLogRaw("Current: ",T00032_A28TipoObstaculo[0]);
               }
               if ( Z29Incidente != T00032_A29Incidente[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"Incidente");
                  GXUtil.WriteLogRaw("Old: ",Z29Incidente);
                  GXUtil.WriteLogRaw("Current: ",T00032_A29Incidente[0]);
               }
               if ( Z30Emergencia != T00032_A30Emergencia[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"Emergencia");
                  GXUtil.WriteLogRaw("Old: ",Z30Emergencia);
                  GXUtil.WriteLogRaw("Current: ",T00032_A30Emergencia[0]);
               }
               if ( StringUtil.StrCmp(Z31IMEI, T00032_A31IMEI[0]) != 0 )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"IMEI");
                  GXUtil.WriteLogRaw("Old: ",Z31IMEI);
                  GXUtil.WriteLogRaw("Current: ",T00032_A31IMEI[0]);
               }
               if ( Z33TimeStamp != T00032_A33TimeStamp[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"TimeStamp");
                  GXUtil.WriteLogRaw("Old: ",Z33TimeStamp);
                  GXUtil.WriteLogRaw("Current: ",T00032_A33TimeStamp[0]);
               }
               if ( Z21RecorridoDetallePaciente != T00032_A21RecorridoDetallePaciente[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"RecorridoDetallePaciente");
                  GXUtil.WriteLogRaw("Old: ",Z21RecorridoDetallePaciente);
                  GXUtil.WriteLogRaw("Current: ",T00032_A21RecorridoDetallePaciente[0]);
               }
               if ( Z22IDCoach != T00032_A22IDCoach[0] )
               {
                  GXUtil.WriteLog("recorrido:[seudo value changed for attri]"+"IDCoach");
                  GXUtil.WriteLogRaw("Old: ",Z22IDCoach);
                  GXUtil.WriteLogRaw("Current: ",T00032_A22IDCoach[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"RecorridoDetalle"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert035( )
      {
         if ( ! IsAuthorized("recorrido_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate035( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable035( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM035( 0) ;
            CheckOptimisticConcurrency035( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm035( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert035( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000320 */
                     pr_default.execute(18, new Object[] {A23RecorridoID, n25TipoSenal, A25TipoSenal, n26Longitud, A26Longitud, n27Latitud, A27Latitud, n28TipoObstaculo, A28TipoObstaculo, n29Incidente, A29Incidente, n30Emergencia, A30Emergencia, n31IMEI, A31IMEI, n33TimeStamp, A33TimeStamp, n21RecorridoDetallePaciente, A21RecorridoDetallePaciente, n22IDCoach, A22IDCoach, A32RecorridoDetalleID});
                     pr_default.close(18);
                     dsDefault.SmartCacheProvider.SetUpdated("RecorridoDetalle") ;
                     if ( (pr_default.getStatus(18) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load035( ) ;
            }
            EndLevel035( ) ;
         }
         CloseExtendedTableCursors035( ) ;
      }

      protected void Update035( )
      {
         if ( ! IsAuthorized("recorrido_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate035( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable035( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency035( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm035( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate035( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000321 */
                     pr_default.execute(19, new Object[] {n25TipoSenal, A25TipoSenal, n26Longitud, A26Longitud, n27Latitud, A27Latitud, n28TipoObstaculo, A28TipoObstaculo, n29Incidente, A29Incidente, n30Emergencia, A30Emergencia, n31IMEI, A31IMEI, n33TimeStamp, A33TimeStamp, n21RecorridoDetallePaciente, A21RecorridoDetallePaciente, n22IDCoach, A22IDCoach, A23RecorridoID, A32RecorridoDetalleID});
                     pr_default.close(19);
                     dsDefault.SmartCacheProvider.SetUpdated("RecorridoDetalle") ;
                     if ( (pr_default.getStatus(19) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RecorridoDetalle"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate035( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey035( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel035( ) ;
         }
         CloseExtendedTableCursors035( ) ;
      }

      protected void DeferredUpdate035( )
      {
      }

      protected void Delete035( )
      {
         if ( ! IsAuthorized("recorrido_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate035( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency035( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls035( ) ;
            AfterConfirm035( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete035( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000322 */
                  pr_default.execute(20, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
                  pr_default.close(20);
                  dsDefault.SmartCacheProvider.SetUpdated("RecorridoDetalle") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode5 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel035( ) ;
         Gx_mode = sMode5;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls035( )
      {
         standaloneModal035( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel035( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart035( )
      {
         /* Scan By routine */
         /* Using cursor T000323 */
         pr_default.execute(21, new Object[] {A23RecorridoID});
         RcdFound5 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound5 = 1;
            A32RecorridoDetalleID = (Guid)((Guid)(T000323_A32RecorridoDetalleID[0]));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext035( )
      {
         /* Scan next routine */
         pr_default.readNext(21);
         RcdFound5 = 0;
         if ( (pr_default.getStatus(21) != 101) )
         {
            RcdFound5 = 1;
            A32RecorridoDetalleID = (Guid)((Guid)(T000323_A32RecorridoDetalleID[0]));
         }
      }

      protected void ScanEnd035( )
      {
         pr_default.close(21);
      }

      protected void AfterConfirm035( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert035( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate035( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete035( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete035( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate035( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes035( )
      {
         edtRecorridoDetalleID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoDetalleID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         edtRecorridoDetallePaciente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoDetallePaciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoDetallePaciente_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         edtIDCoach_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIDCoach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIDCoach_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         cmbTipoSenal.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoSenal_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTipoSenal.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         edtLongitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLongitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLongitud_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         edtLatitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtLatitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtLatitud_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         cmbTipoObstaculo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoObstaculo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbTipoObstaculo.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         cmbIncidente.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIncidente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbIncidente.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         cmbEmergencia.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmergencia_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbEmergencia.Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         edtIMEI_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtIMEI_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtIMEI_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
         edtTimeStamp_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtTimeStamp_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtTimeStamp_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
      }

      protected void send_integrity_lvl_hashes035( )
      {
      }

      protected void send_integrity_lvl_hashes033( )
      {
      }

      protected void SubsflControlProps_395( )
      {
         edtRecorridoDetalleID_Internalname = "RECORRIDODETALLEID_"+sGXsfl_39_idx;
         edtRecorridoDetallePaciente_Internalname = "RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx;
         imgprompt_21_Internalname = "PROMPT_21_"+sGXsfl_39_idx;
         edtIDCoach_Internalname = "IDCOACH_"+sGXsfl_39_idx;
         imgprompt_22_Internalname = "PROMPT_22_"+sGXsfl_39_idx;
         cmbTipoSenal_Internalname = "TIPOSENAL_"+sGXsfl_39_idx;
         edtLongitud_Internalname = "LONGITUD_"+sGXsfl_39_idx;
         edtLatitud_Internalname = "LATITUD_"+sGXsfl_39_idx;
         cmbTipoObstaculo_Internalname = "TIPOOBSTACULO_"+sGXsfl_39_idx;
         cmbIncidente_Internalname = "INCIDENTE_"+sGXsfl_39_idx;
         cmbEmergencia_Internalname = "EMERGENCIA_"+sGXsfl_39_idx;
         edtIMEI_Internalname = "IMEI_"+sGXsfl_39_idx;
         edtTimeStamp_Internalname = "TIMESTAMP_"+sGXsfl_39_idx;
      }

      protected void SubsflControlProps_fel_395( )
      {
         edtRecorridoDetalleID_Internalname = "RECORRIDODETALLEID_"+sGXsfl_39_fel_idx;
         edtRecorridoDetallePaciente_Internalname = "RECORRIDODETALLEPACIENTE_"+sGXsfl_39_fel_idx;
         imgprompt_21_Internalname = "PROMPT_21_"+sGXsfl_39_fel_idx;
         edtIDCoach_Internalname = "IDCOACH_"+sGXsfl_39_fel_idx;
         imgprompt_22_Internalname = "PROMPT_22_"+sGXsfl_39_fel_idx;
         cmbTipoSenal_Internalname = "TIPOSENAL_"+sGXsfl_39_fel_idx;
         edtLongitud_Internalname = "LONGITUD_"+sGXsfl_39_fel_idx;
         edtLatitud_Internalname = "LATITUD_"+sGXsfl_39_fel_idx;
         cmbTipoObstaculo_Internalname = "TIPOOBSTACULO_"+sGXsfl_39_fel_idx;
         cmbIncidente_Internalname = "INCIDENTE_"+sGXsfl_39_fel_idx;
         cmbEmergencia_Internalname = "EMERGENCIA_"+sGXsfl_39_fel_idx;
         edtIMEI_Internalname = "IMEI_"+sGXsfl_39_fel_idx;
         edtTimeStamp_Internalname = "TIMESTAMP_"+sGXsfl_39_fel_idx;
      }

      protected void AddRow035( )
      {
         nGXsfl_39_idx = (short)(nGXsfl_39_idx+1);
         sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
         SubsflControlProps_395( ) ;
         SendRow035( ) ;
      }

      protected void SendRow035( )
      {
         Gridrecorrido_detalleRow = GXWebRow.GetNew(context);
         if ( subGridrecorrido_detalle_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGridrecorrido_detalle_Backstyle = 0;
            if ( StringUtil.StrCmp(subGridrecorrido_detalle_Class, "") != 0 )
            {
               subGridrecorrido_detalle_Linesclass = subGridrecorrido_detalle_Class+"Odd";
            }
         }
         else if ( subGridrecorrido_detalle_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGridrecorrido_detalle_Backstyle = 0;
            subGridrecorrido_detalle_Backcolor = subGridrecorrido_detalle_Allbackcolor;
            if ( StringUtil.StrCmp(subGridrecorrido_detalle_Class, "") != 0 )
            {
               subGridrecorrido_detalle_Linesclass = subGridrecorrido_detalle_Class+"Uniform";
            }
         }
         else if ( subGridrecorrido_detalle_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGridrecorrido_detalle_Backstyle = 1;
            if ( StringUtil.StrCmp(subGridrecorrido_detalle_Class, "") != 0 )
            {
               subGridrecorrido_detalle_Linesclass = subGridrecorrido_detalle_Class+"Odd";
            }
            subGridrecorrido_detalle_Backcolor = (int)(0x0);
         }
         else if ( subGridrecorrido_detalle_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGridrecorrido_detalle_Backstyle = 1;
            if ( ((int)((nGXsfl_39_idx) % (2))) == 0 )
            {
               subGridrecorrido_detalle_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridrecorrido_detalle_Class, "") != 0 )
               {
                  subGridrecorrido_detalle_Linesclass = subGridrecorrido_detalle_Class+"Even";
               }
            }
            else
            {
               subGridrecorrido_detalle_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGridrecorrido_detalle_Class, "") != 0 )
               {
                  subGridrecorrido_detalle_Linesclass = subGridrecorrido_detalle_Class+"Odd";
               }
            }
         }
         imgprompt_22_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"IDCOACH_"+sGXsfl_39_idx+"'), id:'"+"IDCOACH_"+sGXsfl_39_idx+"'"+",IOType:'out'}"+"],"+"gx.dom.form()."+"nIsMod_5_"+sGXsfl_39_idx+","+"'', false"+","+"false"+");");
         imgprompt_21_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx+"'), id:'"+"RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx+"'"+",IOType:'out'}"+"],"+"gx.dom.form()."+"nIsMod_5_"+sGXsfl_39_idx+","+"'', false"+","+"false"+");");
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 40,'',false,'" + sGXsfl_39_idx + "',39)\"";
         ROClassString = "Attribute";
         Gridrecorrido_detalleRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRecorridoDetalleID_Internalname,A32RecorridoDetalleID.ToString(),A32RecorridoDetalleID.ToString(),TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,40);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRecorridoDetalleID_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtRecorridoDetalleID_Enabled,(short)1,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)39,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 41,'',false,'" + sGXsfl_39_idx + "',39)\"";
         ROClassString = "Attribute";
         Gridrecorrido_detalleRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtRecorridoDetallePaciente_Internalname,A21RecorridoDetallePaciente.ToString(),A21RecorridoDetallePaciente.ToString(),TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtRecorridoDetallePaciente_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtRecorridoDetallePaciente_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)39,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )));
         Gridrecorrido_detalleRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_21_Internalname,(String)sImgUrl,(String)imgprompt_21_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_21_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 42,'',false,'" + sGXsfl_39_idx + "',39)\"";
         ROClassString = "Attribute";
         Gridrecorrido_detalleRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIDCoach_Internalname,A22IDCoach.ToString(),A22IDCoach.ToString(),TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIDCoach_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtIDCoach_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)39,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         /* Static images/pictures */
         ClassString = "gx-prompt Image";
         StyleString = "";
         sImgUrl = (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )));
         Gridrecorrido_detalleRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)imgprompt_22_Internalname,(String)sImgUrl,(String)imgprompt_22_Link,(String)"",(String)"",context.GetTheme( ),(int)imgprompt_22_Visible,(short)1,(String)"",(String)"",(short)0,(short)0,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         /* Subfile cell */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 43,'',false,'" + sGXsfl_39_idx + "',39)\"";
         if ( ( cmbTipoSenal.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "TIPOSENAL_" + sGXsfl_39_idx;
            cmbTipoSenal.Name = GXCCtl;
            cmbTipoSenal.WebTags = "";
            cmbTipoSenal.addItem("1", "Inicio", 0);
            cmbTipoSenal.addItem("2", "Progreso", 0);
            cmbTipoSenal.addItem("3", "Fin", 0);
            if ( cmbTipoSenal.ItemCount > 0 )
            {
               A25TipoSenal = (short)(NumberUtil.Val( cmbTipoSenal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0))), "."));
               n25TipoSenal = false;
            }
         }
         /* ComboBox */
         Gridrecorrido_detalleRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTipoSenal,(String)cmbTipoSenal_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0)),(short)1,(String)cmbTipoSenal_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbTipoSenal.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"",(String)"",(bool)true});
         cmbTipoSenal.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoSenal_Internalname, "Values", (String)(cmbTipoSenal.ToJavascriptSource()), !bGXsfl_39_Refreshing);
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 44,'',false,'" + sGXsfl_39_idx + "',39)\"";
         ROClassString = "Attribute";
         Gridrecorrido_detalleRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLongitud_Internalname,(String)A26Longitud,(String)A26Longitud,TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,44);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLongitud_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtLongitud_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)39,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_39_idx + "',39)\"";
         ROClassString = "Attribute";
         Gridrecorrido_detalleRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtLatitud_Internalname,(String)A27Latitud,(String)A27Latitud,TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtLatitud_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtLatitud_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(int)2097152,(short)0,(short)0,(short)39,(short)1,(short)0,(short)-1,(bool)true,(String)"",(String)"left",(bool)false});
         /* Subfile cell */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 46,'',false,'" + sGXsfl_39_idx + "',39)\"";
         if ( ( cmbTipoObstaculo.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "TIPOOBSTACULO_" + sGXsfl_39_idx;
            cmbTipoObstaculo.Name = GXCCtl;
            cmbTipoObstaculo.WebTags = "";
            cmbTipoObstaculo.addItem("0", "Ninguno", 0);
            cmbTipoObstaculo.addItem("1", "Agujero", 0);
            cmbTipoObstaculo.addItem("2", "Gradas", 0);
            cmbTipoObstaculo.addItem("3", "Otros", 0);
            if ( cmbTipoObstaculo.ItemCount > 0 )
            {
               A28TipoObstaculo = (short)(NumberUtil.Val( cmbTipoObstaculo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A28TipoObstaculo), 4, 0))), "."));
               n28TipoObstaculo = false;
            }
         }
         /* ComboBox */
         Gridrecorrido_detalleRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbTipoObstaculo,(String)cmbTipoObstaculo_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A28TipoObstaculo), 4, 0)),(short)1,(String)cmbTipoObstaculo_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbTipoObstaculo.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"",(String)"",(bool)true});
         cmbTipoObstaculo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A28TipoObstaculo), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbTipoObstaculo_Internalname, "Values", (String)(cmbTipoObstaculo.ToJavascriptSource()), !bGXsfl_39_Refreshing);
         /* Subfile cell */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 47,'',false,'" + sGXsfl_39_idx + "',39)\"";
         if ( ( cmbIncidente.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "INCIDENTE_" + sGXsfl_39_idx;
            cmbIncidente.Name = GXCCtl;
            cmbIncidente.WebTags = "";
            cmbIncidente.addItem("0", "Nada", 0);
            cmbIncidente.addItem("1", "Registrar Obstaculo", 0);
            if ( cmbIncidente.ItemCount > 0 )
            {
               A29Incidente = (short)(NumberUtil.Val( cmbIncidente.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A29Incidente), 4, 0))), "."));
               n29Incidente = false;
            }
         }
         /* ComboBox */
         Gridrecorrido_detalleRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbIncidente,(String)cmbIncidente_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A29Incidente), 4, 0)),(short)1,(String)cmbIncidente_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbIncidente.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"",(String)"",(bool)true});
         cmbIncidente.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A29Incidente), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbIncidente_Internalname, "Values", (String)(cmbIncidente.ToJavascriptSource()), !bGXsfl_39_Refreshing);
         /* Subfile cell */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 48,'',false,'" + sGXsfl_39_idx + "',39)\"";
         if ( ( cmbEmergencia.ItemCount == 0 ) && isAjaxCallMode( ) )
         {
            GXCCtl = "EMERGENCIA_" + sGXsfl_39_idx;
            cmbEmergencia.Name = GXCCtl;
            cmbEmergencia.WebTags = "";
            cmbEmergencia.addItem("0", "Nada", 0);
            cmbEmergencia.addItem("1", "Emergencia", 0);
            if ( cmbEmergencia.ItemCount > 0 )
            {
               A30Emergencia = (short)(NumberUtil.Val( cmbEmergencia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A30Emergencia), 4, 0))), "."));
               n30Emergencia = false;
            }
         }
         /* ComboBox */
         Gridrecorrido_detalleRow.AddColumnProperties("combobox", 2, isAjaxCallMode( ), new Object[] {(GXCombobox)cmbEmergencia,(String)cmbEmergencia_Internalname,StringUtil.Trim( StringUtil.Str( (decimal)(A30Emergencia), 4, 0)),(short)1,(String)cmbEmergencia_Jsonclick,(short)0,(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"int",(String)"",(short)-1,cmbEmergencia.Enabled,(short)0,(short)0,(short)0,(String)"px",(short)0,(String)"px",(String)"",(String)"Attribute",(String)"",(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"",(String)"",(bool)true});
         cmbEmergencia.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A30Emergencia), 4, 0));
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmergencia_Internalname, "Values", (String)(cmbEmergencia.ToJavascriptSource()), !bGXsfl_39_Refreshing);
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 49,'',false,'" + sGXsfl_39_idx + "',39)\"";
         ROClassString = "Attribute";
         Gridrecorrido_detalleRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtIMEI_Internalname,(String)A31IMEI,(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,49);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtIMEI_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtIMEI_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         /* Single line edit */
         TempTags = " data-gxoch1=\"gx.fn.setControlValue('nIsMod_5_" + sGXsfl_39_idx + "',1);\"  onfocus=\"gx.evt.onfocus(this, 50,'',false,'" + sGXsfl_39_idx + "',39)\"";
         ROClassString = "Attribute";
         Gridrecorrido_detalleRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtTimeStamp_Internalname,context.localUtil.TToC( A33TimeStamp, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( A33TimeStamp, "99/99/99 99:99"),TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'spa',false,0);"+";gx.evt.onblur(this,50);\"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtTimeStamp_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtTimeStamp_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)39,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         context.httpAjaxContext.ajax_sending_grid_row(Gridrecorrido_detalleRow);
         send_integrity_lvl_hashes035( ) ;
         GXCCtl = "Z32RecorridoDetalleID_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, Z32RecorridoDetalleID.ToString());
         GXCCtl = "Z25TipoSenal_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z25TipoSenal), 4, 0, ",", "")));
         GXCCtl = "Z28TipoObstaculo_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z28TipoObstaculo), 4, 0, ",", "")));
         GXCCtl = "Z29Incidente_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z29Incidente), 4, 0, ",", "")));
         GXCCtl = "Z30Emergencia_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(Z30Emergencia), 4, 0, ",", "")));
         GXCCtl = "Z31IMEI_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, Z31IMEI);
         GXCCtl = "Z33TimeStamp_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, context.localUtil.TToC( Z33TimeStamp, 10, 8, 0, 0, "/", ":", " "));
         GXCCtl = "Z21RecorridoDetallePaciente_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, Z21RecorridoDetallePaciente.ToString());
         GXCCtl = "Z22IDCoach_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, Z22IDCoach.ToString());
         GXCCtl = "nRcdDeleted_5_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdDeleted_5), 4, 0, ",", "")));
         GXCCtl = "nRcdExists_5_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nRcdExists_5), 4, 0, ",", "")));
         GXCCtl = "nIsMod_5_" + sGXsfl_39_idx;
         GxWebStd.gx_hidden_field( context, GXCCtl, StringUtil.LTrim( StringUtil.NToC( (decimal)(nIsMod_5), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "RECORRIDODETALLEID_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtRecorridoDetallePaciente_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IDCOACH_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIDCoach_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TIPOSENAL_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoSenal.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "LONGITUD_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLongitud_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "LATITUD_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtLatitud_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TIPOOBSTACULO_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbTipoObstaculo.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "INCIDENTE_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbIncidente.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "EMERGENCIA_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(cmbEmergencia.Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "IMEI_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtIMEI_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TIMESTAMP_"+sGXsfl_39_idx+"Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtTimeStamp_Enabled), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "PROMPT_21_"+sGXsfl_39_idx+"Link", StringUtil.RTrim( imgprompt_21_Link));
         GxWebStd.gx_hidden_field( context, "PROMPT_22_"+sGXsfl_39_idx+"Link", StringUtil.RTrim( imgprompt_22_Link));
         context.httpAjaxContext.ajax_sending_grid_row(null);
         Gridrecorrido_detalleContainer.AddRow(Gridrecorrido_detalleRow);
      }

      protected void ReadRow035( )
      {
         nGXsfl_39_idx = (short)(nGXsfl_39_idx+1);
         sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
         SubsflControlProps_395( ) ;
         edtRecorridoDetalleID_Enabled = (int)(context.localUtil.CToN( cgiGet( "RECORRIDODETALLEID_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         edtRecorridoDetallePaciente_Enabled = (int)(context.localUtil.CToN( cgiGet( "RECORRIDODETALLEPACIENTE_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         edtIDCoach_Enabled = (int)(context.localUtil.CToN( cgiGet( "IDCOACH_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         cmbTipoSenal.Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPOSENAL_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         edtLongitud_Enabled = (int)(context.localUtil.CToN( cgiGet( "LONGITUD_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         edtLatitud_Enabled = (int)(context.localUtil.CToN( cgiGet( "LATITUD_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         cmbTipoObstaculo.Enabled = (int)(context.localUtil.CToN( cgiGet( "TIPOOBSTACULO_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         cmbIncidente.Enabled = (int)(context.localUtil.CToN( cgiGet( "INCIDENTE_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         cmbEmergencia.Enabled = (int)(context.localUtil.CToN( cgiGet( "EMERGENCIA_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         edtIMEI_Enabled = (int)(context.localUtil.CToN( cgiGet( "IMEI_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         edtTimeStamp_Enabled = (int)(context.localUtil.CToN( cgiGet( "TIMESTAMP_"+sGXsfl_39_idx+"Enabled"), ",", "."));
         imgprompt_21_Link = cgiGet( "PROMPT_21_"+sGXsfl_39_idx+"Link");
         imgprompt_21_Link = cgiGet( "PROMPT_22_"+sGXsfl_39_idx+"Link");
         if ( StringUtil.StrCmp(cgiGet( edtRecorridoDetalleID_Internalname), "") == 0 )
         {
            A32RecorridoDetalleID = (Guid)(Guid.Empty);
         }
         else
         {
            try
            {
               A32RecorridoDetalleID = (Guid)(StringUtil.StrToGuid( cgiGet( edtRecorridoDetalleID_Internalname)));
            }
            catch ( Exception e )
            {
               GXCCtl = "RECORRIDODETALLEID_" + sGXsfl_39_idx;
               GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = edtRecorridoDetalleID_Internalname;
               wbErr = true;
            }
         }
         if ( StringUtil.StrCmp(cgiGet( edtRecorridoDetallePaciente_Internalname), "") == 0 )
         {
            A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
            n21RecorridoDetallePaciente = false;
         }
         else
         {
            try
            {
               A21RecorridoDetallePaciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtRecorridoDetallePaciente_Internalname)));
               n21RecorridoDetallePaciente = false;
            }
            catch ( Exception e )
            {
               GXCCtl = "RECORRIDODETALLEPACIENTE_" + sGXsfl_39_idx;
               GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = edtRecorridoDetallePaciente_Internalname;
               wbErr = true;
            }
         }
         n21RecorridoDetallePaciente = ((Guid.Empty==A21RecorridoDetallePaciente) ? true : false);
         if ( StringUtil.StrCmp(cgiGet( edtIDCoach_Internalname), "") == 0 )
         {
            A22IDCoach = (Guid)(Guid.Empty);
            n22IDCoach = false;
         }
         else
         {
            try
            {
               A22IDCoach = (Guid)(StringUtil.StrToGuid( cgiGet( edtIDCoach_Internalname)));
               n22IDCoach = false;
            }
            catch ( Exception e )
            {
               GXCCtl = "IDCOACH_" + sGXsfl_39_idx;
               GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, GXCCtl);
               AnyError = 1;
               GX_FocusControl = edtIDCoach_Internalname;
               wbErr = true;
            }
         }
         n22IDCoach = ((Guid.Empty==A22IDCoach) ? true : false);
         cmbTipoSenal.Name = cmbTipoSenal_Internalname;
         cmbTipoSenal.CurrentValue = cgiGet( cmbTipoSenal_Internalname);
         A25TipoSenal = (short)(NumberUtil.Val( cgiGet( cmbTipoSenal_Internalname), "."));
         n25TipoSenal = false;
         n25TipoSenal = ((0==A25TipoSenal) ? true : false);
         A26Longitud = cgiGet( edtLongitud_Internalname);
         n26Longitud = false;
         n26Longitud = (String.IsNullOrEmpty(StringUtil.RTrim( A26Longitud)) ? true : false);
         A27Latitud = cgiGet( edtLatitud_Internalname);
         n27Latitud = false;
         n27Latitud = (String.IsNullOrEmpty(StringUtil.RTrim( A27Latitud)) ? true : false);
         cmbTipoObstaculo.Name = cmbTipoObstaculo_Internalname;
         cmbTipoObstaculo.CurrentValue = cgiGet( cmbTipoObstaculo_Internalname);
         A28TipoObstaculo = (short)(NumberUtil.Val( cgiGet( cmbTipoObstaculo_Internalname), "."));
         n28TipoObstaculo = false;
         n28TipoObstaculo = ((0==A28TipoObstaculo) ? true : false);
         cmbIncidente.Name = cmbIncidente_Internalname;
         cmbIncidente.CurrentValue = cgiGet( cmbIncidente_Internalname);
         A29Incidente = (short)(NumberUtil.Val( cgiGet( cmbIncidente_Internalname), "."));
         n29Incidente = false;
         n29Incidente = ((0==A29Incidente) ? true : false);
         cmbEmergencia.Name = cmbEmergencia_Internalname;
         cmbEmergencia.CurrentValue = cgiGet( cmbEmergencia_Internalname);
         A30Emergencia = (short)(NumberUtil.Val( cgiGet( cmbEmergencia_Internalname), "."));
         n30Emergencia = false;
         n30Emergencia = ((0==A30Emergencia) ? true : false);
         A31IMEI = cgiGet( edtIMEI_Internalname);
         n31IMEI = false;
         n31IMEI = (String.IsNullOrEmpty(StringUtil.RTrim( A31IMEI)) ? true : false);
         if ( context.localUtil.VCDateTime( cgiGet( edtTimeStamp_Internalname), 2, 0) == 0 )
         {
            GXCCtl = "TIMESTAMP_" + sGXsfl_39_idx;
            GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Time Stamp"}), 1, GXCCtl);
            AnyError = 1;
            GX_FocusControl = edtTimeStamp_Internalname;
            wbErr = true;
            A33TimeStamp = (DateTime)(DateTime.MinValue);
            n33TimeStamp = false;
         }
         else
         {
            A33TimeStamp = context.localUtil.CToT( cgiGet( edtTimeStamp_Internalname));
            n33TimeStamp = false;
         }
         n33TimeStamp = ((DateTime.MinValue==A33TimeStamp) ? true : false);
         GXCCtl = "Z32RecorridoDetalleID_" + sGXsfl_39_idx;
         Z32RecorridoDetalleID = (Guid)(StringUtil.StrToGuid( cgiGet( GXCCtl)));
         GXCCtl = "Z25TipoSenal_" + sGXsfl_39_idx;
         Z25TipoSenal = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         n25TipoSenal = ((0==A25TipoSenal) ? true : false);
         GXCCtl = "Z28TipoObstaculo_" + sGXsfl_39_idx;
         Z28TipoObstaculo = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         n28TipoObstaculo = ((0==A28TipoObstaculo) ? true : false);
         GXCCtl = "Z29Incidente_" + sGXsfl_39_idx;
         Z29Incidente = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         n29Incidente = ((0==A29Incidente) ? true : false);
         GXCCtl = "Z30Emergencia_" + sGXsfl_39_idx;
         Z30Emergencia = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         n30Emergencia = ((0==A30Emergencia) ? true : false);
         GXCCtl = "Z31IMEI_" + sGXsfl_39_idx;
         Z31IMEI = cgiGet( GXCCtl);
         n31IMEI = (String.IsNullOrEmpty(StringUtil.RTrim( A31IMEI)) ? true : false);
         GXCCtl = "Z33TimeStamp_" + sGXsfl_39_idx;
         Z33TimeStamp = context.localUtil.CToT( cgiGet( GXCCtl), 0);
         n33TimeStamp = ((DateTime.MinValue==A33TimeStamp) ? true : false);
         GXCCtl = "Z21RecorridoDetallePaciente_" + sGXsfl_39_idx;
         Z21RecorridoDetallePaciente = (Guid)(StringUtil.StrToGuid( cgiGet( GXCCtl)));
         n21RecorridoDetallePaciente = ((Guid.Empty==A21RecorridoDetallePaciente) ? true : false);
         GXCCtl = "Z22IDCoach_" + sGXsfl_39_idx;
         Z22IDCoach = (Guid)(StringUtil.StrToGuid( cgiGet( GXCCtl)));
         n22IDCoach = ((Guid.Empty==A22IDCoach) ? true : false);
         GXCCtl = "nRcdDeleted_5_" + sGXsfl_39_idx;
         nRcdDeleted_5 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nRcdExists_5_" + sGXsfl_39_idx;
         nRcdExists_5 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
         GXCCtl = "nIsMod_5_" + sGXsfl_39_idx;
         nIsMod_5 = (short)(context.localUtil.CToN( cgiGet( GXCCtl), ",", "."));
      }

      protected void assign_properties_default( )
      {
         defedtRecorridoDetalleID_Enabled = edtRecorridoDetalleID_Enabled;
      }

      protected void ConfirmValues030( )
      {
         nGXsfl_39_idx = 0;
         sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
         SubsflControlProps_395( ) ;
         while ( nGXsfl_39_idx < nRC_GXsfl_39 )
         {
            nGXsfl_39_idx = (short)(nGXsfl_39_idx+1);
            sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
            SubsflControlProps_395( ) ;
            ChangePostValue( "Z32RecorridoDetalleID_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z32RecorridoDetalleID_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z32RecorridoDetalleID_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z25TipoSenal_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z25TipoSenal_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z25TipoSenal_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z28TipoObstaculo_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z28TipoObstaculo_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z28TipoObstaculo_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z29Incidente_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z29Incidente_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z29Incidente_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z30Emergencia_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z30Emergencia_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z30Emergencia_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z31IMEI_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z31IMEI_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z31IMEI_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z33TimeStamp_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z33TimeStamp_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z33TimeStamp_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z21RecorridoDetallePaciente_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z21RecorridoDetallePaciente_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z21RecorridoDetallePaciente_"+sGXsfl_39_idx) ;
            ChangePostValue( "Z22IDCoach_"+sGXsfl_39_idx, cgiGet( "ZT_"+"Z22IDCoach_"+sGXsfl_39_idx)) ;
            DeletePostValue( "ZT_"+"Z22IDCoach_"+sGXsfl_39_idx) ;
         }
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111816385836", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("recorrido.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z23RecorridoID", Z23RecorridoID.ToString());
         GxWebStd.gx_hidden_field( context, "Z24RecorridoFecha", context.localUtil.TToC( Z24RecorridoFecha, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_39", StringUtil.LTrim( StringUtil.NToC( (decimal)(nGXsfl_39_idx), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("recorrido.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Recorrido" ;
      }

      public override String GetPgmdesc( )
      {
         return "Recorrido" ;
      }

      protected void InitializeNonKey033( )
      {
         A24RecorridoFecha = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A24RecorridoFecha", context.localUtil.TToC( A24RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
         Z24RecorridoFecha = (DateTime)(DateTime.MinValue);
      }

      protected void InitAll033( )
      {
         A23RecorridoID = (Guid)(Guid.Empty);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A23RecorridoID", A23RecorridoID.ToString());
         InitializeNonKey033( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey035( )
      {
         A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         n21RecorridoDetallePaciente = false;
         A22IDCoach = (Guid)(Guid.Empty);
         n22IDCoach = false;
         A25TipoSenal = 0;
         n25TipoSenal = false;
         A26Longitud = "";
         n26Longitud = false;
         A27Latitud = "";
         n27Latitud = false;
         A28TipoObstaculo = 0;
         n28TipoObstaculo = false;
         A29Incidente = 0;
         n29Incidente = false;
         A30Emergencia = 0;
         n30Emergencia = false;
         A31IMEI = "";
         n31IMEI = false;
         A33TimeStamp = (DateTime)(DateTime.MinValue);
         n33TimeStamp = false;
         Z25TipoSenal = 0;
         Z28TipoObstaculo = 0;
         Z29Incidente = 0;
         Z30Emergencia = 0;
         Z31IMEI = "";
         Z33TimeStamp = (DateTime)(DateTime.MinValue);
         Z21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         Z22IDCoach = (Guid)(Guid.Empty);
      }

      protected void InitAll035( )
      {
         A32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
         InitializeNonKey035( ) ;
      }

      protected void StandaloneModalInsert035( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2018111816385841", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("recorrido.js", "?2018111816385841", false);
         /* End function include_jscripts */
      }

      protected void init_level_properties5( )
      {
         edtRecorridoDetalleID_Enabled = defedtRecorridoDetalleID_Enabled;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtRecorridoDetalleID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtRecorridoDetalleID_Enabled), 5, 0)), !bGXsfl_39_Refreshing);
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         edtRecorridoID_Internalname = "RECORRIDOID";
         edtRecorridoFecha_Internalname = "RECORRIDOFECHA";
         lblTitledetalle_Internalname = "TITLEDETALLE";
         edtRecorridoDetalleID_Internalname = "RECORRIDODETALLEID";
         edtRecorridoDetallePaciente_Internalname = "RECORRIDODETALLEPACIENTE";
         edtIDCoach_Internalname = "IDCOACH";
         cmbTipoSenal_Internalname = "TIPOSENAL";
         edtLongitud_Internalname = "LONGITUD";
         edtLatitud_Internalname = "LATITUD";
         cmbTipoObstaculo_Internalname = "TIPOOBSTACULO";
         cmbIncidente_Internalname = "INCIDENTE";
         cmbEmergencia_Internalname = "EMERGENCIA";
         edtIMEI_Internalname = "IMEI";
         edtTimeStamp_Internalname = "TIMESTAMP";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_21_Internalname = "PROMPT_21";
         imgprompt_22_Internalname = "PROMPT_22";
         subGridrecorrido_detalle_Internalname = "GRIDRECORRIDO_DETALLE";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Recorrido";
         edtTimeStamp_Jsonclick = "";
         edtIMEI_Jsonclick = "";
         cmbEmergencia_Jsonclick = "";
         cmbIncidente_Jsonclick = "";
         cmbTipoObstaculo_Jsonclick = "";
         edtLatitud_Jsonclick = "";
         edtLongitud_Jsonclick = "";
         cmbTipoSenal_Jsonclick = "";
         imgprompt_22_Visible = 1;
         imgprompt_22_Link = "";
         edtIDCoach_Jsonclick = "";
         imgprompt_21_Visible = 1;
         imgprompt_21_Link = "";
         imgprompt_21_Visible = 1;
         edtRecorridoDetallePaciente_Jsonclick = "";
         edtRecorridoDetalleID_Jsonclick = "";
         subGridrecorrido_detalle_Class = "Grid";
         subGridrecorrido_detalle_Backcolorstyle = 0;
         subGridrecorrido_detalle_Allowcollapsing = 0;
         subGridrecorrido_detalle_Allowselection = 0;
         edtTimeStamp_Enabled = 1;
         edtIMEI_Enabled = 1;
         cmbEmergencia.Enabled = 1;
         cmbIncidente.Enabled = 1;
         cmbTipoObstaculo.Enabled = 1;
         edtLatitud_Enabled = 1;
         edtLongitud_Enabled = 1;
         cmbTipoSenal.Enabled = 1;
         edtIDCoach_Enabled = 1;
         edtRecorridoDetallePaciente_Enabled = 1;
         edtRecorridoDetalleID_Enabled = 1;
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtRecorridoFecha_Jsonclick = "";
         edtRecorridoFecha_Enabled = 1;
         edtRecorridoID_Jsonclick = "";
         edtRecorridoID_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGridrecorrido_detalle_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         Gx_mode = "INS";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         SubsflControlProps_395( ) ;
         while ( nGXsfl_39_idx <= nRC_GXsfl_39 )
         {
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            standaloneNotModal035( ) ;
            standaloneModal035( ) ;
            GXCCtl = "TIPOSENAL_" + sGXsfl_39_idx;
            cmbTipoSenal.Name = GXCCtl;
            cmbTipoSenal.WebTags = "";
            cmbTipoSenal.addItem("1", "Inicio", 0);
            cmbTipoSenal.addItem("2", "Progreso", 0);
            cmbTipoSenal.addItem("3", "Fin", 0);
            if ( cmbTipoSenal.ItemCount > 0 )
            {
               A25TipoSenal = (short)(NumberUtil.Val( cmbTipoSenal.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A25TipoSenal), 4, 0))), "."));
               n25TipoSenal = false;
            }
            GXCCtl = "TIPOOBSTACULO_" + sGXsfl_39_idx;
            cmbTipoObstaculo.Name = GXCCtl;
            cmbTipoObstaculo.WebTags = "";
            cmbTipoObstaculo.addItem("0", "Ninguno", 0);
            cmbTipoObstaculo.addItem("1", "Agujero", 0);
            cmbTipoObstaculo.addItem("2", "Gradas", 0);
            cmbTipoObstaculo.addItem("3", "Otros", 0);
            if ( cmbTipoObstaculo.ItemCount > 0 )
            {
               A28TipoObstaculo = (short)(NumberUtil.Val( cmbTipoObstaculo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A28TipoObstaculo), 4, 0))), "."));
               n28TipoObstaculo = false;
            }
            GXCCtl = "INCIDENTE_" + sGXsfl_39_idx;
            cmbIncidente.Name = GXCCtl;
            cmbIncidente.WebTags = "";
            cmbIncidente.addItem("0", "Nada", 0);
            cmbIncidente.addItem("1", "Registrar Obstaculo", 0);
            if ( cmbIncidente.ItemCount > 0 )
            {
               A29Incidente = (short)(NumberUtil.Val( cmbIncidente.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A29Incidente), 4, 0))), "."));
               n29Incidente = false;
            }
            GXCCtl = "EMERGENCIA_" + sGXsfl_39_idx;
            cmbEmergencia.Name = GXCCtl;
            cmbEmergencia.WebTags = "";
            cmbEmergencia.addItem("0", "Nada", 0);
            cmbEmergencia.addItem("1", "Emergencia", 0);
            if ( cmbEmergencia.ItemCount > 0 )
            {
               A30Emergencia = (short)(NumberUtil.Val( cmbEmergencia.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A30Emergencia), 4, 0))), "."));
               n30Emergencia = false;
            }
            dynload_actions( ) ;
            SendRow035( ) ;
            nGXsfl_39_idx = (short)(nGXsfl_39_idx+1);
            sGXsfl_39_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_39_idx), 4, 0)), 4, "0");
            SubsflControlProps_395( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( Gridrecorrido_detalleContainer));
         /* End function gxnrGridrecorrido_detalle_newrow */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtRecorridoFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Recorridoid( Guid GX_Parm1 ,
                                     DateTime GX_Parm2 )
      {
         A23RecorridoID = (Guid)(GX_Parm1);
         A24RecorridoFecha = GX_Parm2;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(context.localUtil.TToC( A24RecorridoFecha, 10, 8, 0, 3, "/", ":", " "));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(Z23RecorridoID.ToString());
         isValidOutput.Add(context.localUtil.TToC( Z24RecorridoFecha, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(Gridrecorrido_detalleContainer);
         isValidOutput.Add(bttBtn_delete_Enabled);
         isValidOutput.Add(bttBtn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Recorridodetallepaciente( Guid GX_Parm1 )
      {
         A21RecorridoDetallePaciente = (Guid)(GX_Parm1);
         n21RecorridoDetallePaciente = false;
         /* Using cursor T000324 */
         pr_default.execute(22, new Object[] {n21RecorridoDetallePaciente, A21RecorridoDetallePaciente});
         if ( (pr_default.getStatus(22) == 101) )
         {
            if ( ! ( (Guid.Empty==A21RecorridoDetallePaciente) ) )
            {
               GX_msglist.addItem("No existe 'Recorrido Paciente ST'.", "ForeignKeyNotFound", 1, "RECORRIDODETALLEPACIENTE");
               AnyError = 1;
               GX_FocusControl = edtRecorridoDetallePaciente_Internalname;
            }
         }
         pr_default.close(22);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Idcoach( Guid GX_Parm1 )
      {
         A22IDCoach = (Guid)(GX_Parm1);
         n22IDCoach = false;
         /* Using cursor T000325 */
         pr_default.execute(23, new Object[] {n22IDCoach, A22IDCoach});
         if ( (pr_default.getStatus(23) == 101) )
         {
            if ( ! ( (Guid.Empty==A22IDCoach) ) )
            {
               GX_msglist.addItem("No existe 'Recorrido Detalle Coach ST'.", "ForeignKeyNotFound", 1, "IDCOACH");
               AnyError = 1;
               GX_FocusControl = edtIDCoach_Internalname;
            }
         }
         pr_default.close(23);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(22);
         pr_default.close(23);
         pr_default.close(5);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z23RecorridoID = (Guid)(Guid.Empty);
         Z24RecorridoFecha = (DateTime)(DateTime.MinValue);
         Z32RecorridoDetalleID = (Guid)(Guid.Empty);
         Z31IMEI = "";
         Z33TimeStamp = (DateTime)(DateTime.MinValue);
         Z21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         Z22IDCoach = (Guid)(Guid.Empty);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         A22IDCoach = (Guid)(Guid.Empty);
         GXKey = "";
         GXCCtl = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A23RecorridoID = (Guid)(Guid.Empty);
         A24RecorridoFecha = (DateTime)(DateTime.MinValue);
         lblTitledetalle_Jsonclick = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gridrecorrido_detalleContainer = new GXWebGrid( context);
         Gridrecorrido_detalleColumn = new GXWebColumn();
         A32RecorridoDetalleID = (Guid)(Guid.Empty);
         A26Longitud = "";
         A27Latitud = "";
         A31IMEI = "";
         A33TimeStamp = (DateTime)(DateTime.MinValue);
         Gx_mode = "";
         sMode5 = "";
         sStyleString = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         T00038_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T00038_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         T00039_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T00037_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T00037_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         sMode3 = "";
         T000310_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T000311_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T00036_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T00036_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         T000315_A23RecorridoID = new Guid[] {Guid.Empty} ;
         Z26Longitud = "";
         Z27Latitud = "";
         T000316_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T000316_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         T000316_A25TipoSenal = new short[1] ;
         T000316_n25TipoSenal = new bool[] {false} ;
         T000316_A26Longitud = new String[] {""} ;
         T000316_n26Longitud = new bool[] {false} ;
         T000316_A27Latitud = new String[] {""} ;
         T000316_n27Latitud = new bool[] {false} ;
         T000316_A28TipoObstaculo = new short[1] ;
         T000316_n28TipoObstaculo = new bool[] {false} ;
         T000316_A29Incidente = new short[1] ;
         T000316_n29Incidente = new bool[] {false} ;
         T000316_A30Emergencia = new short[1] ;
         T000316_n30Emergencia = new bool[] {false} ;
         T000316_A31IMEI = new String[] {""} ;
         T000316_n31IMEI = new bool[] {false} ;
         T000316_A33TimeStamp = new DateTime[] {DateTime.MinValue} ;
         T000316_n33TimeStamp = new bool[] {false} ;
         T000316_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         T000316_n21RecorridoDetallePaciente = new bool[] {false} ;
         T000316_A22IDCoach = new Guid[] {Guid.Empty} ;
         T000316_n22IDCoach = new bool[] {false} ;
         T00034_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         T00034_n21RecorridoDetallePaciente = new bool[] {false} ;
         T00035_A22IDCoach = new Guid[] {Guid.Empty} ;
         T00035_n22IDCoach = new bool[] {false} ;
         T000317_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         T000317_n21RecorridoDetallePaciente = new bool[] {false} ;
         T000318_A22IDCoach = new Guid[] {Guid.Empty} ;
         T000318_n22IDCoach = new bool[] {false} ;
         T000319_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T000319_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         T00033_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T00033_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         T00033_A25TipoSenal = new short[1] ;
         T00033_n25TipoSenal = new bool[] {false} ;
         T00033_A26Longitud = new String[] {""} ;
         T00033_n26Longitud = new bool[] {false} ;
         T00033_A27Latitud = new String[] {""} ;
         T00033_n27Latitud = new bool[] {false} ;
         T00033_A28TipoObstaculo = new short[1] ;
         T00033_n28TipoObstaculo = new bool[] {false} ;
         T00033_A29Incidente = new short[1] ;
         T00033_n29Incidente = new bool[] {false} ;
         T00033_A30Emergencia = new short[1] ;
         T00033_n30Emergencia = new bool[] {false} ;
         T00033_A31IMEI = new String[] {""} ;
         T00033_n31IMEI = new bool[] {false} ;
         T00033_A33TimeStamp = new DateTime[] {DateTime.MinValue} ;
         T00033_n33TimeStamp = new bool[] {false} ;
         T00033_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         T00033_n21RecorridoDetallePaciente = new bool[] {false} ;
         T00033_A22IDCoach = new Guid[] {Guid.Empty} ;
         T00033_n22IDCoach = new bool[] {false} ;
         T00032_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T00032_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         T00032_A25TipoSenal = new short[1] ;
         T00032_n25TipoSenal = new bool[] {false} ;
         T00032_A26Longitud = new String[] {""} ;
         T00032_n26Longitud = new bool[] {false} ;
         T00032_A27Latitud = new String[] {""} ;
         T00032_n27Latitud = new bool[] {false} ;
         T00032_A28TipoObstaculo = new short[1] ;
         T00032_n28TipoObstaculo = new bool[] {false} ;
         T00032_A29Incidente = new short[1] ;
         T00032_n29Incidente = new bool[] {false} ;
         T00032_A30Emergencia = new short[1] ;
         T00032_n30Emergencia = new bool[] {false} ;
         T00032_A31IMEI = new String[] {""} ;
         T00032_n31IMEI = new bool[] {false} ;
         T00032_A33TimeStamp = new DateTime[] {DateTime.MinValue} ;
         T00032_n33TimeStamp = new bool[] {false} ;
         T00032_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         T00032_n21RecorridoDetallePaciente = new bool[] {false} ;
         T00032_A22IDCoach = new Guid[] {Guid.Empty} ;
         T00032_n22IDCoach = new bool[] {false} ;
         T000323_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T000323_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         Gridrecorrido_detalleRow = new GXWebRow();
         subGridrecorrido_detalle_Linesclass = "";
         ROClassString = "";
         sImgUrl = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000324_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         T000324_n21RecorridoDetallePaciente = new bool[] {false} ;
         T000325_A22IDCoach = new Guid[] {Guid.Empty} ;
         T000325_n22IDCoach = new bool[] {false} ;
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.recorrido__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.recorrido__default(),
            new Object[][] {
                new Object[] {
               T00032_A23RecorridoID, T00032_A32RecorridoDetalleID, T00032_A25TipoSenal, T00032_n25TipoSenal, T00032_A26Longitud, T00032_n26Longitud, T00032_A27Latitud, T00032_n27Latitud, T00032_A28TipoObstaculo, T00032_n28TipoObstaculo,
               T00032_A29Incidente, T00032_n29Incidente, T00032_A30Emergencia, T00032_n30Emergencia, T00032_A31IMEI, T00032_n31IMEI, T00032_A33TimeStamp, T00032_n33TimeStamp, T00032_A21RecorridoDetallePaciente, T00032_n21RecorridoDetallePaciente,
               T00032_A22IDCoach, T00032_n22IDCoach
               }
               , new Object[] {
               T00033_A23RecorridoID, T00033_A32RecorridoDetalleID, T00033_A25TipoSenal, T00033_n25TipoSenal, T00033_A26Longitud, T00033_n26Longitud, T00033_A27Latitud, T00033_n27Latitud, T00033_A28TipoObstaculo, T00033_n28TipoObstaculo,
               T00033_A29Incidente, T00033_n29Incidente, T00033_A30Emergencia, T00033_n30Emergencia, T00033_A31IMEI, T00033_n31IMEI, T00033_A33TimeStamp, T00033_n33TimeStamp, T00033_A21RecorridoDetallePaciente, T00033_n21RecorridoDetallePaciente,
               T00033_A22IDCoach, T00033_n22IDCoach
               }
               , new Object[] {
               T00034_A21RecorridoDetallePaciente
               }
               , new Object[] {
               T00035_A22IDCoach
               }
               , new Object[] {
               T00036_A23RecorridoID, T00036_A24RecorridoFecha
               }
               , new Object[] {
               T00037_A23RecorridoID, T00037_A24RecorridoFecha
               }
               , new Object[] {
               T00038_A23RecorridoID, T00038_A24RecorridoFecha
               }
               , new Object[] {
               T00039_A23RecorridoID
               }
               , new Object[] {
               T000310_A23RecorridoID
               }
               , new Object[] {
               T000311_A23RecorridoID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000315_A23RecorridoID
               }
               , new Object[] {
               T000316_A23RecorridoID, T000316_A32RecorridoDetalleID, T000316_A25TipoSenal, T000316_n25TipoSenal, T000316_A26Longitud, T000316_n26Longitud, T000316_A27Latitud, T000316_n27Latitud, T000316_A28TipoObstaculo, T000316_n28TipoObstaculo,
               T000316_A29Incidente, T000316_n29Incidente, T000316_A30Emergencia, T000316_n30Emergencia, T000316_A31IMEI, T000316_n31IMEI, T000316_A33TimeStamp, T000316_n33TimeStamp, T000316_A21RecorridoDetallePaciente, T000316_n21RecorridoDetallePaciente,
               T000316_A22IDCoach, T000316_n22IDCoach
               }
               , new Object[] {
               T000317_A21RecorridoDetallePaciente
               }
               , new Object[] {
               T000318_A22IDCoach
               }
               , new Object[] {
               T000319_A23RecorridoID, T000319_A32RecorridoDetalleID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000323_A23RecorridoID, T000323_A32RecorridoDetalleID
               }
               , new Object[] {
               T000324_A21RecorridoDetallePaciente
               }
               , new Object[] {
               T000325_A22IDCoach
               }
            }
         );
         Z32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
         A32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
      }

      private short nIsMod_5 ;
      private short nRC_GXsfl_39 ;
      private short nGXsfl_39_idx=1 ;
      private short Z25TipoSenal ;
      private short Z28TipoObstaculo ;
      private short Z29Incidente ;
      private short Z30Emergencia ;
      private short nRcdDeleted_5 ;
      private short nRcdExists_5 ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A25TipoSenal ;
      private short A28TipoObstaculo ;
      private short A29Incidente ;
      private short A30Emergencia ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short subGridrecorrido_detalle_Backcolorstyle ;
      private short subGridrecorrido_detalle_Allowselection ;
      private short subGridrecorrido_detalle_Allowhovering ;
      private short subGridrecorrido_detalle_Allowcollapsing ;
      private short subGridrecorrido_detalle_Collapsed ;
      private short nBlankRcdCount5 ;
      private short RcdFound5 ;
      private short nBlankRcdUsr5 ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound3 ;
      private short subGridrecorrido_detalle_Backstyle ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtRecorridoID_Enabled ;
      private int edtRecorridoFecha_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int edtRecorridoDetalleID_Enabled ;
      private int edtRecorridoDetallePaciente_Enabled ;
      private int edtIDCoach_Enabled ;
      private int edtLongitud_Enabled ;
      private int edtLatitud_Enabled ;
      private int edtIMEI_Enabled ;
      private int edtTimeStamp_Enabled ;
      private int subGridrecorrido_detalle_Selectioncolor ;
      private int subGridrecorrido_detalle_Hoveringcolor ;
      private int fRowAdded ;
      private int subGridrecorrido_detalle_Backcolor ;
      private int subGridrecorrido_detalle_Allbackcolor ;
      private int imgprompt_21_Visible ;
      private int imgprompt_22_Visible ;
      private int defedtRecorridoDetalleID_Enabled ;
      private int idxLst ;
      private long GRIDRECORRIDO_DETALLE_nFirstRecordOnPage ;
      private String sPrefix ;
      private String sGXsfl_39_idx="0001" ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String GXCCtl ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtRecorridoID_Internalname ;
      private String divTablemain_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtRecorridoID_Jsonclick ;
      private String edtRecorridoFecha_Internalname ;
      private String edtRecorridoFecha_Jsonclick ;
      private String lblTitledetalle_Internalname ;
      private String lblTitledetalle_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sMode5 ;
      private String edtRecorridoDetalleID_Internalname ;
      private String edtRecorridoDetallePaciente_Internalname ;
      private String edtIDCoach_Internalname ;
      private String cmbTipoSenal_Internalname ;
      private String edtLongitud_Internalname ;
      private String edtLatitud_Internalname ;
      private String cmbTipoObstaculo_Internalname ;
      private String cmbIncidente_Internalname ;
      private String cmbEmergencia_Internalname ;
      private String edtIMEI_Internalname ;
      private String edtTimeStamp_Internalname ;
      private String imgprompt_21_Link ;
      private String sStyleString ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode3 ;
      private String imgprompt_21_Internalname ;
      private String imgprompt_22_Internalname ;
      private String sGXsfl_39_fel_idx="0001" ;
      private String subGridrecorrido_detalle_Class ;
      private String subGridrecorrido_detalle_Linesclass ;
      private String imgprompt_22_Link ;
      private String ROClassString ;
      private String edtRecorridoDetalleID_Jsonclick ;
      private String edtRecorridoDetallePaciente_Jsonclick ;
      private String sImgUrl ;
      private String edtIDCoach_Jsonclick ;
      private String cmbTipoSenal_Jsonclick ;
      private String edtLongitud_Jsonclick ;
      private String edtLatitud_Jsonclick ;
      private String cmbTipoObstaculo_Jsonclick ;
      private String cmbIncidente_Jsonclick ;
      private String cmbEmergencia_Jsonclick ;
      private String edtIMEI_Jsonclick ;
      private String edtTimeStamp_Jsonclick ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String subGridrecorrido_detalle_Internalname ;
      private DateTime Z24RecorridoFecha ;
      private DateTime Z33TimeStamp ;
      private DateTime A24RecorridoFecha ;
      private DateTime A33TimeStamp ;
      private bool entryPointCalled ;
      private bool n21RecorridoDetallePaciente ;
      private bool n22IDCoach ;
      private bool toggleJsOutput ;
      private bool n25TipoSenal ;
      private bool n28TipoObstaculo ;
      private bool n29Incidente ;
      private bool n30Emergencia ;
      private bool wbErr ;
      private bool bGXsfl_39_Refreshing=false ;
      private bool n26Longitud ;
      private bool n27Latitud ;
      private bool n31IMEI ;
      private bool n33TimeStamp ;
      private bool Gx_longc ;
      private String A26Longitud ;
      private String A27Latitud ;
      private String Z26Longitud ;
      private String Z27Latitud ;
      private String Z31IMEI ;
      private String A31IMEI ;
      private Guid Z23RecorridoID ;
      private Guid Z32RecorridoDetalleID ;
      private Guid Z21RecorridoDetallePaciente ;
      private Guid Z22IDCoach ;
      private Guid A21RecorridoDetallePaciente ;
      private Guid A22IDCoach ;
      private Guid A23RecorridoID ;
      private Guid A32RecorridoDetalleID ;
      private GxUnknownObjectCollection isValidOutput ;
      private GXWebGrid Gridrecorrido_detalleContainer ;
      private GXWebRow Gridrecorrido_detalleRow ;
      private GXWebColumn Gridrecorrido_detalleColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbTipoSenal ;
      private GXCombobox cmbTipoObstaculo ;
      private GXCombobox cmbIncidente ;
      private GXCombobox cmbEmergencia ;
      private IDataStoreProvider pr_default ;
      private Guid[] T00038_A23RecorridoID ;
      private DateTime[] T00038_A24RecorridoFecha ;
      private Guid[] T00039_A23RecorridoID ;
      private Guid[] T00037_A23RecorridoID ;
      private DateTime[] T00037_A24RecorridoFecha ;
      private Guid[] T000310_A23RecorridoID ;
      private Guid[] T000311_A23RecorridoID ;
      private Guid[] T00036_A23RecorridoID ;
      private DateTime[] T00036_A24RecorridoFecha ;
      private IDataStoreProvider pr_gam ;
      private Guid[] T000315_A23RecorridoID ;
      private Guid[] T000316_A23RecorridoID ;
      private Guid[] T000316_A32RecorridoDetalleID ;
      private short[] T000316_A25TipoSenal ;
      private bool[] T000316_n25TipoSenal ;
      private String[] T000316_A26Longitud ;
      private bool[] T000316_n26Longitud ;
      private String[] T000316_A27Latitud ;
      private bool[] T000316_n27Latitud ;
      private short[] T000316_A28TipoObstaculo ;
      private bool[] T000316_n28TipoObstaculo ;
      private short[] T000316_A29Incidente ;
      private bool[] T000316_n29Incidente ;
      private short[] T000316_A30Emergencia ;
      private bool[] T000316_n30Emergencia ;
      private String[] T000316_A31IMEI ;
      private bool[] T000316_n31IMEI ;
      private DateTime[] T000316_A33TimeStamp ;
      private bool[] T000316_n33TimeStamp ;
      private Guid[] T000316_A21RecorridoDetallePaciente ;
      private bool[] T000316_n21RecorridoDetallePaciente ;
      private Guid[] T000316_A22IDCoach ;
      private bool[] T000316_n22IDCoach ;
      private Guid[] T00034_A21RecorridoDetallePaciente ;
      private bool[] T00034_n21RecorridoDetallePaciente ;
      private Guid[] T00035_A22IDCoach ;
      private bool[] T00035_n22IDCoach ;
      private Guid[] T000317_A21RecorridoDetallePaciente ;
      private bool[] T000317_n21RecorridoDetallePaciente ;
      private Guid[] T000318_A22IDCoach ;
      private bool[] T000318_n22IDCoach ;
      private Guid[] T000319_A23RecorridoID ;
      private Guid[] T000319_A32RecorridoDetalleID ;
      private Guid[] T00033_A23RecorridoID ;
      private Guid[] T00033_A32RecorridoDetalleID ;
      private short[] T00033_A25TipoSenal ;
      private bool[] T00033_n25TipoSenal ;
      private String[] T00033_A26Longitud ;
      private bool[] T00033_n26Longitud ;
      private String[] T00033_A27Latitud ;
      private bool[] T00033_n27Latitud ;
      private short[] T00033_A28TipoObstaculo ;
      private bool[] T00033_n28TipoObstaculo ;
      private short[] T00033_A29Incidente ;
      private bool[] T00033_n29Incidente ;
      private short[] T00033_A30Emergencia ;
      private bool[] T00033_n30Emergencia ;
      private String[] T00033_A31IMEI ;
      private bool[] T00033_n31IMEI ;
      private DateTime[] T00033_A33TimeStamp ;
      private bool[] T00033_n33TimeStamp ;
      private Guid[] T00033_A21RecorridoDetallePaciente ;
      private bool[] T00033_n21RecorridoDetallePaciente ;
      private Guid[] T00033_A22IDCoach ;
      private bool[] T00033_n22IDCoach ;
      private Guid[] T00032_A23RecorridoID ;
      private Guid[] T00032_A32RecorridoDetalleID ;
      private short[] T00032_A25TipoSenal ;
      private bool[] T00032_n25TipoSenal ;
      private String[] T00032_A26Longitud ;
      private bool[] T00032_n26Longitud ;
      private String[] T00032_A27Latitud ;
      private bool[] T00032_n27Latitud ;
      private short[] T00032_A28TipoObstaculo ;
      private bool[] T00032_n28TipoObstaculo ;
      private short[] T00032_A29Incidente ;
      private bool[] T00032_n29Incidente ;
      private short[] T00032_A30Emergencia ;
      private bool[] T00032_n30Emergencia ;
      private String[] T00032_A31IMEI ;
      private bool[] T00032_n31IMEI ;
      private DateTime[] T00032_A33TimeStamp ;
      private bool[] T00032_n33TimeStamp ;
      private Guid[] T00032_A21RecorridoDetallePaciente ;
      private bool[] T00032_n21RecorridoDetallePaciente ;
      private Guid[] T00032_A22IDCoach ;
      private bool[] T00032_n22IDCoach ;
      private Guid[] T000323_A23RecorridoID ;
      private Guid[] T000323_A32RecorridoDetalleID ;
      private Guid[] T000324_A21RecorridoDetallePaciente ;
      private bool[] T000324_n21RecorridoDetallePaciente ;
      private Guid[] T000325_A22IDCoach ;
      private bool[] T000325_n22IDCoach ;
      private GXWebForm Form ;
   }

   public class recorrido__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class recorrido__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new ForEachCursor(def[6])
       ,new ForEachCursor(def[7])
       ,new ForEachCursor(def[8])
       ,new ForEachCursor(def[9])
       ,new UpdateCursor(def[10])
       ,new UpdateCursor(def[11])
       ,new UpdateCursor(def[12])
       ,new ForEachCursor(def[13])
       ,new ForEachCursor(def[14])
       ,new ForEachCursor(def[15])
       ,new ForEachCursor(def[16])
       ,new ForEachCursor(def[17])
       ,new UpdateCursor(def[18])
       ,new UpdateCursor(def[19])
       ,new UpdateCursor(def[20])
       ,new ForEachCursor(def[21])
       ,new ForEachCursor(def[22])
       ,new ForEachCursor(def[23])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmT00038 ;
        prmT00038 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00039 ;
        prmT00039 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00037 ;
        prmT00037 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000310 ;
        prmT000310 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000311 ;
        prmT000311 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00036 ;
        prmT00036 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000312 ;
        prmT000312 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoFecha",SqlDbType.DateTime,8,5}
        } ;
        Object[] prmT000313 ;
        prmT000313 = new Object[] {
        new Object[] {"@RecorridoFecha",SqlDbType.DateTime,8,5} ,
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000314 ;
        prmT000314 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000315 ;
        prmT000315 = new Object[] {
        } ;
        Object[] prmT000316 ;
        prmT000316 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00034 ;
        prmT00034 = new Object[] {
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00035 ;
        prmT00035 = new Object[] {
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000317 ;
        prmT000317 = new Object[] {
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000318 ;
        prmT000318 = new Object[] {
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000319 ;
        prmT000319 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00033 ;
        prmT00033 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00032 ;
        prmT00032 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000320 ;
        prmT000320 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@TipoSenal",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Longitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@Latitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@TipoObstaculo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Incidente",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Emergencia",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@IMEI",SqlDbType.VarChar,50,0} ,
        new Object[] {"@TimeStamp",SqlDbType.DateTime,8,5} ,
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000321 ;
        prmT000321 = new Object[] {
        new Object[] {"@TipoSenal",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Longitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@Latitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@TipoObstaculo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Incidente",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Emergencia",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@IMEI",SqlDbType.VarChar,50,0} ,
        new Object[] {"@TimeStamp",SqlDbType.DateTime,8,5} ,
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000322 ;
        prmT000322 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000323 ;
        prmT000323 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000324 ;
        prmT000324 = new Object[] {
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000325 ;
        prmT000325 = new Object[] {
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("T00032", "SELECT [RecorridoID], [RecorridoDetalleID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente] AS RecorridoDetallePaciente, [IDCoach] AS IDCoach FROM [RecorridoDetalle] WITH (UPDLOCK) WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00032,1,0,true,false )
           ,new CursorDef("T00033", "SELECT [RecorridoID], [RecorridoDetalleID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente] AS RecorridoDetallePaciente, [IDCoach] AS IDCoach FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00033,1,0,true,false )
           ,new CursorDef("T00034", "SELECT [PersonaID] AS RecorridoDetallePaciente FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @RecorridoDetallePaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT00034,1,0,true,false )
           ,new CursorDef("T00035", "SELECT [PersonaID] AS IDCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @IDCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmT00035,1,0,true,false )
           ,new CursorDef("T00036", "SELECT [RecorridoID], [RecorridoFecha] FROM [Recorrido] WITH (UPDLOCK) WHERE [RecorridoID] = @RecorridoID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00036,1,0,true,false )
           ,new CursorDef("T00037", "SELECT [RecorridoID], [RecorridoFecha] FROM [Recorrido] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00037,1,0,true,false )
           ,new CursorDef("T00038", "SELECT TM1.[RecorridoID], TM1.[RecorridoFecha] FROM [Recorrido] TM1 WITH (NOLOCK) WHERE TM1.[RecorridoID] = @RecorridoID ORDER BY TM1.[RecorridoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00038,100,0,true,false )
           ,new CursorDef("T00039", "SELECT [RecorridoID] FROM [Recorrido] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00039,1,0,true,false )
           ,new CursorDef("T000310", "SELECT TOP 1 [RecorridoID] FROM [Recorrido] WITH (NOLOCK) WHERE ( [RecorridoID] > @RecorridoID) ORDER BY [RecorridoID]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000310,1,0,true,true )
           ,new CursorDef("T000311", "SELECT TOP 1 [RecorridoID] FROM [Recorrido] WITH (NOLOCK) WHERE ( [RecorridoID] < @RecorridoID) ORDER BY [RecorridoID] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000311,1,0,true,true )
           ,new CursorDef("T000312", "INSERT INTO [Recorrido]([RecorridoID], [RecorridoFecha]) VALUES(@RecorridoID, @RecorridoFecha)", GxErrorMask.GX_NOMASK,prmT000312)
           ,new CursorDef("T000313", "UPDATE [Recorrido] SET [RecorridoFecha]=@RecorridoFecha  WHERE [RecorridoID] = @RecorridoID", GxErrorMask.GX_NOMASK,prmT000313)
           ,new CursorDef("T000314", "DELETE FROM [Recorrido]  WHERE [RecorridoID] = @RecorridoID", GxErrorMask.GX_NOMASK,prmT000314)
           ,new CursorDef("T000315", "SELECT [RecorridoID] FROM [Recorrido] WITH (NOLOCK) ORDER BY [RecorridoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000315,100,0,true,false )
           ,new CursorDef("T000316", "SELECT [RecorridoID], [RecorridoDetalleID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente] AS RecorridoDetallePaciente, [IDCoach] AS IDCoach FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID and [RecorridoDetalleID] = @RecorridoDetalleID ORDER BY [RecorridoID], [RecorridoDetalleID] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000316,11,0,true,false )
           ,new CursorDef("T000317", "SELECT [PersonaID] AS RecorridoDetallePaciente FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @RecorridoDetallePaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT000317,1,0,true,false )
           ,new CursorDef("T000318", "SELECT [PersonaID] AS IDCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @IDCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmT000318,1,0,true,false )
           ,new CursorDef("T000319", "SELECT [RecorridoID], [RecorridoDetalleID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000319,1,0,true,false )
           ,new CursorDef("T000320", "INSERT INTO [RecorridoDetalle]([RecorridoID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente], [IDCoach], [RecorridoDetalleID]) VALUES(@RecorridoID, @TipoSenal, @Longitud, @Latitud, @TipoObstaculo, @Incidente, @Emergencia, @IMEI, @TimeStamp, @RecorridoDetallePaciente, @IDCoach, @RecorridoDetalleID)", GxErrorMask.GX_NOMASK,prmT000320)
           ,new CursorDef("T000321", "UPDATE [RecorridoDetalle] SET [TipoSenal]=@TipoSenal, [Longitud]=@Longitud, [Latitud]=@Latitud, [TipoObstaculo]=@TipoObstaculo, [Incidente]=@Incidente, [Emergencia]=@Emergencia, [IMEI]=@IMEI, [TimeStamp]=@TimeStamp, [RecorridoDetallePaciente]=@RecorridoDetallePaciente, [IDCoach]=@IDCoach  WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID", GxErrorMask.GX_NOMASK,prmT000321)
           ,new CursorDef("T000322", "DELETE FROM [RecorridoDetalle]  WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID", GxErrorMask.GX_NOMASK,prmT000322)
           ,new CursorDef("T000323", "SELECT [RecorridoID], [RecorridoDetalleID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID ORDER BY [RecorridoID], [RecorridoDetalleID] ",true, GxErrorMask.GX_NOMASK, false, this,prmT000323,11,0,true,false )
           ,new CursorDef("T000324", "SELECT [PersonaID] AS RecorridoDetallePaciente FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @RecorridoDetallePaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT000324,1,0,true,false )
           ,new CursorDef("T000325", "SELECT [PersonaID] AS IDCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @IDCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmT000325,1,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((short[]) buf[8])[0] = rslt.getShort(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((short[]) buf[10])[0] = rslt.getShort(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((short[]) buf[12])[0] = rslt.getShort(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(9);
              ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(10);
              ((Guid[]) buf[18])[0] = rslt.getGuid(11) ;
              ((bool[]) buf[19])[0] = rslt.wasNull(11);
              ((Guid[]) buf[20])[0] = rslt.getGuid(12) ;
              ((bool[]) buf[21])[0] = rslt.wasNull(12);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((short[]) buf[8])[0] = rslt.getShort(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((short[]) buf[10])[0] = rslt.getShort(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((short[]) buf[12])[0] = rslt.getShort(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(9);
              ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(10);
              ((Guid[]) buf[18])[0] = rslt.getGuid(11) ;
              ((bool[]) buf[19])[0] = rslt.wasNull(11);
              ((Guid[]) buf[20])[0] = rslt.getGuid(12) ;
              ((bool[]) buf[21])[0] = rslt.wasNull(12);
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 4 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              return;
           case 5 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              return;
           case 6 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              return;
           case 7 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 8 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 9 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 13 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 14 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((short[]) buf[8])[0] = rslt.getShort(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((short[]) buf[10])[0] = rslt.getShort(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((short[]) buf[12])[0] = rslt.getShort(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(9);
              ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(10);
              ((Guid[]) buf[18])[0] = rslt.getGuid(11) ;
              ((bool[]) buf[19])[0] = rslt.wasNull(11);
              ((Guid[]) buf[20])[0] = rslt.getGuid(12) ;
              ((bool[]) buf[21])[0] = rslt.wasNull(12);
              return;
           case 15 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 16 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 17 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 21 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 22 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 23 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 2 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 3 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 5 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 6 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 7 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 9 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 10 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameterDatetime(2, (DateTime)parms[1]);
              return;
           case 11 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 12 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 14 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 15 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 16 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 17 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 18 :
              stmt.SetParameter(1, (Guid)parms[0]);
              if ( (bool)parms[1] )
              {
                 stmt.setNull( 2 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(2, (short)parms[2]);
              }
              if ( (bool)parms[3] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[4]);
              }
              if ( (bool)parms[5] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[6]);
              }
              if ( (bool)parms[7] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[8]);
              }
              if ( (bool)parms[9] )
              {
                 stmt.setNull( 6 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(6, (short)parms[10]);
              }
              if ( (bool)parms[11] )
              {
                 stmt.setNull( 7 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(7, (short)parms[12]);
              }
              if ( (bool)parms[13] )
              {
                 stmt.setNull( 8 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(8, (String)parms[14]);
              }
              if ( (bool)parms[15] )
              {
                 stmt.setNull( 9 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameterDatetime(9, (DateTime)parms[16]);
              }
              if ( (bool)parms[17] )
              {
                 stmt.setNull( 10 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(10, (Guid)parms[18]);
              }
              if ( (bool)parms[19] )
              {
                 stmt.setNull( 11 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(11, (Guid)parms[20]);
              }
              stmt.SetParameter(12, (Guid)parms[21]);
              return;
           case 19 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(1, (short)parms[1]);
              }
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 2 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(2, (String)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 4 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(4, (short)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[9]);
              }
              if ( (bool)parms[10] )
              {
                 stmt.setNull( 6 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(6, (short)parms[11]);
              }
              if ( (bool)parms[12] )
              {
                 stmt.setNull( 7 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(7, (String)parms[13]);
              }
              if ( (bool)parms[14] )
              {
                 stmt.setNull( 8 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameterDatetime(8, (DateTime)parms[15]);
              }
              if ( (bool)parms[16] )
              {
                 stmt.setNull( 9 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(9, (Guid)parms[17]);
              }
              if ( (bool)parms[18] )
              {
                 stmt.setNull( 10 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(10, (Guid)parms[19]);
              }
              stmt.SetParameter(11, (Guid)parms[20]);
              stmt.SetParameter(12, (Guid)parms[21]);
              return;
           case 20 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 21 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 22 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 23 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
     }
  }

}

}
