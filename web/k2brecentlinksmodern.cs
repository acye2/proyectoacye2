/*
               File: K2BRecentLinksModern
        Description: K2 BRecent Links3
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:32.48
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2brecentlinksmodern : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public k2brecentlinksmodern( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
      }

      public k2brecentlinksmodern( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_FormCaption ,
                           bool aP1_IncludeInStack )
      {
         this.AV8FormCaption = aP0_FormCaption;
         this.AV19IncludeInStack = aP1_IncludeInStack;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV8FormCaption = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
                  AV19IncludeInStack = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19IncludeInStack", AV19IncludeInStack);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(String)AV8FormCaption,(bool)AV19IncludeInStack});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"K2bfreestylerecentlinks") == 0 )
               {
                  nRC_GXsfl_2 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_2_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_2_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrK2bfreestylerecentlinks_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"K2bfreestylerecentlinks") == 0 )
               {
                  AV8FormCaption = GetNextPar( );
                  AV19IncludeInStack = (bool)(BooleanUtil.Val(GetNextPar( )));
                  AV18StartIndex = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  AV16Length = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrK2bfreestylerecentlinks_refresh( AV8FormCaption, AV19IncludeInStack, AV18StartIndex, AV16Length, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA0E2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               context.Gx_err = 0;
               WS0E2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, false);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "K2 BRecent Links3") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171523255", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
            context.WriteHtmlText( "<body ") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" data-gx-class=\"Form\" novalidate action=\""+formatLink("k2brecentlinksmodern.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV8FormCaption)) + "," + UrlEncode(StringUtil.BoolToStr(AV19IncludeInStack))+"\">") ;
               GxWebStd.gx_hidden_field( context, "_EventName", "");
               GxWebStd.gx_hidden_field( context, "_EventGridId", "");
               GxWebStd.gx_hidden_field( context, "_EventRowId", "");
               context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Class", "Form", true);
            }
         }
         else
         {
            bool toggleHtmlOutput = isOutputEnabled( ) ;
            if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
            }
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            if ( toggleHtmlOutput )
            {
               if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableOutput();
                  }
               }
            }
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_2", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_2), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV8FormCaption", StringUtil.RTrim( wcpOAV8FormCaption));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"wcpOAV19IncludeInStack", wcpOAV19IncludeInStack);
         GxWebStd.gx_hidden_field( context, sPrefix+"vFORMCAPTION", StringUtil.RTrim( AV8FormCaption));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vINCLUDEINSTACK", AV19IncludeInStack);
         GxWebStd.gx_hidden_field( context, sPrefix+"vSTARTINDEX", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV18StartIndex), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vLENGTH", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV16Length), 4, 0, ",", "")));
      }

      protected void RenderHtmlCloseForm0E2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && ( context.isAjaxRequest( ) || context.isSpaRequest( ) ) )
         {
            context.AddJavascriptSource("k2brecentlinksmodern.js", "?201811171523257", false);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( nGXWrapped != 1 )
            {
               context.WriteHtmlTextNl( "</form>") ;
            }
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            SendWebComponentState();
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "K2BRecentLinksModern" ;
      }

      public override String GetPgmdesc( )
      {
         return "K2 BRecent Links3" ;
      }

      protected void WB0E0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "k2brecentlinksmodern.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /*  Grid Control  */
            K2bfreestylerecentlinksContainer.SetIsFreestyle(true);
            K2bfreestylerecentlinksContainer.SetWrapped(nGXWrapped);
            if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"K2bfreestylerecentlinksContainer"+"DivS\" data-gxgridid=\"2\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subK2bfreestylerecentlinks_Internalname, subK2bfreestylerecentlinks_Internalname, "", "FreeStyleGrid", 0, "", "", 0, 0, sStyleString, "", 0);
               K2bfreestylerecentlinksContainer.AddObjectProperty("GridName", "K2bfreestylerecentlinks");
            }
            else
            {
               K2bfreestylerecentlinksContainer.AddObjectProperty("GridName", "K2bfreestylerecentlinks");
               K2bfreestylerecentlinksContainer.AddObjectProperty("Class", StringUtil.RTrim( "FreeStyleGrid"));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Class", "FreeStyleGrid");
               K2bfreestylerecentlinksContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Backcolorstyle), 1, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Borderwidth), 4, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("CmpContext", sPrefix);
               K2bfreestylerecentlinksContainer.AddObjectProperty("InMasterPage", "false");
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksColumn.AddObjectProperty("Value", lblK2brecentlinkseparator_Caption);
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               K2bfreestylerecentlinksColumn.AddObjectProperty("Value", lblK2brecentlinkseparator_Caption);
               K2bfreestylerecentlinksContainer.AddColumnProperties(K2bfreestylerecentlinksColumn);
               K2bfreestylerecentlinksContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Allowselection), 1, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Selectioncolor), 9, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Allowhovering), 1, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Hoveringcolor), 9, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Allowcollapsing), 1, 0, ".", "")));
               K2bfreestylerecentlinksContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 2 )
         {
            wbEnd = 0;
            nRC_GXsfl_2 = (short)(nGXsfl_2_idx-1);
            if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+sPrefix+"K2bfreestylerecentlinksContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"K2bfreestylerecentlinks", K2bfreestylerecentlinksContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"K2bfreestylerecentlinksContainerData", K2bfreestylerecentlinksContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"K2bfreestylerecentlinksContainerData"+"V", K2bfreestylerecentlinksContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+sPrefix+"K2bfreestylerecentlinksContainerData"+"V"+"\" value='"+K2bfreestylerecentlinksContainer.GridValuesHidden()+"'/>") ;
               }
            }
         }
         wbLoad = true;
      }

      protected void START0E2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
               Form.Meta.addItem("description", "K2 BRecent Links3", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP0E0( ) ;
            }
         }
      }

      protected void WS0E2( )
      {
         START0E2( ) ;
         EVT0E2( ) ;
      }

      protected void EVT0E2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0E0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0E0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 28), "K2BFREESTYLERECENTLINKS.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP0E0( ) ;
                              }
                              nGXsfl_2_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
                              SubsflControlProps_22( ) ;
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "K2BFREESTYLERECENTLINKS.LOAD") == 0 )
                                 {
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          E110E2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP0E0( ) ;
                                    }
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE0E2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0E2( ) ;
            }
         }
      }

      protected void PA0E2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrK2bfreestylerecentlinks_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_22( ) ;
         while ( nGXsfl_2_idx <= nRC_GXsfl_2 )
         {
            sendrow_22( ) ;
            nGXsfl_2_idx = (short)(((subK2bfreestylerecentlinks_Islastpage==1)&&(nGXsfl_2_idx+1>subK2bfreestylerecentlinks_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
            sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
            SubsflControlProps_22( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( K2bfreestylerecentlinksContainer));
         /* End function gxnrK2bfreestylerecentlinks_newrow */
      }

      protected void gxgrK2bfreestylerecentlinks_refresh( String AV8FormCaption ,
                                                          bool AV19IncludeInStack ,
                                                          short AV18StartIndex ,
                                                          short AV16Length ,
                                                          String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         K2BFREESTYLERECENTLINKS_nCurrentRecord = 0;
         RF0E2( ) ;
         /* End function gxgrK2bfreestylerecentlinks_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0E2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      protected void RF0E2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            K2bfreestylerecentlinksContainer.ClearRows();
         }
         wbStart = 2;
         nGXsfl_2_idx = 1;
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         bGXsfl_2_Refreshing = true;
         K2bfreestylerecentlinksContainer.AddObjectProperty("GridName", "K2bfreestylerecentlinks");
         K2bfreestylerecentlinksContainer.AddObjectProperty("CmpContext", sPrefix);
         K2bfreestylerecentlinksContainer.AddObjectProperty("InMasterPage", "false");
         K2bfreestylerecentlinksContainer.AddObjectProperty("Class", StringUtil.RTrim( "FreeStyleGrid"));
         K2bfreestylerecentlinksContainer.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         K2bfreestylerecentlinksContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         K2bfreestylerecentlinksContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         K2bfreestylerecentlinksContainer.AddObjectProperty("Class", "FreeStyleGrid");
         K2bfreestylerecentlinksContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         K2bfreestylerecentlinksContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         K2bfreestylerecentlinksContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Backcolorstyle), 1, 0, ".", "")));
         K2bfreestylerecentlinksContainer.AddObjectProperty("Borderwidth", StringUtil.LTrim( StringUtil.NToC( (decimal)(subK2bfreestylerecentlinks_Borderwidth), 4, 0, ".", "")));
         K2bfreestylerecentlinksContainer.PageSize = subK2bfreestylerecentlinks_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_22( ) ;
            E110E2 ();
            wbEnd = 2;
            WB0E0( ) ;
         }
         bGXsfl_2_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes0E2( )
      {
      }

      protected int subK2bfreestylerecentlinks_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subK2bfreestylerecentlinks_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subK2bfreestylerecentlinks_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subK2bfreestylerecentlinks_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP0E0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            /* Read saved values. */
            nRC_GXsfl_2 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_2"), ",", "."));
            wcpOAV8FormCaption = cgiGet( sPrefix+"wcpOAV8FormCaption");
            wcpOAV19IncludeInStack = StringUtil.StrToBool( cgiGet( sPrefix+"wcpOAV19IncludeInStack"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      private void E110E2( )
      {
         /* K2bfreestylerecentlinks_Load Routine */
         new k2bstackmanagement(context ).execute(  AV8FormCaption,  AV19IncludeInStack, out  AV12Stack) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19IncludeInStack", AV19IncludeInStack);
         AV7FirstRecentLink = true;
         AV16Length = (short)(AV12Stack.Count);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16Length", StringUtil.LTrim( StringUtil.Str( (decimal)(AV16Length), 4, 0)));
         if ( AV16Length > 1 )
         {
            /* Execute user subroutine: 'GETRECENTLINKSSTARTINDEX' */
            S112 ();
            if (returnInSub) return;
            AV10Index = AV18StartIndex;
            while ( AV10Index <= AV16Length - 1 )
            {
               AV14StackItem = ((SdtK2BStack_K2BStackItem)AV12Stack.Item(AV10Index));
               lblK2brecentlink_Caption = AV14StackItem.gxTpr_Caption;
               lblK2brecentlink_Link = formatLink(AV14StackItem.gxTpr_Url) ;
               if ( ! AV7FirstRecentLink )
               {
                  lblK2brecentlinkseparator_Caption = "/";
               }
               else
               {
                  lblK2brecentlinkseparator_Caption = "";
                  AV7FirstRecentLink = false;
               }
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 2;
               }
               sendrow_22( ) ;
               if ( isFullAjaxMode( ) && ! bGXsfl_2_Refreshing )
               {
                  context.DoAjaxLoad(2, K2bfreestylerecentlinksRow);
               }
               AV10Index = (int)(AV10Index+1);
            }
         }
         /*  Sending Event outputs  */
      }

      protected void S112( )
      {
         /* 'GETRECENTLINKSSTARTINDEX' Routine */
         GXt_int1 = AV17MaxLength;
         new k2bgetrecentlinksmaxlength(context ).execute( out  GXt_int1) ;
         AV17MaxLength = (short)(GXt_int1);
         if ( AV16Length - 1 < AV17MaxLength )
         {
            AV18StartIndex = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18StartIndex", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18StartIndex), 4, 0)));
         }
         else
         {
            AV18StartIndex = (short)(AV16Length-AV17MaxLength);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18StartIndex", StringUtil.LTrim( StringUtil.Str( (decimal)(AV18StartIndex), 4, 0)));
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV8FormCaption = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
         AV19IncludeInStack = (bool)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19IncludeInStack", AV19IncludeInStack);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0E2( ) ;
         WS0E2( ) ;
         WE0E2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV8FormCaption = (String)((String)getParm(obj,0));
         sCtrlAV19IncludeInStack = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA0E2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "k2brecentlinksmodern", GetJustCreated( ));
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA0E2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV8FormCaption = (String)getParm(obj,2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
            AV19IncludeInStack = (bool)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19IncludeInStack", AV19IncludeInStack);
         }
         wcpOAV8FormCaption = cgiGet( sPrefix+"wcpOAV8FormCaption");
         wcpOAV19IncludeInStack = StringUtil.StrToBool( cgiGet( sPrefix+"wcpOAV19IncludeInStack"));
         if ( ! GetJustCreated( ) && ( ( StringUtil.StrCmp(AV8FormCaption, wcpOAV8FormCaption) != 0 ) || ( AV19IncludeInStack != wcpOAV19IncludeInStack ) ) )
         {
            setjustcreated();
         }
         wcpOAV8FormCaption = AV8FormCaption;
         wcpOAV19IncludeInStack = AV19IncludeInStack;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV8FormCaption = cgiGet( sPrefix+"AV8FormCaption_CTRL");
         if ( StringUtil.Len( sCtrlAV8FormCaption) > 0 )
         {
            AV8FormCaption = cgiGet( sCtrlAV8FormCaption);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
         }
         else
         {
            AV8FormCaption = cgiGet( sPrefix+"AV8FormCaption_PARM");
         }
         sCtrlAV19IncludeInStack = cgiGet( sPrefix+"AV19IncludeInStack_CTRL");
         if ( StringUtil.Len( sCtrlAV19IncludeInStack) > 0 )
         {
            AV19IncludeInStack = StringUtil.StrToBool( cgiGet( sCtrlAV19IncludeInStack));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV19IncludeInStack", AV19IncludeInStack);
         }
         else
         {
            AV19IncludeInStack = StringUtil.StrToBool( cgiGet( sPrefix+"AV19IncludeInStack_PARM"));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA0E2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS0E2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS0E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV8FormCaption_PARM", StringUtil.RTrim( AV8FormCaption));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV8FormCaption)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV8FormCaption_CTRL", StringUtil.RTrim( sCtrlAV8FormCaption));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV19IncludeInStack_PARM", StringUtil.BoolToStr( AV19IncludeInStack));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV19IncludeInStack)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV19IncludeInStack_CTRL", StringUtil.RTrim( sCtrlAV19IncludeInStack));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE0E2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), false);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171523280", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("k2brecentlinksmodern.js", "?201811171523281", false);
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_22( )
      {
         lblK2brecentlinkseparator_Internalname = sPrefix+"K2BRECENTLINKSEPARATOR_"+sGXsfl_2_idx;
         lblK2brecentlink_Internalname = sPrefix+"K2BRECENTLINK_"+sGXsfl_2_idx;
      }

      protected void SubsflControlProps_fel_22( )
      {
         lblK2brecentlinkseparator_Internalname = sPrefix+"K2BRECENTLINKSEPARATOR_"+sGXsfl_2_fel_idx;
         lblK2brecentlink_Internalname = sPrefix+"K2BRECENTLINK_"+sGXsfl_2_fel_idx;
      }

      protected void sendrow_22( )
      {
         SubsflControlProps_22( ) ;
         WB0E0( ) ;
         K2bfreestylerecentlinksRow = GXWebRow.GetNew(context,K2bfreestylerecentlinksContainer);
         if ( subK2bfreestylerecentlinks_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subK2bfreestylerecentlinks_Backstyle = 0;
            if ( StringUtil.StrCmp(subK2bfreestylerecentlinks_Class, "") != 0 )
            {
               subK2bfreestylerecentlinks_Linesclass = subK2bfreestylerecentlinks_Class+"Odd";
            }
         }
         else if ( subK2bfreestylerecentlinks_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subK2bfreestylerecentlinks_Backstyle = 0;
            subK2bfreestylerecentlinks_Backcolor = subK2bfreestylerecentlinks_Allbackcolor;
            if ( StringUtil.StrCmp(subK2bfreestylerecentlinks_Class, "") != 0 )
            {
               subK2bfreestylerecentlinks_Linesclass = subK2bfreestylerecentlinks_Class+"Uniform";
            }
         }
         else if ( subK2bfreestylerecentlinks_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subK2bfreestylerecentlinks_Backstyle = 1;
            if ( StringUtil.StrCmp(subK2bfreestylerecentlinks_Class, "") != 0 )
            {
               subK2bfreestylerecentlinks_Linesclass = subK2bfreestylerecentlinks_Class+"Odd";
            }
            subK2bfreestylerecentlinks_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subK2bfreestylerecentlinks_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subK2bfreestylerecentlinks_Backstyle = 1;
            if ( ((int)(((nGXsfl_2_idx-1)/ (decimal)(5)) % (2))) == 0 )
            {
               subK2bfreestylerecentlinks_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subK2bfreestylerecentlinks_Class, "") != 0 )
               {
                  subK2bfreestylerecentlinks_Linesclass = subK2bfreestylerecentlinks_Class+"Odd";
               }
            }
            else
            {
               subK2bfreestylerecentlinks_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subK2bfreestylerecentlinks_Class, "") != 0 )
               {
                  subK2bfreestylerecentlinks_Linesclass = subK2bfreestylerecentlinks_Class+"Even";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            if ( ( 5 == 0 ) && ( nGXsfl_2_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subK2bfreestylerecentlinks_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_2_idx+"\">") ;
            }
            if ( 5 > 0 )
            {
               if ( ( 5 == 1 ) || ( ((int)((nGXsfl_2_idx) % (5))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subK2bfreestylerecentlinks_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_2_idx+"\">") ;
               }
            }
         }
         K2bfreestylerecentlinksRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)subK2bfreestylerecentlinks_Linesclass,(String)""});
         K2bfreestylerecentlinksRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Table start */
         K2bfreestylerecentlinksRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblK2brecentlinkcontainer_Internalname+"_"+sGXsfl_2_idx,(short)1,(String)"Table",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         K2bfreestylerecentlinksRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         K2bfreestylerecentlinksRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         K2bfreestylerecentlinksRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblK2brecentlinkseparator_Internalname,(String)lblK2brecentlinkseparator_Caption,(String)"",(String)"",(String)lblK2brecentlinkseparator_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock_RecentLink",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            K2bfreestylerecentlinksContainer.CloseTag("cell");
         }
         K2bfreestylerecentlinksRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Text block */
         K2bfreestylerecentlinksRow.AddColumnProperties("label", 1, isAjaxCallMode( ), new Object[] {(String)lblK2brecentlink_Internalname,(String)lblK2brecentlink_Caption,(String)lblK2brecentlink_Link,(String)"",(String)lblK2brecentlink_Jsonclick,(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)"",(String)"TextBlock_RecentLink",(short)0,(String)"",(short)1,(short)1,(short)0});
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            K2bfreestylerecentlinksContainer.CloseTag("cell");
         }
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            K2bfreestylerecentlinksContainer.CloseTag("row");
         }
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            K2bfreestylerecentlinksContainer.CloseTag("table");
         }
         /* End of table */
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            K2bfreestylerecentlinksContainer.CloseTag("cell");
         }
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            K2bfreestylerecentlinksContainer.CloseTag("row");
         }
         send_integrity_lvl_hashes0E2( ) ;
         /* End of Columns property logic. */
         if ( K2bfreestylerecentlinksContainer.GetWrapped() == 1 )
         {
            if ( 5 > 0 )
            {
               if ( ((int)((nGXsfl_2_idx) % (5))) == 0 )
               {
                  context.WriteHtmlTextNl( "</tr>") ;
               }
            }
         }
         K2bfreestylerecentlinksContainer.AddRow(K2bfreestylerecentlinksRow);
         nGXsfl_2_idx = (short)(((subK2bfreestylerecentlinks_Islastpage==1)&&(nGXsfl_2_idx+1>subK2bfreestylerecentlinks_Recordsperpage( )) ? 1 : nGXsfl_2_idx+1));
         sGXsfl_2_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_2_idx), 4, 0)), 4, "0");
         SubsflControlProps_22( ) ;
         /* End function sendrow_22 */
      }

      protected void init_default_properties( )
      {
         lblK2brecentlinkseparator_Internalname = sPrefix+"K2BRECENTLINKSEPARATOR";
         lblK2brecentlink_Internalname = sPrefix+"K2BRECENTLINK";
         tblK2brecentlinkcontainer_Internalname = sPrefix+"K2BRECENTLINKCONTAINER";
         Form.Internalname = sPrefix+"FORM";
         subK2bfreestylerecentlinks_Internalname = sPrefix+"K2BFREESTYLERECENTLINKS";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         lblK2brecentlink_Link = "";
         lblK2brecentlink_Caption = " ";
         lblK2brecentlinkseparator_Caption = " ";
         subK2bfreestylerecentlinks_Class = "FreeStyleGrid";
         subK2bfreestylerecentlinks_Allowcollapsing = 0;
         lblK2brecentlinkseparator_Caption = " ";
         subK2bfreestylerecentlinks_Borderwidth = 0;
         subK2bfreestylerecentlinks_Backcolorstyle = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'K2BFREESTYLERECENTLINKS_nFirstRecordOnPage',nv:0},{av:'K2BFREESTYLERECENTLINKS_nEOF',nv:0},{av:'AV8FormCaption',fld:'vFORMCAPTION',pic:'',nv:''},{av:'AV19IncludeInStack',fld:'vINCLUDEINSTACK',pic:'',nv:false},{av:'AV18StartIndex',fld:'vSTARTINDEX',pic:'ZZZ9',nv:0},{av:'AV16Length',fld:'vLENGTH',pic:'ZZZ9',nv:0},{av:'sPrefix',nv:''}],oparms:[]}");
         setEventMetadata("K2BFREESTYLERECENTLINKS.LOAD","{handler:'E110E2',iparms:[{av:'AV8FormCaption',fld:'vFORMCAPTION',pic:'',nv:''},{av:'AV19IncludeInStack',fld:'vINCLUDEINSTACK',pic:'',nv:false},{av:'AV18StartIndex',fld:'vSTARTINDEX',pic:'ZZZ9',nv:0},{av:'AV16Length',fld:'vLENGTH',pic:'ZZZ9',nv:0}],oparms:[{av:'AV16Length',fld:'vLENGTH',pic:'ZZZ9',nv:0},{av:'lblK2brecentlink_Caption',ctrl:'K2BRECENTLINK',prop:'Caption'},{av:'lblK2brecentlink_Link',ctrl:'K2BRECENTLINK',prop:'Link'},{av:'lblK2brecentlinkseparator_Caption',ctrl:'K2BRECENTLINKSEPARATOR',prop:'Caption'},{av:'AV18StartIndex',fld:'vSTARTINDEX',pic:'ZZZ9',nv:0}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV8FormCaption = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         K2bfreestylerecentlinksContainer = new GXWebGrid( context);
         sStyleString = "";
         K2bfreestylerecentlinksColumn = new GXWebColumn();
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV12Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2");
         AV14StackItem = new SdtK2BStack_K2BStackItem(context);
         K2bfreestylerecentlinksRow = new GXWebRow();
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV8FormCaption = "";
         sCtrlAV19IncludeInStack = "";
         subK2bfreestylerecentlinks_Linesclass = "";
         lblK2brecentlinkseparator_Jsonclick = "";
         lblK2brecentlink_Jsonclick = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_2 ;
      private short nGXsfl_2_idx=1 ;
      private short AV18StartIndex ;
      private short AV16Length ;
      private short initialized ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short subK2bfreestylerecentlinks_Backcolorstyle ;
      private short subK2bfreestylerecentlinks_Borderwidth ;
      private short subK2bfreestylerecentlinks_Allowselection ;
      private short subK2bfreestylerecentlinks_Allowhovering ;
      private short subK2bfreestylerecentlinks_Allowcollapsing ;
      private short subK2bfreestylerecentlinks_Collapsed ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV17MaxLength ;
      private short subK2bfreestylerecentlinks_Backstyle ;
      private short K2BFREESTYLERECENTLINKS_nEOF ;
      private int subK2bfreestylerecentlinks_Selectioncolor ;
      private int subK2bfreestylerecentlinks_Hoveringcolor ;
      private int subK2bfreestylerecentlinks_Islastpage ;
      private int AV10Index ;
      private int GXt_int1 ;
      private int idxLst ;
      private int subK2bfreestylerecentlinks_Backcolor ;
      private int subK2bfreestylerecentlinks_Allbackcolor ;
      private long K2BFREESTYLERECENTLINKS_nCurrentRecord ;
      private long K2BFREESTYLERECENTLINKS_nFirstRecordOnPage ;
      private String AV8FormCaption ;
      private String wcpOAV8FormCaption ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_2_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sStyleString ;
      private String subK2bfreestylerecentlinks_Internalname ;
      private String lblK2brecentlinkseparator_Caption ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String lblK2brecentlink_Caption ;
      private String lblK2brecentlink_Link ;
      private String sCtrlAV8FormCaption ;
      private String sCtrlAV19IncludeInStack ;
      private String lblK2brecentlinkseparator_Internalname ;
      private String lblK2brecentlink_Internalname ;
      private String sGXsfl_2_fel_idx="0001" ;
      private String subK2bfreestylerecentlinks_Class ;
      private String subK2bfreestylerecentlinks_Linesclass ;
      private String tblK2brecentlinkcontainer_Internalname ;
      private String lblK2brecentlinkseparator_Jsonclick ;
      private String lblK2brecentlink_Jsonclick ;
      private bool AV19IncludeInStack ;
      private bool wcpOAV19IncludeInStack ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_2_Refreshing=false ;
      private bool AV7FirstRecentLink ;
      private bool returnInSub ;
      private GXWebGrid K2bfreestylerecentlinksContainer ;
      private GXWebRow K2bfreestylerecentlinksRow ;
      private GXWebColumn K2bfreestylerecentlinksColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> AV12Stack ;
      private SdtK2BStack_K2BStackItem AV14StackItem ;
   }

}
