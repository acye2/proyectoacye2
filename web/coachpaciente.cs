/*
               File: CoachPaciente
        Description: Coach Paciente
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:40.85
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class coachpaciente : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_5") == 0 )
         {
            A12CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_5( A12CoachPaciente_Coach) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_6") == 0 )
         {
            A13CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_6( A13CoachPaciente_Paciente) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Coach Paciente", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtCoachPaciente_Coach_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("K2BFlat");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public coachpaciente( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public coachpaciente( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "coachpaciente_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablemain_Internalname, 1, 0, "px", 0, "px", "Container FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Coach Paciente", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "|<", bttBtn_first_Jsonclick, 5, "|<", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "<", bttBtn_previous_Jsonclick, 5, "<", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", ">", bttBtn_next_Jsonclick, 5, ">", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", ">|", bttBtn_last_Jsonclick, 5, ">|", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0020.aspx"+"',["+"{Ctrl:gx.dom.el('"+"COACHPACIENTE_COACH"+"'), id:'"+"COACHPACIENTE_COACH"+"'"+",IOType:'out'}"+","+"{Ctrl:gx.dom.el('"+"COACHPACIENTE_PACIENTE"+"'), id:'"+"COACHPACIENTE_PACIENTE"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_Coach_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_Coach_Internalname, "Paciente_Coach", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_Coach_Internalname, A12CoachPaciente_Coach.ToString(), A12CoachPaciente_Coach.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_Coach_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_Coach_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_CoachPaciente.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_12_Internalname, sImgUrl, imgprompt_12_Link, "", "", context.GetTheme( ), imgprompt_12_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_CoachPNombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_CoachPNombre_Internalname, "Paciente_Coach PNombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_CoachPNombre_Internalname, A14CoachPaciente_CoachPNombre, StringUtil.RTrim( context.localUtil.Format( A14CoachPaciente_CoachPNombre, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_CoachPNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_CoachPNombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_CoachPApellido_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_CoachPApellido_Internalname, "Paciente_Coach PApellido", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_CoachPApellido_Internalname, A15CoachPaciente_CoachPApellido, StringUtil.RTrim( context.localUtil.Format( A15CoachPaciente_CoachPApellido, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_CoachPApellido_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_CoachPApellido_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_Paciente_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_Paciente_Internalname, "Paciente_Paciente", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_Paciente_Internalname, A13CoachPaciente_Paciente.ToString(), A13CoachPaciente_Paciente.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_Paciente_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_Paciente_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_CoachPaciente.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_13_Internalname, sImgUrl, imgprompt_13_Link, "", "", context.GetTheme( ), imgprompt_13_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_PacientePNombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_PacientePNombre_Internalname, "Paciente_Paciente PNombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_PacientePNombre_Internalname, A16CoachPaciente_PacientePNombre, StringUtil.RTrim( context.localUtil.Format( A16CoachPaciente_PacientePNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_PacientePNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_PacientePNombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_PacientePApellid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_PacientePApellid_Internalname, "Paciente_Paciente PApellido", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_PacientePApellid_Internalname, A17CoachPaciente_PacientePApellid, StringUtil.RTrim( context.localUtil.Format( A17CoachPaciente_PacientePApellid, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_PacientePApellid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_PacientePApellid_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_FechaAsignacion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_FechaAsignacion_Internalname, "Paciente_Fecha Asignacion", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtCoachPaciente_FechaAsignacion_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_FechaAsignacion_Internalname, context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"), context.localUtil.Format( A18CoachPaciente_FechaAsignacion, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_FechaAsignacion_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_FechaAsignacion_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_CoachPaciente.htm");
            GxWebStd.gx_bitmap( context, edtCoachPaciente_FechaAsignacion_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtCoachPaciente_FechaAsignacion_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_CoachPaciente.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_DispositivoID_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_DispositivoID_Internalname, "Paciente_Dispositivo ID", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_DispositivoID_Internalname, A19CoachPaciente_DispositivoID, StringUtil.RTrim( context.localUtil.Format( A19CoachPaciente_DispositivoID, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_DispositivoID_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_DispositivoID_Enabled, 0, "text", "", 75, "chr", 1, "row", 75, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtCoachPaciente_Token_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtCoachPaciente_Token_Internalname, "Paciente_Token", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtCoachPaciente_Token_Internalname, A20CoachPaciente_Token, StringUtil.RTrim( context.localUtil.Format( A20CoachPaciente_Token, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtCoachPaciente_Token_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtCoachPaciente_Token_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_CoachPaciente.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( StringUtil.StrCmp(cgiGet( edtCoachPaciente_Coach_Internalname), "") == 0 )
               {
                  A12CoachPaciente_Coach = (Guid)(Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
               }
               else
               {
                  try
                  {
                     A12CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( cgiGet( edtCoachPaciente_Coach_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "COACHPACIENTE_COACH");
                     AnyError = 1;
                     GX_FocusControl = edtCoachPaciente_Coach_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               A14CoachPaciente_CoachPNombre = cgiGet( edtCoachPaciente_CoachPNombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14CoachPaciente_CoachPNombre", A14CoachPaciente_CoachPNombre);
               A15CoachPaciente_CoachPApellido = cgiGet( edtCoachPaciente_CoachPApellido_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15CoachPaciente_CoachPApellido", A15CoachPaciente_CoachPApellido);
               if ( StringUtil.StrCmp(cgiGet( edtCoachPaciente_Paciente_Internalname), "") == 0 )
               {
                  A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
               }
               else
               {
                  try
                  {
                     A13CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtCoachPaciente_Paciente_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "COACHPACIENTE_PACIENTE");
                     AnyError = 1;
                     GX_FocusControl = edtCoachPaciente_Paciente_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               A16CoachPaciente_PacientePNombre = cgiGet( edtCoachPaciente_PacientePNombre_Internalname);
               n16CoachPaciente_PacientePNombre = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16CoachPaciente_PacientePNombre", A16CoachPaciente_PacientePNombre);
               A17CoachPaciente_PacientePApellid = cgiGet( edtCoachPaciente_PacientePApellid_Internalname);
               n17CoachPaciente_PacientePApellid = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17CoachPaciente_PacientePApellid", A17CoachPaciente_PacientePApellid);
               if ( context.localUtil.VCDate( cgiGet( edtCoachPaciente_FechaAsignacion_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Coach Paciente_Fecha Asignacion"}), 1, "COACHPACIENTE_FECHAASIGNACION");
                  AnyError = 1;
                  GX_FocusControl = edtCoachPaciente_FechaAsignacion_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
                  n18CoachPaciente_FechaAsignacion = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18CoachPaciente_FechaAsignacion", context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"));
               }
               else
               {
                  A18CoachPaciente_FechaAsignacion = context.localUtil.CToD( cgiGet( edtCoachPaciente_FechaAsignacion_Internalname), 2);
                  n18CoachPaciente_FechaAsignacion = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18CoachPaciente_FechaAsignacion", context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"));
               }
               n18CoachPaciente_FechaAsignacion = ((DateTime.MinValue==A18CoachPaciente_FechaAsignacion) ? true : false);
               A19CoachPaciente_DispositivoID = cgiGet( edtCoachPaciente_DispositivoID_Internalname);
               n19CoachPaciente_DispositivoID = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19CoachPaciente_DispositivoID", A19CoachPaciente_DispositivoID);
               n19CoachPaciente_DispositivoID = (String.IsNullOrEmpty(StringUtil.RTrim( A19CoachPaciente_DispositivoID)) ? true : false);
               A20CoachPaciente_Token = cgiGet( edtCoachPaciente_Token_Internalname);
               n20CoachPaciente_Token = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20CoachPaciente_Token", A20CoachPaciente_Token);
               n20CoachPaciente_Token = (String.IsNullOrEmpty(StringUtil.RTrim( A20CoachPaciente_Token)) ? true : false);
               /* Read saved values. */
               Z12CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( cgiGet( "Z12CoachPaciente_Coach")));
               Z13CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( cgiGet( "Z13CoachPaciente_Paciente")));
               Z14CoachPaciente_CoachPNombre = cgiGet( "Z14CoachPaciente_CoachPNombre");
               Z15CoachPaciente_CoachPApellido = cgiGet( "Z15CoachPaciente_CoachPApellido");
               Z18CoachPaciente_FechaAsignacion = context.localUtil.CToD( cgiGet( "Z18CoachPaciente_FechaAsignacion"), 0);
               n18CoachPaciente_FechaAsignacion = ((DateTime.MinValue==A18CoachPaciente_FechaAsignacion) ? true : false);
               Z19CoachPaciente_DispositivoID = cgiGet( "Z19CoachPaciente_DispositivoID");
               n19CoachPaciente_DispositivoID = (String.IsNullOrEmpty(StringUtil.RTrim( A19CoachPaciente_DispositivoID)) ? true : false);
               Z20CoachPaciente_Token = cgiGet( "Z20CoachPaciente_Token");
               n20CoachPaciente_Token = (String.IsNullOrEmpty(StringUtil.RTrim( A20CoachPaciente_Token)) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A12CoachPaciente_Coach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
                  A13CoachPaciente_Paciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
                  getEqualNoModal( ) ;
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll022( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes022( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption020( )
      {
      }

      protected void ZM022( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z14CoachPaciente_CoachPNombre = T00023_A14CoachPaciente_CoachPNombre[0];
               Z15CoachPaciente_CoachPApellido = T00023_A15CoachPaciente_CoachPApellido[0];
               Z18CoachPaciente_FechaAsignacion = T00023_A18CoachPaciente_FechaAsignacion[0];
               Z19CoachPaciente_DispositivoID = T00023_A19CoachPaciente_DispositivoID[0];
               Z20CoachPaciente_Token = T00023_A20CoachPaciente_Token[0];
            }
            else
            {
               Z14CoachPaciente_CoachPNombre = A14CoachPaciente_CoachPNombre;
               Z15CoachPaciente_CoachPApellido = A15CoachPaciente_CoachPApellido;
               Z18CoachPaciente_FechaAsignacion = A18CoachPaciente_FechaAsignacion;
               Z19CoachPaciente_DispositivoID = A19CoachPaciente_DispositivoID;
               Z20CoachPaciente_Token = A20CoachPaciente_Token;
            }
         }
         if ( GX_JID == -4 )
         {
            Z14CoachPaciente_CoachPNombre = A14CoachPaciente_CoachPNombre;
            Z15CoachPaciente_CoachPApellido = A15CoachPaciente_CoachPApellido;
            Z18CoachPaciente_FechaAsignacion = A18CoachPaciente_FechaAsignacion;
            Z19CoachPaciente_DispositivoID = A19CoachPaciente_DispositivoID;
            Z20CoachPaciente_Token = A20CoachPaciente_Token;
            Z12CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
            Z13CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
            Z16CoachPaciente_PacientePNombre = A16CoachPaciente_PacientePNombre;
            Z17CoachPaciente_PacientePApellid = A17CoachPaciente_PacientePApellid;
         }
      }

      protected void standaloneNotModal( )
      {
         imgprompt_12_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"COACHPACIENTE_COACH"+"'), id:'"+"COACHPACIENTE_COACH"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"true"+");");
         imgprompt_13_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"COACHPACIENTE_PACIENTE"+"'), id:'"+"COACHPACIENTE_PACIENTE"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");");
      }

      protected void standaloneModal( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
      }

      protected void Load022( )
      {
         /* Using cursor T00026 */
         pr_default.execute(4, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound2 = 1;
            A14CoachPaciente_CoachPNombre = T00026_A14CoachPaciente_CoachPNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14CoachPaciente_CoachPNombre", A14CoachPaciente_CoachPNombre);
            A15CoachPaciente_CoachPApellido = T00026_A15CoachPaciente_CoachPApellido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15CoachPaciente_CoachPApellido", A15CoachPaciente_CoachPApellido);
            A16CoachPaciente_PacientePNombre = T00026_A16CoachPaciente_PacientePNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16CoachPaciente_PacientePNombre", A16CoachPaciente_PacientePNombre);
            n16CoachPaciente_PacientePNombre = T00026_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = T00026_A17CoachPaciente_PacientePApellid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17CoachPaciente_PacientePApellid", A17CoachPaciente_PacientePApellid);
            n17CoachPaciente_PacientePApellid = T00026_n17CoachPaciente_PacientePApellid[0];
            A18CoachPaciente_FechaAsignacion = T00026_A18CoachPaciente_FechaAsignacion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18CoachPaciente_FechaAsignacion", context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"));
            n18CoachPaciente_FechaAsignacion = T00026_n18CoachPaciente_FechaAsignacion[0];
            A19CoachPaciente_DispositivoID = T00026_A19CoachPaciente_DispositivoID[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19CoachPaciente_DispositivoID", A19CoachPaciente_DispositivoID);
            n19CoachPaciente_DispositivoID = T00026_n19CoachPaciente_DispositivoID[0];
            A20CoachPaciente_Token = T00026_A20CoachPaciente_Token[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20CoachPaciente_Token", A20CoachPaciente_Token);
            n20CoachPaciente_Token = T00026_n20CoachPaciente_Token[0];
            ZM022( -4) ;
         }
         pr_default.close(4);
         OnLoadActions022( ) ;
      }

      protected void OnLoadActions022( )
      {
      }

      protected void CheckExtendedTable022( )
      {
         Gx_BScreen = 1;
         standaloneModal( ) ;
         /* Using cursor T00024 */
         pr_default.execute(2, new Object[] {A12CoachPaciente_Coach});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Coach ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_COACH");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(2);
         /* Using cursor T00025 */
         pr_default.execute(3, new Object[] {A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Paciente ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_PACIENTE");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Paciente_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A16CoachPaciente_PacientePNombre = T00025_A16CoachPaciente_PacientePNombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16CoachPaciente_PacientePNombre", A16CoachPaciente_PacientePNombre);
         n16CoachPaciente_PacientePNombre = T00025_n16CoachPaciente_PacientePNombre[0];
         A17CoachPaciente_PacientePApellid = T00025_A17CoachPaciente_PacientePApellid[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17CoachPaciente_PacientePApellid", A17CoachPaciente_PacientePApellid);
         n17CoachPaciente_PacientePApellid = T00025_n17CoachPaciente_PacientePApellid[0];
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A18CoachPaciente_FechaAsignacion) || ( A18CoachPaciente_FechaAsignacion >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Coach Paciente_Fecha Asignacion fuera de rango", "OutOfRange", 1, "COACHPACIENTE_FECHAASIGNACION");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_FechaAsignacion_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors022( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_5( Guid A12CoachPaciente_Coach )
      {
         /* Using cursor T00027 */
         pr_default.execute(5, new Object[] {A12CoachPaciente_Coach});
         if ( (pr_default.getStatus(5) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Coach ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_COACH");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_6( Guid A13CoachPaciente_Paciente )
      {
         /* Using cursor T00028 */
         pr_default.execute(6, new Object[] {A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(6) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Paciente ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_PACIENTE");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Paciente_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A16CoachPaciente_PacientePNombre = T00028_A16CoachPaciente_PacientePNombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16CoachPaciente_PacientePNombre", A16CoachPaciente_PacientePNombre);
         n16CoachPaciente_PacientePNombre = T00028_n16CoachPaciente_PacientePNombre[0];
         A17CoachPaciente_PacientePApellid = T00028_A17CoachPaciente_PacientePApellid[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17CoachPaciente_PacientePApellid", A17CoachPaciente_PacientePApellid);
         n17CoachPaciente_PacientePApellid = T00028_n17CoachPaciente_PacientePApellid[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A16CoachPaciente_PacientePNombre)+"\""+","+"\""+GXUtil.EncodeJSConstant( A17CoachPaciente_PacientePApellid)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey022( )
      {
         /* Using cursor T00029 */
         pr_default.execute(7, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound2 = 1;
         }
         else
         {
            RcdFound2 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00023 */
         pr_default.execute(1, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM022( 4) ;
            RcdFound2 = 1;
            A14CoachPaciente_CoachPNombre = T00023_A14CoachPaciente_CoachPNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14CoachPaciente_CoachPNombre", A14CoachPaciente_CoachPNombre);
            A15CoachPaciente_CoachPApellido = T00023_A15CoachPaciente_CoachPApellido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15CoachPaciente_CoachPApellido", A15CoachPaciente_CoachPApellido);
            A18CoachPaciente_FechaAsignacion = T00023_A18CoachPaciente_FechaAsignacion[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18CoachPaciente_FechaAsignacion", context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"));
            n18CoachPaciente_FechaAsignacion = T00023_n18CoachPaciente_FechaAsignacion[0];
            A19CoachPaciente_DispositivoID = T00023_A19CoachPaciente_DispositivoID[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19CoachPaciente_DispositivoID", A19CoachPaciente_DispositivoID);
            n19CoachPaciente_DispositivoID = T00023_n19CoachPaciente_DispositivoID[0];
            A20CoachPaciente_Token = T00023_A20CoachPaciente_Token[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20CoachPaciente_Token", A20CoachPaciente_Token);
            n20CoachPaciente_Token = T00023_n20CoachPaciente_Token[0];
            A12CoachPaciente_Coach = (Guid)((Guid)(T00023_A12CoachPaciente_Coach[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
            A13CoachPaciente_Paciente = (Guid)((Guid)(T00023_A13CoachPaciente_Paciente[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
            Z12CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
            Z13CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load022( ) ;
            if ( AnyError == 1 )
            {
               RcdFound2 = 0;
               InitializeNonKey022( ) ;
            }
            Gx_mode = sMode2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound2 = 0;
            InitializeNonKey022( ) ;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode2;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey022( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound2 = 0;
         /* Using cursor T000210 */
         pr_default.execute(8, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( GuidUtil.Compare(T000210_A12CoachPaciente_Coach[0], A12CoachPaciente_Coach, 1) < 0 ) || ( T000210_A12CoachPaciente_Coach[0] == A12CoachPaciente_Coach ) && ( GuidUtil.Compare(T000210_A13CoachPaciente_Paciente[0], A13CoachPaciente_Paciente, 1) < 0 ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( GuidUtil.Compare(T000210_A12CoachPaciente_Coach[0], A12CoachPaciente_Coach, 1) > 0 ) || ( T000210_A12CoachPaciente_Coach[0] == A12CoachPaciente_Coach ) && ( GuidUtil.Compare(T000210_A13CoachPaciente_Paciente[0], A13CoachPaciente_Paciente, 1) > 0 ) ) )
            {
               A12CoachPaciente_Coach = (Guid)((Guid)(T000210_A12CoachPaciente_Coach[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
               A13CoachPaciente_Paciente = (Guid)((Guid)(T000210_A13CoachPaciente_Paciente[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
               RcdFound2 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound2 = 0;
         /* Using cursor T000211 */
         pr_default.execute(9, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( GuidUtil.Compare(T000211_A12CoachPaciente_Coach[0], A12CoachPaciente_Coach, 1) > 0 ) || ( T000211_A12CoachPaciente_Coach[0] == A12CoachPaciente_Coach ) && ( GuidUtil.Compare(T000211_A13CoachPaciente_Paciente[0], A13CoachPaciente_Paciente, 1) > 0 ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( GuidUtil.Compare(T000211_A12CoachPaciente_Coach[0], A12CoachPaciente_Coach, 1) < 0 ) || ( T000211_A12CoachPaciente_Coach[0] == A12CoachPaciente_Coach ) && ( GuidUtil.Compare(T000211_A13CoachPaciente_Paciente[0], A13CoachPaciente_Paciente, 1) < 0 ) ) )
            {
               A12CoachPaciente_Coach = (Guid)((Guid)(T000211_A12CoachPaciente_Coach[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
               A13CoachPaciente_Paciente = (Guid)((Guid)(T000211_A13CoachPaciente_Paciente[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
               RcdFound2 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey022( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert022( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound2 == 1 )
            {
               if ( ( A12CoachPaciente_Coach != Z12CoachPaciente_Coach ) || ( A13CoachPaciente_Paciente != Z13CoachPaciente_Paciente ) )
               {
                  A12CoachPaciente_Coach = (Guid)(Z12CoachPaciente_Coach);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
                  A13CoachPaciente_Paciente = (Guid)(Z13CoachPaciente_Paciente);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "COACHPACIENTE_COACH");
                  AnyError = 1;
                  GX_FocusControl = edtCoachPaciente_Coach_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtCoachPaciente_Coach_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update022( ) ;
                  GX_FocusControl = edtCoachPaciente_Coach_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( ( A12CoachPaciente_Coach != Z12CoachPaciente_Coach ) || ( A13CoachPaciente_Paciente != Z13CoachPaciente_Paciente ) )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtCoachPaciente_Coach_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert022( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "COACHPACIENTE_COACH");
                     AnyError = 1;
                     GX_FocusControl = edtCoachPaciente_Coach_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtCoachPaciente_Coach_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert022( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( ( A12CoachPaciente_Coach != Z12CoachPaciente_Coach ) || ( A13CoachPaciente_Paciente != Z13CoachPaciente_Paciente ) )
         {
            A12CoachPaciente_Coach = (Guid)(Z12CoachPaciente_Coach);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
            A13CoachPaciente_Paciente = (Guid)(Z13CoachPaciente_Paciente);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "COACHPACIENTE_COACH");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "COACHPACIENTE_COACH");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtCoachPaciente_CoachPNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart022( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCoachPaciente_CoachPNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd022( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCoachPaciente_CoachPNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCoachPaciente_CoachPNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart022( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound2 != 0 )
            {
               ScanNext022( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtCoachPaciente_CoachPNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd022( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency022( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00022 */
            pr_default.execute(0, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CoachPaciente"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z14CoachPaciente_CoachPNombre, T00022_A14CoachPaciente_CoachPNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z15CoachPaciente_CoachPApellido, T00022_A15CoachPaciente_CoachPApellido[0]) != 0 ) || ( Z18CoachPaciente_FechaAsignacion != T00022_A18CoachPaciente_FechaAsignacion[0] ) || ( StringUtil.StrCmp(Z19CoachPaciente_DispositivoID, T00022_A19CoachPaciente_DispositivoID[0]) != 0 ) || ( StringUtil.StrCmp(Z20CoachPaciente_Token, T00022_A20CoachPaciente_Token[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z14CoachPaciente_CoachPNombre, T00022_A14CoachPaciente_CoachPNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("coachpaciente:[seudo value changed for attri]"+"CoachPaciente_CoachPNombre");
                  GXUtil.WriteLogRaw("Old: ",Z14CoachPaciente_CoachPNombre);
                  GXUtil.WriteLogRaw("Current: ",T00022_A14CoachPaciente_CoachPNombre[0]);
               }
               if ( StringUtil.StrCmp(Z15CoachPaciente_CoachPApellido, T00022_A15CoachPaciente_CoachPApellido[0]) != 0 )
               {
                  GXUtil.WriteLog("coachpaciente:[seudo value changed for attri]"+"CoachPaciente_CoachPApellido");
                  GXUtil.WriteLogRaw("Old: ",Z15CoachPaciente_CoachPApellido);
                  GXUtil.WriteLogRaw("Current: ",T00022_A15CoachPaciente_CoachPApellido[0]);
               }
               if ( Z18CoachPaciente_FechaAsignacion != T00022_A18CoachPaciente_FechaAsignacion[0] )
               {
                  GXUtil.WriteLog("coachpaciente:[seudo value changed for attri]"+"CoachPaciente_FechaAsignacion");
                  GXUtil.WriteLogRaw("Old: ",Z18CoachPaciente_FechaAsignacion);
                  GXUtil.WriteLogRaw("Current: ",T00022_A18CoachPaciente_FechaAsignacion[0]);
               }
               if ( StringUtil.StrCmp(Z19CoachPaciente_DispositivoID, T00022_A19CoachPaciente_DispositivoID[0]) != 0 )
               {
                  GXUtil.WriteLog("coachpaciente:[seudo value changed for attri]"+"CoachPaciente_DispositivoID");
                  GXUtil.WriteLogRaw("Old: ",Z19CoachPaciente_DispositivoID);
                  GXUtil.WriteLogRaw("Current: ",T00022_A19CoachPaciente_DispositivoID[0]);
               }
               if ( StringUtil.StrCmp(Z20CoachPaciente_Token, T00022_A20CoachPaciente_Token[0]) != 0 )
               {
                  GXUtil.WriteLog("coachpaciente:[seudo value changed for attri]"+"CoachPaciente_Token");
                  GXUtil.WriteLogRaw("Old: ",Z20CoachPaciente_Token);
                  GXUtil.WriteLogRaw("Current: ",T00022_A20CoachPaciente_Token[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"CoachPaciente"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert022( )
      {
         if ( ! IsAuthorized("coachpaciente_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM022( 0) ;
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000212 */
                     pr_default.execute(10, new Object[] {A14CoachPaciente_CoachPNombre, A15CoachPaciente_CoachPApellido, n18CoachPaciente_FechaAsignacion, A18CoachPaciente_FechaAsignacion, n19CoachPaciente_DispositivoID, A19CoachPaciente_DispositivoID, n20CoachPaciente_Token, A20CoachPaciente_Token, A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption020( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load022( ) ;
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void Update022( )
      {
         if ( ! IsAuthorized("coachpaciente_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000213 */
                     pr_default.execute(11, new Object[] {A14CoachPaciente_CoachPNombre, A15CoachPaciente_CoachPApellido, n18CoachPaciente_FechaAsignacion, A18CoachPaciente_FechaAsignacion, n19CoachPaciente_DispositivoID, A19CoachPaciente_DispositivoID, n20CoachPaciente_Token, A20CoachPaciente_Token, A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CoachPaciente"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate022( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption020( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void DeferredUpdate022( )
      {
      }

      protected void delete( )
      {
         if ( ! IsAuthorized("coachpaciente_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls022( ) ;
            AfterConfirm022( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete022( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000214 */
                  pr_default.execute(12, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound2 == 0 )
                        {
                           InitAll022( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption020( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode2 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel022( ) ;
         Gx_mode = sMode2;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls022( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000215 */
            pr_default.execute(13, new Object[] {A13CoachPaciente_Paciente});
            A16CoachPaciente_PacientePNombre = T000215_A16CoachPaciente_PacientePNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16CoachPaciente_PacientePNombre", A16CoachPaciente_PacientePNombre);
            n16CoachPaciente_PacientePNombre = T000215_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = T000215_A17CoachPaciente_PacientePApellid[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17CoachPaciente_PacientePApellid", A17CoachPaciente_PacientePApellid);
            n17CoachPaciente_PacientePApellid = T000215_n17CoachPaciente_PacientePApellid[0];
            pr_default.close(13);
         }
      }

      protected void EndLevel022( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete022( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_gam.commit( "CoachPaciente");
            pr_default.commit( "CoachPaciente");
            if ( AnyError == 0 )
            {
               ConfirmValues020( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_gam.rollback( "CoachPaciente");
            pr_default.rollback( "CoachPaciente");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart022( )
      {
         /* Using cursor T000216 */
         pr_default.execute(14);
         RcdFound2 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound2 = 1;
            A12CoachPaciente_Coach = (Guid)((Guid)(T000216_A12CoachPaciente_Coach[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
            A13CoachPaciente_Paciente = (Guid)((Guid)(T000216_A13CoachPaciente_Paciente[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext022( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound2 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound2 = 1;
            A12CoachPaciente_Coach = (Guid)((Guid)(T000216_A12CoachPaciente_Coach[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
            A13CoachPaciente_Paciente = (Guid)((Guid)(T000216_A13CoachPaciente_Paciente[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
         }
      }

      protected void ScanEnd022( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm022( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert022( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate022( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete022( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete022( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate022( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes022( )
      {
         edtCoachPaciente_Coach_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_Coach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_Coach_Enabled), 5, 0)), true);
         edtCoachPaciente_CoachPNombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_CoachPNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_CoachPNombre_Enabled), 5, 0)), true);
         edtCoachPaciente_CoachPApellido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_CoachPApellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_CoachPApellido_Enabled), 5, 0)), true);
         edtCoachPaciente_Paciente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_Paciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_Paciente_Enabled), 5, 0)), true);
         edtCoachPaciente_PacientePNombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_PacientePNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_PacientePNombre_Enabled), 5, 0)), true);
         edtCoachPaciente_PacientePApellid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_PacientePApellid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_PacientePApellid_Enabled), 5, 0)), true);
         edtCoachPaciente_FechaAsignacion_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_FechaAsignacion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_FechaAsignacion_Enabled), 5, 0)), true);
         edtCoachPaciente_DispositivoID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_DispositivoID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_DispositivoID_Enabled), 5, 0)), true);
         edtCoachPaciente_Token_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtCoachPaciente_Token_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtCoachPaciente_Token_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes022( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues020( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111816264195", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("coachpaciente.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z12CoachPaciente_Coach", Z12CoachPaciente_Coach.ToString());
         GxWebStd.gx_hidden_field( context, "Z13CoachPaciente_Paciente", Z13CoachPaciente_Paciente.ToString());
         GxWebStd.gx_hidden_field( context, "Z14CoachPaciente_CoachPNombre", Z14CoachPaciente_CoachPNombre);
         GxWebStd.gx_hidden_field( context, "Z15CoachPaciente_CoachPApellido", Z15CoachPaciente_CoachPApellido);
         GxWebStd.gx_hidden_field( context, "Z18CoachPaciente_FechaAsignacion", context.localUtil.DToC( Z18CoachPaciente_FechaAsignacion, 0, "/"));
         GxWebStd.gx_hidden_field( context, "Z19CoachPaciente_DispositivoID", Z19CoachPaciente_DispositivoID);
         GxWebStd.gx_hidden_field( context, "Z20CoachPaciente_Token", Z20CoachPaciente_Token);
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("coachpaciente.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "CoachPaciente" ;
      }

      public override String GetPgmdesc( )
      {
         return "Coach Paciente" ;
      }

      protected void InitializeNonKey022( )
      {
         A14CoachPaciente_CoachPNombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A14CoachPaciente_CoachPNombre", A14CoachPaciente_CoachPNombre);
         A15CoachPaciente_CoachPApellido = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A15CoachPaciente_CoachPApellido", A15CoachPaciente_CoachPApellido);
         A16CoachPaciente_PacientePNombre = "";
         n16CoachPaciente_PacientePNombre = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16CoachPaciente_PacientePNombre", A16CoachPaciente_PacientePNombre);
         A17CoachPaciente_PacientePApellid = "";
         n17CoachPaciente_PacientePApellid = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17CoachPaciente_PacientePApellid", A17CoachPaciente_PacientePApellid);
         A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         n18CoachPaciente_FechaAsignacion = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A18CoachPaciente_FechaAsignacion", context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"));
         n18CoachPaciente_FechaAsignacion = ((DateTime.MinValue==A18CoachPaciente_FechaAsignacion) ? true : false);
         A19CoachPaciente_DispositivoID = "";
         n19CoachPaciente_DispositivoID = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A19CoachPaciente_DispositivoID", A19CoachPaciente_DispositivoID);
         n19CoachPaciente_DispositivoID = (String.IsNullOrEmpty(StringUtil.RTrim( A19CoachPaciente_DispositivoID)) ? true : false);
         A20CoachPaciente_Token = "";
         n20CoachPaciente_Token = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A20CoachPaciente_Token", A20CoachPaciente_Token);
         n20CoachPaciente_Token = (String.IsNullOrEmpty(StringUtil.RTrim( A20CoachPaciente_Token)) ? true : false);
         Z14CoachPaciente_CoachPNombre = "";
         Z15CoachPaciente_CoachPApellido = "";
         Z18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         Z19CoachPaciente_DispositivoID = "";
         Z20CoachPaciente_Token = "";
      }

      protected void InitAll022( )
      {
         A12CoachPaciente_Coach = (Guid)(Guid.Empty);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A12CoachPaciente_Coach", A12CoachPaciente_Coach.ToString());
         A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A13CoachPaciente_Paciente", A13CoachPaciente_Paciente.ToString());
         InitializeNonKey022( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2018111816264199", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("coachpaciente.js", "?201811181626420", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         edtCoachPaciente_Coach_Internalname = "COACHPACIENTE_COACH";
         edtCoachPaciente_CoachPNombre_Internalname = "COACHPACIENTE_COACHPNOMBRE";
         edtCoachPaciente_CoachPApellido_Internalname = "COACHPACIENTE_COACHPAPELLIDO";
         edtCoachPaciente_Paciente_Internalname = "COACHPACIENTE_PACIENTE";
         edtCoachPaciente_PacientePNombre_Internalname = "COACHPACIENTE_PACIENTEPNOMBRE";
         edtCoachPaciente_PacientePApellid_Internalname = "COACHPACIENTE_PACIENTEPAPELLID";
         edtCoachPaciente_FechaAsignacion_Internalname = "COACHPACIENTE_FECHAASIGNACION";
         edtCoachPaciente_DispositivoID_Internalname = "COACHPACIENTE_DISPOSITIVOID";
         edtCoachPaciente_Token_Internalname = "COACHPACIENTE_TOKEN";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_12_Internalname = "PROMPT_12";
         imgprompt_13_Internalname = "PROMPT_13";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Coach Paciente";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtCoachPaciente_Token_Jsonclick = "";
         edtCoachPaciente_Token_Enabled = 1;
         edtCoachPaciente_DispositivoID_Jsonclick = "";
         edtCoachPaciente_DispositivoID_Enabled = 1;
         edtCoachPaciente_FechaAsignacion_Jsonclick = "";
         edtCoachPaciente_FechaAsignacion_Enabled = 1;
         edtCoachPaciente_PacientePApellid_Jsonclick = "";
         edtCoachPaciente_PacientePApellid_Enabled = 0;
         edtCoachPaciente_PacientePNombre_Jsonclick = "";
         edtCoachPaciente_PacientePNombre_Enabled = 0;
         imgprompt_13_Visible = 1;
         imgprompt_13_Link = "";
         edtCoachPaciente_Paciente_Jsonclick = "";
         edtCoachPaciente_Paciente_Enabled = 1;
         edtCoachPaciente_CoachPApellido_Jsonclick = "";
         edtCoachPaciente_CoachPApellido_Enabled = 1;
         edtCoachPaciente_CoachPNombre_Jsonclick = "";
         edtCoachPaciente_CoachPNombre_Enabled = 1;
         imgprompt_12_Visible = 1;
         imgprompt_12_Link = "";
         edtCoachPaciente_Coach_Jsonclick = "";
         edtCoachPaciente_Coach_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         /* Using cursor T000217 */
         pr_default.execute(15, new Object[] {A12CoachPaciente_Coach});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Coach ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_COACH");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         pr_default.close(15);
         /* Using cursor T000215 */
         pr_default.execute(13, new Object[] {A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Paciente ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_PACIENTE");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Paciente_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         A16CoachPaciente_PacientePNombre = T000215_A16CoachPaciente_PacientePNombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A16CoachPaciente_PacientePNombre", A16CoachPaciente_PacientePNombre);
         n16CoachPaciente_PacientePNombre = T000215_n16CoachPaciente_PacientePNombre[0];
         A17CoachPaciente_PacientePApellid = T000215_A17CoachPaciente_PacientePApellid[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A17CoachPaciente_PacientePApellid", A17CoachPaciente_PacientePApellid);
         n17CoachPaciente_PacientePApellid = T000215_n17CoachPaciente_PacientePApellid[0];
         pr_default.close(13);
         GX_FocusControl = edtCoachPaciente_CoachPNombre_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Coachpaciente_coach( Guid GX_Parm1 )
      {
         A12CoachPaciente_Coach = (Guid)(GX_Parm1);
         /* Using cursor T000217 */
         pr_default.execute(15, new Object[] {A12CoachPaciente_Coach});
         if ( (pr_default.getStatus(15) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Coach ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_COACH");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Coach_Internalname;
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Coachpaciente_paciente( Guid GX_Parm1 ,
                                                Guid GX_Parm2 ,
                                                String GX_Parm3 ,
                                                String GX_Parm4 ,
                                                DateTime GX_Parm5 ,
                                                String GX_Parm6 ,
                                                String GX_Parm7 ,
                                                String GX_Parm8 ,
                                                String GX_Parm9 )
      {
         A12CoachPaciente_Coach = (Guid)(GX_Parm1);
         A13CoachPaciente_Paciente = (Guid)(GX_Parm2);
         A14CoachPaciente_CoachPNombre = GX_Parm3;
         A15CoachPaciente_CoachPApellido = GX_Parm4;
         A18CoachPaciente_FechaAsignacion = GX_Parm5;
         n18CoachPaciente_FechaAsignacion = false;
         A19CoachPaciente_DispositivoID = GX_Parm6;
         n19CoachPaciente_DispositivoID = false;
         A20CoachPaciente_Token = GX_Parm7;
         n20CoachPaciente_Token = false;
         A16CoachPaciente_PacientePNombre = GX_Parm8;
         n16CoachPaciente_PacientePNombre = false;
         A17CoachPaciente_PacientePApellid = GX_Parm9;
         n17CoachPaciente_PacientePApellid = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         /* Using cursor T000215 */
         pr_default.execute(13, new Object[] {A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(13) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Paciente ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_PACIENTE");
            AnyError = 1;
            GX_FocusControl = edtCoachPaciente_Paciente_Internalname;
         }
         A16CoachPaciente_PacientePNombre = T000215_A16CoachPaciente_PacientePNombre[0];
         n16CoachPaciente_PacientePNombre = T000215_n16CoachPaciente_PacientePNombre[0];
         A17CoachPaciente_PacientePApellid = T000215_A17CoachPaciente_PacientePApellid[0];
         n17CoachPaciente_PacientePApellid = T000215_n17CoachPaciente_PacientePApellid[0];
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A16CoachPaciente_PacientePNombre = "";
            n16CoachPaciente_PacientePNombre = false;
            A17CoachPaciente_PacientePApellid = "";
            n17CoachPaciente_PacientePApellid = false;
         }
         isValidOutput.Add(A14CoachPaciente_CoachPNombre);
         isValidOutput.Add(A15CoachPaciente_CoachPApellido);
         isValidOutput.Add(context.localUtil.Format(A18CoachPaciente_FechaAsignacion, "99/99/99"));
         isValidOutput.Add(A19CoachPaciente_DispositivoID);
         isValidOutput.Add(A20CoachPaciente_Token);
         isValidOutput.Add(A16CoachPaciente_PacientePNombre);
         isValidOutput.Add(A17CoachPaciente_PacientePApellid);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(Z12CoachPaciente_Coach.ToString());
         isValidOutput.Add(Z13CoachPaciente_Paciente.ToString());
         isValidOutput.Add(Z14CoachPaciente_CoachPNombre);
         isValidOutput.Add(Z15CoachPaciente_CoachPApellido);
         isValidOutput.Add(context.localUtil.DToC( Z18CoachPaciente_FechaAsignacion, 0, "/"));
         isValidOutput.Add(Z19CoachPaciente_DispositivoID);
         isValidOutput.Add(Z20CoachPaciente_Token);
         isValidOutput.Add(Z16CoachPaciente_PacientePNombre);
         isValidOutput.Add(Z17CoachPaciente_PacientePApellid);
         isValidOutput.Add(bttBtn_delete_Enabled);
         isValidOutput.Add(bttBtn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(15);
         pr_default.close(13);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z12CoachPaciente_Coach = (Guid)(Guid.Empty);
         Z13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         Z14CoachPaciente_CoachPNombre = "";
         Z15CoachPaciente_CoachPApellido = "";
         Z18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         Z19CoachPaciente_DispositivoID = "";
         Z20CoachPaciente_Token = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A12CoachPaciente_Coach = (Guid)(Guid.Empty);
         A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         sImgUrl = "";
         A14CoachPaciente_CoachPNombre = "";
         A15CoachPaciente_CoachPApellido = "";
         A16CoachPaciente_PacientePNombre = "";
         A17CoachPaciente_PacientePApellid = "";
         A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         A19CoachPaciente_DispositivoID = "";
         A20CoachPaciente_Token = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z16CoachPaciente_PacientePNombre = "";
         Z17CoachPaciente_PacientePApellid = "";
         T00026_A14CoachPaciente_CoachPNombre = new String[] {""} ;
         T00026_A15CoachPaciente_CoachPApellido = new String[] {""} ;
         T00026_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         T00026_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         T00026_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         T00026_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         T00026_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         T00026_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         T00026_A19CoachPaciente_DispositivoID = new String[] {""} ;
         T00026_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         T00026_A20CoachPaciente_Token = new String[] {""} ;
         T00026_n20CoachPaciente_Token = new bool[] {false} ;
         T00026_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T00026_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         T00024_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T00025_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         T00025_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         T00025_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         T00025_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         T00027_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T00028_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         T00028_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         T00028_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         T00028_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         T00029_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T00029_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         T00023_A14CoachPaciente_CoachPNombre = new String[] {""} ;
         T00023_A15CoachPaciente_CoachPApellido = new String[] {""} ;
         T00023_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         T00023_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         T00023_A19CoachPaciente_DispositivoID = new String[] {""} ;
         T00023_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         T00023_A20CoachPaciente_Token = new String[] {""} ;
         T00023_n20CoachPaciente_Token = new bool[] {false} ;
         T00023_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T00023_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         sMode2 = "";
         T000210_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T000210_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         T000211_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T000211_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         T00022_A14CoachPaciente_CoachPNombre = new String[] {""} ;
         T00022_A15CoachPaciente_CoachPApellido = new String[] {""} ;
         T00022_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         T00022_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         T00022_A19CoachPaciente_DispositivoID = new String[] {""} ;
         T00022_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         T00022_A20CoachPaciente_Token = new String[] {""} ;
         T00022_n20CoachPaciente_Token = new bool[] {false} ;
         T00022_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T00022_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         T000215_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         T000215_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         T000215_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         T000215_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         T000216_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T000216_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         T000217_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         isValidOutput = new GxUnknownObjectCollection();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.coachpaciente__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.coachpaciente__default(),
            new Object[][] {
                new Object[] {
               T00022_A14CoachPaciente_CoachPNombre, T00022_A15CoachPaciente_CoachPApellido, T00022_A18CoachPaciente_FechaAsignacion, T00022_n18CoachPaciente_FechaAsignacion, T00022_A19CoachPaciente_DispositivoID, T00022_n19CoachPaciente_DispositivoID, T00022_A20CoachPaciente_Token, T00022_n20CoachPaciente_Token, T00022_A12CoachPaciente_Coach, T00022_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T00023_A14CoachPaciente_CoachPNombre, T00023_A15CoachPaciente_CoachPApellido, T00023_A18CoachPaciente_FechaAsignacion, T00023_n18CoachPaciente_FechaAsignacion, T00023_A19CoachPaciente_DispositivoID, T00023_n19CoachPaciente_DispositivoID, T00023_A20CoachPaciente_Token, T00023_n20CoachPaciente_Token, T00023_A12CoachPaciente_Coach, T00023_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T00024_A12CoachPaciente_Coach
               }
               , new Object[] {
               T00025_A16CoachPaciente_PacientePNombre, T00025_n16CoachPaciente_PacientePNombre, T00025_A17CoachPaciente_PacientePApellid, T00025_n17CoachPaciente_PacientePApellid
               }
               , new Object[] {
               T00026_A14CoachPaciente_CoachPNombre, T00026_A15CoachPaciente_CoachPApellido, T00026_A16CoachPaciente_PacientePNombre, T00026_n16CoachPaciente_PacientePNombre, T00026_A17CoachPaciente_PacientePApellid, T00026_n17CoachPaciente_PacientePApellid, T00026_A18CoachPaciente_FechaAsignacion, T00026_n18CoachPaciente_FechaAsignacion, T00026_A19CoachPaciente_DispositivoID, T00026_n19CoachPaciente_DispositivoID,
               T00026_A20CoachPaciente_Token, T00026_n20CoachPaciente_Token, T00026_A12CoachPaciente_Coach, T00026_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T00027_A12CoachPaciente_Coach
               }
               , new Object[] {
               T00028_A16CoachPaciente_PacientePNombre, T00028_n16CoachPaciente_PacientePNombre, T00028_A17CoachPaciente_PacientePApellid, T00028_n17CoachPaciente_PacientePApellid
               }
               , new Object[] {
               T00029_A12CoachPaciente_Coach, T00029_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T000210_A12CoachPaciente_Coach, T000210_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T000211_A12CoachPaciente_Coach, T000211_A13CoachPaciente_Paciente
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000215_A16CoachPaciente_PacientePNombre, T000215_n16CoachPaciente_PacientePNombre, T000215_A17CoachPaciente_PacientePApellid, T000215_n17CoachPaciente_PacientePApellid
               }
               , new Object[] {
               T000216_A12CoachPaciente_Coach, T000216_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T000217_A12CoachPaciente_Coach
               }
            }
         );
      }

      private short GxWebError ;
      private short gxcookieaux ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short GX_JID ;
      private short RcdFound2 ;
      private short Gx_BScreen ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtCoachPaciente_Coach_Enabled ;
      private int imgprompt_12_Visible ;
      private int edtCoachPaciente_CoachPNombre_Enabled ;
      private int edtCoachPaciente_CoachPApellido_Enabled ;
      private int edtCoachPaciente_Paciente_Enabled ;
      private int imgprompt_13_Visible ;
      private int edtCoachPaciente_PacientePNombre_Enabled ;
      private int edtCoachPaciente_PacientePApellid_Enabled ;
      private int edtCoachPaciente_FechaAsignacion_Enabled ;
      private int edtCoachPaciente_DispositivoID_Enabled ;
      private int edtCoachPaciente_Token_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtCoachPaciente_Coach_Internalname ;
      private String divTablemain_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtCoachPaciente_Coach_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_12_Internalname ;
      private String imgprompt_12_Link ;
      private String edtCoachPaciente_CoachPNombre_Internalname ;
      private String edtCoachPaciente_CoachPNombre_Jsonclick ;
      private String edtCoachPaciente_CoachPApellido_Internalname ;
      private String edtCoachPaciente_CoachPApellido_Jsonclick ;
      private String edtCoachPaciente_Paciente_Internalname ;
      private String edtCoachPaciente_Paciente_Jsonclick ;
      private String imgprompt_13_Internalname ;
      private String imgprompt_13_Link ;
      private String edtCoachPaciente_PacientePNombre_Internalname ;
      private String edtCoachPaciente_PacientePNombre_Jsonclick ;
      private String edtCoachPaciente_PacientePApellid_Internalname ;
      private String edtCoachPaciente_PacientePApellid_Jsonclick ;
      private String edtCoachPaciente_FechaAsignacion_Internalname ;
      private String edtCoachPaciente_FechaAsignacion_Jsonclick ;
      private String edtCoachPaciente_DispositivoID_Internalname ;
      private String edtCoachPaciente_DispositivoID_Jsonclick ;
      private String edtCoachPaciente_Token_Internalname ;
      private String edtCoachPaciente_Token_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode2 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z18CoachPaciente_FechaAsignacion ;
      private DateTime A18CoachPaciente_FechaAsignacion ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n16CoachPaciente_PacientePNombre ;
      private bool n17CoachPaciente_PacientePApellid ;
      private bool n18CoachPaciente_FechaAsignacion ;
      private bool n19CoachPaciente_DispositivoID ;
      private bool n20CoachPaciente_Token ;
      private String Z14CoachPaciente_CoachPNombre ;
      private String Z15CoachPaciente_CoachPApellido ;
      private String Z19CoachPaciente_DispositivoID ;
      private String Z20CoachPaciente_Token ;
      private String A14CoachPaciente_CoachPNombre ;
      private String A15CoachPaciente_CoachPApellido ;
      private String A16CoachPaciente_PacientePNombre ;
      private String A17CoachPaciente_PacientePApellid ;
      private String A19CoachPaciente_DispositivoID ;
      private String A20CoachPaciente_Token ;
      private String Z16CoachPaciente_PacientePNombre ;
      private String Z17CoachPaciente_PacientePApellid ;
      private Guid Z12CoachPaciente_Coach ;
      private Guid Z13CoachPaciente_Paciente ;
      private Guid A12CoachPaciente_Coach ;
      private Guid A13CoachPaciente_Paciente ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] T00026_A14CoachPaciente_CoachPNombre ;
      private String[] T00026_A15CoachPaciente_CoachPApellido ;
      private String[] T00026_A16CoachPaciente_PacientePNombre ;
      private bool[] T00026_n16CoachPaciente_PacientePNombre ;
      private String[] T00026_A17CoachPaciente_PacientePApellid ;
      private bool[] T00026_n17CoachPaciente_PacientePApellid ;
      private DateTime[] T00026_A18CoachPaciente_FechaAsignacion ;
      private bool[] T00026_n18CoachPaciente_FechaAsignacion ;
      private String[] T00026_A19CoachPaciente_DispositivoID ;
      private bool[] T00026_n19CoachPaciente_DispositivoID ;
      private String[] T00026_A20CoachPaciente_Token ;
      private bool[] T00026_n20CoachPaciente_Token ;
      private Guid[] T00026_A12CoachPaciente_Coach ;
      private Guid[] T00026_A13CoachPaciente_Paciente ;
      private Guid[] T00024_A12CoachPaciente_Coach ;
      private String[] T00025_A16CoachPaciente_PacientePNombre ;
      private bool[] T00025_n16CoachPaciente_PacientePNombre ;
      private String[] T00025_A17CoachPaciente_PacientePApellid ;
      private bool[] T00025_n17CoachPaciente_PacientePApellid ;
      private Guid[] T00027_A12CoachPaciente_Coach ;
      private String[] T00028_A16CoachPaciente_PacientePNombre ;
      private bool[] T00028_n16CoachPaciente_PacientePNombre ;
      private String[] T00028_A17CoachPaciente_PacientePApellid ;
      private bool[] T00028_n17CoachPaciente_PacientePApellid ;
      private Guid[] T00029_A12CoachPaciente_Coach ;
      private Guid[] T00029_A13CoachPaciente_Paciente ;
      private String[] T00023_A14CoachPaciente_CoachPNombre ;
      private String[] T00023_A15CoachPaciente_CoachPApellido ;
      private DateTime[] T00023_A18CoachPaciente_FechaAsignacion ;
      private bool[] T00023_n18CoachPaciente_FechaAsignacion ;
      private String[] T00023_A19CoachPaciente_DispositivoID ;
      private bool[] T00023_n19CoachPaciente_DispositivoID ;
      private String[] T00023_A20CoachPaciente_Token ;
      private bool[] T00023_n20CoachPaciente_Token ;
      private Guid[] T00023_A12CoachPaciente_Coach ;
      private Guid[] T00023_A13CoachPaciente_Paciente ;
      private Guid[] T000210_A12CoachPaciente_Coach ;
      private Guid[] T000210_A13CoachPaciente_Paciente ;
      private Guid[] T000211_A12CoachPaciente_Coach ;
      private Guid[] T000211_A13CoachPaciente_Paciente ;
      private String[] T00022_A14CoachPaciente_CoachPNombre ;
      private String[] T00022_A15CoachPaciente_CoachPApellido ;
      private DateTime[] T00022_A18CoachPaciente_FechaAsignacion ;
      private bool[] T00022_n18CoachPaciente_FechaAsignacion ;
      private String[] T00022_A19CoachPaciente_DispositivoID ;
      private bool[] T00022_n19CoachPaciente_DispositivoID ;
      private String[] T00022_A20CoachPaciente_Token ;
      private bool[] T00022_n20CoachPaciente_Token ;
      private Guid[] T00022_A12CoachPaciente_Coach ;
      private Guid[] T00022_A13CoachPaciente_Paciente ;
      private String[] T000215_A16CoachPaciente_PacientePNombre ;
      private bool[] T000215_n16CoachPaciente_PacientePNombre ;
      private String[] T000215_A17CoachPaciente_PacientePApellid ;
      private bool[] T000215_n17CoachPaciente_PacientePApellid ;
      private IDataStoreProvider pr_gam ;
      private Guid[] T000216_A12CoachPaciente_Coach ;
      private Guid[] T000216_A13CoachPaciente_Paciente ;
      private Guid[] T000217_A12CoachPaciente_Coach ;
      private GXWebForm Form ;
   }

   public class coachpaciente__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class coachpaciente__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new ForEachCursor(def[6])
       ,new ForEachCursor(def[7])
       ,new ForEachCursor(def[8])
       ,new ForEachCursor(def[9])
       ,new UpdateCursor(def[10])
       ,new UpdateCursor(def[11])
       ,new UpdateCursor(def[12])
       ,new ForEachCursor(def[13])
       ,new ForEachCursor(def[14])
       ,new ForEachCursor(def[15])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmT00026 ;
        prmT00026 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00024 ;
        prmT00024 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00025 ;
        prmT00025 = new Object[] {
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00027 ;
        prmT00027 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00028 ;
        prmT00028 = new Object[] {
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00029 ;
        prmT00029 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00023 ;
        prmT00023 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000210 ;
        prmT000210 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000211 ;
        prmT000211 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00022 ;
        prmT00022 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000212 ;
        prmT000212 = new Object[] {
        new Object[] {"@CoachPaciente_CoachPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_CoachPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_FechaAsignacion",SqlDbType.DateTime,8,0} ,
        new Object[] {"@CoachPaciente_DispositivoID",SqlDbType.VarChar,75,0} ,
        new Object[] {"@CoachPaciente_Token",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000213 ;
        prmT000213 = new Object[] {
        new Object[] {"@CoachPaciente_CoachPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_CoachPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_FechaAsignacion",SqlDbType.DateTime,8,0} ,
        new Object[] {"@CoachPaciente_DispositivoID",SqlDbType.VarChar,75,0} ,
        new Object[] {"@CoachPaciente_Token",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000214 ;
        prmT000214 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000216 ;
        prmT000216 = new Object[] {
        } ;
        Object[] prmT000217 ;
        prmT000217 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000215 ;
        prmT000215 = new Object[] {
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("T00022", "SELECT [CoachPaciente_CoachPNombre], [CoachPaciente_CoachPApellido], [CoachPaciente_FechaAsignacion], [CoachPaciente_DispositivoID], [CoachPaciente_Token], [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (UPDLOCK) WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT00022,1,0,true,false )
           ,new CursorDef("T00023", "SELECT [CoachPaciente_CoachPNombre], [CoachPaciente_CoachPApellido], [CoachPaciente_FechaAsignacion], [CoachPaciente_DispositivoID], [CoachPaciente_Token], [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT00023,1,0,true,false )
           ,new CursorDef("T00024", "SELECT [PersonaID] AS CoachPaciente_Coach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Coach ",true, GxErrorMask.GX_NOMASK, false, this,prmT00024,1,0,true,false )
           ,new CursorDef("T00025", "SELECT [PersonaPNombre] AS CoachPaciente_PacientePNombre, [PersonaPApellido] AS CoachPaciente_PacientePApellid FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT00025,1,0,true,false )
           ,new CursorDef("T00026", "SELECT TM1.[CoachPaciente_CoachPNombre], TM1.[CoachPaciente_CoachPApellido], T2.[PersonaPNombre] AS CoachPaciente_PacientePNombre, T2.[PersonaPApellido] AS CoachPaciente_PacientePApellid, TM1.[CoachPaciente_FechaAsignacion], TM1.[CoachPaciente_DispositivoID], TM1.[CoachPaciente_Token], TM1.[CoachPaciente_Coach] AS CoachPaciente_Coach, TM1.[CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM ([CoachPaciente] TM1 WITH (NOLOCK) INNER JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = TM1.[CoachPaciente_Paciente]) WHERE TM1.[CoachPaciente_Coach] = @CoachPaciente_Coach and TM1.[CoachPaciente_Paciente] = @CoachPaciente_Paciente ORDER BY TM1.[CoachPaciente_Coach], TM1.[CoachPaciente_Paciente]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00026,100,0,true,false )
           ,new CursorDef("T00027", "SELECT [PersonaID] AS CoachPaciente_Coach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Coach ",true, GxErrorMask.GX_NOMASK, false, this,prmT00027,1,0,true,false )
           ,new CursorDef("T00028", "SELECT [PersonaPNombre] AS CoachPaciente_PacientePNombre, [PersonaPApellido] AS CoachPaciente_PacientePApellid FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT00028,1,0,true,false )
           ,new CursorDef("T00029", "SELECT [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00029,1,0,true,false )
           ,new CursorDef("T000210", "SELECT TOP 1 [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (NOLOCK) WHERE ( [CoachPaciente_Coach] > @CoachPaciente_Coach or [CoachPaciente_Coach] = @CoachPaciente_Coach and [CoachPaciente_Paciente] > @CoachPaciente_Paciente) ORDER BY [CoachPaciente_Coach], [CoachPaciente_Paciente]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000210,1,0,true,true )
           ,new CursorDef("T000211", "SELECT TOP 1 [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (NOLOCK) WHERE ( [CoachPaciente_Coach] < @CoachPaciente_Coach or [CoachPaciente_Coach] = @CoachPaciente_Coach and [CoachPaciente_Paciente] < @CoachPaciente_Paciente) ORDER BY [CoachPaciente_Coach] DESC, [CoachPaciente_Paciente] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000211,1,0,true,true )
           ,new CursorDef("T000212", "INSERT INTO [CoachPaciente]([CoachPaciente_CoachPNombre], [CoachPaciente_CoachPApellido], [CoachPaciente_FechaAsignacion], [CoachPaciente_DispositivoID], [CoachPaciente_Token], [CoachPaciente_Coach], [CoachPaciente_Paciente]) VALUES(@CoachPaciente_CoachPNombre, @CoachPaciente_CoachPApellido, @CoachPaciente_FechaAsignacion, @CoachPaciente_DispositivoID, @CoachPaciente_Token, @CoachPaciente_Coach, @CoachPaciente_Paciente)", GxErrorMask.GX_NOMASK,prmT000212)
           ,new CursorDef("T000213", "UPDATE [CoachPaciente] SET [CoachPaciente_CoachPNombre]=@CoachPaciente_CoachPNombre, [CoachPaciente_CoachPApellido]=@CoachPaciente_CoachPApellido, [CoachPaciente_FechaAsignacion]=@CoachPaciente_FechaAsignacion, [CoachPaciente_DispositivoID]=@CoachPaciente_DispositivoID, [CoachPaciente_Token]=@CoachPaciente_Token  WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente", GxErrorMask.GX_NOMASK,prmT000213)
           ,new CursorDef("T000214", "DELETE FROM [CoachPaciente]  WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente", GxErrorMask.GX_NOMASK,prmT000214)
           ,new CursorDef("T000215", "SELECT [PersonaPNombre] AS CoachPaciente_PacientePNombre, [PersonaPApellido] AS CoachPaciente_PacientePApellid FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT000215,1,0,true,false )
           ,new CursorDef("T000216", "SELECT [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (NOLOCK) ORDER BY [CoachPaciente_Coach], [CoachPaciente_Paciente]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000216,100,0,true,false )
           ,new CursorDef("T000217", "SELECT [PersonaID] AS CoachPaciente_Coach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Coach ",true, GxErrorMask.GX_NOMASK, false, this,prmT000217,1,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((Guid[]) buf[8])[0] = rslt.getGuid(6) ;
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              return;
           case 1 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((Guid[]) buf[8])[0] = rslt.getGuid(6) ;
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 3 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(2);
              return;
           case 4 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((Guid[]) buf[12])[0] = rslt.getGuid(8) ;
              ((Guid[]) buf[13])[0] = rslt.getGuid(9) ;
              return;
           case 5 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 6 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(2);
              return;
           case 7 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 8 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 9 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 13 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(2);
              return;
           case 14 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 15 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 2 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 3 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 5 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 6 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 7 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 9 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 10 :
              stmt.SetParameter(1, (String)parms[0]);
              stmt.SetParameter(2, (String)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(3, (DateTime)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              stmt.SetParameter(6, (Guid)parms[8]);
              stmt.SetParameter(7, (Guid)parms[9]);
              return;
           case 11 :
              stmt.SetParameter(1, (String)parms[0]);
              stmt.SetParameter(2, (String)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(3, (DateTime)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              stmt.SetParameter(6, (Guid)parms[8]);
              stmt.SetParameter(7, (Guid)parms[9]);
              return;
           case 12 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 13 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 15 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
