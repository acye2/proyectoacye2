/*
               File: wsregistrarecorrido
        Description: wsregistrarecorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 11:14:33.95
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class awsregistrarecorrido : GXProcedure
   {
      public awsregistrarecorrido( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public awsregistrarecorrido( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_IDSesion ,
                           short aP1_TipoSenal ,
                           ref String aP2_Longitud ,
                           ref String aP3_Latitud ,
                           ref short aP4_TipoObstaculo ,
                           short aP5_Incidente ,
                           short aP6_Emergencia ,
                           ref String aP7_IMEI ,
                           ref String aP8_IDCoach ,
                           ref String aP9_IDPaciente )
      {
         this.AV15IDSesion = aP0_IDSesion;
         this.AV16TipoSenal = aP1_TipoSenal;
         this.AV8Longitud = aP2_Longitud;
         this.AV14Latitud = aP3_Latitud;
         this.AV9TipoObstaculo = aP4_TipoObstaculo;
         this.AV10Incidente = aP5_Incidente;
         this.AV11Emergencia = aP6_Emergencia;
         this.AV12IMEI = aP7_IMEI;
         this.AV13IDCoach = aP8_IDCoach;
         this.AV17IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV15IDSesion;
         aP2_Longitud=this.AV8Longitud;
         aP3_Latitud=this.AV14Latitud;
         aP4_TipoObstaculo=this.AV9TipoObstaculo;
         aP7_IMEI=this.AV12IMEI;
         aP8_IDCoach=this.AV13IDCoach;
         aP9_IDPaciente=this.AV17IDPaciente;
      }

      public String executeUdp( ref String aP0_IDSesion ,
                                short aP1_TipoSenal ,
                                ref String aP2_Longitud ,
                                ref String aP3_Latitud ,
                                ref short aP4_TipoObstaculo ,
                                short aP5_Incidente ,
                                short aP6_Emergencia ,
                                ref String aP7_IMEI ,
                                ref String aP8_IDCoach )
      {
         this.AV15IDSesion = aP0_IDSesion;
         this.AV16TipoSenal = aP1_TipoSenal;
         this.AV8Longitud = aP2_Longitud;
         this.AV14Latitud = aP3_Latitud;
         this.AV9TipoObstaculo = aP4_TipoObstaculo;
         this.AV10Incidente = aP5_Incidente;
         this.AV11Emergencia = aP6_Emergencia;
         this.AV12IMEI = aP7_IMEI;
         this.AV13IDCoach = aP8_IDCoach;
         this.AV17IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV15IDSesion;
         aP2_Longitud=this.AV8Longitud;
         aP3_Latitud=this.AV14Latitud;
         aP4_TipoObstaculo=this.AV9TipoObstaculo;
         aP7_IMEI=this.AV12IMEI;
         aP8_IDCoach=this.AV13IDCoach;
         aP9_IDPaciente=this.AV17IDPaciente;
         return AV17IDPaciente ;
      }

      public void executeSubmit( ref String aP0_IDSesion ,
                                 short aP1_TipoSenal ,
                                 ref String aP2_Longitud ,
                                 ref String aP3_Latitud ,
                                 ref short aP4_TipoObstaculo ,
                                 short aP5_Incidente ,
                                 short aP6_Emergencia ,
                                 ref String aP7_IMEI ,
                                 ref String aP8_IDCoach ,
                                 ref String aP9_IDPaciente )
      {
         awsregistrarecorrido objawsregistrarecorrido;
         objawsregistrarecorrido = new awsregistrarecorrido();
         objawsregistrarecorrido.AV15IDSesion = aP0_IDSesion;
         objawsregistrarecorrido.AV16TipoSenal = aP1_TipoSenal;
         objawsregistrarecorrido.AV8Longitud = aP2_Longitud;
         objawsregistrarecorrido.AV14Latitud = aP3_Latitud;
         objawsregistrarecorrido.AV9TipoObstaculo = aP4_TipoObstaculo;
         objawsregistrarecorrido.AV10Incidente = aP5_Incidente;
         objawsregistrarecorrido.AV11Emergencia = aP6_Emergencia;
         objawsregistrarecorrido.AV12IMEI = aP7_IMEI;
         objawsregistrarecorrido.AV13IDCoach = aP8_IDCoach;
         objawsregistrarecorrido.AV17IDPaciente = aP9_IDPaciente;
         objawsregistrarecorrido.context.SetSubmitInitialConfig(context);
         objawsregistrarecorrido.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objawsregistrarecorrido);
         aP0_IDSesion=this.AV15IDSesion;
         aP2_Longitud=this.AV8Longitud;
         aP3_Latitud=this.AV14Latitud;
         aP4_TipoObstaculo=this.AV9TipoObstaculo;
         aP7_IMEI=this.AV12IMEI;
         aP8_IDCoach=this.AV13IDCoach;
         aP9_IDPaciente=this.AV17IDPaciente;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((awsregistrarecorrido)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV16TipoSenal == 1 )
         {
            AV20GUID = (Guid)(Guid.NewGuid( ));
            AV18Recorrido = new SdtRecorrido(context);
            AV18Recorrido.gxTpr_Recorridoid = (Guid)(AV20GUID);
            AV18Recorrido.gxTpr_Recorridofecha = DateTimeUtil.ServerNow( context, pr_default);
            /* Execute user subroutine: 'GUARDAR' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV15IDSesion = StringUtil.Trim( AV20GUID.ToString());
         }
         else if ( AV16TipoSenal == 2 )
         {
            AV20GUID = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV15IDSesion)));
            AV18Recorrido.Load(AV20GUID);
            /* Execute user subroutine: 'GUARDAR' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( AV16TipoSenal == 3 )
         {
            AV20GUID = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV15IDSesion)));
            AV18Recorrido.Load(AV20GUID);
            /* Execute user subroutine: 'DETALLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'GUARDAR' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'DETALLE' Routine */
         AV22Detalle = new SdtRecorrido_Detalle(context);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV17IDPaciente)) )
         {
            AV22Detalle.gxTpr_Recorridodetallepaciente = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV17IDPaciente)));
         }
         else
         {
            AV22Detalle.gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_SetNull();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV13IDCoach)) )
         {
            AV22Detalle.gxTpr_Idcoach = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV13IDCoach)));
         }
         else
         {
            AV22Detalle.gxTv_SdtRecorrido_Detalle_Idcoach_SetNull();
         }
         AV22Detalle.gxTpr_Tiposenal = AV16TipoSenal;
         AV22Detalle.gxTpr_Longitud = AV8Longitud;
         AV22Detalle.gxTpr_Latitud = AV14Latitud;
         AV22Detalle.gxTpr_Tipoobstaculo = AV9TipoObstaculo;
         AV22Detalle.gxTpr_Incidente = AV10Incidente;
         AV22Detalle.gxTpr_Emergencia = AV11Emergencia;
         AV22Detalle.gxTpr_Imei = AV12IMEI;
         AV22Detalle.gxTpr_Timestamp = DateTimeUtil.ServerNow( context, pr_default);
         AV18Recorrido.gxTpr_Detalle.Add(AV22Detalle, 0);
      }

      protected void S121( )
      {
         /* 'GUARDAR' Routine */
         AV18Recorrido.Save();
         if ( AV18Recorrido.Success() )
         {
            pr_gam.commit( "wsregistrarecorrido");
            pr_default.commit( "wsregistrarecorrido");
         }
         else
         {
            pr_gam.rollback( "wsregistrarecorrido");
            pr_default.rollback( "wsregistrarecorrido");
            AV27GXV2 = 1;
            AV26GXV1 = AV18Recorrido.GetMessages();
            while ( AV27GXV2 <= AV26GXV1.Count )
            {
               AV23Message = ((SdtMessages_Message)AV26GXV1.Item(AV27GXV2));
               GX_msglist.addItem(AV23Message.gxTpr_Description);
               AV27GXV2 = (int)(AV27GXV2+1);
            }
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV20GUID = (Guid)(Guid.Empty);
         AV18Recorrido = new SdtRecorrido(context);
         AV22Detalle = new SdtRecorrido_Detalle(context);
         AV26GXV1 = new GXBaseCollection<SdtMessages_Message>( context, "Message", "GeneXus");
         AV23Message = new SdtMessages_Message(context);
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.awsregistrarecorrido__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.awsregistrarecorrido__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV16TipoSenal ;
      private short AV9TipoObstaculo ;
      private short AV10Incidente ;
      private short AV11Emergencia ;
      private int AV27GXV2 ;
      private String AV15IDSesion ;
      private String AV13IDCoach ;
      private String AV17IDPaciente ;
      private bool returnInSub ;
      private String AV8Longitud ;
      private String AV14Latitud ;
      private String AV12IMEI ;
      private Guid AV20GUID ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_IDSesion ;
      private String aP2_Longitud ;
      private String aP3_Latitud ;
      private short aP4_TipoObstaculo ;
      private String aP7_IMEI ;
      private String aP8_IDCoach ;
      private String aP9_IDPaciente ;
      private IDataStoreProvider pr_default ;
      private IDataStoreProvider pr_gam ;
      private GXBaseCollection<SdtMessages_Message> AV26GXV1 ;
      private SdtRecorrido AV18Recorrido ;
      private SdtRecorrido_Detalle AV22Detalle ;
      private SdtMessages_Message AV23Message ;
   }

   public class awsregistrarecorrido__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class awsregistrarecorrido__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

[ServiceContract(Namespace = "GeneXus.Programs.wsregistrarecorrido_services")]
[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class wsregistrarecorrido_services : GxRestService
{
   [OperationContract]
   [WebInvoke(Method =  "POST" ,
   	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
   	ResponseFormat = WebMessageFormat.Json,
   	UriTemplate = "/")]
   public void execute( ref String IDSesion ,
                        short TipoSenal ,
                        ref String Longitud ,
                        ref String Latitud ,
                        ref short TipoObstaculo ,
                        short Incidente ,
                        short Emergencia ,
                        ref String IMEI ,
                        ref String IDCoach ,
                        ref String IDPaciente )
   {
      try
      {
         if ( ! ProcessHeaders("wsregistrarecorrido") )
         {
            return  ;
         }
         awsregistrarecorrido worker = new awsregistrarecorrido(context) ;
         worker.IsMain = RunAsMain ;
         worker.execute(ref IDSesion,TipoSenal,ref Longitud,ref Latitud,ref TipoObstaculo,Incidente,Emergencia,ref IMEI,ref IDCoach,ref IDPaciente );
         worker.cleanup( );
      }
      catch ( Exception e )
      {
         WebException(e);
      }
      finally
      {
         Cleanup();
      }
   }

}

}
