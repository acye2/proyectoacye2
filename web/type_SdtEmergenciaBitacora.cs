/*
               File: type_SdtEmergenciaBitacora
        Description: Emergencia Bitacora
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:34:9.74
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "EmergenciaBitacora" )]
   [XmlType(TypeName =  "EmergenciaBitacora" , Namespace = "PACYE2" )]
   [Serializable]
   public class SdtEmergenciaBitacora : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtEmergenciaBitacora( )
      {
      }

      public SdtEmergenciaBitacora( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( Guid AV43EmergenciaID )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(Guid)AV43EmergenciaID});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"EmergenciaID", typeof(Guid)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "EmergenciaBitacora");
         metadata.Set("BT", "EmergenciaBitacora");
         metadata.Set("PK", "[ \"EmergenciaID\" ]");
         metadata.Set("PKAssigned", "[ \"EmergenciaID\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"PersonaID\" ],\"FKMap\":[ \"EmergenciaCoach-PersonaID\" ] },{ \"FK\":[ \"PersonaID\" ],\"FKMap\":[ \"EmergenciaPaciente-PersonaID\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GxStringCollection StateAttributes( )
      {
         GxStringCollection state = new GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Emergenciaid_Z");
         state.Add("gxTpr_Emergenciafecha_Z_Nullable");
         state.Add("gxTpr_Emergenciaestado_Z");
         state.Add("gxTpr_Emergenciapaciente_Z");
         state.Add("gxTpr_Emergenciapacientenombre_Z");
         state.Add("gxTpr_Emergenciacoach_Z");
         state.Add("gxTpr_Emergenciapaciente_N");
         state.Add("gxTpr_Emergenciapacientenombre_N");
         state.Add("gxTpr_Emergenciacoach_N");
         state.Add("gxTpr_Emergencialongitud_N");
         state.Add("gxTpr_Emergencialatitud_N");
         state.Add("gxTpr_Emergenciamensaje_N");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         SdtEmergenciaBitacora sdt ;
         sdt = (SdtEmergenciaBitacora)(source);
         gxTv_SdtEmergenciaBitacora_Emergenciaid = sdt.gxTv_SdtEmergenciaBitacora_Emergenciaid ;
         gxTv_SdtEmergenciaBitacora_Emergenciafecha = sdt.gxTv_SdtEmergenciaBitacora_Emergenciafecha ;
         gxTv_SdtEmergenciaBitacora_Emergenciaestado = sdt.gxTv_SdtEmergenciaBitacora_Emergenciaestado ;
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapaciente ;
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre ;
         gxTv_SdtEmergenciaBitacora_Emergenciacoach = sdt.gxTv_SdtEmergenciaBitacora_Emergenciacoach ;
         gxTv_SdtEmergenciaBitacora_Emergencialongitud = sdt.gxTv_SdtEmergenciaBitacora_Emergencialongitud ;
         gxTv_SdtEmergenciaBitacora_Emergencialatitud = sdt.gxTv_SdtEmergenciaBitacora_Emergencialatitud ;
         gxTv_SdtEmergenciaBitacora_Emergenciamensaje = sdt.gxTv_SdtEmergenciaBitacora_Emergenciamensaje ;
         gxTv_SdtEmergenciaBitacora_Mode = sdt.gxTv_SdtEmergenciaBitacora_Mode ;
         gxTv_SdtEmergenciaBitacora_Initialized = sdt.gxTv_SdtEmergenciaBitacora_Initialized ;
         gxTv_SdtEmergenciaBitacora_Emergenciaid_Z = sdt.gxTv_SdtEmergenciaBitacora_Emergenciaid_Z ;
         gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z = sdt.gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z ;
         gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z = sdt.gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z ;
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z ;
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z ;
         gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z = sdt.gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z ;
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N ;
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N ;
         gxTv_SdtEmergenciaBitacora_Emergenciacoach_N = sdt.gxTv_SdtEmergenciaBitacora_Emergenciacoach_N ;
         gxTv_SdtEmergenciaBitacora_Emergencialongitud_N = sdt.gxTv_SdtEmergenciaBitacora_Emergencialongitud_N ;
         gxTv_SdtEmergenciaBitacora_Emergencialatitud_N = sdt.gxTv_SdtEmergenciaBitacora_Emergencialatitud_N ;
         gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N = sdt.gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("EmergenciaID", gxTv_SdtEmergenciaBitacora_Emergenciaid, false);
         datetime_STZ = gxTv_SdtEmergenciaBitacora_Emergenciafecha;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("EmergenciaFecha", sDateCnv, false);
         AddObjectProperty("EmergenciaEstado", gxTv_SdtEmergenciaBitacora_Emergenciaestado, false);
         AddObjectProperty("EmergenciaPaciente", gxTv_SdtEmergenciaBitacora_Emergenciapaciente, false);
         AddObjectProperty("EmergenciaPaciente_N", gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N, false);
         AddObjectProperty("EmergenciaPacienteNombre", gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre, false);
         AddObjectProperty("EmergenciaPacienteNombre_N", gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N, false);
         AddObjectProperty("EmergenciaCoach", gxTv_SdtEmergenciaBitacora_Emergenciacoach, false);
         AddObjectProperty("EmergenciaCoach_N", gxTv_SdtEmergenciaBitacora_Emergenciacoach_N, false);
         AddObjectProperty("EmergenciaLongitud", gxTv_SdtEmergenciaBitacora_Emergencialongitud, false);
         AddObjectProperty("EmergenciaLongitud_N", gxTv_SdtEmergenciaBitacora_Emergencialongitud_N, false);
         AddObjectProperty("EmergenciaLatitud", gxTv_SdtEmergenciaBitacora_Emergencialatitud, false);
         AddObjectProperty("EmergenciaLatitud_N", gxTv_SdtEmergenciaBitacora_Emergencialatitud_N, false);
         AddObjectProperty("EmergenciaMensaje", gxTv_SdtEmergenciaBitacora_Emergenciamensaje, false);
         AddObjectProperty("EmergenciaMensaje_N", gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtEmergenciaBitacora_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtEmergenciaBitacora_Initialized, false);
            AddObjectProperty("EmergenciaID_Z", gxTv_SdtEmergenciaBitacora_Emergenciaid_Z, false);
            datetime_STZ = gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("EmergenciaFecha_Z", sDateCnv, false);
            AddObjectProperty("EmergenciaEstado_Z", gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z, false);
            AddObjectProperty("EmergenciaPaciente_Z", gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z, false);
            AddObjectProperty("EmergenciaPacienteNombre_Z", gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z, false);
            AddObjectProperty("EmergenciaCoach_Z", gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z, false);
            AddObjectProperty("EmergenciaPaciente_N", gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N, false);
            AddObjectProperty("EmergenciaPacienteNombre_N", gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N, false);
            AddObjectProperty("EmergenciaCoach_N", gxTv_SdtEmergenciaBitacora_Emergenciacoach_N, false);
            AddObjectProperty("EmergenciaLongitud_N", gxTv_SdtEmergenciaBitacora_Emergencialongitud_N, false);
            AddObjectProperty("EmergenciaLatitud_N", gxTv_SdtEmergenciaBitacora_Emergencialatitud_N, false);
            AddObjectProperty("EmergenciaMensaje_N", gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N, false);
         }
         return  ;
      }

      public void UpdateDirties( SdtEmergenciaBitacora sdt )
      {
         if ( sdt.IsDirty("EmergenciaID") )
         {
            gxTv_SdtEmergenciaBitacora_Emergenciaid = sdt.gxTv_SdtEmergenciaBitacora_Emergenciaid ;
         }
         if ( sdt.IsDirty("EmergenciaFecha") )
         {
            gxTv_SdtEmergenciaBitacora_Emergenciafecha = sdt.gxTv_SdtEmergenciaBitacora_Emergenciafecha ;
         }
         if ( sdt.IsDirty("EmergenciaEstado") )
         {
            gxTv_SdtEmergenciaBitacora_Emergenciaestado = sdt.gxTv_SdtEmergenciaBitacora_Emergenciaestado ;
         }
         if ( sdt.IsDirty("EmergenciaPaciente") )
         {
            gxTv_SdtEmergenciaBitacora_Emergenciapaciente = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapaciente ;
         }
         if ( sdt.IsDirty("EmergenciaPacienteNombre") )
         {
            gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre = sdt.gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre ;
         }
         if ( sdt.IsDirty("EmergenciaCoach") )
         {
            gxTv_SdtEmergenciaBitacora_Emergenciacoach = sdt.gxTv_SdtEmergenciaBitacora_Emergenciacoach ;
         }
         if ( sdt.IsDirty("EmergenciaLongitud") )
         {
            gxTv_SdtEmergenciaBitacora_Emergencialongitud = sdt.gxTv_SdtEmergenciaBitacora_Emergencialongitud ;
         }
         if ( sdt.IsDirty("EmergenciaLatitud") )
         {
            gxTv_SdtEmergenciaBitacora_Emergencialatitud = sdt.gxTv_SdtEmergenciaBitacora_Emergencialatitud ;
         }
         if ( sdt.IsDirty("EmergenciaMensaje") )
         {
            gxTv_SdtEmergenciaBitacora_Emergenciamensaje = sdt.gxTv_SdtEmergenciaBitacora_Emergenciamensaje ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "EmergenciaID" )]
      [  XmlElement( ElementName = "EmergenciaID"   )]
      public Guid gxTpr_Emergenciaid
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciaid ;
         }

         set {
            if ( gxTv_SdtEmergenciaBitacora_Emergenciaid != value )
            {
               gxTv_SdtEmergenciaBitacora_Mode = "INS";
               this.gxTv_SdtEmergenciaBitacora_Emergenciaid_Z_SetNull( );
               this.gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z_SetNull( );
               this.gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z_SetNull( );
               this.gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z_SetNull( );
               this.gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z_SetNull( );
               this.gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z_SetNull( );
            }
            gxTv_SdtEmergenciaBitacora_Emergenciaid = (Guid)(value);
            SetDirty("Emergenciaid");
         }

      }

      [  SoapElement( ElementName = "EmergenciaFecha" )]
      [  XmlElement( ElementName = "EmergenciaFecha"  , IsNullable=true )]
      public string gxTpr_Emergenciafecha_Nullable
      {
         get {
            if ( gxTv_SdtEmergenciaBitacora_Emergenciafecha == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtEmergenciaBitacora_Emergenciafecha).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtEmergenciaBitacora_Emergenciafecha = DateTime.MinValue;
            else
               gxTv_SdtEmergenciaBitacora_Emergenciafecha = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Emergenciafecha
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciafecha ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciafecha = value;
            SetDirty("Emergenciafecha");
         }

      }

      [  SoapElement( ElementName = "EmergenciaEstado" )]
      [  XmlElement( ElementName = "EmergenciaEstado"   )]
      public short gxTpr_Emergenciaestado
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciaestado ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciaestado = value;
            SetDirty("Emergenciaestado");
         }

      }

      [  SoapElement( ElementName = "EmergenciaPaciente" )]
      [  XmlElement( ElementName = "EmergenciaPaciente"   )]
      public Guid gxTpr_Emergenciapaciente
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciapaciente ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N = 0;
            gxTv_SdtEmergenciaBitacora_Emergenciapaciente = (Guid)(value);
            SetDirty("Emergenciapaciente");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciapaciente_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N = 1;
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciapaciente_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaPacienteNombre" )]
      [  XmlElement( ElementName = "EmergenciaPacienteNombre"   )]
      public String gxTpr_Emergenciapacientenombre
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N = 0;
            gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre = value;
            SetDirty("Emergenciapacientenombre");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N = 1;
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre = "";
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaCoach" )]
      [  XmlElement( ElementName = "EmergenciaCoach"   )]
      public Guid gxTpr_Emergenciacoach
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciacoach ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciacoach_N = 0;
            gxTv_SdtEmergenciaBitacora_Emergenciacoach = (Guid)(value);
            SetDirty("Emergenciacoach");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciacoach_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciacoach_N = 1;
         gxTv_SdtEmergenciaBitacora_Emergenciacoach = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciacoach_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaLongitud" )]
      [  XmlElement( ElementName = "EmergenciaLongitud"   )]
      public String gxTpr_Emergencialongitud
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergencialongitud ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergencialongitud_N = 0;
            gxTv_SdtEmergenciaBitacora_Emergencialongitud = value;
            SetDirty("Emergencialongitud");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergencialongitud_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergencialongitud_N = 1;
         gxTv_SdtEmergenciaBitacora_Emergencialongitud = "";
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergencialongitud_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaLatitud" )]
      [  XmlElement( ElementName = "EmergenciaLatitud"   )]
      public String gxTpr_Emergencialatitud
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergencialatitud ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergencialatitud_N = 0;
            gxTv_SdtEmergenciaBitacora_Emergencialatitud = value;
            SetDirty("Emergencialatitud");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergencialatitud_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergencialatitud_N = 1;
         gxTv_SdtEmergenciaBitacora_Emergencialatitud = "";
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergencialatitud_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaMensaje" )]
      [  XmlElement( ElementName = "EmergenciaMensaje"   )]
      public String gxTpr_Emergenciamensaje
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciamensaje ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N = 0;
            gxTv_SdtEmergenciaBitacora_Emergenciamensaje = value;
            SetDirty("Emergenciamensaje");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciamensaje_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N = 1;
         gxTv_SdtEmergenciaBitacora_Emergenciamensaje = "";
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciamensaje_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Mode ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Mode_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Mode = "";
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Initialized ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Initialized_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaID_Z" )]
      [  XmlElement( ElementName = "EmergenciaID_Z"   )]
      public Guid gxTpr_Emergenciaid_Z
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciaid_Z ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciaid_Z = (Guid)(value);
            SetDirty("Emergenciaid_Z");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciaid_Z_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciaid_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciaid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaFecha_Z" )]
      [  XmlElement( ElementName = "EmergenciaFecha_Z"  , IsNullable=true )]
      public string gxTpr_Emergenciafecha_Z_Nullable
      {
         get {
            if ( gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z = DateTime.MinValue;
            else
               gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Emergenciafecha_Z
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z = value;
            SetDirty("Emergenciafecha_Z");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaEstado_Z" )]
      [  XmlElement( ElementName = "EmergenciaEstado_Z"   )]
      public short gxTpr_Emergenciaestado_Z
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z = value;
            SetDirty("Emergenciaestado_Z");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaPaciente_Z" )]
      [  XmlElement( ElementName = "EmergenciaPaciente_Z"   )]
      public Guid gxTpr_Emergenciapaciente_Z
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z = (Guid)(value);
            SetDirty("Emergenciapaciente_Z");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaPacienteNombre_Z" )]
      [  XmlElement( ElementName = "EmergenciaPacienteNombre_Z"   )]
      public String gxTpr_Emergenciapacientenombre_Z
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z = value;
            SetDirty("Emergenciapacientenombre_Z");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z = "";
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaCoach_Z" )]
      [  XmlElement( ElementName = "EmergenciaCoach_Z"   )]
      public Guid gxTpr_Emergenciacoach_Z
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z = (Guid)(value);
            SetDirty("Emergenciacoach_Z");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaPaciente_N" )]
      [  XmlElement( ElementName = "EmergenciaPaciente_N"   )]
      public short gxTpr_Emergenciapaciente_N
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N = value;
            SetDirty("Emergenciapaciente_N");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaPacienteNombre_N" )]
      [  XmlElement( ElementName = "EmergenciaPacienteNombre_N"   )]
      public short gxTpr_Emergenciapacientenombre_N
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N = value;
            SetDirty("Emergenciapacientenombre_N");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaCoach_N" )]
      [  XmlElement( ElementName = "EmergenciaCoach_N"   )]
      public short gxTpr_Emergenciacoach_N
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciacoach_N ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciacoach_N = value;
            SetDirty("Emergenciacoach_N");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciacoach_N_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciacoach_N = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciacoach_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaLongitud_N" )]
      [  XmlElement( ElementName = "EmergenciaLongitud_N"   )]
      public short gxTpr_Emergencialongitud_N
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergencialongitud_N ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergencialongitud_N = value;
            SetDirty("Emergencialongitud_N");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergencialongitud_N_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergencialongitud_N = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergencialongitud_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaLatitud_N" )]
      [  XmlElement( ElementName = "EmergenciaLatitud_N"   )]
      public short gxTpr_Emergencialatitud_N
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergencialatitud_N ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergencialatitud_N = value;
            SetDirty("Emergencialatitud_N");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergencialatitud_N_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergencialatitud_N = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergencialatitud_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "EmergenciaMensaje_N" )]
      [  XmlElement( ElementName = "EmergenciaMensaje_N"   )]
      public short gxTpr_Emergenciamensaje_N
      {
         get {
            return gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N ;
         }

         set {
            gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N = value;
            SetDirty("Emergenciamensaje_N");
         }

      }

      public void gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N_SetNull( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N = 0;
         return  ;
      }

      public bool gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtEmergenciaBitacora_Emergenciaid = (Guid)(Guid.Empty);
         gxTv_SdtEmergenciaBitacora_Emergenciafecha = (DateTime)(DateTime.MinValue);
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente = (Guid)(Guid.Empty);
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre = "";
         gxTv_SdtEmergenciaBitacora_Emergenciacoach = (Guid)(Guid.Empty);
         gxTv_SdtEmergenciaBitacora_Emergencialongitud = "";
         gxTv_SdtEmergenciaBitacora_Emergencialatitud = "";
         gxTv_SdtEmergenciaBitacora_Emergenciamensaje = "";
         gxTv_SdtEmergenciaBitacora_Mode = "";
         gxTv_SdtEmergenciaBitacora_Emergenciaid_Z = (Guid)(Guid.Empty);
         gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z = (Guid)(Guid.Empty);
         gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z = "";
         gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z = (Guid)(Guid.Empty);
         datetime_STZ = (DateTime)(DateTime.MinValue);
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "emergenciabitacora", "GeneXus.Programs.emergenciabitacora_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtEmergenciaBitacora_Emergenciaestado ;
      private short gxTv_SdtEmergenciaBitacora_Initialized ;
      private short gxTv_SdtEmergenciaBitacora_Emergenciaestado_Z ;
      private short gxTv_SdtEmergenciaBitacora_Emergenciapaciente_N ;
      private short gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_N ;
      private short gxTv_SdtEmergenciaBitacora_Emergenciacoach_N ;
      private short gxTv_SdtEmergenciaBitacora_Emergencialongitud_N ;
      private short gxTv_SdtEmergenciaBitacora_Emergencialatitud_N ;
      private short gxTv_SdtEmergenciaBitacora_Emergenciamensaje_N ;
      private String gxTv_SdtEmergenciaBitacora_Mode ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtEmergenciaBitacora_Emergenciafecha ;
      private DateTime gxTv_SdtEmergenciaBitacora_Emergenciafecha_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtEmergenciaBitacora_Emergencialongitud ;
      private String gxTv_SdtEmergenciaBitacora_Emergencialatitud ;
      private String gxTv_SdtEmergenciaBitacora_Emergenciamensaje ;
      private String gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre ;
      private String gxTv_SdtEmergenciaBitacora_Emergenciapacientenombre_Z ;
      private Guid gxTv_SdtEmergenciaBitacora_Emergenciaid ;
      private Guid gxTv_SdtEmergenciaBitacora_Emergenciapaciente ;
      private Guid gxTv_SdtEmergenciaBitacora_Emergenciacoach ;
      private Guid gxTv_SdtEmergenciaBitacora_Emergenciaid_Z ;
      private Guid gxTv_SdtEmergenciaBitacora_Emergenciapaciente_Z ;
      private Guid gxTv_SdtEmergenciaBitacora_Emergenciacoach_Z ;
   }

   [DataContract(Name = @"EmergenciaBitacora", Namespace = "PACYE2")]
   public class SdtEmergenciaBitacora_RESTInterface : GxGenericCollectionItem<SdtEmergenciaBitacora>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtEmergenciaBitacora_RESTInterface( ) : base()
      {
      }

      public SdtEmergenciaBitacora_RESTInterface( SdtEmergenciaBitacora psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "EmergenciaID" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Emergenciaid
      {
         get {
            return sdt.gxTpr_Emergenciaid ;
         }

         set {
            sdt.gxTpr_Emergenciaid = (Guid)(value);
         }

      }

      [DataMember( Name = "EmergenciaFecha" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Emergenciafecha
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Emergenciafecha) ;
         }

         set {
            sdt.gxTpr_Emergenciafecha = DateTimeUtil.CToT2( value);
         }

      }

      [DataMember( Name = "EmergenciaEstado" , Order = 2 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Emergenciaestado
      {
         get {
            return sdt.gxTpr_Emergenciaestado ;
         }

         set {
            sdt.gxTpr_Emergenciaestado = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "EmergenciaPaciente" , Order = 3 )]
      [GxSeudo()]
      public Guid gxTpr_Emergenciapaciente
      {
         get {
            return sdt.gxTpr_Emergenciapaciente ;
         }

         set {
            sdt.gxTpr_Emergenciapaciente = (Guid)(value);
         }

      }

      [DataMember( Name = "EmergenciaPacienteNombre" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Emergenciapacientenombre
      {
         get {
            return sdt.gxTpr_Emergenciapacientenombre ;
         }

         set {
            sdt.gxTpr_Emergenciapacientenombre = value;
         }

      }

      [DataMember( Name = "EmergenciaCoach" , Order = 5 )]
      [GxSeudo()]
      public Guid gxTpr_Emergenciacoach
      {
         get {
            return sdt.gxTpr_Emergenciacoach ;
         }

         set {
            sdt.gxTpr_Emergenciacoach = (Guid)(value);
         }

      }

      [DataMember( Name = "EmergenciaLongitud" , Order = 6 )]
      public String gxTpr_Emergencialongitud
      {
         get {
            return sdt.gxTpr_Emergencialongitud ;
         }

         set {
            sdt.gxTpr_Emergencialongitud = value;
         }

      }

      [DataMember( Name = "EmergenciaLatitud" , Order = 7 )]
      public String gxTpr_Emergencialatitud
      {
         get {
            return sdt.gxTpr_Emergencialatitud ;
         }

         set {
            sdt.gxTpr_Emergencialatitud = value;
         }

      }

      [DataMember( Name = "EmergenciaMensaje" , Order = 8 )]
      public String gxTpr_Emergenciamensaje
      {
         get {
            return sdt.gxTpr_Emergenciamensaje ;
         }

         set {
            sdt.gxTpr_Emergenciamensaje = value;
         }

      }

      public SdtEmergenciaBitacora sdt
      {
         get {
            return (SdtEmergenciaBitacora)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtEmergenciaBitacora() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 9 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
