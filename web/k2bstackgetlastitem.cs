/*
               File: K2BStackGetLastItem
        Description: Get Last Item of the Stack
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:34.98
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bstackgetlastitem : GXProcedure
   {
      public k2bstackgetlastitem( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bstackgetlastitem( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack ,
                           out SdtK2BStack_K2BStackItem aP1_StackItem )
      {
         this.AV9Stack = aP0_Stack;
         this.AV8StackItem = new SdtK2BStack_K2BStackItem(context) ;
         initialize();
         executePrivate();
         aP1_StackItem=this.AV8StackItem;
      }

      public SdtK2BStack_K2BStackItem executeUdp( GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack )
      {
         this.AV9Stack = aP0_Stack;
         this.AV8StackItem = new SdtK2BStack_K2BStackItem(context) ;
         initialize();
         executePrivate();
         aP1_StackItem=this.AV8StackItem;
         return AV8StackItem ;
      }

      public void executeSubmit( GXBaseCollection<SdtK2BStack_K2BStackItem> aP0_Stack ,
                                 out SdtK2BStack_K2BStackItem aP1_StackItem )
      {
         k2bstackgetlastitem objk2bstackgetlastitem;
         objk2bstackgetlastitem = new k2bstackgetlastitem();
         objk2bstackgetlastitem.AV9Stack = aP0_Stack;
         objk2bstackgetlastitem.AV8StackItem = new SdtK2BStack_K2BStackItem(context) ;
         objk2bstackgetlastitem.context.SetSubmitInitialConfig(context);
         objk2bstackgetlastitem.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bstackgetlastitem);
         aP1_StackItem=this.AV8StackItem;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bstackgetlastitem)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV9Stack.Count > 0 )
         {
            AV8StackItem = ((SdtK2BStack_K2BStackItem)AV9Stack.Item(AV9Stack.Count));
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private SdtK2BStack_K2BStackItem aP1_StackItem ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> AV9Stack ;
      private SdtK2BStack_K2BStackItem AV8StackItem ;
   }

}
