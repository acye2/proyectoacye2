/*
               File: type_SdtPersona
        Description: Persona
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:46.95
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Persona" )]
   [XmlType(TypeName =  "Persona" , Namespace = "PACYE2" )]
   [Serializable]
   public class SdtPersona : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtPersona( )
      {
      }

      public SdtPersona( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( Guid AV1PersonaID )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(Guid)AV1PersonaID});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"PersonaID", typeof(Guid)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Persona");
         metadata.Set("BT", "Persona");
         metadata.Set("PK", "[ \"PersonaID\" ]");
         metadata.Set("PKAssigned", "[ \"PersonaID\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GxStringCollection StateAttributes( )
      {
         GxStringCollection state = new GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Personaid_Z");
         state.Add("gxTpr_Personapnombre_Z");
         state.Add("gxTpr_Personasnombre_Z");
         state.Add("gxTpr_Personapapellido_Z");
         state.Add("gxTpr_Personasapellido_Z");
         state.Add("gxTpr_Personasexo_Z");
         state.Add("gxTpr_Personadpi_Z");
         state.Add("gxTpr_Personatipo_Z");
         state.Add("gxTpr_Personafechanacimiento_Z_Nullable");
         state.Add("gxTpr_Personapacienteorigensordera_Z");
         state.Add("gxTpr_Personaemail_Z");
         state.Add("gxTpr_Personasnombre_N");
         state.Add("gxTpr_Personasapellido_N");
         state.Add("gxTpr_Personasexo_N");
         state.Add("gxTpr_Personadpi_N");
         state.Add("gxTpr_Personafechanacimiento_N");
         state.Add("gxTpr_Personapacienteorigensordera_N");
         state.Add("gxTpr_Personaemail_N");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         SdtPersona sdt ;
         sdt = (SdtPersona)(source);
         gxTv_SdtPersona_Personaid = sdt.gxTv_SdtPersona_Personaid ;
         gxTv_SdtPersona_Personapnombre = sdt.gxTv_SdtPersona_Personapnombre ;
         gxTv_SdtPersona_Personasnombre = sdt.gxTv_SdtPersona_Personasnombre ;
         gxTv_SdtPersona_Personapapellido = sdt.gxTv_SdtPersona_Personapapellido ;
         gxTv_SdtPersona_Personasapellido = sdt.gxTv_SdtPersona_Personasapellido ;
         gxTv_SdtPersona_Personasexo = sdt.gxTv_SdtPersona_Personasexo ;
         gxTv_SdtPersona_Personadpi = sdt.gxTv_SdtPersona_Personadpi ;
         gxTv_SdtPersona_Personatipo = sdt.gxTv_SdtPersona_Personatipo ;
         gxTv_SdtPersona_Personafechanacimiento = sdt.gxTv_SdtPersona_Personafechanacimiento ;
         gxTv_SdtPersona_Personapacienteorigensordera = sdt.gxTv_SdtPersona_Personapacienteorigensordera ;
         gxTv_SdtPersona_Personaemail = sdt.gxTv_SdtPersona_Personaemail ;
         gxTv_SdtPersona_Mode = sdt.gxTv_SdtPersona_Mode ;
         gxTv_SdtPersona_Initialized = sdt.gxTv_SdtPersona_Initialized ;
         gxTv_SdtPersona_Personaid_Z = sdt.gxTv_SdtPersona_Personaid_Z ;
         gxTv_SdtPersona_Personapnombre_Z = sdt.gxTv_SdtPersona_Personapnombre_Z ;
         gxTv_SdtPersona_Personasnombre_Z = sdt.gxTv_SdtPersona_Personasnombre_Z ;
         gxTv_SdtPersona_Personapapellido_Z = sdt.gxTv_SdtPersona_Personapapellido_Z ;
         gxTv_SdtPersona_Personasapellido_Z = sdt.gxTv_SdtPersona_Personasapellido_Z ;
         gxTv_SdtPersona_Personasexo_Z = sdt.gxTv_SdtPersona_Personasexo_Z ;
         gxTv_SdtPersona_Personadpi_Z = sdt.gxTv_SdtPersona_Personadpi_Z ;
         gxTv_SdtPersona_Personatipo_Z = sdt.gxTv_SdtPersona_Personatipo_Z ;
         gxTv_SdtPersona_Personafechanacimiento_Z = sdt.gxTv_SdtPersona_Personafechanacimiento_Z ;
         gxTv_SdtPersona_Personapacienteorigensordera_Z = sdt.gxTv_SdtPersona_Personapacienteorigensordera_Z ;
         gxTv_SdtPersona_Personaemail_Z = sdt.gxTv_SdtPersona_Personaemail_Z ;
         gxTv_SdtPersona_Personasnombre_N = sdt.gxTv_SdtPersona_Personasnombre_N ;
         gxTv_SdtPersona_Personasapellido_N = sdt.gxTv_SdtPersona_Personasapellido_N ;
         gxTv_SdtPersona_Personasexo_N = sdt.gxTv_SdtPersona_Personasexo_N ;
         gxTv_SdtPersona_Personadpi_N = sdt.gxTv_SdtPersona_Personadpi_N ;
         gxTv_SdtPersona_Personafechanacimiento_N = sdt.gxTv_SdtPersona_Personafechanacimiento_N ;
         gxTv_SdtPersona_Personapacienteorigensordera_N = sdt.gxTv_SdtPersona_Personapacienteorigensordera_N ;
         gxTv_SdtPersona_Personaemail_N = sdt.gxTv_SdtPersona_Personaemail_N ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("PersonaID", gxTv_SdtPersona_Personaid, false);
         AddObjectProperty("PersonaPNombre", gxTv_SdtPersona_Personapnombre, false);
         AddObjectProperty("PersonaSNombre", gxTv_SdtPersona_Personasnombre, false);
         AddObjectProperty("PersonaSNombre_N", gxTv_SdtPersona_Personasnombre_N, false);
         AddObjectProperty("PersonaPApellido", gxTv_SdtPersona_Personapapellido, false);
         AddObjectProperty("PersonaSApellido", gxTv_SdtPersona_Personasapellido, false);
         AddObjectProperty("PersonaSApellido_N", gxTv_SdtPersona_Personasapellido_N, false);
         AddObjectProperty("PersonaSexo", gxTv_SdtPersona_Personasexo, false);
         AddObjectProperty("PersonaSexo_N", gxTv_SdtPersona_Personasexo_N, false);
         AddObjectProperty("PersonaDPI", gxTv_SdtPersona_Personadpi, false);
         AddObjectProperty("PersonaDPI_N", gxTv_SdtPersona_Personadpi_N, false);
         AddObjectProperty("PersonaTipo", gxTv_SdtPersona_Personatipo, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtPersona_Personafechanacimiento)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtPersona_Personafechanacimiento)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtPersona_Personafechanacimiento)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("PersonaFechaNacimiento", sDateCnv, false);
         AddObjectProperty("PersonaFechaNacimiento_N", gxTv_SdtPersona_Personafechanacimiento_N, false);
         AddObjectProperty("PersonaPacienteOrigenSordera", gxTv_SdtPersona_Personapacienteorigensordera, false);
         AddObjectProperty("PersonaPacienteOrigenSordera_N", gxTv_SdtPersona_Personapacienteorigensordera_N, false);
         AddObjectProperty("PersonaEMail", gxTv_SdtPersona_Personaemail, false);
         AddObjectProperty("PersonaEMail_N", gxTv_SdtPersona_Personaemail_N, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtPersona_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtPersona_Initialized, false);
            AddObjectProperty("PersonaID_Z", gxTv_SdtPersona_Personaid_Z, false);
            AddObjectProperty("PersonaPNombre_Z", gxTv_SdtPersona_Personapnombre_Z, false);
            AddObjectProperty("PersonaSNombre_Z", gxTv_SdtPersona_Personasnombre_Z, false);
            AddObjectProperty("PersonaPApellido_Z", gxTv_SdtPersona_Personapapellido_Z, false);
            AddObjectProperty("PersonaSApellido_Z", gxTv_SdtPersona_Personasapellido_Z, false);
            AddObjectProperty("PersonaSexo_Z", gxTv_SdtPersona_Personasexo_Z, false);
            AddObjectProperty("PersonaDPI_Z", gxTv_SdtPersona_Personadpi_Z, false);
            AddObjectProperty("PersonaTipo_Z", gxTv_SdtPersona_Personatipo_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtPersona_Personafechanacimiento_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtPersona_Personafechanacimiento_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtPersona_Personafechanacimiento_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("PersonaFechaNacimiento_Z", sDateCnv, false);
            AddObjectProperty("PersonaPacienteOrigenSordera_Z", gxTv_SdtPersona_Personapacienteorigensordera_Z, false);
            AddObjectProperty("PersonaEMail_Z", gxTv_SdtPersona_Personaemail_Z, false);
            AddObjectProperty("PersonaSNombre_N", gxTv_SdtPersona_Personasnombre_N, false);
            AddObjectProperty("PersonaSApellido_N", gxTv_SdtPersona_Personasapellido_N, false);
            AddObjectProperty("PersonaSexo_N", gxTv_SdtPersona_Personasexo_N, false);
            AddObjectProperty("PersonaDPI_N", gxTv_SdtPersona_Personadpi_N, false);
            AddObjectProperty("PersonaFechaNacimiento_N", gxTv_SdtPersona_Personafechanacimiento_N, false);
            AddObjectProperty("PersonaPacienteOrigenSordera_N", gxTv_SdtPersona_Personapacienteorigensordera_N, false);
            AddObjectProperty("PersonaEMail_N", gxTv_SdtPersona_Personaemail_N, false);
         }
         return  ;
      }

      public void UpdateDirties( SdtPersona sdt )
      {
         if ( sdt.IsDirty("PersonaID") )
         {
            gxTv_SdtPersona_Personaid = sdt.gxTv_SdtPersona_Personaid ;
         }
         if ( sdt.IsDirty("PersonaPNombre") )
         {
            gxTv_SdtPersona_Personapnombre = sdt.gxTv_SdtPersona_Personapnombre ;
         }
         if ( sdt.IsDirty("PersonaSNombre") )
         {
            gxTv_SdtPersona_Personasnombre = sdt.gxTv_SdtPersona_Personasnombre ;
         }
         if ( sdt.IsDirty("PersonaPApellido") )
         {
            gxTv_SdtPersona_Personapapellido = sdt.gxTv_SdtPersona_Personapapellido ;
         }
         if ( sdt.IsDirty("PersonaSApellido") )
         {
            gxTv_SdtPersona_Personasapellido = sdt.gxTv_SdtPersona_Personasapellido ;
         }
         if ( sdt.IsDirty("PersonaSexo") )
         {
            gxTv_SdtPersona_Personasexo = sdt.gxTv_SdtPersona_Personasexo ;
         }
         if ( sdt.IsDirty("PersonaDPI") )
         {
            gxTv_SdtPersona_Personadpi = sdt.gxTv_SdtPersona_Personadpi ;
         }
         if ( sdt.IsDirty("PersonaTipo") )
         {
            gxTv_SdtPersona_Personatipo = sdt.gxTv_SdtPersona_Personatipo ;
         }
         if ( sdt.IsDirty("PersonaFechaNacimiento") )
         {
            gxTv_SdtPersona_Personafechanacimiento = sdt.gxTv_SdtPersona_Personafechanacimiento ;
         }
         if ( sdt.IsDirty("PersonaPacienteOrigenSordera") )
         {
            gxTv_SdtPersona_Personapacienteorigensordera = sdt.gxTv_SdtPersona_Personapacienteorigensordera ;
         }
         if ( sdt.IsDirty("PersonaEMail") )
         {
            gxTv_SdtPersona_Personaemail = sdt.gxTv_SdtPersona_Personaemail ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "PersonaID" )]
      [  XmlElement( ElementName = "PersonaID"   )]
      public Guid gxTpr_Personaid
      {
         get {
            return gxTv_SdtPersona_Personaid ;
         }

         set {
            if ( gxTv_SdtPersona_Personaid != value )
            {
               gxTv_SdtPersona_Mode = "INS";
               this.gxTv_SdtPersona_Personaid_Z_SetNull( );
               this.gxTv_SdtPersona_Personapnombre_Z_SetNull( );
               this.gxTv_SdtPersona_Personasnombre_Z_SetNull( );
               this.gxTv_SdtPersona_Personapapellido_Z_SetNull( );
               this.gxTv_SdtPersona_Personasapellido_Z_SetNull( );
               this.gxTv_SdtPersona_Personasexo_Z_SetNull( );
               this.gxTv_SdtPersona_Personadpi_Z_SetNull( );
               this.gxTv_SdtPersona_Personatipo_Z_SetNull( );
               this.gxTv_SdtPersona_Personafechanacimiento_Z_SetNull( );
               this.gxTv_SdtPersona_Personapacienteorigensordera_Z_SetNull( );
               this.gxTv_SdtPersona_Personaemail_Z_SetNull( );
            }
            gxTv_SdtPersona_Personaid = (Guid)(value);
            SetDirty("Personaid");
         }

      }

      [  SoapElement( ElementName = "PersonaPNombre" )]
      [  XmlElement( ElementName = "PersonaPNombre"   )]
      public String gxTpr_Personapnombre
      {
         get {
            return gxTv_SdtPersona_Personapnombre ;
         }

         set {
            gxTv_SdtPersona_Personapnombre = value;
            SetDirty("Personapnombre");
         }

      }

      [  SoapElement( ElementName = "PersonaSNombre" )]
      [  XmlElement( ElementName = "PersonaSNombre"   )]
      public String gxTpr_Personasnombre
      {
         get {
            return gxTv_SdtPersona_Personasnombre ;
         }

         set {
            gxTv_SdtPersona_Personasnombre_N = 0;
            gxTv_SdtPersona_Personasnombre = value;
            SetDirty("Personasnombre");
         }

      }

      public void gxTv_SdtPersona_Personasnombre_SetNull( )
      {
         gxTv_SdtPersona_Personasnombre_N = 1;
         gxTv_SdtPersona_Personasnombre = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personasnombre_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaPApellido" )]
      [  XmlElement( ElementName = "PersonaPApellido"   )]
      public String gxTpr_Personapapellido
      {
         get {
            return gxTv_SdtPersona_Personapapellido ;
         }

         set {
            gxTv_SdtPersona_Personapapellido = value;
            SetDirty("Personapapellido");
         }

      }

      [  SoapElement( ElementName = "PersonaSApellido" )]
      [  XmlElement( ElementName = "PersonaSApellido"   )]
      public String gxTpr_Personasapellido
      {
         get {
            return gxTv_SdtPersona_Personasapellido ;
         }

         set {
            gxTv_SdtPersona_Personasapellido_N = 0;
            gxTv_SdtPersona_Personasapellido = value;
            SetDirty("Personasapellido");
         }

      }

      public void gxTv_SdtPersona_Personasapellido_SetNull( )
      {
         gxTv_SdtPersona_Personasapellido_N = 1;
         gxTv_SdtPersona_Personasapellido = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personasapellido_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaSexo" )]
      [  XmlElement( ElementName = "PersonaSexo"   )]
      public String gxTpr_Personasexo
      {
         get {
            return gxTv_SdtPersona_Personasexo ;
         }

         set {
            gxTv_SdtPersona_Personasexo_N = 0;
            gxTv_SdtPersona_Personasexo = value;
            SetDirty("Personasexo");
         }

      }

      public void gxTv_SdtPersona_Personasexo_SetNull( )
      {
         gxTv_SdtPersona_Personasexo_N = 1;
         gxTv_SdtPersona_Personasexo = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personasexo_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaDPI" )]
      [  XmlElement( ElementName = "PersonaDPI"   )]
      public String gxTpr_Personadpi
      {
         get {
            return gxTv_SdtPersona_Personadpi ;
         }

         set {
            gxTv_SdtPersona_Personadpi_N = 0;
            gxTv_SdtPersona_Personadpi = value;
            SetDirty("Personadpi");
         }

      }

      public void gxTv_SdtPersona_Personadpi_SetNull( )
      {
         gxTv_SdtPersona_Personadpi_N = 1;
         gxTv_SdtPersona_Personadpi = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personadpi_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaTipo" )]
      [  XmlElement( ElementName = "PersonaTipo"   )]
      public short gxTpr_Personatipo
      {
         get {
            return gxTv_SdtPersona_Personatipo ;
         }

         set {
            gxTv_SdtPersona_Personatipo = value;
            SetDirty("Personatipo");
         }

      }

      [  SoapElement( ElementName = "PersonaFechaNacimiento" )]
      [  XmlElement( ElementName = "PersonaFechaNacimiento"  , IsNullable=true )]
      public string gxTpr_Personafechanacimiento_Nullable
      {
         get {
            if ( gxTv_SdtPersona_Personafechanacimiento == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtPersona_Personafechanacimiento).value ;
         }

         set {
            gxTv_SdtPersona_Personafechanacimiento_N = 0;
            if (String.IsNullOrEmpty(value) || value == GxDateString.NullValue )
               gxTv_SdtPersona_Personafechanacimiento = DateTime.MinValue;
            else
               gxTv_SdtPersona_Personafechanacimiento = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Personafechanacimiento
      {
         get {
            return gxTv_SdtPersona_Personafechanacimiento ;
         }

         set {
            gxTv_SdtPersona_Personafechanacimiento_N = 0;
            gxTv_SdtPersona_Personafechanacimiento = value;
            SetDirty("Personafechanacimiento");
         }

      }

      public void gxTv_SdtPersona_Personafechanacimiento_SetNull( )
      {
         gxTv_SdtPersona_Personafechanacimiento_N = 1;
         gxTv_SdtPersona_Personafechanacimiento = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtPersona_Personafechanacimiento_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaPacienteOrigenSordera" )]
      [  XmlElement( ElementName = "PersonaPacienteOrigenSordera"   )]
      public short gxTpr_Personapacienteorigensordera
      {
         get {
            return gxTv_SdtPersona_Personapacienteorigensordera ;
         }

         set {
            gxTv_SdtPersona_Personapacienteorigensordera_N = 0;
            gxTv_SdtPersona_Personapacienteorigensordera = value;
            SetDirty("Personapacienteorigensordera");
         }

      }

      public void gxTv_SdtPersona_Personapacienteorigensordera_SetNull( )
      {
         gxTv_SdtPersona_Personapacienteorigensordera_N = 1;
         gxTv_SdtPersona_Personapacienteorigensordera = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personapacienteorigensordera_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaEMail" )]
      [  XmlElement( ElementName = "PersonaEMail"   )]
      public String gxTpr_Personaemail
      {
         get {
            return gxTv_SdtPersona_Personaemail ;
         }

         set {
            gxTv_SdtPersona_Personaemail_N = 0;
            gxTv_SdtPersona_Personaemail = value;
            SetDirty("Personaemail");
         }

      }

      public void gxTv_SdtPersona_Personaemail_SetNull( )
      {
         gxTv_SdtPersona_Personaemail_N = 1;
         gxTv_SdtPersona_Personaemail = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personaemail_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtPersona_Mode ;
         }

         set {
            gxTv_SdtPersona_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_SdtPersona_Mode_SetNull( )
      {
         gxTv_SdtPersona_Mode = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtPersona_Initialized ;
         }

         set {
            gxTv_SdtPersona_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_SdtPersona_Initialized_SetNull( )
      {
         gxTv_SdtPersona_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaID_Z" )]
      [  XmlElement( ElementName = "PersonaID_Z"   )]
      public Guid gxTpr_Personaid_Z
      {
         get {
            return gxTv_SdtPersona_Personaid_Z ;
         }

         set {
            gxTv_SdtPersona_Personaid_Z = (Guid)(value);
            SetDirty("Personaid_Z");
         }

      }

      public void gxTv_SdtPersona_Personaid_Z_SetNull( )
      {
         gxTv_SdtPersona_Personaid_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtPersona_Personaid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaPNombre_Z" )]
      [  XmlElement( ElementName = "PersonaPNombre_Z"   )]
      public String gxTpr_Personapnombre_Z
      {
         get {
            return gxTv_SdtPersona_Personapnombre_Z ;
         }

         set {
            gxTv_SdtPersona_Personapnombre_Z = value;
            SetDirty("Personapnombre_Z");
         }

      }

      public void gxTv_SdtPersona_Personapnombre_Z_SetNull( )
      {
         gxTv_SdtPersona_Personapnombre_Z = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personapnombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaSNombre_Z" )]
      [  XmlElement( ElementName = "PersonaSNombre_Z"   )]
      public String gxTpr_Personasnombre_Z
      {
         get {
            return gxTv_SdtPersona_Personasnombre_Z ;
         }

         set {
            gxTv_SdtPersona_Personasnombre_Z = value;
            SetDirty("Personasnombre_Z");
         }

      }

      public void gxTv_SdtPersona_Personasnombre_Z_SetNull( )
      {
         gxTv_SdtPersona_Personasnombre_Z = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personasnombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaPApellido_Z" )]
      [  XmlElement( ElementName = "PersonaPApellido_Z"   )]
      public String gxTpr_Personapapellido_Z
      {
         get {
            return gxTv_SdtPersona_Personapapellido_Z ;
         }

         set {
            gxTv_SdtPersona_Personapapellido_Z = value;
            SetDirty("Personapapellido_Z");
         }

      }

      public void gxTv_SdtPersona_Personapapellido_Z_SetNull( )
      {
         gxTv_SdtPersona_Personapapellido_Z = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personapapellido_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaSApellido_Z" )]
      [  XmlElement( ElementName = "PersonaSApellido_Z"   )]
      public String gxTpr_Personasapellido_Z
      {
         get {
            return gxTv_SdtPersona_Personasapellido_Z ;
         }

         set {
            gxTv_SdtPersona_Personasapellido_Z = value;
            SetDirty("Personasapellido_Z");
         }

      }

      public void gxTv_SdtPersona_Personasapellido_Z_SetNull( )
      {
         gxTv_SdtPersona_Personasapellido_Z = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personasapellido_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaSexo_Z" )]
      [  XmlElement( ElementName = "PersonaSexo_Z"   )]
      public String gxTpr_Personasexo_Z
      {
         get {
            return gxTv_SdtPersona_Personasexo_Z ;
         }

         set {
            gxTv_SdtPersona_Personasexo_Z = value;
            SetDirty("Personasexo_Z");
         }

      }

      public void gxTv_SdtPersona_Personasexo_Z_SetNull( )
      {
         gxTv_SdtPersona_Personasexo_Z = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personasexo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaDPI_Z" )]
      [  XmlElement( ElementName = "PersonaDPI_Z"   )]
      public String gxTpr_Personadpi_Z
      {
         get {
            return gxTv_SdtPersona_Personadpi_Z ;
         }

         set {
            gxTv_SdtPersona_Personadpi_Z = value;
            SetDirty("Personadpi_Z");
         }

      }

      public void gxTv_SdtPersona_Personadpi_Z_SetNull( )
      {
         gxTv_SdtPersona_Personadpi_Z = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personadpi_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaTipo_Z" )]
      [  XmlElement( ElementName = "PersonaTipo_Z"   )]
      public short gxTpr_Personatipo_Z
      {
         get {
            return gxTv_SdtPersona_Personatipo_Z ;
         }

         set {
            gxTv_SdtPersona_Personatipo_Z = value;
            SetDirty("Personatipo_Z");
         }

      }

      public void gxTv_SdtPersona_Personatipo_Z_SetNull( )
      {
         gxTv_SdtPersona_Personatipo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personatipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaFechaNacimiento_Z" )]
      [  XmlElement( ElementName = "PersonaFechaNacimiento_Z"  , IsNullable=true )]
      public string gxTpr_Personafechanacimiento_Z_Nullable
      {
         get {
            if ( gxTv_SdtPersona_Personafechanacimiento_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtPersona_Personafechanacimiento_Z).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDateString.NullValue )
               gxTv_SdtPersona_Personafechanacimiento_Z = DateTime.MinValue;
            else
               gxTv_SdtPersona_Personafechanacimiento_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Personafechanacimiento_Z
      {
         get {
            return gxTv_SdtPersona_Personafechanacimiento_Z ;
         }

         set {
            gxTv_SdtPersona_Personafechanacimiento_Z = value;
            SetDirty("Personafechanacimiento_Z");
         }

      }

      public void gxTv_SdtPersona_Personafechanacimiento_Z_SetNull( )
      {
         gxTv_SdtPersona_Personafechanacimiento_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtPersona_Personafechanacimiento_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaPacienteOrigenSordera_Z" )]
      [  XmlElement( ElementName = "PersonaPacienteOrigenSordera_Z"   )]
      public short gxTpr_Personapacienteorigensordera_Z
      {
         get {
            return gxTv_SdtPersona_Personapacienteorigensordera_Z ;
         }

         set {
            gxTv_SdtPersona_Personapacienteorigensordera_Z = value;
            SetDirty("Personapacienteorigensordera_Z");
         }

      }

      public void gxTv_SdtPersona_Personapacienteorigensordera_Z_SetNull( )
      {
         gxTv_SdtPersona_Personapacienteorigensordera_Z = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personapacienteorigensordera_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaEMail_Z" )]
      [  XmlElement( ElementName = "PersonaEMail_Z"   )]
      public String gxTpr_Personaemail_Z
      {
         get {
            return gxTv_SdtPersona_Personaemail_Z ;
         }

         set {
            gxTv_SdtPersona_Personaemail_Z = value;
            SetDirty("Personaemail_Z");
         }

      }

      public void gxTv_SdtPersona_Personaemail_Z_SetNull( )
      {
         gxTv_SdtPersona_Personaemail_Z = "";
         return  ;
      }

      public bool gxTv_SdtPersona_Personaemail_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaSNombre_N" )]
      [  XmlElement( ElementName = "PersonaSNombre_N"   )]
      public short gxTpr_Personasnombre_N
      {
         get {
            return gxTv_SdtPersona_Personasnombre_N ;
         }

         set {
            gxTv_SdtPersona_Personasnombre_N = value;
            SetDirty("Personasnombre_N");
         }

      }

      public void gxTv_SdtPersona_Personasnombre_N_SetNull( )
      {
         gxTv_SdtPersona_Personasnombre_N = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personasnombre_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaSApellido_N" )]
      [  XmlElement( ElementName = "PersonaSApellido_N"   )]
      public short gxTpr_Personasapellido_N
      {
         get {
            return gxTv_SdtPersona_Personasapellido_N ;
         }

         set {
            gxTv_SdtPersona_Personasapellido_N = value;
            SetDirty("Personasapellido_N");
         }

      }

      public void gxTv_SdtPersona_Personasapellido_N_SetNull( )
      {
         gxTv_SdtPersona_Personasapellido_N = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personasapellido_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaSexo_N" )]
      [  XmlElement( ElementName = "PersonaSexo_N"   )]
      public short gxTpr_Personasexo_N
      {
         get {
            return gxTv_SdtPersona_Personasexo_N ;
         }

         set {
            gxTv_SdtPersona_Personasexo_N = value;
            SetDirty("Personasexo_N");
         }

      }

      public void gxTv_SdtPersona_Personasexo_N_SetNull( )
      {
         gxTv_SdtPersona_Personasexo_N = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personasexo_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaDPI_N" )]
      [  XmlElement( ElementName = "PersonaDPI_N"   )]
      public short gxTpr_Personadpi_N
      {
         get {
            return gxTv_SdtPersona_Personadpi_N ;
         }

         set {
            gxTv_SdtPersona_Personadpi_N = value;
            SetDirty("Personadpi_N");
         }

      }

      public void gxTv_SdtPersona_Personadpi_N_SetNull( )
      {
         gxTv_SdtPersona_Personadpi_N = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personadpi_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaFechaNacimiento_N" )]
      [  XmlElement( ElementName = "PersonaFechaNacimiento_N"   )]
      public short gxTpr_Personafechanacimiento_N
      {
         get {
            return gxTv_SdtPersona_Personafechanacimiento_N ;
         }

         set {
            gxTv_SdtPersona_Personafechanacimiento_N = value;
            SetDirty("Personafechanacimiento_N");
         }

      }

      public void gxTv_SdtPersona_Personafechanacimiento_N_SetNull( )
      {
         gxTv_SdtPersona_Personafechanacimiento_N = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personafechanacimiento_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaPacienteOrigenSordera_N" )]
      [  XmlElement( ElementName = "PersonaPacienteOrigenSordera_N"   )]
      public short gxTpr_Personapacienteorigensordera_N
      {
         get {
            return gxTv_SdtPersona_Personapacienteorigensordera_N ;
         }

         set {
            gxTv_SdtPersona_Personapacienteorigensordera_N = value;
            SetDirty("Personapacienteorigensordera_N");
         }

      }

      public void gxTv_SdtPersona_Personapacienteorigensordera_N_SetNull( )
      {
         gxTv_SdtPersona_Personapacienteorigensordera_N = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personapacienteorigensordera_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "PersonaEMail_N" )]
      [  XmlElement( ElementName = "PersonaEMail_N"   )]
      public short gxTpr_Personaemail_N
      {
         get {
            return gxTv_SdtPersona_Personaemail_N ;
         }

         set {
            gxTv_SdtPersona_Personaemail_N = value;
            SetDirty("Personaemail_N");
         }

      }

      public void gxTv_SdtPersona_Personaemail_N_SetNull( )
      {
         gxTv_SdtPersona_Personaemail_N = 0;
         return  ;
      }

      public bool gxTv_SdtPersona_Personaemail_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtPersona_Personaid = (Guid)(Guid.Empty);
         gxTv_SdtPersona_Personapnombre = "";
         gxTv_SdtPersona_Personasnombre = "";
         gxTv_SdtPersona_Personapapellido = "";
         gxTv_SdtPersona_Personasapellido = "";
         gxTv_SdtPersona_Personasexo = "";
         gxTv_SdtPersona_Personadpi = "";
         gxTv_SdtPersona_Personafechanacimiento = DateTime.MinValue;
         gxTv_SdtPersona_Personaemail = "";
         gxTv_SdtPersona_Mode = "";
         gxTv_SdtPersona_Personaid_Z = (Guid)(Guid.Empty);
         gxTv_SdtPersona_Personapnombre_Z = "";
         gxTv_SdtPersona_Personasnombre_Z = "";
         gxTv_SdtPersona_Personapapellido_Z = "";
         gxTv_SdtPersona_Personasapellido_Z = "";
         gxTv_SdtPersona_Personasexo_Z = "";
         gxTv_SdtPersona_Personadpi_Z = "";
         gxTv_SdtPersona_Personafechanacimiento_Z = DateTime.MinValue;
         gxTv_SdtPersona_Personaemail_Z = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "persona", "GeneXus.Programs.persona_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtPersona_Personatipo ;
      private short gxTv_SdtPersona_Personapacienteorigensordera ;
      private short gxTv_SdtPersona_Initialized ;
      private short gxTv_SdtPersona_Personatipo_Z ;
      private short gxTv_SdtPersona_Personapacienteorigensordera_Z ;
      private short gxTv_SdtPersona_Personasnombre_N ;
      private short gxTv_SdtPersona_Personasapellido_N ;
      private short gxTv_SdtPersona_Personasexo_N ;
      private short gxTv_SdtPersona_Personadpi_N ;
      private short gxTv_SdtPersona_Personafechanacimiento_N ;
      private short gxTv_SdtPersona_Personapacienteorigensordera_N ;
      private short gxTv_SdtPersona_Personaemail_N ;
      private String gxTv_SdtPersona_Personasexo ;
      private String gxTv_SdtPersona_Mode ;
      private String gxTv_SdtPersona_Personasexo_Z ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtPersona_Personafechanacimiento ;
      private DateTime gxTv_SdtPersona_Personafechanacimiento_Z ;
      private String gxTv_SdtPersona_Personapnombre ;
      private String gxTv_SdtPersona_Personasnombre ;
      private String gxTv_SdtPersona_Personapapellido ;
      private String gxTv_SdtPersona_Personasapellido ;
      private String gxTv_SdtPersona_Personadpi ;
      private String gxTv_SdtPersona_Personaemail ;
      private String gxTv_SdtPersona_Personapnombre_Z ;
      private String gxTv_SdtPersona_Personasnombre_Z ;
      private String gxTv_SdtPersona_Personapapellido_Z ;
      private String gxTv_SdtPersona_Personasapellido_Z ;
      private String gxTv_SdtPersona_Personadpi_Z ;
      private String gxTv_SdtPersona_Personaemail_Z ;
      private Guid gxTv_SdtPersona_Personaid ;
      private Guid gxTv_SdtPersona_Personaid_Z ;
   }

   [DataContract(Name = @"Persona", Namespace = "PACYE2")]
   public class SdtPersona_RESTInterface : GxGenericCollectionItem<SdtPersona>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtPersona_RESTInterface( ) : base()
      {
      }

      public SdtPersona_RESTInterface( SdtPersona psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "PersonaID" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Personaid
      {
         get {
            return sdt.gxTpr_Personaid ;
         }

         set {
            sdt.gxTpr_Personaid = (Guid)(value);
         }

      }

      [DataMember( Name = "PersonaPNombre" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Personapnombre
      {
         get {
            return sdt.gxTpr_Personapnombre ;
         }

         set {
            sdt.gxTpr_Personapnombre = value;
         }

      }

      [DataMember( Name = "PersonaSNombre" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Personasnombre
      {
         get {
            return sdt.gxTpr_Personasnombre ;
         }

         set {
            sdt.gxTpr_Personasnombre = value;
         }

      }

      [DataMember( Name = "PersonaPApellido" , Order = 3 )]
      [GxSeudo()]
      public String gxTpr_Personapapellido
      {
         get {
            return sdt.gxTpr_Personapapellido ;
         }

         set {
            sdt.gxTpr_Personapapellido = value;
         }

      }

      [DataMember( Name = "PersonaSApellido" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Personasapellido
      {
         get {
            return sdt.gxTpr_Personasapellido ;
         }

         set {
            sdt.gxTpr_Personasapellido = value;
         }

      }

      [DataMember( Name = "PersonaSexo" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Personasexo
      {
         get {
            return StringUtil.RTrim( sdt.gxTpr_Personasexo) ;
         }

         set {
            sdt.gxTpr_Personasexo = value;
         }

      }

      [DataMember( Name = "PersonaDPI" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Personadpi
      {
         get {
            return sdt.gxTpr_Personadpi ;
         }

         set {
            sdt.gxTpr_Personadpi = value;
         }

      }

      [DataMember( Name = "PersonaTipo" , Order = 7 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Personatipo
      {
         get {
            return sdt.gxTpr_Personatipo ;
         }

         set {
            sdt.gxTpr_Personatipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "PersonaFechaNacimiento" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Personafechanacimiento
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Personafechanacimiento) ;
         }

         set {
            sdt.gxTpr_Personafechanacimiento = DateTimeUtil.CToD2( value);
         }

      }

      [DataMember( Name = "PersonaPacienteOrigenSordera" , Order = 9 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Personapacienteorigensordera
      {
         get {
            return sdt.gxTpr_Personapacienteorigensordera ;
         }

         set {
            sdt.gxTpr_Personapacienteorigensordera = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "PersonaEMail" , Order = 10 )]
      [GxSeudo()]
      public String gxTpr_Personaemail
      {
         get {
            return sdt.gxTpr_Personaemail ;
         }

         set {
            sdt.gxTpr_Personaemail = value;
         }

      }

      public SdtPersona sdt
      {
         get {
            return (SdtPersona)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtPersona() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 11 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
