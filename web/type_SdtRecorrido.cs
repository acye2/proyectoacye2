/*
               File: type_SdtRecorrido
        Description: Recorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:39:0.22
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Recorrido" )]
   [XmlType(TypeName =  "Recorrido" , Namespace = "PACYE2" )]
   [Serializable]
   public class SdtRecorrido : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtRecorrido( )
      {
      }

      public SdtRecorrido( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( Guid AV23RecorridoID )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(Guid)AV23RecorridoID});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"RecorridoID", typeof(Guid)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Recorrido");
         metadata.Set("BT", "Recorrido");
         metadata.Set("PK", "[ \"RecorridoID\" ]");
         metadata.Set("Levels", "[ \"Detalle\" ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GxStringCollection StateAttributes( )
      {
         GxStringCollection state = new GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Recorridoid_Z");
         state.Add("gxTpr_Recorridofecha_Z_Nullable");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         SdtRecorrido sdt ;
         sdt = (SdtRecorrido)(source);
         gxTv_SdtRecorrido_Recorridoid = sdt.gxTv_SdtRecorrido_Recorridoid ;
         gxTv_SdtRecorrido_Recorridofecha = sdt.gxTv_SdtRecorrido_Recorridofecha ;
         gxTv_SdtRecorrido_Detalle = sdt.gxTv_SdtRecorrido_Detalle ;
         gxTv_SdtRecorrido_Mode = sdt.gxTv_SdtRecorrido_Mode ;
         gxTv_SdtRecorrido_Initialized = sdt.gxTv_SdtRecorrido_Initialized ;
         gxTv_SdtRecorrido_Recorridoid_Z = sdt.gxTv_SdtRecorrido_Recorridoid_Z ;
         gxTv_SdtRecorrido_Recorridofecha_Z = sdt.gxTv_SdtRecorrido_Recorridofecha_Z ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("RecorridoID", gxTv_SdtRecorrido_Recorridoid, false);
         datetime_STZ = gxTv_SdtRecorrido_Recorridofecha;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("RecorridoFecha", sDateCnv, false);
         if ( gxTv_SdtRecorrido_Detalle != null )
         {
            AddObjectProperty("Detalle", gxTv_SdtRecorrido_Detalle, includeState);
         }
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtRecorrido_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtRecorrido_Initialized, false);
            AddObjectProperty("RecorridoID_Z", gxTv_SdtRecorrido_Recorridoid_Z, false);
            datetime_STZ = gxTv_SdtRecorrido_Recorridofecha_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("RecorridoFecha_Z", sDateCnv, false);
         }
         return  ;
      }

      public void UpdateDirties( SdtRecorrido sdt )
      {
         if ( sdt.IsDirty("RecorridoID") )
         {
            gxTv_SdtRecorrido_Recorridoid = sdt.gxTv_SdtRecorrido_Recorridoid ;
         }
         if ( sdt.IsDirty("RecorridoFecha") )
         {
            gxTv_SdtRecorrido_Recorridofecha = sdt.gxTv_SdtRecorrido_Recorridofecha ;
         }
         if ( gxTv_SdtRecorrido_Detalle != null )
         {
            GXBCLevelCollection<SdtRecorrido_Detalle> newCollectionDetalle = sdt.gxTpr_Detalle ;
            SdtRecorrido_Detalle currItemDetalle ;
            SdtRecorrido_Detalle newItemDetalle ;
            short idx = 1 ;
            while ( idx <= newCollectionDetalle.Count )
            {
               newItemDetalle = ((SdtRecorrido_Detalle)newCollectionDetalle.Item(idx));
               currItemDetalle = gxTv_SdtRecorrido_Detalle.GetByKey(newItemDetalle.gxTpr_Recorridodetalleid);
               if ( StringUtil.StrCmp(currItemDetalle.gxTpr_Mode, "UPD") == 0 )
               {
                  currItemDetalle.UpdateDirties(newItemDetalle);
                  if ( StringUtil.StrCmp(newItemDetalle.gxTpr_Mode, "DLT") == 0 )
                  {
                     currItemDetalle.gxTpr_Mode = "DLT";
                  }
                  currItemDetalle.gxTpr_Modified = 1;
               }
               else
               {
                  gxTv_SdtRecorrido_Detalle.Add(newItemDetalle, 0);
               }
               idx = (short)(idx+1);
            }
         }
         return  ;
      }

      [  SoapElement( ElementName = "RecorridoID" )]
      [  XmlElement( ElementName = "RecorridoID"   )]
      public Guid gxTpr_Recorridoid
      {
         get {
            return gxTv_SdtRecorrido_Recorridoid ;
         }

         set {
            if ( gxTv_SdtRecorrido_Recorridoid != value )
            {
               gxTv_SdtRecorrido_Mode = "INS";
               this.gxTv_SdtRecorrido_Recorridoid_Z_SetNull( );
               this.gxTv_SdtRecorrido_Recorridofecha_Z_SetNull( );
               if ( gxTv_SdtRecorrido_Detalle != null )
               {
                  GXBCLevelCollection<SdtRecorrido_Detalle> collectionDetalle = gxTv_SdtRecorrido_Detalle ;
                  SdtRecorrido_Detalle currItemDetalle ;
                  short idx = 1 ;
                  while ( idx <= collectionDetalle.Count )
                  {
                     currItemDetalle = ((SdtRecorrido_Detalle)collectionDetalle.Item(idx));
                     currItemDetalle.gxTpr_Mode = "INS";
                     currItemDetalle.gxTpr_Modified = 1;
                     idx = (short)(idx+1);
                  }
               }
            }
            gxTv_SdtRecorrido_Recorridoid = (Guid)(value);
            SetDirty("Recorridoid");
         }

      }

      [  SoapElement( ElementName = "RecorridoFecha" )]
      [  XmlElement( ElementName = "RecorridoFecha"  , IsNullable=true )]
      public string gxTpr_Recorridofecha_Nullable
      {
         get {
            if ( gxTv_SdtRecorrido_Recorridofecha == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtRecorrido_Recorridofecha).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtRecorrido_Recorridofecha = DateTime.MinValue;
            else
               gxTv_SdtRecorrido_Recorridofecha = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Recorridofecha
      {
         get {
            return gxTv_SdtRecorrido_Recorridofecha ;
         }

         set {
            gxTv_SdtRecorrido_Recorridofecha = value;
            SetDirty("Recorridofecha");
         }

      }

      [  SoapElement( ElementName = "Detalle" )]
      [  XmlArray( ElementName = "Detalle"  )]
      [  XmlArrayItemAttribute( ElementName= "Recorrido.Detalle"  , IsNullable=false)]
      public GXBCLevelCollection<SdtRecorrido_Detalle> gxTpr_Detalle_GXBCLevelCollection
      {
         get {
            if ( gxTv_SdtRecorrido_Detalle == null )
            {
               gxTv_SdtRecorrido_Detalle = new GXBCLevelCollection<SdtRecorrido_Detalle>( context, "Recorrido.Detalle", "PACYE2");
            }
            return gxTv_SdtRecorrido_Detalle ;
         }

         set {
            if ( gxTv_SdtRecorrido_Detalle == null )
            {
               gxTv_SdtRecorrido_Detalle = new GXBCLevelCollection<SdtRecorrido_Detalle>( context, "Recorrido.Detalle", "PACYE2");
            }
            gxTv_SdtRecorrido_Detalle = value;
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public GXBCLevelCollection<SdtRecorrido_Detalle> gxTpr_Detalle
      {
         get {
            if ( gxTv_SdtRecorrido_Detalle == null )
            {
               gxTv_SdtRecorrido_Detalle = new GXBCLevelCollection<SdtRecorrido_Detalle>( context, "Recorrido.Detalle", "PACYE2");
            }
            return gxTv_SdtRecorrido_Detalle ;
         }

         set {
            gxTv_SdtRecorrido_Detalle = value;
            SetDirty("Detalle");
         }

      }

      public void gxTv_SdtRecorrido_Detalle_SetNull( )
      {
         gxTv_SdtRecorrido_Detalle = null;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Detalle_IsNull( )
      {
         if ( gxTv_SdtRecorrido_Detalle == null )
         {
            return true ;
         }
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtRecorrido_Mode ;
         }

         set {
            gxTv_SdtRecorrido_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_SdtRecorrido_Mode_SetNull( )
      {
         gxTv_SdtRecorrido_Mode = "";
         return  ;
      }

      public bool gxTv_SdtRecorrido_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtRecorrido_Initialized ;
         }

         set {
            gxTv_SdtRecorrido_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_SdtRecorrido_Initialized_SetNull( )
      {
         gxTv_SdtRecorrido_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtRecorrido_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RecorridoID_Z" )]
      [  XmlElement( ElementName = "RecorridoID_Z"   )]
      public Guid gxTpr_Recorridoid_Z
      {
         get {
            return gxTv_SdtRecorrido_Recorridoid_Z ;
         }

         set {
            gxTv_SdtRecorrido_Recorridoid_Z = (Guid)(value);
            SetDirty("Recorridoid_Z");
         }

      }

      public void gxTv_SdtRecorrido_Recorridoid_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Recorridoid_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Recorridoid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "RecorridoFecha_Z" )]
      [  XmlElement( ElementName = "RecorridoFecha_Z"  , IsNullable=true )]
      public string gxTpr_Recorridofecha_Z_Nullable
      {
         get {
            if ( gxTv_SdtRecorrido_Recorridofecha_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtRecorrido_Recorridofecha_Z).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtRecorrido_Recorridofecha_Z = DateTime.MinValue;
            else
               gxTv_SdtRecorrido_Recorridofecha_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Recorridofecha_Z
      {
         get {
            return gxTv_SdtRecorrido_Recorridofecha_Z ;
         }

         set {
            gxTv_SdtRecorrido_Recorridofecha_Z = value;
            SetDirty("Recorridofecha_Z");
         }

      }

      public void gxTv_SdtRecorrido_Recorridofecha_Z_SetNull( )
      {
         gxTv_SdtRecorrido_Recorridofecha_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtRecorrido_Recorridofecha_Z_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtRecorrido_Recorridoid = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Recorridofecha = (DateTime)(DateTime.MinValue);
         gxTv_SdtRecorrido_Mode = "";
         gxTv_SdtRecorrido_Recorridoid_Z = (Guid)(Guid.Empty);
         gxTv_SdtRecorrido_Recorridofecha_Z = (DateTime)(DateTime.MinValue);
         datetime_STZ = (DateTime)(DateTime.MinValue);
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "recorrido", "GeneXus.Programs.recorrido_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtRecorrido_Initialized ;
      private String gxTv_SdtRecorrido_Mode ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtRecorrido_Recorridofecha ;
      private DateTime gxTv_SdtRecorrido_Recorridofecha_Z ;
      private DateTime datetime_STZ ;
      private Guid gxTv_SdtRecorrido_Recorridoid ;
      private Guid gxTv_SdtRecorrido_Recorridoid_Z ;
      private GXBCLevelCollection<SdtRecorrido_Detalle> gxTv_SdtRecorrido_Detalle=null ;
   }

   [DataContract(Name = @"Recorrido", Namespace = "PACYE2")]
   public class SdtRecorrido_RESTInterface : GxGenericCollectionItem<SdtRecorrido>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtRecorrido_RESTInterface( ) : base()
      {
      }

      public SdtRecorrido_RESTInterface( SdtRecorrido psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "RecorridoID" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Recorridoid
      {
         get {
            return sdt.gxTpr_Recorridoid ;
         }

         set {
            sdt.gxTpr_Recorridoid = (Guid)(value);
         }

      }

      [DataMember( Name = "RecorridoFecha" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Recorridofecha
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Recorridofecha) ;
         }

         set {
            sdt.gxTpr_Recorridofecha = DateTimeUtil.CToT2( value);
         }

      }

      [DataMember( Name = "Detalle" , Order = 2 )]
      public GxGenericCollection<SdtRecorrido_Detalle_RESTInterface> gxTpr_Detalle
      {
         get {
            return new GxGenericCollection<SdtRecorrido_Detalle_RESTInterface>(sdt.gxTpr_Detalle) ;
         }

         set {
            value.LoadCollection(sdt.gxTpr_Detalle);
         }

      }

      public SdtRecorrido sdt
      {
         get {
            return (SdtRecorrido)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtRecorrido() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 3 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
