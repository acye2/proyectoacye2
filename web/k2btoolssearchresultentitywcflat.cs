/*
               File: K2BToolsSearchResultEntityWCFlat
        Description: K2 BTools Search Result Entity WCFlat
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:28.79
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2btoolssearchresultentitywcflat : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public k2btoolssearchresultentitywcflat( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BFlat");
         }
      }

      public k2btoolssearchresultentitywcflat( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_SearchCriteriaIn ,
                           String aP1_EntityName )
      {
         this.AV26SearchCriteriaIn = aP0_SearchCriteriaIn;
         this.AV11EntityName = aP1_EntityName;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BFlat");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV26SearchCriteriaIn = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SearchCriteriaIn", AV26SearchCriteriaIn);
                  AV11EntityName = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11EntityName", AV11EntityName);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(String)AV26SearchCriteriaIn,(String)AV11EntityName});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Entitiesresultsgrid") == 0 )
               {
                  nRC_GXsfl_13 = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  nGXsfl_13_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
                  sGXsfl_13_idx = GetNextPar( );
                  sPrefix = GetNextPar( );
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxnrEntitiesresultsgrid_newrow( ) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Entitiesresultsgrid") == 0 )
               {
                  AV26SearchCriteriaIn = GetNextPar( );
                  ajax_req_read_hidden_sdt(GetNextPar( ), AV24ResultsEntities);
                  sPrefix = GetNextPar( );
                  init_default_properties( ) ;
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxgrEntitiesresultsgrid_refresh( AV26SearchCriteriaIn, AV24ResultsEntities, sPrefix) ;
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
                  return  ;
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA1B2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV40Pgmname = "K2BToolsSearchResultEntityWCFlat";
               context.Gx_err = 0;
               edtavCtlsearchresultdescriptionlarge_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlsearchresultdescriptionlarge_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlsearchresultdescriptionlarge_Enabled), 5, 0)), !bGXsfl_13_Refreshing);
               edtavPreviouspage_action_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPreviouspage_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPreviouspage_action_Enabled), 5, 0)), true);
               edtavNextpage_action_Enabled = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNextpage_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNextpage_action_Enabled), 5, 0)), true);
               WS1B2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "K2 BTools Search Result Entity WCFlat") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171522892", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body ") ;
            bodyStyle = "";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("k2btoolssearchresultentitywcflat.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV26SearchCriteriaIn)) + "," + UrlEncode(StringUtil.RTrim(AV11EntityName))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Class", "form-horizontal Form", true);
         }
         else
         {
            bool toggleHtmlOutput = isOutputEnabled( ) ;
            if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
            }
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            if ( toggleHtmlOutput )
            {
               if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableOutput();
                  }
               }
            }
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"Resultsentities", AV24ResultsEntities);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"Resultsentities", AV24ResultsEntities);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"nRC_GXsfl_13", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_13), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV26SearchCriteriaIn", StringUtil.RTrim( wcpOAV26SearchCriteriaIn));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV11EntityName", StringUtil.RTrim( wcpOAV11EntityName));
         GxWebStd.gx_hidden_field( context, sPrefix+"vSEARCHCRITERIAIN", StringUtil.RTrim( AV26SearchCriteriaIn));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, sPrefix+"vRESULTSENTITIES", AV24ResultsEntities);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt(sPrefix+"vRESULTSENTITIES", AV24ResultsEntities);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"vENTITYNAME", StringUtil.RTrim( AV11EntityName));
         GxWebStd.gx_hidden_field( context, sPrefix+"vENTITYITEMSPROCESSED", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV9EntityItemsProcessed), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vPENDINGITEMSEXIST", AV21PendingItemsExist);
      }

      protected void RenderHtmlCloseForm1B2( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && ( context.isAjaxRequest( ) || context.isSpaRequest( ) ) )
         {
            context.AddJavascriptSource("k2btoolssearchresultentitywcflat.js", "?201811171522896", false);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            SendWebComponentState();
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "K2BToolsSearchResultEntityWCFlat" ;
      }

      public override String GetPgmdesc( )
      {
         return "K2 BTools Search Result Entity WCFlat" ;
      }

      protected void WB1B0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "k2btoolssearchresultentitywcflat.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_SearchResultContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table1_6_1B2( true) ;
         }
         else
         {
            wb_table1_6_1B2( false) ;
         }
         return  ;
      }

      protected void wb_table1_6_1B2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            EntitiesresultsgridContainer.SetIsFreestyle(true);
            EntitiesresultsgridContainer.SetWrapped(nGXWrapped);
            if ( EntitiesresultsgridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+sPrefix+"EntitiesresultsgridContainer"+"DivS\" data-gxgridid=\"13\">") ;
               sStyleString = "";
               if ( subEntitiesresultsgrid_Visible == 0 )
               {
                  sStyleString = sStyleString + "display:none;";
               }
               GxWebStd.gx_table_start( context, subEntitiesresultsgrid_Internalname, subEntitiesresultsgrid_Internalname, "", "K2BTools_SearchGrid", 0, "", "", 1, 2, sStyleString, "", 0);
               EntitiesresultsgridContainer.AddObjectProperty("GridName", "Entitiesresultsgrid");
            }
            else
            {
               EntitiesresultsgridContainer.AddObjectProperty("GridName", "Entitiesresultsgrid");
               EntitiesresultsgridContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Visible), 5, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Class", StringUtil.RTrim( "K2BTools_SearchGrid"));
               EntitiesresultsgridContainer.AddObjectProperty("Class", "K2BTools_SearchGrid");
               EntitiesresultsgridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Backcolorstyle), 1, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Visible), 5, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("CmpContext", sPrefix);
               EntitiesresultsgridContainer.AddObjectProperty("InMasterPage", "false");
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV27SearchResultTitle));
               EntitiesresultsgridColumn.AddObjectProperty("Link", StringUtil.RTrim( edtavSearchresulttitle_Link));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               EntitiesresultsgridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavCtlsearchresultdescriptionlarge_Enabled), 5, 0, ".", "")));
               EntitiesresultsgridContainer.AddColumnProperties(EntitiesresultsgridColumn);
               EntitiesresultsgridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Allowselection), 1, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Selectioncolor), 9, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Allowhovering), 1, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Hoveringcolor), 9, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Allowcollapsing), 1, 0, ".", "")));
               EntitiesresultsgridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 13 )
         {
            wbEnd = 0;
            nRC_GXsfl_13 = (short)(nGXsfl_13_idx-1);
            if ( EntitiesresultsgridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               AV37GXV1 = nGXsfl_13_idx;
               if ( subEntitiesresultsgrid_Visible != 0 )
               {
                  sStyleString = "";
               }
               else
               {
                  sStyleString = " style=\"display:none;\"";
               }
               context.WriteHtmlText( "<div id=\""+sPrefix+"EntitiesresultsgridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid(sPrefix+"_"+"Entitiesresultsgrid", EntitiesresultsgridContainer);
               if ( ! isAjaxCallMode( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"EntitiesresultsgridContainerData", EntitiesresultsgridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, sPrefix+"EntitiesresultsgridContainerData"+"V", EntitiesresultsgridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+sPrefix+"EntitiesresultsgridContainerData"+"V"+"\" value='"+EntitiesresultsgridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable1_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_SearchPaginationContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavPreviouspage_action_Internalname, "Previous Page_Action", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavPreviouspage_action_Internalname, StringUtil.RTrim( AV31PreviousPage_Action), StringUtil.RTrim( context.localUtil.Format( AV31PreviousPage_Action, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'E_PREVIOUSPAGE\\'."+"'", "", "", "", "", edtavPreviouspage_action_Jsonclick, 5, "Attribute", "", "", "", "", edtavPreviouspage_action_Visible, edtavPreviouspage_action_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_K2BToolsSearchResultEntityWCFlat.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavNextpage_action_Internalname, "Next Page_Action", "col-sm-3 AttributeLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 41,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavNextpage_action_Internalname, StringUtil.RTrim( AV20NextPage_Action), StringUtil.RTrim( context.localUtil.Format( AV20NextPage_Action, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,41);\"", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'E_NEXTPAGE\\'."+"'", "", "", "", "", edtavNextpage_action_Jsonclick, 5, "Attribute", "", "", "", "", edtavNextpage_action_Visible, edtavNextpage_action_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_K2BToolsSearchResultEntityWCFlat.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", edtavItemstoskipjson_Visible, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavItemstoskipjson_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavItemstoskipjson_Internalname, "Items To Skip JSon", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 46,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavItemstoskipjson_Internalname, StringUtil.RTrim( AV33ItemsToSkipJSon), StringUtil.RTrim( context.localUtil.Format( AV33ItemsToSkipJSon, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,46);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavItemstoskipjson_Jsonclick, 0, "Attribute", "", "", "", "", edtavItemstoskipjson_Visible, edtavItemstoskipjson_Enabled, 0, "text", "", 20, "chr", 1, "row", 20, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_K2BToolsSearchResultEntityWCFlat.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", edtavSearchcriteria_Visible, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavSearchcriteria_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavSearchcriteria_Internalname, "Search Criteria", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 51,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSearchcriteria_Internalname, StringUtil.RTrim( AV25SearchCriteria), StringUtil.RTrim( context.localUtil.Format( AV25SearchCriteria, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,51);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSearchcriteria_Jsonclick, 0, "Attribute", "", "", "", "", edtavSearchcriteria_Visible, edtavSearchcriteria_Enabled, 0, "text", "", 80, "chr", 1, "row", 150, 0, 0, 0, 1, -1, -1, true, "K2BSearchCriteria", "left", true, "HLP_K2BToolsSearchResultEntityWCFlat.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", edtavEntityitemtoskip_Visible, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavEntityitemtoskip_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavEntityitemtoskip_Internalname, "Entity Item To Skip", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 56,'" + sPrefix + "',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEntityitemtoskip_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10EntityItemToSkip), 4, 0, ",", "")), ((edtavEntityitemtoskip_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV10EntityItemToSkip), "ZZZ9")) : context.localUtil.Format( (decimal)(AV10EntityItemToSkip), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,56);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEntityitemtoskip_Jsonclick, 0, "Attribute", "", "", "", "", edtavEntityitemtoskip_Visible, edtavEntityitemtoskip_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "K2BNumber", "right", false, "HLP_K2BToolsSearchResultEntityWCFlat.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START1B2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
               Form.Meta.addItem("description", "K2 BTools Search Result Entity WCFlat", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP1B0( ) ;
            }
         }
      }

      protected void WS1B2( )
      {
         START1B2( ) ;
         EVT1B2( ) ;
      }

      protected void EVT1B2( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1B0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'E_NEXTPAGE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1B0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: 'E_NextPage' */
                                    E111B2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'E_PREVIOUSPAGE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1B0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: 'E_PreviousPage' */
                                    E121B2 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1B0( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavPreviouspage_action_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 24), "ENTITIESRESULTSGRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP1B0( ) ;
                              }
                              nGXsfl_13_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
                              SubsflControlProps_132( ) ;
                              AV37GXV1 = nGXsfl_13_idx;
                              if ( ( AV24ResultsEntities.Count >= AV37GXV1 ) && ( AV37GXV1 > 0 ) )
                              {
                                 AV24ResultsEntities.CurrentItem = ((SdtK2BSearchResult_Item)AV24ResultsEntities.Item(AV37GXV1));
                                 AV27SearchResultTitle = cgiGet( edtavSearchresulttitle_Internalname);
                                 context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavSearchresulttitle_Internalname, AV27SearchResultTitle);
                              }
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPreviouspage_action_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: Start */
                                          E131B2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPreviouspage_action_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          /* Execute user event: Refresh */
                                          E141B2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTITIESRESULTSGRID.LOAD") == 0 )
                                 {
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPreviouspage_action_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                          E151B2 ();
                                       }
                                    }
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          if ( ! wbErr )
                                          {
                                             Rfr0gs = false;
                                             if ( ! Rfr0gs )
                                             {
                                             }
                                             dynload_actions( ) ;
                                          }
                                       }
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                                    {
                                       STRUP1B0( ) ;
                                    }
                                    if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                                    {
                                       context.wbHandled = 1;
                                       if ( ! wbErr )
                                       {
                                          dynload_actions( ) ;
                                          GX_FocusControl = edtavPreviouspage_action_Internalname;
                                          context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                       }
                                    }
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE1B2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm1B2( ) ;
            }
         }
      }

      protected void PA1B2( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavPreviouspage_action_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrEntitiesresultsgrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_132( ) ;
         while ( nGXsfl_13_idx <= nRC_GXsfl_13 )
         {
            sendrow_132( ) ;
            nGXsfl_13_idx = (short)(((subEntitiesresultsgrid_Islastpage==1)&&(nGXsfl_13_idx+1>subEntitiesresultsgrid_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
            sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
            SubsflControlProps_132( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( EntitiesresultsgridContainer));
         /* End function gxnrEntitiesresultsgrid_newrow */
      }

      protected void gxgrEntitiesresultsgrid_refresh( String AV26SearchCriteriaIn ,
                                                      GXBaseCollection<SdtK2BSearchResult_Item> AV24ResultsEntities ,
                                                      String sPrefix )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         ENTITIESRESULTSGRID_nCurrentRecord = 0;
         RF1B2( ) ;
         /* End function gxgrEntitiesresultsgrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF1B2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV40Pgmname = "K2BToolsSearchResultEntityWCFlat";
         context.Gx_err = 0;
         edtavCtlsearchresultdescriptionlarge_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlsearchresultdescriptionlarge_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlsearchresultdescriptionlarge_Enabled), 5, 0)), !bGXsfl_13_Refreshing);
         edtavPreviouspage_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPreviouspage_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPreviouspage_action_Enabled), 5, 0)), true);
         edtavNextpage_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNextpage_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNextpage_action_Enabled), 5, 0)), true);
      }

      protected void RF1B2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            EntitiesresultsgridContainer.ClearRows();
         }
         wbStart = 13;
         /* Execute user event: Refresh */
         E141B2 ();
         nGXsfl_13_idx = 1;
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
         bGXsfl_13_Refreshing = true;
         EntitiesresultsgridContainer.AddObjectProperty("GridName", "Entitiesresultsgrid");
         EntitiesresultsgridContainer.AddObjectProperty("CmpContext", sPrefix);
         EntitiesresultsgridContainer.AddObjectProperty("InMasterPage", "false");
         EntitiesresultsgridContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Visible), 5, 0, ".", "")));
         EntitiesresultsgridContainer.AddObjectProperty("Class", StringUtil.RTrim( "K2BTools_SearchGrid"));
         EntitiesresultsgridContainer.AddObjectProperty("Class", "K2BTools_SearchGrid");
         EntitiesresultsgridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         EntitiesresultsgridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         EntitiesresultsgridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Backcolorstyle), 1, 0, ".", "")));
         EntitiesresultsgridContainer.AddObjectProperty("Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(subEntitiesresultsgrid_Visible), 5, 0, ".", "")));
         EntitiesresultsgridContainer.PageSize = subEntitiesresultsgrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_132( ) ;
            E151B2 ();
            wbEnd = 13;
            WB1B0( ) ;
         }
         bGXsfl_13_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes1B2( )
      {
      }

      protected int subEntitiesresultsgrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subEntitiesresultsgrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subEntitiesresultsgrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subEntitiesresultsgrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP1B0( )
      {
         /* Before Start, stand alone formulas. */
         AV40Pgmname = "K2BToolsSearchResultEntityWCFlat";
         context.Gx_err = 0;
         edtavCtlsearchresultdescriptionlarge_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavCtlsearchresultdescriptionlarge_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCtlsearchresultdescriptionlarge_Enabled), 5, 0)), !bGXsfl_13_Refreshing);
         edtavPreviouspage_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPreviouspage_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPreviouspage_action_Enabled), 5, 0)), true);
         edtavNextpage_action_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNextpage_action_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNextpage_action_Enabled), 5, 0)), true);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E131B2 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( sPrefix+"Resultsentities"), AV24ResultsEntities);
            /* Read variables values. */
            AV31PreviousPage_Action = cgiGet( edtavPreviouspage_action_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31PreviousPage_Action", AV31PreviousPage_Action);
            AV20NextPage_Action = cgiGet( edtavNextpage_action_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20NextPage_Action", AV20NextPage_Action);
            AV33ItemsToSkipJSon = cgiGet( edtavItemstoskipjson_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ItemsToSkipJSon", AV33ItemsToSkipJSon);
            AV25SearchCriteria = cgiGet( edtavSearchcriteria_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25SearchCriteria", AV25SearchCriteria);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavEntityitemtoskip_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavEntityitemtoskip_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vENTITYITEMTOSKIP");
               GX_FocusControl = edtavEntityitemtoskip_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV10EntityItemToSkip = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10EntityItemToSkip", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10EntityItemToSkip), 4, 0)));
            }
            else
            {
               AV10EntityItemToSkip = (short)(context.localUtil.CToN( cgiGet( edtavEntityitemtoskip_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10EntityItemToSkip", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10EntityItemToSkip), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_13 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_13"), ",", "."));
            wcpOAV26SearchCriteriaIn = cgiGet( sPrefix+"wcpOAV26SearchCriteriaIn");
            wcpOAV11EntityName = cgiGet( sPrefix+"wcpOAV11EntityName");
            nRC_GXsfl_13 = (short)(context.localUtil.CToN( cgiGet( sPrefix+"nRC_GXsfl_13"), ",", "."));
            nGXsfl_13_fel_idx = 0;
            while ( nGXsfl_13_fel_idx < nRC_GXsfl_13 )
            {
               nGXsfl_13_fel_idx = (short)(((subEntitiesresultsgrid_Islastpage==1)&&(nGXsfl_13_fel_idx+1>subEntitiesresultsgrid_Recordsperpage( )) ? 1 : nGXsfl_13_fel_idx+1));
               sGXsfl_13_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_132( ) ;
               AV37GXV1 = nGXsfl_13_fel_idx;
               if ( ( AV24ResultsEntities.Count >= AV37GXV1 ) && ( AV37GXV1 > 0 ) )
               {
                  AV24ResultsEntities.CurrentItem = ((SdtK2BSearchResult_Item)AV24ResultsEntities.Item(AV37GXV1));
                  AV27SearchResultTitle = cgiGet( edtavSearchresulttitle_Internalname);
               }
            }
            if ( nGXsfl_13_fel_idx == 0 )
            {
               nGXsfl_13_idx = 1;
               sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
               SubsflControlProps_132( ) ;
            }
            nGXsfl_13_fel_idx = 1;
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void S122( )
      {
         /* 'U_STARTPAGE' Routine */
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E131B2 ();
         if (returnInSub) return;
      }

      protected void E131B2( )
      {
         /* Start Routine */
         if ( new k2bisauthorizedactivityname(context).executeUdp(  "",  "",  "None",  "K2BToolsSearchResult",  AV40Pgmname) )
         {
            if ( StringUtil.StrCmp(AV17HttpRequest.Method, "GET") == 0 )
            {
               /* Execute user subroutine: 'U_OPENPAGE' */
               S112 ();
               if (returnInSub) return;
            }
            AV20NextPage_Action = "Next Page";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV20NextPage_Action", AV20NextPage_Action);
            AV31PreviousPage_Action = "Previous Page";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV31PreviousPage_Action", AV31PreviousPage_Action);
            /* Execute user subroutine: 'U_STARTPAGE' */
            S122 ();
            if (returnInSub) return;
            edtavItemstoskipjson_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavItemstoskipjson_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavItemstoskipjson_Visible), 5, 0)), true);
         }
         else
         {
            CallWebObject(formatLink("k2bnotauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("None")) + "," + UrlEncode(StringUtil.RTrim("K2BToolsSearchResult")) + "," + UrlEncode(StringUtil.RTrim(AV40Pgmname)));
            context.wjLocDisableFrm = 1;
         }
      }

      protected void E141B2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         /* Execute user subroutine: 'U_REFRESHPAGE' */
         S132 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S112( )
      {
         /* 'U_OPENPAGE' Routine */
         edtavNextpage_action_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNextpage_action_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNextpage_action_Visible), 5, 0)), true);
         AV24ResultsEntities = new GXBaseCollection<SdtK2BSearchResult_Item>( context, "Item", "PACYE2");
         gx_BV13 = true;
         edtavPreviouspage_action_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPreviouspage_action_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPreviouspage_action_Visible), 5, 0)), true);
         AV10EntityItemToSkip = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10EntityItemToSkip", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10EntityItemToSkip), 4, 0)));
         AV32ItemsToSkipCollection.Clear();
         AV33ItemsToSkipJSon = AV32ItemsToSkipCollection.ToJSonString(false);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ItemsToSkipJSon", AV33ItemsToSkipJSon);
         AV25SearchCriteria = AV26SearchCriteriaIn;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25SearchCriteria", AV25SearchCriteria);
         edtavSearchcriteria_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavSearchcriteria_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavSearchcriteria_Visible), 5, 0)), true);
         edtavEntityitemtoskip_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavEntityitemtoskip_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEntityitemtoskip_Visible), 5, 0)), true);
         /* Execute user subroutine: 'LOADENTITIESRESULTPAGE' */
         S162 ();
         if (returnInSub) return;
      }

      protected void S132( )
      {
         /* 'U_REFRESHPAGE' Routine */
         AV25SearchCriteria = AV26SearchCriteriaIn;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV25SearchCriteria", AV25SearchCriteria);
      }

      private void E151B2( )
      {
         /* Entitiesresultsgrid_Load Routine */
         AV37GXV1 = 1;
         while ( AV37GXV1 <= AV24ResultsEntities.Count )
         {
            AV24ResultsEntities.CurrentItem = ((SdtK2BSearchResult_Item)AV24ResultsEntities.Item(AV37GXV1));
            AV27SearchResultTitle = ((SdtK2BSearchResult_Item)(AV24ResultsEntities.CurrentItem)).gxTpr_Searchresulttitle;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, edtavSearchresulttitle_Internalname, AV27SearchResultTitle);
            edtavSearchresulttitle_Link = ((SdtK2BSearchResult_Item)(AV24ResultsEntities.CurrentItem)).gxTpr_Searchresultlink;
            /* Load Method */
            if ( wbStart != -1 )
            {
               wbStart = 13;
            }
            sendrow_132( ) ;
            if ( isFullAjaxMode( ) && ! bGXsfl_13_Refreshing )
            {
               context.DoAjaxLoad(13, EntitiesresultsgridRow);
            }
            AV37GXV1 = (short)(AV37GXV1+1);
         }
         /*  Sending Event outputs  */
      }

      protected void S162( )
      {
         /* 'LOADENTITIESRESULTPAGE' Routine */
         if ( AV10EntityItemToSkip == 0 )
         {
            edtavPreviouspage_action_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPreviouspage_action_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPreviouspage_action_Visible), 5, 0)), true);
         }
         else
         {
            edtavPreviouspage_action_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavPreviouspage_action_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavPreviouspage_action_Visible), 5, 0)), true);
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV25SearchCriteria)) )
         {
            new k2btoolsgetsearchresults(context ).execute(  AV11EntityName,  AV25SearchCriteria+"*",  12,  AV10EntityItemToSkip, out  AV9EntityItemsProcessed, out  AV21PendingItemsExist, out  AV24ResultsEntities) ;
            gx_BV13 = true;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11EntityName", AV11EntityName);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10EntityItemToSkip", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10EntityItemToSkip), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9EntityItemsProcessed", StringUtil.LTrim( StringUtil.Str( (decimal)(AV9EntityItemsProcessed), 4, 0)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV21PendingItemsExist", AV21PendingItemsExist);
         }
         AV10EntityItemToSkip = (short)(AV9EntityItemsProcessed+1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10EntityItemToSkip", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10EntityItemToSkip), 4, 0)));
         if ( AV24ResultsEntities.Count == 0 )
         {
            subEntitiesresultsgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, sPrefix+"EntitiesresultsgridContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subEntitiesresultsgrid_Visible), 5, 0)), true);
            tblNoresultsfoundtable_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblNoresultsfoundtable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblNoresultsfoundtable_Visible), 5, 0)), true);
         }
         else
         {
            subEntitiesresultsgrid_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, sPrefix+"EntitiesresultsgridContainerDiv", "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(subEntitiesresultsgrid_Visible), 5, 0)), true);
            tblNoresultsfoundtable_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, tblNoresultsfoundtable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblNoresultsfoundtable_Visible), 5, 0)), true);
         }
         if ( AV21PendingItemsExist )
         {
            edtavNextpage_action_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNextpage_action_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNextpage_action_Visible), 5, 0)), true);
         }
         else
         {
            edtavNextpage_action_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtavNextpage_action_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavNextpage_action_Visible), 5, 0)), true);
         }
      }

      protected void E111B2( )
      {
         AV37GXV1 = nGXsfl_13_idx;
         if ( AV24ResultsEntities.Count >= AV37GXV1 )
         {
            AV24ResultsEntities.CurrentItem = ((SdtK2BSearchResult_Item)AV24ResultsEntities.Item(AV37GXV1));
         }
         /* 'E_NextPage' Routine */
         /* Execute user subroutine: 'U_NEXTPAGE' */
         S142 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24ResultsEntities", AV24ResultsEntities);
         nGXsfl_13_bak_idx = nGXsfl_13_idx;
         gxgrEntitiesresultsgrid_refresh( AV26SearchCriteriaIn, AV24ResultsEntities, sPrefix) ;
         nGXsfl_13_idx = nGXsfl_13_bak_idx;
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
      }

      protected void S142( )
      {
         /* 'U_NEXTPAGE' Routine */
         AV32ItemsToSkipCollection.FromJSonString(AV33ItemsToSkipJSon, null);
         AV32ItemsToSkipCollection.Add(AV10EntityItemToSkip, 0);
         AV33ItemsToSkipJSon = AV32ItemsToSkipCollection.ToJSonString(false);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ItemsToSkipJSon", AV33ItemsToSkipJSon);
         /* Execute user subroutine: 'LOADENTITIESRESULTPAGE' */
         S162 ();
         if (returnInSub) return;
      }

      protected void E121B2( )
      {
         AV37GXV1 = nGXsfl_13_idx;
         if ( AV24ResultsEntities.Count >= AV37GXV1 )
         {
            AV24ResultsEntities.CurrentItem = ((SdtK2BSearchResult_Item)AV24ResultsEntities.Item(AV37GXV1));
         }
         /* 'E_PreviousPage' Routine */
         /* Execute user subroutine: 'U_PREVIOUSPAGE' */
         S152 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV24ResultsEntities", AV24ResultsEntities);
         nGXsfl_13_bak_idx = nGXsfl_13_idx;
         gxgrEntitiesresultsgrid_refresh( AV26SearchCriteriaIn, AV24ResultsEntities, sPrefix) ;
         nGXsfl_13_idx = nGXsfl_13_bak_idx;
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
      }

      protected void S152( )
      {
         /* 'U_PREVIOUSPAGE' Routine */
         AV32ItemsToSkipCollection.FromJSonString(AV33ItemsToSkipJSon, null);
         AV34Count = (short)(AV32ItemsToSkipCollection.Count);
         if ( AV34Count > 1 )
         {
            AV32ItemsToSkipCollection.RemoveItem(AV34Count);
            AV10EntityItemToSkip = (short)(AV32ItemsToSkipCollection.GetNumeric(AV34Count-1));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10EntityItemToSkip", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10EntityItemToSkip), 4, 0)));
         }
         else
         {
            if ( AV34Count == 1 )
            {
               AV32ItemsToSkipCollection.RemoveItem(AV34Count);
            }
            AV10EntityItemToSkip = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV10EntityItemToSkip", StringUtil.LTrim( StringUtil.Str( (decimal)(AV10EntityItemToSkip), 4, 0)));
         }
         AV33ItemsToSkipJSon = AV32ItemsToSkipCollection.ToJSonString(false);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV33ItemsToSkipJSon", AV33ItemsToSkipJSon);
         /* Execute user subroutine: 'LOADENTITIESRESULTPAGE' */
         S162 ();
         if (returnInSub) return;
      }

      protected void wb_table1_6_1B2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblNoresultsfoundtable_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblNoresultsfoundtable_Internalname, tblNoresultsfoundtable_Internalname, "", "K2BToolsTable_NoResultsFoundUniversalSearch", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblNoresultsfoundtextblock_Internalname, "No results found", "", "", lblNoresultsfoundtextblock_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_NoResultsFound", 0, "", 1, 1, 0, "HLP_K2BToolsSearchResultEntityWCFlat.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_6_1B2e( true) ;
         }
         else
         {
            wb_table1_6_1B2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV26SearchCriteriaIn = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SearchCriteriaIn", AV26SearchCriteriaIn);
         AV11EntityName = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11EntityName", AV11EntityName);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("K2BFlat");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA1B2( ) ;
         WS1B2( ) ;
         WE1B2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV26SearchCriteriaIn = (String)((String)getParm(obj,0));
         sCtrlAV11EntityName = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA1B2( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "k2btoolssearchresultentitywcflat", GetJustCreated( ));
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA1B2( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV26SearchCriteriaIn = (String)getParm(obj,2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SearchCriteriaIn", AV26SearchCriteriaIn);
            AV11EntityName = (String)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11EntityName", AV11EntityName);
         }
         wcpOAV26SearchCriteriaIn = cgiGet( sPrefix+"wcpOAV26SearchCriteriaIn");
         wcpOAV11EntityName = cgiGet( sPrefix+"wcpOAV11EntityName");
         if ( ! GetJustCreated( ) && ( ( StringUtil.StrCmp(AV26SearchCriteriaIn, wcpOAV26SearchCriteriaIn) != 0 ) || ( StringUtil.StrCmp(AV11EntityName, wcpOAV11EntityName) != 0 ) ) )
         {
            setjustcreated();
         }
         wcpOAV26SearchCriteriaIn = AV26SearchCriteriaIn;
         wcpOAV11EntityName = AV11EntityName;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV26SearchCriteriaIn = cgiGet( sPrefix+"AV26SearchCriteriaIn_CTRL");
         if ( StringUtil.Len( sCtrlAV26SearchCriteriaIn) > 0 )
         {
            AV26SearchCriteriaIn = cgiGet( sCtrlAV26SearchCriteriaIn);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV26SearchCriteriaIn", AV26SearchCriteriaIn);
         }
         else
         {
            AV26SearchCriteriaIn = cgiGet( sPrefix+"AV26SearchCriteriaIn_PARM");
         }
         sCtrlAV11EntityName = cgiGet( sPrefix+"AV11EntityName_CTRL");
         if ( StringUtil.Len( sCtrlAV11EntityName) > 0 )
         {
            AV11EntityName = cgiGet( sCtrlAV11EntityName);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV11EntityName", AV11EntityName);
         }
         else
         {
            AV11EntityName = cgiGet( sPrefix+"AV11EntityName_PARM");
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA1B2( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS1B2( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS1B2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV26SearchCriteriaIn_PARM", StringUtil.RTrim( AV26SearchCriteriaIn));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV26SearchCriteriaIn)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV26SearchCriteriaIn_CTRL", StringUtil.RTrim( sCtrlAV26SearchCriteriaIn));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV11EntityName_PARM", StringUtil.RTrim( AV11EntityName));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV11EntityName)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV11EntityName_CTRL", StringUtil.RTrim( sCtrlAV11EntityName));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE1B2( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171523024", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("k2btoolssearchresultentitywcflat.js", "?201811171523024", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_132( )
      {
         edtavCtlsearchresultimagelarge_Internalname = sPrefix+"CTLSEARCHRESULTIMAGELARGE_"+sGXsfl_13_idx;
         edtavSearchresulttitle_Internalname = sPrefix+"vSEARCHRESULTTITLE_"+sGXsfl_13_idx;
         edtavCtlsearchresultdescriptionlarge_Internalname = sPrefix+"CTLSEARCHRESULTDESCRIPTIONLARGE_"+sGXsfl_13_idx;
      }

      protected void SubsflControlProps_fel_132( )
      {
         edtavCtlsearchresultimagelarge_Internalname = sPrefix+"CTLSEARCHRESULTIMAGELARGE_"+sGXsfl_13_fel_idx;
         edtavSearchresulttitle_Internalname = sPrefix+"vSEARCHRESULTTITLE_"+sGXsfl_13_fel_idx;
         edtavCtlsearchresultdescriptionlarge_Internalname = sPrefix+"CTLSEARCHRESULTDESCRIPTIONLARGE_"+sGXsfl_13_fel_idx;
      }

      protected void sendrow_132( )
      {
         SubsflControlProps_132( ) ;
         WB1B0( ) ;
         EntitiesresultsgridRow = GXWebRow.GetNew(context,EntitiesresultsgridContainer);
         if ( subEntitiesresultsgrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subEntitiesresultsgrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subEntitiesresultsgrid_Class, "") != 0 )
            {
               subEntitiesresultsgrid_Linesclass = subEntitiesresultsgrid_Class+"Odd";
            }
         }
         else if ( subEntitiesresultsgrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subEntitiesresultsgrid_Backstyle = 0;
            subEntitiesresultsgrid_Backcolor = subEntitiesresultsgrid_Allbackcolor;
            if ( StringUtil.StrCmp(subEntitiesresultsgrid_Class, "") != 0 )
            {
               subEntitiesresultsgrid_Linesclass = subEntitiesresultsgrid_Class+"Uniform";
            }
         }
         else if ( subEntitiesresultsgrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subEntitiesresultsgrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subEntitiesresultsgrid_Class, "") != 0 )
            {
               subEntitiesresultsgrid_Linesclass = subEntitiesresultsgrid_Class+"Odd";
            }
            subEntitiesresultsgrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subEntitiesresultsgrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subEntitiesresultsgrid_Backstyle = 1;
            if ( ((int)((nGXsfl_13_idx) % (2))) == 0 )
            {
               subEntitiesresultsgrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subEntitiesresultsgrid_Class, "") != 0 )
               {
                  subEntitiesresultsgrid_Linesclass = subEntitiesresultsgrid_Class+"Even";
               }
            }
            else
            {
               subEntitiesresultsgrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subEntitiesresultsgrid_Class, "") != 0 )
               {
                  subEntitiesresultsgrid_Linesclass = subEntitiesresultsgrid_Class+"Odd";
               }
            }
         }
         /* Start of Columns property logic. */
         if ( EntitiesresultsgridContainer.GetWrapped() == 1 )
         {
            if ( ( 1 == 0 ) && ( nGXsfl_13_idx == 1 ) )
            {
               context.WriteHtmlText( "<tr"+" class=\""+subEntitiesresultsgrid_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_13_idx+"\">") ;
            }
            if ( 1 > 0 )
            {
               if ( ( 1 == 1 ) || ( ((int)((nGXsfl_13_idx) % (1))) - 1 == 0 ) )
               {
                  context.WriteHtmlText( "<tr"+" class=\""+subEntitiesresultsgrid_Linesclass+"\" style=\""+""+"\""+" data-gxrow=\""+sGXsfl_13_idx+"\">") ;
               }
            }
         }
         /* Table start */
         EntitiesresultsgridRow.AddColumnProperties("table", -1, isAjaxCallMode( ), new Object[] {(String)tblGrid2table1_Internalname+"_"+sGXsfl_13_idx,(short)1,(String)"K2BToolsSection_Card",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(short)2,(String)"",(String)"",(String)"",(String)"px",(String)"px"});
         EntitiesresultsgridRow.AddColumnProperties("row", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)"",(String)""});
         EntitiesresultsgridRow.AddColumnProperties("cell", -1, isAjaxCallMode( ), new Object[] {(String)"",(String)""});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)divTablelarge_Internalname+"_"+sGXsfl_13_idx,(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"Table",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"row",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"col-xs-4",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)" gx-attribute",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Attribute/Variable Label */
         EntitiesresultsgridRow.AddColumnProperties("html_label", -1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsearchresultimagelarge_Internalname,(String)"SearchResultImage",(String)"col-sm-3 K2BTools_SearchResultImageLabel",(short)0,(bool)true});
         /* Static Bitmap Variable */
         ClassString = "K2BTools_SearchResultImage";
         StyleString = "";
         sImgUrl = context.PathToRelativeUrl( ((SdtK2BSearchResult_Item)AV24ResultsEntities.Item(AV37GXV1)).gxTpr_Searchresultimage);
         EntitiesresultsgridRow.AddColumnProperties("bitmap", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsearchresultimagelarge_Internalname,(String)sImgUrl,(String)"",(String)"",(String)"",context.GetTheme( ),(short)1,(short)0,(String)"",(String)"",(short)0,(short)-1,(short)0,(String)"",(short)0,(String)"",(short)0,(short)0,(short)0,(String)"",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(String)"",(short)1,(bool)false,(bool)false,context.GetImageSrcSet( sImgUrl)});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"col-xs-8",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)divTable2large_Internalname+"_"+sGXsfl_13_idx,(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"K2BToolsTable_SearchTextContainer",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"row",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"col-xs-12",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)" gx-attribute",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Attribute/Variable Label */
         EntitiesresultsgridRow.AddColumnProperties("html_label", -1, isAjaxCallMode( ), new Object[] {(String)edtavSearchresulttitle_Internalname,(String)"Search Result Title",(String)"col-sm-3 AttributeLabel",(short)0,(bool)true});
         /* Single line edit */
         ROClassString = "Attribute";
         EntitiesresultsgridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavSearchresulttitle_Internalname,StringUtil.RTrim( AV27SearchResultTitle),(String)"",(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(String)edtavSearchresulttitle_Link,(String)"",(String)"",(String)"",(String)edtavSearchresulttitle_Jsonclick,(short)0,(String)"Attribute",(String)"",(String)ROClassString,(String)"",(String)"",(short)1,(short)0,(short)0,(String)"text",(String)"",(short)50,(String)"chr",(short)1,(String)"row",(short)50,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"K2BDescription",(String)"left",(bool)true});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"row",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)"col-xs-12",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Div Control */
         EntitiesresultsgridRow.AddColumnProperties("div_start", -1, isAjaxCallMode( ), new Object[] {(String)"",(short)1,(short)0,(String)"px",(short)0,(String)"px",(String)" gx-attribute",(String)"left",(String)"top",(String)"",(String)"",(String)"div"});
         /* Attribute/Variable Label */
         EntitiesresultsgridRow.AddColumnProperties("html_label", -1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsearchresultdescriptionlarge_Internalname,(String)"SearchResultDescription",(String)"col-sm-3 K2BSearchResult_DescriptionLabel",(short)0,(bool)true});
         /* Multiple line edit */
         ClassString = "K2BSearchResult_Description";
         StyleString = "";
         ClassString = "K2BSearchResult_Description";
         StyleString = "";
         EntitiesresultsgridRow.AddColumnProperties("html_textarea", 1, isAjaxCallMode( ), new Object[] {(String)edtavCtlsearchresultdescriptionlarge_Internalname,StringUtil.RTrim( ((SdtK2BSearchResult_Item)AV24ResultsEntities.Item(AV37GXV1)).gxTpr_Searchresultdescription),(String)"",(String)"",(short)1,(short)1,(int)edtavCtlsearchresultdescriptionlarge_Enabled,(short)0,(short)80,(String)"chr",(short)5,(String)"row",(String)StyleString,(String)ClassString,(String)"",(String)"",(String)"400",(short)-1,(short)0,(String)"",(String)"",(short)-1,(bool)true,(String)"",(String)"'"+sPrefix+"'"+",false,"+"'"+""+"'",(short)0});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         EntitiesresultsgridRow.AddColumnProperties("div_end", -1, isAjaxCallMode( ), new Object[] {(String)"left",(String)"top",(String)"div"});
         if ( EntitiesresultsgridContainer.GetWrapped() == 1 )
         {
            EntitiesresultsgridContainer.CloseTag("cell");
         }
         if ( EntitiesresultsgridContainer.GetWrapped() == 1 )
         {
            EntitiesresultsgridContainer.CloseTag("row");
         }
         if ( EntitiesresultsgridContainer.GetWrapped() == 1 )
         {
            EntitiesresultsgridContainer.CloseTag("table");
         }
         /* End of table */
         send_integrity_lvl_hashes1B2( ) ;
         /* End of Columns property logic. */
         EntitiesresultsgridContainer.AddRow(EntitiesresultsgridRow);
         nGXsfl_13_idx = (short)(((subEntitiesresultsgrid_Islastpage==1)&&(nGXsfl_13_idx+1>subEntitiesresultsgrid_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
         /* End function sendrow_132 */
      }

      protected void init_default_properties( )
      {
         lblNoresultsfoundtextblock_Internalname = sPrefix+"NORESULTSFOUNDTEXTBLOCK";
         tblNoresultsfoundtable_Internalname = sPrefix+"NORESULTSFOUNDTABLE";
         edtavCtlsearchresultimagelarge_Internalname = sPrefix+"CTLSEARCHRESULTIMAGELARGE";
         edtavSearchresulttitle_Internalname = sPrefix+"vSEARCHRESULTTITLE";
         edtavCtlsearchresultdescriptionlarge_Internalname = sPrefix+"CTLSEARCHRESULTDESCRIPTIONLARGE";
         divTable2large_Internalname = sPrefix+"TABLE2LARGE";
         divTablelarge_Internalname = sPrefix+"TABLELARGE";
         tblGrid2table1_Internalname = sPrefix+"GRID2TABLE1";
         edtavPreviouspage_action_Internalname = sPrefix+"vPREVIOUSPAGE_ACTION";
         edtavNextpage_action_Internalname = sPrefix+"vNEXTPAGE_ACTION";
         divTable1_Internalname = sPrefix+"TABLE1";
         edtavItemstoskipjson_Internalname = sPrefix+"vITEMSTOSKIPJSON";
         edtavSearchcriteria_Internalname = sPrefix+"vSEARCHCRITERIA";
         edtavEntityitemtoskip_Internalname = sPrefix+"vENTITYITEMTOSKIP";
         divMaintable_Internalname = sPrefix+"MAINTABLE";
         Form.Internalname = sPrefix+"FORM";
         subEntitiesresultsgrid_Internalname = sPrefix+"ENTITIESRESULTSGRID";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavSearchresulttitle_Jsonclick = "";
         subEntitiesresultsgrid_Class = "K2BTools_SearchGrid";
         tblNoresultsfoundtable_Visible = 1;
         edtavEntityitemtoskip_Jsonclick = "";
         edtavEntityitemtoskip_Enabled = 1;
         edtavEntityitemtoskip_Visible = 1;
         edtavSearchcriteria_Jsonclick = "";
         edtavSearchcriteria_Enabled = 1;
         edtavSearchcriteria_Visible = 1;
         edtavItemstoskipjson_Jsonclick = "";
         edtavItemstoskipjson_Enabled = 1;
         edtavItemstoskipjson_Visible = 1;
         edtavNextpage_action_Jsonclick = "";
         edtavNextpage_action_Enabled = 1;
         edtavNextpage_action_Visible = 1;
         edtavPreviouspage_action_Jsonclick = "";
         edtavPreviouspage_action_Enabled = 1;
         edtavPreviouspage_action_Visible = 1;
         subEntitiesresultsgrid_Allowcollapsing = 0;
         edtavCtlsearchresultdescriptionlarge_Enabled = 0;
         edtavSearchresulttitle_Link = "";
         subEntitiesresultsgrid_Backcolorstyle = 0;
         subEntitiesresultsgrid_Visible = 1;
         edtavCtlsearchresultdescriptionlarge_Enabled = -1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'ENTITIESRESULTSGRID_nFirstRecordOnPage',nv:0},{av:'ENTITIESRESULTSGRID_nEOF',nv:0},{av:'AV24ResultsEntities',fld:'vRESULTSENTITIES',grid:13,pic:'',nv:null},{av:'sPrefix',nv:''},{av:'AV26SearchCriteriaIn',fld:'vSEARCHCRITERIAIN',pic:'',nv:''}],oparms:[{av:'AV25SearchCriteria',fld:'vSEARCHCRITERIA',pic:'',nv:''}]}");
         setEventMetadata("ENTITIESRESULTSGRID.LOAD","{handler:'E151B2',iparms:[{av:'AV24ResultsEntities',fld:'vRESULTSENTITIES',grid:13,pic:'',nv:null},{av:'ENTITIESRESULTSGRID_nFirstRecordOnPage',nv:0}],oparms:[{av:'AV27SearchResultTitle',fld:'vSEARCHRESULTTITLE',pic:'',nv:''},{av:'edtavSearchresulttitle_Link',ctrl:'vSEARCHRESULTTITLE',prop:'Link'}]}");
         setEventMetadata("'E_NEXTPAGE'","{handler:'E111B2',iparms:[{av:'AV33ItemsToSkipJSon',fld:'vITEMSTOSKIPJSON',pic:'',nv:''},{av:'AV10EntityItemToSkip',fld:'vENTITYITEMTOSKIP',pic:'ZZZ9',nv:0},{av:'AV25SearchCriteria',fld:'vSEARCHCRITERIA',pic:'',nv:''},{av:'AV11EntityName',fld:'vENTITYNAME',pic:'',nv:''},{av:'AV9EntityItemsProcessed',fld:'vENTITYITEMSPROCESSED',pic:'ZZZ9',nv:0},{av:'AV24ResultsEntities',fld:'vRESULTSENTITIES',grid:13,pic:'',nv:null},{av:'ENTITIESRESULTSGRID_nFirstRecordOnPage',nv:0},{av:'AV21PendingItemsExist',fld:'vPENDINGITEMSEXIST',pic:'',nv:false},{av:'ENTITIESRESULTSGRID_nEOF',nv:0},{av:'AV26SearchCriteriaIn',fld:'vSEARCHCRITERIAIN',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV33ItemsToSkipJSon',fld:'vITEMSTOSKIPJSON',pic:'',nv:''},{av:'edtavPreviouspage_action_Visible',ctrl:'vPREVIOUSPAGE_ACTION',prop:'Visible'},{av:'AV24ResultsEntities',fld:'vRESULTSENTITIES',grid:13,pic:'',nv:null},{av:'ENTITIESRESULTSGRID_nFirstRecordOnPage',nv:0},{av:'AV21PendingItemsExist',fld:'vPENDINGITEMSEXIST',pic:'',nv:false},{av:'AV9EntityItemsProcessed',fld:'vENTITYITEMSPROCESSED',pic:'ZZZ9',nv:0},{av:'AV10EntityItemToSkip',fld:'vENTITYITEMTOSKIP',pic:'ZZZ9',nv:0},{av:'subEntitiesresultsgrid_Visible',ctrl:'ENTITIESRESULTSGRID',prop:'Visible'},{av:'tblNoresultsfoundtable_Visible',ctrl:'NORESULTSFOUNDTABLE',prop:'Visible'},{av:'edtavNextpage_action_Visible',ctrl:'vNEXTPAGE_ACTION',prop:'Visible'}]}");
         setEventMetadata("'E_PREVIOUSPAGE'","{handler:'E121B2',iparms:[{av:'AV33ItemsToSkipJSon',fld:'vITEMSTOSKIPJSON',pic:'',nv:''},{av:'AV10EntityItemToSkip',fld:'vENTITYITEMTOSKIP',pic:'ZZZ9',nv:0},{av:'AV25SearchCriteria',fld:'vSEARCHCRITERIA',pic:'',nv:''},{av:'AV11EntityName',fld:'vENTITYNAME',pic:'',nv:''},{av:'AV9EntityItemsProcessed',fld:'vENTITYITEMSPROCESSED',pic:'ZZZ9',nv:0},{av:'AV24ResultsEntities',fld:'vRESULTSENTITIES',grid:13,pic:'',nv:null},{av:'ENTITIESRESULTSGRID_nFirstRecordOnPage',nv:0},{av:'AV21PendingItemsExist',fld:'vPENDINGITEMSEXIST',pic:'',nv:false},{av:'ENTITIESRESULTSGRID_nEOF',nv:0},{av:'AV26SearchCriteriaIn',fld:'vSEARCHCRITERIAIN',pic:'',nv:''},{av:'sPrefix',nv:''}],oparms:[{av:'AV10EntityItemToSkip',fld:'vENTITYITEMTOSKIP',pic:'ZZZ9',nv:0},{av:'AV33ItemsToSkipJSon',fld:'vITEMSTOSKIPJSON',pic:'',nv:''},{av:'edtavPreviouspage_action_Visible',ctrl:'vPREVIOUSPAGE_ACTION',prop:'Visible'},{av:'AV24ResultsEntities',fld:'vRESULTSENTITIES',grid:13,pic:'',nv:null},{av:'ENTITIESRESULTSGRID_nFirstRecordOnPage',nv:0},{av:'AV21PendingItemsExist',fld:'vPENDINGITEMSEXIST',pic:'',nv:false},{av:'AV9EntityItemsProcessed',fld:'vENTITYITEMSPROCESSED',pic:'ZZZ9',nv:0},{av:'subEntitiesresultsgrid_Visible',ctrl:'ENTITIESRESULTSGRID',prop:'Visible'},{av:'tblNoresultsfoundtable_Visible',ctrl:'NORESULTSFOUNDTABLE',prop:'Visible'},{av:'edtavNextpage_action_Visible',ctrl:'vNEXTPAGE_ACTION',prop:'Visible'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV26SearchCriteriaIn = "";
         wcpOAV11EntityName = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV24ResultsEntities = new GXBaseCollection<SdtK2BSearchResult_Item>( context, "Item", "PACYE2");
         GXKey = "";
         AV40Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         EntitiesresultsgridContainer = new GXWebGrid( context);
         sStyleString = "";
         EntitiesresultsgridColumn = new GXWebColumn();
         AV27SearchResultTitle = "";
         TempTags = "";
         AV31PreviousPage_Action = "";
         AV20NextPage_Action = "";
         AV33ItemsToSkipJSon = "";
         AV25SearchCriteria = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17HttpRequest = new GxHttpRequest( context);
         AV32ItemsToSkipCollection = new GxSimpleCollection<short>();
         EntitiesresultsgridRow = new GXWebRow();
         lblNoresultsfoundtextblock_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV26SearchCriteriaIn = "";
         sCtrlAV11EntityName = "";
         subEntitiesresultsgrid_Linesclass = "";
         ClassString = "";
         StyleString = "";
         sImgUrl = "";
         ROClassString = "";
         AV40Pgmname = "K2BToolsSearchResultEntityWCFlat";
         /* GeneXus formulas. */
         AV40Pgmname = "K2BToolsSearchResultEntityWCFlat";
         context.Gx_err = 0;
         edtavCtlsearchresultdescriptionlarge_Enabled = 0;
         edtavPreviouspage_action_Enabled = 0;
         edtavNextpage_action_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short nRC_GXsfl_13 ;
      private short nGXsfl_13_idx=1 ;
      private short initialized ;
      private short AV9EntityItemsProcessed ;
      private short wbEnd ;
      private short wbStart ;
      private short subEntitiesresultsgrid_Backcolorstyle ;
      private short subEntitiesresultsgrid_Allowselection ;
      private short subEntitiesresultsgrid_Allowhovering ;
      private short subEntitiesresultsgrid_Allowcollapsing ;
      private short subEntitiesresultsgrid_Collapsed ;
      private short AV37GXV1 ;
      private short AV10EntityItemToSkip ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXsfl_13_fel_idx=1 ;
      private short ENTITIESRESULTSGRID_nEOF ;
      private short nGXsfl_13_bak_idx=1 ;
      private short AV34Count ;
      private short nGXWrapped ;
      private short subEntitiesresultsgrid_Backstyle ;
      private int edtavCtlsearchresultdescriptionlarge_Enabled ;
      private int edtavPreviouspage_action_Enabled ;
      private int edtavNextpage_action_Enabled ;
      private int subEntitiesresultsgrid_Visible ;
      private int subEntitiesresultsgrid_Selectioncolor ;
      private int subEntitiesresultsgrid_Hoveringcolor ;
      private int edtavPreviouspage_action_Visible ;
      private int edtavNextpage_action_Visible ;
      private int edtavItemstoskipjson_Visible ;
      private int edtavItemstoskipjson_Enabled ;
      private int edtavSearchcriteria_Visible ;
      private int edtavSearchcriteria_Enabled ;
      private int edtavEntityitemtoskip_Visible ;
      private int edtavEntityitemtoskip_Enabled ;
      private int subEntitiesresultsgrid_Islastpage ;
      private int tblNoresultsfoundtable_Visible ;
      private int idxLst ;
      private int subEntitiesresultsgrid_Backcolor ;
      private int subEntitiesresultsgrid_Allbackcolor ;
      private long ENTITIESRESULTSGRID_nCurrentRecord ;
      private long ENTITIESRESULTSGRID_nFirstRecordOnPage ;
      private String AV26SearchCriteriaIn ;
      private String AV11EntityName ;
      private String wcpOAV26SearchCriteriaIn ;
      private String wcpOAV11EntityName ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String sGXsfl_13_idx="0001" ;
      private String GXKey ;
      private String AV40Pgmname ;
      private String edtavCtlsearchresultdescriptionlarge_Internalname ;
      private String edtavPreviouspage_action_Internalname ;
      private String edtavNextpage_action_Internalname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String divMaintable_Internalname ;
      private String sStyleString ;
      private String subEntitiesresultsgrid_Internalname ;
      private String AV27SearchResultTitle ;
      private String edtavSearchresulttitle_Link ;
      private String divTable1_Internalname ;
      private String TempTags ;
      private String AV31PreviousPage_Action ;
      private String edtavPreviouspage_action_Jsonclick ;
      private String AV20NextPage_Action ;
      private String edtavNextpage_action_Jsonclick ;
      private String edtavItemstoskipjson_Internalname ;
      private String AV33ItemsToSkipJSon ;
      private String edtavItemstoskipjson_Jsonclick ;
      private String edtavSearchcriteria_Internalname ;
      private String AV25SearchCriteria ;
      private String edtavSearchcriteria_Jsonclick ;
      private String edtavEntityitemtoskip_Internalname ;
      private String edtavEntityitemtoskip_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSearchresulttitle_Internalname ;
      private String sGXsfl_13_fel_idx="0001" ;
      private String tblNoresultsfoundtable_Internalname ;
      private String lblNoresultsfoundtextblock_Internalname ;
      private String lblNoresultsfoundtextblock_Jsonclick ;
      private String sCtrlAV26SearchCriteriaIn ;
      private String sCtrlAV11EntityName ;
      private String edtavCtlsearchresultimagelarge_Internalname ;
      private String subEntitiesresultsgrid_Class ;
      private String subEntitiesresultsgrid_Linesclass ;
      private String tblGrid2table1_Internalname ;
      private String divTablelarge_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String sImgUrl ;
      private String divTable2large_Internalname ;
      private String ROClassString ;
      private String edtavSearchresulttitle_Jsonclick ;
      private bool entryPointCalled ;
      private bool bGXsfl_13_Refreshing=false ;
      private bool toggleJsOutput ;
      private bool AV21PendingItemsExist ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool gx_BV13 ;
      private GxSimpleCollection<short> AV32ItemsToSkipCollection ;
      private GXWebGrid EntitiesresultsgridContainer ;
      private GXWebRow EntitiesresultsgridRow ;
      private GXWebColumn EntitiesresultsgridColumn ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV17HttpRequest ;
      private GXBaseCollection<SdtK2BSearchResult_Item> AV24ResultsEntities ;
   }

}
