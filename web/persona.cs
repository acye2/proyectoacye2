/*
               File: Persona
        Description: Persona
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:42.48
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class persona : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               nDynComponent = 1;
               sCompPrefix = GetNextPar( );
               sSFPrefix = GetNextPar( );
               Gx_mode = GetNextPar( );
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
               AV7PersonaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7PersonaID", AV7PersonaID.ToString());
               setjustcreated();
               componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(String)Gx_mode,(Guid)AV7PersonaID});
               componentstart();
               context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
               componentdraw();
               context.httpAjaxContext.ajax_rspEndCmp();
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV7PersonaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7PersonaID", AV7PersonaID.ToString());
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         cmbPersonaTipo.Name = "PERSONATIPO";
         cmbPersonaTipo.WebTags = "";
         cmbPersonaTipo.addItem("1", "COACH", 0);
         cmbPersonaTipo.addItem("2", "Paciente", 0);
         if ( cmbPersonaTipo.ItemCount > 0 )
         {
            A8PersonaTipo = (short)(NumberUtil.Val( cmbPersonaTipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A8PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)));
         }
         cmbPersonaPacienteOrigenSordera.Name = "PERSONAPACIENTEORIGENSORDERA";
         cmbPersonaPacienteOrigenSordera.WebTags = "";
         cmbPersonaPacienteOrigenSordera.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(0), 4, 0)), "(Ninguno)", 0);
         cmbPersonaPacienteOrigenSordera.addItem("1", "Congenito", 0);
         cmbPersonaPacienteOrigenSordera.addItem("2", "Accidental", 0);
         cmbPersonaPacienteOrigenSordera.addItem("3", "Otros", 0);
         if ( cmbPersonaPacienteOrigenSordera.ItemCount > 0 )
         {
            A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cmbPersonaPacienteOrigenSordera.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0))), "."));
            n10PersonaPacienteOrigenSordera = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)));
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
               Form.Meta.addItem("description", "Persona", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         GX_FocusControl = edtPersonaID_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public persona( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("K2BFlatCompactGreen");
         }
      }

      public persona( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_Gx_mode ,
                           Guid aP1_PersonaID )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV7PersonaID = aP1_PersonaID;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
         cmbPersonaTipo = new GXCombobox();
         cmbPersonaPacienteOrigenSordera = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "persona_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            UserMain( ) ;
            if ( ! isFullAjaxMode( ) )
            {
               Draw( ) ;
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbPersonaTipo.ItemCount > 0 )
         {
            A8PersonaTipo = (short)(NumberUtil.Val( cmbPersonaTipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A8PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbPersonaTipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaTipo_Internalname, "Values", cmbPersonaTipo.ToJavascriptSource(), true);
         }
         if ( cmbPersonaPacienteOrigenSordera.ItemCount > 0 )
         {
            A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cmbPersonaPacienteOrigenSordera.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0))), "."));
            n10PersonaPacienteOrigenSordera = false;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbPersonaPacienteOrigenSordera.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaPacienteOrigenSordera_Internalname, "Values", cmbPersonaPacienteOrigenSordera.ToJavascriptSource(), true);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "persona.aspx");
               context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
               context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
               context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
               context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2besmaintable_Internalname, 1, 0, "px", 0, "px", "", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2beserrviewercell_Internalname, 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = this_Class;
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, sPrefix, "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2besdataareacontainercell_Internalname, 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2babstracttabledataareacontainer_Internalname, 1, 0, "px", 0, "px", "Table_DataAreaContainer Table_TransactionDataAreaContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2btrnformmaintablecell_Internalname, 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTableattributesinformsection1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaID_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaID_Internalname, "ID", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaID_Internalname, A1PersonaID.ToString(), A1PersonaID.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,17);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaID_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtPersonaID_Enabled, 1, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaPNombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaPNombre_Internalname, "PNombre:", "col-sm-3 Attribute_TrnLabel Attribute_RequiredLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2PersonaPNombre", A2PersonaPNombre);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 22,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaPNombre_Internalname, A2PersonaPNombre, StringUtil.RTrim( context.localUtil.Format( A2PersonaPNombre, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,22);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaPNombre_Jsonclick, 0, edtPersonaPNombre_Class, "", "", "", "", 1, edtPersonaPNombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaSNombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaSNombre_Internalname, "SNombre", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3PersonaSNombre", A3PersonaSNombre);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaSNombre_Internalname, A3PersonaSNombre, StringUtil.RTrim( context.localUtil.Format( A3PersonaSNombre, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,27);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaSNombre_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtPersonaSNombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaPApellido_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaPApellido_Internalname, "PApellido", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4PersonaPApellido", A4PersonaPApellido);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaPApellido_Internalname, A4PersonaPApellido, StringUtil.RTrim( context.localUtil.Format( A4PersonaPApellido, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,32);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaPApellido_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtPersonaPApellido_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaSApellido_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaSApellido_Internalname, "SApellido", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5PersonaSApellido", A5PersonaSApellido);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 37,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaSApellido_Internalname, A5PersonaSApellido, StringUtil.RTrim( context.localUtil.Format( A5PersonaSApellido, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,37);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaSApellido_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtPersonaSApellido_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaSexo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaSexo_Internalname, "Sexo", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6PersonaSexo", A6PersonaSexo);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaSexo_Internalname, StringUtil.RTrim( A6PersonaSexo), StringUtil.RTrim( context.localUtil.Format( A6PersonaSexo, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,42);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaSexo_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtPersonaSexo_Enabled, 0, "text", "", 1, "chr", 1, "row", 1, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaDPI_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaDPI_Internalname, "DPI", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A7PersonaDPI", A7PersonaDPI);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 47,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaDPI_Internalname, A7PersonaDPI, StringUtil.RTrim( context.localUtil.Format( A7PersonaDPI, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,47);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaDPI_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtPersonaDPI_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbPersonaTipo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbPersonaTipo_Internalname, "Tipo", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A8PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 52,'" + sPrefix + "',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbPersonaTipo, cmbPersonaTipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)), 1, cmbPersonaTipo_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbPersonaTipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute_Trn", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,52);\"", "", true, "HLP_Persona.htm");
            cmbPersonaTipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaTipo_Internalname, "Values", (String)(cmbPersonaTipo.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaFechaNacimiento_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaFechaNacimiento_Internalname, "Fecha Nacimiento", "col-sm-3 Attribute_TrnDateLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9PersonaFechaNacimiento", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 57,'" + sPrefix + "',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtPersonaFechaNacimiento_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtPersonaFechaNacimiento_Internalname, context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"), context.localUtil.Format( A9PersonaFechaNacimiento, "99/99/99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',0,24,'spa',false,0);"+";gx.evt.onblur(this,57);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtPersonaFechaNacimiento_Jsonclick, 0, "Attribute_TrnDate", "", "", "", "", 1, edtPersonaFechaNacimiento_Enabled, 0, "text", "", 8, "chr", 1, "row", 8, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Persona.htm");
            GxWebStd.gx_bitmap( context, edtPersonaFechaNacimiento_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtPersonaFechaNacimiento_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Persona.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbPersonaPacienteOrigenSordera_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbPersonaPacienteOrigenSordera_Internalname, "Origen Sordera", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)));
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 62,'" + sPrefix + "',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbPersonaPacienteOrigenSordera, cmbPersonaPacienteOrigenSordera_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)), 1, cmbPersonaPacienteOrigenSordera_Jsonclick, 0, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbPersonaPacienteOrigenSordera.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute_Trn", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,62);\"", "", true, "HLP_Persona.htm");
            cmbPersonaPacienteOrigenSordera.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaPacienteOrigenSordera_Internalname, "Values", (String)(cmbPersonaPacienteOrigenSordera.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtPersonaEMail_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtPersonaEMail_Internalname, "EMail", "col-sm-3 Attribute_TrnLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11PersonaEMail", A11PersonaEMail);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtPersonaEMail_Internalname, A11PersonaEMail, StringUtil.RTrim( context.localUtil.Format( A11PersonaEMail, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,67);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "mailto:"+A11PersonaEMail, "", "", "", edtPersonaEMail_Jsonclick, 0, "Attribute_Trn", "", "", "", "", 1, edtPersonaEMail_Enabled, 0, "email", "", 80, "chr", 1, "row", 100, 0, 0, 0, 1, -1, 0, true, "GeneXus\\Email", "left", true, "HLP_Persona.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2besactioncontainercell_Internalname, 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table1_70_011( true) ;
         }
         return  ;
      }

      protected void wb_table1_70_011e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bescontrolbeaufitycell_Internalname, 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+sPrefix+"K2BCONTROLBEAUTIFY1Container"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
            RenderHtmlCloseForm011( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void wb_table1_70_011( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblActionscontainerbuttons_Internalname, tblActionscontainerbuttons_Internalname, "", "Table_TrnActionsContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='K2BToolsTableCell_ActionContainer'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'" + sPrefix + "',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttEnter_Internalname, "", bttEnter_Caption, bttEnter_Jsonclick, 5, bttEnter_Tooltiptext, "", StyleString, ClassString, bttEnter_Visible, bttEnter_Enabled, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td class='K2BToolsTableCell_ActionContainer'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'" + sPrefix + "',false,'',0)\"";
            ClassString = "K2BToolsButton_MinimalAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttCancel_Internalname, "", "Cancelar", bttCancel_Jsonclick, 5, "Cancelar", "", StyleString, ClassString, bttCancel_Visible, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'DOCANCEL\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_Persona.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_70_011e( true) ;
         }
         else
         {
            wb_table1_70_011e( false) ;
         }
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               standaloneStartupServer( ) ;
            }
         }
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11012 ();
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         nDoneStart = 1;
         if ( AnyError == 0 )
         {
            sXEvt = cgiGet( "_EventName");
            if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( StringUtil.StrCmp(cgiGet( edtPersonaID_Internalname), "") == 0 )
               {
                  A1PersonaID = (Guid)(Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
               }
               else
               {
                  try
                  {
                     A1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtPersonaID_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "PERSONAID");
                     AnyError = 1;
                     GX_FocusControl = edtPersonaID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               A2PersonaPNombre = cgiGet( edtPersonaPNombre_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2PersonaPNombre", A2PersonaPNombre);
               A3PersonaSNombre = cgiGet( edtPersonaSNombre_Internalname);
               n3PersonaSNombre = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3PersonaSNombre", A3PersonaSNombre);
               n3PersonaSNombre = (String.IsNullOrEmpty(StringUtil.RTrim( A3PersonaSNombre)) ? true : false);
               A4PersonaPApellido = cgiGet( edtPersonaPApellido_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4PersonaPApellido", A4PersonaPApellido);
               A5PersonaSApellido = cgiGet( edtPersonaSApellido_Internalname);
               n5PersonaSApellido = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5PersonaSApellido", A5PersonaSApellido);
               n5PersonaSApellido = (String.IsNullOrEmpty(StringUtil.RTrim( A5PersonaSApellido)) ? true : false);
               A6PersonaSexo = cgiGet( edtPersonaSexo_Internalname);
               n6PersonaSexo = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6PersonaSexo", A6PersonaSexo);
               n6PersonaSexo = (String.IsNullOrEmpty(StringUtil.RTrim( A6PersonaSexo)) ? true : false);
               A7PersonaDPI = cgiGet( edtPersonaDPI_Internalname);
               n7PersonaDPI = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A7PersonaDPI", A7PersonaDPI);
               n7PersonaDPI = (String.IsNullOrEmpty(StringUtil.RTrim( A7PersonaDPI)) ? true : false);
               cmbPersonaTipo.CurrentValue = cgiGet( cmbPersonaTipo_Internalname);
               A8PersonaTipo = (short)(NumberUtil.Val( cgiGet( cmbPersonaTipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A8PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)));
               if ( context.localUtil.VCDate( cgiGet( edtPersonaFechaNacimiento_Internalname), 2) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_faildate", new   object[]  {"Persona Fecha Nacimiento"}), 1, "PERSONAFECHANACIMIENTO");
                  AnyError = 1;
                  GX_FocusControl = edtPersonaFechaNacimiento_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A9PersonaFechaNacimiento = DateTime.MinValue;
                  n9PersonaFechaNacimiento = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9PersonaFechaNacimiento", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
               }
               else
               {
                  A9PersonaFechaNacimiento = context.localUtil.CToD( cgiGet( edtPersonaFechaNacimiento_Internalname), 2);
                  n9PersonaFechaNacimiento = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9PersonaFechaNacimiento", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
               }
               n9PersonaFechaNacimiento = ((DateTime.MinValue==A9PersonaFechaNacimiento) ? true : false);
               cmbPersonaPacienteOrigenSordera.CurrentValue = cgiGet( cmbPersonaPacienteOrigenSordera_Internalname);
               A10PersonaPacienteOrigenSordera = (short)(NumberUtil.Val( cgiGet( cmbPersonaPacienteOrigenSordera_Internalname), "."));
               n10PersonaPacienteOrigenSordera = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)));
               n10PersonaPacienteOrigenSordera = ((0==A10PersonaPacienteOrigenSordera) ? true : false);
               A11PersonaEMail = cgiGet( edtPersonaEMail_Internalname);
               n11PersonaEMail = false;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11PersonaEMail", A11PersonaEMail);
               n11PersonaEMail = (String.IsNullOrEmpty(StringUtil.RTrim( A11PersonaEMail)) ? true : false);
               /* Read saved values. */
               Z1PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( sPrefix+"Z1PersonaID")));
               Z2PersonaPNombre = cgiGet( sPrefix+"Z2PersonaPNombre");
               Z3PersonaSNombre = cgiGet( sPrefix+"Z3PersonaSNombre");
               n3PersonaSNombre = (String.IsNullOrEmpty(StringUtil.RTrim( A3PersonaSNombre)) ? true : false);
               Z4PersonaPApellido = cgiGet( sPrefix+"Z4PersonaPApellido");
               Z5PersonaSApellido = cgiGet( sPrefix+"Z5PersonaSApellido");
               n5PersonaSApellido = (String.IsNullOrEmpty(StringUtil.RTrim( A5PersonaSApellido)) ? true : false);
               Z6PersonaSexo = cgiGet( sPrefix+"Z6PersonaSexo");
               n6PersonaSexo = (String.IsNullOrEmpty(StringUtil.RTrim( A6PersonaSexo)) ? true : false);
               Z7PersonaDPI = cgiGet( sPrefix+"Z7PersonaDPI");
               n7PersonaDPI = (String.IsNullOrEmpty(StringUtil.RTrim( A7PersonaDPI)) ? true : false);
               Z8PersonaTipo = (short)(context.localUtil.CToN( cgiGet( sPrefix+"Z8PersonaTipo"), ",", "."));
               Z9PersonaFechaNacimiento = context.localUtil.CToD( cgiGet( sPrefix+"Z9PersonaFechaNacimiento"), 0);
               n9PersonaFechaNacimiento = ((DateTime.MinValue==A9PersonaFechaNacimiento) ? true : false);
               Z10PersonaPacienteOrigenSordera = (short)(context.localUtil.CToN( cgiGet( sPrefix+"Z10PersonaPacienteOrigenSordera"), ",", "."));
               n10PersonaPacienteOrigenSordera = ((0==A10PersonaPacienteOrigenSordera) ? true : false);
               Z11PersonaEMail = cgiGet( sPrefix+"Z11PersonaEMail");
               n11PersonaEMail = (String.IsNullOrEmpty(StringUtil.RTrim( A11PersonaEMail)) ? true : false);
               wcpOGx_mode = cgiGet( sPrefix+"wcpOGx_mode");
               wcpOAV7PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( sPrefix+"wcpOAV7PersonaID")));
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( sPrefix+"IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( sPrefix+"IsModified"), ",", "."));
               Gx_mode = cgiGet( sPrefix+"Mode");
               AV7PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( sPrefix+"vPERSONAID")));
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( sPrefix+"vGXBSCREEN"), ",", "."));
               AV24Pgmname = cgiGet( sPrefix+"vPGMNAME");
               Gx_mode = cgiGet( sPrefix+"vMODE");
               K2bcontrolbeautify1_Objectcall = cgiGet( sPrefix+"K2BCONTROLBEAUTIFY1_Objectcall");
               K2bcontrolbeautify1_Class = cgiGet( sPrefix+"K2BCONTROLBEAUTIFY1_Class");
               K2bcontrolbeautify1_Enabled = StringUtil.StrToBool( cgiGet( sPrefix+"K2BCONTROLBEAUTIFY1_Enabled"));
               K2bcontrolbeautify1_Visible = StringUtil.StrToBool( cgiGet( sPrefix+"K2BCONTROLBEAUTIFY1_Visible"));
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               forbiddenHiddens = sPrefix + "hsh" + "Persona";
               forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
               hsh = cgiGet( sPrefix+"hsh");
               if ( ( ! ( ( A1PersonaID != Z1PersonaID ) ) || ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) ) && ! GXUtil.CheckEncryptedHash( forbiddenHiddens, hsh, GXKey) )
               {
                  GXUtil.WriteLog("persona:[SecurityCheckFailed value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
                  GxWebError = 1;
                  context.HttpContext.Response.StatusDescription = 403.ToString();
                  context.HttpContext.Response.StatusCode = 403;
                  context.WriteHtmlText( "<title>403 Forbidden</title>") ;
                  context.WriteHtmlText( "<h1>403 Forbidden</h1>") ;
                  context.WriteHtmlText( "<p /><hr />") ;
                  GXUtil.WriteLog("send_http_error_code " + 403.ToString());
                  AnyError = 1;
                  return  ;
               }
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  A1PersonaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                  getEqualNoModal( ) ;
                  if ( ! (Guid.Empty==AV7PersonaID) )
                  {
                     A1PersonaID = (Guid)(AV7PersonaID);
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                  }
                  else
                  {
                     if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A1PersonaID) && ( Gx_BScreen == 0 ) )
                     {
                        A1PersonaID = (Guid)(Guid.NewGuid( ));
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                     }
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  disable_std_buttons( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
                  {
                     sMode1 = Gx_mode;
                     Gx_mode = "UPD";
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                     if ( ! (Guid.Empty==AV7PersonaID) )
                     {
                        A1PersonaID = (Guid)(AV7PersonaID);
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                     }
                     else
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A1PersonaID) && ( Gx_BScreen == 0 ) )
                        {
                           A1PersonaID = (Guid)(Guid.NewGuid( ));
                           context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                        }
                     }
                     Gx_mode = sMode1;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
                  }
                  standaloneModal( ) ;
                  if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
                  {
                     getByPrimaryKey( ) ;
                     if ( RcdFound1 == 1 )
                     {
                        if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                        {
                           /* Confirm record */
                           CONFIRM_010( ) ;
                           if ( AnyError == 0 )
                           {
                              GX_FocusControl = bttEnter_Internalname;
                              context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noinsert", ""), 1, "PERSONAID");
                        AnyError = 1;
                        GX_FocusControl = edtPersonaID_Internalname;
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
      }

      protected void Process( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read Transaction buttons. */
            if ( context.wbHandled == 0 )
            {
               if ( StringUtil.Len( sPrefix) == 0 )
               {
                  sEvt = cgiGet( "_EventName");
                  EvtGridId = cgiGet( "_EventGridId");
                  EvtRowId = cgiGet( "_EventRowId");
               }
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 dynload_actions( ) ;
                                 /* Execute user event: Start */
                                 E11012 ();
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "AFTER TRN") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 dynload_actions( ) ;
                                 /* Execute user event: After Trn */
                                 E12012 ();
                              }
                           }
                        }
                        else if ( StringUtil.StrCmp(sEvt, "'DOCANCEL'") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 dynload_actions( ) ;
                                 /* Execute user event: 'DoCancel' */
                                 E13012 ();
                              }
                           }
                           nKeyPressed = 3;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                           {
                              standaloneStartupServer( ) ;
                           }
                           if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
                                 {
                                    btn_enter( ) ;
                                 }
                              }
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            /* Execute user event: After Trn */
            E12012 ();
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll011( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
            {
               bttEnter_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttEnter_Visible), 5, 0)), true);
            }
            DisableAttributes011( ) ;
         }
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaPNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPNombre_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaSNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSNombre_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaPApellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPApellido_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaSApellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSApellido_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaSexo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSexo_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaDPI_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaDPI_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaTipo.Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaFechaNacimiento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaFechaNacimiento_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaPacienteOrigenSordera_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaPacienteOrigenSordera.Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaEMail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaEMail_Enabled), 5, 0)), true);
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttEnter_Visible), 5, 0)), true);
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void CONFIRM_010( )
      {
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls011( ) ;
            }
            else
            {
               CheckExtendedTable011( ) ;
               CloseExtendedTableCursors011( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         }
      }

      protected void ResetCaption010( )
      {
      }

      protected void E11012( )
      {
         /* Start Routine */
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV16StandardActivityType = "Insert";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StandardActivityType", AV16StandardActivityType);
            AV17UserActivityType = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UserActivityType", AV17UserActivityType);
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV16StandardActivityType = "Update";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StandardActivityType", AV16StandardActivityType);
            AV17UserActivityType = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UserActivityType", AV17UserActivityType);
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV16StandardActivityType = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StandardActivityType", AV16StandardActivityType);
            AV17UserActivityType = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UserActivityType", AV17UserActivityType);
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            AV16StandardActivityType = "Display";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StandardActivityType", AV16StandardActivityType);
            AV17UserActivityType = "";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UserActivityType", AV17UserActivityType);
         }
         new k2bisauthorizedactivityname(context ).execute(  "Persona",  "Persona",  AV16StandardActivityType,  AV17UserActivityType,  AV24Pgmname, out  AV18IsAuthorized) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16StandardActivityType", AV16StandardActivityType);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17UserActivityType", AV17UserActivityType);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Pgmname", AV24Pgmname);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV18IsAuthorized", AV18IsAuthorized);
         if ( ! AV18IsAuthorized )
         {
            CallWebObject(formatLink("k2bnotauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim("Persona")) + "," + UrlEncode(StringUtil.RTrim("Persona")) + "," + UrlEncode(StringUtil.RTrim(AV16StandardActivityType)) + "," + UrlEncode(StringUtil.RTrim(AV17UserActivityType)) + "," + UrlEncode(StringUtil.RTrim(AV24Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         new k2bgetcontext(context ).execute( out  AV12Context) ;
         AV13BtnCaption = "Confirmar";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13BtnCaption", AV13BtnCaption);
         AV14BtnTooltip = "Confirmar";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14BtnTooltip", AV14BtnTooltip);
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV13BtnCaption = "Actualizar";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13BtnCaption", AV13BtnCaption);
            AV14BtnTooltip = "Actualizar";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14BtnTooltip", AV14BtnTooltip);
         }
         else if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV13BtnCaption = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13BtnCaption", AV13BtnCaption);
            AV14BtnTooltip = "Confirmar";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14BtnTooltip", AV14BtnTooltip);
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV13BtnCaption = "Eliminar";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV13BtnCaption", AV13BtnCaption);
            AV14BtnTooltip = "Eliminar";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV14BtnTooltip", AV14BtnTooltip);
         }
         bttEnter_Caption = AV13BtnCaption;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Caption", bttEnter_Caption, true);
         bttEnter_Tooltiptext = AV14BtnTooltip;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Tooltiptext", bttEnter_Tooltiptext, true);
         bttEnter_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttEnter_Visible), 5, 0)), true);
         bttCancel_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttCancel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCancel_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 )
         {
            bttEnter_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttEnter_Visible), 5, 0)), true);
            bttCancel_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttCancel_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttCancel_Visible), 5, 0)), true);
         }
         new k2bgettrncontextbyname(context ).execute(  "Persona", out  AV8TrnContext) ;
         if ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Returnmode, "value 0") == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               Form.Caption = "Insertar Persona";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Caption", Form.Caption, true);
            }
            else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
            {
               Form.Caption = "Actualizar Persona";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Caption", Form.Caption, true);
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               Form.Caption = "Eliminar Persona";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Caption", Form.Caption, true);
            }
         }
      }

      protected void E12012( )
      {
         /* After Trn Routine */
         if ( AV8TrnContext.gxTpr_Savepk )
         {
            AV20AttributeValue = new GXBaseCollection<SdtK2BAttributeValue_K2BAttributeValueItem>( context, "K2BAttributeValueItem", "PACYE2");
            AV21AttributeValueItem = new SdtK2BAttributeValue_K2BAttributeValueItem(context);
            AV21AttributeValueItem.gxTpr_Attributename = "PersonaID";
            AV21AttributeValueItem.gxTpr_Attributevalue = A1PersonaID.ToString();
            AV20AttributeValue.Add(AV21AttributeValueItem, 0);
            new k2bsettransactionpk(context ).execute(  "Persona",  AV20AttributeValue) ;
         }
         AV22Message = new SdtMessages_Message(context);
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV22Message.gxTpr_Description = StringUtil.Format( "La persona %1 fue creada", A2PersonaPNombre, "", "", "", "", "", "", "", "");
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV22Message.gxTpr_Description = StringUtil.Format( "La persona %1 fue actualizada", A2PersonaPNombre, "", "", "", "", "", "", "", "");
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV22Message.gxTpr_Description = StringUtil.Format( "La persona %1 fue eliminada", A2PersonaPNombre, "", "", "", "", "", "", "", "");
         }
         new k2btoolsmessagequeueadd(context ).execute(  AV22Message) ;
         if ( ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( AV8TrnContext.gxTpr_Afterinsert.gxTpr_Aftertrn != 5 ) ) || ( ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) && ( AV8TrnContext.gxTpr_Afterupdate.gxTpr_Aftertrn != 5 ) ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            new k2bremovetrncontextbyname(context ).execute(  "Persona") ;
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            AV10Navigation = AV8TrnContext.gxTpr_Afterinsert;
            /* Execute user subroutine: 'DOAFTERTRNACTION' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            AV10Navigation = AV8TrnContext.gxTpr_Afterupdate;
            /* Execute user subroutine: 'DOAFTERTRNACTION' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV10Navigation = AV8TrnContext.gxTpr_Afterdelete;
            /* Execute user subroutine: 'DOAFTERTRNACTION' */
            S112 ();
            if ( returnInSub )
            {
               returnInSub = true;
               if (true) return;
            }
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV10Navigation", AV10Navigation);
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri(sPrefix, false, "AV8TrnContext", AV8TrnContext);
      }

      protected void S112( )
      {
         /* 'DOAFTERTRNACTION' Routine */
         GXt_char1 = AV19encrypt;
         new k2btoolsgetuseencryption(context ).execute( out  GXt_char1) ;
         AV19encrypt = GXt_char1;
         if ( AV10Navigation.gxTpr_Aftertrn == 2 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem("K2BEntityServices: TransactionNavigation Invalid invocation method. Delete method cannot return using entity manager");
            }
            else
            {
               AV11DinamicObjToLink = StringUtil.Lower( AV8TrnContext.gxTpr_Entitymanagername);
               new k2bgetdynamicobjecttolink(context ).execute( ref  AV11DinamicObjToLink) ;
               if ( StringUtil.StrCmp(AV19encrypt, "SITE") == 0 )
               {
                  try {
                     args = new Object[] {(String)"_site_encryption",AV8TrnContext.gxTpr_Entitymanagernexttaskmode,(Guid)A1PersonaID,AV8TrnContext.gxTpr_Entitymanagernexttaskcode} ;
                     ClassLoader.WebExecute(AV11DinamicObjToLink,"GeneXus.Programs",AV11DinamicObjToLink.ToLower().Trim(), new Object[] {context }, "execute", args);
                     if ( ( args != null ) && ( args.Length == 4 ) )
                     {
                        AV8TrnContext.gxTpr_Entitymanagernexttaskmode = (String)(args[1]) ;
                        A1PersonaID = (Guid)((args[2])) ;
                        AV8TrnContext.gxTpr_Entitymanagernexttaskcode = (String)(args[3]) ;
                     }
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                  }
                  catch (GxClassLoaderException) {
                     if ( AV11DinamicObjToLink .Trim().Length < 6 || AV11DinamicObjToLink .Substring( AV11DinamicObjToLink .Trim().Length - 5, 5) != ".aspx")
                     {
                        context.wjLoc = formatLink(AV11DinamicObjToLink+".aspx") + "?" + UrlEncode(StringUtil.RTrim("_site_encryption")) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskmode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskcode));
                     }
                     else
                     {
                        GXKey = Crypto.GetSiteKey( );
                        GXEncryptionTmp = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskmode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskcode));
                        context.wjLoc = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+ "?" + Crypto.Encrypt64( GXEncryptionTmp+Crypto.CheckSum( GXEncryptionTmp, 6), GXKey);
                     }
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(AV19encrypt, "SESSION") == 0 )
                  {
                     try {
                        args = new Object[] {(String)"_session_encryption",AV8TrnContext.gxTpr_Entitymanagernexttaskmode,(Guid)A1PersonaID,AV8TrnContext.gxTpr_Entitymanagernexttaskcode} ;
                        ClassLoader.WebExecute(AV11DinamicObjToLink,"GeneXus.Programs",AV11DinamicObjToLink.ToLower().Trim(), new Object[] {context }, "execute", args);
                        if ( ( args != null ) && ( args.Length == 4 ) )
                        {
                           AV8TrnContext.gxTpr_Entitymanagernexttaskmode = (String)(args[1]) ;
                           A1PersonaID = (Guid)((args[2])) ;
                           AV8TrnContext.gxTpr_Entitymanagernexttaskcode = (String)(args[3]) ;
                        }
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                     }
                     catch (GxClassLoaderException) {
                        if ( AV11DinamicObjToLink .Trim().Length < 6 || AV11DinamicObjToLink .Substring( AV11DinamicObjToLink .Trim().Length - 5, 5) != ".aspx")
                        {
                           context.wjLoc = formatLink(AV11DinamicObjToLink+".aspx") + "?" + UrlEncode(StringUtil.RTrim("_session_encryption")) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskmode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskcode));
                        }
                        else
                        {
                           if ( StringUtil.Len( sPrefix) == 0 )
                           {
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
                              {
                                 gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
                              }
                           }
                           GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                           GXEncryptionTmp = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskmode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskcode));
                           context.wjLoc = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+ "?" + Crypto.Encrypt64( GXEncryptionTmp+Crypto.CheckSum( GXEncryptionTmp, 6), GXKey);
                        }
                     }
                  }
                  else
                  {
                     try {
                        args = new Object[] {AV8TrnContext.gxTpr_Entitymanagernexttaskmode,(Guid)A1PersonaID,AV8TrnContext.gxTpr_Entitymanagernexttaskcode} ;
                        ClassLoader.WebExecute(AV11DinamicObjToLink,"GeneXus.Programs",AV11DinamicObjToLink.ToLower().Trim(), new Object[] {context }, "execute", args);
                        if ( ( args != null ) && ( args.Length == 3 ) )
                        {
                           AV8TrnContext.gxTpr_Entitymanagernexttaskmode = (String)(args[0]) ;
                           A1PersonaID = (Guid)((args[1])) ;
                           AV8TrnContext.gxTpr_Entitymanagernexttaskcode = (String)(args[2]) ;
                        }
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                     }
                     catch (GxClassLoaderException) {
                        if ( AV11DinamicObjToLink .Trim().Length < 6 || AV11DinamicObjToLink .Substring( AV11DinamicObjToLink .Trim().Length - 5, 5) != ".aspx")
                        {
                           context.wjLoc = formatLink(AV11DinamicObjToLink+".aspx") + "?" + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskmode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskcode));
                        }
                        else
                        {
                           context.wjLoc = formatLink(AV11DinamicObjToLink) + "?" + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskmode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV8TrnContext.gxTpr_Entitymanagernexttaskcode));
                        }
                     }
                  }
               }
            }
         }
         else
         {
            if ( AV10Navigation.gxTpr_Aftertrn == 3 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( AV10Navigation.gxTpr_Mode)) )
               {
                  AV10Navigation.gxTpr_Mode = Gx_mode;
               }
               AV11DinamicObjToLink = StringUtil.Lower( AV10Navigation.gxTpr_Objecttolink);
               new k2bgetdynamicobjecttolink(context ).execute( ref  AV11DinamicObjToLink) ;
               if ( StringUtil.StrCmp(AV19encrypt, "SITE") == 0 )
               {
                  try {
                     args = new Object[] {(String)"_site_encryption",AV10Navigation.gxTpr_Mode,(Guid)A1PersonaID,AV10Navigation.gxTpr_Extraparameter} ;
                     ClassLoader.WebExecute(AV11DinamicObjToLink,"GeneXus.Programs",AV11DinamicObjToLink.ToLower().Trim(), new Object[] {context }, "execute", args);
                     if ( ( args != null ) && ( args.Length == 4 ) )
                     {
                        AV10Navigation.gxTpr_Mode = (String)(args[1]) ;
                        A1PersonaID = (Guid)((args[2])) ;
                        AV10Navigation.gxTpr_Extraparameter = (String)(args[3]) ;
                     }
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                  }
                  catch (GxClassLoaderException) {
                     if ( AV11DinamicObjToLink .Trim().Length < 6 || AV11DinamicObjToLink .Substring( AV11DinamicObjToLink .Trim().Length - 5, 5) != ".aspx")
                     {
                        context.wjLoc = formatLink(AV11DinamicObjToLink+".aspx") + "?" + UrlEncode(StringUtil.RTrim("_site_encryption")) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Mode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Extraparameter));
                     }
                     else
                     {
                        GXKey = Crypto.GetSiteKey( );
                        GXEncryptionTmp = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Mode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Extraparameter));
                        context.wjLoc = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+ "?" + Crypto.Encrypt64( GXEncryptionTmp+Crypto.CheckSum( GXEncryptionTmp, 6), GXKey);
                     }
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(AV19encrypt, "SESSION") == 0 )
                  {
                     try {
                        args = new Object[] {(String)"_session_encryption",AV10Navigation.gxTpr_Mode,(Guid)A1PersonaID,AV10Navigation.gxTpr_Extraparameter} ;
                        ClassLoader.WebExecute(AV11DinamicObjToLink,"GeneXus.Programs",AV11DinamicObjToLink.ToLower().Trim(), new Object[] {context }, "execute", args);
                        if ( ( args != null ) && ( args.Length == 4 ) )
                        {
                           AV10Navigation.gxTpr_Mode = (String)(args[1]) ;
                           A1PersonaID = (Guid)((args[2])) ;
                           AV10Navigation.gxTpr_Extraparameter = (String)(args[3]) ;
                        }
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                     }
                     catch (GxClassLoaderException) {
                        if ( AV11DinamicObjToLink .Trim().Length < 6 || AV11DinamicObjToLink .Substring( AV11DinamicObjToLink .Trim().Length - 5, 5) != ".aspx")
                        {
                           context.wjLoc = formatLink(AV11DinamicObjToLink+".aspx") + "?" + UrlEncode(StringUtil.RTrim("_session_encryption")) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Mode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Extraparameter));
                        }
                        else
                        {
                           if ( StringUtil.Len( sPrefix) == 0 )
                           {
                              if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
                              {
                                 gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
                              }
                           }
                           GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
                           GXEncryptionTmp = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Mode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Extraparameter));
                           context.wjLoc = StringUtil.Trim( StringUtil.Lower( AV11DinamicObjToLink))+ "?" + Crypto.Encrypt64( GXEncryptionTmp+Crypto.CheckSum( GXEncryptionTmp, 6), GXKey);
                        }
                     }
                  }
                  else
                  {
                     try {
                        args = new Object[] {AV10Navigation.gxTpr_Mode,(Guid)A1PersonaID,AV10Navigation.gxTpr_Extraparameter} ;
                        ClassLoader.WebExecute(AV11DinamicObjToLink,"GeneXus.Programs",AV11DinamicObjToLink.ToLower().Trim(), new Object[] {context }, "execute", args);
                        if ( ( args != null ) && ( args.Length == 3 ) )
                        {
                           AV10Navigation.gxTpr_Mode = (String)(args[0]) ;
                           A1PersonaID = (Guid)((args[1])) ;
                           AV10Navigation.gxTpr_Extraparameter = (String)(args[2]) ;
                        }
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                     }
                     catch (GxClassLoaderException) {
                        if ( AV11DinamicObjToLink .Trim().Length < 6 || AV11DinamicObjToLink .Substring( AV11DinamicObjToLink .Trim().Length - 5, 5) != ".aspx")
                        {
                           context.wjLoc = formatLink(AV11DinamicObjToLink+".aspx") + "?" + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Mode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Extraparameter));
                        }
                        else
                        {
                           context.wjLoc = formatLink(AV11DinamicObjToLink) + "?" + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Mode)) + "," + UrlEncode(A1PersonaID.ToString()) + "," + UrlEncode(StringUtil.RTrim(AV10Navigation.gxTpr_Extraparameter));
                        }
                     }
                  }
               }
            }
            else
            {
               if ( AV10Navigation.gxTpr_Aftertrn != 5 )
               {
                  /* Execute user subroutine: 'K2BCLOSE' */
                  S122 ();
                  if (returnInSub) return;
               }
            }
         }
      }

      protected void E13012( )
      {
         /* 'DoCancel' Routine */
         new k2bremovetrncontextbyname(context ).execute(  "Persona") ;
         /* Execute user subroutine: 'K2BCLOSE' */
         S122 ();
         if ( returnInSub )
         {
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S122( )
      {
         /* 'K2BCLOSE' Routine */
         if ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Returnmode, "Stack") == 0 )
         {
            context.setWebReturnParms(new Object[] {});
            context.setWebReturnParmsMetadata(new Object[] {});
            context.wjLocDisableFrm = 1;
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
         else
         {
            if ( StringUtil.StrCmp(AV8TrnContext.gxTpr_Returnmode, "CallerObject") == 0 )
            {
               AV15Url = AV8TrnContext.gxTpr_Callerurl;
               CallWebObject(formatLink(AV15Url) );
               context.wjLocDisableFrm = 0;
            }
            else
            {
               context.setWebReturnParms(new Object[] {});
               context.setWebReturnParmsMetadata(new Object[] {});
               context.wjLocDisableFrm = 1;
               context.nUserReturn = 1;
               returnInSub = true;
               if (true) return;
            }
         }
      }

      protected void ZM011( short GX_JID )
      {
         if ( ( GX_JID == 11 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z2PersonaPNombre = T00013_A2PersonaPNombre[0];
               Z3PersonaSNombre = T00013_A3PersonaSNombre[0];
               Z4PersonaPApellido = T00013_A4PersonaPApellido[0];
               Z5PersonaSApellido = T00013_A5PersonaSApellido[0];
               Z6PersonaSexo = T00013_A6PersonaSexo[0];
               Z7PersonaDPI = T00013_A7PersonaDPI[0];
               Z8PersonaTipo = T00013_A8PersonaTipo[0];
               Z9PersonaFechaNacimiento = T00013_A9PersonaFechaNacimiento[0];
               Z10PersonaPacienteOrigenSordera = T00013_A10PersonaPacienteOrigenSordera[0];
               Z11PersonaEMail = T00013_A11PersonaEMail[0];
            }
            else
            {
               Z2PersonaPNombre = A2PersonaPNombre;
               Z3PersonaSNombre = A3PersonaSNombre;
               Z4PersonaPApellido = A4PersonaPApellido;
               Z5PersonaSApellido = A5PersonaSApellido;
               Z6PersonaSexo = A6PersonaSexo;
               Z7PersonaDPI = A7PersonaDPI;
               Z8PersonaTipo = A8PersonaTipo;
               Z9PersonaFechaNacimiento = A9PersonaFechaNacimiento;
               Z10PersonaPacienteOrigenSordera = A10PersonaPacienteOrigenSordera;
               Z11PersonaEMail = A11PersonaEMail;
            }
         }
         if ( GX_JID == -11 )
         {
            Z1PersonaID = (Guid)(A1PersonaID);
            Z2PersonaPNombre = A2PersonaPNombre;
            Z3PersonaSNombre = A3PersonaSNombre;
            Z4PersonaPApellido = A4PersonaPApellido;
            Z5PersonaSApellido = A5PersonaSApellido;
            Z6PersonaSexo = A6PersonaSexo;
            Z7PersonaDPI = A7PersonaDPI;
            Z8PersonaTipo = A8PersonaTipo;
            Z9PersonaFechaNacimiento = A9PersonaFechaNacimiento;
            Z10PersonaPacienteOrigenSordera = A10PersonaPacienteOrigenSordera;
            Z11PersonaEMail = A11PersonaEMail;
         }
      }

      protected void standaloneNotModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            edtPersonaPNombre_Class = "Attribute_Trn"+" "+"Attribute_RequiredReadOnly";
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaPNombre_Internalname, "Class", edtPersonaPNombre_Class, true);
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "DSP") != 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
            {
               edtPersonaPNombre_Class = "Attribute_Trn"+" "+"Attribute_Required";
               context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaPNombre_Internalname, "Class", edtPersonaPNombre_Class, true);
            }
         }
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         if ( ! (Guid.Empty==AV7PersonaID) )
         {
            edtPersonaID_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaID_Enabled), 5, 0)), true);
         }
         else
         {
            edtPersonaID_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaID_Enabled), 5, 0)), true);
         }
         if ( ! (Guid.Empty==AV7PersonaID) )
         {
            edtPersonaID_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaID_Enabled), 5, 0)), true);
         }
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )  )
         {
            this_Class = "K2BWarning";
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttEnter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttEnter_Enabled), 5, 0)), true);
         }
         else
         {
            bttEnter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttEnter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttEnter_Enabled), 5, 0)), true);
         }
         if ( ! (Guid.Empty==AV7PersonaID) )
         {
            A1PersonaID = (Guid)(AV7PersonaID);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
         }
         else
         {
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A1PersonaID) && ( Gx_BScreen == 0 ) )
            {
               A1PersonaID = (Guid)(Guid.NewGuid( ));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
            }
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
            AV24Pgmname = "Persona";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Pgmname", AV24Pgmname);
         }
      }

      protected void Load011( )
      {
         /* Using cursor T00014 */
         pr_default.execute(2, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(2) != 101) )
         {
            RcdFound1 = 1;
            A2PersonaPNombre = T00014_A2PersonaPNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2PersonaPNombre", A2PersonaPNombre);
            A3PersonaSNombre = T00014_A3PersonaSNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3PersonaSNombre", A3PersonaSNombre);
            n3PersonaSNombre = T00014_n3PersonaSNombre[0];
            A4PersonaPApellido = T00014_A4PersonaPApellido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4PersonaPApellido", A4PersonaPApellido);
            A5PersonaSApellido = T00014_A5PersonaSApellido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5PersonaSApellido", A5PersonaSApellido);
            n5PersonaSApellido = T00014_n5PersonaSApellido[0];
            A6PersonaSexo = T00014_A6PersonaSexo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6PersonaSexo", A6PersonaSexo);
            n6PersonaSexo = T00014_n6PersonaSexo[0];
            A7PersonaDPI = T00014_A7PersonaDPI[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A7PersonaDPI", A7PersonaDPI);
            n7PersonaDPI = T00014_n7PersonaDPI[0];
            A8PersonaTipo = T00014_A8PersonaTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A8PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)));
            A9PersonaFechaNacimiento = T00014_A9PersonaFechaNacimiento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9PersonaFechaNacimiento", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
            n9PersonaFechaNacimiento = T00014_n9PersonaFechaNacimiento[0];
            A10PersonaPacienteOrigenSordera = T00014_A10PersonaPacienteOrigenSordera[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)));
            n10PersonaPacienteOrigenSordera = T00014_n10PersonaPacienteOrigenSordera[0];
            A11PersonaEMail = T00014_A11PersonaEMail[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11PersonaEMail", A11PersonaEMail);
            n11PersonaEMail = T00014_n11PersonaEMail[0];
            ZM011( -11) ;
         }
         pr_default.close(2);
         OnLoadActions011( ) ;
      }

      protected void OnLoadActions011( )
      {
         AV24Pgmname = "Persona";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Pgmname", AV24Pgmname);
      }

      protected void CheckExtendedTable011( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         AV24Pgmname = "Persona";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Pgmname", AV24Pgmname);
         if ( String.IsNullOrEmpty(StringUtil.RTrim( A2PersonaPNombre)) )
         {
            GX_msglist.addItem("PNombre es obligatorio", 1, "PERSONAPNOMBRE");
            AnyError = 1;
            GX_FocusControl = edtPersonaPNombre_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( (DateTime.MinValue==A9PersonaFechaNacimiento) || ( A9PersonaFechaNacimiento >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Persona Fecha Nacimiento fuera de rango", "OutOfRange", 1, "PERSONAFECHANACIMIENTO");
            AnyError = 1;
            GX_FocusControl = edtPersonaFechaNacimiento_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( GxRegex.IsMatch(A11PersonaEMail,"^((\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*)|(\\s*))$") || String.IsNullOrEmpty(StringUtil.RTrim( A11PersonaEMail)) ) )
         {
            GX_msglist.addItem("El valor de Persona EMail no coincide con el patr�n especificado", "OutOfRange", 1, "PERSONAEMAIL");
            AnyError = 1;
            GX_FocusControl = edtPersonaEMail_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
      }

      protected void CloseExtendedTableCursors011( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey011( )
      {
         /* Using cursor T00015 */
         pr_default.execute(3, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound1 = 1;
         }
         else
         {
            RcdFound1 = 0;
         }
         pr_default.close(3);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00013 */
         pr_default.execute(1, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM011( 11) ;
            RcdFound1 = 1;
            A1PersonaID = (Guid)((Guid)(T00013_A1PersonaID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
            A2PersonaPNombre = T00013_A2PersonaPNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2PersonaPNombre", A2PersonaPNombre);
            A3PersonaSNombre = T00013_A3PersonaSNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3PersonaSNombre", A3PersonaSNombre);
            n3PersonaSNombre = T00013_n3PersonaSNombre[0];
            A4PersonaPApellido = T00013_A4PersonaPApellido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4PersonaPApellido", A4PersonaPApellido);
            A5PersonaSApellido = T00013_A5PersonaSApellido[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5PersonaSApellido", A5PersonaSApellido);
            n5PersonaSApellido = T00013_n5PersonaSApellido[0];
            A6PersonaSexo = T00013_A6PersonaSexo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6PersonaSexo", A6PersonaSexo);
            n6PersonaSexo = T00013_n6PersonaSexo[0];
            A7PersonaDPI = T00013_A7PersonaDPI[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A7PersonaDPI", A7PersonaDPI);
            n7PersonaDPI = T00013_n7PersonaDPI[0];
            A8PersonaTipo = T00013_A8PersonaTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A8PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)));
            A9PersonaFechaNacimiento = T00013_A9PersonaFechaNacimiento[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9PersonaFechaNacimiento", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
            n9PersonaFechaNacimiento = T00013_n9PersonaFechaNacimiento[0];
            A10PersonaPacienteOrigenSordera = T00013_A10PersonaPacienteOrigenSordera[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)));
            n10PersonaPacienteOrigenSordera = T00013_n10PersonaPacienteOrigenSordera[0];
            A11PersonaEMail = T00013_A11PersonaEMail[0];
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11PersonaEMail", A11PersonaEMail);
            n11PersonaEMail = T00013_n11PersonaEMail[0];
            Z1PersonaID = (Guid)(A1PersonaID);
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
            Load011( ) ;
            if ( AnyError == 1 )
            {
               RcdFound1 = 0;
               InitializeNonKey011( ) ;
            }
            Gx_mode = sMode1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound1 = 0;
            InitializeNonKey011( ) ;
            sMode1 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode1;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey011( ) ;
         if ( RcdFound1 == 0 )
         {
         }
         else
         {
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound1 = 0;
         /* Using cursor T00016 */
         pr_default.execute(4, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(4) != 101) )
         {
            while ( (pr_default.getStatus(4) != 101) && ( ( GuidUtil.Compare(T00016_A1PersonaID[0], A1PersonaID, 1) < 0 ) ) )
            {
               pr_default.readNext(4);
            }
            if ( (pr_default.getStatus(4) != 101) && ( ( GuidUtil.Compare(T00016_A1PersonaID[0], A1PersonaID, 1) > 0 ) ) )
            {
               A1PersonaID = (Guid)((Guid)(T00016_A1PersonaID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
               RcdFound1 = 1;
            }
         }
         pr_default.close(4);
      }

      protected void move_previous( )
      {
         RcdFound1 = 0;
         /* Using cursor T00017 */
         pr_default.execute(5, new Object[] {A1PersonaID});
         if ( (pr_default.getStatus(5) != 101) )
         {
            while ( (pr_default.getStatus(5) != 101) && ( ( GuidUtil.Compare(T00017_A1PersonaID[0], A1PersonaID, 1) > 0 ) ) )
            {
               pr_default.readNext(5);
            }
            if ( (pr_default.getStatus(5) != 101) && ( ( GuidUtil.Compare(T00017_A1PersonaID[0], A1PersonaID, 1) < 0 ) ) )
            {
               A1PersonaID = (Guid)((Guid)(T00017_A1PersonaID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
               RcdFound1 = 1;
            }
         }
         pr_default.close(5);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey011( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtPersonaID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            Insert011( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound1 == 1 )
            {
               if ( A1PersonaID != Z1PersonaID )
               {
                  A1PersonaID = (Guid)(Z1PersonaID);
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "PERSONAID");
                  AnyError = 1;
                  GX_FocusControl = edtPersonaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtPersonaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  /* Update record */
                  Update011( ) ;
                  GX_FocusControl = edtPersonaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A1PersonaID != Z1PersonaID )
               {
                  /* Insert record */
                  GX_FocusControl = edtPersonaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  Insert011( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "PERSONAID");
                     AnyError = 1;
                     GX_FocusControl = edtPersonaID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     /* Insert record */
                     GX_FocusControl = edtPersonaID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                     Insert011( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            if ( ( AnyError == 0 ) && ( StringUtil.Len( sPrefix) == 0 ) )
            {
               context.nUserReturn = 1;
            }
         }
      }

      protected void btn_delete( )
      {
         if ( A1PersonaID != Z1PersonaID )
         {
            A1PersonaID = (Guid)(Z1PersonaID);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "PERSONAID");
            AnyError = 1;
            GX_FocusControl = edtPersonaID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtPersonaID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
         }
      }

      protected void CheckOptimisticConcurrency011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00012 */
            pr_default.execute(0, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Persona"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z2PersonaPNombre, T00012_A2PersonaPNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z3PersonaSNombre, T00012_A3PersonaSNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z4PersonaPApellido, T00012_A4PersonaPApellido[0]) != 0 ) || ( StringUtil.StrCmp(Z5PersonaSApellido, T00012_A5PersonaSApellido[0]) != 0 ) || ( StringUtil.StrCmp(Z6PersonaSexo, T00012_A6PersonaSexo[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( StringUtil.StrCmp(Z7PersonaDPI, T00012_A7PersonaDPI[0]) != 0 ) || ( Z8PersonaTipo != T00012_A8PersonaTipo[0] ) || ( Z9PersonaFechaNacimiento != T00012_A9PersonaFechaNacimiento[0] ) || ( Z10PersonaPacienteOrigenSordera != T00012_A10PersonaPacienteOrigenSordera[0] ) || ( StringUtil.StrCmp(Z11PersonaEMail, T00012_A11PersonaEMail[0]) != 0 ) )
            {
               if ( StringUtil.StrCmp(Z2PersonaPNombre, T00012_A2PersonaPNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaPNombre");
                  GXUtil.WriteLogRaw("Old: ",Z2PersonaPNombre);
                  GXUtil.WriteLogRaw("Current: ",T00012_A2PersonaPNombre[0]);
               }
               if ( StringUtil.StrCmp(Z3PersonaSNombre, T00012_A3PersonaSNombre[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaSNombre");
                  GXUtil.WriteLogRaw("Old: ",Z3PersonaSNombre);
                  GXUtil.WriteLogRaw("Current: ",T00012_A3PersonaSNombre[0]);
               }
               if ( StringUtil.StrCmp(Z4PersonaPApellido, T00012_A4PersonaPApellido[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaPApellido");
                  GXUtil.WriteLogRaw("Old: ",Z4PersonaPApellido);
                  GXUtil.WriteLogRaw("Current: ",T00012_A4PersonaPApellido[0]);
               }
               if ( StringUtil.StrCmp(Z5PersonaSApellido, T00012_A5PersonaSApellido[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaSApellido");
                  GXUtil.WriteLogRaw("Old: ",Z5PersonaSApellido);
                  GXUtil.WriteLogRaw("Current: ",T00012_A5PersonaSApellido[0]);
               }
               if ( StringUtil.StrCmp(Z6PersonaSexo, T00012_A6PersonaSexo[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaSexo");
                  GXUtil.WriteLogRaw("Old: ",Z6PersonaSexo);
                  GXUtil.WriteLogRaw("Current: ",T00012_A6PersonaSexo[0]);
               }
               if ( StringUtil.StrCmp(Z7PersonaDPI, T00012_A7PersonaDPI[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaDPI");
                  GXUtil.WriteLogRaw("Old: ",Z7PersonaDPI);
                  GXUtil.WriteLogRaw("Current: ",T00012_A7PersonaDPI[0]);
               }
               if ( Z8PersonaTipo != T00012_A8PersonaTipo[0] )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaTipo");
                  GXUtil.WriteLogRaw("Old: ",Z8PersonaTipo);
                  GXUtil.WriteLogRaw("Current: ",T00012_A8PersonaTipo[0]);
               }
               if ( Z9PersonaFechaNacimiento != T00012_A9PersonaFechaNacimiento[0] )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaFechaNacimiento");
                  GXUtil.WriteLogRaw("Old: ",Z9PersonaFechaNacimiento);
                  GXUtil.WriteLogRaw("Current: ",T00012_A9PersonaFechaNacimiento[0]);
               }
               if ( Z10PersonaPacienteOrigenSordera != T00012_A10PersonaPacienteOrigenSordera[0] )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaPacienteOrigenSordera");
                  GXUtil.WriteLogRaw("Old: ",Z10PersonaPacienteOrigenSordera);
                  GXUtil.WriteLogRaw("Current: ",T00012_A10PersonaPacienteOrigenSordera[0]);
               }
               if ( StringUtil.StrCmp(Z11PersonaEMail, T00012_A11PersonaEMail[0]) != 0 )
               {
                  GXUtil.WriteLog("persona:[seudo value changed for attri]"+"PersonaEMail");
                  GXUtil.WriteLogRaw("Old: ",Z11PersonaEMail);
                  GXUtil.WriteLogRaw("Current: ",T00012_A11PersonaEMail[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Persona"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert011( )
      {
         if ( ! IsAuthorized("persona_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM011( 0) ;
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00018 */
                     pr_default.execute(6, new Object[] {A2PersonaPNombre, n3PersonaSNombre, A3PersonaSNombre, A4PersonaPApellido, n5PersonaSApellido, A5PersonaSApellido, n6PersonaSexo, A6PersonaSexo, n7PersonaDPI, A7PersonaDPI, A8PersonaTipo, n9PersonaFechaNacimiento, A9PersonaFechaNacimiento, n10PersonaPacienteOrigenSordera, A10PersonaPacienteOrigenSordera, n11PersonaEMail, A11PersonaEMail, A1PersonaID});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Persona") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        this_Class = "K2BMessage";
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( ( AnyError == 0 ) && ( StringUtil.Len( sPrefix) == 0 ) )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load011( ) ;
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void Update011( )
      {
         if ( ! IsAuthorized("persona_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable011( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm011( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate011( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T00019 */
                     pr_default.execute(7, new Object[] {A2PersonaPNombre, n3PersonaSNombre, A3PersonaSNombre, A4PersonaPApellido, n5PersonaSApellido, A5PersonaSApellido, n6PersonaSexo, A6PersonaSexo, n7PersonaDPI, A7PersonaDPI, A8PersonaTipo, n9PersonaFechaNacimiento, A9PersonaFechaNacimiento, n10PersonaPacienteOrigenSordera, A10PersonaPacienteOrigenSordera, n11PersonaEMail, A11PersonaEMail, A1PersonaID});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("Persona") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Persona"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate011( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        this_Class = "K2BMessage";
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                           {
                              if ( ( AnyError == 0 ) && ( StringUtil.Len( sPrefix) == 0 ) )
                              {
                                 context.nUserReturn = 1;
                              }
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel011( ) ;
         }
         CloseExtendedTableCursors011( ) ;
      }

      protected void DeferredUpdate011( )
      {
      }

      protected void delete( )
      {
         if ( ! IsAuthorized("persona_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate011( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency011( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls011( ) ;
            AfterConfirm011( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete011( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000110 */
                  pr_default.execute(8, new Object[] {A1PersonaID});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("Persona") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
                        {
                           if ( ( AnyError == 0 ) && ( StringUtil.Len( sPrefix) == 0 ) )
                           {
                              context.nUserReturn = 1;
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode1 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         EndLevel011( ) ;
         Gx_mode = sMode1;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls011( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            AV24Pgmname = "Persona";
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV24Pgmname", AV24Pgmname);
         }
         if ( AnyError == 0 )
         {
            /* Using cursor T000111 */
            pr_default.execute(9, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(9) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Emergencia Bitacora"+" ("+"Emergencia Coach ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(9);
            /* Using cursor T000112 */
            pr_default.execute(10, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(10) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Emergencia Bitacora"+" ("+"Emergencia Paciente ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(10);
            /* Using cursor T000113 */
            pr_default.execute(11, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(11) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Obstaculo"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor T000114 */
            pr_default.execute(12, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(12) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Detalle"+" ("+"Recorrido Detalle Coach ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(12);
            /* Using cursor T000115 */
            pr_default.execute(13, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(13) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Detalle"+" ("+"Recorrido Paciente ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(13);
            /* Using cursor T000116 */
            pr_default.execute(14, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(14) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Coach Paciente"+" ("+"Coach Paciente Paciente ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(14);
            /* Using cursor T000117 */
            pr_default.execute(15, new Object[] {A1PersonaID});
            if ( (pr_default.getStatus(15) != 101) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_del", new   object[]  {"Coach Paciente"+" ("+"Coach Paciente Coach ST"+")"}), "CannotDeleteReferencedRecord", 1, "");
               AnyError = 1;
            }
            pr_default.close(15);
         }
      }

      protected void EndLevel011( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete011( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_gam.commit( "Persona");
            pr_default.commit( "Persona");
            if ( AnyError == 0 )
            {
               ConfirmValues010( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_gam.rollback( "Persona");
            pr_default.rollback( "Persona");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart011( )
      {
         /* Scan By routine */
         /* Using cursor T000118 */
         pr_default.execute(16);
         RcdFound1 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound1 = 1;
            A1PersonaID = (Guid)((Guid)(T000118_A1PersonaID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext011( )
      {
         /* Scan next routine */
         pr_default.readNext(16);
         RcdFound1 = 0;
         if ( (pr_default.getStatus(16) != 101) )
         {
            RcdFound1 = 1;
            A1PersonaID = (Guid)((Guid)(T000118_A1PersonaID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
         }
      }

      protected void ScanEnd011( )
      {
         pr_default.close(16);
      }

      protected void AfterConfirm011( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert011( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate011( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete011( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete011( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate011( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes011( )
      {
         edtPersonaID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaID_Enabled), 5, 0)), true);
         edtPersonaPNombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaPNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPNombre_Enabled), 5, 0)), true);
         edtPersonaSNombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaSNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSNombre_Enabled), 5, 0)), true);
         edtPersonaPApellido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaPApellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaPApellido_Enabled), 5, 0)), true);
         edtPersonaSApellido_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaSApellido_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSApellido_Enabled), 5, 0)), true);
         edtPersonaSexo_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaSexo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaSexo_Enabled), 5, 0)), true);
         edtPersonaDPI_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaDPI_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaDPI_Enabled), 5, 0)), true);
         cmbPersonaTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaTipo.Enabled), 5, 0)), true);
         edtPersonaFechaNacimiento_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaFechaNacimiento_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaFechaNacimiento_Enabled), 5, 0)), true);
         cmbPersonaPacienteOrigenSordera.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, cmbPersonaPacienteOrigenSordera_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbPersonaPacienteOrigenSordera.Enabled), 5, 0)), true);
         edtPersonaEMail_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, edtPersonaEMail_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtPersonaEMail_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes011( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues010( )
      {
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( Form.Caption) ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?2018111816264423", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body ") ;
            bodyStyle = "";
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("persona.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode(AV7PersonaID.ToString())+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Class", "form-horizontal Form", true);
         }
         else
         {
            bool toggleHtmlOutput = isOutputEnabled( ) ;
            if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
            }
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            if ( toggleHtmlOutput )
            {
               if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableOutput();
                  }
               }
            }
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         forbiddenHiddens = sPrefix + "hsh" + "Persona";
         forbiddenHiddens = forbiddenHiddens + StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!"));
         GxWebStd.gx_hidden_field( context, sPrefix+"hsh", GXUtil.GetEncryptedHash( forbiddenHiddens, GXKey));
         GXUtil.WriteLog("persona:[SendSecurityCheck value for]"+"Gx_mode:"+StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, sPrefix+"Z1PersonaID", Z1PersonaID.ToString());
         GxWebStd.gx_hidden_field( context, sPrefix+"Z2PersonaPNombre", Z2PersonaPNombre);
         GxWebStd.gx_hidden_field( context, sPrefix+"Z3PersonaSNombre", Z3PersonaSNombre);
         GxWebStd.gx_hidden_field( context, sPrefix+"Z4PersonaPApellido", Z4PersonaPApellido);
         GxWebStd.gx_hidden_field( context, sPrefix+"Z5PersonaSApellido", Z5PersonaSApellido);
         GxWebStd.gx_hidden_field( context, sPrefix+"Z6PersonaSexo", StringUtil.RTrim( Z6PersonaSexo));
         GxWebStd.gx_hidden_field( context, sPrefix+"Z7PersonaDPI", Z7PersonaDPI);
         GxWebStd.gx_hidden_field( context, sPrefix+"Z8PersonaTipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z8PersonaTipo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"Z9PersonaFechaNacimiento", context.localUtil.DToC( Z9PersonaFechaNacimiento, 0, "/"));
         GxWebStd.gx_hidden_field( context, sPrefix+"Z10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z10PersonaPacienteOrigenSordera), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"Z11PersonaEMail", Z11PersonaEMail);
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOGx_mode", StringUtil.RTrim( wcpOGx_mode));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV7PersonaID", wcpOAV7PersonaID.ToString());
         GxWebStd.gx_hidden_field( context, sPrefix+"IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPERSONAID", AV7PersonaID.ToString());
         GxWebStd.gx_hidden_field( context, sPrefix+"vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV24Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, sPrefix+"K2BCONTROLBEAUTIFY1_Objectcall", StringUtil.RTrim( K2bcontrolbeautify1_Objectcall));
         GxWebStd.gx_hidden_field( context, sPrefix+"K2BCONTROLBEAUTIFY1_Enabled", StringUtil.BoolToStr( K2bcontrolbeautify1_Enabled));
      }

      protected void RenderHtmlCloseForm011( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && ( context.isAjaxRequest( ) || context.isSpaRequest( ) ) )
         {
            context.AddJavascriptSource("persona.js", "?2018111816264429", false);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            SendWebComponentState();
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "Persona" ;
      }

      public override String GetPgmdesc( )
      {
         return "Persona" ;
      }

      protected void InitializeNonKey011( )
      {
         A2PersonaPNombre = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A2PersonaPNombre", A2PersonaPNombre);
         A3PersonaSNombre = "";
         n3PersonaSNombre = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A3PersonaSNombre", A3PersonaSNombre);
         n3PersonaSNombre = (String.IsNullOrEmpty(StringUtil.RTrim( A3PersonaSNombre)) ? true : false);
         A4PersonaPApellido = "";
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A4PersonaPApellido", A4PersonaPApellido);
         A5PersonaSApellido = "";
         n5PersonaSApellido = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A5PersonaSApellido", A5PersonaSApellido);
         n5PersonaSApellido = (String.IsNullOrEmpty(StringUtil.RTrim( A5PersonaSApellido)) ? true : false);
         A6PersonaSexo = "";
         n6PersonaSexo = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A6PersonaSexo", A6PersonaSexo);
         n6PersonaSexo = (String.IsNullOrEmpty(StringUtil.RTrim( A6PersonaSexo)) ? true : false);
         A7PersonaDPI = "";
         n7PersonaDPI = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A7PersonaDPI", A7PersonaDPI);
         n7PersonaDPI = (String.IsNullOrEmpty(StringUtil.RTrim( A7PersonaDPI)) ? true : false);
         A8PersonaTipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A8PersonaTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A8PersonaTipo), 4, 0)));
         A9PersonaFechaNacimiento = DateTime.MinValue;
         n9PersonaFechaNacimiento = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A9PersonaFechaNacimiento", context.localUtil.Format(A9PersonaFechaNacimiento, "99/99/99"));
         n9PersonaFechaNacimiento = ((DateTime.MinValue==A9PersonaFechaNacimiento) ? true : false);
         A10PersonaPacienteOrigenSordera = 0;
         n10PersonaPacienteOrigenSordera = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A10PersonaPacienteOrigenSordera", StringUtil.LTrim( StringUtil.Str( (decimal)(A10PersonaPacienteOrigenSordera), 4, 0)));
         n10PersonaPacienteOrigenSordera = ((0==A10PersonaPacienteOrigenSordera) ? true : false);
         A11PersonaEMail = "";
         n11PersonaEMail = false;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A11PersonaEMail", A11PersonaEMail);
         n11PersonaEMail = (String.IsNullOrEmpty(StringUtil.RTrim( A11PersonaEMail)) ? true : false);
         Z2PersonaPNombre = "";
         Z3PersonaSNombre = "";
         Z4PersonaPApellido = "";
         Z5PersonaSApellido = "";
         Z6PersonaSexo = "";
         Z7PersonaDPI = "";
         Z8PersonaTipo = 0;
         Z9PersonaFechaNacimiento = DateTime.MinValue;
         Z10PersonaPacienteOrigenSordera = 0;
         Z11PersonaEMail = "";
      }

      protected void InitAll011( )
      {
         A1PersonaID = (Guid)(Guid.NewGuid( ));
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "A1PersonaID", A1PersonaID.ToString());
         InitializeNonKey011( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlGx_mode = (String)((String)getParm(obj,0));
         sCtrlAV7PersonaID = (String)((String)getParm(obj,1));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         if ( StringUtil.Len( sPrefix) != 0 )
         {
            initialize_properties( ) ;
         }
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         if ( nDoneStart == 0 )
         {
            createObjects();
            initialize();
         }
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "persona", GetJustCreated( ));
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITENV( ) ;
            INITTRN( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            Gx_mode = (String)getParm(obj,2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
            AV7PersonaID = (Guid)((Guid)getParm(obj,3));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7PersonaID", AV7PersonaID.ToString());
         }
         wcpOGx_mode = cgiGet( sPrefix+"wcpOGx_mode");
         wcpOAV7PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( sPrefix+"wcpOAV7PersonaID")));
         if ( ! GetJustCreated( ) && ( ( StringUtil.StrCmp(Gx_mode, wcpOGx_mode) != 0 ) || ( AV7PersonaID != wcpOAV7PersonaID ) ) )
         {
            setjustcreated();
         }
         wcpOGx_mode = Gx_mode;
         wcpOAV7PersonaID = (Guid)(AV7PersonaID);
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlGx_mode = cgiGet( sPrefix+"Gx_mode_CTRL");
         if ( StringUtil.Len( sCtrlGx_mode) > 0 )
         {
            Gx_mode = cgiGet( sCtrlGx_mode);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = cgiGet( sPrefix+"Gx_mode_PARM");
         }
         sCtrlAV7PersonaID = cgiGet( sPrefix+"AV7PersonaID_CTRL");
         if ( StringUtil.Len( sCtrlAV7PersonaID) > 0 )
         {
            AV7PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( sCtrlAV7PersonaID)));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV7PersonaID", AV7PersonaID.ToString());
         }
         else
         {
            AV7PersonaID = (Guid)(StringUtil.StrToGuid( cgiGet( sPrefix+"AV7PersonaID_PARM")));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITENV( ) ;
         INITTRN( ) ;
         nDraw = 0;
         sEvt = sCompEvt;
         if ( isFullAjaxMode( ) )
         {
            UserMain( ) ;
         }
         else
         {
            WCParametersGet( ) ;
         }
         Process( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         UserMain( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"Gx_mode_PARM", StringUtil.RTrim( Gx_mode));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlGx_mode)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"Gx_mode_CTRL", StringUtil.RTrim( sCtrlGx_mode));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV7PersonaID_PARM", AV7PersonaID.ToString());
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV7PersonaID)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV7PersonaID_CTRL", StringUtil.RTrim( sCtrlAV7PersonaID));
         }
      }

      public override void componentdraw( )
      {
         if ( CheckCmpSecurityAccess() )
         {
            if ( nDoneStart == 0 )
            {
               WCStart( ) ;
            }
            BackMsgLst = context.GX_msglist;
            context.GX_msglist = LclMsgLst;
            WCParametersSet( ) ;
            Draw( ) ;
            SaveComponentMsgList(sPrefix);
            context.GX_msglist = BackMsgLst;
         }
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("K2BControlBeautify/montrezorro-bootstrap-checkbox/css/bootstrap-checkbox.css", "");
         AddStyleSheetFile("K2BControlBeautify/silviomoreto-bootstrap-select/dist/css/bootstrap-select.css", "");
         AddStyleSheetFile("K2BControlBeautify/toastr-master/toastr.min.css", "");
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?2018111816264435", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("persona.js", "?2018111816264436", false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         divK2beserrviewercell_Internalname = sPrefix+"K2BESERRVIEWERCELL";
         edtPersonaID_Internalname = sPrefix+"PERSONAID";
         edtPersonaPNombre_Internalname = sPrefix+"PERSONAPNOMBRE";
         edtPersonaSNombre_Internalname = sPrefix+"PERSONASNOMBRE";
         edtPersonaPApellido_Internalname = sPrefix+"PERSONAPAPELLIDO";
         edtPersonaSApellido_Internalname = sPrefix+"PERSONASAPELLIDO";
         edtPersonaSexo_Internalname = sPrefix+"PERSONASEXO";
         edtPersonaDPI_Internalname = sPrefix+"PERSONADPI";
         cmbPersonaTipo_Internalname = sPrefix+"PERSONATIPO";
         edtPersonaFechaNacimiento_Internalname = sPrefix+"PERSONAFECHANACIMIENTO";
         cmbPersonaPacienteOrigenSordera_Internalname = sPrefix+"PERSONAPACIENTEORIGENSORDERA";
         edtPersonaEMail_Internalname = sPrefix+"PERSONAEMAIL";
         divTableattributesinformsection1_Internalname = sPrefix+"TABLEATTRIBUTESINFORMSECTION1";
         divK2btrnformmaintablecell_Internalname = sPrefix+"K2BTRNFORMMAINTABLECELL";
         divK2babstracttabledataareacontainer_Internalname = sPrefix+"K2BABSTRACTTABLEDATAAREACONTAINER";
         divK2besdataareacontainercell_Internalname = sPrefix+"K2BESDATAAREACONTAINERCELL";
         bttEnter_Internalname = sPrefix+"ENTER";
         bttCancel_Internalname = sPrefix+"CANCEL";
         tblActionscontainerbuttons_Internalname = sPrefix+"ACTIONSCONTAINERBUTTONS";
         divK2besactioncontainercell_Internalname = sPrefix+"K2BESACTIONCONTAINERCELL";
         K2bcontrolbeautify1_Internalname = sPrefix+"K2BCONTROLBEAUTIFY1";
         divK2bescontrolbeaufitycell_Internalname = sPrefix+"K2BESCONTROLBEAUFITYCELL";
         divK2besmaintable_Internalname = sPrefix+"K2BESMAINTABLE";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         Form.Caption = "Persona";
         bttCancel_Visible = 1;
         bttEnter_Tooltiptext = "";
         bttEnter_Caption = "Confirmar";
         bttEnter_Enabled = 1;
         bttEnter_Visible = 1;
         edtPersonaEMail_Jsonclick = "";
         edtPersonaEMail_Enabled = 1;
         cmbPersonaPacienteOrigenSordera_Jsonclick = "";
         cmbPersonaPacienteOrigenSordera.Enabled = 1;
         edtPersonaFechaNacimiento_Jsonclick = "";
         edtPersonaFechaNacimiento_Enabled = 1;
         cmbPersonaTipo_Jsonclick = "";
         cmbPersonaTipo.Enabled = 1;
         edtPersonaDPI_Jsonclick = "";
         edtPersonaDPI_Enabled = 1;
         edtPersonaSexo_Jsonclick = "";
         edtPersonaSexo_Enabled = 1;
         edtPersonaSApellido_Jsonclick = "";
         edtPersonaSApellido_Enabled = 1;
         edtPersonaPApellido_Jsonclick = "";
         edtPersonaPApellido_Enabled = 1;
         edtPersonaSNombre_Jsonclick = "";
         edtPersonaSNombre_Enabled = 1;
         edtPersonaPNombre_Jsonclick = "";
         edtPersonaPNombre_Class = "Attribute_Trn Attribute_Required";
         edtPersonaPNombre_Enabled = 1;
         edtPersonaID_Jsonclick = "";
         edtPersonaID_Enabled = 1;
         this_Class = "ErrorViewer";
         context.GX_msglist.DisplayMode = 1;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'componentprocess',iparms:[{postForm:true},{sPrefix:true},{sSFPrefix:true},{sCompEvt:true},{av:'Gx_mode',fld:'vMODE',pic:'@!',nv:''},{av:'AV7PersonaID',fld:'vPERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',nv:''}],oparms:[]}");
         setEventMetadata("AFTER TRN","{handler:'E12012',iparms:[{av:'AV8TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'Gx_mode',fld:'vMODE',pic:'@!',nv:''},{av:'A2PersonaPNombre',fld:'PERSONAPNOMBRE',pic:'',nv:''},{av:'AV10Navigation',fld:'vNAVIGATION',pic:'',nv:null}],oparms:[{av:'AV10Navigation',fld:'vNAVIGATION',pic:'',nv:null},{av:'AV8TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null},{av:'A1PersonaID',fld:'PERSONAID',pic:'',nv:'00000000-0000-0000-0000-000000000000'}]}");
         setEventMetadata("'DOCANCEL'","{handler:'E13012',iparms:[{av:'AV8TrnContext',fld:'vTRNCONTEXT',pic:'',nv:null}],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         sPrefix = "";
         wcpOGx_mode = "";
         wcpOAV7PersonaID = (Guid)(Guid.Empty);
         Z1PersonaID = (Guid)(Guid.Empty);
         Z2PersonaPNombre = "";
         Z3PersonaSNombre = "";
         Z4PersonaPApellido = "";
         Z5PersonaSApellido = "";
         Z6PersonaSexo = "";
         Z7PersonaDPI = "";
         Z9PersonaFechaNacimiento = DateTime.MinValue;
         Z11PersonaEMail = "";
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         sXEvt = "";
         GX_FocusControl = "";
         ClassString = "";
         StyleString = "";
         A1PersonaID = (Guid)(Guid.Empty);
         TempTags = "";
         A2PersonaPNombre = "";
         A3PersonaSNombre = "";
         A4PersonaPApellido = "";
         A5PersonaSApellido = "";
         A6PersonaSexo = "";
         A7PersonaDPI = "";
         A9PersonaFechaNacimiento = DateTime.MinValue;
         A11PersonaEMail = "";
         sStyleString = "";
         bttEnter_Jsonclick = "";
         bttCancel_Jsonclick = "";
         AV24Pgmname = "";
         K2bcontrolbeautify1_Objectcall = "";
         K2bcontrolbeautify1_Class = "";
         forbiddenHiddens = "";
         hsh = "";
         sMode1 = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV16StandardActivityType = "";
         AV17UserActivityType = "";
         AV12Context = new SdtK2BContext(context);
         AV13BtnCaption = "";
         AV14BtnTooltip = "";
         AV8TrnContext = new SdtK2BTrnContext(context);
         AV20AttributeValue = new GXBaseCollection<SdtK2BAttributeValue_K2BAttributeValueItem>( context, "K2BAttributeValueItem", "PACYE2");
         AV21AttributeValueItem = new SdtK2BAttributeValue_K2BAttributeValueItem(context);
         AV22Message = new SdtMessages_Message(context);
         AV10Navigation = new SdtK2BTrnNavigation(context);
         AV19encrypt = "";
         GXt_char1 = "";
         AV11DinamicObjToLink = "";
         GXEncryptionTmp = "";
         AV15Url = "";
         T00014_A1PersonaID = new Guid[] {Guid.Empty} ;
         T00014_A2PersonaPNombre = new String[] {""} ;
         T00014_A3PersonaSNombre = new String[] {""} ;
         T00014_n3PersonaSNombre = new bool[] {false} ;
         T00014_A4PersonaPApellido = new String[] {""} ;
         T00014_A5PersonaSApellido = new String[] {""} ;
         T00014_n5PersonaSApellido = new bool[] {false} ;
         T00014_A6PersonaSexo = new String[] {""} ;
         T00014_n6PersonaSexo = new bool[] {false} ;
         T00014_A7PersonaDPI = new String[] {""} ;
         T00014_n7PersonaDPI = new bool[] {false} ;
         T00014_A8PersonaTipo = new short[1] ;
         T00014_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         T00014_n9PersonaFechaNacimiento = new bool[] {false} ;
         T00014_A10PersonaPacienteOrigenSordera = new short[1] ;
         T00014_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         T00014_A11PersonaEMail = new String[] {""} ;
         T00014_n11PersonaEMail = new bool[] {false} ;
         T00015_A1PersonaID = new Guid[] {Guid.Empty} ;
         T00013_A1PersonaID = new Guid[] {Guid.Empty} ;
         T00013_A2PersonaPNombre = new String[] {""} ;
         T00013_A3PersonaSNombre = new String[] {""} ;
         T00013_n3PersonaSNombre = new bool[] {false} ;
         T00013_A4PersonaPApellido = new String[] {""} ;
         T00013_A5PersonaSApellido = new String[] {""} ;
         T00013_n5PersonaSApellido = new bool[] {false} ;
         T00013_A6PersonaSexo = new String[] {""} ;
         T00013_n6PersonaSexo = new bool[] {false} ;
         T00013_A7PersonaDPI = new String[] {""} ;
         T00013_n7PersonaDPI = new bool[] {false} ;
         T00013_A8PersonaTipo = new short[1] ;
         T00013_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         T00013_n9PersonaFechaNacimiento = new bool[] {false} ;
         T00013_A10PersonaPacienteOrigenSordera = new short[1] ;
         T00013_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         T00013_A11PersonaEMail = new String[] {""} ;
         T00013_n11PersonaEMail = new bool[] {false} ;
         T00016_A1PersonaID = new Guid[] {Guid.Empty} ;
         T00017_A1PersonaID = new Guid[] {Guid.Empty} ;
         T00012_A1PersonaID = new Guid[] {Guid.Empty} ;
         T00012_A2PersonaPNombre = new String[] {""} ;
         T00012_A3PersonaSNombre = new String[] {""} ;
         T00012_n3PersonaSNombre = new bool[] {false} ;
         T00012_A4PersonaPApellido = new String[] {""} ;
         T00012_A5PersonaSApellido = new String[] {""} ;
         T00012_n5PersonaSApellido = new bool[] {false} ;
         T00012_A6PersonaSexo = new String[] {""} ;
         T00012_n6PersonaSexo = new bool[] {false} ;
         T00012_A7PersonaDPI = new String[] {""} ;
         T00012_n7PersonaDPI = new bool[] {false} ;
         T00012_A8PersonaTipo = new short[1] ;
         T00012_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         T00012_n9PersonaFechaNacimiento = new bool[] {false} ;
         T00012_A10PersonaPacienteOrigenSordera = new short[1] ;
         T00012_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         T00012_A11PersonaEMail = new String[] {""} ;
         T00012_n11PersonaEMail = new bool[] {false} ;
         T000111_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T000112_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T000113_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         T000114_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T000114_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         T000115_A23RecorridoID = new Guid[] {Guid.Empty} ;
         T000115_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         T000116_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T000116_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         T000117_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         T000117_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         T000118_A1PersonaID = new Guid[] {Guid.Empty} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         sCtrlGx_mode = "";
         sCtrlAV7PersonaID = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.persona__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.persona__default(),
            new Object[][] {
                new Object[] {
               T00012_A1PersonaID, T00012_A2PersonaPNombre, T00012_A3PersonaSNombre, T00012_n3PersonaSNombre, T00012_A4PersonaPApellido, T00012_A5PersonaSApellido, T00012_n5PersonaSApellido, T00012_A6PersonaSexo, T00012_n6PersonaSexo, T00012_A7PersonaDPI,
               T00012_n7PersonaDPI, T00012_A8PersonaTipo, T00012_A9PersonaFechaNacimiento, T00012_n9PersonaFechaNacimiento, T00012_A10PersonaPacienteOrigenSordera, T00012_n10PersonaPacienteOrigenSordera, T00012_A11PersonaEMail, T00012_n11PersonaEMail
               }
               , new Object[] {
               T00013_A1PersonaID, T00013_A2PersonaPNombre, T00013_A3PersonaSNombre, T00013_n3PersonaSNombre, T00013_A4PersonaPApellido, T00013_A5PersonaSApellido, T00013_n5PersonaSApellido, T00013_A6PersonaSexo, T00013_n6PersonaSexo, T00013_A7PersonaDPI,
               T00013_n7PersonaDPI, T00013_A8PersonaTipo, T00013_A9PersonaFechaNacimiento, T00013_n9PersonaFechaNacimiento, T00013_A10PersonaPacienteOrigenSordera, T00013_n10PersonaPacienteOrigenSordera, T00013_A11PersonaEMail, T00013_n11PersonaEMail
               }
               , new Object[] {
               T00014_A1PersonaID, T00014_A2PersonaPNombre, T00014_A3PersonaSNombre, T00014_n3PersonaSNombre, T00014_A4PersonaPApellido, T00014_A5PersonaSApellido, T00014_n5PersonaSApellido, T00014_A6PersonaSexo, T00014_n6PersonaSexo, T00014_A7PersonaDPI,
               T00014_n7PersonaDPI, T00014_A8PersonaTipo, T00014_A9PersonaFechaNacimiento, T00014_n9PersonaFechaNacimiento, T00014_A10PersonaPacienteOrigenSordera, T00014_n10PersonaPacienteOrigenSordera, T00014_A11PersonaEMail, T00014_n11PersonaEMail
               }
               , new Object[] {
               T00015_A1PersonaID
               }
               , new Object[] {
               T00016_A1PersonaID
               }
               , new Object[] {
               T00017_A1PersonaID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000111_A43EmergenciaID
               }
               , new Object[] {
               T000112_A43EmergenciaID
               }
               , new Object[] {
               T000113_A35ObstaculoID
               }
               , new Object[] {
               T000114_A23RecorridoID, T000114_A32RecorridoDetalleID
               }
               , new Object[] {
               T000115_A23RecorridoID, T000115_A32RecorridoDetalleID
               }
               , new Object[] {
               T000116_A12CoachPaciente_Coach, T000116_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T000117_A12CoachPaciente_Coach, T000117_A13CoachPaciente_Paciente
               }
               , new Object[] {
               T000118_A1PersonaID
               }
            }
         );
         Z1PersonaID = (Guid)(Guid.NewGuid( ));
         A1PersonaID = (Guid)(Guid.NewGuid( ));
         AV24Pgmname = "Persona";
      }

      private short Z8PersonaTipo ;
      private short Z10PersonaPacienteOrigenSordera ;
      private short GxWebError ;
      private short nDynComponent ;
      private short gxcookieaux ;
      private short A8PersonaTipo ;
      private short A10PersonaPacienteOrigenSordera ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short nDraw ;
      private short nDoneStart ;
      private short Gx_BScreen ;
      private short RcdFound1 ;
      private short GX_JID ;
      private int trnEnded ;
      private int edtPersonaID_Enabled ;
      private int edtPersonaPNombre_Enabled ;
      private int edtPersonaSNombre_Enabled ;
      private int edtPersonaPApellido_Enabled ;
      private int edtPersonaSApellido_Enabled ;
      private int edtPersonaSexo_Enabled ;
      private int edtPersonaDPI_Enabled ;
      private int edtPersonaFechaNacimiento_Enabled ;
      private int edtPersonaEMail_Enabled ;
      private int bttEnter_Visible ;
      private int bttEnter_Enabled ;
      private int bttCancel_Visible ;
      private int idxLst ;
      private String sPrefix ;
      private String wcpOGx_mode ;
      private String Z6PersonaSexo ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String Gx_mode ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String sXEvt ;
      private String GX_FocusControl ;
      private String edtPersonaID_Internalname ;
      private String cmbPersonaTipo_Internalname ;
      private String cmbPersonaPacienteOrigenSordera_Internalname ;
      private String divK2besmaintable_Internalname ;
      private String divK2beserrviewercell_Internalname ;
      private String ClassString ;
      private String this_Class ;
      private String StyleString ;
      private String divK2besdataareacontainercell_Internalname ;
      private String divK2babstracttabledataareacontainer_Internalname ;
      private String divK2btrnformmaintablecell_Internalname ;
      private String divTableattributesinformsection1_Internalname ;
      private String TempTags ;
      private String edtPersonaID_Jsonclick ;
      private String edtPersonaPNombre_Internalname ;
      private String edtPersonaPNombre_Jsonclick ;
      private String edtPersonaPNombre_Class ;
      private String edtPersonaSNombre_Internalname ;
      private String edtPersonaSNombre_Jsonclick ;
      private String edtPersonaPApellido_Internalname ;
      private String edtPersonaPApellido_Jsonclick ;
      private String edtPersonaSApellido_Internalname ;
      private String edtPersonaSApellido_Jsonclick ;
      private String edtPersonaSexo_Internalname ;
      private String A6PersonaSexo ;
      private String edtPersonaSexo_Jsonclick ;
      private String edtPersonaDPI_Internalname ;
      private String edtPersonaDPI_Jsonclick ;
      private String cmbPersonaTipo_Jsonclick ;
      private String edtPersonaFechaNacimiento_Internalname ;
      private String edtPersonaFechaNacimiento_Jsonclick ;
      private String cmbPersonaPacienteOrigenSordera_Jsonclick ;
      private String edtPersonaEMail_Internalname ;
      private String edtPersonaEMail_Jsonclick ;
      private String divK2besactioncontainercell_Internalname ;
      private String divK2bescontrolbeaufitycell_Internalname ;
      private String sStyleString ;
      private String tblActionscontainerbuttons_Internalname ;
      private String bttEnter_Internalname ;
      private String bttEnter_Caption ;
      private String bttEnter_Jsonclick ;
      private String bttEnter_Tooltiptext ;
      private String bttCancel_Internalname ;
      private String bttCancel_Jsonclick ;
      private String AV24Pgmname ;
      private String K2bcontrolbeautify1_Objectcall ;
      private String K2bcontrolbeautify1_Class ;
      private String forbiddenHiddens ;
      private String hsh ;
      private String sMode1 ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String AV16StandardActivityType ;
      private String AV13BtnCaption ;
      private String AV14BtnTooltip ;
      private String AV19encrypt ;
      private String GXt_char1 ;
      private String GXEncryptionTmp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String sCtrlGx_mode ;
      private String sCtrlAV7PersonaID ;
      private String K2bcontrolbeautify1_Internalname ;
      private DateTime Z9PersonaFechaNacimiento ;
      private DateTime A9PersonaFechaNacimiento ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool n10PersonaPacienteOrigenSordera ;
      private bool wbErr ;
      private bool n3PersonaSNombre ;
      private bool n5PersonaSApellido ;
      private bool n6PersonaSexo ;
      private bool n7PersonaDPI ;
      private bool n9PersonaFechaNacimiento ;
      private bool n11PersonaEMail ;
      private bool K2bcontrolbeautify1_Enabled ;
      private bool K2bcontrolbeautify1_Visible ;
      private bool AV18IsAuthorized ;
      private bool returnInSub ;
      private bool Gx_longc ;
      private String Z2PersonaPNombre ;
      private String Z3PersonaSNombre ;
      private String Z4PersonaPApellido ;
      private String Z5PersonaSApellido ;
      private String Z7PersonaDPI ;
      private String Z11PersonaEMail ;
      private String A2PersonaPNombre ;
      private String A3PersonaSNombre ;
      private String A4PersonaPApellido ;
      private String A5PersonaSApellido ;
      private String A7PersonaDPI ;
      private String A11PersonaEMail ;
      private String AV17UserActivityType ;
      private String AV11DinamicObjToLink ;
      private String AV15Url ;
      private Guid wcpOAV7PersonaID ;
      private Guid Z1PersonaID ;
      private Guid AV7PersonaID ;
      private Guid A1PersonaID ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbPersonaTipo ;
      private GXCombobox cmbPersonaPacienteOrigenSordera ;
      private Object[] args ;
      private IDataStoreProvider pr_default ;
      private Guid[] T00014_A1PersonaID ;
      private String[] T00014_A2PersonaPNombre ;
      private String[] T00014_A3PersonaSNombre ;
      private bool[] T00014_n3PersonaSNombre ;
      private String[] T00014_A4PersonaPApellido ;
      private String[] T00014_A5PersonaSApellido ;
      private bool[] T00014_n5PersonaSApellido ;
      private String[] T00014_A6PersonaSexo ;
      private bool[] T00014_n6PersonaSexo ;
      private String[] T00014_A7PersonaDPI ;
      private bool[] T00014_n7PersonaDPI ;
      private short[] T00014_A8PersonaTipo ;
      private DateTime[] T00014_A9PersonaFechaNacimiento ;
      private bool[] T00014_n9PersonaFechaNacimiento ;
      private short[] T00014_A10PersonaPacienteOrigenSordera ;
      private bool[] T00014_n10PersonaPacienteOrigenSordera ;
      private String[] T00014_A11PersonaEMail ;
      private bool[] T00014_n11PersonaEMail ;
      private Guid[] T00015_A1PersonaID ;
      private Guid[] T00013_A1PersonaID ;
      private String[] T00013_A2PersonaPNombre ;
      private String[] T00013_A3PersonaSNombre ;
      private bool[] T00013_n3PersonaSNombre ;
      private String[] T00013_A4PersonaPApellido ;
      private String[] T00013_A5PersonaSApellido ;
      private bool[] T00013_n5PersonaSApellido ;
      private String[] T00013_A6PersonaSexo ;
      private bool[] T00013_n6PersonaSexo ;
      private String[] T00013_A7PersonaDPI ;
      private bool[] T00013_n7PersonaDPI ;
      private short[] T00013_A8PersonaTipo ;
      private DateTime[] T00013_A9PersonaFechaNacimiento ;
      private bool[] T00013_n9PersonaFechaNacimiento ;
      private short[] T00013_A10PersonaPacienteOrigenSordera ;
      private bool[] T00013_n10PersonaPacienteOrigenSordera ;
      private String[] T00013_A11PersonaEMail ;
      private bool[] T00013_n11PersonaEMail ;
      private Guid[] T00016_A1PersonaID ;
      private Guid[] T00017_A1PersonaID ;
      private Guid[] T00012_A1PersonaID ;
      private String[] T00012_A2PersonaPNombre ;
      private String[] T00012_A3PersonaSNombre ;
      private bool[] T00012_n3PersonaSNombre ;
      private String[] T00012_A4PersonaPApellido ;
      private String[] T00012_A5PersonaSApellido ;
      private bool[] T00012_n5PersonaSApellido ;
      private String[] T00012_A6PersonaSexo ;
      private bool[] T00012_n6PersonaSexo ;
      private String[] T00012_A7PersonaDPI ;
      private bool[] T00012_n7PersonaDPI ;
      private short[] T00012_A8PersonaTipo ;
      private DateTime[] T00012_A9PersonaFechaNacimiento ;
      private bool[] T00012_n9PersonaFechaNacimiento ;
      private short[] T00012_A10PersonaPacienteOrigenSordera ;
      private bool[] T00012_n10PersonaPacienteOrigenSordera ;
      private String[] T00012_A11PersonaEMail ;
      private bool[] T00012_n11PersonaEMail ;
      private Guid[] T000111_A43EmergenciaID ;
      private Guid[] T000112_A43EmergenciaID ;
      private Guid[] T000113_A35ObstaculoID ;
      private Guid[] T000114_A23RecorridoID ;
      private Guid[] T000114_A32RecorridoDetalleID ;
      private Guid[] T000115_A23RecorridoID ;
      private Guid[] T000115_A32RecorridoDetalleID ;
      private Guid[] T000116_A12CoachPaciente_Coach ;
      private Guid[] T000116_A13CoachPaciente_Paciente ;
      private Guid[] T000117_A12CoachPaciente_Coach ;
      private Guid[] T000117_A13CoachPaciente_Paciente ;
      private IDataStoreProvider pr_gam ;
      private Guid[] T000118_A1PersonaID ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXBaseCollection<SdtK2BAttributeValue_K2BAttributeValueItem> AV20AttributeValue ;
      private SdtK2BTrnContext AV8TrnContext ;
      private SdtK2BTrnNavigation AV10Navigation ;
      private SdtK2BContext AV12Context ;
      private SdtK2BAttributeValue_K2BAttributeValueItem AV21AttributeValueItem ;
      private SdtMessages_Message AV22Message ;
   }

   public class persona__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class persona__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new UpdateCursor(def[6])
       ,new UpdateCursor(def[7])
       ,new UpdateCursor(def[8])
       ,new ForEachCursor(def[9])
       ,new ForEachCursor(def[10])
       ,new ForEachCursor(def[11])
       ,new ForEachCursor(def[12])
       ,new ForEachCursor(def[13])
       ,new ForEachCursor(def[14])
       ,new ForEachCursor(def[15])
       ,new ForEachCursor(def[16])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmT00014 ;
        prmT00014 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00015 ;
        prmT00015 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00013 ;
        prmT00013 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00016 ;
        prmT00016 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00017 ;
        prmT00017 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00012 ;
        prmT00012 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00018 ;
        prmT00018 = new Object[] {
        new Object[] {"@PersonaPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSexo",SqlDbType.Char,1,0} ,
        new Object[] {"@PersonaDPI",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaFechaNacimiento",SqlDbType.DateTime,8,0} ,
        new Object[] {"@PersonaPacienteOrigenSordera",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaEMail",SqlDbType.VarChar,100,0} ,
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00019 ;
        prmT00019 = new Object[] {
        new Object[] {"@PersonaPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaSexo",SqlDbType.Char,1,0} ,
        new Object[] {"@PersonaDPI",SqlDbType.VarChar,40,0} ,
        new Object[] {"@PersonaTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaFechaNacimiento",SqlDbType.DateTime,8,0} ,
        new Object[] {"@PersonaPacienteOrigenSordera",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@PersonaEMail",SqlDbType.VarChar,100,0} ,
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000110 ;
        prmT000110 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000111 ;
        prmT000111 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000112 ;
        prmT000112 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000113 ;
        prmT000113 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000114 ;
        prmT000114 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000115 ;
        prmT000115 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000116 ;
        prmT000116 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000117 ;
        prmT000117 = new Object[] {
        new Object[] {"@PersonaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000118 ;
        prmT000118 = new Object[] {
        } ;
        def= new CursorDef[] {
            new CursorDef("T00012", "SELECT [PersonaID], [PersonaPNombre], [PersonaSNombre], [PersonaPApellido], [PersonaSApellido], [PersonaSexo], [PersonaDPI], [PersonaTipo], [PersonaFechaNacimiento], [PersonaPacienteOrigenSordera], [PersonaEMail] FROM [Persona] WITH (UPDLOCK) WHERE [PersonaID] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00012,1,0,true,false )
           ,new CursorDef("T00013", "SELECT [PersonaID], [PersonaPNombre], [PersonaSNombre], [PersonaPApellido], [PersonaSApellido], [PersonaSexo], [PersonaDPI], [PersonaTipo], [PersonaFechaNacimiento], [PersonaPacienteOrigenSordera], [PersonaEMail] FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00013,1,0,true,false )
           ,new CursorDef("T00014", "SELECT TM1.[PersonaID], TM1.[PersonaPNombre], TM1.[PersonaSNombre], TM1.[PersonaPApellido], TM1.[PersonaSApellido], TM1.[PersonaSexo], TM1.[PersonaDPI], TM1.[PersonaTipo], TM1.[PersonaFechaNacimiento], TM1.[PersonaPacienteOrigenSordera], TM1.[PersonaEMail] FROM [Persona] TM1 WITH (NOLOCK) WHERE TM1.[PersonaID] = @PersonaID ORDER BY TM1.[PersonaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00014,100,0,true,false )
           ,new CursorDef("T00015", "SELECT [PersonaID] FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @PersonaID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00015,1,0,true,false )
           ,new CursorDef("T00016", "SELECT TOP 1 [PersonaID] FROM [Persona] WITH (NOLOCK) WHERE ( [PersonaID] > @PersonaID) ORDER BY [PersonaID]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00016,1,0,true,true )
           ,new CursorDef("T00017", "SELECT TOP 1 [PersonaID] FROM [Persona] WITH (NOLOCK) WHERE ( [PersonaID] < @PersonaID) ORDER BY [PersonaID] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00017,1,0,true,true )
           ,new CursorDef("T00018", "INSERT INTO [Persona]([PersonaPNombre], [PersonaSNombre], [PersonaPApellido], [PersonaSApellido], [PersonaSexo], [PersonaDPI], [PersonaTipo], [PersonaFechaNacimiento], [PersonaPacienteOrigenSordera], [PersonaEMail], [PersonaID]) VALUES(@PersonaPNombre, @PersonaSNombre, @PersonaPApellido, @PersonaSApellido, @PersonaSexo, @PersonaDPI, @PersonaTipo, @PersonaFechaNacimiento, @PersonaPacienteOrigenSordera, @PersonaEMail, @PersonaID)", GxErrorMask.GX_NOMASK,prmT00018)
           ,new CursorDef("T00019", "UPDATE [Persona] SET [PersonaPNombre]=@PersonaPNombre, [PersonaSNombre]=@PersonaSNombre, [PersonaPApellido]=@PersonaPApellido, [PersonaSApellido]=@PersonaSApellido, [PersonaSexo]=@PersonaSexo, [PersonaDPI]=@PersonaDPI, [PersonaTipo]=@PersonaTipo, [PersonaFechaNacimiento]=@PersonaFechaNacimiento, [PersonaPacienteOrigenSordera]=@PersonaPacienteOrigenSordera, [PersonaEMail]=@PersonaEMail  WHERE [PersonaID] = @PersonaID", GxErrorMask.GX_NOMASK,prmT00019)
           ,new CursorDef("T000110", "DELETE FROM [Persona]  WHERE [PersonaID] = @PersonaID", GxErrorMask.GX_NOMASK,prmT000110)
           ,new CursorDef("T000111", "SELECT TOP 1 [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaCoach] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000111,1,0,true,true )
           ,new CursorDef("T000112", "SELECT TOP 1 [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaPaciente] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000112,1,0,true,true )
           ,new CursorDef("T000113", "SELECT TOP 1 [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) WHERE [ObstaculoPacienteRegistra] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000113,1,0,true,true )
           ,new CursorDef("T000114", "SELECT TOP 1 [RecorridoID], [RecorridoDetalleID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [IDCoach] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000114,1,0,true,true )
           ,new CursorDef("T000115", "SELECT TOP 1 [RecorridoID], [RecorridoDetalleID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoDetallePaciente] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000115,1,0,true,true )
           ,new CursorDef("T000116", "SELECT TOP 1 [CoachPaciente_Coach], [CoachPaciente_Paciente] FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Paciente] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000116,1,0,true,true )
           ,new CursorDef("T000117", "SELECT TOP 1 [CoachPaciente_Coach], [CoachPaciente_Paciente] FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Coach] = @PersonaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT000117,1,0,true,true )
           ,new CursorDef("T000118", "SELECT [PersonaID] FROM [Persona] WITH (NOLOCK) ORDER BY [PersonaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000118,100,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((short[]) buf[11])[0] = rslt.getShort(8) ;
              ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(9);
              ((short[]) buf[14])[0] = rslt.getShort(10) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(10);
              ((String[]) buf[16])[0] = rslt.getVarchar(11) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(11);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((short[]) buf[11])[0] = rslt.getShort(8) ;
              ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(9);
              ((short[]) buf[14])[0] = rslt.getShort(10) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(10);
              ((String[]) buf[16])[0] = rslt.getVarchar(11) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(11);
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((String[]) buf[5])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getString(6, 1) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((short[]) buf[11])[0] = rslt.getShort(8) ;
              ((DateTime[]) buf[12])[0] = rslt.getGXDate(9) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(9);
              ((short[]) buf[14])[0] = rslt.getShort(10) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(10);
              ((String[]) buf[16])[0] = rslt.getVarchar(11) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(11);
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 4 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 5 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 9 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 10 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 11 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 12 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 13 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 14 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 15 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 16 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 2 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 3 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 5 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 6 :
              stmt.SetParameter(1, (String)parms[0]);
              if ( (bool)parms[1] )
              {
                 stmt.setNull( 2 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(2, (String)parms[2]);
              }
              stmt.SetParameter(3, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.Char );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(6, (String)parms[9]);
              }
              stmt.SetParameter(7, (short)parms[10]);
              if ( (bool)parms[11] )
              {
                 stmt.setNull( 8 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(8, (DateTime)parms[12]);
              }
              if ( (bool)parms[13] )
              {
                 stmt.setNull( 9 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(9, (short)parms[14]);
              }
              if ( (bool)parms[15] )
              {
                 stmt.setNull( 10 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(10, (String)parms[16]);
              }
              stmt.SetParameter(11, (Guid)parms[17]);
              return;
           case 7 :
              stmt.SetParameter(1, (String)parms[0]);
              if ( (bool)parms[1] )
              {
                 stmt.setNull( 2 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(2, (String)parms[2]);
              }
              stmt.SetParameter(3, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.Char );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(6, (String)parms[9]);
              }
              stmt.SetParameter(7, (short)parms[10]);
              if ( (bool)parms[11] )
              {
                 stmt.setNull( 8 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(8, (DateTime)parms[12]);
              }
              if ( (bool)parms[13] )
              {
                 stmt.setNull( 9 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(9, (short)parms[14]);
              }
              if ( (bool)parms[15] )
              {
                 stmt.setNull( 10 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(10, (String)parms[16]);
              }
              stmt.SetParameter(11, (Guid)parms[17]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 9 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 10 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 11 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 12 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 13 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 14 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 15 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
