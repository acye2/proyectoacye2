/*
               File: type_SdtCoachPaciente
        Description: Coach Paciente
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:45.56
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "CoachPaciente" )]
   [XmlType(TypeName =  "CoachPaciente" , Namespace = "PACYE2" )]
   [Serializable]
   public class SdtCoachPaciente : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtCoachPaciente( )
      {
      }

      public SdtCoachPaciente( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( Guid AV12CoachPaciente_Coach ,
                        Guid AV13CoachPaciente_Paciente )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(Guid)AV12CoachPaciente_Coach,(Guid)AV13CoachPaciente_Paciente});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"CoachPaciente_Coach", typeof(Guid)}, new Object[]{"CoachPaciente_Paciente", typeof(Guid)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "CoachPaciente");
         metadata.Set("BT", "CoachPaciente");
         metadata.Set("PK", "[ \"CoachPaciente_Coach\",\"CoachPaciente_Paciente\" ]");
         metadata.Set("PKAssigned", "[ \"CoachPaciente_Coach\",\"CoachPaciente_Paciente\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"PersonaID\" ],\"FKMap\":[ \"CoachPaciente_Coach-PersonaID\" ] },{ \"FK\":[ \"PersonaID\" ],\"FKMap\":[ \"CoachPaciente_Paciente-PersonaID\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GxStringCollection StateAttributes( )
      {
         GxStringCollection state = new GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Coachpaciente_coach_Z");
         state.Add("gxTpr_Coachpaciente_coachpnombre_Z");
         state.Add("gxTpr_Coachpaciente_coachpapellido_Z");
         state.Add("gxTpr_Coachpaciente_paciente_Z");
         state.Add("gxTpr_Coachpaciente_pacientepnombre_Z");
         state.Add("gxTpr_Coachpaciente_pacientepapellido_Z");
         state.Add("gxTpr_Coachpaciente_fechaasignacion_Z_Nullable");
         state.Add("gxTpr_Coachpaciente_dispositivoid_Z");
         state.Add("gxTpr_Coachpaciente_token_Z");
         state.Add("gxTpr_Coachpaciente_pacientepnombre_N");
         state.Add("gxTpr_Coachpaciente_pacientepapellido_N");
         state.Add("gxTpr_Coachpaciente_fechaasignacion_N");
         state.Add("gxTpr_Coachpaciente_dispositivoid_N");
         state.Add("gxTpr_Coachpaciente_token_N");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         SdtCoachPaciente sdt ;
         sdt = (SdtCoachPaciente)(source);
         gxTv_SdtCoachPaciente_Coachpaciente_coach = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coach ;
         gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre ;
         gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido ;
         gxTv_SdtCoachPaciente_Coachpaciente_paciente = sdt.gxTv_SdtCoachPaciente_Coachpaciente_paciente ;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre ;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido ;
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion = sdt.gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion ;
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid = sdt.gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid ;
         gxTv_SdtCoachPaciente_Coachpaciente_token = sdt.gxTv_SdtCoachPaciente_Coachpaciente_token ;
         gxTv_SdtCoachPaciente_Mode = sdt.gxTv_SdtCoachPaciente_Mode ;
         gxTv_SdtCoachPaciente_Initialized = sdt.gxTv_SdtCoachPaciente_Initialized ;
         gxTv_SdtCoachPaciente_Coachpaciente_coach_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coach_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_token_Z = sdt.gxTv_SdtCoachPaciente_Coachpaciente_token_Z ;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N ;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N ;
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N = sdt.gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N ;
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N = sdt.gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N ;
         gxTv_SdtCoachPaciente_Coachpaciente_token_N = sdt.gxTv_SdtCoachPaciente_Coachpaciente_token_N ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("CoachPaciente_Coach", gxTv_SdtCoachPaciente_Coachpaciente_coach, false);
         AddObjectProperty("CoachPaciente_CoachPNombre", gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre, false);
         AddObjectProperty("CoachPaciente_CoachPApellido", gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido, false);
         AddObjectProperty("CoachPaciente_Paciente", gxTv_SdtCoachPaciente_Coachpaciente_paciente, false);
         AddObjectProperty("CoachPaciente_PacientePNombre", gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre, false);
         AddObjectProperty("CoachPaciente_PacientePNombre_N", gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N, false);
         AddObjectProperty("CoachPaciente_PacientePApellido", gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido, false);
         AddObjectProperty("CoachPaciente_PacientePApellido_N", gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N, false);
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("CoachPaciente_FechaAsignacion", sDateCnv, false);
         AddObjectProperty("CoachPaciente_FechaAsignacion_N", gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N, false);
         AddObjectProperty("CoachPaciente_DispositivoID", gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid, false);
         AddObjectProperty("CoachPaciente_DispositivoID_N", gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N, false);
         AddObjectProperty("CoachPaciente_Token", gxTv_SdtCoachPaciente_Coachpaciente_token, false);
         AddObjectProperty("CoachPaciente_Token_N", gxTv_SdtCoachPaciente_Coachpaciente_token_N, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtCoachPaciente_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtCoachPaciente_Initialized, false);
            AddObjectProperty("CoachPaciente_Coach_Z", gxTv_SdtCoachPaciente_Coachpaciente_coach_Z, false);
            AddObjectProperty("CoachPaciente_CoachPNombre_Z", gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z, false);
            AddObjectProperty("CoachPaciente_CoachPApellido_Z", gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z, false);
            AddObjectProperty("CoachPaciente_Paciente_Z", gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z, false);
            AddObjectProperty("CoachPaciente_PacientePNombre_Z", gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z, false);
            AddObjectProperty("CoachPaciente_PacientePApellido_Z", gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z, false);
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("CoachPaciente_FechaAsignacion_Z", sDateCnv, false);
            AddObjectProperty("CoachPaciente_DispositivoID_Z", gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z, false);
            AddObjectProperty("CoachPaciente_Token_Z", gxTv_SdtCoachPaciente_Coachpaciente_token_Z, false);
            AddObjectProperty("CoachPaciente_PacientePNombre_N", gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N, false);
            AddObjectProperty("CoachPaciente_PacientePApellido_N", gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N, false);
            AddObjectProperty("CoachPaciente_FechaAsignacion_N", gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N, false);
            AddObjectProperty("CoachPaciente_DispositivoID_N", gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N, false);
            AddObjectProperty("CoachPaciente_Token_N", gxTv_SdtCoachPaciente_Coachpaciente_token_N, false);
         }
         return  ;
      }

      public void UpdateDirties( SdtCoachPaciente sdt )
      {
         if ( sdt.IsDirty("CoachPaciente_Coach") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_coach = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coach ;
         }
         if ( sdt.IsDirty("CoachPaciente_CoachPNombre") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre ;
         }
         if ( sdt.IsDirty("CoachPaciente_CoachPApellido") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido = sdt.gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido ;
         }
         if ( sdt.IsDirty("CoachPaciente_Paciente") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_paciente = sdt.gxTv_SdtCoachPaciente_Coachpaciente_paciente ;
         }
         if ( sdt.IsDirty("CoachPaciente_PacientePNombre") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre ;
         }
         if ( sdt.IsDirty("CoachPaciente_PacientePApellido") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido = sdt.gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido ;
         }
         if ( sdt.IsDirty("CoachPaciente_FechaAsignacion") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion = sdt.gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion ;
         }
         if ( sdt.IsDirty("CoachPaciente_DispositivoID") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid = sdt.gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid ;
         }
         if ( sdt.IsDirty("CoachPaciente_Token") )
         {
            gxTv_SdtCoachPaciente_Coachpaciente_token = sdt.gxTv_SdtCoachPaciente_Coachpaciente_token ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "CoachPaciente_Coach" )]
      [  XmlElement( ElementName = "CoachPaciente_Coach"   )]
      public Guid gxTpr_Coachpaciente_coach
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_coach ;
         }

         set {
            if ( gxTv_SdtCoachPaciente_Coachpaciente_coach != value )
            {
               gxTv_SdtCoachPaciente_Mode = "INS";
               this.gxTv_SdtCoachPaciente_Coachpaciente_coach_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_token_Z_SetNull( );
            }
            gxTv_SdtCoachPaciente_Coachpaciente_coach = (Guid)(value);
            SetDirty("Coachpaciente_coach");
         }

      }

      [  SoapElement( ElementName = "CoachPaciente_CoachPNombre" )]
      [  XmlElement( ElementName = "CoachPaciente_CoachPNombre"   )]
      public String gxTpr_Coachpaciente_coachpnombre
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre = value;
            SetDirty("Coachpaciente_coachpnombre");
         }

      }

      [  SoapElement( ElementName = "CoachPaciente_CoachPApellido" )]
      [  XmlElement( ElementName = "CoachPaciente_CoachPApellido"   )]
      public String gxTpr_Coachpaciente_coachpapellido
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido = value;
            SetDirty("Coachpaciente_coachpapellido");
         }

      }

      [  SoapElement( ElementName = "CoachPaciente_Paciente" )]
      [  XmlElement( ElementName = "CoachPaciente_Paciente"   )]
      public Guid gxTpr_Coachpaciente_paciente
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_paciente ;
         }

         set {
            if ( gxTv_SdtCoachPaciente_Coachpaciente_paciente != value )
            {
               gxTv_SdtCoachPaciente_Mode = "INS";
               this.gxTv_SdtCoachPaciente_Coachpaciente_coach_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z_SetNull( );
               this.gxTv_SdtCoachPaciente_Coachpaciente_token_Z_SetNull( );
            }
            gxTv_SdtCoachPaciente_Coachpaciente_paciente = (Guid)(value);
            SetDirty("Coachpaciente_paciente");
         }

      }

      [  SoapElement( ElementName = "CoachPaciente_PacientePNombre" )]
      [  XmlElement( ElementName = "CoachPaciente_PacientePNombre"   )]
      public String gxTpr_Coachpaciente_pacientepnombre
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N = 0;
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre = value;
            SetDirty("Coachpaciente_pacientepnombre");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N = 1;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_PacientePApellido" )]
      [  XmlElement( ElementName = "CoachPaciente_PacientePApellido"   )]
      public String gxTpr_Coachpaciente_pacientepapellido
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N = 0;
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido = value;
            SetDirty("Coachpaciente_pacientepapellido");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N = 1;
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_FechaAsignacion" )]
      [  XmlElement( ElementName = "CoachPaciente_FechaAsignacion"  , IsNullable=true )]
      public string gxTpr_Coachpaciente_fechaasignacion_Nullable
      {
         get {
            if ( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion).value ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N = 0;
            if (String.IsNullOrEmpty(value) || value == GxDateString.NullValue )
               gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion = DateTime.MinValue;
            else
               gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Coachpaciente_fechaasignacion
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N = 0;
            gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion = value;
            SetDirty("Coachpaciente_fechaasignacion");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N = 1;
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_DispositivoID" )]
      [  XmlElement( ElementName = "CoachPaciente_DispositivoID"   )]
      public String gxTpr_Coachpaciente_dispositivoid
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N = 0;
            gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid = value;
            SetDirty("Coachpaciente_dispositivoid");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N = 1;
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_Token" )]
      [  XmlElement( ElementName = "CoachPaciente_Token"   )]
      public String gxTpr_Coachpaciente_token
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_token ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_token_N = 0;
            gxTv_SdtCoachPaciente_Coachpaciente_token = value;
            SetDirty("Coachpaciente_token");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_token_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_token_N = 1;
         gxTv_SdtCoachPaciente_Coachpaciente_token = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_token_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtCoachPaciente_Mode ;
         }

         set {
            gxTv_SdtCoachPaciente_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_SdtCoachPaciente_Mode_SetNull( )
      {
         gxTv_SdtCoachPaciente_Mode = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtCoachPaciente_Initialized ;
         }

         set {
            gxTv_SdtCoachPaciente_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_SdtCoachPaciente_Initialized_SetNull( )
      {
         gxTv_SdtCoachPaciente_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_Coach_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_Coach_Z"   )]
      public Guid gxTpr_Coachpaciente_coach_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_coach_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_coach_Z = (Guid)(value);
            SetDirty("Coachpaciente_coach_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_coach_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_coach_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_coach_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_CoachPNombre_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_CoachPNombre_Z"   )]
      public String gxTpr_Coachpaciente_coachpnombre_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z = value;
            SetDirty("Coachpaciente_coachpnombre_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_CoachPApellido_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_CoachPApellido_Z"   )]
      public String gxTpr_Coachpaciente_coachpapellido_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z = value;
            SetDirty("Coachpaciente_coachpapellido_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_Paciente_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_Paciente_Z"   )]
      public Guid gxTpr_Coachpaciente_paciente_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z = (Guid)(value);
            SetDirty("Coachpaciente_paciente_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_PacientePNombre_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_PacientePNombre_Z"   )]
      public String gxTpr_Coachpaciente_pacientepnombre_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z = value;
            SetDirty("Coachpaciente_pacientepnombre_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_PacientePApellido_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_PacientePApellido_Z"   )]
      public String gxTpr_Coachpaciente_pacientepapellido_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z = value;
            SetDirty("Coachpaciente_pacientepapellido_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_FechaAsignacion_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_FechaAsignacion_Z"  , IsNullable=true )]
      public string gxTpr_Coachpaciente_fechaasignacion_Z_Nullable
      {
         get {
            if ( gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z == DateTime.MinValue)
               return null;
            return new GxDateString(gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDateString.NullValue )
               gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z = DateTime.MinValue;
            else
               gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Coachpaciente_fechaasignacion_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z = value;
            SetDirty("Coachpaciente_fechaasignacion_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_DispositivoID_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_DispositivoID_Z"   )]
      public String gxTpr_Coachpaciente_dispositivoid_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z = value;
            SetDirty("Coachpaciente_dispositivoid_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_Token_Z" )]
      [  XmlElement( ElementName = "CoachPaciente_Token_Z"   )]
      public String gxTpr_Coachpaciente_token_Z
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_token_Z ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_token_Z = value;
            SetDirty("Coachpaciente_token_Z");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_token_Z_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_token_Z = "";
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_token_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_PacientePNombre_N" )]
      [  XmlElement( ElementName = "CoachPaciente_PacientePNombre_N"   )]
      public short gxTpr_Coachpaciente_pacientepnombre_N
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N = value;
            SetDirty("Coachpaciente_pacientepnombre_N");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N = 0;
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_PacientePApellido_N" )]
      [  XmlElement( ElementName = "CoachPaciente_PacientePApellido_N"   )]
      public short gxTpr_Coachpaciente_pacientepapellido_N
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N = value;
            SetDirty("Coachpaciente_pacientepapellido_N");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N = 0;
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_FechaAsignacion_N" )]
      [  XmlElement( ElementName = "CoachPaciente_FechaAsignacion_N"   )]
      public short gxTpr_Coachpaciente_fechaasignacion_N
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N = value;
            SetDirty("Coachpaciente_fechaasignacion_N");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N = 0;
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_DispositivoID_N" )]
      [  XmlElement( ElementName = "CoachPaciente_DispositivoID_N"   )]
      public short gxTpr_Coachpaciente_dispositivoid_N
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N = value;
            SetDirty("Coachpaciente_dispositivoid_N");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N = 0;
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "CoachPaciente_Token_N" )]
      [  XmlElement( ElementName = "CoachPaciente_Token_N"   )]
      public short gxTpr_Coachpaciente_token_N
      {
         get {
            return gxTv_SdtCoachPaciente_Coachpaciente_token_N ;
         }

         set {
            gxTv_SdtCoachPaciente_Coachpaciente_token_N = value;
            SetDirty("Coachpaciente_token_N");
         }

      }

      public void gxTv_SdtCoachPaciente_Coachpaciente_token_N_SetNull( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_token_N = 0;
         return  ;
      }

      public bool gxTv_SdtCoachPaciente_Coachpaciente_token_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtCoachPaciente_Coachpaciente_coach = (Guid)(Guid.Empty);
         gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre = "";
         gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido = "";
         gxTv_SdtCoachPaciente_Coachpaciente_paciente = (Guid)(Guid.Empty);
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre = "";
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido = "";
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion = DateTime.MinValue;
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid = "";
         gxTv_SdtCoachPaciente_Coachpaciente_token = "";
         gxTv_SdtCoachPaciente_Mode = "";
         gxTv_SdtCoachPaciente_Coachpaciente_coach_Z = (Guid)(Guid.Empty);
         gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z = "";
         gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z = "";
         gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z = (Guid)(Guid.Empty);
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z = "";
         gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z = "";
         gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z = DateTime.MinValue;
         gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z = "";
         gxTv_SdtCoachPaciente_Coachpaciente_token_Z = "";
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "coachpaciente", "GeneXus.Programs.coachpaciente_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtCoachPaciente_Initialized ;
      private short gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_N ;
      private short gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_N ;
      private short gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_N ;
      private short gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_N ;
      private short gxTv_SdtCoachPaciente_Coachpaciente_token_N ;
      private String gxTv_SdtCoachPaciente_Mode ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion ;
      private DateTime gxTv_SdtCoachPaciente_Coachpaciente_fechaasignacion_Z ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_token ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_coachpnombre_Z ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_coachpapellido_Z ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_pacientepnombre_Z ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_pacientepapellido_Z ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_dispositivoid_Z ;
      private String gxTv_SdtCoachPaciente_Coachpaciente_token_Z ;
      private Guid gxTv_SdtCoachPaciente_Coachpaciente_coach ;
      private Guid gxTv_SdtCoachPaciente_Coachpaciente_paciente ;
      private Guid gxTv_SdtCoachPaciente_Coachpaciente_coach_Z ;
      private Guid gxTv_SdtCoachPaciente_Coachpaciente_paciente_Z ;
   }

   [DataContract(Name = @"CoachPaciente", Namespace = "PACYE2")]
   public class SdtCoachPaciente_RESTInterface : GxGenericCollectionItem<SdtCoachPaciente>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtCoachPaciente_RESTInterface( ) : base()
      {
      }

      public SdtCoachPaciente_RESTInterface( SdtCoachPaciente psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "CoachPaciente_Coach" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Coachpaciente_coach
      {
         get {
            return sdt.gxTpr_Coachpaciente_coach ;
         }

         set {
            sdt.gxTpr_Coachpaciente_coach = (Guid)(value);
         }

      }

      [DataMember( Name = "CoachPaciente_CoachPNombre" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Coachpaciente_coachpnombre
      {
         get {
            return sdt.gxTpr_Coachpaciente_coachpnombre ;
         }

         set {
            sdt.gxTpr_Coachpaciente_coachpnombre = value;
         }

      }

      [DataMember( Name = "CoachPaciente_CoachPApellido" , Order = 2 )]
      [GxSeudo()]
      public String gxTpr_Coachpaciente_coachpapellido
      {
         get {
            return sdt.gxTpr_Coachpaciente_coachpapellido ;
         }

         set {
            sdt.gxTpr_Coachpaciente_coachpapellido = value;
         }

      }

      [DataMember( Name = "CoachPaciente_Paciente" , Order = 3 )]
      [GxSeudo()]
      public Guid gxTpr_Coachpaciente_paciente
      {
         get {
            return sdt.gxTpr_Coachpaciente_paciente ;
         }

         set {
            sdt.gxTpr_Coachpaciente_paciente = (Guid)(value);
         }

      }

      [DataMember( Name = "CoachPaciente_PacientePNombre" , Order = 4 )]
      [GxSeudo()]
      public String gxTpr_Coachpaciente_pacientepnombre
      {
         get {
            return sdt.gxTpr_Coachpaciente_pacientepnombre ;
         }

         set {
            sdt.gxTpr_Coachpaciente_pacientepnombre = value;
         }

      }

      [DataMember( Name = "CoachPaciente_PacientePApellido" , Order = 5 )]
      [GxSeudo()]
      public String gxTpr_Coachpaciente_pacientepapellido
      {
         get {
            return sdt.gxTpr_Coachpaciente_pacientepapellido ;
         }

         set {
            sdt.gxTpr_Coachpaciente_pacientepapellido = value;
         }

      }

      [DataMember( Name = "CoachPaciente_FechaAsignacion" , Order = 6 )]
      [GxSeudo()]
      public String gxTpr_Coachpaciente_fechaasignacion
      {
         get {
            return DateTimeUtil.DToC2( sdt.gxTpr_Coachpaciente_fechaasignacion) ;
         }

         set {
            sdt.gxTpr_Coachpaciente_fechaasignacion = DateTimeUtil.CToD2( value);
         }

      }

      [DataMember( Name = "CoachPaciente_DispositivoID" , Order = 7 )]
      [GxSeudo()]
      public String gxTpr_Coachpaciente_dispositivoid
      {
         get {
            return sdt.gxTpr_Coachpaciente_dispositivoid ;
         }

         set {
            sdt.gxTpr_Coachpaciente_dispositivoid = value;
         }

      }

      [DataMember( Name = "CoachPaciente_Token" , Order = 8 )]
      [GxSeudo()]
      public String gxTpr_Coachpaciente_token
      {
         get {
            return sdt.gxTpr_Coachpaciente_token ;
         }

         set {
            sdt.gxTpr_Coachpaciente_token = value;
         }

      }

      public SdtCoachPaciente sdt
      {
         get {
            return (SdtCoachPaciente)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtCoachPaciente() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 9 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
