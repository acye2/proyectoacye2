/*
               File: K2BStackGoBack
        Description: Get Last Item and remove it.
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:35.12
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bstackgoback : GXProcedure
   {
      public k2bstackgoback( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bstackgoback( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( out String aP0_Url )
      {
         this.AV8Url = "" ;
         initialize();
         executePrivate();
         aP0_Url=this.AV8Url;
      }

      public String executeUdp( )
      {
         this.AV8Url = "" ;
         initialize();
         executePrivate();
         aP0_Url=this.AV8Url;
         return AV8Url ;
      }

      public void executeSubmit( out String aP0_Url )
      {
         k2bstackgoback objk2bstackgoback;
         objk2bstackgoback = new k2bstackgoback();
         objk2bstackgoback.AV8Url = "" ;
         objk2bstackgoback.context.SetSubmitInitialConfig(context);
         objk2bstackgoback.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bstackgoback);
         aP0_Url=this.AV8Url;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bstackgoback)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         new k2bgetstack(context ).execute( out  AV10Stack) ;
         if ( AV10Stack.Count > 0 )
         {
            new k2bstackgetlastitem(context ).execute(  AV10Stack, out  AV11StackItem) ;
            GXt_char1 = AV12ProgramName;
            new k2burlgetprogramname(context ).execute(  AV11StackItem.gxTpr_Url, out  GXt_char1) ;
            AV12ProgramName = GXt_char1;
            if ( StringUtil.StrCmp(StringUtil.Upper( AV12ProgramName), StringUtil.Upper( StringUtil.Trim( AV9Request.ScriptName))) == 0 )
            {
               new k2bstackpop(context ).execute(  AV10Stack) ;
               new k2bstackgetlastitem(context ).execute(  AV10Stack, out  AV11StackItem) ;
               AV8Url = AV11StackItem.gxTpr_Url;
            }
            else
            {
               AV8Url = AV11StackItem.gxTpr_Url;
            }
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10Stack = new GXBaseCollection<SdtK2BStack_K2BStackItem>( context, "K2BStackItem", "PACYE2");
         AV11StackItem = new SdtK2BStack_K2BStackItem(context);
         AV12ProgramName = "";
         GXt_char1 = "";
         AV9Request = new GxHttpRequest( context);
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String GXt_char1 ;
      private String AV8Url ;
      private String AV12ProgramName ;
      private String aP0_Url ;
      private GxHttpRequest AV9Request ;
      private GXBaseCollection<SdtK2BStack_K2BStackItem> AV10Stack ;
      private SdtK2BStack_K2BStackItem AV11StackItem ;
   }

}
