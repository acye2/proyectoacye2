/*
               File: K2BRemovePrefixInCaption
        Description: Remove a prefix string in caption.
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:34.13
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bremoveprefixincaption : GXProcedure
   {
      public k2bremoveprefixincaption( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bremoveprefixincaption( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_Caption ,
                           String aP1_String ,
                           out String aP2_NewCaption )
      {
         this.AV8Caption = aP0_Caption;
         this.AV10String = aP1_String;
         this.AV9NewCaption = "" ;
         initialize();
         executePrivate();
         aP2_NewCaption=this.AV9NewCaption;
      }

      public String executeUdp( String aP0_Caption ,
                                String aP1_String )
      {
         this.AV8Caption = aP0_Caption;
         this.AV10String = aP1_String;
         this.AV9NewCaption = "" ;
         initialize();
         executePrivate();
         aP2_NewCaption=this.AV9NewCaption;
         return AV9NewCaption ;
      }

      public void executeSubmit( String aP0_Caption ,
                                 String aP1_String ,
                                 out String aP2_NewCaption )
      {
         k2bremoveprefixincaption objk2bremoveprefixincaption;
         objk2bremoveprefixincaption = new k2bremoveprefixincaption();
         objk2bremoveprefixincaption.AV8Caption = aP0_Caption;
         objk2bremoveprefixincaption.AV10String = aP1_String;
         objk2bremoveprefixincaption.AV9NewCaption = "" ;
         objk2bremoveprefixincaption.context.SetSubmitInitialConfig(context);
         objk2bremoveprefixincaption.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bremoveprefixincaption);
         aP2_NewCaption=this.AV9NewCaption;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bremoveprefixincaption)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV9NewCaption = AV8Caption;
         AV11lengthCaption = StringUtil.Len( AV8Caption);
         AV12lengthString = StringUtil.Len( AV10String);
         AV13lengthStringPlusOne = (int)(AV12lengthString+1);
         AV14lengthNewCaption = (decimal)(AV11lengthCaption-AV12lengthString);
         if ( StringUtil.StrCmp(StringUtil.Upper( StringUtil.Substring( AV8Caption, 1, AV12lengthString)), StringUtil.Upper( AV10String)) == 0 )
         {
            AV9NewCaption = StringUtil.Substring( AV8Caption, AV13lengthStringPlusOne, (int)(AV14lengthNewCaption));
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private int AV11lengthCaption ;
      private int AV12lengthString ;
      private int AV13lengthStringPlusOne ;
      private decimal AV14lengthNewCaption ;
      private String AV8Caption ;
      private String AV10String ;
      private String AV9NewCaption ;
      private String aP2_NewCaption ;
   }

}
