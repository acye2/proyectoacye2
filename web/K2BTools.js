jQuery.noConflict();
jQuery(document).ready(function () {

	jQuery(document).find(".tabs_container").each(function(i1){
		jQuery(this).children("div").each(function(i2){
			if(i2>0){
				jQuery(this).addClass("hide");
			}
		});
	});
	
})

function hideTabs(){
	jQuery(document).find(".tabs_control").each(function(i1){
	
		var selectedTab = -1
		jQuery(this).children("div").each(function(i2){
			if(jQuery(this).hasClass("selected")){
				selectedTab = i2;
				return false;
			}
		});
		
		jQuery(this).find(".tabs_container").children("div").each(function(i3){
			if(selectedTab == i3){
				jQuery(this).removeClass("hide");
			}else{
				jQuery(this).addClass("hide");
			}
		});
	});
}

var handler = function() { hideTabs(); };

jQuery(document).on("DOMNodeInserted", handler);

jQuery(document).on('click',".tab_desc, .selected", function () {
	jQuery(this).parent().children(".selected").toggleClass("selected").toggleClass("tab_desc");
	jQuery(this).toggleClass("tab_desc").toggleClass("selected");

	array = jQuery(this).parent().children(".selected, .tab_desc");
	index = jQuery.inArray(this, array);

	jQuery(this).parent().children(".tabs_container").children().addClass("hide");
	jQuery(this).parent().children(".tabs_container").children(":eq(" + index + ")").removeClass("hide");
});

// JScript source code
function showHideFilters(image_id, table_id, internalNameHideAttribute, showTitle, hideTitle){
	if (gx.dom.el(internalNameHideAttribute).value == 0){
		gx.dom.el(internalNameHideAttribute).value = 1;//change variable value 
		//hide the table
		gx.dom.el(table_id).style.display = "none";
		gx.dom.el(image_id).className = "K2BButtonDown";
		gx.dom.el(image_id).title = showTitle;
	}
	else
	{
		gx.dom.el(internalNameHideAttribute).value = 0;
		//show table
		gx.dom.el(table_id).style.display = "block";
		gx.dom.el(image_id).className = "K2BButtonUp";
		gx.dom.el(image_id).title = hideTitle;		
	}
	return false;
}


function ChangeDivClassAndColapseDiv(e, themeHeaderOpen, themeHeaderColapse, themeContentOpen, themeContentColapse)
{
    var targ;
	if (!e) var e = window.event;
	if (e.target) targ = e.target;
	else if (e.srcElement) targ = e.srcElement;
	if (targ.nodeType == 3) // defeat Safari bug
		targ = targ.parentNode;
    if (targ != null)
    {
        if (targ.nextSibling != null)
        {
            if (targ.className == themeHeaderOpen)
            {
                // colapse content div
                targ.nextSibling.className = themeContentColapse;
                // cambio el tema del header
                targ.className = themeHeaderColapse;
            }
            else
            {
                 // open content div
                targ.nextSibling.className = themeContentOpen;
                // cambio el tema del header
                targ.className = themeHeaderOpen;
            }
        }
    }
}

function checkall(ev) {
    checkallgrid(ev, 'vSEL');
}

function getEventTarget(e) {
  if(e.target || e.srcElement)
	return e.target || e.srcElement;
  else{
	e = window.event;
	return e.target || e.srcElement;
  }
}

function checkallgrid(e, checksNamePart) {
    if (!e) var e = window.event;
    e.cancelBubble = true;
    if (e.stopPropagation) e.stopPropagation();

	var target = getEventTarget(e);
	
    for (var i = 0; i < document.MAINFORM.elements.length; i++) {
        var c = document.MAINFORM.elements[i];

        if (((c.type == 'checkbox') && (c.name != 'allbox')) && (c.name.toUpperCase().indexOf(checksNamePart.toUpperCase()) >= 0)) {
            if (c.checked != target.checked) {
                c.click();
				c.checked= target.checked;
				
				 
            }
        }
    }
}


// JScript source code
function showTable(_tables, _beginTab, _tab, _endTab, table_selected, table_count, _K2BCurrentFilterTab) {
    var _i = 0;
    var imageUrl = document.getElementById(_beginTab[table_selected]).src;
    var lastSlash = imageUrl.lastIndexOf("/");
    var baseUrl = imageUrl.slice(0, lastSlash + 1);
    while (_i < table_count) {
        if (_i != table_selected) {
            // change no selected image tab	                        
            _tables[_i].style.display = "none";
            document.getElementById(_beginTab[_i]).src = baseUrl + "tabbeginunselectedfilter.gif";
            document.getElementById(_endTab[_i]).src = baseUrl + "tabendunselectedfilter.gif";
            document.getElementById(_tab[_i]).className = "K2BFilterUnSelectedTab";
        }
        _i = _i + 1;
    }
    document.getElementById(_K2BCurrentFilterTab).value = table_selected;
    _tables[table_selected].style.display = "block";
    //change image to display
    document.getElementById(_tab[table_selected]).className = "K2BFilterSelectedTab";
    document.getElementById(_beginTab[table_selected]).src = baseUrl + "tabbeginselectedfilter.gif";
    document.getElementById(_endTab[table_selected]).src = baseUrl + "tabendselectedfilter.gif";
    return false;
}

var parentsCheck = function(element, hops){
	i = 0;
	while(i<hops && element != document.documentElement && element.parentElement!=null){
		element = element.parentElement;
		i++;
	}
	return element.parentElement != null || element == document.documentElement;
}

// Used to hide menus
var htmlClickCallback = function (e) {
	jQuery(".ControlBeautify_CollapsableTable:visible").each(function(index, element){
		var containerTable = jQuery(element).closest(".ControlBeautify_ParentCollapsableTable");
		
		if ((parentsCheck(e.target,10))&&(jQuery(e.target).closest(containerTable).length < 1)) {
			jQuery(element).hide();
		}
	});
}
	
jQuery('html').on('click', this.htmlClickCallback);