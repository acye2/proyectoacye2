/*
               File: K2BSelectColumns
        Description: Select Columns
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:45.65
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bselectcolumns : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public k2bselectcolumns( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bselectcolumns( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_programName ,
                           String aP1_GridName )
      {
         this.AV22programName = aP0_programName;
         this.AV17GridName = aP1_GridName;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavAttributeselected = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Columnsgrid") == 0 )
            {
               nRC_GXsfl_13 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_13_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_13_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrColumnsgrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Columnsgrid") == 0 )
            {
               AV23Reload_ColumnsGrid = (bool)(BooleanUtil.Val(GetNextPar( )));
               AV15CurrentPage_ColumnsGrid = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV20I_LoadCount_ColumnsGrid = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV22programName = GetNextPar( );
               AV17GridName = GetNextPar( );
               ajax_req_read_hidden_sdt(GetNextPar( ), AV14columns);
               AV18HasNextPage_ColumnsGrid = (bool)(BooleanUtil.Val(GetNextPar( )));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrColumnsgrid_refresh( AV23Reload_ColumnsGrid, AV15CurrentPage_ColumnsGrid, AV20I_LoadCount_ColumnsGrid, AV22programName, AV17GridName, AV14columns, AV18HasNextPage_ColumnsGrid) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV22programName = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22programName", AV22programName);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROGRAMNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22programName, "")), context));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV17GridName = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GridName", AV17GridName);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17GridName, "")), context));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "selectcolumns_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA112( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START112( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, false);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171524572", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = ((nGXWrapped==0) ? " data-HasEnter=\"false\" data-Skiponenter=\"false\"" : "");
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"Form\" data-gx-class=\"Form\" novalidate action=\""+formatLink("k2bselectcolumns.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV22programName)) + "," + UrlEncode(StringUtil.RTrim(AV17GridName))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "Form", true);
         }
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_13", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_13), 4, 0, ",", "")));
         GxWebStd.gx_boolean_hidden_field( context, "vRELOAD_COLUMNSGRID", AV23Reload_ColumnsGrid);
         GxWebStd.gx_hidden_field( context, "gxhash_vRELOAD_COLUMNSGRID", GetSecureSignedToken( "", AV23Reload_ColumnsGrid, context));
         GxWebStd.gx_hidden_field( context, "vI_LOADCOUNT_COLUMNSGRID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV20I_LoadCount_ColumnsGrid), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPROGRAMNAME", StringUtil.RTrim( AV22programName));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROGRAMNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22programName, "")), context));
         GxWebStd.gx_hidden_field( context, "vGRIDNAME", StringUtil.RTrim( AV17GridName));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRIDNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17GridName, "")), context));
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "vCOLUMNS", AV14columns);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vCOLUMNS", AV14columns);
         }
         GxWebStd.gx_boolean_hidden_field( context, "vHASNEXTPAGE_COLUMNSGRID", AV18HasNextPage_ColumnsGrid);
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( nGXWrapped != 1 )
         {
            context.WriteHtmlTextNl( "</form>") ;
         }
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE112( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT112( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("k2bselectcolumns.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV22programName)) + "," + UrlEncode(StringUtil.RTrim(AV17GridName)) ;
      }

      public override String GetPgmname( )
      {
         return "K2BSelectColumns" ;
      }

      public override String GetPgmdesc( )
      {
         return "Select Columns" ;
      }

      protected void WB110( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", "", "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            wb_table1_4_112( true) ;
         }
         else
         {
            wb_table1_4_112( false) ;
         }
         return  ;
      }

      protected void wb_table1_4_112e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START112( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Select Columns", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP110( ) ;
      }

      protected void WS112( )
      {
         START112( ) ;
         EVT112( ) ;
      }

      protected void EVT112( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'E_CONFIRM'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'E_Confirm' */
                              E11112 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 16), "COLUMNSGRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_13_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
                              SubsflControlProps_132( ) ;
                              if ( ( ( ((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0) < 0 ) ) || ( ( ((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0) > 9999 ) ) )
                              {
                                 GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vATTRIBUTESELECTED");
                                 GX_FocusControl = chkavAttributeselected_Internalname;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                                 wbErr = true;
                                 AV10AttributeSelected = 0;
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavAttributeselected_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AttributeSelected), 4, 0)));
                              }
                              else
                              {
                                 AV10AttributeSelected = (short)(((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0));
                                 context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavAttributeselected_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AttributeSelected), 4, 0)));
                              }
                              AV13ColumnName = cgiGet( edtavColumnname_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavColumnname_Internalname, AV13ColumnName);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCOLUMNNAME"+"_"+sGXsfl_13_idx, GetSecureSignedToken( sGXsfl_13_idx, StringUtil.RTrim( context.localUtil.Format( AV13ColumnName, "")), context));
                              AV11ClmTitle = cgiGet( edtavClmtitle_Internalname);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavClmtitle_Internalname, AV11ClmTitle);
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E12112 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Refresh */
                                    E13112 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "COLUMNSGRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E14112 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE112( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA112( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            GXCCtl = "vATTRIBUTESELECTED_" + sGXsfl_13_idx;
            chkavAttributeselected.Name = GXCCtl;
            chkavAttributeselected.WebTags = "";
            chkavAttributeselected.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAttributeselected_Internalname, "TitleCaption", chkavAttributeselected.Caption, !bGXsfl_13_Refreshing);
            chkavAttributeselected.CheckedValue = "0";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavCurrentpage_columnsgrid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrColumnsgrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_132( ) ;
         while ( nGXsfl_13_idx <= nRC_GXsfl_13 )
         {
            sendrow_132( ) ;
            nGXsfl_13_idx = (short)(((subColumnsgrid_Islastpage==1)&&(nGXsfl_13_idx+1>subColumnsgrid_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
            sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
            SubsflControlProps_132( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( ColumnsgridContainer));
         /* End function gxnrColumnsgrid_newrow */
      }

      protected void gxgrColumnsgrid_refresh( bool AV23Reload_ColumnsGrid ,
                                              short AV15CurrentPage_ColumnsGrid ,
                                              short AV20I_LoadCount_ColumnsGrid ,
                                              String AV22programName ,
                                              String AV17GridName ,
                                              GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> AV14columns ,
                                              bool AV18HasNextPage_ColumnsGrid )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         COLUMNSGRID_nCurrentRecord = 0;
         RF112( ) ;
         /* End function gxgrColumnsgrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_vCOLUMNNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV13ColumnName, "")), context));
         GxWebStd.gx_hidden_field( context, "vCOLUMNNAME", StringUtil.RTrim( AV13ColumnName));
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF112( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV29Pgmname = "K2BSelectColumns";
         context.Gx_err = 0;
         edtavColumnname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColumnname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColumnname_Enabled), 5, 0)), !bGXsfl_13_Refreshing);
         edtavClmtitle_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClmtitle_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClmtitle_Enabled), 5, 0)), !bGXsfl_13_Refreshing);
         edtavCurrentpage_columnsgrid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCurrentpage_columnsgrid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCurrentpage_columnsgrid_Enabled), 5, 0)), true);
      }

      protected void RF112( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            ColumnsgridContainer.ClearRows();
         }
         wbStart = 13;
         /* Execute user event: Refresh */
         E13112 ();
         nGXsfl_13_idx = 1;
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
         bGXsfl_13_Refreshing = true;
         ColumnsgridContainer.AddObjectProperty("GridName", "Columnsgrid");
         ColumnsgridContainer.AddObjectProperty("CmpContext", "");
         ColumnsgridContainer.AddObjectProperty("InMasterPage", "false");
         ColumnsgridContainer.AddObjectProperty("Class", "Grid_WorkWith");
         ColumnsgridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
         ColumnsgridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         ColumnsgridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Backcolorstyle), 1, 0, ".", "")));
         ColumnsgridContainer.PageSize = subColumnsgrid_Recordsperpage( );
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_132( ) ;
            E14112 ();
            wbEnd = 13;
            WB110( ) ;
         }
         bGXsfl_13_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes112( )
      {
         GxWebStd.gx_boolean_hidden_field( context, "vRELOAD_COLUMNSGRID", AV23Reload_ColumnsGrid);
         GxWebStd.gx_hidden_field( context, "gxhash_vRELOAD_COLUMNSGRID", GetSecureSignedToken( "", AV23Reload_ColumnsGrid, context));
         GxWebStd.gx_hidden_field( context, "vPROGRAMNAME", StringUtil.RTrim( AV22programName));
         GxWebStd.gx_hidden_field( context, "gxhash_vPROGRAMNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22programName, "")), context));
         GxWebStd.gx_hidden_field( context, "vGRIDNAME", StringUtil.RTrim( AV17GridName));
         GxWebStd.gx_hidden_field( context, "gxhash_vGRIDNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17GridName, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_vCOLUMNNAME"+"_"+sGXsfl_13_idx, GetSecureSignedToken( sGXsfl_13_idx, StringUtil.RTrim( context.localUtil.Format( AV13ColumnName, "")), context));
      }

      protected int subColumnsgrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subColumnsgrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subColumnsgrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subColumnsgrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP110( )
      {
         /* Before Start, stand alone formulas. */
         AV29Pgmname = "K2BSelectColumns";
         context.Gx_err = 0;
         edtavColumnname_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavColumnname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavColumnname_Enabled), 5, 0)), !bGXsfl_13_Refreshing);
         edtavClmtitle_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClmtitle_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClmtitle_Enabled), 5, 0)), !bGXsfl_13_Refreshing);
         edtavCurrentpage_columnsgrid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCurrentpage_columnsgrid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCurrentpage_columnsgrid_Enabled), 5, 0)), true);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E12112 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            if ( ( ( context.localUtil.CToN( cgiGet( edtavCurrentpage_columnsgrid_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavCurrentpage_columnsgrid_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vCURRENTPAGE_COLUMNSGRID");
               GX_FocusControl = edtavCurrentpage_columnsgrid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV15CurrentPage_ColumnsGrid = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage_ColumnsGrid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage_ColumnsGrid), 4, 0)));
            }
            else
            {
               AV15CurrentPage_ColumnsGrid = (short)(context.localUtil.CToN( cgiGet( edtavCurrentpage_columnsgrid_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage_ColumnsGrid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage_ColumnsGrid), 4, 0)));
            }
            /* Read saved values. */
            nRC_GXsfl_13 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_13"), ",", "."));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void S132( )
      {
         /* 'U_REFRESHPAGE' Routine */
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E12112 ();
         if (returnInSub) return;
      }

      protected void E12112( )
      {
         /* Start Routine */
         if ( ! new k2bisauthorizedactivityname(context).executeUdp(  "",  "",  "None",  "SelectColumns",  AV29Pgmname) )
         {
            CallWebObject(formatLink("k2bnotauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("None")) + "," + UrlEncode(StringUtil.RTrim("SelectColumns")) + "," + UrlEncode(StringUtil.RTrim(AV29Pgmname)));
            context.wjLocDisableFrm = 1;
         }
         else
         {
            if ( (0==AV15CurrentPage_ColumnsGrid) )
            {
               AV15CurrentPage_ColumnsGrid = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15CurrentPage_ColumnsGrid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV15CurrentPage_ColumnsGrid), 4, 0)));
            }
            AV23Reload_ColumnsGrid = true;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23Reload_ColumnsGrid", AV23Reload_ColumnsGrid);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRELOAD_COLUMNSGRID", GetSecureSignedToken( "", AV23Reload_ColumnsGrid, context));
            if ( StringUtil.StrCmp(AV19HttpRequest.Method, "GET") == 0 )
            {
               /* Execute user subroutine: 'U_OPENPAGE' */
               S112 ();
               if (returnInSub) return;
            }
            /* Execute user subroutine: 'U_STARTPAGE' */
            S122 ();
            if (returnInSub) return;
         }
      }

      protected void S122( )
      {
         /* 'U_STARTPAGE' Routine */
      }

      protected void E13112( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         /* Execute user subroutine: 'U_REFRESHPAGE' */
         S132 ();
         if (returnInSub) return;
      }

      protected void S112( )
      {
         /* 'U_OPENPAGE' Routine */
      }

      protected void S152( )
      {
         /* 'UPDATEPAGINGCONTROLS(COLUMNSGRID)' Routine */
         imgI_pagingpreviousctrol_columnsgrid_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgI_pagingpreviousctrol_columnsgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgI_pagingpreviousctrol_columnsgrid_Visible), 5, 0)), true);
         imgI_pagingnextctrol_columnsgrid_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgI_pagingnextctrol_columnsgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgI_pagingnextctrol_columnsgrid_Visible), 5, 0)), true);
         if ( AV15CurrentPage_ColumnsGrid == 1 )
         {
            imgI_pagingpreviousctrol_columnsgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgI_pagingpreviousctrol_columnsgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgI_pagingpreviousctrol_columnsgrid_Visible), 5, 0)), true);
         }
         if ( ! AV18HasNextPage_ColumnsGrid )
         {
            imgI_pagingnextctrol_columnsgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgI_pagingnextctrol_columnsgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(imgI_pagingnextctrol_columnsgrid_Visible), 5, 0)), true);
         }
      }

      private void E14112( )
      {
         /* Columnsgrid_Load Routine */
         if ( ! AV23Reload_ColumnsGrid )
         {
            /* Start For Each Line in Columnsgrid */
            nRC_GXsfl_13 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_13"), ",", "."));
            nGXsfl_13_fel_idx = 0;
            while ( nGXsfl_13_fel_idx < nRC_GXsfl_13 )
            {
               nGXsfl_13_fel_idx = (short)(((subColumnsgrid_Islastpage==1)&&(nGXsfl_13_fel_idx+1>subColumnsgrid_Recordsperpage( )) ? 1 : nGXsfl_13_fel_idx+1));
               sGXsfl_13_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_fel_idx), 4, 0)), 4, "0");
               SubsflControlProps_fel_132( ) ;
               if ( ( ( ((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0) < 0 ) ) || ( ( ((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0) > 9999 ) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vATTRIBUTESELECTED");
                  GX_FocusControl = chkavAttributeselected_Internalname;
                  wbErr = true;
                  AV10AttributeSelected = 0;
               }
               else
               {
                  AV10AttributeSelected = (short)(((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0));
               }
               AV13ColumnName = cgiGet( edtavColumnname_Internalname);
               AV11ClmTitle = cgiGet( edtavClmtitle_Internalname);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 13;
               }
               sendrow_132( ) ;
               if ( isFullAjaxMode( ) && ! bGXsfl_13_Refreshing )
               {
                  context.DoAjaxLoad(13, ColumnsgridRow);
               }
               /* End For Each Line */
            }
            if ( nGXsfl_13_fel_idx == 0 )
            {
               nGXsfl_13_idx = 1;
               sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
               SubsflControlProps_132( ) ;
            }
            nGXsfl_13_fel_idx = 1;
         }
         else
         {
            AV20I_LoadCount_ColumnsGrid = 0;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20I_LoadCount_ColumnsGrid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20I_LoadCount_ColumnsGrid), 4, 0)));
            AV18HasNextPage_ColumnsGrid = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18HasNextPage_ColumnsGrid", AV18HasNextPage_ColumnsGrid);
            AV16Exit_ColumnsGrid = false;
            while ( true )
            {
               AV20I_LoadCount_ColumnsGrid = (short)(AV20I_LoadCount_ColumnsGrid+1);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20I_LoadCount_ColumnsGrid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV20I_LoadCount_ColumnsGrid), 4, 0)));
               if ( AV20I_LoadCount_ColumnsGrid > 10 * AV15CurrentPage_ColumnsGrid )
               {
                  AV18HasNextPage_ColumnsGrid = true;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18HasNextPage_ColumnsGrid", AV18HasNextPage_ColumnsGrid);
                  if (true) break;
               }
               /* Execute user subroutine: 'U_LOADROWVARS(COLUMNSGRID)' */
               S142 ();
               if (returnInSub) return;
               if ( AV16Exit_ColumnsGrid )
               {
                  if (true) break;
               }
               if ( ( AV20I_LoadCount_ColumnsGrid > ( 10 * ( AV15CurrentPage_ColumnsGrid - 1 ) ) ) && ( AV20I_LoadCount_ColumnsGrid <= ( 10 * AV15CurrentPage_ColumnsGrid ) ) )
               {
                  /* Load Method */
                  if ( wbStart != -1 )
                  {
                     wbStart = 13;
                  }
                  sendrow_132( ) ;
                  if ( isFullAjaxMode( ) && ! bGXsfl_13_Refreshing )
                  {
                     context.DoAjaxLoad(13, ColumnsgridRow);
                  }
               }
            }
            /* Execute user subroutine: 'UPDATEPAGINGCONTROLS(COLUMNSGRID)' */
            S152 ();
            if (returnInSub) return;
         }
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14columns", AV14columns);
      }

      protected void S142( )
      {
         /* 'U_LOADROWVARS(COLUMNSGRID)' Routine */
         if ( AV20I_LoadCount_ColumnsGrid == 1 )
         {
            GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1 = AV14columns;
            new k2bloadgridcolumns(context ).execute(  AV22programName,  AV17GridName, out  GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1) ;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22programName", AV22programName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROGRAMNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22programName, "")), context));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GridName", AV17GridName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17GridName, "")), context));
            AV14columns = GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1;
         }
         AV7addedItem = false;
         while ( ( ( AV14columns.Count >= AV20I_LoadCount_ColumnsGrid ) ) && ! AV7addedItem )
         {
            AV13ColumnName = ((SdtK2BGridColumns_K2BGridColumnsItem)AV14columns.Item(AV20I_LoadCount_ColumnsGrid)).gxTpr_Attributename;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavColumnname_Internalname, AV13ColumnName);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vCOLUMNNAME"+"_"+sGXsfl_13_idx, GetSecureSignedToken( sGXsfl_13_idx, StringUtil.RTrim( context.localUtil.Format( AV13ColumnName, "")), context));
            AV11ClmTitle = ((SdtK2BGridColumns_K2BGridColumnsItem)AV14columns.Item(AV20I_LoadCount_ColumnsGrid)).gxTpr_Columntitle;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavClmtitle_Internalname, AV11ClmTitle);
            if ( ((SdtK2BGridColumns_K2BGridColumnsItem)AV14columns.Item(AV20I_LoadCount_ColumnsGrid)).gxTpr_Showattribute )
            {
               AV10AttributeSelected = 1;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavAttributeselected_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AttributeSelected), 4, 0)));
            }
            else
            {
               AV10AttributeSelected = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, chkavAttributeselected_Internalname, StringUtil.LTrim( StringUtil.Str( (decimal)(AV10AttributeSelected), 4, 0)));
            }
            AV7addedItem = true;
         }
         if ( ! AV7addedItem )
         {
            AV16Exit_ColumnsGrid = true;
         }
      }

      protected void S162( )
      {
         /* 'U_CONFIRM' Routine */
         GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1 = AV14columns;
         new k2bloadgridcolumns(context ).execute(  AV22programName,  AV17GridName, out  GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22programName", AV22programName);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROGRAMNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22programName, "")), context));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GridName", AV17GridName);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17GridName, "")), context));
         AV14columns = GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1;
         /* Start For Each Line */
         nRC_GXsfl_13 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_13"), ",", "."));
         nGXsfl_13_fel_idx = 0;
         while ( nGXsfl_13_fel_idx < nRC_GXsfl_13 )
         {
            nGXsfl_13_fel_idx = (short)(((subColumnsgrid_Islastpage==1)&&(nGXsfl_13_fel_idx+1>subColumnsgrid_Recordsperpage( )) ? 1 : nGXsfl_13_fel_idx+1));
            sGXsfl_13_fel_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_fel_idx), 4, 0)), 4, "0");
            SubsflControlProps_fel_132( ) ;
            if ( ( ( ((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0) < 0 ) ) || ( ( ((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0) > 9999 ) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vATTRIBUTESELECTED");
               GX_FocusControl = chkavAttributeselected_Internalname;
               wbErr = true;
               AV10AttributeSelected = 0;
            }
            else
            {
               AV10AttributeSelected = (short)(((StringUtil.StrCmp(cgiGet( chkavAttributeselected_Internalname), "1")==0) ? 1 : 0));
            }
            AV13ColumnName = cgiGet( edtavColumnname_Internalname);
            AV11ClmTitle = cgiGet( edtavClmtitle_Internalname);
            AV32GXV1 = 1;
            while ( AV32GXV1 <= AV14columns.Count )
            {
               AV12Column = ((SdtK2BGridColumns_K2BGridColumnsItem)AV14columns.Item(AV32GXV1));
               if ( StringUtil.StrCmp(AV12Column.gxTpr_Attributename, AV13ColumnName) == 0 )
               {
                  if ( AV10AttributeSelected == 1 )
                  {
                     AV12Column.gxTpr_Showattribute = true;
                  }
                  else
                  {
                     AV12Column.gxTpr_Showattribute = false;
                  }
                  if (true) break;
               }
               AV32GXV1 = (int)(AV32GXV1+1);
            }
            /* End For Each Line */
         }
         if ( nGXsfl_13_fel_idx == 0 )
         {
            nGXsfl_13_idx = 1;
            sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
            SubsflControlProps_132( ) ;
         }
         nGXsfl_13_fel_idx = 1;
         new k2bsavegridcolumns(context ).execute(  AV22programName,  AV17GridName,  AV14columns) ;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22programName", AV22programName);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROGRAMNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22programName, "")), context));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GridName", AV17GridName);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17GridName, "")), context));
         context.setWebReturnParms(new Object[] {});
         context.setWebReturnParmsMetadata(new Object[] {});
         context.wjLocDisableFrm = 1;
         context.nUserReturn = 1;
         returnInSub = true;
         if (true) return;
      }

      protected void E11112( )
      {
         /* 'E_Confirm' Routine */
         /* Execute user subroutine: 'U_CONFIRM' */
         S162 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
         context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", false, "AV14columns", AV14columns);
      }

      protected void wb_table1_4_112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "Table_Basic_Widht100Percent", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:10px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsection_datagen_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section_PagingGridOrderedby", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /*  Grid Control  */
            ColumnsgridContainer.SetWrapped(nGXWrapped);
            if ( ColumnsgridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"ColumnsgridContainer"+"DivS\" data-gxgridid=\"13\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subColumnsgrid_Internalname, subColumnsgrid_Internalname, "", "Grid_WorkWith", 0, "", "", 0, 1, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subColumnsgrid_Backcolorstyle == 0 )
               {
                  subColumnsgrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subColumnsgrid_Class) > 0 )
                  {
                     subColumnsgrid_Linesclass = subColumnsgrid_Class+"Title";
                  }
               }
               else
               {
                  subColumnsgrid_Titlebackstyle = 1;
                  if ( subColumnsgrid_Backcolorstyle == 1 )
                  {
                     subColumnsgrid_Titlebackcolor = subColumnsgrid_Allbackcolor;
                     if ( StringUtil.Len( subColumnsgrid_Class) > 0 )
                     {
                        subColumnsgrid_Linesclass = subColumnsgrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subColumnsgrid_Class) > 0 )
                     {
                        subColumnsgrid_Linesclass = subColumnsgrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" width="+StringUtil.LTrim( StringUtil.Str( (decimal)(14), 4, 0))+"px"+" class=\""+subColumnsgrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subColumnsgrid_Linesclass+"\" "+" style=\""+"display:none;"+""+"\" "+">") ;
               context.SendWebValue( "Column Name") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"left"+"\" "+" nowrap=\"nowrap\" "+" class=\""+subColumnsgrid_Linesclass+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Column") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               ColumnsgridContainer.AddObjectProperty("GridName", "Columnsgrid");
            }
            else
            {
               ColumnsgridContainer.AddObjectProperty("GridName", "Columnsgrid");
               ColumnsgridContainer.AddObjectProperty("Class", "Grid_WorkWith");
               ColumnsgridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(0), 4, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Backcolorstyle), 1, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("CmpContext", "");
               ColumnsgridContainer.AddObjectProperty("InMasterPage", "false");
               ColumnsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               ColumnsgridColumn.AddObjectProperty("Value", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV10AttributeSelected), 4, 0, ".", "")));
               ColumnsgridContainer.AddColumnProperties(ColumnsgridColumn);
               ColumnsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               ColumnsgridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV13ColumnName));
               ColumnsgridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavColumnname_Enabled), 5, 0, ".", "")));
               ColumnsgridContainer.AddColumnProperties(ColumnsgridColumn);
               ColumnsgridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               ColumnsgridColumn.AddObjectProperty("Value", StringUtil.RTrim( AV11ClmTitle));
               ColumnsgridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavClmtitle_Enabled), 5, 0, ".", "")));
               ColumnsgridContainer.AddColumnProperties(ColumnsgridColumn);
               ColumnsgridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Allowselection), 1, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Selectioncolor), 9, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Allowhovering), 1, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Hoveringcolor), 9, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Allowcollapsing), 1, 0, ".", "")));
               ColumnsgridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subColumnsgrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 13 )
         {
            wbEnd = 0;
            nRC_GXsfl_13 = (short)(nGXsfl_13_idx-1);
            if ( ColumnsgridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"ColumnsgridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Columnsgrid", ColumnsgridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "ColumnsgridContainerData", ColumnsgridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "ColumnsgridContainerData"+"V", ColumnsgridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"ColumnsgridContainerData"+"V"+"\" value='"+ColumnsgridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            wb_table2_19_112( true) ;
         }
         else
         {
            wb_table2_19_112( false) ;
         }
         return  ;
      }

      protected void wb_table2_19_112e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            wb_table3_36_112( true) ;
         }
         else
         {
            wb_table3_36_112( false) ;
         }
         return  ;
      }

      protected void wb_table3_36_112e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divK2bsection_hiddenitemscontainer_Internalname, 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:10px")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_4_112e( true) ;
         }
         else
         {
            wb_table1_4_112e( false) ;
         }
      }

      protected void wb_table3_36_112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblColumns1_Internalname, tblColumns1_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td data-align=\"center\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-center;text-align:-moz-center;text-align:-webkit-center;vertical-align:top;width:100%")+"\">") ;
            wb_table4_39_112( true) ;
         }
         else
         {
            wb_table4_39_112( false) ;
         }
         return  ;
      }

      protected void wb_table4_39_112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_36_112e( true) ;
         }
         else
         {
            wb_table3_36_112e( false) ;
         }
      }

      protected void wb_table4_39_112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable4_Internalname, tblTable4_Internalname, "", "Table_Basic_Widht100Percent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 42,'',false,'',0)\"";
            ClassString = "Button_Standard";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttConfirm_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(13), 2, 0)+","+"null"+");", " Confirm", bttConfirm_Jsonclick, 5, "", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'E_CONFIRM\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_K2BSelectColumns.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_39_112e( true) ;
         }
         else
         {
            wb_table4_39_112e( false) ;
         }
      }

      protected void wb_table2_19_112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            sStyleString = sStyleString + " width: " + StringUtil.LTrim( StringUtil.Str( (decimal)(99), 10, 0)) + "%" + ";";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 3, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td data-align=\"right\"  style=\""+CSSHelper.Prettify( "text-align:-khtml-right;text-align:-moz-right;text-align:-webkit-right")+"\">") ;
            wb_table5_23_112( true) ;
         }
         else
         {
            wb_table5_23_112( false) ;
         }
         return  ;
      }

      protected void wb_table5_23_112e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_19_112e( true) ;
         }
         else
         {
            wb_table2_19_112e( false) ;
         }
      }

      protected void wb_table5_23_112( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable3_Internalname, tblTable3_Internalname, "", "Table", 0, "", "", 0, 0, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 27,'',false,'',0)\"";
            ClassString = "Image_PagingLefts";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "9c80ca2c-4f86-4e22-a96f-0c8babb7dda0", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgI_pagingpreviousctrol_columnsgrid_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgI_pagingpreviousctrol_columnsgrid_Visible, 1, "", "Previous", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgI_pagingpreviousctrol_columnsgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e15111_client"+"'", StyleString, ClassString, "", "", "", "", ""+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_K2BSelectColumns.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock1_Internalname, "Page ", "", "", lblTextblock1_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock_Page", 0, "", 1, 1, 0, "HLP_K2BSelectColumns.htm");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 32,'',false,'" + sGXsfl_13_idx + "',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCurrentpage_columnsgrid_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV15CurrentPage_ColumnsGrid), 4, 0, ",", "")), ((edtavCurrentpage_columnsgrid_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV15CurrentPage_ColumnsGrid), "ZZZ9")) : context.localUtil.Format( (decimal)(AV15CurrentPage_ColumnsGrid), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,32);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCurrentpage_columnsgrid_Jsonclick, 0, "Attribute_CurrentPage", "", "", "", "", 1, edtavCurrentpage_columnsgrid_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_K2BSelectColumns.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "", "div");
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 35,'',false,'',0)\"";
            ClassString = "Image_PagingRights";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "91afb8f5-23fc-449b-a958-d6ad445f8a8a", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgI_pagingnextctrol_columnsgrid_Internalname, sImgUrl, "", "", "", context.GetTheme( ), imgI_pagingnextctrol_columnsgrid_Visible, 1, "", "Next", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgI_pagingnextctrol_columnsgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e16111_client"+"'", StyleString, ClassString, "", "", "", "", ""+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_K2BSelectColumns.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table5_23_112e( true) ;
         }
         else
         {
            wb_table5_23_112e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV22programName = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22programName", AV22programName);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPROGRAMNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV22programName, "")), context));
         AV17GridName = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17GridName", AV17GridName);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vGRIDNAME", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV17GridName, "")), context));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlatCompactGreen");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA112( ) ;
         WS112( ) ;
         WE112( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), false);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171524627", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         if ( nGXWrapped != 1 )
         {
            context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
            context.AddJavascriptSource("k2bselectcolumns.js", "?201811171524627", false);
         }
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_132( )
      {
         chkavAttributeselected_Internalname = "vATTRIBUTESELECTED_"+sGXsfl_13_idx;
         edtavColumnname_Internalname = "vCOLUMNNAME_"+sGXsfl_13_idx;
         edtavClmtitle_Internalname = "vCLMTITLE_"+sGXsfl_13_idx;
      }

      protected void SubsflControlProps_fel_132( )
      {
         chkavAttributeselected_Internalname = "vATTRIBUTESELECTED_"+sGXsfl_13_fel_idx;
         edtavColumnname_Internalname = "vCOLUMNNAME_"+sGXsfl_13_fel_idx;
         edtavClmtitle_Internalname = "vCLMTITLE_"+sGXsfl_13_fel_idx;
      }

      protected void sendrow_132( )
      {
         SubsflControlProps_132( ) ;
         WB110( ) ;
         ColumnsgridRow = GXWebRow.GetNew(context,ColumnsgridContainer);
         if ( subColumnsgrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subColumnsgrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subColumnsgrid_Class, "") != 0 )
            {
               subColumnsgrid_Linesclass = subColumnsgrid_Class+"Odd";
            }
         }
         else if ( subColumnsgrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subColumnsgrid_Backstyle = 0;
            subColumnsgrid_Backcolor = subColumnsgrid_Allbackcolor;
            if ( StringUtil.StrCmp(subColumnsgrid_Class, "") != 0 )
            {
               subColumnsgrid_Linesclass = subColumnsgrid_Class+"Uniform";
            }
         }
         else if ( subColumnsgrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subColumnsgrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subColumnsgrid_Class, "") != 0 )
            {
               subColumnsgrid_Linesclass = subColumnsgrid_Class+"Odd";
            }
            subColumnsgrid_Backcolor = (int)(0xFFFFFF);
         }
         else if ( subColumnsgrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subColumnsgrid_Backstyle = 1;
            if ( ((int)((nGXsfl_13_idx) % (2))) == 0 )
            {
               subColumnsgrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subColumnsgrid_Class, "") != 0 )
               {
                  subColumnsgrid_Linesclass = subColumnsgrid_Class+"Even";
               }
            }
            else
            {
               subColumnsgrid_Backcolor = (int)(0xFFFFFF);
               if ( StringUtil.StrCmp(subColumnsgrid_Class, "") != 0 )
               {
                  subColumnsgrid_Linesclass = subColumnsgrid_Class+"Odd";
               }
            }
         }
         if ( ColumnsgridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+subColumnsgrid_Linesclass+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_13_idx+"\">") ;
         }
         /* Subfile cell */
         if ( ColumnsgridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Check box */
         TempTags = " " + ((chkavAttributeselected.Enabled!=0)&&(chkavAttributeselected.Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 14,'',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ClassString = "Attribute";
         StyleString = "";
         ColumnsgridRow.AddColumnProperties("checkbox", 1, isAjaxCallMode( ), new Object[] {(String)chkavAttributeselected_Internalname,StringUtil.Str( (decimal)(AV10AttributeSelected), 4, 0),(String)"",(String)"",(short)-1,(short)1,(String)"   1",(String)"",(String)StyleString,(String)ClassString,(String)"",(String)"",TempTags+" onclick=\"gx.fn.checkboxClick(14, this, 1, 0);gx.evt.onchange(this, event);\" "+((chkavAttributeselected.Enabled!=0)&&(chkavAttributeselected.Visible!=0) ? " onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,14);\"" : " ")});
         /* Subfile cell */
         if ( ColumnsgridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+"display:none;"+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavColumnname_Enabled!=0)&&(edtavColumnname_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 15,'',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute_Grid";
         ColumnsgridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavColumnname_Internalname,StringUtil.RTrim( AV13ColumnName),(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavColumnname_Enabled!=0)&&(edtavColumnname_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,15);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavColumnname_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"",(String)"",(short)0,(int)edtavColumnname_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         /* Subfile cell */
         if ( ColumnsgridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"left"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         TempTags = " " + ((edtavClmtitle_Enabled!=0)&&(edtavClmtitle_Visible!=0) ? " onfocus=\"gx.evt.onfocus(this, 16,'',false,'"+sGXsfl_13_idx+"',13)\"" : " ");
         ROClassString = "Attribute_Grid";
         ColumnsgridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavClmtitle_Internalname,StringUtil.RTrim( AV11ClmTitle),(String)"",TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+((edtavClmtitle_Enabled!=0)&&(edtavClmtitle_Visible!=0) ? " onblur=\""+""+";gx.evt.onblur(this,16);\"" : " "),(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavClmtitle_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"",(String)"",(short)-1,(int)edtavClmtitle_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)50,(short)0,(short)0,(short)13,(short)1,(short)-1,(short)-1,(bool)true,(String)"",(String)"left",(bool)true});
         send_integrity_lvl_hashes112( ) ;
         ColumnsgridContainer.AddRow(ColumnsgridRow);
         nGXsfl_13_idx = (short)(((subColumnsgrid_Islastpage==1)&&(nGXsfl_13_idx+1>subColumnsgrid_Recordsperpage( )) ? 1 : nGXsfl_13_idx+1));
         sGXsfl_13_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_13_idx), 4, 0)), 4, "0");
         SubsflControlProps_132( ) ;
         /* End function sendrow_132 */
      }

      protected void init_default_properties( )
      {
         chkavAttributeselected_Internalname = "vATTRIBUTESELECTED";
         edtavColumnname_Internalname = "vCOLUMNNAME";
         edtavClmtitle_Internalname = "vCLMTITLE";
         imgI_pagingpreviousctrol_columnsgrid_Internalname = "I_PAGINGPREVIOUSCTROL_COLUMNSGRID";
         lblTextblock1_Internalname = "TEXTBLOCK1";
         edtavCurrentpage_columnsgrid_Internalname = "vCURRENTPAGE_COLUMNSGRID";
         imgI_pagingnextctrol_columnsgrid_Internalname = "I_PAGINGNEXTCTROL_COLUMNSGRID";
         tblTable3_Internalname = "TABLE3";
         tblTable2_Internalname = "TABLE2";
         bttConfirm_Internalname = "CONFIRM";
         tblTable4_Internalname = "TABLE4";
         tblColumns1_Internalname = "COLUMNS1";
         divK2bsection_datagen_Internalname = "K2BSECTION_DATAGEN";
         divK2bsection_hiddenitemscontainer_Internalname = "K2BSECTION_HIDDENITEMSCONTAINER";
         tblTable1_Internalname = "TABLE1";
         Form.Internalname = "FORM";
         subColumnsgrid_Internalname = "COLUMNSGRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavClmtitle_Jsonclick = "";
         edtavClmtitle_Visible = -1;
         edtavColumnname_Jsonclick = "";
         edtavColumnname_Visible = 0;
         chkavAttributeselected.Visible = -1;
         chkavAttributeselected.Enabled = 1;
         imgI_pagingnextctrol_columnsgrid_Visible = 1;
         edtavCurrentpage_columnsgrid_Jsonclick = "";
         edtavCurrentpage_columnsgrid_Enabled = 1;
         imgI_pagingpreviousctrol_columnsgrid_Visible = 1;
         subColumnsgrid_Allowcollapsing = 0;
         subColumnsgrid_Allowselection = 0;
         edtavClmtitle_Enabled = 1;
         edtavColumnname_Enabled = 1;
         subColumnsgrid_Class = "Grid_WorkWith";
         subColumnsgrid_Backcolorstyle = 3;
         chkavAttributeselected.Caption = "";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Select Columns";
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'COLUMNSGRID_nFirstRecordOnPage',nv:0},{av:'COLUMNSGRID_nEOF',nv:0},{av:'AV15CurrentPage_ColumnsGrid',fld:'vCURRENTPAGE_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV20I_LoadCount_ColumnsGrid',fld:'vI_LOADCOUNT_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV14columns',fld:'vCOLUMNS',pic:'',nv:null},{av:'AV18HasNextPage_ColumnsGrid',fld:'vHASNEXTPAGE_COLUMNSGRID',pic:'',nv:false},{av:'AV22programName',fld:'vPROGRAMNAME',pic:'',hsh:true,nv:''},{av:'AV17GridName',fld:'vGRIDNAME',pic:'',hsh:true,nv:''},{av:'AV23Reload_ColumnsGrid',fld:'vRELOAD_COLUMNSGRID',pic:'',hsh:true,nv:false}],oparms:[]}");
         setEventMetadata("'PAGINGNEXT(COLUMNSGRID)'","{handler:'E16111',iparms:[{av:'COLUMNSGRID_nFirstRecordOnPage',nv:0},{av:'COLUMNSGRID_nEOF',nv:0},{av:'AV23Reload_ColumnsGrid',fld:'vRELOAD_COLUMNSGRID',pic:'',hsh:true,nv:false},{av:'AV15CurrentPage_ColumnsGrid',fld:'vCURRENTPAGE_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV20I_LoadCount_ColumnsGrid',fld:'vI_LOADCOUNT_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV22programName',fld:'vPROGRAMNAME',pic:'',hsh:true,nv:''},{av:'AV17GridName',fld:'vGRIDNAME',pic:'',hsh:true,nv:''},{av:'AV14columns',fld:'vCOLUMNS',pic:'',nv:null},{av:'AV18HasNextPage_ColumnsGrid',fld:'vHASNEXTPAGE_COLUMNSGRID',pic:'',nv:false}],oparms:[{av:'AV15CurrentPage_ColumnsGrid',fld:'vCURRENTPAGE_COLUMNSGRID',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("'PAGINGPREVIOUS(COLUMNSGRID)'","{handler:'E15111',iparms:[{av:'COLUMNSGRID_nFirstRecordOnPage',nv:0},{av:'COLUMNSGRID_nEOF',nv:0},{av:'AV23Reload_ColumnsGrid',fld:'vRELOAD_COLUMNSGRID',pic:'',hsh:true,nv:false},{av:'AV15CurrentPage_ColumnsGrid',fld:'vCURRENTPAGE_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV20I_LoadCount_ColumnsGrid',fld:'vI_LOADCOUNT_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV22programName',fld:'vPROGRAMNAME',pic:'',hsh:true,nv:''},{av:'AV17GridName',fld:'vGRIDNAME',pic:'',hsh:true,nv:''},{av:'AV14columns',fld:'vCOLUMNS',pic:'',nv:null},{av:'AV18HasNextPage_ColumnsGrid',fld:'vHASNEXTPAGE_COLUMNSGRID',pic:'',nv:false}],oparms:[{av:'AV15CurrentPage_ColumnsGrid',fld:'vCURRENTPAGE_COLUMNSGRID',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("COLUMNSGRID.LOAD","{handler:'E14112',iparms:[{av:'AV23Reload_ColumnsGrid',fld:'vRELOAD_COLUMNSGRID',pic:'',hsh:true,nv:false},{av:'AV15CurrentPage_ColumnsGrid',fld:'vCURRENTPAGE_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV20I_LoadCount_ColumnsGrid',fld:'vI_LOADCOUNT_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV22programName',fld:'vPROGRAMNAME',pic:'',hsh:true,nv:''},{av:'AV17GridName',fld:'vGRIDNAME',pic:'',hsh:true,nv:''},{av:'AV14columns',fld:'vCOLUMNS',pic:'',nv:null},{av:'AV18HasNextPage_ColumnsGrid',fld:'vHASNEXTPAGE_COLUMNSGRID',pic:'',nv:false}],oparms:[{av:'AV20I_LoadCount_ColumnsGrid',fld:'vI_LOADCOUNT_COLUMNSGRID',pic:'ZZZ9',nv:0},{av:'AV18HasNextPage_ColumnsGrid',fld:'vHASNEXTPAGE_COLUMNSGRID',pic:'',nv:false},{av:'AV14columns',fld:'vCOLUMNS',pic:'',nv:null},{av:'AV13ColumnName',fld:'vCOLUMNNAME',pic:'',hsh:true,nv:''},{av:'AV11ClmTitle',fld:'vCLMTITLE',pic:'',nv:''},{av:'AV10AttributeSelected',fld:'vATTRIBUTESELECTED',pic:'ZZZ9',nv:0},{av:'imgI_pagingpreviousctrol_columnsgrid_Visible',ctrl:'I_PAGINGPREVIOUSCTROL_COLUMNSGRID',prop:'Visible'},{av:'imgI_pagingnextctrol_columnsgrid_Visible',ctrl:'I_PAGINGNEXTCTROL_COLUMNSGRID',prop:'Visible'}]}");
         setEventMetadata("'E_CONFIRM'","{handler:'E11112',iparms:[{av:'AV22programName',fld:'vPROGRAMNAME',pic:'',hsh:true,nv:''},{av:'AV17GridName',fld:'vGRIDNAME',pic:'',hsh:true,nv:''},{av:'AV13ColumnName',fld:'vCOLUMNNAME',grid:13,pic:'',hsh:true,nv:''},{av:'AV10AttributeSelected',fld:'vATTRIBUTESELECTED',grid:13,pic:'ZZZ9',nv:0}],oparms:[{av:'AV14columns',fld:'vCOLUMNS',pic:'',nv:null}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV22programName = "";
         wcpOAV17GridName = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV14columns = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2");
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV13ColumnName = "";
         AV11ClmTitle = "";
         GXCCtl = "";
         ColumnsgridContainer = new GXWebGrid( context);
         AV29Pgmname = "";
         AV19HttpRequest = new GxHttpRequest( context);
         ColumnsgridRow = new GXWebRow();
         GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1 = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2");
         AV12Column = new SdtK2BGridColumns_K2BGridColumnsItem(context);
         sStyleString = "";
         subColumnsgrid_Linesclass = "";
         ColumnsgridColumn = new GXWebColumn();
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttConfirm_Jsonclick = "";
         sImgUrl = "";
         imgI_pagingpreviousctrol_columnsgrid_Jsonclick = "";
         lblTextblock1_Jsonclick = "";
         imgI_pagingnextctrol_columnsgrid_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         AV29Pgmname = "K2BSelectColumns";
         /* GeneXus formulas. */
         AV29Pgmname = "K2BSelectColumns";
         context.Gx_err = 0;
         edtavColumnname_Enabled = 0;
         edtavClmtitle_Enabled = 0;
         edtavCurrentpage_columnsgrid_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_13 ;
      private short nGXsfl_13_idx=1 ;
      private short AV15CurrentPage_ColumnsGrid ;
      private short AV20I_LoadCount_ColumnsGrid ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short nGXWrapped ;
      private short wbEnd ;
      private short wbStart ;
      private short AV10AttributeSelected ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short subColumnsgrid_Backcolorstyle ;
      private short COLUMNSGRID_nEOF ;
      private short nGXsfl_13_fel_idx=1 ;
      private short subColumnsgrid_Titlebackstyle ;
      private short subColumnsgrid_Allowselection ;
      private short subColumnsgrid_Allowhovering ;
      private short subColumnsgrid_Allowcollapsing ;
      private short subColumnsgrid_Collapsed ;
      private short subColumnsgrid_Backstyle ;
      private int subColumnsgrid_Islastpage ;
      private int edtavColumnname_Enabled ;
      private int edtavClmtitle_Enabled ;
      private int edtavCurrentpage_columnsgrid_Enabled ;
      private int imgI_pagingpreviousctrol_columnsgrid_Visible ;
      private int imgI_pagingnextctrol_columnsgrid_Visible ;
      private int AV32GXV1 ;
      private int subColumnsgrid_Titlebackcolor ;
      private int subColumnsgrid_Allbackcolor ;
      private int subColumnsgrid_Selectioncolor ;
      private int subColumnsgrid_Hoveringcolor ;
      private int idxLst ;
      private int subColumnsgrid_Backcolor ;
      private int edtavColumnname_Visible ;
      private int edtavClmtitle_Visible ;
      private long COLUMNSGRID_nCurrentRecord ;
      private long COLUMNSGRID_nFirstRecordOnPage ;
      private String AV22programName ;
      private String AV17GridName ;
      private String wcpOAV22programName ;
      private String wcpOAV17GridName ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_13_idx="0001" ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String chkavAttributeselected_Internalname ;
      private String AV13ColumnName ;
      private String edtavColumnname_Internalname ;
      private String AV11ClmTitle ;
      private String edtavClmtitle_Internalname ;
      private String GXCCtl ;
      private String edtavCurrentpage_columnsgrid_Internalname ;
      private String AV29Pgmname ;
      private String imgI_pagingpreviousctrol_columnsgrid_Internalname ;
      private String imgI_pagingnextctrol_columnsgrid_Internalname ;
      private String sGXsfl_13_fel_idx="0001" ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String divK2bsection_datagen_Internalname ;
      private String subColumnsgrid_Internalname ;
      private String subColumnsgrid_Class ;
      private String subColumnsgrid_Linesclass ;
      private String divK2bsection_hiddenitemscontainer_Internalname ;
      private String tblColumns1_Internalname ;
      private String tblTable4_Internalname ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttConfirm_Internalname ;
      private String bttConfirm_Jsonclick ;
      private String tblTable2_Internalname ;
      private String tblTable3_Internalname ;
      private String sImgUrl ;
      private String imgI_pagingpreviousctrol_columnsgrid_Jsonclick ;
      private String lblTextblock1_Internalname ;
      private String lblTextblock1_Jsonclick ;
      private String edtavCurrentpage_columnsgrid_Jsonclick ;
      private String imgI_pagingnextctrol_columnsgrid_Jsonclick ;
      private String ROClassString ;
      private String edtavColumnname_Jsonclick ;
      private String edtavClmtitle_Jsonclick ;
      private bool entryPointCalled ;
      private bool AV23Reload_ColumnsGrid ;
      private bool AV18HasNextPage_ColumnsGrid ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_13_Refreshing=false ;
      private bool returnInSub ;
      private bool gx_refresh_fired ;
      private bool AV16Exit_ColumnsGrid ;
      private bool AV7addedItem ;
      private GXWebGrid ColumnsgridContainer ;
      private GXWebRow ColumnsgridRow ;
      private GXWebColumn ColumnsgridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCheckbox chkavAttributeselected ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GxHttpRequest AV19HttpRequest ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> AV14columns ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> GXt_objcol_SdtK2BGridColumns_K2BGridColumnsItem1 ;
      private GXWebForm Form ;
      private SdtK2BGridColumns_K2BGridColumnsItem AV12Column ;
   }

}
