/*
               File: ExportWWPersona
        Description: Export WWPersona
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 12:48:51.30
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Office;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aexportwwpersona : GXWebProcedure, System.Web.SessionState.IRequiresSessionState
   {
      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "persona_Execute" ;
         }

      }

      public override void webExecute( )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize();
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            if ( ! entryPointCalled )
            {
               AV14PersonaPNombre = gxfirstwebparm;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV16PersonaFechaNacimiento_From = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV17PersonaFechaNacimiento_To = context.localUtil.ParseDateParm( GetNextPar( ));
                  AV9OrderedBy = (short)(NumberUtil.Val( GetNextPar( ), "."));
               }
            }
         }
         if ( GxWebError == 0 )
         {
            executePrivate();
         }
         cleanup();
      }

      public aexportwwpersona( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public aexportwwpersona( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_PersonaPNombre ,
                           DateTime aP1_PersonaFechaNacimiento_From ,
                           DateTime aP2_PersonaFechaNacimiento_To ,
                           short aP3_OrderedBy )
      {
         this.AV14PersonaPNombre = aP0_PersonaPNombre;
         this.AV16PersonaFechaNacimiento_From = aP1_PersonaFechaNacimiento_From;
         this.AV17PersonaFechaNacimiento_To = aP2_PersonaFechaNacimiento_To;
         this.AV9OrderedBy = aP3_OrderedBy;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_PersonaPNombre ,
                                 DateTime aP1_PersonaFechaNacimiento_From ,
                                 DateTime aP2_PersonaFechaNacimiento_To ,
                                 short aP3_OrderedBy )
      {
         aexportwwpersona objaexportwwpersona;
         objaexportwwpersona = new aexportwwpersona();
         objaexportwwpersona.AV14PersonaPNombre = aP0_PersonaPNombre;
         objaexportwwpersona.AV16PersonaFechaNacimiento_From = aP1_PersonaFechaNacimiento_From;
         objaexportwwpersona.AV17PersonaFechaNacimiento_To = aP2_PersonaFechaNacimiento_To;
         objaexportwwpersona.AV9OrderedBy = aP3_OrderedBy;
         objaexportwwpersona.context.SetSubmitInitialConfig(context);
         objaexportwwpersona.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaexportwwpersona);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aexportwwpersona)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( ! new k2bisauthorizedactivityname(context).executeUdp(  "Persona",  "Persona",  "List",  "",  AV43Pgmname) )
         {
            AV11Filename = "";
            AV12ErrorMessage = "You are not authorized to do activity ";
            AV12ErrorMessage = AV12ErrorMessage + "EntityName:" + "Persona";
            AV12ErrorMessage = AV12ErrorMessage + "TransactionName:" + "Persona";
            AV12ErrorMessage = AV12ErrorMessage + "ActivityType:" + "List";
            AV12ErrorMessage = AV12ErrorMessage + " PgmName:" + AV43Pgmname;
            AV22HttpResponse.AddString(AV12ErrorMessage);
            context.nUserReturn = 1;
            this.cleanup();
            if (true) return;
         }
         new k2bgetcontext(context ).execute( out  AV8Context) ;
         AV20Random = (int)(NumberUtil.Random( )*10000);
         AV11Filename = GxDirectory.TemporaryFilesPath + AV24File.Separator + "ExportWWPersona-" + StringUtil.Trim( StringUtil.Str( (decimal)(AV20Random), 8, 0)) + ".xlsx";
         /* Execute user subroutine: 'HIDESHOWCOLUMNS' */
         S121 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13ExcelDocument.Open(AV11Filename);
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13ExcelDocument.Clear();
         AV13ExcelDocument.AutoFit = 1;
         AV18CellRow = 1;
         AV19FirstColumn = 1;
         AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn, 1, 1).Size = 15;
         AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn, 1, 1).Bold = 1;
         AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn, 1, 1).Text = "Personas";
         AV18CellRow = (int)(AV18CellRow+4);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14PersonaPNombre)) )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+0, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+0, 1, 1).Text = "PNombre";
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+1, 1, 1).Text = StringUtil.RTrim( AV14PersonaPNombre);
            AV18CellRow = (int)(AV18CellRow+1);
         }
         if ( ! (DateTime.MinValue==AV16PersonaFechaNacimiento_From) || ! (DateTime.MinValue==AV17PersonaFechaNacimiento_To) )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+0, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+0, 1, 1).Text = "Fecha Nacimiento";
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+1, 1, 1).Text = context.localUtil.DToC( AV16PersonaFechaNacimiento_From, 2, "/")+" - "+context.localUtil.DToC( AV17PersonaFechaNacimiento_To, 2, "/");
            AV18CellRow = (int)(AV18CellRow+1);
         }
         AV18CellRow = (int)(AV18CellRow+3);
         AV21ColumnIndex = 0;
         if ( AV26GridColumnVisible_PersonaPNombre )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "PNombre";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV27GridColumnVisible_PersonaSNombre )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "SNombre";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV28GridColumnVisible_PersonaPApellido )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "PApellido";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV29GridColumnVisible_PersonaSApellido )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "SApellido";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV30GridColumnVisible_PersonaSexo )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "Sexo";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV31GridColumnVisible_PersonaDPI )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "DPI";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV32GridColumnVisible_PersonaTipo )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "Tipo";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV33GridColumnVisible_PersonaFechaNacimiento )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "Fecha Nacimiento";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         if ( AV34GridColumnVisible_PersonaPacienteOrigenSordera )
         {
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Bold = 1;
            AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = "Origen Sordera";
            AV21ColumnIndex = (short)(AV21ColumnIndex+1);
         }
         pr_default.dynParam(0, new Object[]{ new Object[]{
                                              AV14PersonaPNombre ,
                                              AV17PersonaFechaNacimiento_To ,
                                              AV16PersonaFechaNacimiento_From ,
                                              A2PersonaPNombre ,
                                              A9PersonaFechaNacimiento ,
                                              AV9OrderedBy } ,
                                              new int[]{
                                              TypeConstants.STRING, TypeConstants.DATE, TypeConstants.DATE, TypeConstants.STRING, TypeConstants.DATE, TypeConstants.BOOLEAN, TypeConstants.SHORT
                                              }
         } ) ;
         lV14PersonaPNombre = StringUtil.Concat( StringUtil.RTrim( AV14PersonaPNombre), "%", "");
         /* Using cursor P002F2 */
         pr_default.execute(0, new Object[] {lV14PersonaPNombre, AV17PersonaFechaNacimiento_To, AV16PersonaFechaNacimiento_From});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A9PersonaFechaNacimiento = P002F2_A9PersonaFechaNacimiento[0];
            n9PersonaFechaNacimiento = P002F2_n9PersonaFechaNacimiento[0];
            A2PersonaPNombre = P002F2_A2PersonaPNombre[0];
            A3PersonaSNombre = P002F2_A3PersonaSNombre[0];
            n3PersonaSNombre = P002F2_n3PersonaSNombre[0];
            A4PersonaPApellido = P002F2_A4PersonaPApellido[0];
            A5PersonaSApellido = P002F2_A5PersonaSApellido[0];
            n5PersonaSApellido = P002F2_n5PersonaSApellido[0];
            A6PersonaSexo = P002F2_A6PersonaSexo[0];
            n6PersonaSexo = P002F2_n6PersonaSexo[0];
            A7PersonaDPI = P002F2_A7PersonaDPI[0];
            n7PersonaDPI = P002F2_n7PersonaDPI[0];
            A8PersonaTipo = P002F2_A8PersonaTipo[0];
            A10PersonaPacienteOrigenSordera = P002F2_A10PersonaPacienteOrigenSordera[0];
            n10PersonaPacienteOrigenSordera = P002F2_n10PersonaPacienteOrigenSordera[0];
            A1PersonaID = (Guid)((Guid)(P002F2_A1PersonaID[0]));
            AV18CellRow = (int)(AV18CellRow+1);
            AV21ColumnIndex = 0;
            if ( AV26GridColumnVisible_PersonaPNombre )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = StringUtil.RTrim( A2PersonaPNombre);
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV27GridColumnVisible_PersonaSNombre )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = StringUtil.RTrim( A3PersonaSNombre);
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV28GridColumnVisible_PersonaPApellido )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = StringUtil.RTrim( A4PersonaPApellido);
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV29GridColumnVisible_PersonaSApellido )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = StringUtil.RTrim( A5PersonaSApellido);
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV30GridColumnVisible_PersonaSexo )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = StringUtil.RTrim( A6PersonaSexo);
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV31GridColumnVisible_PersonaDPI )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Text = StringUtil.RTrim( A7PersonaDPI);
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV32GridColumnVisible_PersonaTipo )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Number = A8PersonaTipo;
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV33GridColumnVisible_PersonaFechaNacimiento )
            {
               GXt_dtime1 = DateTimeUtil.ResetTime( A9PersonaFechaNacimiento ) ;
               AV13ExcelDocument.SetDateFormat(context, 8, 5, 0, 3, "/", ":", " ");
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Date = GXt_dtime1;
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            if ( AV34GridColumnVisible_PersonaPacienteOrigenSordera )
            {
               AV13ExcelDocument.get_Cells(AV18CellRow, AV19FirstColumn+AV21ColumnIndex, 1, 1).Number = A10PersonaPacienteOrigenSordera;
               AV21ColumnIndex = (short)(AV21ColumnIndex+1);
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV13ExcelDocument.Save();
         /* Execute user subroutine: 'CHECKSTATUS' */
         S111 ();
         if ( returnInSub )
         {
            this.cleanup();
            if (true) return;
         }
         AV13ExcelDocument.Close();
         if ( ! context.isAjaxRequest( ) )
         {
            AV22HttpResponse.AppendHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
         }
         if ( ! context.isAjaxRequest( ) )
         {
            AV22HttpResponse.AppendHeader("Content-Disposition", "attachment;filename=ExportWWPersona.xlsx");
         }
         if ( ! context.isAjaxRequest( ) )
         {
            AV22HttpResponse.AppendHeader("X-Frame-Options", "deny");
         }
         if ( ! context.isAjaxRequest( ) )
         {
            AV22HttpResponse.AppendHeader("Type-Options", " nosniff");
         }
         AV22HttpResponse.AddFile(AV11Filename);
         new k2bremoveexceldocument(context).executeSubmit(  AV11Filename) ;
         if ( context.WillRedirect( ) )
         {
            context.Redirect( context.wjLoc );
            context.wjLoc = "";
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'CHECKSTATUS' Routine */
         if ( AV13ExcelDocument.ErrCode != 0 )
         {
            AV11Filename = "";
            AV12ErrorMessage = AV13ExcelDocument.ErrDescription;
            AV22HttpResponse.AddString(AV12ErrorMessage);
            context.nUserReturn = 1;
            returnInSub = true;
            if (true) return;
         }
      }

      protected void S121( )
      {
         /* 'HIDESHOWCOLUMNS' Routine */
         new k2bloadgridcolumns(context ).execute(  "WWPersona",  "Grid", out  AV35GridColumns) ;
         AV39AttributeName = "PersonaID";
         AV40ColumnTitle = "ID";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV25GridColumnVisible_PersonaID = true;
         AV39AttributeName = "PersonaPNombre";
         AV40ColumnTitle = "PNombre";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV26GridColumnVisible_PersonaPNombre = true;
         AV39AttributeName = "PersonaSNombre";
         AV40ColumnTitle = "SNombre";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV27GridColumnVisible_PersonaSNombre = true;
         AV39AttributeName = "PersonaPApellido";
         AV40ColumnTitle = "PApellido";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV28GridColumnVisible_PersonaPApellido = true;
         AV39AttributeName = "PersonaSApellido";
         AV40ColumnTitle = "SApellido";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV29GridColumnVisible_PersonaSApellido = true;
         AV39AttributeName = "PersonaSexo";
         AV40ColumnTitle = "Sexo";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV30GridColumnVisible_PersonaSexo = true;
         AV39AttributeName = "PersonaDPI";
         AV40ColumnTitle = "DPI";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV31GridColumnVisible_PersonaDPI = true;
         AV39AttributeName = "PersonaTipo";
         AV40ColumnTitle = "Tipo";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV32GridColumnVisible_PersonaTipo = true;
         AV39AttributeName = "PersonaFechaNacimiento";
         AV40ColumnTitle = "Fecha Nacimiento";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV33GridColumnVisible_PersonaFechaNacimiento = true;
         AV39AttributeName = "PersonaPacienteOrigenSordera";
         AV40ColumnTitle = "Origen Sordera";
         /* Execute user subroutine: 'ADDCOLUMNIFNOTEXISTS' */
         S131 ();
         if (returnInSub) return;
         AV34GridColumnVisible_PersonaPacienteOrigenSordera = true;
         new k2bsavegridcolumns(context ).execute(  "WWPersona",  "Grid",  AV35GridColumns) ;
         AV45GXV1 = 1;
         while ( AV45GXV1 <= AV35GridColumns.Count )
         {
            AV36GridColumn = ((SdtK2BGridColumns_K2BGridColumnsItem)AV35GridColumns.Item(AV45GXV1));
            if ( ! AV36GridColumn.gxTpr_Showattribute )
            {
               if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaID") == 0 )
               {
                  AV25GridColumnVisible_PersonaID = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaPNombre") == 0 )
               {
                  AV26GridColumnVisible_PersonaPNombre = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaSNombre") == 0 )
               {
                  AV27GridColumnVisible_PersonaSNombre = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaPApellido") == 0 )
               {
                  AV28GridColumnVisible_PersonaPApellido = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaSApellido") == 0 )
               {
                  AV29GridColumnVisible_PersonaSApellido = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaSexo") == 0 )
               {
                  AV30GridColumnVisible_PersonaSexo = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaDPI") == 0 )
               {
                  AV31GridColumnVisible_PersonaDPI = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaTipo") == 0 )
               {
                  AV32GridColumnVisible_PersonaTipo = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaFechaNacimiento") == 0 )
               {
                  AV33GridColumnVisible_PersonaFechaNacimiento = false;
               }
               else if ( StringUtil.StrCmp(AV36GridColumn.gxTpr_Attributename, "PersonaPacienteOrigenSordera") == 0 )
               {
                  AV34GridColumnVisible_PersonaPacienteOrigenSordera = false;
               }
            }
            AV45GXV1 = (int)(AV45GXV1+1);
         }
      }

      protected void S131( )
      {
         /* 'ADDCOLUMNIFNOTEXISTS' Routine */
         AV37FoundColumn = false;
         AV38CurrentColumn = 1;
         while ( ( AV38CurrentColumn <= AV35GridColumns.Count ) && ! AV37FoundColumn )
         {
            if ( StringUtil.StrCmp(((SdtK2BGridColumns_K2BGridColumnsItem)AV35GridColumns.Item(AV38CurrentColumn)).gxTpr_Attributename, AV39AttributeName) == 0 )
            {
               AV37FoundColumn = true;
            }
            AV38CurrentColumn = (short)(AV38CurrentColumn+1);
         }
         if ( ! AV37FoundColumn )
         {
            AV36GridColumn = new SdtK2BGridColumns_K2BGridColumnsItem(context);
            AV36GridColumn.gxTpr_Attributename = AV39AttributeName;
            AV36GridColumn.gxTpr_Columntitle = AV40ColumnTitle;
            AV36GridColumn.gxTpr_Showattribute = true;
            AV35GridColumns.Add(AV36GridColumn, 0);
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         base.cleanup();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         GXKey = "";
         gxfirstwebparm = "";
         AV43Pgmname = "";
         AV11Filename = "";
         AV12ErrorMessage = "";
         AV22HttpResponse = new GxHttpResponse( context);
         AV8Context = new SdtK2BContext(context);
         AV24File = new GxFile(context.GetPhysicalPath());
         AV13ExcelDocument = new ExcelDocumentI();
         scmdbuf = "";
         lV14PersonaPNombre = "";
         A2PersonaPNombre = "";
         A9PersonaFechaNacimiento = DateTime.MinValue;
         P002F2_A9PersonaFechaNacimiento = new DateTime[] {DateTime.MinValue} ;
         P002F2_n9PersonaFechaNacimiento = new bool[] {false} ;
         P002F2_A2PersonaPNombre = new String[] {""} ;
         P002F2_A3PersonaSNombre = new String[] {""} ;
         P002F2_n3PersonaSNombre = new bool[] {false} ;
         P002F2_A4PersonaPApellido = new String[] {""} ;
         P002F2_A5PersonaSApellido = new String[] {""} ;
         P002F2_n5PersonaSApellido = new bool[] {false} ;
         P002F2_A6PersonaSexo = new String[] {""} ;
         P002F2_n6PersonaSexo = new bool[] {false} ;
         P002F2_A7PersonaDPI = new String[] {""} ;
         P002F2_n7PersonaDPI = new bool[] {false} ;
         P002F2_A8PersonaTipo = new short[1] ;
         P002F2_A10PersonaPacienteOrigenSordera = new short[1] ;
         P002F2_n10PersonaPacienteOrigenSordera = new bool[] {false} ;
         P002F2_A1PersonaID = new Guid[] {Guid.Empty} ;
         A3PersonaSNombre = "";
         A4PersonaPApellido = "";
         A5PersonaSApellido = "";
         A6PersonaSexo = "";
         A7PersonaDPI = "";
         A1PersonaID = (Guid)(Guid.Empty);
         GXt_dtime1 = (DateTime)(DateTime.MinValue);
         AV35GridColumns = new GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem>( context, "K2BGridColumnsItem", "PACYE2");
         AV39AttributeName = "";
         AV40ColumnTitle = "";
         AV36GridColumn = new SdtK2BGridColumns_K2BGridColumnsItem(context);
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aexportwwpersona__default(),
            new Object[][] {
                new Object[] {
               P002F2_A9PersonaFechaNacimiento, P002F2_n9PersonaFechaNacimiento, P002F2_A2PersonaPNombre, P002F2_A3PersonaSNombre, P002F2_n3PersonaSNombre, P002F2_A4PersonaPApellido, P002F2_A5PersonaSApellido, P002F2_n5PersonaSApellido, P002F2_A6PersonaSexo, P002F2_n6PersonaSexo,
               P002F2_A7PersonaDPI, P002F2_n7PersonaDPI, P002F2_A8PersonaTipo, P002F2_A10PersonaPacienteOrigenSordera, P002F2_n10PersonaPacienteOrigenSordera, P002F2_A1PersonaID
               }
            }
         );
         AV43Pgmname = "AExportWWPersona";
         /* GeneXus formulas. */
         AV43Pgmname = "AExportWWPersona";
         context.Gx_err = 0;
      }

      private short gxcookieaux ;
      private short nGotPars ;
      private short AV9OrderedBy ;
      private short GxWebError ;
      private short AV21ColumnIndex ;
      private short A8PersonaTipo ;
      private short A10PersonaPacienteOrigenSordera ;
      private short AV38CurrentColumn ;
      private int AV20Random ;
      private int AV18CellRow ;
      private int AV19FirstColumn ;
      private int AV45GXV1 ;
      private String GXKey ;
      private String gxfirstwebparm ;
      private String AV43Pgmname ;
      private String scmdbuf ;
      private String A6PersonaSexo ;
      private String AV40ColumnTitle ;
      private DateTime GXt_dtime1 ;
      private DateTime AV16PersonaFechaNacimiento_From ;
      private DateTime AV17PersonaFechaNacimiento_To ;
      private DateTime A9PersonaFechaNacimiento ;
      private bool entryPointCalled ;
      private bool returnInSub ;
      private bool AV26GridColumnVisible_PersonaPNombre ;
      private bool AV27GridColumnVisible_PersonaSNombre ;
      private bool AV28GridColumnVisible_PersonaPApellido ;
      private bool AV29GridColumnVisible_PersonaSApellido ;
      private bool AV30GridColumnVisible_PersonaSexo ;
      private bool AV31GridColumnVisible_PersonaDPI ;
      private bool AV32GridColumnVisible_PersonaTipo ;
      private bool AV33GridColumnVisible_PersonaFechaNacimiento ;
      private bool AV34GridColumnVisible_PersonaPacienteOrigenSordera ;
      private bool n9PersonaFechaNacimiento ;
      private bool n3PersonaSNombre ;
      private bool n5PersonaSApellido ;
      private bool n6PersonaSexo ;
      private bool n7PersonaDPI ;
      private bool n10PersonaPacienteOrigenSordera ;
      private bool AV25GridColumnVisible_PersonaID ;
      private bool AV37FoundColumn ;
      private String AV14PersonaPNombre ;
      private String AV11Filename ;
      private String AV12ErrorMessage ;
      private String lV14PersonaPNombre ;
      private String A2PersonaPNombre ;
      private String A3PersonaSNombre ;
      private String A4PersonaPApellido ;
      private String A5PersonaSApellido ;
      private String A7PersonaDPI ;
      private String AV39AttributeName ;
      private Guid A1PersonaID ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private DateTime[] P002F2_A9PersonaFechaNacimiento ;
      private bool[] P002F2_n9PersonaFechaNacimiento ;
      private String[] P002F2_A2PersonaPNombre ;
      private String[] P002F2_A3PersonaSNombre ;
      private bool[] P002F2_n3PersonaSNombre ;
      private String[] P002F2_A4PersonaPApellido ;
      private String[] P002F2_A5PersonaSApellido ;
      private bool[] P002F2_n5PersonaSApellido ;
      private String[] P002F2_A6PersonaSexo ;
      private bool[] P002F2_n6PersonaSexo ;
      private String[] P002F2_A7PersonaDPI ;
      private bool[] P002F2_n7PersonaDPI ;
      private short[] P002F2_A8PersonaTipo ;
      private short[] P002F2_A10PersonaPacienteOrigenSordera ;
      private bool[] P002F2_n10PersonaPacienteOrigenSordera ;
      private Guid[] P002F2_A1PersonaID ;
      private GxHttpResponse AV22HttpResponse ;
      private ExcelDocumentI AV13ExcelDocument ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> AV35GridColumns ;
      private GxFile AV24File ;
      private SdtK2BContext AV8Context ;
      private SdtK2BGridColumns_K2BGridColumnsItem AV36GridColumn ;
   }

   public class aexportwwpersona__default : DataStoreHelperBase, IDataStoreHelper
   {
      protected Object[] conditional_P002F2( IGxContext context ,
                                             String AV14PersonaPNombre ,
                                             DateTime AV17PersonaFechaNacimiento_To ,
                                             DateTime AV16PersonaFechaNacimiento_From ,
                                             String A2PersonaPNombre ,
                                             DateTime A9PersonaFechaNacimiento ,
                                             short AV9OrderedBy )
      {
         String sWhereString = "" ;
         String scmdbuf ;
         short[] GXv_int2 ;
         GXv_int2 = new short [3] ;
         Object[] GXv_Object3 ;
         GXv_Object3 = new Object [2] ;
         scmdbuf = "SELECT [PersonaFechaNacimiento], [PersonaPNombre], [PersonaSNombre], [PersonaPApellido], [PersonaSApellido], [PersonaSexo], [PersonaDPI], [PersonaTipo], [PersonaPacienteOrigenSordera], [PersonaID] FROM [Persona] WITH (NOLOCK)";
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14PersonaPNombre)) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PersonaPNombre] like @lV14PersonaPNombre)";
            }
            else
            {
               sWhereString = sWhereString + " ([PersonaPNombre] like @lV14PersonaPNombre)";
            }
         }
         else
         {
            GXv_int2[0] = 1;
         }
         if ( ! (DateTime.MinValue==AV17PersonaFechaNacimiento_To) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PersonaFechaNacimiento] <= @AV17PersonaFechaNacimiento_To)";
            }
            else
            {
               sWhereString = sWhereString + " ([PersonaFechaNacimiento] <= @AV17PersonaFechaNacimiento_To)";
            }
         }
         else
         {
            GXv_int2[1] = 1;
         }
         if ( ! (DateTime.MinValue==AV16PersonaFechaNacimiento_From) )
         {
            if ( StringUtil.StrCmp("", sWhereString) != 0 )
            {
               sWhereString = sWhereString + " and ([PersonaFechaNacimiento] >= @AV16PersonaFechaNacimiento_From)";
            }
            else
            {
               sWhereString = sWhereString + " ([PersonaFechaNacimiento] >= @AV16PersonaFechaNacimiento_From)";
            }
         }
         else
         {
            GXv_int2[2] = 1;
         }
         if ( StringUtil.StrCmp("", sWhereString) != 0 )
         {
            scmdbuf = scmdbuf + " WHERE" + sWhereString;
         }
         if ( AV9OrderedBy == 0 )
         {
            scmdbuf = scmdbuf + " ORDER BY [PersonaID]";
         }
         else if ( AV9OrderedBy == 1 )
         {
            scmdbuf = scmdbuf + " ORDER BY [PersonaID] DESC";
         }
         else if ( AV9OrderedBy == 2 )
         {
            scmdbuf = scmdbuf + " ORDER BY [PersonaPNombre]";
         }
         else if ( AV9OrderedBy == 3 )
         {
            scmdbuf = scmdbuf + " ORDER BY [PersonaPNombre] DESC";
         }
         GXv_Object3[0] = scmdbuf;
         GXv_Object3[1] = GXv_int2;
         return GXv_Object3 ;
      }

      public override Object [] getDynamicStatement( int cursor ,
                                                     IGxContext context ,
                                                     Object [] dynConstraints )
      {
         switch ( cursor )
         {
               case 0 :
                     return conditional_P002F2(context, (String)dynConstraints[0] , (DateTime)dynConstraints[1] , (DateTime)dynConstraints[2] , (String)dynConstraints[3] , (DateTime)dynConstraints[4] , (short)dynConstraints[5] );
         }
         return base.getDynamicStatement(cursor, context, dynConstraints);
      }

      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmP002F2 ;
          prmP002F2 = new Object[] {
          new Object[] {"@lV14PersonaPNombre",SqlDbType.VarChar,40,0} ,
          new Object[] {"@AV17PersonaFechaNacimiento_To",SqlDbType.DateTime,8,0} ,
          new Object[] {"@AV16PersonaFechaNacimiento_From",SqlDbType.DateTime,8,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("P002F2", "scmdbuf",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002F2,100,0,false,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((DateTime[]) buf[0])[0] = rslt.getGXDate(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
                ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
                ((bool[]) buf[4])[0] = rslt.wasNull(3);
                ((String[]) buf[5])[0] = rslt.getVarchar(4) ;
                ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
                ((bool[]) buf[7])[0] = rslt.wasNull(5);
                ((String[]) buf[8])[0] = rslt.getString(6, 1) ;
                ((bool[]) buf[9])[0] = rslt.wasNull(6);
                ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
                ((bool[]) buf[11])[0] = rslt.wasNull(7);
                ((short[]) buf[12])[0] = rslt.getShort(8) ;
                ((short[]) buf[13])[0] = rslt.getShort(9) ;
                ((bool[]) buf[14])[0] = rslt.wasNull(9);
                ((Guid[]) buf[15])[0] = rslt.getGuid(10) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       short sIdx ;
       switch ( cursor )
       {
             case 0 :
                sIdx = 0;
                if ( (short)parms[0] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (String)parms[3]);
                }
                if ( (short)parms[1] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[4]);
                }
                if ( (short)parms[2] == 0 )
                {
                   sIdx = (short)(sIdx+1);
                   stmt.SetParameter(sIdx, (DateTime)parms[5]);
                }
                return;
       }
    }

 }

}
