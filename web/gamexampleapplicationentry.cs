/*
               File: GAMExampleApplicationEntry
        Description: Application
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:3:51.65
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class gamexampleapplicationentry : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public gamexampleapplicationentry( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("Carmine");
      }

      public gamexampleapplicationentry( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Gx_mode ,
                           ref long aP1_Id )
      {
         this.Gx_mode = aP0_Gx_mode;
         this.AV34Id = aP1_Id;
         executePrivate();
         aP0_Gx_mode=this.Gx_mode;
         aP1_Id=this.AV34Id;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         chkavUseabsoluteurlbyenvironment = new GXCheckbox();
         cmbavMainmenu = new GXCombobox();
         chkavAccessrequirespermission = new GXCheckbox();
         chkavClientaccessuniquebyuser = new GXCheckbox();
         chkavClientallowremoteauth = new GXCheckbox();
         chkavClientallowgetuserroles = new GXCheckbox();
         chkavClientallowgetuseradddata = new GXCheckbox();
         chkavEnvironmentsecureprotocol = new GXCheckbox();
         chkavAutoregisteranomymoususer = new GXCheckbox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               Gx_mode = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") != 0 )
               {
                  AV34Id = (long)(NumberUtil.Val( GetNextPar( ), "."));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Id), 12, 0)));
               }
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "gamexampleapplicationentry_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("gammasterpage", "GeneXus.Programs.gammasterpage", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA2G2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START2G2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171535190", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManager.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/json2005.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/rsh.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManagerCreate.js", "", false);
         context.AddJavascriptSource("Tab/TabRender.js", "", false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("gamexampleapplicationentry.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV34Id)+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         GxWebStd.gx_hidden_field( context, "TAB2_Class", StringUtil.RTrim( Tab2_Class));
         GxWebStd.gx_hidden_field( context, "TAB2_Pagecount", StringUtil.LTrim( StringUtil.NToC( (decimal)(Tab2_Pagecount), 9, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "TAB2_Historymanagement", StringUtil.BoolToStr( Tab2_Historymanagement));
         GxWebStd.gx_hidden_field( context, "ACTIONSCONTAINER_Class", StringUtil.RTrim( divActionscontainer_Class));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE2G2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT2G2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("gamexampleapplicationentry.aspx") + "?" + UrlEncode(StringUtil.RTrim(Gx_mode)) + "," + UrlEncode("" +AV34Id) ;
      }

      public override String GetPgmname( )
      {
         return "GAMExampleApplicationEntry" ;
      }

      public override String GetPgmdesc( )
      {
         return "Application" ;
      }

      protected void WB2G0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "BodyContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-10 col-sm-offset-1 col-lg-8", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable3_Internalname, 1, 0, "px", 0, "px", "TableTop", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-7 col-sm-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTextblock2_Internalname, "Application", "", "", lblTextblock2_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-5 col-sm-12 hidden-sm hidden-md hidden-lg", "Right", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 11,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttShowhide_Internalname, "", "Actions", bttShowhide_Jsonclick, 7, "Actions", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"e112g1_client"+"'", TempTags, "", 2, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "Right", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-3 col-sm-push-9", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divActionscontainer_Internalname, divActionscontainer_Visible, 0, "px", 0, "px", divActionscontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable6_Internalname, 1, 0, "px", 0, "px", "ActionsContainerItem", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "baac128e-62aa-4a75-8034-3bc742d1fc6c", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImageprm_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" ", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-10", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPermissions_Internalname, "Permissions", "", "", lblPermissions_Jsonclick, "'"+""+"'"+",false,"+"'"+"e122g1_client"+"'", "", "ActionText TextLikeLink", 7, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable4_Internalname, 1, 0, "px", 0, "px", "ActionsContainerItem", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "c6f2be06-b81b-4875-8b52-aa40300ba5b9", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImagemenu_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" ", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-10", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMenus_Internalname, "Menus", "", "", lblMenus_Jsonclick, "'"+""+"'"+",false,"+"'"+"e132g1_client"+"'", "", "ActionText TextLikeLink", 7, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable7_Internalname, 1, 0, "px", 0, "px", "ActionsContainerItem", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = imgImagerevoke_Bitmap;
            GxWebStd.gx_bitmap( context, imgImagerevoke_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" ", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-10", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblApplicationstatus_Internalname, lblApplicationstatus_Caption, "", "", lblApplicationstatus_Jsonclick, "'"+""+"'"+",false,"+"'"+"E\\'REVOKE-AUTHORIZE\\'."+"'", "", "ActionText TextLikeLink", 5, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTable5_Internalname, 1, 0, "px", 0, "px", "ActionsContainerItem", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-2", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "014fc44a-5df4-4dab-a62f-c70059b92e11", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImagedlt_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" ", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-10", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblDeleteapp_Internalname, "Delete Application", "", "", lblDeleteapp_Jsonclick, "'"+""+"'"+",false,"+"'"+"e142g1_client"+"'", "", "ActionText TextLikeLink", 7, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 col-sm-offset-1 col-sm-pull-3", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"TAB2Container"+"\"></div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"title1"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGeneral_title_Internalname, "General", "", "", lblGeneral_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "General") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"panel1"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabpage1table_Internalname, 1, 0, "px", 0, "px", "TabsFormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavId_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavId_Internalname, "Id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtavId_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34Id), 12, 0, ",", "")), ((edtavId_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(AV34Id), "ZZZZZZZZZZZ9")) : context.localUtil.Format( (decimal)(AV34Id), "ZZZZZZZZZZZ9")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavId_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavId_Enabled, 0, "text", "", 12, "chr", 1, "row", 12, 0, 0, 0, 1, -1, 0, true, "GAMKeyNumLong", "right", false, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavGuid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavGuid_Internalname, "GUID", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 66,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavGuid_Internalname, StringUtil.RTrim( AV32GUID), StringUtil.RTrim( context.localUtil.Format( AV32GUID, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,66);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavGuid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavGuid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavName_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavName_Internalname, "Name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 71,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavName_Internalname, StringUtil.RTrim( AV39Name), StringUtil.RTrim( context.localUtil.Format( AV39Name, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,71);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavName_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavName_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavDsc_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavDsc_Internalname, "Description", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 76,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavDsc_Internalname, StringUtil.RTrim( AV22Dsc), StringUtil.RTrim( context.localUtil.Format( AV22Dsc, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,76);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavDsc_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavDsc_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavVersion_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavVersion_Internalname, "Version", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 81,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavVersion_Internalname, StringUtil.RTrim( AV41Version), StringUtil.RTrim( context.localUtil.Format( AV41Version, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,81);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavVersion_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavVersion_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCompany_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCompany_Internalname, "Company", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 86,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCompany_Internalname, StringUtil.RTrim( AV20Company), StringUtil.RTrim( context.localUtil.Format( AV20Company, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,86);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCompany_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCompany_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavCopyright_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavCopyright_Internalname, "Copyright", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 91,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavCopyright_Internalname, StringUtil.RTrim( AV21Copyright), StringUtil.RTrim( context.localUtil.Format( AV21Copyright, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,91);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavCopyright_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavCopyright_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavUseabsoluteurlbyenvironment_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavUseabsoluteurlbyenvironment_Internalname, "Use absolute URL by Environment", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 96,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavUseabsoluteurlbyenvironment_Internalname, StringUtil.BoolToStr( AV43UseAbsoluteUrlByEnvironment), "", "Use absolute URL by Environment", 1, chkavUseabsoluteurlbyenvironment.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(96, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,96);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavHomeobject_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavHomeobject_Internalname, "Home Object", "col-sm-3 URLAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 101,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavHomeobject_Internalname, AV33HomeObject, StringUtil.RTrim( context.localUtil.Format( AV33HomeObject, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,101);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavHomeobject_Jsonclick, 0, "URLAttribute", "", "", "", "", 1, edtavHomeobject_Enabled, 1, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavLogoutobject_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavLogoutobject_Internalname, "Local Logout Object (specify an object or a URL)", "col-sm-3 URLAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 106,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavLogoutobject_Internalname, AV17LogoutObject, StringUtil.RTrim( context.localUtil.Format( AV17LogoutObject, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,106);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavLogoutobject_Jsonclick, 0, "URLAttribute", "", "", "", "", 1, edtavLogoutobject_Enabled, 1, "text", "", 80, "chr", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbavMainmenu_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavMainmenu_Internalname, "Main Menu", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 111,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavMainmenu, cmbavMainmenu_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0)), 1, cmbavMainmenu_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbavMainmenu.Enabled, 1, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,111);\"", "", true, "HLP_GAMExampleApplicationEntry.htm");
            cmbavMainmenu.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMainmenu_Internalname, "Values", (String)(cmbavMainmenu.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellSimple", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavAccessrequirespermission_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavAccessrequirespermission_Internalname, "Requires Permissions?", "col-sm-3 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 116,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAccessrequirespermission_Internalname, StringUtil.BoolToStr( AV5AccessRequiresPermission), "", "Requires Permissions?", 1, chkavAccessrequirespermission.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(116, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,116);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"title2"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblClientdata_title_Internalname, "Client Application Data", "", "", lblClientdata_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "ClientData") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"panel2"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabpage2table_Internalname, 1, 0, "px", 0, "px", "TabsFormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientid_Internalname, "Client Id", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 126,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientid_Internalname, StringUtil.RTrim( AV14ClientId), StringUtil.RTrim( context.localUtil.Format( AV14ClientId, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,126);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavClientid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMClientApplicationId", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientsecret_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientsecret_Internalname, "Client Secret", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 131,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientsecret_Internalname, StringUtil.RTrim( AV19ClientSecret), StringUtil.RTrim( context.localUtil.Format( AV19ClientSecret, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,131);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientsecret_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavClientsecret_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, 0, true, "GAMClientApplicationSecret", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavClientaccessuniquebyuser_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavClientaccessuniquebyuser_Internalname, "Single user access?", "col-sm-3 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 136,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientaccessuniquebyuser_Internalname, StringUtil.BoolToStr( AV8ClientAccessUniqueByUser), "", "Single user access?", 1, chkavClientaccessuniquebyuser.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(136, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,136);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellSimple", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientrevoked_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientrevoked_Internalname, "Revoked", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 141,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtavClientrevoked_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtavClientrevoked_Internalname, context.localUtil.TToC( AV18ClientRevoked, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( AV18ClientRevoked, "99/99/9999 99:99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 10,'DMY',5,24,'spa',false,0);"+";gx.evt.onblur(this,141);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientrevoked_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavClientrevoked_Enabled, 1, "text", "", 16, "chr", 1, "row", 16, 0, 0, 0, 1, -1, 0, true, "GAMDateTime", "right", false, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_bitmap( context, edtavClientrevoked_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtavClientrevoked_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_GAMExampleApplicationEntry.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"title3"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTabpage3_title_Internalname, "Remote Authentication", "", "", lblTabpage3_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "TabPage3") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"panel3"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabpage3table_Internalname, 1, 0, "px", 0, "px", "TabsFormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divAllowcell_Internalname, 1, 0, "px", 0, "px", divAllowcell_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavClientallowremoteauth_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavClientallowremoteauth_Internalname, "Allow remote authentication?", "col-sm-3 col-lg-4 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 col-lg-8 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 151,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientallowremoteauth_Internalname, StringUtil.BoolToStr( AV11ClientAllowRemoteAuth), "", "Allow remote authentication?", 1, chkavClientallowremoteauth.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onblur=\""+""+";gx.evt.onblur(this,151);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTblclient3_Internalname, divTblclient3_Visible, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavClientallowgetuserroles_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavClientallowgetuserroles_Internalname, "Can get user roles?", "col-sm-4 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 159,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientallowgetuserroles_Internalname, StringUtil.BoolToStr( AV10ClientAllowGetUserRoles), "", "Can get user roles?", 1, chkavClientallowgetuserroles.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(159, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,159);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavClientallowgetuseradddata_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavClientallowgetuseradddata_Internalname, "Can get user additional data?", "col-sm-4 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 164,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavClientallowgetuseradddata_Internalname, StringUtil.BoolToStr( AV9ClientAllowGetUserAddData), "", "Can get user additional data?", 1, chkavClientallowgetuseradddata.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(164, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,164);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientlocalloginurl_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientlocalloginurl_Internalname, "Local login URL", "col-sm-4 URLAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 169,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientlocalloginurl_Internalname, AV16ClientLocalLoginURL, StringUtil.RTrim( context.localUtil.Format( AV16ClientLocalLoginURL, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,169);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientlocalloginurl_Jsonclick, 0, "URLAttribute", "", "", "", "", 1, edtavClientlocalloginurl_Enabled, 1, "text", "", 80, "%", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientcallbackurl_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientcallbackurl_Internalname, "Callback URL", "col-sm-4 URLAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 174,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientcallbackurl_Internalname, AV12ClientCallbackURL, StringUtil.RTrim( context.localUtil.Format( AV12ClientCallbackURL, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,174);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientcallbackurl_Jsonclick, 0, "URLAttribute", "", "", "", "", 1, edtavClientcallbackurl_Enabled, 1, "text", "", 80, "%", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientimageurl_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientimageurl_Internalname, "Image URL", "col-sm-4 URLAttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 179,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientimageurl_Internalname, AV15ClientImageURL, StringUtil.RTrim( context.localUtil.Format( AV15ClientImageURL, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,179);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientimageurl_Jsonclick, 0, "URLAttribute", "", "", "", "", 1, edtavClientimageurl_Enabled, 1, "text", "", 80, "%", 1, "row", 2048, 0, 0, 0, 1, -1, 0, true, "GAMURL", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-8 FormCellSimple", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientencryptionkey_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientencryptionkey_Internalname, "Private encryption key", "col-sm-7 col-lg-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-5 col-lg-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 184,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientencryptionkey_Internalname, StringUtil.RTrim( AV13ClientEncryptionKey), StringUtil.RTrim( context.localUtil.Format( AV13ClientEncryptionKey, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,184);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientencryptionkey_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavClientencryptionkey_Enabled, 1, "text", "", 32, "chr", 1, "row", 32, 0, 0, 0, 1, -1, 0, true, "GAMEncryptionKey", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-4 FormCellSimple", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 186,'',false,'',0)\"";
            ClassString = "Button";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttGeneratekeygamremote_Internalname, "", "Generate Key GAMRemote", bttGeneratekeygamremote_Jsonclick, 5, "Generate Key GAMRemote", "", StyleString, ClassString, bttGeneratekeygamremote_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'GENERATEKEYGAMREMOTE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavClientrepositoryguid_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavClientrepositoryguid_Internalname, "Repository GUID", "col-sm-4 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-8 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 191,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavClientrepositoryguid_Internalname, StringUtil.RTrim( AV42ClientRepositoryGUID), StringUtil.RTrim( context.localUtil.Format( AV42ClientRepositoryGUID, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,191);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavClientrepositoryguid_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavClientrepositoryguid_Enabled, 1, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, 0, true, "GAMGUID", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"title4"+"\" style=\"display:none;\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblEnvironment_title_Internalname, "Environment Settings", "", "", lblEnvironment_title_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_GAMExampleApplicationEntry.htm");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", "", "display:none;", "div");
            context.WriteHtmlText( "Environment") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            context.WriteHtmlText( "<div class=\"gx_usercontrol_child\" id=\""+"TAB2Container"+"panel4"+"\" style=\"display:none;\">") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTabpage1table1_Internalname, 1, 0, "px", 0, "px", "TabsFormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavEnvironmentname_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavEnvironmentname_Internalname, "Name", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 201,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEnvironmentname_Internalname, StringUtil.RTrim( AV24EnvironmentName), StringUtil.RTrim( context.localUtil.Format( AV24EnvironmentName, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,201);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEnvironmentname_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavEnvironmentname_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+chkavEnvironmentsecureprotocol_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavEnvironmentsecureprotocol_Internalname, "Is HTTPS?", "col-sm-3 CheckBoxLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 206,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavEnvironmentsecureprotocol_Internalname, StringUtil.BoolToStr( AV28EnvironmentSecureProtocol), "", "Is HTTPS?", 1, chkavEnvironmentsecureprotocol.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(206, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,206);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavEnvironmenthost_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavEnvironmenthost_Internalname, "Host", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 211,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEnvironmenthost_Internalname, StringUtil.RTrim( AV23EnvironmentHost), StringUtil.RTrim( context.localUtil.Format( AV23EnvironmentHost, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,211);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEnvironmenthost_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavEnvironmenthost_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavEnvironmentport_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavEnvironmentport_Internalname, "Port", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 216,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEnvironmentport_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(AV25EnvironmentPort), 5, 0, ",", "")), StringUtil.LTrim( context.localUtil.Format( (decimal)(AV25EnvironmentPort), "ZZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,216);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEnvironmentport_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavEnvironmentport_Enabled, 1, "text", "", 5, "chr", 1, "row", 5, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavEnvironmentvirtualdirectory_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavEnvironmentvirtualdirectory_Internalname, "Virtual Directory", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 221,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEnvironmentvirtualdirectory_Internalname, StringUtil.RTrim( AV29EnvironmentVirtualDirectory), StringUtil.RTrim( context.localUtil.Format( AV29EnvironmentVirtualDirectory, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,221);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEnvironmentvirtualdirectory_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavEnvironmentvirtualdirectory_Enabled, 1, "text", "", 80, "chr", 1, "row", 120, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionMedium", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCell", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavEnvironmentprogrampackage_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavEnvironmentprogrampackage_Internalname, "Package", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 226,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEnvironmentprogrampackage_Internalname, StringUtil.RTrim( AV27EnvironmentProgramPackage), StringUtil.RTrim( context.localUtil.Format( AV27EnvironmentProgramPackage, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,226);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEnvironmentprogrampackage_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavEnvironmentprogrampackage_Enabled, 1, "text", "", 0, "px", 1, "row", 254, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionLong", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 FormCellSimple", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtavEnvironmentprogramextension_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavEnvironmentprogramextension_Internalname, "Extension", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 231,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavEnvironmentprogramextension_Internalname, StringUtil.RTrim( AV26EnvironmentProgramExtension), StringUtil.RTrim( context.localUtil.Format( AV26EnvironmentProgramExtension, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,231);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavEnvironmentprogramextension_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtavEnvironmentprogramextension_Enabled, 1, "text", "", 60, "chr", 1, "row", 60, 0, 0, 0, 1, -1, -1, true, "GAMDescriptionShort", "left", true, "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 236,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtncancel_Internalname, "", "Cancelar", bttBtncancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 238,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtnconfirm_Internalname, "", bttBtnconfirm_Caption, bttBtnconfirm_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtnconfirm_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_GAMExampleApplicationEntry.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "Center", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, chkavAutoregisteranomymoususer_Internalname, "Auto Register Anomymous User", "col-sm-3 CheckBoxLabel", 0, true);
            /* Check box */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 242,'',false,'',0)\"";
            ClassString = "CheckBox";
            StyleString = "";
            GxWebStd.gx_checkbox_ctrl( context, chkavAutoregisteranomymoususer_Internalname, StringUtil.BoolToStr( AV7AutoRegisterAnomymousUser), "", "Auto Register Anomymous User", chkavAutoregisteranomymoususer.Visible, chkavAutoregisteranomymoususer.Enabled, "true", "", StyleString, ClassString, "", "", TempTags+" onclick=\"gx.fn.checkboxClick(242, this, 'true', 'false');gx.evt.onchange(this, event);\" "+" onblur=\""+""+";gx.evt.onblur(this,242);\"");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "Center", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START2G2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Application", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP2G0( ) ;
      }

      protected void WS2G2( )
      {
         START2G2( ) ;
         EVT2G2( ) ;
      }

      protected void EVT2G2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Start */
                              E152G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              context.wbHandled = 1;
                              if ( ! wbErr )
                              {
                                 Rfr0gs = false;
                                 if ( ! Rfr0gs )
                                 {
                                    /* Execute user event: Enter */
                                    E162G2 ();
                                 }
                                 dynload_actions( ) ;
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'GENERATEKEYGAMREMOTE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'GenerateKeyGAMRemote' */
                              E172G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'REVOKE-AUTHORIZE'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'Revoke-Authorize' */
                              E182G2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: Load */
                              E192G2 ();
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE2G2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA2G2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            chkavUseabsoluteurlbyenvironment.Name = "vUSEABSOLUTEURLBYENVIRONMENT";
            chkavUseabsoluteurlbyenvironment.WebTags = "";
            chkavUseabsoluteurlbyenvironment.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUseabsoluteurlbyenvironment_Internalname, "TitleCaption", chkavUseabsoluteurlbyenvironment.Caption, true);
            chkavUseabsoluteurlbyenvironment.CheckedValue = "false";
            cmbavMainmenu.Name = "vMAINMENU";
            cmbavMainmenu.WebTags = "";
            if ( cmbavMainmenu.ItemCount > 0 )
            {
               AV36MainMenu = (long)(NumberUtil.Val( cmbavMainmenu.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36MainMenu", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0)));
            }
            chkavAccessrequirespermission.Name = "vACCESSREQUIRESPERMISSION";
            chkavAccessrequirespermission.WebTags = "";
            chkavAccessrequirespermission.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAccessrequirespermission_Internalname, "TitleCaption", chkavAccessrequirespermission.Caption, true);
            chkavAccessrequirespermission.CheckedValue = "false";
            chkavClientaccessuniquebyuser.Name = "vCLIENTACCESSUNIQUEBYUSER";
            chkavClientaccessuniquebyuser.WebTags = "";
            chkavClientaccessuniquebyuser.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientaccessuniquebyuser_Internalname, "TitleCaption", chkavClientaccessuniquebyuser.Caption, true);
            chkavClientaccessuniquebyuser.CheckedValue = "false";
            chkavClientallowremoteauth.Name = "vCLIENTALLOWREMOTEAUTH";
            chkavClientallowremoteauth.WebTags = "";
            chkavClientallowremoteauth.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowremoteauth_Internalname, "TitleCaption", chkavClientallowremoteauth.Caption, true);
            chkavClientallowremoteauth.CheckedValue = "false";
            chkavClientallowgetuserroles.Name = "vCLIENTALLOWGETUSERROLES";
            chkavClientallowgetuserroles.WebTags = "";
            chkavClientallowgetuserroles.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuserroles_Internalname, "TitleCaption", chkavClientallowgetuserroles.Caption, true);
            chkavClientallowgetuserroles.CheckedValue = "false";
            chkavClientallowgetuseradddata.Name = "vCLIENTALLOWGETUSERADDDATA";
            chkavClientallowgetuseradddata.WebTags = "";
            chkavClientallowgetuseradddata.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuseradddata_Internalname, "TitleCaption", chkavClientallowgetuseradddata.Caption, true);
            chkavClientallowgetuseradddata.CheckedValue = "false";
            chkavEnvironmentsecureprotocol.Name = "vENVIRONMENTSECUREPROTOCOL";
            chkavEnvironmentsecureprotocol.WebTags = "";
            chkavEnvironmentsecureprotocol.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavEnvironmentsecureprotocol_Internalname, "TitleCaption", chkavEnvironmentsecureprotocol.Caption, true);
            chkavEnvironmentsecureprotocol.CheckedValue = "false";
            chkavAutoregisteranomymoususer.Name = "vAUTOREGISTERANOMYMOUSUSER";
            chkavAutoregisteranomymoususer.WebTags = "";
            chkavAutoregisteranomymoususer.Caption = "";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAutoregisteranomymoususer_Internalname, "TitleCaption", chkavAutoregisteranomymoususer.Caption, true);
            chkavAutoregisteranomymoususer.CheckedValue = "false";
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavGuid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavMainmenu.ItemCount > 0 )
         {
            AV36MainMenu = (long)(NumberUtil.Val( cmbavMainmenu.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36MainMenu", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavMainmenu.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMainmenu_Internalname, "Values", cmbavMainmenu.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF2G2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)), true);
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)), true);
         edtavClientrevoked_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientrevoked_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientrevoked_Enabled), 5, 0)), true);
      }

      protected void RF2G2( )
      {
         initialize_formulas( ) ;
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E192G2 ();
            WB2G0( ) ;
         }
      }

      protected void send_integrity_lvl_hashes2G2( )
      {
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
      }

      protected void STRUP2G0( )
      {
         /* Before Start, stand alone formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavId_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavId_Enabled), 5, 0)), true);
         edtavGuid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)), true);
         edtavClientrevoked_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientrevoked_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientrevoked_Enabled), 5, 0)), true);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E152G2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV34Id = (long)(context.localUtil.CToN( cgiGet( edtavId_Internalname), ",", "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Id), 12, 0)));
            AV32GUID = cgiGet( edtavGuid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32GUID", AV32GUID);
            AV39Name = cgiGet( edtavName_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Name", AV39Name);
            AV22Dsc = cgiGet( edtavDsc_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Dsc", AV22Dsc);
            AV41Version = cgiGet( edtavVersion_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Version", AV41Version);
            AV20Company = cgiGet( edtavCompany_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Company", AV20Company);
            AV21Copyright = cgiGet( edtavCopyright_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Copyright", AV21Copyright);
            AV43UseAbsoluteUrlByEnvironment = StringUtil.StrToBool( cgiGet( chkavUseabsoluteurlbyenvironment_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43UseAbsoluteUrlByEnvironment", AV43UseAbsoluteUrlByEnvironment);
            AV33HomeObject = cgiGet( edtavHomeobject_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33HomeObject", AV33HomeObject);
            AV17LogoutObject = cgiGet( edtavLogoutobject_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LogoutObject", AV17LogoutObject);
            cmbavMainmenu.CurrentValue = cgiGet( cmbavMainmenu_Internalname);
            AV36MainMenu = (long)(NumberUtil.Val( cgiGet( cmbavMainmenu_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36MainMenu", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0)));
            AV5AccessRequiresPermission = StringUtil.StrToBool( cgiGet( chkavAccessrequirespermission_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AccessRequiresPermission", AV5AccessRequiresPermission);
            AV14ClientId = cgiGet( edtavClientid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientId", AV14ClientId);
            AV19ClientSecret = cgiGet( edtavClientsecret_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ClientSecret", AV19ClientSecret);
            AV8ClientAccessUniqueByUser = StringUtil.StrToBool( cgiGet( chkavClientaccessuniquebyuser_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ClientAccessUniqueByUser", AV8ClientAccessUniqueByUser);
            if ( context.localUtil.VCDateTime( cgiGet( edtavClientrevoked_Internalname), 0, 0) == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Client Revoked"}), 1, "vCLIENTREVOKED");
               GX_FocusControl = edtavClientrevoked_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV18ClientRevoked = (DateTime)(DateTime.MinValue);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ClientRevoked", context.localUtil.TToC( AV18ClientRevoked, 10, 5, 0, 3, "/", ":", " "));
            }
            else
            {
               AV18ClientRevoked = context.localUtil.CToT( cgiGet( edtavClientrevoked_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ClientRevoked", context.localUtil.TToC( AV18ClientRevoked, 10, 5, 0, 3, "/", ":", " "));
            }
            AV11ClientAllowRemoteAuth = StringUtil.StrToBool( cgiGet( chkavClientallowremoteauth_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ClientAllowRemoteAuth", AV11ClientAllowRemoteAuth);
            AV10ClientAllowGetUserRoles = StringUtil.StrToBool( cgiGet( chkavClientallowgetuserroles_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ClientAllowGetUserRoles", AV10ClientAllowGetUserRoles);
            AV9ClientAllowGetUserAddData = StringUtil.StrToBool( cgiGet( chkavClientallowgetuseradddata_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ClientAllowGetUserAddData", AV9ClientAllowGetUserAddData);
            AV16ClientLocalLoginURL = cgiGet( edtavClientlocalloginurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ClientLocalLoginURL", AV16ClientLocalLoginURL);
            AV12ClientCallbackURL = cgiGet( edtavClientcallbackurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ClientCallbackURL", AV12ClientCallbackURL);
            AV15ClientImageURL = cgiGet( edtavClientimageurl_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientImageURL", AV15ClientImageURL);
            AV13ClientEncryptionKey = cgiGet( edtavClientencryptionkey_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ClientEncryptionKey", AV13ClientEncryptionKey);
            AV42ClientRepositoryGUID = cgiGet( edtavClientrepositoryguid_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ClientRepositoryGUID", AV42ClientRepositoryGUID);
            AV24EnvironmentName = cgiGet( edtavEnvironmentname_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24EnvironmentName", AV24EnvironmentName);
            AV28EnvironmentSecureProtocol = StringUtil.StrToBool( cgiGet( chkavEnvironmentsecureprotocol_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28EnvironmentSecureProtocol", AV28EnvironmentSecureProtocol);
            AV23EnvironmentHost = cgiGet( edtavEnvironmenthost_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23EnvironmentHost", AV23EnvironmentHost);
            if ( ( ( context.localUtil.CToN( cgiGet( edtavEnvironmentport_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtavEnvironmentport_Internalname), ",", ".") > Convert.ToDecimal( 99999 )) ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "vENVIRONMENTPORT");
               GX_FocusControl = edtavEnvironmentport_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               wbErr = true;
               AV25EnvironmentPort = 0;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25EnvironmentPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25EnvironmentPort), 5, 0)));
            }
            else
            {
               AV25EnvironmentPort = (int)(context.localUtil.CToN( cgiGet( edtavEnvironmentport_Internalname), ",", "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25EnvironmentPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25EnvironmentPort), 5, 0)));
            }
            AV29EnvironmentVirtualDirectory = cgiGet( edtavEnvironmentvirtualdirectory_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29EnvironmentVirtualDirectory", AV29EnvironmentVirtualDirectory);
            AV27EnvironmentProgramPackage = cgiGet( edtavEnvironmentprogrampackage_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27EnvironmentProgramPackage", AV27EnvironmentProgramPackage);
            AV26EnvironmentProgramExtension = cgiGet( edtavEnvironmentprogramextension_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26EnvironmentProgramExtension", AV26EnvironmentProgramExtension);
            AV7AutoRegisterAnomymousUser = StringUtil.StrToBool( cgiGet( chkavAutoregisteranomymoususer_Internalname));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AutoRegisterAnomymousUser", AV7AutoRegisterAnomymousUser);
            /* Read saved values. */
            Tab2_Class = cgiGet( "TAB2_Class");
            Tab2_Pagecount = (int)(context.localUtil.CToN( cgiGet( "TAB2_Pagecount"), ",", "."));
            Tab2_Historymanagement = StringUtil.StrToBool( cgiGet( "TAB2_Historymanagement"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E152G2 ();
         if (returnInSub) return;
      }

      protected void E152G2( )
      {
         /* Start Routine */
         chkavAutoregisteranomymoususer.Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAutoregisteranomymoususer_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAutoregisteranomymoususer.Visible), 5, 0)), true);
         AV40User = new SdtGAMUser(context).get();
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            AV6Application.load( AV34Id);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Id), 12, 0)));
            AV34Id = AV6Application.gxTpr_Id;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Id), 12, 0)));
            AV32GUID = AV6Application.gxTpr_Guid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV32GUID", AV32GUID);
            AV39Name = AV6Application.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV39Name", AV39Name);
            AV22Dsc = AV6Application.gxTpr_Description;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV22Dsc", AV22Dsc);
            AV41Version = AV6Application.gxTpr_Version;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV41Version", AV41Version);
            AV21Copyright = AV6Application.gxTpr_Copyright;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV21Copyright", AV21Copyright);
            AV20Company = AV6Application.gxTpr_Companyname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV20Company", AV20Company);
            AV43UseAbsoluteUrlByEnvironment = AV6Application.gxTpr_Useabsoluteurlbyenvironment;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV43UseAbsoluteUrlByEnvironment", AV43UseAbsoluteUrlByEnvironment);
            AV33HomeObject = AV6Application.gxTpr_Homeobject;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33HomeObject", AV33HomeObject);
            AV17LogoutObject = AV6Application.gxTpr_Logoutobject;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV17LogoutObject", AV17LogoutObject);
            AV48GXV2 = 1;
            AV47GXV1 = AV6Application.getmenus(AV38MenuFilter, out  AV31Errors);
            while ( AV48GXV2 <= AV47GXV1.Count )
            {
               AV37Menu = ((SdtGAMApplicationMenu)AV47GXV1.Item(AV48GXV2));
               cmbavMainmenu.addItem(StringUtil.Trim( StringUtil.Str( (decimal)(AV37Menu.gxTpr_Id), 12, 0)), AV37Menu.gxTpr_Name, 0);
               AV48GXV2 = (int)(AV48GXV2+1);
            }
            AV36MainMenu = AV6Application.gxTpr_Mainmenuid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV36MainMenu", StringUtil.LTrim( StringUtil.Str( (decimal)(AV36MainMenu), 12, 0)));
            AV5AccessRequiresPermission = AV6Application.gxTpr_Accessrequirespermission;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV5AccessRequiresPermission", AV5AccessRequiresPermission);
            AV7AutoRegisterAnomymousUser = AV6Application.gxTpr_Clientautoregisteranomymoususer;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV7AutoRegisterAnomymousUser", AV7AutoRegisterAnomymousUser);
            AV14ClientId = AV6Application.gxTpr_Clientid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV14ClientId", AV14ClientId);
            AV19ClientSecret = AV6Application.gxTpr_Clientsecret;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV19ClientSecret", AV19ClientSecret);
            AV8ClientAccessUniqueByUser = AV6Application.gxTpr_Clientaccessuniquebyuser;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV8ClientAccessUniqueByUser", AV8ClientAccessUniqueByUser);
            AV18ClientRevoked = AV6Application.gxTpr_Clientrevoked;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV18ClientRevoked", context.localUtil.TToC( AV18ClientRevoked, 10, 5, 0, 3, "/", ":", " "));
            AV11ClientAllowRemoteAuth = AV6Application.gxTpr_Clientallowremoteauthentication;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV11ClientAllowRemoteAuth", AV11ClientAllowRemoteAuth);
            AV10ClientAllowGetUserRoles = AV6Application.gxTpr_Clientallowgetuserroles;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV10ClientAllowGetUserRoles", AV10ClientAllowGetUserRoles);
            AV9ClientAllowGetUserAddData = AV6Application.gxTpr_Clientallowgetuseradditionaldata;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV9ClientAllowGetUserAddData", AV9ClientAllowGetUserAddData);
            AV16ClientLocalLoginURL = AV6Application.gxTpr_Clientlocalloginurl;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV16ClientLocalLoginURL", AV16ClientLocalLoginURL);
            AV12ClientCallbackURL = AV6Application.gxTpr_Clientcallbackurl;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV12ClientCallbackURL", AV12ClientCallbackURL);
            AV15ClientImageURL = AV6Application.gxTpr_Clientimageurl;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV15ClientImageURL", AV15ClientImageURL);
            AV13ClientEncryptionKey = AV6Application.gxTpr_Clientencryptionkey;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ClientEncryptionKey", AV13ClientEncryptionKey);
            AV42ClientRepositoryGUID = AV6Application.gxTpr_Clientrepositoryguid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV42ClientRepositoryGUID", AV42ClientRepositoryGUID);
            AV24EnvironmentName = AV6Application.gxTpr_Environment.gxTpr_Name;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24EnvironmentName", AV24EnvironmentName);
            AV28EnvironmentSecureProtocol = AV6Application.gxTpr_Environment.gxTpr_Secureprotocol;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV28EnvironmentSecureProtocol", AV28EnvironmentSecureProtocol);
            AV23EnvironmentHost = AV6Application.gxTpr_Environment.gxTpr_Host;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV23EnvironmentHost", AV23EnvironmentHost);
            AV25EnvironmentPort = AV6Application.gxTpr_Environment.gxTpr_Port;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25EnvironmentPort", StringUtil.LTrim( StringUtil.Str( (decimal)(AV25EnvironmentPort), 5, 0)));
            AV29EnvironmentVirtualDirectory = AV6Application.gxTpr_Environment.gxTpr_Virtualdirectory;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29EnvironmentVirtualDirectory", AV29EnvironmentVirtualDirectory);
            AV27EnvironmentProgramPackage = AV6Application.gxTpr_Environment.gxTpr_Programpackage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV27EnvironmentProgramPackage", AV27EnvironmentProgramPackage);
            AV26EnvironmentProgramExtension = AV6Application.gxTpr_Environment.gxTpr_Programextension;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV26EnvironmentProgramExtension", AV26EnvironmentProgramExtension);
            if ( (DateTime.MinValue==AV6Application.gxTpr_Clientrevoked) )
            {
               lblApplicationstatus_Caption = "Revoke";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblApplicationstatus_Internalname, "Caption", lblApplicationstatus_Caption, true);
               imgImagerevoke_Bitmap = context.GetImagePath( "b423b6f7-ce34-4e3a-bcfe-788dddb2cbc6", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgImagerevoke_Bitmap)), true);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "SrcSet", context.GetImageSrcSet( imgImagerevoke_Bitmap), true);
            }
            else
            {
               lblApplicationstatus_Caption = "Authorize";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblApplicationstatus_Internalname, "Caption", lblApplicationstatus_Caption, true);
               imgImagerevoke_Bitmap = context.GetImagePath( "2ae17eb5-7b83-4853-9ade-265674418bc5", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgImagerevoke_Bitmap)), true);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "SrcSet", context.GetImageSrcSet( imgImagerevoke_Bitmap), true);
            }
         }
         else
         {
            divActionscontainer_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divActionscontainer_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divActionscontainer_Visible), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            edtavGuid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavGuid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavGuid_Enabled), 5, 0)), true);
            edtavName_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavName_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavName_Enabled), 5, 0)), true);
            edtavDsc_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavDsc_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavDsc_Enabled), 5, 0)), true);
            edtavVersion_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavVersion_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavVersion_Enabled), 5, 0)), true);
            edtavCopyright_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCopyright_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCopyright_Enabled), 5, 0)), true);
            edtavCompany_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavCompany_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavCompany_Enabled), 5, 0)), true);
            chkavUseabsoluteurlbyenvironment.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavUseabsoluteurlbyenvironment_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavUseabsoluteurlbyenvironment.Enabled), 5, 0)), true);
            edtavHomeobject_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavHomeobject_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavHomeobject_Enabled), 5, 0)), true);
            edtavLogoutobject_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavLogoutobject_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavLogoutobject_Enabled), 5, 0)), true);
            cmbavMainmenu.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavMainmenu_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbavMainmenu.Enabled), 5, 0)), true);
            chkavAccessrequirespermission.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavAccessrequirespermission_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavAccessrequirespermission.Enabled), 5, 0)), true);
            edtavClientid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientid_Enabled), 5, 0)), true);
            edtavClientsecret_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientsecret_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientsecret_Enabled), 5, 0)), true);
            chkavClientaccessuniquebyuser.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientaccessuniquebyuser_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientaccessuniquebyuser.Enabled), 5, 0)), true);
            edtavClientrevoked_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientrevoked_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientrevoked_Enabled), 5, 0)), true);
            chkavClientallowremoteauth.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowremoteauth_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientallowremoteauth.Enabled), 5, 0)), true);
            chkavClientallowgetuserroles.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuserroles_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientallowgetuserroles.Enabled), 5, 0)), true);
            chkavClientallowgetuseradddata.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavClientallowgetuseradddata_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavClientallowgetuseradddata.Enabled), 5, 0)), true);
            edtavClientlocalloginurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientlocalloginurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientlocalloginurl_Enabled), 5, 0)), true);
            edtavClientcallbackurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientcallbackurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientcallbackurl_Enabled), 5, 0)), true);
            edtavClientimageurl_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientimageurl_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientimageurl_Enabled), 5, 0)), true);
            edtavClientencryptionkey_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientencryptionkey_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientencryptionkey_Enabled), 5, 0)), true);
            edtavClientrepositoryguid_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavClientrepositoryguid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavClientrepositoryguid_Enabled), 5, 0)), true);
            edtavEnvironmentname_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEnvironmentname_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEnvironmentname_Enabled), 5, 0)), true);
            chkavEnvironmentsecureprotocol.Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, chkavEnvironmentsecureprotocol_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(chkavEnvironmentsecureprotocol.Enabled), 5, 0)), true);
            edtavEnvironmenthost_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEnvironmenthost_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEnvironmenthost_Enabled), 5, 0)), true);
            edtavEnvironmentport_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEnvironmentport_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEnvironmentport_Enabled), 5, 0)), true);
            edtavEnvironmentvirtualdirectory_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEnvironmentvirtualdirectory_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEnvironmentvirtualdirectory_Enabled), 5, 0)), true);
            edtavEnvironmentprogrampackage_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEnvironmentprogrampackage_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEnvironmentprogrampackage_Enabled), 5, 0)), true);
            edtavEnvironmentprogramextension_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavEnvironmentprogramextension_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavEnvironmentprogramextension_Enabled), 5, 0)), true);
            bttGeneratekeygamremote_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttGeneratekeygamremote_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttGeneratekeygamremote_Visible), 5, 0)), true);
            bttBtnconfirm_Caption = "Delete";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Caption", bttBtnconfirm_Caption, true);
         }
         divTblclient3_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblclient3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblclient3_Visible), 5, 0)), true);
         if ( AV11ClientAllowRemoteAuth )
         {
            divTblclient3_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, divTblclient3_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divTblclient3_Visible), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtnconfirm_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtnconfirm_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtnconfirm_Visible), 5, 0)), true);
         }
      }

      public void GXEnter( )
      {
         /* Execute user event: Enter */
         E162G2 ();
         if (returnInSub) return;
      }

      protected void E162G2( )
      {
         /* Enter Routine */
         AV6Application.load( AV34Id);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Id), 12, 0)));
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 ) )
         {
            AV6Application.gxTpr_Name = AV39Name;
            AV6Application.gxTpr_Description = AV22Dsc;
            AV6Application.gxTpr_Version = AV41Version;
            AV6Application.gxTpr_Copyright = AV21Copyright;
            AV6Application.gxTpr_Companyname = AV20Company;
            AV6Application.gxTpr_Useabsoluteurlbyenvironment = AV43UseAbsoluteUrlByEnvironment;
            AV6Application.gxTpr_Homeobject = AV33HomeObject;
            AV6Application.gxTpr_Logoutobject = AV17LogoutObject;
            AV6Application.gxTpr_Mainmenuid = AV36MainMenu;
            AV6Application.gxTpr_Accessrequirespermission = AV5AccessRequiresPermission;
            AV6Application.gxTpr_Clientautoregisteranomymoususer = AV7AutoRegisterAnomymousUser;
            AV6Application.gxTpr_Clientid = AV14ClientId;
            AV6Application.gxTpr_Clientsecret = AV19ClientSecret;
            AV6Application.gxTpr_Clientaccessuniquebyuser = AV8ClientAccessUniqueByUser;
            AV6Application.gxTpr_Clientallowremoteauthentication = AV11ClientAllowRemoteAuth;
            AV6Application.gxTpr_Clientallowgetuserroles = AV10ClientAllowGetUserRoles;
            AV6Application.gxTpr_Clientallowgetuseradditionaldata = AV9ClientAllowGetUserAddData;
            AV6Application.gxTpr_Clientlocalloginurl = AV16ClientLocalLoginURL;
            AV6Application.gxTpr_Clientcallbackurl = AV12ClientCallbackURL;
            AV6Application.gxTpr_Clientimageurl = AV15ClientImageURL;
            AV6Application.gxTpr_Clientencryptionkey = AV13ClientEncryptionKey;
            AV6Application.gxTpr_Clientrepositoryguid = AV42ClientRepositoryGUID;
            AV6Application.gxTpr_Environment.gxTpr_Name = AV24EnvironmentName;
            AV6Application.gxTpr_Environment.gxTpr_Secureprotocol = AV28EnvironmentSecureProtocol;
            AV6Application.gxTpr_Environment.gxTpr_Host = AV23EnvironmentHost;
            AV6Application.gxTpr_Environment.gxTpr_Port = AV25EnvironmentPort;
            AV6Application.gxTpr_Environment.gxTpr_Virtualdirectory = AV29EnvironmentVirtualDirectory;
            AV6Application.gxTpr_Environment.gxTpr_Programpackage = AV27EnvironmentProgramPackage;
            AV6Application.gxTpr_Environment.gxTpr_Programextension = AV26EnvironmentProgramExtension;
            AV6Application.save();
         }
         else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
         {
            AV6Application.delete();
         }
         if ( AV6Application.success() )
         {
            pr_gam.commit( "GAMExampleApplicationEntry");
            pr_default.commit( "GAMExampleApplicationEntry");
            CallWebObject(formatLink("gamexamplewwapplications.aspx") );
            context.wjLocDisableFrm = 1;
         }
         else
         {
            AV31Errors = AV6Application.geterrors();
            /* Execute user subroutine: 'ERRORS' */
            S112 ();
            if (returnInSub) return;
         }
         /*  Sending Event outputs  */
      }

      protected void S112( )
      {
         /* 'ERRORS' Routine */
         if ( AV31Errors.Count > 0 )
         {
            AV49GXV3 = 1;
            while ( AV49GXV3 <= AV31Errors.Count )
            {
               AV30Error = ((SdtGAMError)AV31Errors.Item(AV49GXV3));
               GX_msglist.addItem(StringUtil.Format( "%1 (GAM%2)", AV30Error.gxTpr_Message, StringUtil.LTrim( StringUtil.Str( (decimal)(AV30Error.gxTpr_Code), 12, 0)), "", "", "", "", "", "", ""));
               AV49GXV3 = (int)(AV49GXV3+1);
            }
         }
      }

      protected void E172G2( )
      {
         /* 'GenerateKeyGAMRemote' Routine */
         AV13ClientEncryptionKey = Crypto.GetEncryptionKey( );
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV13ClientEncryptionKey", AV13ClientEncryptionKey);
         /*  Sending Event outputs  */
      }

      protected void E182G2( )
      {
         /* 'Revoke-Authorize' Routine */
         AV6Application.load( AV34Id);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Id), 12, 0)));
         if ( (DateTime.MinValue==AV6Application.gxTpr_Clientrevoked) )
         {
            AV35isOk = AV6Application.revokeclient(out  AV31Errors);
         }
         else
         {
            AV35isOk = AV6Application.authorizeclient(out  AV31Errors);
         }
         if ( AV35isOk )
         {
            if ( (DateTime.MinValue==AV6Application.gxTpr_Clientrevoked) )
            {
               lblApplicationstatus_Caption = "Revoke";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblApplicationstatus_Internalname, "Caption", lblApplicationstatus_Caption, true);
               imgImagerevoke_Bitmap = context.GetImagePath( "b423b6f7-ce34-4e3a-bcfe-788dddb2cbc6", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgImagerevoke_Bitmap)), true);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "SrcSet", context.GetImageSrcSet( imgImagerevoke_Bitmap), true);
            }
            else
            {
               lblApplicationstatus_Caption = "Authorize";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblApplicationstatus_Internalname, "Caption", lblApplicationstatus_Caption, true);
               imgImagerevoke_Bitmap = context.GetImagePath( "2ae17eb5-7b83-4853-9ade-265674418bc5", "", context.GetTheme( ));
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "Bitmap", context.convertURL( context.PathToRelativeUrl( imgImagerevoke_Bitmap)), true);
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, imgImagerevoke_Internalname, "SrcSet", context.GetImageSrcSet( imgImagerevoke_Bitmap), true);
            }
            pr_gam.commit( "GAMExampleApplicationEntry");
            pr_default.commit( "GAMExampleApplicationEntry");
            context.DoAjaxRefresh();
         }
         else
         {
            /* Execute user subroutine: 'ERRORS' */
            S112 ();
            if (returnInSub) return;
         }
         /*  Sending Event outputs  */
      }

      protected void nextLoad( )
      {
      }

      protected void E192G2( )
      {
         /* Load Routine */
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         Gx_mode = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vMODE", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( Gx_mode, "@!")), context));
         AV34Id = Convert.ToInt64(getParm(obj,1));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34Id", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34Id), 12, 0)));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("Carmine");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA2G2( ) ;
         WS2G2( ) ;
         WE2G2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?20181117154330", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("gamexampleapplicationentry.js", "?20181117154331", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManager.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/json2005.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/rsh/rsh.js", "", false);
         context.AddJavascriptSource("Shared/HistoryManager/HistoryManagerCreate.js", "", false);
         context.AddJavascriptSource("Tab/TabRender.js", "", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTextblock2_Internalname = "TEXTBLOCK2";
         bttShowhide_Internalname = "SHOWHIDE";
         divTable3_Internalname = "TABLE3";
         imgImageprm_Internalname = "IMAGEPRM";
         lblPermissions_Internalname = "PERMISSIONS";
         divTable6_Internalname = "TABLE6";
         imgImagemenu_Internalname = "IMAGEMENU";
         lblMenus_Internalname = "MENUS";
         divTable4_Internalname = "TABLE4";
         imgImagerevoke_Internalname = "IMAGEREVOKE";
         lblApplicationstatus_Internalname = "APPLICATIONSTATUS";
         divTable7_Internalname = "TABLE7";
         imgImagedlt_Internalname = "IMAGEDLT";
         lblDeleteapp_Internalname = "DELETEAPP";
         divTable5_Internalname = "TABLE5";
         divActionscontainer_Internalname = "ACTIONSCONTAINER";
         lblGeneral_title_Internalname = "GENERAL_TITLE";
         edtavId_Internalname = "vID";
         edtavGuid_Internalname = "vGUID";
         edtavName_Internalname = "vNAME";
         edtavDsc_Internalname = "vDSC";
         edtavVersion_Internalname = "vVERSION";
         edtavCompany_Internalname = "vCOMPANY";
         edtavCopyright_Internalname = "vCOPYRIGHT";
         chkavUseabsoluteurlbyenvironment_Internalname = "vUSEABSOLUTEURLBYENVIRONMENT";
         edtavHomeobject_Internalname = "vHOMEOBJECT";
         edtavLogoutobject_Internalname = "vLOGOUTOBJECT";
         cmbavMainmenu_Internalname = "vMAINMENU";
         chkavAccessrequirespermission_Internalname = "vACCESSREQUIRESPERMISSION";
         divTabpage1table_Internalname = "TABPAGE1TABLE";
         lblClientdata_title_Internalname = "CLIENTDATA_TITLE";
         edtavClientid_Internalname = "vCLIENTID";
         edtavClientsecret_Internalname = "vCLIENTSECRET";
         chkavClientaccessuniquebyuser_Internalname = "vCLIENTACCESSUNIQUEBYUSER";
         edtavClientrevoked_Internalname = "vCLIENTREVOKED";
         divTabpage2table_Internalname = "TABPAGE2TABLE";
         lblTabpage3_title_Internalname = "TABPAGE3_TITLE";
         chkavClientallowremoteauth_Internalname = "vCLIENTALLOWREMOTEAUTH";
         divAllowcell_Internalname = "ALLOWCELL";
         chkavClientallowgetuserroles_Internalname = "vCLIENTALLOWGETUSERROLES";
         chkavClientallowgetuseradddata_Internalname = "vCLIENTALLOWGETUSERADDDATA";
         edtavClientlocalloginurl_Internalname = "vCLIENTLOCALLOGINURL";
         edtavClientcallbackurl_Internalname = "vCLIENTCALLBACKURL";
         edtavClientimageurl_Internalname = "vCLIENTIMAGEURL";
         edtavClientencryptionkey_Internalname = "vCLIENTENCRYPTIONKEY";
         bttGeneratekeygamremote_Internalname = "GENERATEKEYGAMREMOTE";
         edtavClientrepositoryguid_Internalname = "vCLIENTREPOSITORYGUID";
         divTblclient3_Internalname = "TBLCLIENT3";
         divTabpage3table_Internalname = "TABPAGE3TABLE";
         lblEnvironment_title_Internalname = "ENVIRONMENT_TITLE";
         edtavEnvironmentname_Internalname = "vENVIRONMENTNAME";
         chkavEnvironmentsecureprotocol_Internalname = "vENVIRONMENTSECUREPROTOCOL";
         edtavEnvironmenthost_Internalname = "vENVIRONMENTHOST";
         edtavEnvironmentport_Internalname = "vENVIRONMENTPORT";
         edtavEnvironmentvirtualdirectory_Internalname = "vENVIRONMENTVIRTUALDIRECTORY";
         edtavEnvironmentprogrampackage_Internalname = "vENVIRONMENTPROGRAMPACKAGE";
         edtavEnvironmentprogramextension_Internalname = "vENVIRONMENTPROGRAMEXTENSION";
         divTabpage1table1_Internalname = "TABPAGE1TABLE1";
         Tab2_Internalname = "TAB2";
         bttBtncancel_Internalname = "BTNCANCEL";
         bttBtnconfirm_Internalname = "BTNCONFIRM";
         chkavAutoregisteranomymoususer_Internalname = "vAUTOREGISTERANOMYMOUSUSER";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         chkavAutoregisteranomymoususer.Caption = "Auto Register Anomymous User";
         chkavEnvironmentsecureprotocol.Caption = "Is HTTPS?";
         chkavClientallowgetuseradddata.Caption = "Can get user additional data?";
         chkavClientallowgetuserroles.Caption = "Can get user roles?";
         chkavClientallowremoteauth.Caption = "Allow remote authentication?";
         chkavClientaccessuniquebyuser.Caption = "Single user access?";
         chkavAccessrequirespermission.Caption = "Requires Permissions?";
         chkavUseabsoluteurlbyenvironment.Caption = "Use absolute URL by Environment";
         chkavAutoregisteranomymoususer.Enabled = 1;
         chkavAutoregisteranomymoususer.Visible = 1;
         bttBtnconfirm_Caption = "Confirmar";
         bttBtnconfirm_Visible = 1;
         edtavEnvironmentprogramextension_Jsonclick = "";
         edtavEnvironmentprogramextension_Enabled = 1;
         edtavEnvironmentprogrampackage_Jsonclick = "";
         edtavEnvironmentprogrampackage_Enabled = 1;
         edtavEnvironmentvirtualdirectory_Jsonclick = "";
         edtavEnvironmentvirtualdirectory_Enabled = 1;
         edtavEnvironmentport_Jsonclick = "";
         edtavEnvironmentport_Enabled = 1;
         edtavEnvironmenthost_Jsonclick = "";
         edtavEnvironmenthost_Enabled = 1;
         chkavEnvironmentsecureprotocol.Enabled = 1;
         edtavEnvironmentname_Jsonclick = "";
         edtavEnvironmentname_Enabled = 1;
         edtavClientrepositoryguid_Jsonclick = "";
         edtavClientrepositoryguid_Enabled = 1;
         bttGeneratekeygamremote_Visible = 1;
         edtavClientencryptionkey_Jsonclick = "";
         edtavClientencryptionkey_Enabled = 1;
         edtavClientimageurl_Jsonclick = "";
         edtavClientimageurl_Enabled = 1;
         edtavClientcallbackurl_Jsonclick = "";
         edtavClientcallbackurl_Enabled = 1;
         edtavClientlocalloginurl_Jsonclick = "";
         edtavClientlocalloginurl_Enabled = 1;
         chkavClientallowgetuseradddata.Enabled = 1;
         chkavClientallowgetuserroles.Enabled = 1;
         divTblclient3_Visible = 1;
         chkavClientallowremoteauth.Enabled = 1;
         divAllowcell_Class = "col-xs-12 FormCellSimple";
         edtavClientrevoked_Jsonclick = "";
         edtavClientrevoked_Enabled = 1;
         chkavClientaccessuniquebyuser.Enabled = 1;
         edtavClientsecret_Jsonclick = "";
         edtavClientsecret_Enabled = 1;
         edtavClientid_Jsonclick = "";
         edtavClientid_Enabled = 1;
         chkavAccessrequirespermission.Enabled = 1;
         cmbavMainmenu_Jsonclick = "";
         cmbavMainmenu.Enabled = 1;
         edtavLogoutobject_Jsonclick = "";
         edtavLogoutobject_Enabled = 1;
         edtavHomeobject_Jsonclick = "";
         edtavHomeobject_Enabled = 1;
         chkavUseabsoluteurlbyenvironment.Enabled = 1;
         edtavCopyright_Jsonclick = "";
         edtavCopyright_Enabled = 1;
         edtavCompany_Jsonclick = "";
         edtavCompany_Enabled = 1;
         edtavVersion_Jsonclick = "";
         edtavVersion_Enabled = 1;
         edtavDsc_Jsonclick = "";
         edtavDsc_Enabled = 1;
         edtavName_Jsonclick = "";
         edtavName_Enabled = 1;
         edtavGuid_Jsonclick = "";
         edtavGuid_Enabled = 1;
         edtavId_Jsonclick = "";
         edtavId_Enabled = 0;
         lblApplicationstatus_Caption = "Revoke";
         imgImagerevoke_Bitmap = (String)(context.GetImagePath( "b423b6f7-ce34-4e3a-bcfe-788dddb2cbc6", "", context.GetTheme( )));
         lblMenus_Jsonclick = "";
         lblPermissions_Jsonclick = "";
         divActionscontainer_Visible = 1;
         divActionscontainer_Class = "ActionsContainer";
         Tab2_Historymanagement = Convert.ToBoolean( 0);
         Tab2_Pagecount = 4;
         Tab2_Class = "Tab";
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Application";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''}],oparms:[]}");
         setEventMetadata("ENTER","{handler:'E162G2',iparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0},{av:'Gx_mode',fld:'vMODE',pic:'@!',hsh:true,nv:''},{av:'AV39Name',fld:'vNAME',pic:'',nv:''},{av:'AV22Dsc',fld:'vDSC',pic:'',nv:''},{av:'AV41Version',fld:'vVERSION',pic:'',nv:''},{av:'AV21Copyright',fld:'vCOPYRIGHT',pic:'',nv:''},{av:'AV20Company',fld:'vCOMPANY',pic:'',nv:''},{av:'AV43UseAbsoluteUrlByEnvironment',fld:'vUSEABSOLUTEURLBYENVIRONMENT',pic:'',nv:false},{av:'AV33HomeObject',fld:'vHOMEOBJECT',pic:'',nv:''},{av:'AV17LogoutObject',fld:'vLOGOUTOBJECT',pic:'',nv:''},{av:'cmbavMainmenu'},{av:'AV36MainMenu',fld:'vMAINMENU',pic:'ZZZZZZZZZZZ9',nv:0},{av:'AV5AccessRequiresPermission',fld:'vACCESSREQUIRESPERMISSION',pic:'',nv:false},{av:'AV7AutoRegisterAnomymousUser',fld:'vAUTOREGISTERANOMYMOUSUSER',pic:'',nv:false},{av:'AV14ClientId',fld:'vCLIENTID',pic:'',nv:''},{av:'AV19ClientSecret',fld:'vCLIENTSECRET',pic:'',nv:''},{av:'AV8ClientAccessUniqueByUser',fld:'vCLIENTACCESSUNIQUEBYUSER',pic:'',nv:false},{av:'AV11ClientAllowRemoteAuth',fld:'vCLIENTALLOWREMOTEAUTH',pic:'',nv:false},{av:'AV10ClientAllowGetUserRoles',fld:'vCLIENTALLOWGETUSERROLES',pic:'',nv:false},{av:'AV9ClientAllowGetUserAddData',fld:'vCLIENTALLOWGETUSERADDDATA',pic:'',nv:false},{av:'AV16ClientLocalLoginURL',fld:'vCLIENTLOCALLOGINURL',pic:'',nv:''},{av:'AV12ClientCallbackURL',fld:'vCLIENTCALLBACKURL',pic:'',nv:''},{av:'AV15ClientImageURL',fld:'vCLIENTIMAGEURL',pic:'',nv:''},{av:'AV13ClientEncryptionKey',fld:'vCLIENTENCRYPTIONKEY',pic:'',nv:''},{av:'AV42ClientRepositoryGUID',fld:'vCLIENTREPOSITORYGUID',pic:'',nv:''},{av:'AV24EnvironmentName',fld:'vENVIRONMENTNAME',pic:'',nv:''},{av:'AV28EnvironmentSecureProtocol',fld:'vENVIRONMENTSECUREPROTOCOL',pic:'',nv:false},{av:'AV23EnvironmentHost',fld:'vENVIRONMENTHOST',pic:'',nv:''},{av:'AV25EnvironmentPort',fld:'vENVIRONMENTPORT',pic:'ZZZZ9',nv:0},{av:'AV29EnvironmentVirtualDirectory',fld:'vENVIRONMENTVIRTUALDIRECTORY',pic:'',nv:''},{av:'AV27EnvironmentProgramPackage',fld:'vENVIRONMENTPROGRAMPACKAGE',pic:'',nv:''},{av:'AV26EnvironmentProgramExtension',fld:'vENVIRONMENTPROGRAMEXTENSION',pic:'',nv:''}],oparms:[]}");
         setEventMetadata("'GENERATEKEYGAMREMOTE'","{handler:'E172G2',iparms:[],oparms:[{av:'AV13ClientEncryptionKey',fld:'vCLIENTENCRYPTIONKEY',pic:'',nv:''}]}");
         setEventMetadata("'REVOKE-AUTHORIZE'","{handler:'E182G2',iparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0}],oparms:[{av:'lblApplicationstatus_Caption',ctrl:'APPLICATIONSTATUS',prop:'Caption'}]}");
         setEventMetadata("'DELETE'","{handler:'E142G1',iparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0}],oparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'PERMISSIONS'","{handler:'E122G1',iparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0}],oparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'MENUS'","{handler:'E132G1',iparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0}],oparms:[{av:'AV34Id',fld:'vID',pic:'ZZZZZZZZZZZ9',nv:0}]}");
         setEventMetadata("'SHOWHIDE'","{handler:'E112G1',iparms:[{av:'divActionscontainer_Class',ctrl:'ACTIONSCONTAINER',prop:'Class'}],oparms:[{av:'divActionscontainer_Class',ctrl:'ACTIONSCONTAINER',prop:'Class'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOGx_mode = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         lblTextblock2_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttShowhide_Jsonclick = "";
         sImgUrl = "";
         lblApplicationstatus_Jsonclick = "";
         lblDeleteapp_Jsonclick = "";
         lblGeneral_title_Jsonclick = "";
         AV32GUID = "";
         AV39Name = "";
         AV22Dsc = "";
         AV41Version = "";
         AV20Company = "";
         AV21Copyright = "";
         AV33HomeObject = "";
         AV17LogoutObject = "";
         lblClientdata_title_Jsonclick = "";
         AV14ClientId = "";
         AV19ClientSecret = "";
         AV18ClientRevoked = (DateTime)(DateTime.MinValue);
         lblTabpage3_title_Jsonclick = "";
         AV16ClientLocalLoginURL = "";
         AV12ClientCallbackURL = "";
         AV15ClientImageURL = "";
         AV13ClientEncryptionKey = "";
         bttGeneratekeygamremote_Jsonclick = "";
         AV42ClientRepositoryGUID = "";
         lblEnvironment_title_Jsonclick = "";
         AV24EnvironmentName = "";
         AV23EnvironmentHost = "";
         AV29EnvironmentVirtualDirectory = "";
         AV27EnvironmentProgramPackage = "";
         AV26EnvironmentProgramExtension = "";
         bttBtncancel_Jsonclick = "";
         bttBtnconfirm_Jsonclick = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV40User = new SdtGAMUser(context);
         AV6Application = new SdtGAMApplication(context);
         AV47GXV1 = new GXExternalCollection<SdtGAMApplicationMenu>( context, "SdtGAMApplicationMenu", "GeneXus.Programs");
         AV38MenuFilter = new SdtGAMApplicationMenuFilter(context);
         AV31Errors = new GXExternalCollection<SdtGAMError>( context, "SdtGAMError", "GeneXus.Programs");
         AV37Menu = new SdtGAMApplicationMenu(context);
         AV30Error = new SdtGAMError(context);
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.gamexampleapplicationentry__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.gamexampleapplicationentry__default(),
            new Object[][] {
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
         edtavId_Enabled = 0;
         edtavGuid_Enabled = 0;
         edtavClientrevoked_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int Tab2_Pagecount ;
      private int divActionscontainer_Visible ;
      private int edtavId_Enabled ;
      private int edtavGuid_Enabled ;
      private int edtavName_Enabled ;
      private int edtavDsc_Enabled ;
      private int edtavVersion_Enabled ;
      private int edtavCompany_Enabled ;
      private int edtavCopyright_Enabled ;
      private int edtavHomeobject_Enabled ;
      private int edtavLogoutobject_Enabled ;
      private int edtavClientid_Enabled ;
      private int edtavClientsecret_Enabled ;
      private int edtavClientrevoked_Enabled ;
      private int divTblclient3_Visible ;
      private int edtavClientlocalloginurl_Enabled ;
      private int edtavClientcallbackurl_Enabled ;
      private int edtavClientimageurl_Enabled ;
      private int edtavClientencryptionkey_Enabled ;
      private int bttGeneratekeygamremote_Visible ;
      private int edtavClientrepositoryguid_Enabled ;
      private int edtavEnvironmentname_Enabled ;
      private int edtavEnvironmenthost_Enabled ;
      private int AV25EnvironmentPort ;
      private int edtavEnvironmentport_Enabled ;
      private int edtavEnvironmentvirtualdirectory_Enabled ;
      private int edtavEnvironmentprogrampackage_Enabled ;
      private int edtavEnvironmentprogramextension_Enabled ;
      private int bttBtnconfirm_Visible ;
      private int AV48GXV2 ;
      private int AV49GXV3 ;
      private int idxLst ;
      private long AV34Id ;
      private long wcpOAV34Id ;
      private long AV36MainMenu ;
      private String Gx_mode ;
      private String wcpOGx_mode ;
      private String divActionscontainer_Class ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String Tab2_Class ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String divTable3_Internalname ;
      private String lblTextblock2_Internalname ;
      private String lblTextblock2_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttShowhide_Internalname ;
      private String bttShowhide_Jsonclick ;
      private String divActionscontainer_Internalname ;
      private String divTable6_Internalname ;
      private String sImgUrl ;
      private String imgImageprm_Internalname ;
      private String lblPermissions_Internalname ;
      private String lblPermissions_Jsonclick ;
      private String divTable4_Internalname ;
      private String imgImagemenu_Internalname ;
      private String lblMenus_Internalname ;
      private String lblMenus_Jsonclick ;
      private String divTable7_Internalname ;
      private String imgImagerevoke_Internalname ;
      private String lblApplicationstatus_Internalname ;
      private String lblApplicationstatus_Caption ;
      private String lblApplicationstatus_Jsonclick ;
      private String divTable5_Internalname ;
      private String imgImagedlt_Internalname ;
      private String lblDeleteapp_Internalname ;
      private String lblDeleteapp_Jsonclick ;
      private String lblGeneral_title_Internalname ;
      private String lblGeneral_title_Jsonclick ;
      private String divTabpage1table_Internalname ;
      private String edtavId_Internalname ;
      private String edtavId_Jsonclick ;
      private String edtavGuid_Internalname ;
      private String AV32GUID ;
      private String edtavGuid_Jsonclick ;
      private String edtavName_Internalname ;
      private String AV39Name ;
      private String edtavName_Jsonclick ;
      private String edtavDsc_Internalname ;
      private String AV22Dsc ;
      private String edtavDsc_Jsonclick ;
      private String edtavVersion_Internalname ;
      private String AV41Version ;
      private String edtavVersion_Jsonclick ;
      private String edtavCompany_Internalname ;
      private String AV20Company ;
      private String edtavCompany_Jsonclick ;
      private String edtavCopyright_Internalname ;
      private String AV21Copyright ;
      private String edtavCopyright_Jsonclick ;
      private String chkavUseabsoluteurlbyenvironment_Internalname ;
      private String edtavHomeobject_Internalname ;
      private String edtavHomeobject_Jsonclick ;
      private String edtavLogoutobject_Internalname ;
      private String edtavLogoutobject_Jsonclick ;
      private String cmbavMainmenu_Internalname ;
      private String cmbavMainmenu_Jsonclick ;
      private String chkavAccessrequirespermission_Internalname ;
      private String lblClientdata_title_Internalname ;
      private String lblClientdata_title_Jsonclick ;
      private String divTabpage2table_Internalname ;
      private String edtavClientid_Internalname ;
      private String AV14ClientId ;
      private String edtavClientid_Jsonclick ;
      private String edtavClientsecret_Internalname ;
      private String AV19ClientSecret ;
      private String edtavClientsecret_Jsonclick ;
      private String chkavClientaccessuniquebyuser_Internalname ;
      private String edtavClientrevoked_Internalname ;
      private String edtavClientrevoked_Jsonclick ;
      private String lblTabpage3_title_Internalname ;
      private String lblTabpage3_title_Jsonclick ;
      private String divTabpage3table_Internalname ;
      private String divAllowcell_Internalname ;
      private String divAllowcell_Class ;
      private String chkavClientallowremoteauth_Internalname ;
      private String divTblclient3_Internalname ;
      private String chkavClientallowgetuserroles_Internalname ;
      private String chkavClientallowgetuseradddata_Internalname ;
      private String edtavClientlocalloginurl_Internalname ;
      private String edtavClientlocalloginurl_Jsonclick ;
      private String edtavClientcallbackurl_Internalname ;
      private String edtavClientcallbackurl_Jsonclick ;
      private String edtavClientimageurl_Internalname ;
      private String edtavClientimageurl_Jsonclick ;
      private String edtavClientencryptionkey_Internalname ;
      private String AV13ClientEncryptionKey ;
      private String edtavClientencryptionkey_Jsonclick ;
      private String bttGeneratekeygamremote_Internalname ;
      private String bttGeneratekeygamremote_Jsonclick ;
      private String edtavClientrepositoryguid_Internalname ;
      private String AV42ClientRepositoryGUID ;
      private String edtavClientrepositoryguid_Jsonclick ;
      private String lblEnvironment_title_Internalname ;
      private String lblEnvironment_title_Jsonclick ;
      private String divTabpage1table1_Internalname ;
      private String edtavEnvironmentname_Internalname ;
      private String AV24EnvironmentName ;
      private String edtavEnvironmentname_Jsonclick ;
      private String chkavEnvironmentsecureprotocol_Internalname ;
      private String edtavEnvironmenthost_Internalname ;
      private String AV23EnvironmentHost ;
      private String edtavEnvironmenthost_Jsonclick ;
      private String edtavEnvironmentport_Internalname ;
      private String edtavEnvironmentport_Jsonclick ;
      private String edtavEnvironmentvirtualdirectory_Internalname ;
      private String AV29EnvironmentVirtualDirectory ;
      private String edtavEnvironmentvirtualdirectory_Jsonclick ;
      private String edtavEnvironmentprogrampackage_Internalname ;
      private String AV27EnvironmentProgramPackage ;
      private String edtavEnvironmentprogrampackage_Jsonclick ;
      private String edtavEnvironmentprogramextension_Internalname ;
      private String AV26EnvironmentProgramExtension ;
      private String edtavEnvironmentprogramextension_Jsonclick ;
      private String bttBtncancel_Internalname ;
      private String bttBtncancel_Jsonclick ;
      private String bttBtnconfirm_Internalname ;
      private String bttBtnconfirm_Caption ;
      private String bttBtnconfirm_Jsonclick ;
      private String chkavAutoregisteranomymoususer_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String Tab2_Internalname ;
      private DateTime AV18ClientRevoked ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool Tab2_Historymanagement ;
      private bool wbLoad ;
      private bool AV43UseAbsoluteUrlByEnvironment ;
      private bool AV5AccessRequiresPermission ;
      private bool AV8ClientAccessUniqueByUser ;
      private bool AV11ClientAllowRemoteAuth ;
      private bool AV10ClientAllowGetUserRoles ;
      private bool AV9ClientAllowGetUserAddData ;
      private bool AV28EnvironmentSecureProtocol ;
      private bool AV7AutoRegisterAnomymousUser ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private bool AV35isOk ;
      private String AV33HomeObject ;
      private String AV17LogoutObject ;
      private String AV16ClientLocalLoginURL ;
      private String AV12ClientCallbackURL ;
      private String AV15ClientImageURL ;
      private String imgImagerevoke_Bitmap ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Gx_mode ;
      private long aP1_Id ;
      private GXCheckbox chkavUseabsoluteurlbyenvironment ;
      private GXCombobox cmbavMainmenu ;
      private GXCheckbox chkavAccessrequirespermission ;
      private GXCheckbox chkavClientaccessuniquebyuser ;
      private GXCheckbox chkavClientallowremoteauth ;
      private GXCheckbox chkavClientallowgetuserroles ;
      private GXCheckbox chkavClientallowgetuseradddata ;
      private GXCheckbox chkavEnvironmentsecureprotocol ;
      private GXCheckbox chkavAutoregisteranomymoususer ;
      private IDataStoreProvider pr_gam ;
      private IDataStoreProvider pr_default ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXExternalCollection<SdtGAMError> AV31Errors ;
      private GXExternalCollection<SdtGAMApplicationMenu> AV47GXV1 ;
      private GXWebForm Form ;
      private SdtGAMApplication AV6Application ;
      private SdtGAMError AV30Error ;
      private SdtGAMApplicationMenu AV37Menu ;
      private SdtGAMApplicationMenuFilter AV38MenuFilter ;
      private SdtGAMUser AV40User ;
   }

   public class gamexampleapplicationentry__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class gamexampleapplicationentry__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        def= new CursorDef[] {
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
     }
  }

}

}
