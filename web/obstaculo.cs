/*
               File: Obstaculo
        Description: Obstaculo
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 8:45:19.29
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class obstaculo : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_7") == 0 )
         {
            A34ObstaculoPacienteRegistra = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            n34ObstaculoPacienteRegistra = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34ObstaculoPacienteRegistra", A34ObstaculoPacienteRegistra.ToString());
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_7( A34ObstaculoPacienteRegistra) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbObstaculoTipo.Name = "OBSTACULOTIPO";
         cmbObstaculoTipo.WebTags = "";
         cmbObstaculoTipo.addItem("0", "Ninguno", 0);
         cmbObstaculoTipo.addItem("1", "Agujero", 0);
         cmbObstaculoTipo.addItem("2", "Gradas", 0);
         cmbObstaculoTipo.addItem("3", "Otros", 0);
         if ( cmbObstaculoTipo.ItemCount > 0 )
         {
            A37ObstaculoTipo = (short)(NumberUtil.Val( cmbObstaculoTipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37ObstaculoTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Obstaculo", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtObstaculoID_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("K2BFlatCompactGreen");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public obstaculo( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public obstaculo( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbObstaculoTipo = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "obstaculo_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbObstaculoTipo.ItemCount > 0 )
         {
            A37ObstaculoTipo = (short)(NumberUtil.Val( cmbObstaculoTipo.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37ObstaculoTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbObstaculoTipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbObstaculoTipo_Internalname, "Values", cmbObstaculoTipo.ToJavascriptSource(), true);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablemain_Internalname, 1, 0, "px", 0, "px", "Container FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Obstaculo", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "|<", bttBtn_first_Jsonclick, 5, "|<", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "<", bttBtn_previous_Jsonclick, 5, "<", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", ">", bttBtn_next_Jsonclick, 5, ">", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", ">|", bttBtn_last_Jsonclick, 5, ">|", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0060.aspx"+"',["+"{Ctrl:gx.dom.el('"+"OBSTACULOID"+"'), id:'"+"OBSTACULOID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtObstaculoID_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtObstaculoID_Internalname, "ID", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtObstaculoID_Internalname, A35ObstaculoID.ToString(), A35ObstaculoID.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtObstaculoID_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtObstaculoID_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtObstaculoFechaRegistro_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtObstaculoFechaRegistro_Internalname, "Fecha Registro", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtObstaculoFechaRegistro_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtObstaculoFechaRegistro_Internalname, context.localUtil.TToC( A36ObstaculoFechaRegistro, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A36ObstaculoFechaRegistro, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'spa',false,0);"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtObstaculoFechaRegistro_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtObstaculoFechaRegistro_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Obstaculo.htm");
            GxWebStd.gx_bitmap( context, edtObstaculoFechaRegistro_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtObstaculoFechaRegistro_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_Obstaculo.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbObstaculoTipo_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbObstaculoTipo_Internalname, "Tipo", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbObstaculoTipo, cmbObstaculoTipo_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0)), 1, cmbObstaculoTipo_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbObstaculoTipo.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_Obstaculo.htm");
            cmbObstaculoTipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbObstaculoTipo_Internalname, "Values", (String)(cmbObstaculoTipo.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtObstaculoPacienteRegistra_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtObstaculoPacienteRegistra_Internalname, "Paciente Registra", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtObstaculoPacienteRegistra_Internalname, A34ObstaculoPacienteRegistra.ToString(), A34ObstaculoPacienteRegistra.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtObstaculoPacienteRegistra_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtObstaculoPacienteRegistra_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_Obstaculo.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_34_Internalname, sImgUrl, imgprompt_34_Link, "", "", context.GetTheme( ), imgprompt_34_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtObstaculoLongitud_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtObstaculoLongitud_Internalname, "Longitud", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtObstaculoLongitud_Internalname, A38ObstaculoLongitud, "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,48);\"", 0, 1, edtObstaculoLongitud_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "2097152", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtObstaculoLatitud_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtObstaculoLatitud_Internalname, "Latitud", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtObstaculoLatitud_Internalname, A39ObstaculoLatitud, "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", 0, 1, edtObstaculoLatitud_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "2097152", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtObstaculoNumeroAdvertencias_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtObstaculoNumeroAdvertencias_Internalname, "Numero Advertencias", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtObstaculoNumeroAdvertencias_Internalname, StringUtil.LTrim( StringUtil.NToC( (decimal)(A40ObstaculoNumeroAdvertencias), 4, 0, ",", "")), ((edtObstaculoNumeroAdvertencias_Enabled!=0) ? StringUtil.LTrim( context.localUtil.Format( (decimal)(A40ObstaculoNumeroAdvertencias), "ZZZ9")) : context.localUtil.Format( (decimal)(A40ObstaculoNumeroAdvertencias), "ZZZ9")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.num.valid_integer( this,'.');"+";gx.evt.onblur(this,58);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtObstaculoNumeroAdvertencias_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtObstaculoNumeroAdvertencias_Enabled, 0, "text", "", 4, "chr", 1, "row", 4, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 65,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 67,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_Obstaculo.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( StringUtil.StrCmp(cgiGet( edtObstaculoID_Internalname), "") == 0 )
               {
                  A35ObstaculoID = (Guid)(Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
               }
               else
               {
                  try
                  {
                     A35ObstaculoID = (Guid)(StringUtil.StrToGuid( cgiGet( edtObstaculoID_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "OBSTACULOID");
                     AnyError = 1;
                     GX_FocusControl = edtObstaculoID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtObstaculoFechaRegistro_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Obstaculo Fecha Registro"}), 1, "OBSTACULOFECHAREGISTRO");
                  AnyError = 1;
                  GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36ObstaculoFechaRegistro", context.localUtil.TToC( A36ObstaculoFechaRegistro, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A36ObstaculoFechaRegistro = context.localUtil.CToT( cgiGet( edtObstaculoFechaRegistro_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36ObstaculoFechaRegistro", context.localUtil.TToC( A36ObstaculoFechaRegistro, 8, 5, 0, 3, "/", ":", " "));
               }
               cmbObstaculoTipo.CurrentValue = cgiGet( cmbObstaculoTipo_Internalname);
               A37ObstaculoTipo = (short)(NumberUtil.Val( cgiGet( cmbObstaculoTipo_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37ObstaculoTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0)));
               if ( StringUtil.StrCmp(cgiGet( edtObstaculoPacienteRegistra_Internalname), "") == 0 )
               {
                  A34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
                  n34ObstaculoPacienteRegistra = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34ObstaculoPacienteRegistra", A34ObstaculoPacienteRegistra.ToString());
               }
               else
               {
                  try
                  {
                     A34ObstaculoPacienteRegistra = (Guid)(StringUtil.StrToGuid( cgiGet( edtObstaculoPacienteRegistra_Internalname)));
                     n34ObstaculoPacienteRegistra = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34ObstaculoPacienteRegistra", A34ObstaculoPacienteRegistra.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "OBSTACULOPACIENTEREGISTRA");
                     AnyError = 1;
                     GX_FocusControl = edtObstaculoPacienteRegistra_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               n34ObstaculoPacienteRegistra = ((Guid.Empty==A34ObstaculoPacienteRegistra) ? true : false);
               A38ObstaculoLongitud = cgiGet( edtObstaculoLongitud_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38ObstaculoLongitud", A38ObstaculoLongitud);
               A39ObstaculoLatitud = cgiGet( edtObstaculoLatitud_Internalname);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39ObstaculoLatitud", A39ObstaculoLatitud);
               if ( ( ( context.localUtil.CToN( cgiGet( edtObstaculoNumeroAdvertencias_Internalname), ",", ".") < Convert.ToDecimal( 0 )) ) || ( ( context.localUtil.CToN( cgiGet( edtObstaculoNumeroAdvertencias_Internalname), ",", ".") > Convert.ToDecimal( 9999 )) ) )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_badnum", ""), 1, "OBSTACULONUMEROADVERTENCIAS");
                  AnyError = 1;
                  GX_FocusControl = edtObstaculoNumeroAdvertencias_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A40ObstaculoNumeroAdvertencias = 0;
                  n40ObstaculoNumeroAdvertencias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40ObstaculoNumeroAdvertencias", StringUtil.LTrim( StringUtil.Str( (decimal)(A40ObstaculoNumeroAdvertencias), 4, 0)));
               }
               else
               {
                  A40ObstaculoNumeroAdvertencias = (short)(context.localUtil.CToN( cgiGet( edtObstaculoNumeroAdvertencias_Internalname), ",", "."));
                  n40ObstaculoNumeroAdvertencias = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40ObstaculoNumeroAdvertencias", StringUtil.LTrim( StringUtil.Str( (decimal)(A40ObstaculoNumeroAdvertencias), 4, 0)));
               }
               n40ObstaculoNumeroAdvertencias = ((0==A40ObstaculoNumeroAdvertencias) ? true : false);
               /* Read saved values. */
               Z35ObstaculoID = (Guid)(StringUtil.StrToGuid( cgiGet( "Z35ObstaculoID")));
               Z36ObstaculoFechaRegistro = context.localUtil.CToT( cgiGet( "Z36ObstaculoFechaRegistro"), 0);
               Z37ObstaculoTipo = (short)(context.localUtil.CToN( cgiGet( "Z37ObstaculoTipo"), ",", "."));
               Z40ObstaculoNumeroAdvertencias = (short)(context.localUtil.CToN( cgiGet( "Z40ObstaculoNumeroAdvertencias"), ",", "."));
               n40ObstaculoNumeroAdvertencias = ((0==A40ObstaculoNumeroAdvertencias) ? true : false);
               Z34ObstaculoPacienteRegistra = (Guid)(StringUtil.StrToGuid( cgiGet( "Z34ObstaculoPacienteRegistra")));
               n34ObstaculoPacienteRegistra = ((Guid.Empty==A34ObstaculoPacienteRegistra) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A35ObstaculoID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
                  getEqualNoModal( ) ;
                  if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A35ObstaculoID) && ( Gx_BScreen == 0 ) )
                  {
                     A35ObstaculoID = (Guid)(Guid.NewGuid( ));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  getEqualNoModal( ) ;
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll046( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes046( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption040( )
      {
      }

      protected void ZM046( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z36ObstaculoFechaRegistro = T00043_A36ObstaculoFechaRegistro[0];
               Z37ObstaculoTipo = T00043_A37ObstaculoTipo[0];
               Z40ObstaculoNumeroAdvertencias = T00043_A40ObstaculoNumeroAdvertencias[0];
               Z34ObstaculoPacienteRegistra = (Guid)(T00043_A34ObstaculoPacienteRegistra[0]);
            }
            else
            {
               Z36ObstaculoFechaRegistro = A36ObstaculoFechaRegistro;
               Z37ObstaculoTipo = A37ObstaculoTipo;
               Z40ObstaculoNumeroAdvertencias = A40ObstaculoNumeroAdvertencias;
               Z34ObstaculoPacienteRegistra = (Guid)(A34ObstaculoPacienteRegistra);
            }
         }
         if ( GX_JID == -6 )
         {
            Z35ObstaculoID = (Guid)(A35ObstaculoID);
            Z36ObstaculoFechaRegistro = A36ObstaculoFechaRegistro;
            Z37ObstaculoTipo = A37ObstaculoTipo;
            Z38ObstaculoLongitud = A38ObstaculoLongitud;
            Z39ObstaculoLatitud = A39ObstaculoLatitud;
            Z40ObstaculoNumeroAdvertencias = A40ObstaculoNumeroAdvertencias;
            Z34ObstaculoPacienteRegistra = (Guid)(A34ObstaculoPacienteRegistra);
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         imgprompt_34_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"OBSTACULOPACIENTEREGISTRA"+"'), id:'"+"OBSTACULOPACIENTEREGISTRA"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A35ObstaculoID) && ( Gx_BScreen == 0 ) )
         {
            A35ObstaculoID = (Guid)(Guid.NewGuid( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load046( )
      {
         /* Using cursor T00045 */
         pr_default.execute(3, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound6 = 1;
            A36ObstaculoFechaRegistro = T00045_A36ObstaculoFechaRegistro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36ObstaculoFechaRegistro", context.localUtil.TToC( A36ObstaculoFechaRegistro, 8, 5, 0, 3, "/", ":", " "));
            A37ObstaculoTipo = T00045_A37ObstaculoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37ObstaculoTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0)));
            A38ObstaculoLongitud = T00045_A38ObstaculoLongitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38ObstaculoLongitud", A38ObstaculoLongitud);
            A39ObstaculoLatitud = T00045_A39ObstaculoLatitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39ObstaculoLatitud", A39ObstaculoLatitud);
            A40ObstaculoNumeroAdvertencias = T00045_A40ObstaculoNumeroAdvertencias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40ObstaculoNumeroAdvertencias", StringUtil.LTrim( StringUtil.Str( (decimal)(A40ObstaculoNumeroAdvertencias), 4, 0)));
            n40ObstaculoNumeroAdvertencias = T00045_n40ObstaculoNumeroAdvertencias[0];
            A34ObstaculoPacienteRegistra = (Guid)((Guid)(T00045_A34ObstaculoPacienteRegistra[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34ObstaculoPacienteRegistra", A34ObstaculoPacienteRegistra.ToString());
            n34ObstaculoPacienteRegistra = T00045_n34ObstaculoPacienteRegistra[0];
            ZM046( -6) ;
         }
         pr_default.close(3);
         OnLoadActions046( ) ;
      }

      protected void OnLoadActions046( )
      {
      }

      protected void CheckExtendedTable046( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A36ObstaculoFechaRegistro) || ( A36ObstaculoFechaRegistro >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Obstaculo Fecha Registro fuera de rango", "OutOfRange", 1, "OBSTACULOFECHAREGISTRO");
            AnyError = 1;
            GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A37ObstaculoTipo == 0 ) || ( A37ObstaculoTipo == 1 ) || ( A37ObstaculoTipo == 2 ) || ( A37ObstaculoTipo == 3 ) ) )
         {
            GX_msglist.addItem("Campo Obstaculo Tipo fuera de rango", "OutOfRange", 1, "OBSTACULOTIPO");
            AnyError = 1;
            GX_FocusControl = cmbObstaculoTipo_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00044 */
         pr_default.execute(2, new Object[] {n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (Guid.Empty==A34ObstaculoPacienteRegistra) ) )
            {
               GX_msglist.addItem("No existe 'Obstaculo Paciente_ST'.", "ForeignKeyNotFound", 1, "OBSTACULOPACIENTEREGISTRA");
               AnyError = 1;
               GX_FocusControl = edtObstaculoPacienteRegistra_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors046( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_7( Guid A34ObstaculoPacienteRegistra )
      {
         /* Using cursor T00046 */
         pr_default.execute(4, new Object[] {n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra});
         if ( (pr_default.getStatus(4) == 101) )
         {
            if ( ! ( (Guid.Empty==A34ObstaculoPacienteRegistra) ) )
            {
               GX_msglist.addItem("No existe 'Obstaculo Paciente_ST'.", "ForeignKeyNotFound", 1, "OBSTACULOPACIENTEREGISTRA");
               AnyError = 1;
               GX_FocusControl = edtObstaculoPacienteRegistra_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(4) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(4);
      }

      protected void GetKey046( )
      {
         /* Using cursor T00047 */
         pr_default.execute(5, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound6 = 1;
         }
         else
         {
            RcdFound6 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00043 */
         pr_default.execute(1, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM046( 6) ;
            RcdFound6 = 1;
            A35ObstaculoID = (Guid)((Guid)(T00043_A35ObstaculoID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
            A36ObstaculoFechaRegistro = T00043_A36ObstaculoFechaRegistro[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36ObstaculoFechaRegistro", context.localUtil.TToC( A36ObstaculoFechaRegistro, 8, 5, 0, 3, "/", ":", " "));
            A37ObstaculoTipo = T00043_A37ObstaculoTipo[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37ObstaculoTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0)));
            A38ObstaculoLongitud = T00043_A38ObstaculoLongitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38ObstaculoLongitud", A38ObstaculoLongitud);
            A39ObstaculoLatitud = T00043_A39ObstaculoLatitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39ObstaculoLatitud", A39ObstaculoLatitud);
            A40ObstaculoNumeroAdvertencias = T00043_A40ObstaculoNumeroAdvertencias[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40ObstaculoNumeroAdvertencias", StringUtil.LTrim( StringUtil.Str( (decimal)(A40ObstaculoNumeroAdvertencias), 4, 0)));
            n40ObstaculoNumeroAdvertencias = T00043_n40ObstaculoNumeroAdvertencias[0];
            A34ObstaculoPacienteRegistra = (Guid)((Guid)(T00043_A34ObstaculoPacienteRegistra[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34ObstaculoPacienteRegistra", A34ObstaculoPacienteRegistra.ToString());
            n34ObstaculoPacienteRegistra = T00043_n34ObstaculoPacienteRegistra[0];
            Z35ObstaculoID = (Guid)(A35ObstaculoID);
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load046( ) ;
            if ( AnyError == 1 )
            {
               RcdFound6 = 0;
               InitializeNonKey046( ) ;
            }
            Gx_mode = sMode6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound6 = 0;
            InitializeNonKey046( ) ;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode6;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey046( ) ;
         if ( RcdFound6 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound6 = 0;
         /* Using cursor T00048 */
         pr_default.execute(6, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(6) != 101) )
         {
            while ( (pr_default.getStatus(6) != 101) && ( ( GuidUtil.Compare(T00048_A35ObstaculoID[0], A35ObstaculoID, 1) < 0 ) ) )
            {
               pr_default.readNext(6);
            }
            if ( (pr_default.getStatus(6) != 101) && ( ( GuidUtil.Compare(T00048_A35ObstaculoID[0], A35ObstaculoID, 1) > 0 ) ) )
            {
               A35ObstaculoID = (Guid)((Guid)(T00048_A35ObstaculoID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
               RcdFound6 = 1;
            }
         }
         pr_default.close(6);
      }

      protected void move_previous( )
      {
         RcdFound6 = 0;
         /* Using cursor T00049 */
         pr_default.execute(7, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(7) != 101) )
         {
            while ( (pr_default.getStatus(7) != 101) && ( ( GuidUtil.Compare(T00049_A35ObstaculoID[0], A35ObstaculoID, 1) > 0 ) ) )
            {
               pr_default.readNext(7);
            }
            if ( (pr_default.getStatus(7) != 101) && ( ( GuidUtil.Compare(T00049_A35ObstaculoID[0], A35ObstaculoID, 1) < 0 ) ) )
            {
               A35ObstaculoID = (Guid)((Guid)(T00049_A35ObstaculoID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
               RcdFound6 = 1;
            }
         }
         pr_default.close(7);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey046( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtObstaculoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert046( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound6 == 1 )
            {
               if ( A35ObstaculoID != Z35ObstaculoID )
               {
                  A35ObstaculoID = (Guid)(Z35ObstaculoID);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "OBSTACULOID");
                  AnyError = 1;
                  GX_FocusControl = edtObstaculoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtObstaculoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update046( ) ;
                  GX_FocusControl = edtObstaculoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A35ObstaculoID != Z35ObstaculoID )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtObstaculoID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert046( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "OBSTACULOID");
                     AnyError = 1;
                     GX_FocusControl = edtObstaculoID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtObstaculoID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert046( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A35ObstaculoID != Z35ObstaculoID )
         {
            A35ObstaculoID = (Guid)(Z35ObstaculoID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "OBSTACULOID");
            AnyError = 1;
            GX_FocusControl = edtObstaculoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtObstaculoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "OBSTACULOID");
            AnyError = 1;
            GX_FocusControl = edtObstaculoID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart046( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd046( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart046( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound6 != 0 )
            {
               ScanNext046( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd046( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency046( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00042 */
            pr_default.execute(0, new Object[] {A35ObstaculoID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Obstaculo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z36ObstaculoFechaRegistro != T00042_A36ObstaculoFechaRegistro[0] ) || ( Z37ObstaculoTipo != T00042_A37ObstaculoTipo[0] ) || ( Z40ObstaculoNumeroAdvertencias != T00042_A40ObstaculoNumeroAdvertencias[0] ) || ( Z34ObstaculoPacienteRegistra != T00042_A34ObstaculoPacienteRegistra[0] ) )
            {
               if ( Z36ObstaculoFechaRegistro != T00042_A36ObstaculoFechaRegistro[0] )
               {
                  GXUtil.WriteLog("obstaculo:[seudo value changed for attri]"+"ObstaculoFechaRegistro");
                  GXUtil.WriteLogRaw("Old: ",Z36ObstaculoFechaRegistro);
                  GXUtil.WriteLogRaw("Current: ",T00042_A36ObstaculoFechaRegistro[0]);
               }
               if ( Z37ObstaculoTipo != T00042_A37ObstaculoTipo[0] )
               {
                  GXUtil.WriteLog("obstaculo:[seudo value changed for attri]"+"ObstaculoTipo");
                  GXUtil.WriteLogRaw("Old: ",Z37ObstaculoTipo);
                  GXUtil.WriteLogRaw("Current: ",T00042_A37ObstaculoTipo[0]);
               }
               if ( Z40ObstaculoNumeroAdvertencias != T00042_A40ObstaculoNumeroAdvertencias[0] )
               {
                  GXUtil.WriteLog("obstaculo:[seudo value changed for attri]"+"ObstaculoNumeroAdvertencias");
                  GXUtil.WriteLogRaw("Old: ",Z40ObstaculoNumeroAdvertencias);
                  GXUtil.WriteLogRaw("Current: ",T00042_A40ObstaculoNumeroAdvertencias[0]);
               }
               if ( Z34ObstaculoPacienteRegistra != T00042_A34ObstaculoPacienteRegistra[0] )
               {
                  GXUtil.WriteLog("obstaculo:[seudo value changed for attri]"+"ObstaculoPacienteRegistra");
                  GXUtil.WriteLogRaw("Old: ",Z34ObstaculoPacienteRegistra);
                  GXUtil.WriteLogRaw("Current: ",T00042_A34ObstaculoPacienteRegistra[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Obstaculo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert046( )
      {
         if ( ! IsAuthorized("obstaculo_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate046( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable046( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM046( 0) ;
            CheckOptimisticConcurrency046( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm046( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert046( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000410 */
                     pr_default.execute(8, new Object[] {A36ObstaculoFechaRegistro, A37ObstaculoTipo, A38ObstaculoLongitud, A39ObstaculoLatitud, n40ObstaculoNumeroAdvertencias, A40ObstaculoNumeroAdvertencias, n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra, A35ObstaculoID});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Obstaculo") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption040( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load046( ) ;
            }
            EndLevel046( ) ;
         }
         CloseExtendedTableCursors046( ) ;
      }

      protected void Update046( )
      {
         if ( ! IsAuthorized("obstaculo_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate046( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable046( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency046( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm046( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate046( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000411 */
                     pr_default.execute(9, new Object[] {A36ObstaculoFechaRegistro, A37ObstaculoTipo, A38ObstaculoLongitud, A39ObstaculoLatitud, n40ObstaculoNumeroAdvertencias, A40ObstaculoNumeroAdvertencias, n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra, A35ObstaculoID});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Obstaculo") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Obstaculo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate046( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption040( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel046( ) ;
         }
         CloseExtendedTableCursors046( ) ;
      }

      protected void DeferredUpdate046( )
      {
      }

      protected void delete( )
      {
         if ( ! IsAuthorized("obstaculo_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate046( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency046( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls046( ) ;
            AfterConfirm046( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete046( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000412 */
                  pr_default.execute(10, new Object[] {A35ObstaculoID});
                  pr_default.close(10);
                  dsDefault.SmartCacheProvider.SetUpdated("Obstaculo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound6 == 0 )
                        {
                           InitAll046( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption040( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode6 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel046( ) ;
         Gx_mode = sMode6;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls046( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel046( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete046( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_gam.commit( "Obstaculo");
            pr_default.commit( "Obstaculo");
            if ( AnyError == 0 )
            {
               ConfirmValues040( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_gam.rollback( "Obstaculo");
            pr_default.rollback( "Obstaculo");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart046( )
      {
         /* Using cursor T000413 */
         pr_default.execute(11);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound6 = 1;
            A35ObstaculoID = (Guid)((Guid)(T000413_A35ObstaculoID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext046( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound6 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound6 = 1;
            A35ObstaculoID = (Guid)((Guid)(T000413_A35ObstaculoID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
         }
      }

      protected void ScanEnd046( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm046( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert046( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate046( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete046( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete046( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate046( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes046( )
      {
         edtObstaculoID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtObstaculoID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtObstaculoID_Enabled), 5, 0)), true);
         edtObstaculoFechaRegistro_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtObstaculoFechaRegistro_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtObstaculoFechaRegistro_Enabled), 5, 0)), true);
         cmbObstaculoTipo.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbObstaculoTipo_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbObstaculoTipo.Enabled), 5, 0)), true);
         edtObstaculoPacienteRegistra_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtObstaculoPacienteRegistra_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtObstaculoPacienteRegistra_Enabled), 5, 0)), true);
         edtObstaculoLongitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtObstaculoLongitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtObstaculoLongitud_Enabled), 5, 0)), true);
         edtObstaculoLatitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtObstaculoLatitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtObstaculoLatitud_Enabled), 5, 0)), true);
         edtObstaculoNumeroAdvertencias_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtObstaculoNumeroAdvertencias_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtObstaculoNumeroAdvertencias_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes046( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues040( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?20181118845209", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("obstaculo.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z35ObstaculoID", Z35ObstaculoID.ToString());
         GxWebStd.gx_hidden_field( context, "Z36ObstaculoFechaRegistro", context.localUtil.TToC( Z36ObstaculoFechaRegistro, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z37ObstaculoTipo", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z37ObstaculoTipo), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z40ObstaculoNumeroAdvertencias", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z40ObstaculoNumeroAdvertencias), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z34ObstaculoPacienteRegistra", Z34ObstaculoPacienteRegistra.ToString());
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("obstaculo.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "Obstaculo" ;
      }

      public override String GetPgmdesc( )
      {
         return "Obstaculo" ;
      }

      protected void InitializeNonKey046( )
      {
         A36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A36ObstaculoFechaRegistro", context.localUtil.TToC( A36ObstaculoFechaRegistro, 8, 5, 0, 3, "/", ":", " "));
         A37ObstaculoTipo = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A37ObstaculoTipo", StringUtil.LTrim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0)));
         A34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
         n34ObstaculoPacienteRegistra = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A34ObstaculoPacienteRegistra", A34ObstaculoPacienteRegistra.ToString());
         n34ObstaculoPacienteRegistra = ((Guid.Empty==A34ObstaculoPacienteRegistra) ? true : false);
         A38ObstaculoLongitud = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A38ObstaculoLongitud", A38ObstaculoLongitud);
         A39ObstaculoLatitud = "";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A39ObstaculoLatitud", A39ObstaculoLatitud);
         A40ObstaculoNumeroAdvertencias = 0;
         n40ObstaculoNumeroAdvertencias = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A40ObstaculoNumeroAdvertencias", StringUtil.LTrim( StringUtil.Str( (decimal)(A40ObstaculoNumeroAdvertencias), 4, 0)));
         n40ObstaculoNumeroAdvertencias = ((0==A40ObstaculoNumeroAdvertencias) ? true : false);
         Z36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         Z37ObstaculoTipo = 0;
         Z40ObstaculoNumeroAdvertencias = 0;
         Z34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
      }

      protected void InitAll046( )
      {
         A35ObstaculoID = (Guid)(Guid.NewGuid( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A35ObstaculoID", A35ObstaculoID.ToString());
         InitializeNonKey046( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811188452013", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("obstaculo.js", "?201811188452013", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         edtObstaculoID_Internalname = "OBSTACULOID";
         edtObstaculoFechaRegistro_Internalname = "OBSTACULOFECHAREGISTRO";
         cmbObstaculoTipo_Internalname = "OBSTACULOTIPO";
         edtObstaculoPacienteRegistra_Internalname = "OBSTACULOPACIENTEREGISTRA";
         edtObstaculoLongitud_Internalname = "OBSTACULOLONGITUD";
         edtObstaculoLatitud_Internalname = "OBSTACULOLATITUD";
         edtObstaculoNumeroAdvertencias_Internalname = "OBSTACULONUMEROADVERTENCIAS";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_34_Internalname = "PROMPT_34";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Obstaculo";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtObstaculoNumeroAdvertencias_Jsonclick = "";
         edtObstaculoNumeroAdvertencias_Enabled = 1;
         edtObstaculoLatitud_Enabled = 1;
         edtObstaculoLongitud_Enabled = 1;
         imgprompt_34_Visible = 1;
         imgprompt_34_Link = "";
         edtObstaculoPacienteRegistra_Jsonclick = "";
         edtObstaculoPacienteRegistra_Enabled = 1;
         cmbObstaculoTipo_Jsonclick = "";
         cmbObstaculoTipo.Enabled = 1;
         edtObstaculoFechaRegistro_Jsonclick = "";
         edtObstaculoFechaRegistro_Enabled = 1;
         edtObstaculoID_Jsonclick = "";
         edtObstaculoID_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtObstaculoFechaRegistro_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Obstaculoid( Guid GX_Parm1 ,
                                     DateTime GX_Parm2 ,
                                     GXCombobox cmbGX_Parm3 ,
                                     String GX_Parm4 ,
                                     String GX_Parm5 ,
                                     short GX_Parm6 ,
                                     Guid GX_Parm7 )
      {
         A35ObstaculoID = (Guid)(GX_Parm1);
         A36ObstaculoFechaRegistro = GX_Parm2;
         cmbObstaculoTipo = cmbGX_Parm3;
         A37ObstaculoTipo = (short)(NumberUtil.Val( cmbObstaculoTipo.CurrentValue, "."));
         cmbObstaculoTipo.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0));
         A38ObstaculoLongitud = GX_Parm4;
         A39ObstaculoLatitud = GX_Parm5;
         A40ObstaculoNumeroAdvertencias = GX_Parm6;
         n40ObstaculoNumeroAdvertencias = false;
         A34ObstaculoPacienteRegistra = (Guid)(GX_Parm7);
         n34ObstaculoPacienteRegistra = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
         }
         isValidOutput.Add(context.localUtil.TToC( A36ObstaculoFechaRegistro, 10, 8, 0, 3, "/", ":", " "));
         cmbObstaculoTipo.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A37ObstaculoTipo), 4, 0));
         isValidOutput.Add(cmbObstaculoTipo);
         isValidOutput.Add(A34ObstaculoPacienteRegistra.ToString());
         isValidOutput.Add(A38ObstaculoLongitud);
         isValidOutput.Add(A39ObstaculoLatitud);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(A40ObstaculoNumeroAdvertencias), 4, 0, ".", "")));
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(Z35ObstaculoID.ToString());
         isValidOutput.Add(context.localUtil.TToC( Z36ObstaculoFechaRegistro, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z37ObstaculoTipo), 4, 0, ",", "")));
         isValidOutput.Add(Z34ObstaculoPacienteRegistra.ToString());
         isValidOutput.Add(Z38ObstaculoLongitud);
         isValidOutput.Add(Z39ObstaculoLatitud);
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z40ObstaculoNumeroAdvertencias), 4, 0, ",", "")));
         isValidOutput.Add(bttBtn_delete_Enabled);
         isValidOutput.Add(bttBtn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Obstaculopacienteregistra( Guid GX_Parm1 )
      {
         A34ObstaculoPacienteRegistra = (Guid)(GX_Parm1);
         n34ObstaculoPacienteRegistra = false;
         /* Using cursor T000414 */
         pr_default.execute(12, new Object[] {n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra});
         if ( (pr_default.getStatus(12) == 101) )
         {
            if ( ! ( (Guid.Empty==A34ObstaculoPacienteRegistra) ) )
            {
               GX_msglist.addItem("No existe 'Obstaculo Paciente_ST'.", "ForeignKeyNotFound", 1, "OBSTACULOPACIENTEREGISTRA");
               AnyError = 1;
               GX_FocusControl = edtObstaculoPacienteRegistra_Internalname;
            }
         }
         pr_default.close(12);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(12);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z35ObstaculoID = (Guid)(Guid.Empty);
         Z36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         Z34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A35ObstaculoID = (Guid)(Guid.Empty);
         A36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         sImgUrl = "";
         A38ObstaculoLongitud = "";
         A39ObstaculoLatitud = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z38ObstaculoLongitud = "";
         Z39ObstaculoLatitud = "";
         T00045_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         T00045_A36ObstaculoFechaRegistro = new DateTime[] {DateTime.MinValue} ;
         T00045_A37ObstaculoTipo = new short[1] ;
         T00045_A38ObstaculoLongitud = new String[] {""} ;
         T00045_A39ObstaculoLatitud = new String[] {""} ;
         T00045_A40ObstaculoNumeroAdvertencias = new short[1] ;
         T00045_n40ObstaculoNumeroAdvertencias = new bool[] {false} ;
         T00045_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         T00045_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         T00044_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         T00044_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         T00046_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         T00046_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         T00047_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         T00043_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         T00043_A36ObstaculoFechaRegistro = new DateTime[] {DateTime.MinValue} ;
         T00043_A37ObstaculoTipo = new short[1] ;
         T00043_A38ObstaculoLongitud = new String[] {""} ;
         T00043_A39ObstaculoLatitud = new String[] {""} ;
         T00043_A40ObstaculoNumeroAdvertencias = new short[1] ;
         T00043_n40ObstaculoNumeroAdvertencias = new bool[] {false} ;
         T00043_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         T00043_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         sMode6 = "";
         T00048_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         T00049_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         T00042_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         T00042_A36ObstaculoFechaRegistro = new DateTime[] {DateTime.MinValue} ;
         T00042_A37ObstaculoTipo = new short[1] ;
         T00042_A38ObstaculoLongitud = new String[] {""} ;
         T00042_A39ObstaculoLatitud = new String[] {""} ;
         T00042_A40ObstaculoNumeroAdvertencias = new short[1] ;
         T00042_n40ObstaculoNumeroAdvertencias = new bool[] {false} ;
         T00042_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         T00042_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         T000413_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000414_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         T000414_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.obstaculo__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.obstaculo__default(),
            new Object[][] {
                new Object[] {
               T00042_A35ObstaculoID, T00042_A36ObstaculoFechaRegistro, T00042_A37ObstaculoTipo, T00042_A38ObstaculoLongitud, T00042_A39ObstaculoLatitud, T00042_A40ObstaculoNumeroAdvertencias, T00042_n40ObstaculoNumeroAdvertencias, T00042_A34ObstaculoPacienteRegistra, T00042_n34ObstaculoPacienteRegistra
               }
               , new Object[] {
               T00043_A35ObstaculoID, T00043_A36ObstaculoFechaRegistro, T00043_A37ObstaculoTipo, T00043_A38ObstaculoLongitud, T00043_A39ObstaculoLatitud, T00043_A40ObstaculoNumeroAdvertencias, T00043_n40ObstaculoNumeroAdvertencias, T00043_A34ObstaculoPacienteRegistra, T00043_n34ObstaculoPacienteRegistra
               }
               , new Object[] {
               T00044_A34ObstaculoPacienteRegistra
               }
               , new Object[] {
               T00045_A35ObstaculoID, T00045_A36ObstaculoFechaRegistro, T00045_A37ObstaculoTipo, T00045_A38ObstaculoLongitud, T00045_A39ObstaculoLatitud, T00045_A40ObstaculoNumeroAdvertencias, T00045_n40ObstaculoNumeroAdvertencias, T00045_A34ObstaculoPacienteRegistra, T00045_n34ObstaculoPacienteRegistra
               }
               , new Object[] {
               T00046_A34ObstaculoPacienteRegistra
               }
               , new Object[] {
               T00047_A35ObstaculoID
               }
               , new Object[] {
               T00048_A35ObstaculoID
               }
               , new Object[] {
               T00049_A35ObstaculoID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000413_A35ObstaculoID
               }
               , new Object[] {
               T000414_A34ObstaculoPacienteRegistra
               }
            }
         );
         Z35ObstaculoID = (Guid)(Guid.NewGuid( ));
         A35ObstaculoID = (Guid)(Guid.NewGuid( ));
      }

      private short Z37ObstaculoTipo ;
      private short Z40ObstaculoNumeroAdvertencias ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A37ObstaculoTipo ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short A40ObstaculoNumeroAdvertencias ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound6 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtObstaculoID_Enabled ;
      private int edtObstaculoFechaRegistro_Enabled ;
      private int edtObstaculoPacienteRegistra_Enabled ;
      private int imgprompt_34_Visible ;
      private int edtObstaculoLongitud_Enabled ;
      private int edtObstaculoLatitud_Enabled ;
      private int edtObstaculoNumeroAdvertencias_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtObstaculoID_Internalname ;
      private String cmbObstaculoTipo_Internalname ;
      private String divTablemain_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtObstaculoID_Jsonclick ;
      private String edtObstaculoFechaRegistro_Internalname ;
      private String edtObstaculoFechaRegistro_Jsonclick ;
      private String cmbObstaculoTipo_Jsonclick ;
      private String edtObstaculoPacienteRegistra_Internalname ;
      private String edtObstaculoPacienteRegistra_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_34_Internalname ;
      private String imgprompt_34_Link ;
      private String edtObstaculoLongitud_Internalname ;
      private String edtObstaculoLatitud_Internalname ;
      private String edtObstaculoNumeroAdvertencias_Internalname ;
      private String edtObstaculoNumeroAdvertencias_Jsonclick ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode6 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z36ObstaculoFechaRegistro ;
      private DateTime A36ObstaculoFechaRegistro ;
      private bool entryPointCalled ;
      private bool n34ObstaculoPacienteRegistra ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n40ObstaculoNumeroAdvertencias ;
      private String A38ObstaculoLongitud ;
      private String A39ObstaculoLatitud ;
      private String Z38ObstaculoLongitud ;
      private String Z39ObstaculoLatitud ;
      private Guid Z35ObstaculoID ;
      private Guid Z34ObstaculoPacienteRegistra ;
      private Guid A34ObstaculoPacienteRegistra ;
      private Guid A35ObstaculoID ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbObstaculoTipo ;
      private IDataStoreProvider pr_default ;
      private Guid[] T00045_A35ObstaculoID ;
      private DateTime[] T00045_A36ObstaculoFechaRegistro ;
      private short[] T00045_A37ObstaculoTipo ;
      private String[] T00045_A38ObstaculoLongitud ;
      private String[] T00045_A39ObstaculoLatitud ;
      private short[] T00045_A40ObstaculoNumeroAdvertencias ;
      private bool[] T00045_n40ObstaculoNumeroAdvertencias ;
      private Guid[] T00045_A34ObstaculoPacienteRegistra ;
      private bool[] T00045_n34ObstaculoPacienteRegistra ;
      private Guid[] T00044_A34ObstaculoPacienteRegistra ;
      private bool[] T00044_n34ObstaculoPacienteRegistra ;
      private Guid[] T00046_A34ObstaculoPacienteRegistra ;
      private bool[] T00046_n34ObstaculoPacienteRegistra ;
      private Guid[] T00047_A35ObstaculoID ;
      private Guid[] T00043_A35ObstaculoID ;
      private DateTime[] T00043_A36ObstaculoFechaRegistro ;
      private short[] T00043_A37ObstaculoTipo ;
      private String[] T00043_A38ObstaculoLongitud ;
      private String[] T00043_A39ObstaculoLatitud ;
      private short[] T00043_A40ObstaculoNumeroAdvertencias ;
      private bool[] T00043_n40ObstaculoNumeroAdvertencias ;
      private Guid[] T00043_A34ObstaculoPacienteRegistra ;
      private bool[] T00043_n34ObstaculoPacienteRegistra ;
      private Guid[] T00048_A35ObstaculoID ;
      private Guid[] T00049_A35ObstaculoID ;
      private Guid[] T00042_A35ObstaculoID ;
      private DateTime[] T00042_A36ObstaculoFechaRegistro ;
      private short[] T00042_A37ObstaculoTipo ;
      private String[] T00042_A38ObstaculoLongitud ;
      private String[] T00042_A39ObstaculoLatitud ;
      private short[] T00042_A40ObstaculoNumeroAdvertencias ;
      private bool[] T00042_n40ObstaculoNumeroAdvertencias ;
      private Guid[] T00042_A34ObstaculoPacienteRegistra ;
      private bool[] T00042_n34ObstaculoPacienteRegistra ;
      private IDataStoreProvider pr_gam ;
      private Guid[] T000413_A35ObstaculoID ;
      private Guid[] T000414_A34ObstaculoPacienteRegistra ;
      private bool[] T000414_n34ObstaculoPacienteRegistra ;
      private GXWebForm Form ;
   }

   public class obstaculo__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class obstaculo__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new ForEachCursor(def[6])
       ,new ForEachCursor(def[7])
       ,new UpdateCursor(def[8])
       ,new UpdateCursor(def[9])
       ,new UpdateCursor(def[10])
       ,new ForEachCursor(def[11])
       ,new ForEachCursor(def[12])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmT00045 ;
        prmT00045 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00044 ;
        prmT00044 = new Object[] {
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00046 ;
        prmT00046 = new Object[] {
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00047 ;
        prmT00047 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00043 ;
        prmT00043 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00048 ;
        prmT00048 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00049 ;
        prmT00049 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00042 ;
        prmT00042 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000410 ;
        prmT000410 = new Object[] {
        new Object[] {"@ObstaculoFechaRegistro",SqlDbType.DateTime,8,5} ,
        new Object[] {"@ObstaculoTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoNumeroAdvertencias",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000411 ;
        prmT000411 = new Object[] {
        new Object[] {"@ObstaculoFechaRegistro",SqlDbType.DateTime,8,5} ,
        new Object[] {"@ObstaculoTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoNumeroAdvertencias",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000412 ;
        prmT000412 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000413 ;
        prmT000413 = new Object[] {
        } ;
        Object[] prmT000414 ;
        prmT000414 = new Object[] {
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("T00042", "SELECT [ObstaculoID], [ObstaculoFechaRegistro], [ObstaculoTipo], [ObstaculoLongitud], [ObstaculoLatitud], [ObstaculoNumeroAdvertencias], [ObstaculoPacienteRegistra] AS ObstaculoPacienteRegistra FROM [Obstaculo] WITH (UPDLOCK) WHERE [ObstaculoID] = @ObstaculoID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00042,1,0,true,false )
           ,new CursorDef("T00043", "SELECT [ObstaculoID], [ObstaculoFechaRegistro], [ObstaculoTipo], [ObstaculoLongitud], [ObstaculoLatitud], [ObstaculoNumeroAdvertencias], [ObstaculoPacienteRegistra] AS ObstaculoPacienteRegistra FROM [Obstaculo] WITH (NOLOCK) WHERE [ObstaculoID] = @ObstaculoID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00043,1,0,true,false )
           ,new CursorDef("T00044", "SELECT [PersonaID] AS ObstaculoPacienteRegistra FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @ObstaculoPacienteRegistra ",true, GxErrorMask.GX_NOMASK, false, this,prmT00044,1,0,true,false )
           ,new CursorDef("T00045", "SELECT TM1.[ObstaculoID], TM1.[ObstaculoFechaRegistro], TM1.[ObstaculoTipo], TM1.[ObstaculoLongitud], TM1.[ObstaculoLatitud], TM1.[ObstaculoNumeroAdvertencias], TM1.[ObstaculoPacienteRegistra] AS ObstaculoPacienteRegistra FROM [Obstaculo] TM1 WITH (NOLOCK) WHERE TM1.[ObstaculoID] = @ObstaculoID ORDER BY TM1.[ObstaculoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00045,100,0,true,false )
           ,new CursorDef("T00046", "SELECT [PersonaID] AS ObstaculoPacienteRegistra FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @ObstaculoPacienteRegistra ",true, GxErrorMask.GX_NOMASK, false, this,prmT00046,1,0,true,false )
           ,new CursorDef("T00047", "SELECT [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) WHERE [ObstaculoID] = @ObstaculoID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00047,1,0,true,false )
           ,new CursorDef("T00048", "SELECT TOP 1 [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) WHERE ( [ObstaculoID] > @ObstaculoID) ORDER BY [ObstaculoID]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00048,1,0,true,true )
           ,new CursorDef("T00049", "SELECT TOP 1 [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) WHERE ( [ObstaculoID] < @ObstaculoID) ORDER BY [ObstaculoID] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00049,1,0,true,true )
           ,new CursorDef("T000410", "INSERT INTO [Obstaculo]([ObstaculoFechaRegistro], [ObstaculoTipo], [ObstaculoLongitud], [ObstaculoLatitud], [ObstaculoNumeroAdvertencias], [ObstaculoPacienteRegistra], [ObstaculoID]) VALUES(@ObstaculoFechaRegistro, @ObstaculoTipo, @ObstaculoLongitud, @ObstaculoLatitud, @ObstaculoNumeroAdvertencias, @ObstaculoPacienteRegistra, @ObstaculoID)", GxErrorMask.GX_NOMASK,prmT000410)
           ,new CursorDef("T000411", "UPDATE [Obstaculo] SET [ObstaculoFechaRegistro]=@ObstaculoFechaRegistro, [ObstaculoTipo]=@ObstaculoTipo, [ObstaculoLongitud]=@ObstaculoLongitud, [ObstaculoLatitud]=@ObstaculoLatitud, [ObstaculoNumeroAdvertencias]=@ObstaculoNumeroAdvertencias, [ObstaculoPacienteRegistra]=@ObstaculoPacienteRegistra  WHERE [ObstaculoID] = @ObstaculoID", GxErrorMask.GX_NOMASK,prmT000411)
           ,new CursorDef("T000412", "DELETE FROM [Obstaculo]  WHERE [ObstaculoID] = @ObstaculoID", GxErrorMask.GX_NOMASK,prmT000412)
           ,new CursorDef("T000413", "SELECT [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) ORDER BY [ObstaculoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000413,100,0,true,false )
           ,new CursorDef("T000414", "SELECT [PersonaID] AS ObstaculoPacienteRegistra FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @ObstaculoPacienteRegistra ",true, GxErrorMask.GX_NOMASK, false, this,prmT000414,1,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
              ((short[]) buf[5])[0] = rslt.getShort(6) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(6);
              ((Guid[]) buf[7])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(7);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
              ((short[]) buf[5])[0] = rslt.getShort(6) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(6);
              ((Guid[]) buf[7])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(7);
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
              ((short[]) buf[5])[0] = rslt.getShort(6) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(6);
              ((Guid[]) buf[7])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(7);
              return;
           case 4 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 5 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 6 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 7 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 11 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 12 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 2 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 3 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 4 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 5 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 6 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 7 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 8 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              stmt.SetParameter(3, (String)parms[2]);
              stmt.SetParameter(4, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[7]);
              }
              stmt.SetParameter(7, (Guid)parms[8]);
              return;
           case 9 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              stmt.SetParameter(3, (String)parms[2]);
              stmt.SetParameter(4, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[7]);
              }
              stmt.SetParameter(7, (Guid)parms[8]);
              return;
           case 10 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 12 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
     }
  }

}

}
