/*
               File: CoachPaciente_BC
        Description: Coach Paciente
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:26:44.97
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class coachpaciente_bc : GXHttpHandler, IGxSilentTrn
   {
      public coachpaciente_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public coachpaciente_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow022( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey022( ) ;
         standaloneModal( ) ;
         AddRow022( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z12CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
               Z13CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_020( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls022( ) ;
            }
            else
            {
               CheckExtendedTable022( ) ;
               if ( AnyError == 0 )
               {
                  ZM022( 5) ;
                  ZM022( 6) ;
               }
               CloseExtendedTableCursors022( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM022( short GX_JID )
      {
         if ( ( GX_JID == 4 ) || ( GX_JID == 0 ) )
         {
            Z14CoachPaciente_CoachPNombre = A14CoachPaciente_CoachPNombre;
            Z15CoachPaciente_CoachPApellido = A15CoachPaciente_CoachPApellido;
            Z18CoachPaciente_FechaAsignacion = A18CoachPaciente_FechaAsignacion;
            Z19CoachPaciente_DispositivoID = A19CoachPaciente_DispositivoID;
            Z20CoachPaciente_Token = A20CoachPaciente_Token;
         }
         if ( ( GX_JID == 5 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z16CoachPaciente_PacientePNombre = A16CoachPaciente_PacientePNombre;
            Z17CoachPaciente_PacientePApellid = A17CoachPaciente_PacientePApellid;
         }
         if ( GX_JID == -4 )
         {
            Z14CoachPaciente_CoachPNombre = A14CoachPaciente_CoachPNombre;
            Z15CoachPaciente_CoachPApellido = A15CoachPaciente_CoachPApellido;
            Z18CoachPaciente_FechaAsignacion = A18CoachPaciente_FechaAsignacion;
            Z19CoachPaciente_DispositivoID = A19CoachPaciente_DispositivoID;
            Z20CoachPaciente_Token = A20CoachPaciente_Token;
            Z12CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
            Z13CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
            Z16CoachPaciente_PacientePNombre = A16CoachPaciente_PacientePNombre;
            Z17CoachPaciente_PacientePApellid = A17CoachPaciente_PacientePApellid;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load022( )
      {
         /* Using cursor BC00026 */
         pr_default.execute(4, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound2 = 1;
            A14CoachPaciente_CoachPNombre = BC00026_A14CoachPaciente_CoachPNombre[0];
            A15CoachPaciente_CoachPApellido = BC00026_A15CoachPaciente_CoachPApellido[0];
            A16CoachPaciente_PacientePNombre = BC00026_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = BC00026_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = BC00026_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = BC00026_n17CoachPaciente_PacientePApellid[0];
            A18CoachPaciente_FechaAsignacion = BC00026_A18CoachPaciente_FechaAsignacion[0];
            n18CoachPaciente_FechaAsignacion = BC00026_n18CoachPaciente_FechaAsignacion[0];
            A19CoachPaciente_DispositivoID = BC00026_A19CoachPaciente_DispositivoID[0];
            n19CoachPaciente_DispositivoID = BC00026_n19CoachPaciente_DispositivoID[0];
            A20CoachPaciente_Token = BC00026_A20CoachPaciente_Token[0];
            n20CoachPaciente_Token = BC00026_n20CoachPaciente_Token[0];
            ZM022( -4) ;
         }
         pr_default.close(4);
         OnLoadActions022( ) ;
      }

      protected void OnLoadActions022( )
      {
      }

      protected void CheckExtendedTable022( )
      {
         standaloneModal( ) ;
         /* Using cursor BC00024 */
         pr_default.execute(2, new Object[] {A12CoachPaciente_Coach});
         if ( (pr_default.getStatus(2) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Coach ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_COACH");
            AnyError = 1;
         }
         pr_default.close(2);
         /* Using cursor BC00025 */
         pr_default.execute(3, new Object[] {A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(3) == 101) )
         {
            GX_msglist.addItem("No existe 'Coach Paciente Paciente ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_PACIENTE");
            AnyError = 1;
         }
         A16CoachPaciente_PacientePNombre = BC00025_A16CoachPaciente_PacientePNombre[0];
         n16CoachPaciente_PacientePNombre = BC00025_n16CoachPaciente_PacientePNombre[0];
         A17CoachPaciente_PacientePApellid = BC00025_A17CoachPaciente_PacientePApellid[0];
         n17CoachPaciente_PacientePApellid = BC00025_n17CoachPaciente_PacientePApellid[0];
         pr_default.close(3);
         if ( ! ( (DateTime.MinValue==A18CoachPaciente_FechaAsignacion) || ( A18CoachPaciente_FechaAsignacion >= context.localUtil.YMDToD( 1753, 1, 1) ) ) )
         {
            GX_msglist.addItem("Campo Coach Paciente_Fecha Asignacion fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors022( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey022( )
      {
         /* Using cursor BC00027 */
         pr_default.execute(5, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(5) != 101) )
         {
            RcdFound2 = 1;
         }
         else
         {
            RcdFound2 = 0;
         }
         pr_default.close(5);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00023 */
         pr_default.execute(1, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM022( 4) ;
            RcdFound2 = 1;
            A14CoachPaciente_CoachPNombre = BC00023_A14CoachPaciente_CoachPNombre[0];
            A15CoachPaciente_CoachPApellido = BC00023_A15CoachPaciente_CoachPApellido[0];
            A18CoachPaciente_FechaAsignacion = BC00023_A18CoachPaciente_FechaAsignacion[0];
            n18CoachPaciente_FechaAsignacion = BC00023_n18CoachPaciente_FechaAsignacion[0];
            A19CoachPaciente_DispositivoID = BC00023_A19CoachPaciente_DispositivoID[0];
            n19CoachPaciente_DispositivoID = BC00023_n19CoachPaciente_DispositivoID[0];
            A20CoachPaciente_Token = BC00023_A20CoachPaciente_Token[0];
            n20CoachPaciente_Token = BC00023_n20CoachPaciente_Token[0];
            A12CoachPaciente_Coach = (Guid)((Guid)(BC00023_A12CoachPaciente_Coach[0]));
            A13CoachPaciente_Paciente = (Guid)((Guid)(BC00023_A13CoachPaciente_Paciente[0]));
            Z12CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
            Z13CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load022( ) ;
            if ( AnyError == 1 )
            {
               RcdFound2 = 0;
               InitializeNonKey022( ) ;
            }
            Gx_mode = sMode2;
         }
         else
         {
            RcdFound2 = 0;
            InitializeNonKey022( ) ;
            sMode2 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode2;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey022( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_020( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency022( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00022 */
            pr_default.execute(0, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CoachPaciente"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( StringUtil.StrCmp(Z14CoachPaciente_CoachPNombre, BC00022_A14CoachPaciente_CoachPNombre[0]) != 0 ) || ( StringUtil.StrCmp(Z15CoachPaciente_CoachPApellido, BC00022_A15CoachPaciente_CoachPApellido[0]) != 0 ) || ( Z18CoachPaciente_FechaAsignacion != BC00022_A18CoachPaciente_FechaAsignacion[0] ) || ( StringUtil.StrCmp(Z19CoachPaciente_DispositivoID, BC00022_A19CoachPaciente_DispositivoID[0]) != 0 ) || ( StringUtil.StrCmp(Z20CoachPaciente_Token, BC00022_A20CoachPaciente_Token[0]) != 0 ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"CoachPaciente"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert022( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM022( 0) ;
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00028 */
                     pr_default.execute(6, new Object[] {A14CoachPaciente_CoachPNombre, A15CoachPaciente_CoachPApellido, n18CoachPaciente_FechaAsignacion, A18CoachPaciente_FechaAsignacion, n19CoachPaciente_DispositivoID, A19CoachPaciente_DispositivoID, n20CoachPaciente_Token, A20CoachPaciente_Token, A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
                     if ( (pr_default.getStatus(6) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load022( ) ;
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void Update022( )
      {
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable022( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm022( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate022( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00029 */
                     pr_default.execute(7, new Object[] {A14CoachPaciente_CoachPNombre, A15CoachPaciente_CoachPApellido, n18CoachPaciente_FechaAsignacion, A18CoachPaciente_FechaAsignacion, n19CoachPaciente_DispositivoID, A19CoachPaciente_DispositivoID, n20CoachPaciente_Token, A20CoachPaciente_Token, A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
                     pr_default.close(7);
                     dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
                     if ( (pr_default.getStatus(7) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"CoachPaciente"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate022( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel022( ) ;
         }
         CloseExtendedTableCursors022( ) ;
      }

      protected void DeferredUpdate022( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate022( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency022( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls022( ) ;
            AfterConfirm022( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete022( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000210 */
                  pr_default.execute(8, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
                  pr_default.close(8);
                  dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode2 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel022( ) ;
         Gx_mode = sMode2;
      }

      protected void OnDeleteControls022( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor BC000211 */
            pr_default.execute(9, new Object[] {A13CoachPaciente_Paciente});
            A16CoachPaciente_PacientePNombre = BC000211_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = BC000211_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = BC000211_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = BC000211_n17CoachPaciente_PacientePApellid[0];
            pr_default.close(9);
         }
      }

      protected void EndLevel022( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete022( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart022( )
      {
         /* Using cursor BC000212 */
         pr_default.execute(10, new Object[] {A12CoachPaciente_Coach, A13CoachPaciente_Paciente});
         RcdFound2 = 0;
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound2 = 1;
            A14CoachPaciente_CoachPNombre = BC000212_A14CoachPaciente_CoachPNombre[0];
            A15CoachPaciente_CoachPApellido = BC000212_A15CoachPaciente_CoachPApellido[0];
            A16CoachPaciente_PacientePNombre = BC000212_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = BC000212_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = BC000212_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = BC000212_n17CoachPaciente_PacientePApellid[0];
            A18CoachPaciente_FechaAsignacion = BC000212_A18CoachPaciente_FechaAsignacion[0];
            n18CoachPaciente_FechaAsignacion = BC000212_n18CoachPaciente_FechaAsignacion[0];
            A19CoachPaciente_DispositivoID = BC000212_A19CoachPaciente_DispositivoID[0];
            n19CoachPaciente_DispositivoID = BC000212_n19CoachPaciente_DispositivoID[0];
            A20CoachPaciente_Token = BC000212_A20CoachPaciente_Token[0];
            n20CoachPaciente_Token = BC000212_n20CoachPaciente_Token[0];
            A12CoachPaciente_Coach = (Guid)((Guid)(BC000212_A12CoachPaciente_Coach[0]));
            A13CoachPaciente_Paciente = (Guid)((Guid)(BC000212_A13CoachPaciente_Paciente[0]));
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext022( )
      {
         /* Scan next routine */
         pr_default.readNext(10);
         RcdFound2 = 0;
         ScanKeyLoad022( ) ;
      }

      protected void ScanKeyLoad022( )
      {
         sMode2 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(10) != 101) )
         {
            RcdFound2 = 1;
            A14CoachPaciente_CoachPNombre = BC000212_A14CoachPaciente_CoachPNombre[0];
            A15CoachPaciente_CoachPApellido = BC000212_A15CoachPaciente_CoachPApellido[0];
            A16CoachPaciente_PacientePNombre = BC000212_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = BC000212_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = BC000212_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = BC000212_n17CoachPaciente_PacientePApellid[0];
            A18CoachPaciente_FechaAsignacion = BC000212_A18CoachPaciente_FechaAsignacion[0];
            n18CoachPaciente_FechaAsignacion = BC000212_n18CoachPaciente_FechaAsignacion[0];
            A19CoachPaciente_DispositivoID = BC000212_A19CoachPaciente_DispositivoID[0];
            n19CoachPaciente_DispositivoID = BC000212_n19CoachPaciente_DispositivoID[0];
            A20CoachPaciente_Token = BC000212_A20CoachPaciente_Token[0];
            n20CoachPaciente_Token = BC000212_n20CoachPaciente_Token[0];
            A12CoachPaciente_Coach = (Guid)((Guid)(BC000212_A12CoachPaciente_Coach[0]));
            A13CoachPaciente_Paciente = (Guid)((Guid)(BC000212_A13CoachPaciente_Paciente[0]));
         }
         Gx_mode = sMode2;
      }

      protected void ScanKeyEnd022( )
      {
         pr_default.close(10);
      }

      protected void AfterConfirm022( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert022( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate022( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete022( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete022( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate022( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes022( )
      {
      }

      protected void send_integrity_lvl_hashes022( )
      {
      }

      protected void AddRow022( )
      {
         VarsToRow2( bcCoachPaciente) ;
      }

      protected void ReadRow022( )
      {
         RowToVars2( bcCoachPaciente, 1) ;
      }

      protected void InitializeNonKey022( )
      {
         A14CoachPaciente_CoachPNombre = "";
         A15CoachPaciente_CoachPApellido = "";
         A16CoachPaciente_PacientePNombre = "";
         n16CoachPaciente_PacientePNombre = false;
         A17CoachPaciente_PacientePApellid = "";
         n17CoachPaciente_PacientePApellid = false;
         A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         n18CoachPaciente_FechaAsignacion = false;
         A19CoachPaciente_DispositivoID = "";
         n19CoachPaciente_DispositivoID = false;
         A20CoachPaciente_Token = "";
         n20CoachPaciente_Token = false;
         Z14CoachPaciente_CoachPNombre = "";
         Z15CoachPaciente_CoachPApellido = "";
         Z18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         Z19CoachPaciente_DispositivoID = "";
         Z20CoachPaciente_Token = "";
      }

      protected void InitAll022( )
      {
         A12CoachPaciente_Coach = (Guid)(Guid.Empty);
         A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         InitializeNonKey022( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow2( SdtCoachPaciente obj2 )
      {
         obj2.gxTpr_Mode = Gx_mode;
         obj2.gxTpr_Coachpaciente_coachpnombre = A14CoachPaciente_CoachPNombre;
         obj2.gxTpr_Coachpaciente_coachpapellido = A15CoachPaciente_CoachPApellido;
         obj2.gxTpr_Coachpaciente_pacientepnombre = A16CoachPaciente_PacientePNombre;
         obj2.gxTpr_Coachpaciente_pacientepapellido = A17CoachPaciente_PacientePApellid;
         obj2.gxTpr_Coachpaciente_fechaasignacion = A18CoachPaciente_FechaAsignacion;
         obj2.gxTpr_Coachpaciente_dispositivoid = A19CoachPaciente_DispositivoID;
         obj2.gxTpr_Coachpaciente_token = A20CoachPaciente_Token;
         obj2.gxTpr_Coachpaciente_coach = (Guid)(A12CoachPaciente_Coach);
         obj2.gxTpr_Coachpaciente_paciente = (Guid)(A13CoachPaciente_Paciente);
         obj2.gxTpr_Coachpaciente_coach_Z = (Guid)(Z12CoachPaciente_Coach);
         obj2.gxTpr_Coachpaciente_coachpnombre_Z = Z14CoachPaciente_CoachPNombre;
         obj2.gxTpr_Coachpaciente_coachpapellido_Z = Z15CoachPaciente_CoachPApellido;
         obj2.gxTpr_Coachpaciente_paciente_Z = (Guid)(Z13CoachPaciente_Paciente);
         obj2.gxTpr_Coachpaciente_pacientepnombre_Z = Z16CoachPaciente_PacientePNombre;
         obj2.gxTpr_Coachpaciente_pacientepapellido_Z = Z17CoachPaciente_PacientePApellid;
         obj2.gxTpr_Coachpaciente_fechaasignacion_Z = Z18CoachPaciente_FechaAsignacion;
         obj2.gxTpr_Coachpaciente_dispositivoid_Z = Z19CoachPaciente_DispositivoID;
         obj2.gxTpr_Coachpaciente_token_Z = Z20CoachPaciente_Token;
         obj2.gxTpr_Coachpaciente_pacientepnombre_N = (short)(Convert.ToInt16(n16CoachPaciente_PacientePNombre));
         obj2.gxTpr_Coachpaciente_pacientepapellido_N = (short)(Convert.ToInt16(n17CoachPaciente_PacientePApellid));
         obj2.gxTpr_Coachpaciente_fechaasignacion_N = (short)(Convert.ToInt16(n18CoachPaciente_FechaAsignacion));
         obj2.gxTpr_Coachpaciente_dispositivoid_N = (short)(Convert.ToInt16(n19CoachPaciente_DispositivoID));
         obj2.gxTpr_Coachpaciente_token_N = (short)(Convert.ToInt16(n20CoachPaciente_Token));
         obj2.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow2( SdtCoachPaciente obj2 )
      {
         obj2.gxTpr_Coachpaciente_coach = (Guid)(A12CoachPaciente_Coach);
         obj2.gxTpr_Coachpaciente_paciente = (Guid)(A13CoachPaciente_Paciente);
         return  ;
      }

      public void RowToVars2( SdtCoachPaciente obj2 ,
                              int forceLoad )
      {
         Gx_mode = obj2.gxTpr_Mode;
         A14CoachPaciente_CoachPNombre = obj2.gxTpr_Coachpaciente_coachpnombre;
         A15CoachPaciente_CoachPApellido = obj2.gxTpr_Coachpaciente_coachpapellido;
         A16CoachPaciente_PacientePNombre = obj2.gxTpr_Coachpaciente_pacientepnombre;
         n16CoachPaciente_PacientePNombre = false;
         A17CoachPaciente_PacientePApellid = obj2.gxTpr_Coachpaciente_pacientepapellido;
         n17CoachPaciente_PacientePApellid = false;
         A18CoachPaciente_FechaAsignacion = obj2.gxTpr_Coachpaciente_fechaasignacion;
         n18CoachPaciente_FechaAsignacion = false;
         A19CoachPaciente_DispositivoID = obj2.gxTpr_Coachpaciente_dispositivoid;
         n19CoachPaciente_DispositivoID = false;
         A20CoachPaciente_Token = obj2.gxTpr_Coachpaciente_token;
         n20CoachPaciente_Token = false;
         A12CoachPaciente_Coach = (Guid)(obj2.gxTpr_Coachpaciente_coach);
         A13CoachPaciente_Paciente = (Guid)(obj2.gxTpr_Coachpaciente_paciente);
         Z12CoachPaciente_Coach = (Guid)(obj2.gxTpr_Coachpaciente_coach_Z);
         Z14CoachPaciente_CoachPNombre = obj2.gxTpr_Coachpaciente_coachpnombre_Z;
         Z15CoachPaciente_CoachPApellido = obj2.gxTpr_Coachpaciente_coachpapellido_Z;
         Z13CoachPaciente_Paciente = (Guid)(obj2.gxTpr_Coachpaciente_paciente_Z);
         Z16CoachPaciente_PacientePNombre = obj2.gxTpr_Coachpaciente_pacientepnombre_Z;
         Z17CoachPaciente_PacientePApellid = obj2.gxTpr_Coachpaciente_pacientepapellido_Z;
         Z18CoachPaciente_FechaAsignacion = obj2.gxTpr_Coachpaciente_fechaasignacion_Z;
         Z19CoachPaciente_DispositivoID = obj2.gxTpr_Coachpaciente_dispositivoid_Z;
         Z20CoachPaciente_Token = obj2.gxTpr_Coachpaciente_token_Z;
         n16CoachPaciente_PacientePNombre = (bool)(Convert.ToBoolean(obj2.gxTpr_Coachpaciente_pacientepnombre_N));
         n17CoachPaciente_PacientePApellid = (bool)(Convert.ToBoolean(obj2.gxTpr_Coachpaciente_pacientepapellido_N));
         n18CoachPaciente_FechaAsignacion = (bool)(Convert.ToBoolean(obj2.gxTpr_Coachpaciente_fechaasignacion_N));
         n19CoachPaciente_DispositivoID = (bool)(Convert.ToBoolean(obj2.gxTpr_Coachpaciente_dispositivoid_N));
         n20CoachPaciente_Token = (bool)(Convert.ToBoolean(obj2.gxTpr_Coachpaciente_token_N));
         Gx_mode = obj2.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A12CoachPaciente_Coach = (Guid)((Guid)getParm(obj,0));
         A13CoachPaciente_Paciente = (Guid)((Guid)getParm(obj,1));
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey022( ) ;
         ScanKeyStart022( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000213 */
            pr_default.execute(11, new Object[] {A12CoachPaciente_Coach});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("No existe 'Coach Paciente Coach ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_COACH");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC000211 */
            pr_default.execute(9, new Object[] {A13CoachPaciente_Paciente});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("No existe 'Coach Paciente Paciente ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_PACIENTE");
               AnyError = 1;
            }
            A16CoachPaciente_PacientePNombre = BC000211_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = BC000211_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = BC000211_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = BC000211_n17CoachPaciente_PacientePApellid[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z12CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
            Z13CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
         }
         ZM022( -4) ;
         OnLoadActions022( ) ;
         AddRow022( ) ;
         ScanKeyEnd022( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars2( bcCoachPaciente, 0) ;
         ScanKeyStart022( ) ;
         if ( RcdFound2 == 0 )
         {
            Gx_mode = "INS";
            /* Using cursor BC000213 */
            pr_default.execute(11, new Object[] {A12CoachPaciente_Coach});
            if ( (pr_default.getStatus(11) == 101) )
            {
               GX_msglist.addItem("No existe 'Coach Paciente Coach ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_COACH");
               AnyError = 1;
            }
            pr_default.close(11);
            /* Using cursor BC000211 */
            pr_default.execute(9, new Object[] {A13CoachPaciente_Paciente});
            if ( (pr_default.getStatus(9) == 101) )
            {
               GX_msglist.addItem("No existe 'Coach Paciente Paciente ST'.", "ForeignKeyNotFound", 1, "COACHPACIENTE_PACIENTE");
               AnyError = 1;
            }
            A16CoachPaciente_PacientePNombre = BC000211_A16CoachPaciente_PacientePNombre[0];
            n16CoachPaciente_PacientePNombre = BC000211_n16CoachPaciente_PacientePNombre[0];
            A17CoachPaciente_PacientePApellid = BC000211_A17CoachPaciente_PacientePApellid[0];
            n17CoachPaciente_PacientePApellid = BC000211_n17CoachPaciente_PacientePApellid[0];
            pr_default.close(9);
         }
         else
         {
            Gx_mode = "UPD";
            Z12CoachPaciente_Coach = (Guid)(A12CoachPaciente_Coach);
            Z13CoachPaciente_Paciente = (Guid)(A13CoachPaciente_Paciente);
         }
         ZM022( -4) ;
         OnLoadActions022( ) ;
         AddRow022( ) ;
         ScanKeyEnd022( ) ;
         if ( RcdFound2 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      protected void SaveImpl( )
      {
         nKeyPressed = 1;
         GetKey022( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert022( ) ;
         }
         else
         {
            if ( RcdFound2 == 1 )
            {
               if ( ( A12CoachPaciente_Coach != Z12CoachPaciente_Coach ) || ( A13CoachPaciente_Paciente != Z13CoachPaciente_Paciente ) )
               {
                  A12CoachPaciente_Coach = (Guid)(Z12CoachPaciente_Coach);
                  A13CoachPaciente_Paciente = (Guid)(Z13CoachPaciente_Paciente);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update022( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( ( A12CoachPaciente_Coach != Z12CoachPaciente_Coach ) || ( A13CoachPaciente_Paciente != Z13CoachPaciente_Paciente ) )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert022( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert022( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcCoachPaciente, 0) ;
         SaveImpl( ) ;
         VarsToRow2( bcCoachPaciente) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public bool Insert( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcCoachPaciente, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert022( ) ;
         AfterTrn( ) ;
         VarsToRow2( bcCoachPaciente) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      protected void UpdateImpl( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            SaveImpl( ) ;
         }
         else
         {
            SdtCoachPaciente auxBC = new SdtCoachPaciente(context) ;
            auxBC.Load(A12CoachPaciente_Coach, A13CoachPaciente_Paciente);
            auxBC.UpdateDirties(bcCoachPaciente);
            auxBC.Save();
            IGxSilentTrn auxTrn = auxBC.getTransaction() ;
            LclMsgLst = (msglist)(auxTrn.GetMessages());
            AnyError = (short)(auxTrn.Errors());
            Gx_mode = auxTrn.GetMode();
            context.GX_msglist = LclMsgLst;
            AfterTrn( ) ;
         }
      }

      public bool Update( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcCoachPaciente, 0) ;
         UpdateImpl( ) ;
         VarsToRow2( bcCoachPaciente) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public bool InsertOrUpdate( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars2( bcCoachPaciente, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert022( ) ;
         if ( AnyError == 1 )
         {
            AnyError = 0;
            context.GX_msglist.removeAllItems();
            UpdateImpl( ) ;
         }
         else
         {
            AfterTrn( ) ;
         }
         VarsToRow2( bcCoachPaciente) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars2( bcCoachPaciente, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey022( ) ;
         if ( RcdFound2 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( ( A12CoachPaciente_Coach != Z12CoachPaciente_Coach ) || ( A13CoachPaciente_Paciente != Z13CoachPaciente_Paciente ) )
            {
               A12CoachPaciente_Coach = (Guid)(Z12CoachPaciente_Coach);
               A13CoachPaciente_Paciente = (Guid)(Z13CoachPaciente_Paciente);
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( ( A12CoachPaciente_Coach != Z12CoachPaciente_Coach ) || ( A13CoachPaciente_Paciente != Z13CoachPaciente_Paciente ) )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
         pr_gam.rollback( "CoachPaciente_BC");
         pr_default.rollback( "CoachPaciente_BC");
         VarsToRow2( bcCoachPaciente) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcCoachPaciente.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcCoachPaciente.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcCoachPaciente )
         {
            bcCoachPaciente = (SdtCoachPaciente)(sdt);
            if ( StringUtil.StrCmp(bcCoachPaciente.gxTpr_Mode, "") == 0 )
            {
               bcCoachPaciente.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow2( bcCoachPaciente) ;
            }
            else
            {
               RowToVars2( bcCoachPaciente, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcCoachPaciente.gxTpr_Mode, "") == 0 )
            {
               bcCoachPaciente.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars2( bcCoachPaciente, 1) ;
         return  ;
      }

      public SdtCoachPaciente CoachPaciente_BC
      {
         get {
            return bcCoachPaciente ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "coachpaciente_Execute" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(11);
         pr_default.close(9);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z12CoachPaciente_Coach = (Guid)(Guid.Empty);
         A12CoachPaciente_Coach = (Guid)(Guid.Empty);
         Z13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         A13CoachPaciente_Paciente = (Guid)(Guid.Empty);
         Z14CoachPaciente_CoachPNombre = "";
         A14CoachPaciente_CoachPNombre = "";
         Z15CoachPaciente_CoachPApellido = "";
         A15CoachPaciente_CoachPApellido = "";
         Z18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         A18CoachPaciente_FechaAsignacion = DateTime.MinValue;
         Z19CoachPaciente_DispositivoID = "";
         A19CoachPaciente_DispositivoID = "";
         Z20CoachPaciente_Token = "";
         A20CoachPaciente_Token = "";
         Z16CoachPaciente_PacientePNombre = "";
         A16CoachPaciente_PacientePNombre = "";
         Z17CoachPaciente_PacientePApellid = "";
         A17CoachPaciente_PacientePApellid = "";
         BC00026_A14CoachPaciente_CoachPNombre = new String[] {""} ;
         BC00026_A15CoachPaciente_CoachPApellido = new String[] {""} ;
         BC00026_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         BC00026_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         BC00026_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         BC00026_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         BC00026_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         BC00026_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         BC00026_A19CoachPaciente_DispositivoID = new String[] {""} ;
         BC00026_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         BC00026_A20CoachPaciente_Token = new String[] {""} ;
         BC00026_n20CoachPaciente_Token = new bool[] {false} ;
         BC00026_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC00026_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         BC00024_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC00025_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         BC00025_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         BC00025_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         BC00025_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         BC00027_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC00027_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         BC00023_A14CoachPaciente_CoachPNombre = new String[] {""} ;
         BC00023_A15CoachPaciente_CoachPApellido = new String[] {""} ;
         BC00023_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         BC00023_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         BC00023_A19CoachPaciente_DispositivoID = new String[] {""} ;
         BC00023_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         BC00023_A20CoachPaciente_Token = new String[] {""} ;
         BC00023_n20CoachPaciente_Token = new bool[] {false} ;
         BC00023_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC00023_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         sMode2 = "";
         BC00022_A14CoachPaciente_CoachPNombre = new String[] {""} ;
         BC00022_A15CoachPaciente_CoachPApellido = new String[] {""} ;
         BC00022_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         BC00022_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         BC00022_A19CoachPaciente_DispositivoID = new String[] {""} ;
         BC00022_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         BC00022_A20CoachPaciente_Token = new String[] {""} ;
         BC00022_n20CoachPaciente_Token = new bool[] {false} ;
         BC00022_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC00022_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         BC000211_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         BC000211_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         BC000211_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         BC000211_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         BC000212_A14CoachPaciente_CoachPNombre = new String[] {""} ;
         BC000212_A15CoachPaciente_CoachPApellido = new String[] {""} ;
         BC000212_A16CoachPaciente_PacientePNombre = new String[] {""} ;
         BC000212_n16CoachPaciente_PacientePNombre = new bool[] {false} ;
         BC000212_A17CoachPaciente_PacientePApellid = new String[] {""} ;
         BC000212_n17CoachPaciente_PacientePApellid = new bool[] {false} ;
         BC000212_A18CoachPaciente_FechaAsignacion = new DateTime[] {DateTime.MinValue} ;
         BC000212_n18CoachPaciente_FechaAsignacion = new bool[] {false} ;
         BC000212_A19CoachPaciente_DispositivoID = new String[] {""} ;
         BC000212_n19CoachPaciente_DispositivoID = new bool[] {false} ;
         BC000212_A20CoachPaciente_Token = new String[] {""} ;
         BC000212_n20CoachPaciente_Token = new bool[] {false} ;
         BC000212_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         BC000212_A13CoachPaciente_Paciente = new Guid[] {Guid.Empty} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         BC000213_A12CoachPaciente_Coach = new Guid[] {Guid.Empty} ;
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.coachpaciente_bc__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.coachpaciente_bc__default(),
            new Object[][] {
                new Object[] {
               BC00022_A14CoachPaciente_CoachPNombre, BC00022_A15CoachPaciente_CoachPApellido, BC00022_A18CoachPaciente_FechaAsignacion, BC00022_n18CoachPaciente_FechaAsignacion, BC00022_A19CoachPaciente_DispositivoID, BC00022_n19CoachPaciente_DispositivoID, BC00022_A20CoachPaciente_Token, BC00022_n20CoachPaciente_Token, BC00022_A12CoachPaciente_Coach, BC00022_A13CoachPaciente_Paciente
               }
               , new Object[] {
               BC00023_A14CoachPaciente_CoachPNombre, BC00023_A15CoachPaciente_CoachPApellido, BC00023_A18CoachPaciente_FechaAsignacion, BC00023_n18CoachPaciente_FechaAsignacion, BC00023_A19CoachPaciente_DispositivoID, BC00023_n19CoachPaciente_DispositivoID, BC00023_A20CoachPaciente_Token, BC00023_n20CoachPaciente_Token, BC00023_A12CoachPaciente_Coach, BC00023_A13CoachPaciente_Paciente
               }
               , new Object[] {
               BC00024_A12CoachPaciente_Coach
               }
               , new Object[] {
               BC00025_A16CoachPaciente_PacientePNombre, BC00025_n16CoachPaciente_PacientePNombre, BC00025_A17CoachPaciente_PacientePApellid, BC00025_n17CoachPaciente_PacientePApellid
               }
               , new Object[] {
               BC00026_A14CoachPaciente_CoachPNombre, BC00026_A15CoachPaciente_CoachPApellido, BC00026_A16CoachPaciente_PacientePNombre, BC00026_n16CoachPaciente_PacientePNombre, BC00026_A17CoachPaciente_PacientePApellid, BC00026_n17CoachPaciente_PacientePApellid, BC00026_A18CoachPaciente_FechaAsignacion, BC00026_n18CoachPaciente_FechaAsignacion, BC00026_A19CoachPaciente_DispositivoID, BC00026_n19CoachPaciente_DispositivoID,
               BC00026_A20CoachPaciente_Token, BC00026_n20CoachPaciente_Token, BC00026_A12CoachPaciente_Coach, BC00026_A13CoachPaciente_Paciente
               }
               , new Object[] {
               BC00027_A12CoachPaciente_Coach, BC00027_A13CoachPaciente_Paciente
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000211_A16CoachPaciente_PacientePNombre, BC000211_n16CoachPaciente_PacientePNombre, BC000211_A17CoachPaciente_PacientePApellid, BC000211_n17CoachPaciente_PacientePApellid
               }
               , new Object[] {
               BC000212_A14CoachPaciente_CoachPNombre, BC000212_A15CoachPaciente_CoachPApellido, BC000212_A16CoachPaciente_PacientePNombre, BC000212_n16CoachPaciente_PacientePNombre, BC000212_A17CoachPaciente_PacientePApellid, BC000212_n17CoachPaciente_PacientePApellid, BC000212_A18CoachPaciente_FechaAsignacion, BC000212_n18CoachPaciente_FechaAsignacion, BC000212_A19CoachPaciente_DispositivoID, BC000212_n19CoachPaciente_DispositivoID,
               BC000212_A20CoachPaciente_Token, BC000212_n20CoachPaciente_Token, BC000212_A12CoachPaciente_Coach, BC000212_A13CoachPaciente_Paciente
               }
               , new Object[] {
               BC000213_A12CoachPaciente_Coach
               }
            }
         );
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short RcdFound2 ;
      private int trnEnded ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode2 ;
      private DateTime Z18CoachPaciente_FechaAsignacion ;
      private DateTime A18CoachPaciente_FechaAsignacion ;
      private bool n16CoachPaciente_PacientePNombre ;
      private bool n17CoachPaciente_PacientePApellid ;
      private bool n18CoachPaciente_FechaAsignacion ;
      private bool n19CoachPaciente_DispositivoID ;
      private bool n20CoachPaciente_Token ;
      private String Z14CoachPaciente_CoachPNombre ;
      private String A14CoachPaciente_CoachPNombre ;
      private String Z15CoachPaciente_CoachPApellido ;
      private String A15CoachPaciente_CoachPApellido ;
      private String Z19CoachPaciente_DispositivoID ;
      private String A19CoachPaciente_DispositivoID ;
      private String Z20CoachPaciente_Token ;
      private String A20CoachPaciente_Token ;
      private String Z16CoachPaciente_PacientePNombre ;
      private String A16CoachPaciente_PacientePNombre ;
      private String Z17CoachPaciente_PacientePApellid ;
      private String A17CoachPaciente_PacientePApellid ;
      private Guid Z12CoachPaciente_Coach ;
      private Guid A12CoachPaciente_Coach ;
      private Guid Z13CoachPaciente_Paciente ;
      private Guid A13CoachPaciente_Paciente ;
      private SdtCoachPaciente bcCoachPaciente ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private String[] BC00026_A14CoachPaciente_CoachPNombre ;
      private String[] BC00026_A15CoachPaciente_CoachPApellido ;
      private String[] BC00026_A16CoachPaciente_PacientePNombre ;
      private bool[] BC00026_n16CoachPaciente_PacientePNombre ;
      private String[] BC00026_A17CoachPaciente_PacientePApellid ;
      private bool[] BC00026_n17CoachPaciente_PacientePApellid ;
      private DateTime[] BC00026_A18CoachPaciente_FechaAsignacion ;
      private bool[] BC00026_n18CoachPaciente_FechaAsignacion ;
      private String[] BC00026_A19CoachPaciente_DispositivoID ;
      private bool[] BC00026_n19CoachPaciente_DispositivoID ;
      private String[] BC00026_A20CoachPaciente_Token ;
      private bool[] BC00026_n20CoachPaciente_Token ;
      private Guid[] BC00026_A12CoachPaciente_Coach ;
      private Guid[] BC00026_A13CoachPaciente_Paciente ;
      private Guid[] BC00024_A12CoachPaciente_Coach ;
      private String[] BC00025_A16CoachPaciente_PacientePNombre ;
      private bool[] BC00025_n16CoachPaciente_PacientePNombre ;
      private String[] BC00025_A17CoachPaciente_PacientePApellid ;
      private bool[] BC00025_n17CoachPaciente_PacientePApellid ;
      private Guid[] BC00027_A12CoachPaciente_Coach ;
      private Guid[] BC00027_A13CoachPaciente_Paciente ;
      private String[] BC00023_A14CoachPaciente_CoachPNombre ;
      private String[] BC00023_A15CoachPaciente_CoachPApellido ;
      private DateTime[] BC00023_A18CoachPaciente_FechaAsignacion ;
      private bool[] BC00023_n18CoachPaciente_FechaAsignacion ;
      private String[] BC00023_A19CoachPaciente_DispositivoID ;
      private bool[] BC00023_n19CoachPaciente_DispositivoID ;
      private String[] BC00023_A20CoachPaciente_Token ;
      private bool[] BC00023_n20CoachPaciente_Token ;
      private Guid[] BC00023_A12CoachPaciente_Coach ;
      private Guid[] BC00023_A13CoachPaciente_Paciente ;
      private String[] BC00022_A14CoachPaciente_CoachPNombre ;
      private String[] BC00022_A15CoachPaciente_CoachPApellido ;
      private DateTime[] BC00022_A18CoachPaciente_FechaAsignacion ;
      private bool[] BC00022_n18CoachPaciente_FechaAsignacion ;
      private String[] BC00022_A19CoachPaciente_DispositivoID ;
      private bool[] BC00022_n19CoachPaciente_DispositivoID ;
      private String[] BC00022_A20CoachPaciente_Token ;
      private bool[] BC00022_n20CoachPaciente_Token ;
      private Guid[] BC00022_A12CoachPaciente_Coach ;
      private Guid[] BC00022_A13CoachPaciente_Paciente ;
      private String[] BC000211_A16CoachPaciente_PacientePNombre ;
      private bool[] BC000211_n16CoachPaciente_PacientePNombre ;
      private String[] BC000211_A17CoachPaciente_PacientePApellid ;
      private bool[] BC000211_n17CoachPaciente_PacientePApellid ;
      private String[] BC000212_A14CoachPaciente_CoachPNombre ;
      private String[] BC000212_A15CoachPaciente_CoachPApellido ;
      private String[] BC000212_A16CoachPaciente_PacientePNombre ;
      private bool[] BC000212_n16CoachPaciente_PacientePNombre ;
      private String[] BC000212_A17CoachPaciente_PacientePApellid ;
      private bool[] BC000212_n17CoachPaciente_PacientePApellid ;
      private DateTime[] BC000212_A18CoachPaciente_FechaAsignacion ;
      private bool[] BC000212_n18CoachPaciente_FechaAsignacion ;
      private String[] BC000212_A19CoachPaciente_DispositivoID ;
      private bool[] BC000212_n19CoachPaciente_DispositivoID ;
      private String[] BC000212_A20CoachPaciente_Token ;
      private bool[] BC000212_n20CoachPaciente_Token ;
      private Guid[] BC000212_A12CoachPaciente_Coach ;
      private Guid[] BC000212_A13CoachPaciente_Paciente ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private Guid[] BC000213_A12CoachPaciente_Coach ;
      private IDataStoreProvider pr_gam ;
   }

   public class coachpaciente_bc__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class coachpaciente_bc__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new UpdateCursor(def[6])
       ,new UpdateCursor(def[7])
       ,new UpdateCursor(def[8])
       ,new ForEachCursor(def[9])
       ,new ForEachCursor(def[10])
       ,new ForEachCursor(def[11])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmBC00026 ;
        prmBC00026 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00024 ;
        prmBC00024 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00025 ;
        prmBC00025 = new Object[] {
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00027 ;
        prmBC00027 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00023 ;
        prmBC00023 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00022 ;
        prmBC00022 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00028 ;
        prmBC00028 = new Object[] {
        new Object[] {"@CoachPaciente_CoachPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_CoachPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_FechaAsignacion",SqlDbType.DateTime,8,0} ,
        new Object[] {"@CoachPaciente_DispositivoID",SqlDbType.VarChar,75,0} ,
        new Object[] {"@CoachPaciente_Token",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00029 ;
        prmBC00029 = new Object[] {
        new Object[] {"@CoachPaciente_CoachPNombre",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_CoachPApellido",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_FechaAsignacion",SqlDbType.DateTime,8,0} ,
        new Object[] {"@CoachPaciente_DispositivoID",SqlDbType.VarChar,75,0} ,
        new Object[] {"@CoachPaciente_Token",SqlDbType.VarChar,40,0} ,
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000210 ;
        prmBC000210 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000212 ;
        prmBC000212 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000213 ;
        prmBC000213 = new Object[] {
        new Object[] {"@CoachPaciente_Coach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000211 ;
        prmBC000211 = new Object[] {
        new Object[] {"@CoachPaciente_Paciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("BC00022", "SELECT [CoachPaciente_CoachPNombre], [CoachPaciente_CoachPApellido], [CoachPaciente_FechaAsignacion], [CoachPaciente_DispositivoID], [CoachPaciente_Token], [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (UPDLOCK) WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00022,1,0,true,false )
           ,new CursorDef("BC00023", "SELECT [CoachPaciente_CoachPNombre], [CoachPaciente_CoachPApellido], [CoachPaciente_FechaAsignacion], [CoachPaciente_DispositivoID], [CoachPaciente_Token], [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00023,1,0,true,false )
           ,new CursorDef("BC00024", "SELECT [PersonaID] AS CoachPaciente_Coach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Coach ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00024,1,0,true,false )
           ,new CursorDef("BC00025", "SELECT [PersonaPNombre] AS CoachPaciente_PacientePNombre, [PersonaPApellido] AS CoachPaciente_PacientePApellid FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00025,1,0,true,false )
           ,new CursorDef("BC00026", "SELECT TM1.[CoachPaciente_CoachPNombre], TM1.[CoachPaciente_CoachPApellido], T2.[PersonaPNombre] AS CoachPaciente_PacientePNombre, T2.[PersonaPApellido] AS CoachPaciente_PacientePApellid, TM1.[CoachPaciente_FechaAsignacion], TM1.[CoachPaciente_DispositivoID], TM1.[CoachPaciente_Token], TM1.[CoachPaciente_Coach] AS CoachPaciente_Coach, TM1.[CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM ([CoachPaciente] TM1 WITH (NOLOCK) INNER JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = TM1.[CoachPaciente_Paciente]) WHERE TM1.[CoachPaciente_Coach] = @CoachPaciente_Coach and TM1.[CoachPaciente_Paciente] = @CoachPaciente_Paciente ORDER BY TM1.[CoachPaciente_Coach], TM1.[CoachPaciente_Paciente]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00026,100,0,true,false )
           ,new CursorDef("BC00027", "SELECT [CoachPaciente_Coach] AS CoachPaciente_Coach, [CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM [CoachPaciente] WITH (NOLOCK) WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00027,1,0,true,false )
           ,new CursorDef("BC00028", "INSERT INTO [CoachPaciente]([CoachPaciente_CoachPNombre], [CoachPaciente_CoachPApellido], [CoachPaciente_FechaAsignacion], [CoachPaciente_DispositivoID], [CoachPaciente_Token], [CoachPaciente_Coach], [CoachPaciente_Paciente]) VALUES(@CoachPaciente_CoachPNombre, @CoachPaciente_CoachPApellido, @CoachPaciente_FechaAsignacion, @CoachPaciente_DispositivoID, @CoachPaciente_Token, @CoachPaciente_Coach, @CoachPaciente_Paciente)", GxErrorMask.GX_NOMASK,prmBC00028)
           ,new CursorDef("BC00029", "UPDATE [CoachPaciente] SET [CoachPaciente_CoachPNombre]=@CoachPaciente_CoachPNombre, [CoachPaciente_CoachPApellido]=@CoachPaciente_CoachPApellido, [CoachPaciente_FechaAsignacion]=@CoachPaciente_FechaAsignacion, [CoachPaciente_DispositivoID]=@CoachPaciente_DispositivoID, [CoachPaciente_Token]=@CoachPaciente_Token  WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente", GxErrorMask.GX_NOMASK,prmBC00029)
           ,new CursorDef("BC000210", "DELETE FROM [CoachPaciente]  WHERE [CoachPaciente_Coach] = @CoachPaciente_Coach AND [CoachPaciente_Paciente] = @CoachPaciente_Paciente", GxErrorMask.GX_NOMASK,prmBC000210)
           ,new CursorDef("BC000211", "SELECT [PersonaPNombre] AS CoachPaciente_PacientePNombre, [PersonaPApellido] AS CoachPaciente_PacientePApellid FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Paciente ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000211,1,0,true,false )
           ,new CursorDef("BC000212", "SELECT TM1.[CoachPaciente_CoachPNombre], TM1.[CoachPaciente_CoachPApellido], T2.[PersonaPNombre] AS CoachPaciente_PacientePNombre, T2.[PersonaPApellido] AS CoachPaciente_PacientePApellid, TM1.[CoachPaciente_FechaAsignacion], TM1.[CoachPaciente_DispositivoID], TM1.[CoachPaciente_Token], TM1.[CoachPaciente_Coach] AS CoachPaciente_Coach, TM1.[CoachPaciente_Paciente] AS CoachPaciente_Paciente FROM ([CoachPaciente] TM1 WITH (NOLOCK) INNER JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = TM1.[CoachPaciente_Paciente]) WHERE TM1.[CoachPaciente_Coach] = @CoachPaciente_Coach and TM1.[CoachPaciente_Paciente] = @CoachPaciente_Paciente ORDER BY TM1.[CoachPaciente_Coach], TM1.[CoachPaciente_Paciente]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000212,100,0,true,false )
           ,new CursorDef("BC000213", "SELECT [PersonaID] AS CoachPaciente_Coach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @CoachPaciente_Coach ",true, GxErrorMask.GX_NOMASK, false, this,prmBC000213,1,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((Guid[]) buf[8])[0] = rslt.getGuid(6) ;
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              return;
           case 1 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((DateTime[]) buf[2])[0] = rslt.getGXDate(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((Guid[]) buf[8])[0] = rslt.getGuid(6) ;
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 3 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(2);
              return;
           case 4 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((Guid[]) buf[12])[0] = rslt.getGuid(8) ;
              ((Guid[]) buf[13])[0] = rslt.getGuid(9) ;
              return;
           case 5 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 9 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              ((String[]) buf[2])[0] = rslt.getVarchar(2) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(2);
              return;
           case 10 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((String[]) buf[2])[0] = rslt.getVarchar(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((DateTime[]) buf[6])[0] = rslt.getGXDate(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((String[]) buf[8])[0] = rslt.getVarchar(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((String[]) buf[10])[0] = rslt.getVarchar(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((Guid[]) buf[12])[0] = rslt.getGuid(8) ;
              ((Guid[]) buf[13])[0] = rslt.getGuid(9) ;
              return;
           case 11 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 2 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 3 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 5 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 6 :
              stmt.SetParameter(1, (String)parms[0]);
              stmt.SetParameter(2, (String)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(3, (DateTime)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              stmt.SetParameter(6, (Guid)parms[8]);
              stmt.SetParameter(7, (Guid)parms[9]);
              return;
           case 7 :
              stmt.SetParameter(1, (String)parms[0]);
              stmt.SetParameter(2, (String)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameter(3, (DateTime)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              stmt.SetParameter(6, (Guid)parms[8]);
              stmt.SetParameter(7, (Guid)parms[9]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 9 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 10 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 11 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
