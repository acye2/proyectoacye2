/*
               File: K2BSaveGridColumns
        Description: K2B Save Grid State
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:37.48
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bsavegridcolumns : GXProcedure
   {
      public k2bsavegridcolumns( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public k2bsavegridcolumns( IGxContext context )
      {
         this.context = context;
         IsMain = false;
      }

      public void release( )
      {
      }

      public void execute( String aP0_ProgramName ,
                           String aP1_GridName ,
                           GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> aP2_GridState )
      {
         this.AV11ProgramName = aP0_ProgramName;
         this.AV8GridName = aP1_GridName;
         this.AV9GridState = aP2_GridState;
         initialize();
         executePrivate();
      }

      public void executeSubmit( String aP0_ProgramName ,
                                 String aP1_GridName ,
                                 GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> aP2_GridState )
      {
         k2bsavegridcolumns objk2bsavegridcolumns;
         objk2bsavegridcolumns = new k2bsavegridcolumns();
         objk2bsavegridcolumns.AV11ProgramName = aP0_ProgramName;
         objk2bsavegridcolumns.AV8GridName = aP1_GridName;
         objk2bsavegridcolumns.AV9GridState = aP2_GridState;
         objk2bsavegridcolumns.context.SetSubmitInitialConfig(context);
         objk2bsavegridcolumns.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objk2bsavegridcolumns);
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((k2bsavegridcolumns)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV13SessionString = AV12Session.Get(StringUtil.Trim( AV11ProgramName)+StringUtil.Trim( AV8GridName)+"GridColumns");
         AV10NewSessionString = AV9GridState.ToXml(false, true, "K2BGridColumns", "PACYE2");
         if ( StringUtil.StrCmp(AV13SessionString, AV10NewSessionString) != 0 )
         {
            AV12Session.Set(StringUtil.Trim( AV11ProgramName)+StringUtil.Trim( AV8GridName)+"GridColumns", AV10NewSessionString);
            new k2bpersistgridcolumns(context ).execute(  AV11ProgramName,  AV8GridName,  AV9GridState) ;
         }
         this.cleanup();
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV13SessionString = "";
         AV12Session = context.GetSession();
         AV10NewSessionString = "";
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String AV11ProgramName ;
      private String AV8GridName ;
      private String AV13SessionString ;
      private String AV10NewSessionString ;
      private IGxSession AV12Session ;
      private GXBaseCollection<SdtK2BGridColumns_K2BGridColumnsItem> AV9GridState ;
   }

}
