function K2BAccordionMenu(jQuery)
{
	this._$ = jQuery;
	this.MenuItems;
	this.Toggle;
	this.DoubleTapGo;
	this.UseImageClassAsFontAwesome;
	_internatAccordionThis = this;
	
	// Databinding for property MenuItems
	this.SetMenuItems = function(data)
	{
		///UserCodeRegionStart:[SetMenuItems] (do not remove this comment.)
	    this.MenuItems = data;
		///UserCodeRegionEnd: (do not remove this comment.)
	}

	// Databinding for property MenuItems
	this.GetMenuItems = function()
	{
		///UserCodeRegionStart:[GetMenuItems] (do not remove this comment.)
	    return this.MenuItems;
		///UserCodeRegionEnd: (do not remove this comment.)
	}
	

	this.show = function () {
	    ///UserCodeRegionStart:[show] (do not remove this comment.)
		
		var urlComps = window.location.pathname.split("/");
		var scriptName = urlComps[urlComps.length-1];
		
		if (!this.IsPostBack) {
			var rootNode = this._$('<aside>').addClass('sidebar');
			var navNode = this._$('<nav>').addClass('sidebar-nav');
			rootNode.append(navNode);
			var ulNode = this._$('<ul>').attr('id', this.ControlName).addClass('K2BMetisMenu');
			navNode.append(ulNode);
	         this.loadMenuData(this.MenuItems, 0, ulNode);
			this.setHtml(rootNode[0].outerHTML);
			
			// Para seleccionar el item actual
			var currentWPinMenu = false;
			_internatAccordionThis._$("UL.K2BMetisMenu a").each(function(i, item){
				if(_internatAccordionThis._$(item).attr("href") == scriptName){
					currentWPinMenu = true;
				}
			});
			
			if(currentWPinMenu){
				_internatAccordionThis._$("UL.K2BMetisMenu li").removeClass("activeelement");
				_internatAccordionThis._$("UL.K2BMetisMenu a").each(function(i, item){
					if(_internatAccordionThis._$(item).attr("href") == scriptName){
						_internatAccordionThis._$(item).closest("li").addClass("activeelement");
					}
				});
			}
		
			_internatAccordionThis._$.each(_internatAccordionThis._$("UL.K2BMetisMenu a"), function(i, element){
				if ((_internatAccordionThis._$(element).attr("href")!="") && (_internatAccordionThis._$(element).attr("href")!=undefined))
				_internatAccordionThis._$(element).on("click", _internatAccordionThis.onMenuClick);
			});
			
			//
	    }
		_internatAccordionThis._$(document).ready(this.htmlClickCallback);

	    ///UserCodeRegionEnd: (do not remove this comment.)
	}
	///UserCodeRegionStart:[User Functions] (do not remove this comment.)

	this.onMenuClick = function(){ 
		_internatAccordionThis._$("UL.K2BMetisMenu li").removeClass("activeelement");
		_internatAccordionThis._$(this).closest("li").addClass("activeelement");
		
		var parent = _internatAccordionThis._$(this).closest("DIV.K2BToolsMenuContainerVisibleCompact");
		parent.removeClass("K2BToolsMenuContainerVisibleCompact");
		parent.addClass("K2BToolsMenuContainerInvisibleCompact");
	}
	this.loadMenuData = function (MenuData, step, currentNode) {
	 
	    var i = 0;
	    for (i = 0; MenuData[i] != undefined; i++) {
		
			hasFontAwesome =((MenuData[i].ImageClass!=undefined) && (MenuData[i].ImageClass)) && (this.UseImageClassAsFontAwesome);
			hasImageClass = ((MenuData[i].ImageClass!=undefined) && (MenuData[i].ImageClass)) && (!this.UseImageClassAsFontAwesome);
			hasImage = (MenuData[i].ImageUrl!=undefined) && (MenuData[i].ImageUrl);
			var liElement = _internatAccordionThis._$('<li>')
			if (!MenuData[i].ShowInExtraSmall)
			{
				liElement.addClass("InvisibleInExtraSmallMenu");
			}
			if (!MenuData[i].ShowInSmall)
			{
				liElement.addClass("InvisibleInSmallMenu");
				
			}
			if (!MenuData[i].ShowInMedium)
			{
				liElement.addClass("InvisibleInMediumMenu");
			}
			
			if (!MenuData[i].ShowInLarge)
			{
				liElement.addClass("InvisibleInLargeMenu");
				
			}
			currentNode.append(liElement);
			
			
	        if (MenuData[i].Items != undefined && MenuData[i].Items.toString() != "") 
			{
	    
				var linkElement = _internatAccordionThis._$('<a></a>')
				liElement.append(linkElement);
	    		if (hasFontAwesome)
				{
					var spanElement = _internatAccordionThis._$('<span>').addClass('sidebar-nav-item-icon').addClass('fa').addClass(MenuData[i].ImageClass);
					linkElement.append(spanElement);
					
				}
				
				if (hasImage)
				{
					
					var image = _internatAccordionThis._$('<image>').attr('src', MenuData[i].ImageUrl).addClass('sidebar-nav-item-icon').addClass('K2BMenuItemImage').addClass(MenuData[i].ImageClass);
					linkElement.append(image);
			
				}
				
				if ((hasImage )|| (hasFontAwesome))
				{
					linkElement.append(_internatAccordionThis._$("<span>").addClass("sidebar-nav-item").text(MenuData[i].Title));
					
				}
				
				else
				{
					linkElement.text(MenuData[i].Title);
				
				}
				
				
				linkElement.append(_internatAccordionThis._$('<span>').addClass('fa').addClass('arrow'));
				
				
	    		var newUl = _internatAccordionThis._$('<ul>');
				liElement.append(newUl);
	            this.loadMenuData(MenuData[i].Items, step +1, newUl);
	        }
			
			
	        else {
	           
					linkObject = _internatAccordionThis._$('<a>').attr('href', MenuData[i].Link);
				liElement.append(linkObject);
				
				
				if (hasFontAwesome)
				{
					
					var spamFontAwesome = _internatAccordionThis._$('<span>').addClass('sidebar-nav-item-icon').addClass('fa').addClass(MenuData[i].ImageClass);
					linkObject.append(spamFontAwesome);
				}
				
				if (hasImage)
				{
					imageObject = $('<image>').attr('src', MenuData[i].ImageUrl).addClass('sidebar-nav-item-icon').addClass('K2BMenuItemImage').addClass(MenuData[i].ImageClass);
					linkObject.append(imageObject);

					
				}
				linkObject.append(document.createTextNode(MenuData[i].Title));
	            
		
	        }

	    }
		return;
	
 
	}
	this.htmlClickCallback = function(e)
	{
		    _internatAccordionThis._$("#" + _internatAccordionThis.ControlName).metisMenu(
			{
				toggle: _internatAccordionThis.Toggle,
				dougleTapGo: _internatAccordionThis.DoubleTapGo
			}
			);
	}
	
	function isItemEvenOrZero(n) 
	{
		return n==0  || (n % 2 == 0);
	
	}
	
		
	
	
	///UserCodeRegionEnd: (do not remove this comment.):
	
}
