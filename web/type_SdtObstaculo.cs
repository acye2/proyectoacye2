/*
               File: type_SdtObstaculo
        Description: Obstaculo
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 8:45:20.88
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [XmlSerializerFormat]
   [XmlRoot(ElementName = "Obstaculo" )]
   [XmlType(TypeName =  "Obstaculo" , Namespace = "PACYE2" )]
   [Serializable]
   public class SdtObstaculo : GxSilentTrnSdt, System.Web.SessionState.IRequiresSessionState
   {
      public SdtObstaculo( )
      {
      }

      public SdtObstaculo( IGxContext context )
      {
         this.context = context;
         constructorCallingAssembly = Assembly.GetCallingAssembly();
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public void Load( Guid AV35ObstaculoID )
      {
         IGxSilentTrn obj ;
         obj = getTransaction();
         obj.LoadKey(new Object[] {(Guid)AV35ObstaculoID});
         return  ;
      }

      public override Object[][] GetBCKey( )
      {
         return (Object[][])(new Object[][]{new Object[]{"ObstaculoID", typeof(Guid)}}) ;
      }

      public override GXProperties GetMetadata( )
      {
         GXProperties metadata = new GXProperties() ;
         metadata.Set("Name", "Obstaculo");
         metadata.Set("BT", "Obstaculo");
         metadata.Set("PK", "[ \"ObstaculoID\" ]");
         metadata.Set("PKAssigned", "[ \"ObstaculoID\" ]");
         metadata.Set("FKList", "[ { \"FK\":[ \"PersonaID\" ],\"FKMap\":[ \"ObstaculoPacienteRegistra-PersonaID\" ] } ]");
         metadata.Set("AllowInsert", "True");
         metadata.Set("AllowUpdate", "True");
         metadata.Set("AllowDelete", "True");
         return metadata ;
      }

      public override GxStringCollection StateAttributes( )
      {
         GxStringCollection state = new GxStringCollection() ;
         state.Add("gxTpr_Mode");
         state.Add("gxTpr_Initialized");
         state.Add("gxTpr_Obstaculoid_Z");
         state.Add("gxTpr_Obstaculofecharegistro_Z_Nullable");
         state.Add("gxTpr_Obstaculotipo_Z");
         state.Add("gxTpr_Obstaculopacienteregistra_Z");
         state.Add("gxTpr_Obstaculonumeroadvertencias_Z");
         state.Add("gxTpr_Obstaculopacienteregistra_N");
         state.Add("gxTpr_Obstaculonumeroadvertencias_N");
         return state ;
      }

      public override void Copy( GxUserType source )
      {
         SdtObstaculo sdt ;
         sdt = (SdtObstaculo)(source);
         gxTv_SdtObstaculo_Obstaculoid = sdt.gxTv_SdtObstaculo_Obstaculoid ;
         gxTv_SdtObstaculo_Obstaculofecharegistro = sdt.gxTv_SdtObstaculo_Obstaculofecharegistro ;
         gxTv_SdtObstaculo_Obstaculotipo = sdt.gxTv_SdtObstaculo_Obstaculotipo ;
         gxTv_SdtObstaculo_Obstaculopacienteregistra = sdt.gxTv_SdtObstaculo_Obstaculopacienteregistra ;
         gxTv_SdtObstaculo_Obstaculolongitud = sdt.gxTv_SdtObstaculo_Obstaculolongitud ;
         gxTv_SdtObstaculo_Obstaculolatitud = sdt.gxTv_SdtObstaculo_Obstaculolatitud ;
         gxTv_SdtObstaculo_Obstaculonumeroadvertencias = sdt.gxTv_SdtObstaculo_Obstaculonumeroadvertencias ;
         gxTv_SdtObstaculo_Mode = sdt.gxTv_SdtObstaculo_Mode ;
         gxTv_SdtObstaculo_Initialized = sdt.gxTv_SdtObstaculo_Initialized ;
         gxTv_SdtObstaculo_Obstaculoid_Z = sdt.gxTv_SdtObstaculo_Obstaculoid_Z ;
         gxTv_SdtObstaculo_Obstaculofecharegistro_Z = sdt.gxTv_SdtObstaculo_Obstaculofecharegistro_Z ;
         gxTv_SdtObstaculo_Obstaculotipo_Z = sdt.gxTv_SdtObstaculo_Obstaculotipo_Z ;
         gxTv_SdtObstaculo_Obstaculopacienteregistra_Z = sdt.gxTv_SdtObstaculo_Obstaculopacienteregistra_Z ;
         gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z = sdt.gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z ;
         gxTv_SdtObstaculo_Obstaculopacienteregistra_N = sdt.gxTv_SdtObstaculo_Obstaculopacienteregistra_N ;
         gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N = sdt.gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N ;
         return  ;
      }

      public override void ToJSON( )
      {
         ToJSON( true) ;
         return  ;
      }

      public override void ToJSON( bool includeState )
      {
         AddObjectProperty("ObstaculoID", gxTv_SdtObstaculo_Obstaculoid, false);
         datetime_STZ = gxTv_SdtObstaculo_Obstaculofecharegistro;
         sDateCnv = "";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "-";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + "T";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         sDateCnv = sDateCnv + ":";
         sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
         sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
         AddObjectProperty("ObstaculoFechaRegistro", sDateCnv, false);
         AddObjectProperty("ObstaculoTipo", gxTv_SdtObstaculo_Obstaculotipo, false);
         AddObjectProperty("ObstaculoPacienteRegistra", gxTv_SdtObstaculo_Obstaculopacienteregistra, false);
         AddObjectProperty("ObstaculoPacienteRegistra_N", gxTv_SdtObstaculo_Obstaculopacienteregistra_N, false);
         AddObjectProperty("ObstaculoLongitud", gxTv_SdtObstaculo_Obstaculolongitud, false);
         AddObjectProperty("ObstaculoLatitud", gxTv_SdtObstaculo_Obstaculolatitud, false);
         AddObjectProperty("ObstaculoNumeroAdvertencias", gxTv_SdtObstaculo_Obstaculonumeroadvertencias, false);
         AddObjectProperty("ObstaculoNumeroAdvertencias_N", gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N, false);
         if ( includeState )
         {
            AddObjectProperty("Mode", gxTv_SdtObstaculo_Mode, false);
            AddObjectProperty("Initialized", gxTv_SdtObstaculo_Initialized, false);
            AddObjectProperty("ObstaculoID_Z", gxTv_SdtObstaculo_Obstaculoid_Z, false);
            datetime_STZ = gxTv_SdtObstaculo_Obstaculofecharegistro_Z;
            sDateCnv = "";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Year( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "0000", 1, 4-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Month( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "-";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Day( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + "T";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Hour( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Minute( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            sDateCnv = sDateCnv + ":";
            sNumToPad = StringUtil.Trim( StringUtil.Str( (decimal)(DateTimeUtil.Second( datetime_STZ)), 10, 0));
            sDateCnv = sDateCnv + StringUtil.Substring( "00", 1, 2-StringUtil.Len( sNumToPad)) + sNumToPad;
            AddObjectProperty("ObstaculoFechaRegistro_Z", sDateCnv, false);
            AddObjectProperty("ObstaculoTipo_Z", gxTv_SdtObstaculo_Obstaculotipo_Z, false);
            AddObjectProperty("ObstaculoPacienteRegistra_Z", gxTv_SdtObstaculo_Obstaculopacienteregistra_Z, false);
            AddObjectProperty("ObstaculoNumeroAdvertencias_Z", gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z, false);
            AddObjectProperty("ObstaculoPacienteRegistra_N", gxTv_SdtObstaculo_Obstaculopacienteregistra_N, false);
            AddObjectProperty("ObstaculoNumeroAdvertencias_N", gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N, false);
         }
         return  ;
      }

      public void UpdateDirties( SdtObstaculo sdt )
      {
         if ( sdt.IsDirty("ObstaculoID") )
         {
            gxTv_SdtObstaculo_Obstaculoid = sdt.gxTv_SdtObstaculo_Obstaculoid ;
         }
         if ( sdt.IsDirty("ObstaculoFechaRegistro") )
         {
            gxTv_SdtObstaculo_Obstaculofecharegistro = sdt.gxTv_SdtObstaculo_Obstaculofecharegistro ;
         }
         if ( sdt.IsDirty("ObstaculoTipo") )
         {
            gxTv_SdtObstaculo_Obstaculotipo = sdt.gxTv_SdtObstaculo_Obstaculotipo ;
         }
         if ( sdt.IsDirty("ObstaculoPacienteRegistra") )
         {
            gxTv_SdtObstaculo_Obstaculopacienteregistra = sdt.gxTv_SdtObstaculo_Obstaculopacienteregistra ;
         }
         if ( sdt.IsDirty("ObstaculoLongitud") )
         {
            gxTv_SdtObstaculo_Obstaculolongitud = sdt.gxTv_SdtObstaculo_Obstaculolongitud ;
         }
         if ( sdt.IsDirty("ObstaculoLatitud") )
         {
            gxTv_SdtObstaculo_Obstaculolatitud = sdt.gxTv_SdtObstaculo_Obstaculolatitud ;
         }
         if ( sdt.IsDirty("ObstaculoNumeroAdvertencias") )
         {
            gxTv_SdtObstaculo_Obstaculonumeroadvertencias = sdt.gxTv_SdtObstaculo_Obstaculonumeroadvertencias ;
         }
         return  ;
      }

      [  SoapElement( ElementName = "ObstaculoID" )]
      [  XmlElement( ElementName = "ObstaculoID"   )]
      public Guid gxTpr_Obstaculoid
      {
         get {
            return gxTv_SdtObstaculo_Obstaculoid ;
         }

         set {
            if ( gxTv_SdtObstaculo_Obstaculoid != value )
            {
               gxTv_SdtObstaculo_Mode = "INS";
               this.gxTv_SdtObstaculo_Obstaculoid_Z_SetNull( );
               this.gxTv_SdtObstaculo_Obstaculofecharegistro_Z_SetNull( );
               this.gxTv_SdtObstaculo_Obstaculotipo_Z_SetNull( );
               this.gxTv_SdtObstaculo_Obstaculopacienteregistra_Z_SetNull( );
               this.gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z_SetNull( );
            }
            gxTv_SdtObstaculo_Obstaculoid = (Guid)(value);
            SetDirty("Obstaculoid");
         }

      }

      [  SoapElement( ElementName = "ObstaculoFechaRegistro" )]
      [  XmlElement( ElementName = "ObstaculoFechaRegistro"  , IsNullable=true )]
      public string gxTpr_Obstaculofecharegistro_Nullable
      {
         get {
            if ( gxTv_SdtObstaculo_Obstaculofecharegistro == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtObstaculo_Obstaculofecharegistro).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtObstaculo_Obstaculofecharegistro = DateTime.MinValue;
            else
               gxTv_SdtObstaculo_Obstaculofecharegistro = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Obstaculofecharegistro
      {
         get {
            return gxTv_SdtObstaculo_Obstaculofecharegistro ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculofecharegistro = value;
            SetDirty("Obstaculofecharegistro");
         }

      }

      [  SoapElement( ElementName = "ObstaculoTipo" )]
      [  XmlElement( ElementName = "ObstaculoTipo"   )]
      public short gxTpr_Obstaculotipo
      {
         get {
            return gxTv_SdtObstaculo_Obstaculotipo ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculotipo = value;
            SetDirty("Obstaculotipo");
         }

      }

      [  SoapElement( ElementName = "ObstaculoPacienteRegistra" )]
      [  XmlElement( ElementName = "ObstaculoPacienteRegistra"   )]
      public Guid gxTpr_Obstaculopacienteregistra
      {
         get {
            return gxTv_SdtObstaculo_Obstaculopacienteregistra ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculopacienteregistra_N = 0;
            gxTv_SdtObstaculo_Obstaculopacienteregistra = (Guid)(value);
            SetDirty("Obstaculopacienteregistra");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculopacienteregistra_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculopacienteregistra_N = 1;
         gxTv_SdtObstaculo_Obstaculopacienteregistra = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculopacienteregistra_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoLongitud" )]
      [  XmlElement( ElementName = "ObstaculoLongitud"   )]
      public String gxTpr_Obstaculolongitud
      {
         get {
            return gxTv_SdtObstaculo_Obstaculolongitud ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculolongitud = value;
            SetDirty("Obstaculolongitud");
         }

      }

      [  SoapElement( ElementName = "ObstaculoLatitud" )]
      [  XmlElement( ElementName = "ObstaculoLatitud"   )]
      public String gxTpr_Obstaculolatitud
      {
         get {
            return gxTv_SdtObstaculo_Obstaculolatitud ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculolatitud = value;
            SetDirty("Obstaculolatitud");
         }

      }

      [  SoapElement( ElementName = "ObstaculoNumeroAdvertencias" )]
      [  XmlElement( ElementName = "ObstaculoNumeroAdvertencias"   )]
      public short gxTpr_Obstaculonumeroadvertencias
      {
         get {
            return gxTv_SdtObstaculo_Obstaculonumeroadvertencias ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N = 0;
            gxTv_SdtObstaculo_Obstaculonumeroadvertencias = value;
            SetDirty("Obstaculonumeroadvertencias");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculonumeroadvertencias_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N = 1;
         gxTv_SdtObstaculo_Obstaculonumeroadvertencias = 0;
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculonumeroadvertencias_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Mode" )]
      [  XmlElement( ElementName = "Mode"   )]
      public String gxTpr_Mode
      {
         get {
            return gxTv_SdtObstaculo_Mode ;
         }

         set {
            gxTv_SdtObstaculo_Mode = value;
            SetDirty("Mode");
         }

      }

      public void gxTv_SdtObstaculo_Mode_SetNull( )
      {
         gxTv_SdtObstaculo_Mode = "";
         return  ;
      }

      public bool gxTv_SdtObstaculo_Mode_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "Initialized" )]
      [  XmlElement( ElementName = "Initialized"   )]
      public short gxTpr_Initialized
      {
         get {
            return gxTv_SdtObstaculo_Initialized ;
         }

         set {
            gxTv_SdtObstaculo_Initialized = value;
            SetDirty("Initialized");
         }

      }

      public void gxTv_SdtObstaculo_Initialized_SetNull( )
      {
         gxTv_SdtObstaculo_Initialized = 0;
         return  ;
      }

      public bool gxTv_SdtObstaculo_Initialized_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoID_Z" )]
      [  XmlElement( ElementName = "ObstaculoID_Z"   )]
      public Guid gxTpr_Obstaculoid_Z
      {
         get {
            return gxTv_SdtObstaculo_Obstaculoid_Z ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculoid_Z = (Guid)(value);
            SetDirty("Obstaculoid_Z");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculoid_Z_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculoid_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculoid_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoFechaRegistro_Z" )]
      [  XmlElement( ElementName = "ObstaculoFechaRegistro_Z"  , IsNullable=true )]
      public string gxTpr_Obstaculofecharegistro_Z_Nullable
      {
         get {
            if ( gxTv_SdtObstaculo_Obstaculofecharegistro_Z == DateTime.MinValue)
               return null;
            return new GxDatetimeString(gxTv_SdtObstaculo_Obstaculofecharegistro_Z).value ;
         }

         set {
            if (String.IsNullOrEmpty(value) || value == GxDatetimeString.NullValue )
               gxTv_SdtObstaculo_Obstaculofecharegistro_Z = DateTime.MinValue;
            else
               gxTv_SdtObstaculo_Obstaculofecharegistro_Z = DateTime.Parse( value);
         }

      }

      [SoapIgnore]
      [XmlIgnore]
      public DateTime gxTpr_Obstaculofecharegistro_Z
      {
         get {
            return gxTv_SdtObstaculo_Obstaculofecharegistro_Z ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculofecharegistro_Z = value;
            SetDirty("Obstaculofecharegistro_Z");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculofecharegistro_Z_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculofecharegistro_Z = (DateTime)(DateTime.MinValue);
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculofecharegistro_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoTipo_Z" )]
      [  XmlElement( ElementName = "ObstaculoTipo_Z"   )]
      public short gxTpr_Obstaculotipo_Z
      {
         get {
            return gxTv_SdtObstaculo_Obstaculotipo_Z ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculotipo_Z = value;
            SetDirty("Obstaculotipo_Z");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculotipo_Z_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculotipo_Z = 0;
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculotipo_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoPacienteRegistra_Z" )]
      [  XmlElement( ElementName = "ObstaculoPacienteRegistra_Z"   )]
      public Guid gxTpr_Obstaculopacienteregistra_Z
      {
         get {
            return gxTv_SdtObstaculo_Obstaculopacienteregistra_Z ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculopacienteregistra_Z = (Guid)(value);
            SetDirty("Obstaculopacienteregistra_Z");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculopacienteregistra_Z_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculopacienteregistra_Z = (Guid)(Guid.Empty);
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculopacienteregistra_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoNumeroAdvertencias_Z" )]
      [  XmlElement( ElementName = "ObstaculoNumeroAdvertencias_Z"   )]
      public short gxTpr_Obstaculonumeroadvertencias_Z
      {
         get {
            return gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z = value;
            SetDirty("Obstaculonumeroadvertencias_Z");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z = 0;
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoPacienteRegistra_N" )]
      [  XmlElement( ElementName = "ObstaculoPacienteRegistra_N"   )]
      public short gxTpr_Obstaculopacienteregistra_N
      {
         get {
            return gxTv_SdtObstaculo_Obstaculopacienteregistra_N ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculopacienteregistra_N = value;
            SetDirty("Obstaculopacienteregistra_N");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculopacienteregistra_N_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculopacienteregistra_N = 0;
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculopacienteregistra_N_IsNull( )
      {
         return false ;
      }

      [  SoapElement( ElementName = "ObstaculoNumeroAdvertencias_N" )]
      [  XmlElement( ElementName = "ObstaculoNumeroAdvertencias_N"   )]
      public short gxTpr_Obstaculonumeroadvertencias_N
      {
         get {
            return gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N ;
         }

         set {
            gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N = value;
            SetDirty("Obstaculonumeroadvertencias_N");
         }

      }

      public void gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N_SetNull( )
      {
         gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N = 0;
         return  ;
      }

      public bool gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N_IsNull( )
      {
         return false ;
      }

      public void initialize( )
      {
         gxTv_SdtObstaculo_Obstaculoid = (Guid)(Guid.Empty);
         gxTv_SdtObstaculo_Obstaculofecharegistro = (DateTime)(DateTime.MinValue);
         gxTv_SdtObstaculo_Obstaculopacienteregistra = (Guid)(Guid.Empty);
         gxTv_SdtObstaculo_Obstaculolongitud = "";
         gxTv_SdtObstaculo_Obstaculolatitud = "";
         gxTv_SdtObstaculo_Mode = "";
         gxTv_SdtObstaculo_Obstaculoid_Z = (Guid)(Guid.Empty);
         gxTv_SdtObstaculo_Obstaculofecharegistro_Z = (DateTime)(DateTime.MinValue);
         gxTv_SdtObstaculo_Obstaculopacienteregistra_Z = (Guid)(Guid.Empty);
         datetime_STZ = (DateTime)(DateTime.MinValue);
         sDateCnv = "";
         sNumToPad = "";
         IGxSilentTrn obj ;
         obj = (IGxSilentTrn)ClassLoader.FindInstance( "obstaculo", "GeneXus.Programs.obstaculo_bc", new Object[] {context}, constructorCallingAssembly);;
         obj.initialize();
         obj.SetSDT(this, 1);
         setTransaction( obj) ;
         obj.SetMode("INS");
         return  ;
      }

      private short gxTv_SdtObstaculo_Obstaculotipo ;
      private short gxTv_SdtObstaculo_Obstaculonumeroadvertencias ;
      private short gxTv_SdtObstaculo_Initialized ;
      private short gxTv_SdtObstaculo_Obstaculotipo_Z ;
      private short gxTv_SdtObstaculo_Obstaculonumeroadvertencias_Z ;
      private short gxTv_SdtObstaculo_Obstaculopacienteregistra_N ;
      private short gxTv_SdtObstaculo_Obstaculonumeroadvertencias_N ;
      private String gxTv_SdtObstaculo_Mode ;
      private String sDateCnv ;
      private String sNumToPad ;
      private DateTime gxTv_SdtObstaculo_Obstaculofecharegistro ;
      private DateTime gxTv_SdtObstaculo_Obstaculofecharegistro_Z ;
      private DateTime datetime_STZ ;
      private String gxTv_SdtObstaculo_Obstaculolongitud ;
      private String gxTv_SdtObstaculo_Obstaculolatitud ;
      private Guid gxTv_SdtObstaculo_Obstaculoid ;
      private Guid gxTv_SdtObstaculo_Obstaculopacienteregistra ;
      private Guid gxTv_SdtObstaculo_Obstaculoid_Z ;
      private Guid gxTv_SdtObstaculo_Obstaculopacienteregistra_Z ;
   }

   [DataContract(Name = @"Obstaculo", Namespace = "PACYE2")]
   public class SdtObstaculo_RESTInterface : GxGenericCollectionItem<SdtObstaculo>, System.Web.SessionState.IRequiresSessionState
   {
      public SdtObstaculo_RESTInterface( ) : base()
      {
      }

      public SdtObstaculo_RESTInterface( SdtObstaculo psdt ) : base(psdt)
      {
      }

      [DataMember( Name = "ObstaculoID" , Order = 0 )]
      [GxSeudo()]
      public Guid gxTpr_Obstaculoid
      {
         get {
            return sdt.gxTpr_Obstaculoid ;
         }

         set {
            sdt.gxTpr_Obstaculoid = (Guid)(value);
         }

      }

      [DataMember( Name = "ObstaculoFechaRegistro" , Order = 1 )]
      [GxSeudo()]
      public String gxTpr_Obstaculofecharegistro
      {
         get {
            return DateTimeUtil.TToC2( sdt.gxTpr_Obstaculofecharegistro) ;
         }

         set {
            sdt.gxTpr_Obstaculofecharegistro = DateTimeUtil.CToT2( value);
         }

      }

      [DataMember( Name = "ObstaculoTipo" , Order = 2 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Obstaculotipo
      {
         get {
            return sdt.gxTpr_Obstaculotipo ;
         }

         set {
            sdt.gxTpr_Obstaculotipo = (short)(value.HasValue ? value.Value : 0);
         }

      }

      [DataMember( Name = "ObstaculoPacienteRegistra" , Order = 3 )]
      [GxSeudo()]
      public Guid gxTpr_Obstaculopacienteregistra
      {
         get {
            return sdt.gxTpr_Obstaculopacienteregistra ;
         }

         set {
            sdt.gxTpr_Obstaculopacienteregistra = (Guid)(value);
         }

      }

      [DataMember( Name = "ObstaculoLongitud" , Order = 4 )]
      public String gxTpr_Obstaculolongitud
      {
         get {
            return sdt.gxTpr_Obstaculolongitud ;
         }

         set {
            sdt.gxTpr_Obstaculolongitud = value;
         }

      }

      [DataMember( Name = "ObstaculoLatitud" , Order = 5 )]
      public String gxTpr_Obstaculolatitud
      {
         get {
            return sdt.gxTpr_Obstaculolatitud ;
         }

         set {
            sdt.gxTpr_Obstaculolatitud = value;
         }

      }

      [DataMember( Name = "ObstaculoNumeroAdvertencias" , Order = 6 )]
      [GxSeudo()]
      public Nullable<short> gxTpr_Obstaculonumeroadvertencias
      {
         get {
            return sdt.gxTpr_Obstaculonumeroadvertencias ;
         }

         set {
            sdt.gxTpr_Obstaculonumeroadvertencias = (short)(value.HasValue ? value.Value : 0);
         }

      }

      public SdtObstaculo sdt
      {
         get {
            return (SdtObstaculo)Sdt ;
         }

         set {
            Sdt = value ;
         }

      }

      [OnDeserializing]
      void checkSdt( StreamingContext ctx )
      {
         if ( sdt == null )
         {
            sdt = new SdtObstaculo() ;
         }
      }

      [DataMember( Name = "gx_md5_hash", Order = 7 )]
      public string Hash
      {
         get {
            if ( StringUtil.StrCmp(md5Hash, null) == 0 )
            {
               md5Hash = (String)(getHash());
            }
            return md5Hash ;
         }

         set {
            md5Hash = value ;
         }

      }

      private String md5Hash ;
   }

}
