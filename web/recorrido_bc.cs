/*
               File: Recorrido_BC
        Description: Recorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:38:59.16
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class recorrido_bc : GXHttpHandler, IGxSilentTrn
   {
      public recorrido_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public recorrido_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow033( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey033( ) ;
         standaloneModal( ) ;
         AddRow033( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z23RecorridoID = (Guid)(A23RecorridoID);
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_030( )
      {
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls033( ) ;
            }
            else
            {
               CheckExtendedTable033( ) ;
               if ( AnyError == 0 )
               {
               }
               CloseExtendedTableCursors033( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            /* Save parent mode. */
            sMode3 = Gx_mode;
            CONFIRM_035( ) ;
            if ( AnyError == 0 )
            {
               /* Restore parent mode. */
               Gx_mode = sMode3;
               IsConfirmed = 1;
            }
            /* Restore parent mode. */
            Gx_mode = sMode3;
         }
      }

      protected void CONFIRM_035( )
      {
         nGXsfl_5_idx = 0;
         while ( nGXsfl_5_idx < bcRecorrido.gxTpr_Detalle.Count )
         {
            ReadRow035( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound5 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_5 != 0 ) )
            {
               GetKey035( ) ;
               if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 ) )
               {
                  if ( RcdFound5 == 0 )
                  {
                     Gx_mode = "INS";
                     BeforeValidate035( ) ;
                     if ( AnyError == 0 )
                     {
                        CheckExtendedTable035( ) ;
                        if ( AnyError == 0 )
                        {
                           ZM035( 14) ;
                           ZM035( 15) ;
                        }
                        CloseExtendedTableCursors035( ) ;
                        if ( AnyError == 0 )
                        {
                           IsConfirmed = 1;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                     AnyError = 1;
                  }
               }
               else
               {
                  if ( RcdFound5 != 0 )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                     {
                        Gx_mode = "DLT";
                        getByPrimaryKey035( ) ;
                        Load035( ) ;
                        BeforeValidate035( ) ;
                        if ( AnyError == 0 )
                        {
                           OnDeleteControls035( ) ;
                        }
                     }
                     else
                     {
                        if ( nIsMod_5 != 0 )
                        {
                           Gx_mode = "UPD";
                           BeforeValidate035( ) ;
                           if ( AnyError == 0 )
                           {
                              CheckExtendedTable035( ) ;
                              if ( AnyError == 0 )
                              {
                                 ZM035( 14) ;
                                 ZM035( 15) ;
                              }
                              CloseExtendedTableCursors035( ) ;
                              if ( AnyError == 0 )
                              {
                                 IsConfirmed = 1;
                              }
                           }
                        }
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "DLT") != 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
               VarsToRow5( ((SdtRecorrido_Detalle)bcRecorrido.gxTpr_Detalle.Item(nGXsfl_5_idx))) ;
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
      }

      protected void ZM033( short GX_JID )
      {
         if ( ( GX_JID == 12 ) || ( GX_JID == 0 ) )
         {
            Z24RecorridoFecha = A24RecorridoFecha;
         }
         if ( GX_JID == -12 )
         {
            Z23RecorridoID = (Guid)(A23RecorridoID);
            Z24RecorridoFecha = A24RecorridoFecha;
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
      }

      protected void Load033( )
      {
         /* Using cursor BC00038 */
         pr_default.execute(6, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(6) != 101) )
         {
            RcdFound3 = 1;
            A24RecorridoFecha = BC00038_A24RecorridoFecha[0];
            ZM033( -12) ;
         }
         pr_default.close(6);
         OnLoadActions033( ) ;
      }

      protected void OnLoadActions033( )
      {
      }

      protected void CheckExtendedTable033( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A24RecorridoFecha) || ( A24RecorridoFecha >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Recorrido Fecha fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors033( )
      {
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey033( )
      {
         /* Using cursor BC00039 */
         pr_default.execute(7, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound3 = 1;
         }
         else
         {
            RcdFound3 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00037 */
         pr_default.execute(5, new Object[] {A23RecorridoID});
         if ( (pr_default.getStatus(5) != 101) )
         {
            ZM033( 12) ;
            RcdFound3 = 1;
            A23RecorridoID = (Guid)((Guid)(BC00037_A23RecorridoID[0]));
            A24RecorridoFecha = BC00037_A24RecorridoFecha[0];
            Z23RecorridoID = (Guid)(A23RecorridoID);
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load033( ) ;
            if ( AnyError == 1 )
            {
               RcdFound3 = 0;
               InitializeNonKey033( ) ;
            }
            Gx_mode = sMode3;
         }
         else
         {
            RcdFound3 = 0;
            InitializeNonKey033( ) ;
            sMode3 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode3;
         }
         pr_default.close(5);
      }

      protected void getEqualNoModal( )
      {
         GetKey033( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_030( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency033( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00036 */
            pr_default.execute(4, new Object[] {A23RecorridoID});
            if ( (pr_default.getStatus(4) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Recorrido"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(4) == 101) || ( Z24RecorridoFecha != BC00036_A24RecorridoFecha[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Recorrido"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert033( )
      {
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable033( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM033( 0) ;
            CheckOptimisticConcurrency033( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm033( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert033( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000310 */
                     pr_default.execute(8, new Object[] {A23RecorridoID, A24RecorridoFecha});
                     pr_default.close(8);
                     dsDefault.SmartCacheProvider.SetUpdated("Recorrido") ;
                     if ( (pr_default.getStatus(8) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel033( ) ;
                           if ( AnyError == 0 )
                           {
                              /* Save values for previous() function. */
                              GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           }
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load033( ) ;
            }
            EndLevel033( ) ;
         }
         CloseExtendedTableCursors033( ) ;
      }

      protected void Update033( )
      {
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable033( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency033( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm033( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate033( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000311 */
                     pr_default.execute(9, new Object[] {A24RecorridoFecha, A23RecorridoID});
                     pr_default.close(9);
                     dsDefault.SmartCacheProvider.SetUpdated("Recorrido") ;
                     if ( (pr_default.getStatus(9) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Recorrido"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate033( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           ProcessLevel033( ) ;
                           if ( AnyError == 0 )
                           {
                              getByPrimaryKey( ) ;
                              GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           }
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel033( ) ;
         }
         CloseExtendedTableCursors033( ) ;
      }

      protected void DeferredUpdate033( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate033( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency033( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls033( ) ;
            AfterConfirm033( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete033( ) ;
               if ( AnyError == 0 )
               {
                  ScanKeyStart035( ) ;
                  while ( RcdFound5 != 0 )
                  {
                     getByPrimaryKey035( ) ;
                     Delete035( ) ;
                     ScanKeyNext035( ) ;
                  }
                  ScanKeyEnd035( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000312 */
                     pr_default.execute(10, new Object[] {A23RecorridoID});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("Recorrido") ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( delete) rules */
                        /* End of After( delete) rules */
                        if ( AnyError == 0 )
                        {
                           GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
         }
         sMode3 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel033( ) ;
         Gx_mode = sMode3;
      }

      protected void OnDeleteControls033( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void ProcessNestedLevel035( )
      {
         nGXsfl_5_idx = 0;
         while ( nGXsfl_5_idx < bcRecorrido.gxTpr_Detalle.Count )
         {
            ReadRow035( ) ;
            if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
            {
               if ( RcdFound5 == 0 )
               {
                  Gx_mode = "INS";
               }
               else
               {
                  Gx_mode = "UPD";
               }
            }
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") != 0 ) || ( nIsMod_5 != 0 ) )
            {
               standaloneNotModal035( ) ;
               if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
               {
                  Gx_mode = "INS";
                  Insert035( ) ;
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
                  {
                     Gx_mode = "DLT";
                     Delete035( ) ;
                  }
                  else
                  {
                     Gx_mode = "UPD";
                     Update035( ) ;
                  }
               }
            }
            KeyVarsToRow5( ((SdtRecorrido_Detalle)bcRecorrido.gxTpr_Detalle.Item(nGXsfl_5_idx))) ;
         }
         if ( AnyError == 0 )
         {
            /* Batch update SDT rows */
            nGXsfl_5_idx = 0;
            while ( nGXsfl_5_idx < bcRecorrido.gxTpr_Detalle.Count )
            {
               ReadRow035( ) ;
               if ( String.IsNullOrEmpty(StringUtil.RTrim( Gx_mode)) )
               {
                  if ( RcdFound5 == 0 )
                  {
                     Gx_mode = "INS";
                  }
                  else
                  {
                     Gx_mode = "UPD";
                  }
               }
               /* Update SDT row */
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  bcRecorrido.gxTpr_Detalle.RemoveElement(nGXsfl_5_idx);
                  nGXsfl_5_idx = (short)(nGXsfl_5_idx-1);
               }
               else
               {
                  Gx_mode = "UPD";
                  getByPrimaryKey035( ) ;
                  VarsToRow5( ((SdtRecorrido_Detalle)bcRecorrido.gxTpr_Detalle.Item(nGXsfl_5_idx))) ;
               }
            }
         }
         /* Start of After( level) rules */
         /* End of After( level) rules */
         InitAll035( ) ;
         if ( AnyError != 0 )
         {
         }
         nRcdExists_5 = 0;
         nIsMod_5 = 0;
         Gxremove5 = 0;
      }

      protected void ProcessLevel033( )
      {
         /* Save parent mode. */
         sMode3 = Gx_mode;
         ProcessNestedLevel035( ) ;
         if ( AnyError != 0 )
         {
         }
         /* Restore parent mode. */
         Gx_mode = sMode3;
         /* ' Update level parameters */
      }

      protected void EndLevel033( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(4);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete033( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart033( )
      {
         /* Using cursor BC000313 */
         pr_default.execute(11, new Object[] {A23RecorridoID});
         RcdFound3 = 0;
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound3 = 1;
            A23RecorridoID = (Guid)((Guid)(BC000313_A23RecorridoID[0]));
            A24RecorridoFecha = BC000313_A24RecorridoFecha[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext033( )
      {
         /* Scan next routine */
         pr_default.readNext(11);
         RcdFound3 = 0;
         ScanKeyLoad033( ) ;
      }

      protected void ScanKeyLoad033( )
      {
         sMode3 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(11) != 101) )
         {
            RcdFound3 = 1;
            A23RecorridoID = (Guid)((Guid)(BC000313_A23RecorridoID[0]));
            A24RecorridoFecha = BC000313_A24RecorridoFecha[0];
         }
         Gx_mode = sMode3;
      }

      protected void ScanKeyEnd033( )
      {
         pr_default.close(11);
      }

      protected void AfterConfirm033( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert033( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate033( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete033( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete033( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate033( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes033( )
      {
      }

      protected void ZM035( short GX_JID )
      {
         if ( ( GX_JID == 13 ) || ( GX_JID == 0 ) )
         {
            Z25TipoSenal = A25TipoSenal;
            Z28TipoObstaculo = A28TipoObstaculo;
            Z29Incidente = A29Incidente;
            Z30Emergencia = A30Emergencia;
            Z31IMEI = A31IMEI;
            Z33TimeStamp = A33TimeStamp;
            Z21RecorridoDetallePaciente = (Guid)(A21RecorridoDetallePaciente);
            Z22IDCoach = (Guid)(A22IDCoach);
         }
         if ( ( GX_JID == 14 ) || ( GX_JID == 0 ) )
         {
         }
         if ( ( GX_JID == 15 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -13 )
         {
            Z23RecorridoID = (Guid)(A23RecorridoID);
            Z32RecorridoDetalleID = (Guid)(A32RecorridoDetalleID);
            Z25TipoSenal = A25TipoSenal;
            Z26Longitud = A26Longitud;
            Z27Latitud = A27Latitud;
            Z28TipoObstaculo = A28TipoObstaculo;
            Z29Incidente = A29Incidente;
            Z30Emergencia = A30Emergencia;
            Z31IMEI = A31IMEI;
            Z33TimeStamp = A33TimeStamp;
            Z21RecorridoDetallePaciente = (Guid)(A21RecorridoDetallePaciente);
            Z22IDCoach = (Guid)(A22IDCoach);
         }
      }

      protected void standaloneNotModal035( )
      {
      }

      protected void standaloneModal035( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A32RecorridoDetalleID) )
         {
            A32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load035( )
      {
         /* Using cursor BC000314 */
         pr_default.execute(12, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
         if ( (pr_default.getStatus(12) != 101) )
         {
            RcdFound5 = 1;
            A25TipoSenal = BC000314_A25TipoSenal[0];
            n25TipoSenal = BC000314_n25TipoSenal[0];
            A26Longitud = BC000314_A26Longitud[0];
            n26Longitud = BC000314_n26Longitud[0];
            A27Latitud = BC000314_A27Latitud[0];
            n27Latitud = BC000314_n27Latitud[0];
            A28TipoObstaculo = BC000314_A28TipoObstaculo[0];
            n28TipoObstaculo = BC000314_n28TipoObstaculo[0];
            A29Incidente = BC000314_A29Incidente[0];
            n29Incidente = BC000314_n29Incidente[0];
            A30Emergencia = BC000314_A30Emergencia[0];
            n30Emergencia = BC000314_n30Emergencia[0];
            A31IMEI = BC000314_A31IMEI[0];
            n31IMEI = BC000314_n31IMEI[0];
            A33TimeStamp = BC000314_A33TimeStamp[0];
            n33TimeStamp = BC000314_n33TimeStamp[0];
            A21RecorridoDetallePaciente = (Guid)((Guid)(BC000314_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = BC000314_n21RecorridoDetallePaciente[0];
            A22IDCoach = (Guid)((Guid)(BC000314_A22IDCoach[0]));
            n22IDCoach = BC000314_n22IDCoach[0];
            ZM035( -13) ;
         }
         pr_default.close(12);
         OnLoadActions035( ) ;
      }

      protected void OnLoadActions035( )
      {
      }

      protected void CheckExtendedTable035( )
      {
         Gx_BScreen = 1;
         standaloneModal035( ) ;
         Gx_BScreen = 0;
         /* Using cursor BC00034 */
         pr_default.execute(2, new Object[] {n21RecorridoDetallePaciente, A21RecorridoDetallePaciente});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (Guid.Empty==A21RecorridoDetallePaciente) ) )
            {
               GX_msglist.addItem("No existe 'Recorrido Paciente ST'.", "ForeignKeyNotFound", 1, "RECORRIDODETALLEPACIENTE");
               AnyError = 1;
            }
         }
         pr_default.close(2);
         /* Using cursor BC00035 */
         pr_default.execute(3, new Object[] {n22IDCoach, A22IDCoach});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (Guid.Empty==A22IDCoach) ) )
            {
               GX_msglist.addItem("No existe 'Recorrido Detalle Coach ST'.", "ForeignKeyNotFound", 1, "IDCOACH");
               AnyError = 1;
            }
         }
         pr_default.close(3);
         if ( ! ( ( A25TipoSenal == 1 ) || ( A25TipoSenal == 2 ) || ( A25TipoSenal == 3 ) || (0==A25TipoSenal) ) )
         {
            GX_msglist.addItem("Campo Tipo Senal fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A28TipoObstaculo == 0 ) || ( A28TipoObstaculo == 1 ) || ( A28TipoObstaculo == 2 ) || ( A28TipoObstaculo == 3 ) || (0==A28TipoObstaculo) ) )
         {
            GX_msglist.addItem("Campo Tipo Obstaculo fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A29Incidente == 0 ) || ( A29Incidente == 1 ) || (0==A29Incidente) ) )
         {
            GX_msglist.addItem("Campo Incidente fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A30Emergencia == 0 ) || ( A30Emergencia == 1 ) || (0==A30Emergencia) ) )
         {
            GX_msglist.addItem("Campo Emergencia fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( (DateTime.MinValue==A33TimeStamp) || ( A33TimeStamp >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Time Stamp fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
      }

      protected void CloseExtendedTableCursors035( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable035( )
      {
      }

      protected void GetKey035( )
      {
         /* Using cursor BC000315 */
         pr_default.execute(13, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
         if ( (pr_default.getStatus(13) != 101) )
         {
            RcdFound5 = 1;
         }
         else
         {
            RcdFound5 = 0;
         }
         pr_default.close(13);
      }

      protected void getByPrimaryKey035( )
      {
         /* Using cursor BC00033 */
         pr_default.execute(1, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM035( 13) ;
            RcdFound5 = 1;
            InitializeNonKey035( ) ;
            A32RecorridoDetalleID = (Guid)((Guid)(BC00033_A32RecorridoDetalleID[0]));
            A25TipoSenal = BC00033_A25TipoSenal[0];
            n25TipoSenal = BC00033_n25TipoSenal[0];
            A26Longitud = BC00033_A26Longitud[0];
            n26Longitud = BC00033_n26Longitud[0];
            A27Latitud = BC00033_A27Latitud[0];
            n27Latitud = BC00033_n27Latitud[0];
            A28TipoObstaculo = BC00033_A28TipoObstaculo[0];
            n28TipoObstaculo = BC00033_n28TipoObstaculo[0];
            A29Incidente = BC00033_A29Incidente[0];
            n29Incidente = BC00033_n29Incidente[0];
            A30Emergencia = BC00033_A30Emergencia[0];
            n30Emergencia = BC00033_n30Emergencia[0];
            A31IMEI = BC00033_A31IMEI[0];
            n31IMEI = BC00033_n31IMEI[0];
            A33TimeStamp = BC00033_A33TimeStamp[0];
            n33TimeStamp = BC00033_n33TimeStamp[0];
            A21RecorridoDetallePaciente = (Guid)((Guid)(BC00033_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = BC00033_n21RecorridoDetallePaciente[0];
            A22IDCoach = (Guid)((Guid)(BC00033_A22IDCoach[0]));
            n22IDCoach = BC00033_n22IDCoach[0];
            Z23RecorridoID = (Guid)(A23RecorridoID);
            Z32RecorridoDetalleID = (Guid)(A32RecorridoDetalleID);
            sMode5 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal035( ) ;
            Load035( ) ;
            Gx_mode = sMode5;
         }
         else
         {
            RcdFound5 = 0;
            InitializeNonKey035( ) ;
            sMode5 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal035( ) ;
            Gx_mode = sMode5;
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 ) || ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 ) )
         {
            DisableAttributes035( ) ;
         }
         pr_default.close(1);
      }

      protected void CheckOptimisticConcurrency035( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00032 */
            pr_default.execute(0, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RecorridoDetalle"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            Gx_longc = false;
            if ( (pr_default.getStatus(0) == 101) || ( Z25TipoSenal != BC00032_A25TipoSenal[0] ) || ( Z28TipoObstaculo != BC00032_A28TipoObstaculo[0] ) || ( Z29Incidente != BC00032_A29Incidente[0] ) || ( Z30Emergencia != BC00032_A30Emergencia[0] ) || ( StringUtil.StrCmp(Z31IMEI, BC00032_A31IMEI[0]) != 0 ) )
            {
               Gx_longc = true;
            }
            if ( Gx_longc || ( Z33TimeStamp != BC00032_A33TimeStamp[0] ) || ( Z21RecorridoDetallePaciente != BC00032_A21RecorridoDetallePaciente[0] ) || ( Z22IDCoach != BC00032_A22IDCoach[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"RecorridoDetalle"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert035( )
      {
         BeforeValidate035( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable035( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM035( 0) ;
            CheckOptimisticConcurrency035( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm035( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert035( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000316 */
                     pr_default.execute(14, new Object[] {A23RecorridoID, n25TipoSenal, A25TipoSenal, n26Longitud, A26Longitud, n27Latitud, A27Latitud, n28TipoObstaculo, A28TipoObstaculo, n29Incidente, A29Incidente, n30Emergencia, A30Emergencia, n31IMEI, A31IMEI, n33TimeStamp, A33TimeStamp, n21RecorridoDetallePaciente, A21RecorridoDetallePaciente, n22IDCoach, A22IDCoach, A32RecorridoDetalleID});
                     pr_default.close(14);
                     dsDefault.SmartCacheProvider.SetUpdated("RecorridoDetalle") ;
                     if ( (pr_default.getStatus(14) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load035( ) ;
            }
            EndLevel035( ) ;
         }
         CloseExtendedTableCursors035( ) ;
      }

      protected void Update035( )
      {
         BeforeValidate035( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable035( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency035( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm035( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate035( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC000317 */
                     pr_default.execute(15, new Object[] {n25TipoSenal, A25TipoSenal, n26Longitud, A26Longitud, n27Latitud, A27Latitud, n28TipoObstaculo, A28TipoObstaculo, n29Incidente, A29Incidente, n30Emergencia, A30Emergencia, n31IMEI, A31IMEI, n33TimeStamp, A33TimeStamp, n21RecorridoDetallePaciente, A21RecorridoDetallePaciente, n22IDCoach, A22IDCoach, A23RecorridoID, A32RecorridoDetalleID});
                     pr_default.close(15);
                     dsDefault.SmartCacheProvider.SetUpdated("RecorridoDetalle") ;
                     if ( (pr_default.getStatus(15) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"RecorridoDetalle"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate035( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey035( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel035( ) ;
         }
         CloseExtendedTableCursors035( ) ;
      }

      protected void DeferredUpdate035( )
      {
      }

      protected void Delete035( )
      {
         Gx_mode = "DLT";
         BeforeValidate035( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency035( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls035( ) ;
            AfterConfirm035( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete035( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC000318 */
                  pr_default.execute(16, new Object[] {A23RecorridoID, A32RecorridoDetalleID});
                  pr_default.close(16);
                  dsDefault.SmartCacheProvider.SetUpdated("RecorridoDetalle") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode5 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel035( ) ;
         Gx_mode = sMode5;
      }

      protected void OnDeleteControls035( )
      {
         standaloneModal035( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel035( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart035( )
      {
         /* Scan By routine */
         /* Using cursor BC000319 */
         pr_default.execute(17, new Object[] {A23RecorridoID});
         RcdFound5 = 0;
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound5 = 1;
            A32RecorridoDetalleID = (Guid)((Guid)(BC000319_A32RecorridoDetalleID[0]));
            A25TipoSenal = BC000319_A25TipoSenal[0];
            n25TipoSenal = BC000319_n25TipoSenal[0];
            A26Longitud = BC000319_A26Longitud[0];
            n26Longitud = BC000319_n26Longitud[0];
            A27Latitud = BC000319_A27Latitud[0];
            n27Latitud = BC000319_n27Latitud[0];
            A28TipoObstaculo = BC000319_A28TipoObstaculo[0];
            n28TipoObstaculo = BC000319_n28TipoObstaculo[0];
            A29Incidente = BC000319_A29Incidente[0];
            n29Incidente = BC000319_n29Incidente[0];
            A30Emergencia = BC000319_A30Emergencia[0];
            n30Emergencia = BC000319_n30Emergencia[0];
            A31IMEI = BC000319_A31IMEI[0];
            n31IMEI = BC000319_n31IMEI[0];
            A33TimeStamp = BC000319_A33TimeStamp[0];
            n33TimeStamp = BC000319_n33TimeStamp[0];
            A21RecorridoDetallePaciente = (Guid)((Guid)(BC000319_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = BC000319_n21RecorridoDetallePaciente[0];
            A22IDCoach = (Guid)((Guid)(BC000319_A22IDCoach[0]));
            n22IDCoach = BC000319_n22IDCoach[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext035( )
      {
         /* Scan next routine */
         pr_default.readNext(17);
         RcdFound5 = 0;
         ScanKeyLoad035( ) ;
      }

      protected void ScanKeyLoad035( )
      {
         sMode5 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(17) != 101) )
         {
            RcdFound5 = 1;
            A32RecorridoDetalleID = (Guid)((Guid)(BC000319_A32RecorridoDetalleID[0]));
            A25TipoSenal = BC000319_A25TipoSenal[0];
            n25TipoSenal = BC000319_n25TipoSenal[0];
            A26Longitud = BC000319_A26Longitud[0];
            n26Longitud = BC000319_n26Longitud[0];
            A27Latitud = BC000319_A27Latitud[0];
            n27Latitud = BC000319_n27Latitud[0];
            A28TipoObstaculo = BC000319_A28TipoObstaculo[0];
            n28TipoObstaculo = BC000319_n28TipoObstaculo[0];
            A29Incidente = BC000319_A29Incidente[0];
            n29Incidente = BC000319_n29Incidente[0];
            A30Emergencia = BC000319_A30Emergencia[0];
            n30Emergencia = BC000319_n30Emergencia[0];
            A31IMEI = BC000319_A31IMEI[0];
            n31IMEI = BC000319_n31IMEI[0];
            A33TimeStamp = BC000319_A33TimeStamp[0];
            n33TimeStamp = BC000319_n33TimeStamp[0];
            A21RecorridoDetallePaciente = (Guid)((Guid)(BC000319_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = BC000319_n21RecorridoDetallePaciente[0];
            A22IDCoach = (Guid)((Guid)(BC000319_A22IDCoach[0]));
            n22IDCoach = BC000319_n22IDCoach[0];
         }
         Gx_mode = sMode5;
      }

      protected void ScanKeyEnd035( )
      {
         pr_default.close(17);
      }

      protected void AfterConfirm035( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert035( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate035( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete035( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete035( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate035( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes035( )
      {
      }

      protected void send_integrity_lvl_hashes035( )
      {
      }

      protected void send_integrity_lvl_hashes033( )
      {
      }

      protected void AddRow033( )
      {
         VarsToRow3( bcRecorrido) ;
      }

      protected void ReadRow033( )
      {
         RowToVars3( bcRecorrido, 1) ;
      }

      protected void AddRow035( )
      {
         SdtRecorrido_Detalle obj5 ;
         obj5 = new SdtRecorrido_Detalle(context);
         VarsToRow5( obj5) ;
         bcRecorrido.gxTpr_Detalle.Add(obj5, 0);
         obj5.gxTpr_Mode = "UPD";
         obj5.gxTpr_Modified = 0;
      }

      protected void ReadRow035( )
      {
         nGXsfl_5_idx = (short)(nGXsfl_5_idx+1);
         RowToVars5( ((SdtRecorrido_Detalle)bcRecorrido.gxTpr_Detalle.Item(nGXsfl_5_idx)), 1) ;
      }

      protected void InitializeNonKey033( )
      {
         A24RecorridoFecha = (DateTime)(DateTime.MinValue);
         Z24RecorridoFecha = (DateTime)(DateTime.MinValue);
      }

      protected void InitAll033( )
      {
         A23RecorridoID = (Guid)(Guid.Empty);
         InitializeNonKey033( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void InitializeNonKey035( )
      {
         A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         n21RecorridoDetallePaciente = false;
         A22IDCoach = (Guid)(Guid.Empty);
         n22IDCoach = false;
         A25TipoSenal = 0;
         n25TipoSenal = false;
         A26Longitud = "";
         n26Longitud = false;
         A27Latitud = "";
         n27Latitud = false;
         A28TipoObstaculo = 0;
         n28TipoObstaculo = false;
         A29Incidente = 0;
         n29Incidente = false;
         A30Emergencia = 0;
         n30Emergencia = false;
         A31IMEI = "";
         n31IMEI = false;
         A33TimeStamp = (DateTime)(DateTime.MinValue);
         n33TimeStamp = false;
         Z25TipoSenal = 0;
         Z28TipoObstaculo = 0;
         Z29Incidente = 0;
         Z30Emergencia = 0;
         Z31IMEI = "";
         Z33TimeStamp = (DateTime)(DateTime.MinValue);
         Z21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         Z22IDCoach = (Guid)(Guid.Empty);
      }

      protected void InitAll035( )
      {
         A32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
         InitializeNonKey035( ) ;
      }

      protected void StandaloneModalInsert035( )
      {
      }

      public void VarsToRow3( SdtRecorrido obj3 )
      {
         obj3.gxTpr_Mode = Gx_mode;
         obj3.gxTpr_Recorridofecha = A24RecorridoFecha;
         obj3.gxTpr_Recorridoid = (Guid)(A23RecorridoID);
         obj3.gxTpr_Recorridoid_Z = (Guid)(Z23RecorridoID);
         obj3.gxTpr_Recorridofecha_Z = Z24RecorridoFecha;
         obj3.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow3( SdtRecorrido obj3 )
      {
         obj3.gxTpr_Recorridoid = (Guid)(A23RecorridoID);
         return  ;
      }

      public void RowToVars3( SdtRecorrido obj3 ,
                              int forceLoad )
      {
         Gx_mode = obj3.gxTpr_Mode;
         A24RecorridoFecha = obj3.gxTpr_Recorridofecha;
         A23RecorridoID = (Guid)(obj3.gxTpr_Recorridoid);
         Z23RecorridoID = (Guid)(obj3.gxTpr_Recorridoid_Z);
         Z24RecorridoFecha = obj3.gxTpr_Recorridofecha_Z;
         Gx_mode = obj3.gxTpr_Mode;
         return  ;
      }

      public void VarsToRow5( SdtRecorrido_Detalle obj5 )
      {
         obj5.gxTpr_Mode = Gx_mode;
         obj5.gxTpr_Recorridodetallepaciente = (Guid)(A21RecorridoDetallePaciente);
         obj5.gxTpr_Idcoach = (Guid)(A22IDCoach);
         obj5.gxTpr_Tiposenal = A25TipoSenal;
         obj5.gxTpr_Longitud = A26Longitud;
         obj5.gxTpr_Latitud = A27Latitud;
         obj5.gxTpr_Tipoobstaculo = A28TipoObstaculo;
         obj5.gxTpr_Incidente = A29Incidente;
         obj5.gxTpr_Emergencia = A30Emergencia;
         obj5.gxTpr_Imei = A31IMEI;
         obj5.gxTpr_Timestamp = A33TimeStamp;
         obj5.gxTpr_Recorridodetalleid = (Guid)(A32RecorridoDetalleID);
         obj5.gxTpr_Recorridodetalleid_Z = (Guid)(Z32RecorridoDetalleID);
         obj5.gxTpr_Recorridodetallepaciente_Z = (Guid)(Z21RecorridoDetallePaciente);
         obj5.gxTpr_Idcoach_Z = (Guid)(Z22IDCoach);
         obj5.gxTpr_Tiposenal_Z = Z25TipoSenal;
         obj5.gxTpr_Tipoobstaculo_Z = Z28TipoObstaculo;
         obj5.gxTpr_Incidente_Z = Z29Incidente;
         obj5.gxTpr_Emergencia_Z = Z30Emergencia;
         obj5.gxTpr_Imei_Z = Z31IMEI;
         obj5.gxTpr_Timestamp_Z = Z33TimeStamp;
         obj5.gxTpr_Recorridodetallepaciente_N = (short)(Convert.ToInt16(n21RecorridoDetallePaciente));
         obj5.gxTpr_Idcoach_N = (short)(Convert.ToInt16(n22IDCoach));
         obj5.gxTpr_Tiposenal_N = (short)(Convert.ToInt16(n25TipoSenal));
         obj5.gxTpr_Longitud_N = (short)(Convert.ToInt16(n26Longitud));
         obj5.gxTpr_Latitud_N = (short)(Convert.ToInt16(n27Latitud));
         obj5.gxTpr_Tipoobstaculo_N = (short)(Convert.ToInt16(n28TipoObstaculo));
         obj5.gxTpr_Incidente_N = (short)(Convert.ToInt16(n29Incidente));
         obj5.gxTpr_Emergencia_N = (short)(Convert.ToInt16(n30Emergencia));
         obj5.gxTpr_Imei_N = (short)(Convert.ToInt16(n31IMEI));
         obj5.gxTpr_Timestamp_N = (short)(Convert.ToInt16(n33TimeStamp));
         obj5.gxTpr_Modified = nIsMod_5;
         return  ;
      }

      public void KeyVarsToRow5( SdtRecorrido_Detalle obj5 )
      {
         obj5.gxTpr_Recorridodetalleid = (Guid)(A32RecorridoDetalleID);
         return  ;
      }

      public void RowToVars5( SdtRecorrido_Detalle obj5 ,
                              int forceLoad )
      {
         Gx_mode = obj5.gxTpr_Mode;
         A21RecorridoDetallePaciente = (Guid)(obj5.gxTpr_Recorridodetallepaciente);
         n21RecorridoDetallePaciente = false;
         A22IDCoach = (Guid)(obj5.gxTpr_Idcoach);
         n22IDCoach = false;
         A25TipoSenal = obj5.gxTpr_Tiposenal;
         n25TipoSenal = false;
         A26Longitud = obj5.gxTpr_Longitud;
         n26Longitud = false;
         A27Latitud = obj5.gxTpr_Latitud;
         n27Latitud = false;
         A28TipoObstaculo = obj5.gxTpr_Tipoobstaculo;
         n28TipoObstaculo = false;
         A29Incidente = obj5.gxTpr_Incidente;
         n29Incidente = false;
         A30Emergencia = obj5.gxTpr_Emergencia;
         n30Emergencia = false;
         A31IMEI = obj5.gxTpr_Imei;
         n31IMEI = false;
         A33TimeStamp = obj5.gxTpr_Timestamp;
         n33TimeStamp = false;
         A32RecorridoDetalleID = (Guid)(obj5.gxTpr_Recorridodetalleid);
         Z32RecorridoDetalleID = (Guid)(obj5.gxTpr_Recorridodetalleid_Z);
         Z21RecorridoDetallePaciente = (Guid)(obj5.gxTpr_Recorridodetallepaciente_Z);
         Z22IDCoach = (Guid)(obj5.gxTpr_Idcoach_Z);
         Z25TipoSenal = obj5.gxTpr_Tiposenal_Z;
         Z28TipoObstaculo = obj5.gxTpr_Tipoobstaculo_Z;
         Z29Incidente = obj5.gxTpr_Incidente_Z;
         Z30Emergencia = obj5.gxTpr_Emergencia_Z;
         Z31IMEI = obj5.gxTpr_Imei_Z;
         Z33TimeStamp = obj5.gxTpr_Timestamp_Z;
         n21RecorridoDetallePaciente = (bool)(Convert.ToBoolean(obj5.gxTpr_Recorridodetallepaciente_N));
         n22IDCoach = (bool)(Convert.ToBoolean(obj5.gxTpr_Idcoach_N));
         n25TipoSenal = (bool)(Convert.ToBoolean(obj5.gxTpr_Tiposenal_N));
         n26Longitud = (bool)(Convert.ToBoolean(obj5.gxTpr_Longitud_N));
         n27Latitud = (bool)(Convert.ToBoolean(obj5.gxTpr_Latitud_N));
         n28TipoObstaculo = (bool)(Convert.ToBoolean(obj5.gxTpr_Tipoobstaculo_N));
         n29Incidente = (bool)(Convert.ToBoolean(obj5.gxTpr_Incidente_N));
         n30Emergencia = (bool)(Convert.ToBoolean(obj5.gxTpr_Emergencia_N));
         n31IMEI = (bool)(Convert.ToBoolean(obj5.gxTpr_Imei_N));
         n33TimeStamp = (bool)(Convert.ToBoolean(obj5.gxTpr_Timestamp_N));
         nIsMod_5 = obj5.gxTpr_Modified;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A23RecorridoID = (Guid)((Guid)getParm(obj,0));
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey033( ) ;
         ScanKeyStart033( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z23RecorridoID = (Guid)(A23RecorridoID);
         }
         ZM033( -12) ;
         OnLoadActions033( ) ;
         AddRow033( ) ;
         bcRecorrido.gxTpr_Detalle.ClearCollection();
         if ( RcdFound3 == 1 )
         {
            ScanKeyStart035( ) ;
            nGXsfl_5_idx = 1;
            while ( RcdFound5 != 0 )
            {
               Z23RecorridoID = (Guid)(A23RecorridoID);
               Z32RecorridoDetalleID = (Guid)(A32RecorridoDetalleID);
               ZM035( -13) ;
               OnLoadActions035( ) ;
               nRcdExists_5 = 1;
               nIsMod_5 = 0;
               AddRow035( ) ;
               nGXsfl_5_idx = (short)(nGXsfl_5_idx+1);
               ScanKeyNext035( ) ;
            }
            ScanKeyEnd035( ) ;
         }
         ScanKeyEnd033( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars3( bcRecorrido, 0) ;
         ScanKeyStart033( ) ;
         if ( RcdFound3 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z23RecorridoID = (Guid)(A23RecorridoID);
         }
         ZM033( -12) ;
         OnLoadActions033( ) ;
         AddRow033( ) ;
         bcRecorrido.gxTpr_Detalle.ClearCollection();
         if ( RcdFound3 == 1 )
         {
            ScanKeyStart035( ) ;
            nGXsfl_5_idx = 1;
            while ( RcdFound5 != 0 )
            {
               Z23RecorridoID = (Guid)(A23RecorridoID);
               Z32RecorridoDetalleID = (Guid)(A32RecorridoDetalleID);
               ZM035( -13) ;
               OnLoadActions035( ) ;
               nRcdExists_5 = 1;
               nIsMod_5 = 0;
               AddRow035( ) ;
               nGXsfl_5_idx = (short)(nGXsfl_5_idx+1);
               ScanKeyNext035( ) ;
            }
            ScanKeyEnd035( ) ;
         }
         ScanKeyEnd033( ) ;
         if ( RcdFound3 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      protected void SaveImpl( )
      {
         nKeyPressed = 1;
         GetKey033( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert033( ) ;
         }
         else
         {
            if ( RcdFound3 == 1 )
            {
               if ( A23RecorridoID != Z23RecorridoID )
               {
                  A23RecorridoID = (Guid)(Z23RecorridoID);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update033( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A23RecorridoID != Z23RecorridoID )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert033( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert033( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars3( bcRecorrido, 0) ;
         SaveImpl( ) ;
         VarsToRow3( bcRecorrido) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public bool Insert( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars3( bcRecorrido, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert033( ) ;
         AfterTrn( ) ;
         VarsToRow3( bcRecorrido) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      protected void UpdateImpl( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            SaveImpl( ) ;
         }
         else
         {
            SdtRecorrido auxBC = new SdtRecorrido(context) ;
            auxBC.Load(A23RecorridoID);
            auxBC.UpdateDirties(bcRecorrido);
            auxBC.Save();
            IGxSilentTrn auxTrn = auxBC.getTransaction() ;
            LclMsgLst = (msglist)(auxTrn.GetMessages());
            AnyError = (short)(auxTrn.Errors());
            Gx_mode = auxTrn.GetMode();
            context.GX_msglist = LclMsgLst;
            AfterTrn( ) ;
         }
      }

      public bool Update( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars3( bcRecorrido, 0) ;
         UpdateImpl( ) ;
         VarsToRow3( bcRecorrido) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public bool InsertOrUpdate( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars3( bcRecorrido, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert033( ) ;
         if ( AnyError == 1 )
         {
            AnyError = 0;
            context.GX_msglist.removeAllItems();
            UpdateImpl( ) ;
         }
         else
         {
            AfterTrn( ) ;
         }
         VarsToRow3( bcRecorrido) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars3( bcRecorrido, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey033( ) ;
         if ( RcdFound3 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A23RecorridoID != Z23RecorridoID )
            {
               A23RecorridoID = (Guid)(Z23RecorridoID);
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A23RecorridoID != Z23RecorridoID )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(5);
         pr_default.close(1);
         pr_gam.rollback( "Recorrido_BC");
         pr_default.rollback( "Recorrido_BC");
         VarsToRow3( bcRecorrido) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcRecorrido.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcRecorrido.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcRecorrido )
         {
            bcRecorrido = (SdtRecorrido)(sdt);
            if ( StringUtil.StrCmp(bcRecorrido.gxTpr_Mode, "") == 0 )
            {
               bcRecorrido.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow3( bcRecorrido) ;
            }
            else
            {
               RowToVars3( bcRecorrido, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcRecorrido.gxTpr_Mode, "") == 0 )
            {
               bcRecorrido.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars3( bcRecorrido, 1) ;
         return  ;
      }

      public SdtRecorrido Recorrido_BC
      {
         get {
            return bcRecorrido ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "recorrido_Execute" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(5);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z23RecorridoID = (Guid)(Guid.Empty);
         A23RecorridoID = (Guid)(Guid.Empty);
         sMode3 = "";
         Z24RecorridoFecha = (DateTime)(DateTime.MinValue);
         A24RecorridoFecha = (DateTime)(DateTime.MinValue);
         BC00038_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC00038_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         BC00039_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC00037_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC00037_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         BC00036_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC00036_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         BC000313_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC000313_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         Z31IMEI = "";
         A31IMEI = "";
         Z33TimeStamp = (DateTime)(DateTime.MinValue);
         A33TimeStamp = (DateTime)(DateTime.MinValue);
         Z21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         Z22IDCoach = (Guid)(Guid.Empty);
         A22IDCoach = (Guid)(Guid.Empty);
         Z32RecorridoDetalleID = (Guid)(Guid.Empty);
         A32RecorridoDetalleID = (Guid)(Guid.Empty);
         Z26Longitud = "";
         A26Longitud = "";
         Z27Latitud = "";
         A27Latitud = "";
         BC000314_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC000314_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         BC000314_A25TipoSenal = new short[1] ;
         BC000314_n25TipoSenal = new bool[] {false} ;
         BC000314_A26Longitud = new String[] {""} ;
         BC000314_n26Longitud = new bool[] {false} ;
         BC000314_A27Latitud = new String[] {""} ;
         BC000314_n27Latitud = new bool[] {false} ;
         BC000314_A28TipoObstaculo = new short[1] ;
         BC000314_n28TipoObstaculo = new bool[] {false} ;
         BC000314_A29Incidente = new short[1] ;
         BC000314_n29Incidente = new bool[] {false} ;
         BC000314_A30Emergencia = new short[1] ;
         BC000314_n30Emergencia = new bool[] {false} ;
         BC000314_A31IMEI = new String[] {""} ;
         BC000314_n31IMEI = new bool[] {false} ;
         BC000314_A33TimeStamp = new DateTime[] {DateTime.MinValue} ;
         BC000314_n33TimeStamp = new bool[] {false} ;
         BC000314_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         BC000314_n21RecorridoDetallePaciente = new bool[] {false} ;
         BC000314_A22IDCoach = new Guid[] {Guid.Empty} ;
         BC000314_n22IDCoach = new bool[] {false} ;
         BC00034_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         BC00034_n21RecorridoDetallePaciente = new bool[] {false} ;
         BC00035_A22IDCoach = new Guid[] {Guid.Empty} ;
         BC00035_n22IDCoach = new bool[] {false} ;
         BC000315_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC000315_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         BC00033_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC00033_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         BC00033_A25TipoSenal = new short[1] ;
         BC00033_n25TipoSenal = new bool[] {false} ;
         BC00033_A26Longitud = new String[] {""} ;
         BC00033_n26Longitud = new bool[] {false} ;
         BC00033_A27Latitud = new String[] {""} ;
         BC00033_n27Latitud = new bool[] {false} ;
         BC00033_A28TipoObstaculo = new short[1] ;
         BC00033_n28TipoObstaculo = new bool[] {false} ;
         BC00033_A29Incidente = new short[1] ;
         BC00033_n29Incidente = new bool[] {false} ;
         BC00033_A30Emergencia = new short[1] ;
         BC00033_n30Emergencia = new bool[] {false} ;
         BC00033_A31IMEI = new String[] {""} ;
         BC00033_n31IMEI = new bool[] {false} ;
         BC00033_A33TimeStamp = new DateTime[] {DateTime.MinValue} ;
         BC00033_n33TimeStamp = new bool[] {false} ;
         BC00033_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         BC00033_n21RecorridoDetallePaciente = new bool[] {false} ;
         BC00033_A22IDCoach = new Guid[] {Guid.Empty} ;
         BC00033_n22IDCoach = new bool[] {false} ;
         sMode5 = "";
         BC00032_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC00032_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         BC00032_A25TipoSenal = new short[1] ;
         BC00032_n25TipoSenal = new bool[] {false} ;
         BC00032_A26Longitud = new String[] {""} ;
         BC00032_n26Longitud = new bool[] {false} ;
         BC00032_A27Latitud = new String[] {""} ;
         BC00032_n27Latitud = new bool[] {false} ;
         BC00032_A28TipoObstaculo = new short[1] ;
         BC00032_n28TipoObstaculo = new bool[] {false} ;
         BC00032_A29Incidente = new short[1] ;
         BC00032_n29Incidente = new bool[] {false} ;
         BC00032_A30Emergencia = new short[1] ;
         BC00032_n30Emergencia = new bool[] {false} ;
         BC00032_A31IMEI = new String[] {""} ;
         BC00032_n31IMEI = new bool[] {false} ;
         BC00032_A33TimeStamp = new DateTime[] {DateTime.MinValue} ;
         BC00032_n33TimeStamp = new bool[] {false} ;
         BC00032_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         BC00032_n21RecorridoDetallePaciente = new bool[] {false} ;
         BC00032_A22IDCoach = new Guid[] {Guid.Empty} ;
         BC00032_n22IDCoach = new bool[] {false} ;
         BC000319_A23RecorridoID = new Guid[] {Guid.Empty} ;
         BC000319_A32RecorridoDetalleID = new Guid[] {Guid.Empty} ;
         BC000319_A25TipoSenal = new short[1] ;
         BC000319_n25TipoSenal = new bool[] {false} ;
         BC000319_A26Longitud = new String[] {""} ;
         BC000319_n26Longitud = new bool[] {false} ;
         BC000319_A27Latitud = new String[] {""} ;
         BC000319_n27Latitud = new bool[] {false} ;
         BC000319_A28TipoObstaculo = new short[1] ;
         BC000319_n28TipoObstaculo = new bool[] {false} ;
         BC000319_A29Incidente = new short[1] ;
         BC000319_n29Incidente = new bool[] {false} ;
         BC000319_A30Emergencia = new short[1] ;
         BC000319_n30Emergencia = new bool[] {false} ;
         BC000319_A31IMEI = new String[] {""} ;
         BC000319_n31IMEI = new bool[] {false} ;
         BC000319_A33TimeStamp = new DateTime[] {DateTime.MinValue} ;
         BC000319_n33TimeStamp = new bool[] {false} ;
         BC000319_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         BC000319_n21RecorridoDetallePaciente = new bool[] {false} ;
         BC000319_A22IDCoach = new Guid[] {Guid.Empty} ;
         BC000319_n22IDCoach = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.recorrido_bc__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.recorrido_bc__default(),
            new Object[][] {
                new Object[] {
               BC00032_A23RecorridoID, BC00032_A32RecorridoDetalleID, BC00032_A25TipoSenal, BC00032_n25TipoSenal, BC00032_A26Longitud, BC00032_n26Longitud, BC00032_A27Latitud, BC00032_n27Latitud, BC00032_A28TipoObstaculo, BC00032_n28TipoObstaculo,
               BC00032_A29Incidente, BC00032_n29Incidente, BC00032_A30Emergencia, BC00032_n30Emergencia, BC00032_A31IMEI, BC00032_n31IMEI, BC00032_A33TimeStamp, BC00032_n33TimeStamp, BC00032_A21RecorridoDetallePaciente, BC00032_n21RecorridoDetallePaciente,
               BC00032_A22IDCoach, BC00032_n22IDCoach
               }
               , new Object[] {
               BC00033_A23RecorridoID, BC00033_A32RecorridoDetalleID, BC00033_A25TipoSenal, BC00033_n25TipoSenal, BC00033_A26Longitud, BC00033_n26Longitud, BC00033_A27Latitud, BC00033_n27Latitud, BC00033_A28TipoObstaculo, BC00033_n28TipoObstaculo,
               BC00033_A29Incidente, BC00033_n29Incidente, BC00033_A30Emergencia, BC00033_n30Emergencia, BC00033_A31IMEI, BC00033_n31IMEI, BC00033_A33TimeStamp, BC00033_n33TimeStamp, BC00033_A21RecorridoDetallePaciente, BC00033_n21RecorridoDetallePaciente,
               BC00033_A22IDCoach, BC00033_n22IDCoach
               }
               , new Object[] {
               BC00034_A21RecorridoDetallePaciente
               }
               , new Object[] {
               BC00035_A22IDCoach
               }
               , new Object[] {
               BC00036_A23RecorridoID, BC00036_A24RecorridoFecha
               }
               , new Object[] {
               BC00037_A23RecorridoID, BC00037_A24RecorridoFecha
               }
               , new Object[] {
               BC00038_A23RecorridoID, BC00038_A24RecorridoFecha
               }
               , new Object[] {
               BC00039_A23RecorridoID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000313_A23RecorridoID, BC000313_A24RecorridoFecha
               }
               , new Object[] {
               BC000314_A23RecorridoID, BC000314_A32RecorridoDetalleID, BC000314_A25TipoSenal, BC000314_n25TipoSenal, BC000314_A26Longitud, BC000314_n26Longitud, BC000314_A27Latitud, BC000314_n27Latitud, BC000314_A28TipoObstaculo, BC000314_n28TipoObstaculo,
               BC000314_A29Incidente, BC000314_n29Incidente, BC000314_A30Emergencia, BC000314_n30Emergencia, BC000314_A31IMEI, BC000314_n31IMEI, BC000314_A33TimeStamp, BC000314_n33TimeStamp, BC000314_A21RecorridoDetallePaciente, BC000314_n21RecorridoDetallePaciente,
               BC000314_A22IDCoach, BC000314_n22IDCoach
               }
               , new Object[] {
               BC000315_A23RecorridoID, BC000315_A32RecorridoDetalleID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000319_A23RecorridoID, BC000319_A32RecorridoDetalleID, BC000319_A25TipoSenal, BC000319_n25TipoSenal, BC000319_A26Longitud, BC000319_n26Longitud, BC000319_A27Latitud, BC000319_n27Latitud, BC000319_A28TipoObstaculo, BC000319_n28TipoObstaculo,
               BC000319_A29Incidente, BC000319_n29Incidente, BC000319_A30Emergencia, BC000319_n30Emergencia, BC000319_A31IMEI, BC000319_n31IMEI, BC000319_A33TimeStamp, BC000319_n33TimeStamp, BC000319_A21RecorridoDetallePaciente, BC000319_n21RecorridoDetallePaciente,
               BC000319_A22IDCoach, BC000319_n22IDCoach
               }
            }
         );
         Z32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
         A32RecorridoDetalleID = (Guid)(Guid.NewGuid( ));
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short nGXsfl_5_idx=1 ;
      private short nIsMod_5 ;
      private short RcdFound5 ;
      private short GX_JID ;
      private short RcdFound3 ;
      private short nRcdExists_5 ;
      private short Gxremove5 ;
      private short Z25TipoSenal ;
      private short A25TipoSenal ;
      private short Z28TipoObstaculo ;
      private short A28TipoObstaculo ;
      private short Z29Incidente ;
      private short A29Incidente ;
      private short Z30Emergencia ;
      private short A30Emergencia ;
      private short Gx_BScreen ;
      private int trnEnded ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode3 ;
      private String sMode5 ;
      private DateTime Z24RecorridoFecha ;
      private DateTime A24RecorridoFecha ;
      private DateTime Z33TimeStamp ;
      private DateTime A33TimeStamp ;
      private bool n25TipoSenal ;
      private bool n26Longitud ;
      private bool n27Latitud ;
      private bool n28TipoObstaculo ;
      private bool n29Incidente ;
      private bool n30Emergencia ;
      private bool n31IMEI ;
      private bool n33TimeStamp ;
      private bool n21RecorridoDetallePaciente ;
      private bool n22IDCoach ;
      private bool Gx_longc ;
      private String Z26Longitud ;
      private String A26Longitud ;
      private String Z27Latitud ;
      private String A27Latitud ;
      private String Z31IMEI ;
      private String A31IMEI ;
      private Guid Z23RecorridoID ;
      private Guid A23RecorridoID ;
      private Guid Z21RecorridoDetallePaciente ;
      private Guid A21RecorridoDetallePaciente ;
      private Guid Z22IDCoach ;
      private Guid A22IDCoach ;
      private Guid Z32RecorridoDetalleID ;
      private Guid A32RecorridoDetalleID ;
      private SdtRecorrido bcRecorrido ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] BC00038_A23RecorridoID ;
      private DateTime[] BC00038_A24RecorridoFecha ;
      private Guid[] BC00039_A23RecorridoID ;
      private Guid[] BC00037_A23RecorridoID ;
      private DateTime[] BC00037_A24RecorridoFecha ;
      private Guid[] BC00036_A23RecorridoID ;
      private DateTime[] BC00036_A24RecorridoFecha ;
      private Guid[] BC000313_A23RecorridoID ;
      private DateTime[] BC000313_A24RecorridoFecha ;
      private Guid[] BC000314_A23RecorridoID ;
      private Guid[] BC000314_A32RecorridoDetalleID ;
      private short[] BC000314_A25TipoSenal ;
      private bool[] BC000314_n25TipoSenal ;
      private String[] BC000314_A26Longitud ;
      private bool[] BC000314_n26Longitud ;
      private String[] BC000314_A27Latitud ;
      private bool[] BC000314_n27Latitud ;
      private short[] BC000314_A28TipoObstaculo ;
      private bool[] BC000314_n28TipoObstaculo ;
      private short[] BC000314_A29Incidente ;
      private bool[] BC000314_n29Incidente ;
      private short[] BC000314_A30Emergencia ;
      private bool[] BC000314_n30Emergencia ;
      private String[] BC000314_A31IMEI ;
      private bool[] BC000314_n31IMEI ;
      private DateTime[] BC000314_A33TimeStamp ;
      private bool[] BC000314_n33TimeStamp ;
      private Guid[] BC000314_A21RecorridoDetallePaciente ;
      private bool[] BC000314_n21RecorridoDetallePaciente ;
      private Guid[] BC000314_A22IDCoach ;
      private bool[] BC000314_n22IDCoach ;
      private Guid[] BC00034_A21RecorridoDetallePaciente ;
      private bool[] BC00034_n21RecorridoDetallePaciente ;
      private Guid[] BC00035_A22IDCoach ;
      private bool[] BC00035_n22IDCoach ;
      private Guid[] BC000315_A23RecorridoID ;
      private Guid[] BC000315_A32RecorridoDetalleID ;
      private Guid[] BC00033_A23RecorridoID ;
      private Guid[] BC00033_A32RecorridoDetalleID ;
      private short[] BC00033_A25TipoSenal ;
      private bool[] BC00033_n25TipoSenal ;
      private String[] BC00033_A26Longitud ;
      private bool[] BC00033_n26Longitud ;
      private String[] BC00033_A27Latitud ;
      private bool[] BC00033_n27Latitud ;
      private short[] BC00033_A28TipoObstaculo ;
      private bool[] BC00033_n28TipoObstaculo ;
      private short[] BC00033_A29Incidente ;
      private bool[] BC00033_n29Incidente ;
      private short[] BC00033_A30Emergencia ;
      private bool[] BC00033_n30Emergencia ;
      private String[] BC00033_A31IMEI ;
      private bool[] BC00033_n31IMEI ;
      private DateTime[] BC00033_A33TimeStamp ;
      private bool[] BC00033_n33TimeStamp ;
      private Guid[] BC00033_A21RecorridoDetallePaciente ;
      private bool[] BC00033_n21RecorridoDetallePaciente ;
      private Guid[] BC00033_A22IDCoach ;
      private bool[] BC00033_n22IDCoach ;
      private Guid[] BC00032_A23RecorridoID ;
      private Guid[] BC00032_A32RecorridoDetalleID ;
      private short[] BC00032_A25TipoSenal ;
      private bool[] BC00032_n25TipoSenal ;
      private String[] BC00032_A26Longitud ;
      private bool[] BC00032_n26Longitud ;
      private String[] BC00032_A27Latitud ;
      private bool[] BC00032_n27Latitud ;
      private short[] BC00032_A28TipoObstaculo ;
      private bool[] BC00032_n28TipoObstaculo ;
      private short[] BC00032_A29Incidente ;
      private bool[] BC00032_n29Incidente ;
      private short[] BC00032_A30Emergencia ;
      private bool[] BC00032_n30Emergencia ;
      private String[] BC00032_A31IMEI ;
      private bool[] BC00032_n31IMEI ;
      private DateTime[] BC00032_A33TimeStamp ;
      private bool[] BC00032_n33TimeStamp ;
      private Guid[] BC00032_A21RecorridoDetallePaciente ;
      private bool[] BC00032_n21RecorridoDetallePaciente ;
      private Guid[] BC00032_A22IDCoach ;
      private bool[] BC00032_n22IDCoach ;
      private Guid[] BC000319_A23RecorridoID ;
      private Guid[] BC000319_A32RecorridoDetalleID ;
      private short[] BC000319_A25TipoSenal ;
      private bool[] BC000319_n25TipoSenal ;
      private String[] BC000319_A26Longitud ;
      private bool[] BC000319_n26Longitud ;
      private String[] BC000319_A27Latitud ;
      private bool[] BC000319_n27Latitud ;
      private short[] BC000319_A28TipoObstaculo ;
      private bool[] BC000319_n28TipoObstaculo ;
      private short[] BC000319_A29Incidente ;
      private bool[] BC000319_n29Incidente ;
      private short[] BC000319_A30Emergencia ;
      private bool[] BC000319_n30Emergencia ;
      private String[] BC000319_A31IMEI ;
      private bool[] BC000319_n31IMEI ;
      private DateTime[] BC000319_A33TimeStamp ;
      private bool[] BC000319_n33TimeStamp ;
      private Guid[] BC000319_A21RecorridoDetallePaciente ;
      private bool[] BC000319_n21RecorridoDetallePaciente ;
      private Guid[] BC000319_A22IDCoach ;
      private bool[] BC000319_n22IDCoach ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_gam ;
   }

   public class recorrido_bc__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class recorrido_bc__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new ForEachCursor(def[6])
       ,new ForEachCursor(def[7])
       ,new UpdateCursor(def[8])
       ,new UpdateCursor(def[9])
       ,new UpdateCursor(def[10])
       ,new ForEachCursor(def[11])
       ,new ForEachCursor(def[12])
       ,new ForEachCursor(def[13])
       ,new UpdateCursor(def[14])
       ,new UpdateCursor(def[15])
       ,new UpdateCursor(def[16])
       ,new ForEachCursor(def[17])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmBC00038 ;
        prmBC00038 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00039 ;
        prmBC00039 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00037 ;
        prmBC00037 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00036 ;
        prmBC00036 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000310 ;
        prmBC000310 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoFecha",SqlDbType.DateTime,8,5}
        } ;
        Object[] prmBC000311 ;
        prmBC000311 = new Object[] {
        new Object[] {"@RecorridoFecha",SqlDbType.DateTime,8,5} ,
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000312 ;
        prmBC000312 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000313 ;
        prmBC000313 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000314 ;
        prmBC000314 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00034 ;
        prmBC00034 = new Object[] {
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00035 ;
        prmBC00035 = new Object[] {
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000315 ;
        prmBC000315 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00033 ;
        prmBC00033 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00032 ;
        prmBC00032 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000316 ;
        prmBC000316 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@TipoSenal",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Longitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@Latitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@TipoObstaculo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Incidente",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Emergencia",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@IMEI",SqlDbType.VarChar,50,0} ,
        new Object[] {"@TimeStamp",SqlDbType.DateTime,8,5} ,
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000317 ;
        prmBC000317 = new Object[] {
        new Object[] {"@TipoSenal",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Longitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@Latitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@TipoObstaculo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Incidente",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@Emergencia",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@IMEI",SqlDbType.VarChar,50,0} ,
        new Object[] {"@TimeStamp",SqlDbType.DateTime,8,5} ,
        new Object[] {"@RecorridoDetallePaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@IDCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000318 ;
        prmBC000318 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@RecorridoDetalleID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000319 ;
        prmBC000319 = new Object[] {
        new Object[] {"@RecorridoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("BC00032", "SELECT [RecorridoID], [RecorridoDetalleID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente] AS RecorridoDetallePaciente, [IDCoach] AS IDCoach FROM [RecorridoDetalle] WITH (UPDLOCK) WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00032,1,0,true,false )
           ,new CursorDef("BC00033", "SELECT [RecorridoID], [RecorridoDetalleID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente] AS RecorridoDetallePaciente, [IDCoach] AS IDCoach FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00033,1,0,true,false )
           ,new CursorDef("BC00034", "SELECT [PersonaID] AS RecorridoDetallePaciente FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @RecorridoDetallePaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00034,1,0,true,false )
           ,new CursorDef("BC00035", "SELECT [PersonaID] AS IDCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @IDCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00035,1,0,true,false )
           ,new CursorDef("BC00036", "SELECT [RecorridoID], [RecorridoFecha] FROM [Recorrido] WITH (UPDLOCK) WHERE [RecorridoID] = @RecorridoID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00036,1,0,true,false )
           ,new CursorDef("BC00037", "SELECT [RecorridoID], [RecorridoFecha] FROM [Recorrido] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00037,1,0,true,false )
           ,new CursorDef("BC00038", "SELECT TM1.[RecorridoID], TM1.[RecorridoFecha] FROM [Recorrido] TM1 WITH (NOLOCK) WHERE TM1.[RecorridoID] = @RecorridoID ORDER BY TM1.[RecorridoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00038,100,0,true,false )
           ,new CursorDef("BC00039", "SELECT [RecorridoID] FROM [Recorrido] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00039,1,0,true,false )
           ,new CursorDef("BC000310", "INSERT INTO [Recorrido]([RecorridoID], [RecorridoFecha]) VALUES(@RecorridoID, @RecorridoFecha)", GxErrorMask.GX_NOMASK,prmBC000310)
           ,new CursorDef("BC000311", "UPDATE [Recorrido] SET [RecorridoFecha]=@RecorridoFecha  WHERE [RecorridoID] = @RecorridoID", GxErrorMask.GX_NOMASK,prmBC000311)
           ,new CursorDef("BC000312", "DELETE FROM [Recorrido]  WHERE [RecorridoID] = @RecorridoID", GxErrorMask.GX_NOMASK,prmBC000312)
           ,new CursorDef("BC000313", "SELECT TM1.[RecorridoID], TM1.[RecorridoFecha] FROM [Recorrido] TM1 WITH (NOLOCK) WHERE TM1.[RecorridoID] = @RecorridoID ORDER BY TM1.[RecorridoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000313,100,0,true,false )
           ,new CursorDef("BC000314", "SELECT [RecorridoID], [RecorridoDetalleID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente] AS RecorridoDetallePaciente, [IDCoach] AS IDCoach FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID and [RecorridoDetalleID] = @RecorridoDetalleID ORDER BY [RecorridoID], [RecorridoDetalleID]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000314,11,0,true,false )
           ,new CursorDef("BC000315", "SELECT [RecorridoID], [RecorridoDetalleID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000315,1,0,true,false )
           ,new CursorDef("BC000316", "INSERT INTO [RecorridoDetalle]([RecorridoID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente], [IDCoach], [RecorridoDetalleID]) VALUES(@RecorridoID, @TipoSenal, @Longitud, @Latitud, @TipoObstaculo, @Incidente, @Emergencia, @IMEI, @TimeStamp, @RecorridoDetallePaciente, @IDCoach, @RecorridoDetalleID)", GxErrorMask.GX_NOMASK,prmBC000316)
           ,new CursorDef("BC000317", "UPDATE [RecorridoDetalle] SET [TipoSenal]=@TipoSenal, [Longitud]=@Longitud, [Latitud]=@Latitud, [TipoObstaculo]=@TipoObstaculo, [Incidente]=@Incidente, [Emergencia]=@Emergencia, [IMEI]=@IMEI, [TimeStamp]=@TimeStamp, [RecorridoDetallePaciente]=@RecorridoDetallePaciente, [IDCoach]=@IDCoach  WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID", GxErrorMask.GX_NOMASK,prmBC000317)
           ,new CursorDef("BC000318", "DELETE FROM [RecorridoDetalle]  WHERE [RecorridoID] = @RecorridoID AND [RecorridoDetalleID] = @RecorridoDetalleID", GxErrorMask.GX_NOMASK,prmBC000318)
           ,new CursorDef("BC000319", "SELECT [RecorridoID], [RecorridoDetalleID], [TipoSenal], [Longitud], [Latitud], [TipoObstaculo], [Incidente], [Emergencia], [IMEI], [TimeStamp], [RecorridoDetallePaciente] AS RecorridoDetallePaciente, [IDCoach] AS IDCoach FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoID] = @RecorridoID ORDER BY [RecorridoID], [RecorridoDetalleID]  OPTION (FAST 11)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000319,11,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((short[]) buf[8])[0] = rslt.getShort(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((short[]) buf[10])[0] = rslt.getShort(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((short[]) buf[12])[0] = rslt.getShort(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(9);
              ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(10);
              ((Guid[]) buf[18])[0] = rslt.getGuid(11) ;
              ((bool[]) buf[19])[0] = rslt.wasNull(11);
              ((Guid[]) buf[20])[0] = rslt.getGuid(12) ;
              ((bool[]) buf[21])[0] = rslt.wasNull(12);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((short[]) buf[8])[0] = rslt.getShort(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((short[]) buf[10])[0] = rslt.getShort(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((short[]) buf[12])[0] = rslt.getShort(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(9);
              ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(10);
              ((Guid[]) buf[18])[0] = rslt.getGuid(11) ;
              ((bool[]) buf[19])[0] = rslt.wasNull(11);
              ((Guid[]) buf[20])[0] = rslt.getGuid(12) ;
              ((bool[]) buf[21])[0] = rslt.wasNull(12);
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 4 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              return;
           case 5 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              return;
           case 6 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              return;
           case 7 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 11 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              return;
           case 12 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((short[]) buf[8])[0] = rslt.getShort(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((short[]) buf[10])[0] = rslt.getShort(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((short[]) buf[12])[0] = rslt.getShort(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(9);
              ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(10);
              ((Guid[]) buf[18])[0] = rslt.getGuid(11) ;
              ((bool[]) buf[19])[0] = rslt.wasNull(11);
              ((Guid[]) buf[20])[0] = rslt.getGuid(12) ;
              ((bool[]) buf[21])[0] = rslt.wasNull(12);
              return;
           case 13 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              return;
           case 17 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((Guid[]) buf[1])[0] = rslt.getGuid(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((bool[]) buf[3])[0] = rslt.wasNull(3);
              ((String[]) buf[4])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[5])[0] = rslt.wasNull(4);
              ((String[]) buf[6])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[7])[0] = rslt.wasNull(5);
              ((short[]) buf[8])[0] = rslt.getShort(6) ;
              ((bool[]) buf[9])[0] = rslt.wasNull(6);
              ((short[]) buf[10])[0] = rslt.getShort(7) ;
              ((bool[]) buf[11])[0] = rslt.wasNull(7);
              ((short[]) buf[12])[0] = rslt.getShort(8) ;
              ((bool[]) buf[13])[0] = rslt.wasNull(8);
              ((String[]) buf[14])[0] = rslt.getVarchar(9) ;
              ((bool[]) buf[15])[0] = rslt.wasNull(9);
              ((DateTime[]) buf[16])[0] = rslt.getGXDateTime(10) ;
              ((bool[]) buf[17])[0] = rslt.wasNull(10);
              ((Guid[]) buf[18])[0] = rslt.getGuid(11) ;
              ((bool[]) buf[19])[0] = rslt.wasNull(11);
              ((Guid[]) buf[20])[0] = rslt.getGuid(12) ;
              ((bool[]) buf[21])[0] = rslt.wasNull(12);
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 2 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 3 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 5 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 6 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 7 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameterDatetime(2, (DateTime)parms[1]);
              return;
           case 9 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 10 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 11 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 12 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 13 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 14 :
              stmt.SetParameter(1, (Guid)parms[0]);
              if ( (bool)parms[1] )
              {
                 stmt.setNull( 2 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(2, (short)parms[2]);
              }
              if ( (bool)parms[3] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[4]);
              }
              if ( (bool)parms[5] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[6]);
              }
              if ( (bool)parms[7] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[8]);
              }
              if ( (bool)parms[9] )
              {
                 stmt.setNull( 6 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(6, (short)parms[10]);
              }
              if ( (bool)parms[11] )
              {
                 stmt.setNull( 7 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(7, (short)parms[12]);
              }
              if ( (bool)parms[13] )
              {
                 stmt.setNull( 8 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(8, (String)parms[14]);
              }
              if ( (bool)parms[15] )
              {
                 stmt.setNull( 9 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameterDatetime(9, (DateTime)parms[16]);
              }
              if ( (bool)parms[17] )
              {
                 stmt.setNull( 10 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(10, (Guid)parms[18]);
              }
              if ( (bool)parms[19] )
              {
                 stmt.setNull( 11 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(11, (Guid)parms[20]);
              }
              stmt.SetParameter(12, (Guid)parms[21]);
              return;
           case 15 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(1, (short)parms[1]);
              }
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 2 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(2, (String)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 4 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(4, (short)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[9]);
              }
              if ( (bool)parms[10] )
              {
                 stmt.setNull( 6 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(6, (short)parms[11]);
              }
              if ( (bool)parms[12] )
              {
                 stmt.setNull( 7 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(7, (String)parms[13]);
              }
              if ( (bool)parms[14] )
              {
                 stmt.setNull( 8 , SqlDbType.DateTime );
              }
              else
              {
                 stmt.SetParameterDatetime(8, (DateTime)parms[15]);
              }
              if ( (bool)parms[16] )
              {
                 stmt.setNull( 9 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(9, (Guid)parms[17]);
              }
              if ( (bool)parms[18] )
              {
                 stmt.setNull( 10 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(10, (Guid)parms[19]);
              }
              stmt.SetParameter(11, (Guid)parms[20]);
              stmt.SetParameter(12, (Guid)parms[21]);
              return;
           case 16 :
              stmt.SetParameter(1, (Guid)parms[0]);
              stmt.SetParameter(2, (Guid)parms[1]);
              return;
           case 17 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
