/*
               File: registrarrecorrido
        Description: registrarrecorrido
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:30:18.40
       Program type: Main program
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Mail;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class aregistrarrecorrido : GXProcedure
   {
      public aregistrarrecorrido( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public aregistrarrecorrido( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_IDSesion ,
                           short aP1_TipoSenal ,
                           ref String aP2_Longitud ,
                           ref String aP3_Latitud ,
                           ref short aP4_TipoObstaculo ,
                           short aP5_Incidente ,
                           short aP6_Emergencia ,
                           ref String aP7_IMEI ,
                           ref String aP8_IDCoach ,
                           ref String aP9_IDPaciente )
      {
         this.AV11IDSesion = aP0_IDSesion;
         this.AV10TipoSenal = aP1_TipoSenal;
         this.AV12Longitud = aP2_Longitud;
         this.AV13Latitud = aP3_Latitud;
         this.AV14TipoObstaculo = aP4_TipoObstaculo;
         this.AV15Incidente = aP5_Incidente;
         this.AV16Emergencia = aP6_Emergencia;
         this.AV17IMEI = aP7_IMEI;
         this.AV18IDCoach = aP8_IDCoach;
         this.AV19IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV11IDSesion;
         aP2_Longitud=this.AV12Longitud;
         aP3_Latitud=this.AV13Latitud;
         aP4_TipoObstaculo=this.AV14TipoObstaculo;
         aP7_IMEI=this.AV17IMEI;
         aP8_IDCoach=this.AV18IDCoach;
         aP9_IDPaciente=this.AV19IDPaciente;
      }

      public String executeUdp( ref String aP0_IDSesion ,
                                short aP1_TipoSenal ,
                                ref String aP2_Longitud ,
                                ref String aP3_Latitud ,
                                ref short aP4_TipoObstaculo ,
                                short aP5_Incidente ,
                                short aP6_Emergencia ,
                                ref String aP7_IMEI ,
                                ref String aP8_IDCoach )
      {
         this.AV11IDSesion = aP0_IDSesion;
         this.AV10TipoSenal = aP1_TipoSenal;
         this.AV12Longitud = aP2_Longitud;
         this.AV13Latitud = aP3_Latitud;
         this.AV14TipoObstaculo = aP4_TipoObstaculo;
         this.AV15Incidente = aP5_Incidente;
         this.AV16Emergencia = aP6_Emergencia;
         this.AV17IMEI = aP7_IMEI;
         this.AV18IDCoach = aP8_IDCoach;
         this.AV19IDPaciente = aP9_IDPaciente;
         initialize();
         executePrivate();
         aP0_IDSesion=this.AV11IDSesion;
         aP2_Longitud=this.AV12Longitud;
         aP3_Latitud=this.AV13Latitud;
         aP4_TipoObstaculo=this.AV14TipoObstaculo;
         aP7_IMEI=this.AV17IMEI;
         aP8_IDCoach=this.AV18IDCoach;
         aP9_IDPaciente=this.AV19IDPaciente;
         return AV19IDPaciente ;
      }

      public void executeSubmit( ref String aP0_IDSesion ,
                                 short aP1_TipoSenal ,
                                 ref String aP2_Longitud ,
                                 ref String aP3_Latitud ,
                                 ref short aP4_TipoObstaculo ,
                                 short aP5_Incidente ,
                                 short aP6_Emergencia ,
                                 ref String aP7_IMEI ,
                                 ref String aP8_IDCoach ,
                                 ref String aP9_IDPaciente )
      {
         aregistrarrecorrido objaregistrarrecorrido;
         objaregistrarrecorrido = new aregistrarrecorrido();
         objaregistrarrecorrido.AV11IDSesion = aP0_IDSesion;
         objaregistrarrecorrido.AV10TipoSenal = aP1_TipoSenal;
         objaregistrarrecorrido.AV12Longitud = aP2_Longitud;
         objaregistrarrecorrido.AV13Latitud = aP3_Latitud;
         objaregistrarrecorrido.AV14TipoObstaculo = aP4_TipoObstaculo;
         objaregistrarrecorrido.AV15Incidente = aP5_Incidente;
         objaregistrarrecorrido.AV16Emergencia = aP6_Emergencia;
         objaregistrarrecorrido.AV17IMEI = aP7_IMEI;
         objaregistrarrecorrido.AV18IDCoach = aP8_IDCoach;
         objaregistrarrecorrido.AV19IDPaciente = aP9_IDPaciente;
         objaregistrarrecorrido.context.SetSubmitInitialConfig(context);
         objaregistrarrecorrido.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objaregistrarrecorrido);
         aP0_IDSesion=this.AV11IDSesion;
         aP2_Longitud=this.AV12Longitud;
         aP3_Latitud=this.AV13Latitud;
         aP4_TipoObstaculo=this.AV14TipoObstaculo;
         aP7_IMEI=this.AV17IMEI;
         aP8_IDCoach=this.AV18IDCoach;
         aP9_IDPaciente=this.AV19IDPaciente;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((aregistrarrecorrido)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         if ( AV10TipoSenal == 1 )
         {
            AV8GUID = (Guid)(Guid.NewGuid( ));
            AV9Recorrido = new SdtRecorrido(context);
            AV9Recorrido.gxTpr_Recorridoid = (Guid)(AV8GUID);
            AV9Recorrido.gxTpr_Recorridofecha = DateTimeUtil.ServerNow( context, pr_default);
            /* Execute user subroutine: 'DETALLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'GUARDAR' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            AV11IDSesion = StringUtil.Trim( AV8GUID.ToString());
         }
         else if ( AV10TipoSenal == 2 )
         {
            AV8GUID = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV11IDSesion)));
            AV9Recorrido.Load(AV8GUID);
            /* Execute user subroutine: 'DETALLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'MANEJAOBSTACULOS' */
            S121 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'GUARDAR' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         else if ( AV10TipoSenal == 3 )
         {
            AV8GUID = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV11IDSesion)));
            AV9Recorrido.Load(AV8GUID);
            /* Execute user subroutine: 'DETALLE' */
            S111 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
            /* Execute user subroutine: 'GUARDAR' */
            S131 ();
            if ( returnInSub )
            {
               this.cleanup();
               if (true) return;
            }
         }
         this.cleanup();
      }

      protected void S111( )
      {
         /* 'DETALLE' Routine */
         AV21Detalle = new SdtRecorrido_Detalle(context);
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19IDPaciente)) )
         {
            AV21Detalle.gxTpr_Recorridodetallepaciente = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV19IDPaciente)));
         }
         else
         {
            AV21Detalle.gxTv_SdtRecorrido_Detalle_Recorridodetallepaciente_SetNull();
         }
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18IDCoach)) )
         {
            AV21Detalle.gxTpr_Idcoach = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV18IDCoach)));
         }
         else
         {
            AV21Detalle.gxTv_SdtRecorrido_Detalle_Idcoach_SetNull();
         }
         AV21Detalle.gxTpr_Tiposenal = AV10TipoSenal;
         AV21Detalle.gxTpr_Longitud = AV12Longitud;
         AV21Detalle.gxTpr_Latitud = AV13Latitud;
         AV21Detalle.gxTpr_Tipoobstaculo = AV14TipoObstaculo;
         AV21Detalle.gxTpr_Incidente = AV15Incidente;
         AV21Detalle.gxTpr_Emergencia = AV16Emergencia;
         AV21Detalle.gxTpr_Imei = AV17IMEI;
         AV21Detalle.gxTpr_Timestamp = DateTimeUtil.ServerNow( context, pr_default);
         AV9Recorrido.gxTpr_Detalle.Add(AV21Detalle, 0);
      }

      protected void S121( )
      {
         /* 'MANEJAOBSTACULOS' Routine */
         if ( AV15Incidente == 1 )
         {
            AV23Obstaculo = new SdtObstaculo(context);
            AV23Obstaculo.gxTpr_Obstaculofecharegistro = DateTimeUtil.ServerNow( context, pr_default);
            AV23Obstaculo.gxTpr_Obstaculotipo = AV14TipoObstaculo;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19IDPaciente)) )
            {
               AV23Obstaculo.gxTpr_Obstaculopacienteregistra = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV19IDPaciente)));
            }
            else
            {
               AV23Obstaculo.gxTv_SdtObstaculo_Obstaculopacienteregistra_SetNull();
            }
            AV23Obstaculo.gxTpr_Obstaculolongitud = AV12Longitud;
            AV23Obstaculo.gxTpr_Obstaculolatitud = AV13Latitud;
            AV23Obstaculo.Save();
         }
         else
         {
            AV29GXLvl72 = 0;
            /* Using cursor P002L2 */
            pr_default.execute(0, new Object[] {AV13Latitud, AV12Longitud});
            while ( (pr_default.getStatus(0) != 101) )
            {
               A38ObstaculoLongitud = P002L2_A38ObstaculoLongitud[0];
               A39ObstaculoLatitud = P002L2_A39ObstaculoLatitud[0];
               A37ObstaculoTipo = P002L2_A37ObstaculoTipo[0];
               A35ObstaculoID = (Guid)((Guid)(P002L2_A35ObstaculoID[0]));
               AV29GXLvl72 = 1;
               AV14TipoObstaculo = A37ObstaculoTipo;
               pr_default.readNext(0);
            }
            pr_default.close(0);
            if ( AV29GXLvl72 == 0 )
            {
               AV14TipoObstaculo = 0;
            }
         }
         if ( AV16Emergencia == 1 )
         {
            AV24SMTP.Secure = 1;
            AV24SMTP.Authentication = 1;
            AV24SMTP.Host = "smtp.gmail.com";
            AV24SMTP.Port = 587;
            AV24SMTP.UserName = "rmatheu@sifco.org";
            AV24SMTP.Password = "1205sifco";
            AV24SMTP.Sender.Address = "rmatheu@sifco.org";
            AV24SMTP.Sender.Name = "Bast�n Inteligente";
            AV24SMTP.Login();
            if ( AV24SMTP.ErrCode == 0 )
            {
               /* Using cursor P002L3 */
               pr_default.execute(1);
               while ( (pr_default.getStatus(1) != 101) )
               {
                  A1PersonaID = (Guid)((Guid)(P002L3_A1PersonaID[0]));
                  A11PersonaEMail = P002L3_A11PersonaEMail[0];
                  n11PersonaEMail = P002L3_n11PersonaEMail[0];
                  A2PersonaPNombre = P002L3_A2PersonaPNombre[0];
                  if ( A1PersonaID == StringUtil.StrToGuid( StringUtil.Trim( AV18IDCoach)) )
                  {
                     AV25Mail = new GeneXus.Mail.GXMailMessage();
                     AV25Mail.To.New(A2PersonaPNombre, A11PersonaEMail) ;
                     AV25Mail.Subject = "Emergencia";
                     AV25Mail.HTMLText = "Paciente: "+StringUtil.Trim( AV19IDPaciente)+" Presenta emergencia en: "+AV13Latitud+" , "+AV12Longitud;
                     AV24SMTP.Send(AV25Mail);
                     if ( AV24SMTP.ErrCode == 0 )
                     {
                     }
                     else
                     {
                     }
                     AV24SMTP.Logout();
                  }
                  pr_default.readNext(1);
               }
               pr_default.close(1);
            }
            else
            {
               AV13Latitud = AV24SMTP.ErrDescription;
            }
            AV26EmergenciaBitacora = new SdtEmergenciaBitacora(context);
            AV26EmergenciaBitacora.gxTpr_Emergenciafecha = DateTimeUtil.ServerNow( context, pr_default);
            AV26EmergenciaBitacora.gxTpr_Emergenciaestado = 1;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV19IDPaciente)) )
            {
               AV26EmergenciaBitacora.gxTpr_Emergenciapaciente = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV19IDPaciente)));
            }
            else
            {
            }
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18IDCoach)) )
            {
               AV26EmergenciaBitacora.gxTpr_Emergenciacoach = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV18IDCoach)));
            }
            else
            {
               AV26EmergenciaBitacora.gxTv_SdtEmergenciaBitacora_Emergenciacoach_SetNull();
            }
            AV26EmergenciaBitacora.gxTpr_Emergencialongitud = AV12Longitud;
            AV26EmergenciaBitacora.gxTpr_Emergencialatitud = AV13Latitud;
            AV26EmergenciaBitacora.gxTpr_Emergenciamensaje = "Paciente: "+StringUtil.Trim( AV19IDPaciente)+" Presenta emergencia en: "+AV13Latitud+" , "+AV12Longitud;
            AV26EmergenciaBitacora.Save();
         }
         else
         {
         }
      }

      protected void S131( )
      {
         /* 'GUARDAR' Routine */
         AV9Recorrido.Save();
         if ( AV9Recorrido.Success() )
         {
            pr_gam.commit( "registrarrecorrido");
            pr_default.commit( "registrarrecorrido");
         }
         else
         {
            pr_gam.rollback( "registrarrecorrido");
            pr_default.rollback( "registrarrecorrido");
            AV32GXV2 = 1;
            AV31GXV1 = AV9Recorrido.GetMessages();
            while ( AV32GXV2 <= AV31GXV1.Count )
            {
               AV22Message = ((SdtMessages_Message)AV31GXV1.Item(AV32GXV2));
               GX_msglist.addItem(AV22Message.gxTpr_Description);
               AV32GXV2 = (int)(AV32GXV2+1);
            }
         }
      }

      public override void cleanup( )
      {
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV8GUID = (Guid)(Guid.Empty);
         AV9Recorrido = new SdtRecorrido(context);
         AV21Detalle = new SdtRecorrido_Detalle(context);
         AV23Obstaculo = new SdtObstaculo(context);
         scmdbuf = "";
         P002L2_A38ObstaculoLongitud = new String[] {""} ;
         P002L2_A39ObstaculoLatitud = new String[] {""} ;
         P002L2_A37ObstaculoTipo = new short[1] ;
         P002L2_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         A38ObstaculoLongitud = "";
         A39ObstaculoLatitud = "";
         A35ObstaculoID = (Guid)(Guid.Empty);
         AV24SMTP = new GeneXus.Mail.GXSMTPSession(context.GetPhysicalPath());
         P002L3_A1PersonaID = new Guid[] {Guid.Empty} ;
         P002L3_A11PersonaEMail = new String[] {""} ;
         P002L3_n11PersonaEMail = new bool[] {false} ;
         P002L3_A2PersonaPNombre = new String[] {""} ;
         A1PersonaID = (Guid)(Guid.Empty);
         A11PersonaEMail = "";
         A2PersonaPNombre = "";
         AV25Mail = new GeneXus.Mail.GXMailMessage();
         AV26EmergenciaBitacora = new SdtEmergenciaBitacora(context);
         AV31GXV1 = new GXBaseCollection<SdtMessages_Message>( context, "Message", "GeneXus");
         AV22Message = new SdtMessages_Message(context);
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.aregistrarrecorrido__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.aregistrarrecorrido__default(),
            new Object[][] {
                new Object[] {
               P002L2_A38ObstaculoLongitud, P002L2_A39ObstaculoLatitud, P002L2_A37ObstaculoTipo, P002L2_A35ObstaculoID
               }
               , new Object[] {
               P002L3_A1PersonaID, P002L3_A11PersonaEMail, P002L3_n11PersonaEMail, P002L3_A2PersonaPNombre
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private short AV10TipoSenal ;
      private short AV14TipoObstaculo ;
      private short AV15Incidente ;
      private short AV16Emergencia ;
      private short AV29GXLvl72 ;
      private short A37ObstaculoTipo ;
      private int AV32GXV2 ;
      private String AV11IDSesion ;
      private String AV18IDCoach ;
      private String AV19IDPaciente ;
      private String scmdbuf ;
      private bool returnInSub ;
      private bool n11PersonaEMail ;
      private String AV12Longitud ;
      private String AV13Latitud ;
      private String A38ObstaculoLongitud ;
      private String A39ObstaculoLatitud ;
      private String AV17IMEI ;
      private String A11PersonaEMail ;
      private String A2PersonaPNombre ;
      private Guid AV8GUID ;
      private Guid A35ObstaculoID ;
      private Guid A1PersonaID ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_IDSesion ;
      private String aP2_Longitud ;
      private String aP3_Latitud ;
      private short aP4_TipoObstaculo ;
      private String aP7_IMEI ;
      private String aP8_IDCoach ;
      private String aP9_IDPaciente ;
      private IDataStoreProvider pr_default ;
      private String[] P002L2_A38ObstaculoLongitud ;
      private String[] P002L2_A39ObstaculoLatitud ;
      private short[] P002L2_A37ObstaculoTipo ;
      private Guid[] P002L2_A35ObstaculoID ;
      private Guid[] P002L3_A1PersonaID ;
      private String[] P002L3_A11PersonaEMail ;
      private bool[] P002L3_n11PersonaEMail ;
      private String[] P002L3_A2PersonaPNombre ;
      private IDataStoreProvider pr_gam ;
      private GeneXus.Mail.GXMailMessage AV25Mail ;
      private GeneXus.Mail.GXSMTPSession AV24SMTP ;
      private GXBaseCollection<SdtMessages_Message> AV31GXV1 ;
      private SdtRecorrido AV9Recorrido ;
      private SdtRecorrido_Detalle AV21Detalle ;
      private SdtMessages_Message AV22Message ;
      private SdtObstaculo AV23Obstaculo ;
      private SdtEmergenciaBitacora AV26EmergenciaBitacora ;
   }

   public class aregistrarrecorrido__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class aregistrarrecorrido__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmP002L2 ;
        prmP002L2 = new Object[] {
        new Object[] {"@AV13Latitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@AV12Longitud",SqlDbType.VarChar,2097152,0}
        } ;
        Object[] prmP002L3 ;
        prmP002L3 = new Object[] {
        } ;
        def= new CursorDef[] {
            new CursorDef("P002L2", "SELECT [ObstaculoLongitud], [ObstaculoLatitud], [ObstaculoTipo], [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) WHERE (@AV13Latitud = [ObstaculoLatitud]) AND (@AV12Longitud = [ObstaculoLongitud]) ORDER BY [ObstaculoID] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002L2,100,0,false,false )
           ,new CursorDef("P002L3", "SELECT [PersonaID], [PersonaEMail], [PersonaPNombre] FROM [Persona] WITH (NOLOCK) ORDER BY [PersonaID] ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002L3,100,0,false,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((String[]) buf[0])[0] = rslt.getLongVarchar(1) ;
              ((String[]) buf[1])[0] = rslt.getLongVarchar(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((Guid[]) buf[3])[0] = rslt.getGuid(4) ;
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((String[]) buf[1])[0] = rslt.getVarchar(2) ;
              ((bool[]) buf[2])[0] = rslt.wasNull(2);
              ((String[]) buf[3])[0] = rslt.getVarchar(3) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (String)parms[0]);
              stmt.SetParameter(2, (String)parms[1]);
              return;
     }
  }

}

[ServiceContract(Namespace = "GeneXus.Programs.registrarrecorrido_services")]
[ServiceBehavior(ConcurrencyMode = ConcurrencyMode.Multiple)]
[AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
public class registrarrecorrido_services : GxRestService
{
   [OperationContract]
   [WebInvoke(Method =  "POST" ,
   	BodyStyle =  WebMessageBodyStyle.Wrapped  ,
   	ResponseFormat = WebMessageFormat.Json,
   	UriTemplate = "/")]
   public void execute( ref String IDSesion ,
                        short TipoSenal ,
                        ref String Longitud ,
                        ref String Latitud ,
                        ref short TipoObstaculo ,
                        short Incidente ,
                        short Emergencia ,
                        ref String IMEI ,
                        ref String IDCoach ,
                        ref String IDPaciente )
   {
      try
      {
         if ( ! ProcessHeaders("registrarrecorrido") )
         {
            return  ;
         }
         aregistrarrecorrido worker = new aregistrarrecorrido(context) ;
         worker.IsMain = RunAsMain ;
         worker.execute(ref IDSesion,TipoSenal,ref Longitud,ref Latitud,ref TipoObstaculo,Incidente,Emergencia,ref IMEI,ref IDCoach,ref IDPaciente );
         worker.cleanup( );
      }
      catch ( Exception e )
      {
         WebException(e);
      }
      finally
      {
         Cleanup();
      }
   }

}

}
