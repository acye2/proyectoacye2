/*
               File: WPRecorridos
        Description: Recorridos
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:21:3.48
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using GeneXus.Http.Server;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class wprecorridos : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      public wprecorridos( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public wprecorridos( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_PacienteID )
      {
         this.AV24PacienteID = aP0_PacienteID;
         executePrivate();
         aP0_PacienteID=this.AV24PacienteID;
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbavGridsettingsrowsperpage_grid = new GXCombobox();
      }

      protected void INITWEB( )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         if ( nGotPars == 0 )
         {
            entryPointCalled = false;
            gxfirstwebparm = GetNextPar( );
            gxfirstwebparm_bkp = gxfirstwebparm;
            gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
            {
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               dyncall( GetNextPar( )) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
            {
               setAjaxEventMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
            {
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = GetNextPar( );
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxNewRow_"+"Grid") == 0 )
            {
               nRC_GXsfl_54 = (short)(NumberUtil.Val( GetNextPar( ), "."));
               nGXsfl_54_idx = (short)(NumberUtil.Val( GetNextPar( ), "."));
               sGXsfl_54_idx = GetNextPar( );
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxnrGrid_newrow( ) ;
               return  ;
            }
            else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxGridRefresh_"+"Grid") == 0 )
            {
               AV42Pgmname = GetNextPar( );
               AV29CurrentPage_Grid = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV34RowsPerPage_Grid = (short)(NumberUtil.Val( GetNextPar( ), "."));
               AV33HasNextPage_Grid = (bool)(BooleanUtil.Val(GetNextPar( )));
               AV24PacienteID = GetNextPar( );
               A21RecorridoDetallePaciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               n21RecorridoDetallePaciente = false;
               A23RecorridoID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
               A24RecorridoFecha = context.localUtil.ParseDTimeParm( GetNextPar( ));
               setAjaxCallMode();
               if ( ! IsValidAjaxCall( true) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxgrGrid_refresh( AV42Pgmname, AV29CurrentPage_Grid, AV34RowsPerPage_Grid, AV33HasNextPage_Grid, AV24PacienteID, A21RecorridoDetallePaciente, A23RecorridoID, A24RecorridoFecha) ;
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               return  ;
            }
            else
            {
               if ( ! IsValidAjaxCall( false) )
               {
                  GxWebError = 1;
                  return  ;
               }
               gxfirstwebparm = gxfirstwebparm_bkp;
            }
            if ( ! entryPointCalled && ! ( isAjaxCallMode( ) || isFullAjaxMode( ) ) )
            {
               AV24PacienteID = gxfirstwebparm;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24PacienteID", AV24PacienteID);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPACIENTEID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV24PacienteID, "")), context));
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "wprecorridos_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      public override short ExecuteStartEvent( )
      {
         PA3A2( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            START3A2( ) ;
         }
         return gxajaxcallmode ;
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         if ( nGXWrapped != 1 )
         {
            MasterPageObj.master_styles();
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811181721354", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         if ( nGXWrapped == 0 )
         {
            bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         }
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("wprecorridos.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV24PacienteID))+"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "nRC_GXsfl_54", StringUtil.LTrim( StringUtil.NToC( (decimal)(nRC_GXsfl_54), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vCURRENTPAGE_GRID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV29CurrentPage_Grid), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vROWSPERPAGE_GRID", StringUtil.LTrim( StringUtil.NToC( (decimal)(AV34RowsPerPage_Grid), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vPGMNAME", StringUtil.RTrim( AV42Pgmname));
         GxWebStd.gx_boolean_hidden_field( context, "vHASNEXTPAGE_GRID", AV33HasNextPage_Grid);
         GxWebStd.gx_hidden_field( context, "vPACIENTEID", AV24PacienteID);
         GxWebStd.gx_hidden_field( context, "gxhash_vPACIENTEID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV24PacienteID, "")), context));
         GxWebStd.gx_hidden_field( context, "RECORRIDODETALLEPACIENTE", A21RecorridoDetallePaciente.ToString());
         GxWebStd.gx_hidden_field( context, "RECORRIDOID", A23RecorridoID.ToString());
         GxWebStd.gx_hidden_field( context, "RECORRIDOFECHA", context.localUtil.TToC( A24RecorridoFecha, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "GRIDSETTINGS_CONTENTOUTERTABLEGRID_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(divGridsettings_contentoutertablegrid_Visible), 5, 0, ".", "")));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
         if ( ! ( WebComp_Wcrecorrido == null ) )
         {
            WebComp_Wcrecorrido.componentjscripts();
         }
      }

      public override void RenderHtmlContent( )
      {
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         if ( ( gxajaxcallmode == 0 ) && ( GxWebError == 0 ) )
         {
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            WE3A2( ) ;
            context.WriteHtmlText( "</div>") ;
         }
      }

      public override void DispatchEvents( )
      {
         EVT3A2( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return false ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("wprecorridos.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV24PacienteID)) ;
      }

      public override String GetPgmname( )
      {
         return "WPRecorridos" ;
      }

      public override String GetPgmdesc( )
      {
         return "Recorridos" ;
      }

      protected void WB3A0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( nGXWrapped == 1 )
            {
               RenderHtmlHeaders( ) ;
               RenderHtmlOpenForm( ) ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_MainContentTable", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divContenttable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divColumns1_maincolumnstable_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divColumncontainertable_column_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_globalgridtable_grid_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_RoundedBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_table3_grid_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_TopBorder", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divLayoutdefined_tableactionscontainer_grid_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_FullWidth", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table1_27_3A2( true) ;
         }
         else
         {
            wb_table1_27_3A2( false) ;
         }
         return  ;
      }

      protected void wb_table1_27_3A2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaingrid_responsivetable_grid_Internalname, 1, 0, "px", 0, "px", "Section_Grid", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /*  Grid Control  */
            GridContainer.SetWrapped(nGXWrapped);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"DivS\" data-gxgridid=\"54\">") ;
               sStyleString = "";
               GxWebStd.gx_table_start( context, subGrid_Internalname, subGrid_Internalname, "", "Grid_AllowSelection Grid_WorkWith", 0, "", "", 1, 2, sStyleString, "", 0);
               /* Subfile titles */
               context.WriteHtmlText( "<tr") ;
               context.WriteHtmlTextNl( ">") ;
               if ( subGrid_Backcolorstyle == 0 )
               {
                  subGrid_Titlebackstyle = 0;
                  if ( StringUtil.Len( subGrid_Class) > 0 )
                  {
                     subGrid_Linesclass = subGrid_Class+"Title";
                  }
               }
               else
               {
                  subGrid_Titlebackstyle = 1;
                  if ( subGrid_Backcolorstyle == 1 )
                  {
                     subGrid_Titlebackcolor = subGrid_Allbackcolor;
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"UniformTitle";
                     }
                  }
                  else
                  {
                     if ( StringUtil.Len( subGrid_Class) > 0 )
                     {
                        subGrid_Linesclass = subGrid_Class+"Title";
                     }
                  }
               }
               context.WriteHtmlText( "<th align=\""+""+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "ID") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlText( "<th align=\""+"right"+"\" "+" nowrap=\"nowrap\" "+" class=\""+"Attribute_Grid"+"\" "+" style=\""+""+""+"\" "+">") ;
               context.SendWebValue( "Fecha") ;
               context.WriteHtmlTextNl( "</th>") ;
               context.WriteHtmlTextNl( "</tr>") ;
               GridContainer.AddObjectProperty("GridName", "Grid");
            }
            else
            {
               GridContainer.AddObjectProperty("GridName", "Grid");
               GridContainer.AddObjectProperty("Class", "Grid_AllowSelection Grid_WorkWith");
               GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
               GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("CmpContext", "");
               GridContainer.AddObjectProperty("InMasterPage", "false");
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", AV36RecorridoID.ToString());
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRecorridoid_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridColumn = GXWebColumn.GetNew(isAjaxCallMode( ));
               GridColumn.AddObjectProperty("Value", context.localUtil.TToC( AV37RecorridoFecha, 10, 8, 0, 3, "/", ":", " "));
               GridColumn.AddObjectProperty("Enabled", StringUtil.LTrim( StringUtil.NToC( (decimal)(edtavRecorridofecha_Enabled), 5, 0, ".", "")));
               GridContainer.AddColumnProperties(GridColumn);
               GridContainer.AddObjectProperty("Allowselection", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowselection), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Selectioncolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Selectioncolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowhover", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowhovering), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Hovercolor", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Hoveringcolor), 9, 0, ".", "")));
               GridContainer.AddObjectProperty("Allowcollapsing", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Allowcollapsing), 1, 0, ".", "")));
               GridContainer.AddObjectProperty("Collapsed", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Collapsed), 1, 0, ".", "")));
            }
         }
         if ( wbEnd == 54 )
         {
            wbEnd = 0;
            nRC_GXsfl_54 = (short)(nGXsfl_54_idx-1);
            if ( GridContainer.GetWrapped() == 1 )
            {
               context.WriteHtmlText( "</table>") ;
               context.WriteHtmlText( "</div>") ;
            }
            else
            {
               sStyleString = "";
               context.WriteHtmlText( "<div id=\""+"GridContainer"+"Div\" "+sStyleString+">"+"</div>") ;
               context.httpAjaxContext.ajax_rsp_assign_grid("_"+"Grid", GridContainer);
               if ( ! context.isAjaxRequest( ) && ! context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData", GridContainer.ToJavascriptSource());
               }
               if ( context.isAjaxRequest( ) || context.isSpaRequest( ) )
               {
                  GxWebStd.gx_hidden_field( context, "GridContainerData"+"V", GridContainer.GridValuesHidden());
               }
               else
               {
                  context.WriteHtmlText( "<input autocomplete=\"off\" type=\"hidden\" "+"name=\""+"GridContainerData"+"V"+"\" value='"+GridContainer.GridValuesHidden()+"'/>") ;
               }
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table2_59_3A2( true) ;
         }
         else
         {
            wb_table2_59_3A2( false) ;
         }
         return  ;
      }

      protected void wb_table2_59_3A2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table3_65_3A2( true) ;
         }
         else
         {
            wb_table3_65_3A2( false) ;
         }
         return  ;
      }

      protected void wb_table3_65_3A2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-6", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divColumncontainertable_column1_Internalname, 1, 0, "px", 0, "px", "Table", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            if ( ! isFullAjaxMode( ) )
            {
               /* WebComponent */
               GxWebStd.gx_hidden_field( context, "W0087"+"", StringUtil.RTrim( WebComp_Wcrecorrido_Component));
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gxwebcomponent");
               context.WriteHtmlText( " id=\""+"gxHTMLWrpW0087"+""+"\""+"") ;
               context.WriteHtmlText( ">") ;
               if ( StringUtil.Len( WebComp_Wcrecorrido_Component) != 0 )
               {
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcrecorrido), StringUtil.Lower( WebComp_Wcrecorrido_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0087"+"");
                  }
                  WebComp_Wcrecorrido.componentdraw();
                  if ( StringUtil.StrCmp(StringUtil.Lower( OldWcrecorrido), StringUtil.Lower( WebComp_Wcrecorrido_Component)) != 0 )
                  {
                     context.httpAjaxContext.ajax_rspEndCmp();
                  }
               }
               context.WriteHtmlText( "</div>") ;
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"K2BCONTROLBEAUTIFY1Container"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START3A2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Recorridos", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP3A0( ) ;
      }

      protected void WS3A2( )
      {
         START3A2( ) ;
         EVT3A2( ) ;
      }

      protected void EVT3A2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
                  {
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'SAVEGRIDSETTINGS(GRID)'") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              /* Execute user event: 'SaveGridSettings(Grid)' */
                              E113A2 ();
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              context.wbHandled = 1;
                              dynload_actions( ) ;
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                           sEvtType = StringUtil.Right( sEvt, 4);
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-4));
                           if ( ( StringUtil.StrCmp(StringUtil.Left( sEvt, 7), "REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "START") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 12), "GRID.REFRESH") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 9), "GRID.LOAD") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 19), "GRID.ONLINEACTIVATE") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 5), "ENTER") == 0 ) || ( StringUtil.StrCmp(StringUtil.Left( sEvt, 6), "CANCEL") == 0 ) )
                           {
                              nGXsfl_54_idx = (short)(NumberUtil.Val( sEvtType, "."));
                              sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
                              SubsflControlProps_542( ) ;
                              AV36RecorridoID = (Guid)(StringUtil.StrToGuid( cgiGet( edtavRecorridoid_Internalname)));
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRecorridoid_Internalname, AV36RecorridoID.ToString());
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRECORRIDOID"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, AV36RecorridoID, context));
                              AV37RecorridoFecha = context.localUtil.CToT( cgiGet( edtavRecorridofecha_Internalname), 0);
                              context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRecorridofecha_Internalname, context.localUtil.TToC( AV37RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
                              sEvtType = StringUtil.Right( sEvt, 1);
                              if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                              {
                                 sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                                 if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Refresh */
                                    E123A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E133A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.REFRESH") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E143A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.LOAD") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E153A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "GRID.ONLINEACTIVATE") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                    E163A2 ();
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                    /* No code required for Cancel button. It is implemented as the Reset button. */
                                 }
                                 else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                                 {
                                    context.wbHandled = 1;
                                    dynload_actions( ) ;
                                 }
                              }
                              else
                              {
                              }
                           }
                        }
                     }
                     else if ( StringUtil.StrCmp(sEvtType, "W") == 0 )
                     {
                        sEvtType = StringUtil.Left( sEvt, 4);
                        sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-4));
                        nCmpId = (short)(NumberUtil.Val( sEvtType, "."));
                        if ( nCmpId == 87 )
                        {
                           OldWcrecorrido = cgiGet( "W0087");
                           if ( ( StringUtil.Len( OldWcrecorrido) == 0 ) || ( StringUtil.StrCmp(OldWcrecorrido, WebComp_Wcrecorrido_Component) != 0 ) )
                           {
                              WebComp_Wcrecorrido = getWebComponent(GetType(), "GeneXus.Programs", OldWcrecorrido, new Object[] {context} );
                              WebComp_Wcrecorrido.ComponentInit();
                              WebComp_Wcrecorrido.Name = "OldWcrecorrido";
                              WebComp_Wcrecorrido_Component = OldWcrecorrido;
                           }
                           if ( StringUtil.Len( WebComp_Wcrecorrido_Component) != 0 )
                           {
                              WebComp_Wcrecorrido.componentprocess("W0087", "", sEvt);
                           }
                           WebComp_Wcrecorrido_Component = OldWcrecorrido;
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE3A2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               if ( nGXWrapped == 1 )
               {
                  RenderHtmlCloseForm( ) ;
               }
            }
         }
      }

      protected void PA3A2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            cmbavGridsettingsrowsperpage_grid.Name = "vGRIDSETTINGSROWSPERPAGE_GRID";
            cmbavGridsettingsrowsperpage_grid.WebTags = "";
            cmbavGridsettingsrowsperpage_grid.addItem("10", "10", 0);
            cmbavGridsettingsrowsperpage_grid.addItem("20", "20", 0);
            cmbavGridsettingsrowsperpage_grid.addItem("50", "50", 0);
            cmbavGridsettingsrowsperpage_grid.addItem("100", "100", 0);
            cmbavGridsettingsrowsperpage_grid.addItem("200", "200", 0);
            if ( cmbavGridsettingsrowsperpage_grid.ItemCount > 0 )
            {
               AV35GridSettingsRowsPerPage_Grid = (short)(NumberUtil.Val( cmbavGridsettingsrowsperpage_grid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0))), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35GridSettingsRowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0)));
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = cmbavGridsettingsrowsperpage_grid_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void gxnrGrid_newrow( )
      {
         GxWebStd.set_html_headers( context, 0, "", "");
         SubsflControlProps_542( ) ;
         while ( nGXsfl_54_idx <= nRC_GXsfl_54 )
         {
            sendrow_542( ) ;
            nGXsfl_54_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_54_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_54_idx+1));
            sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
            SubsflControlProps_542( ) ;
         }
         context.GX_webresponse.AddString(context.httpAjaxContext.getJSONContainerResponse( GridContainer));
         /* End function gxnrGrid_newrow */
      }

      protected void gxgrGrid_refresh( String AV42Pgmname ,
                                       short AV29CurrentPage_Grid ,
                                       short AV34RowsPerPage_Grid ,
                                       bool AV33HasNextPage_Grid ,
                                       String AV24PacienteID ,
                                       Guid A21RecorridoDetallePaciente ,
                                       Guid A23RecorridoID ,
                                       DateTime A24RecorridoFecha )
      {
         initialize_formulas( ) ;
         GxWebStd.set_html_headers( context, 0, "", "");
         GRID_nCurrentRecord = 0;
         RF3A2( ) ;
         /* End function gxgrGrid_refresh */
      }

      protected void send_integrity_hashes( )
      {
         GxWebStd.gx_hidden_field( context, "gxhash_vRECORRIDOID", GetSecureSignedToken( "", AV36RecorridoID, context));
         GxWebStd.gx_hidden_field( context, "vRECORRIDOID", AV36RecorridoID.ToString());
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbavGridsettingsrowsperpage_grid.ItemCount > 0 )
         {
            AV35GridSettingsRowsPerPage_Grid = (short)(NumberUtil.Val( cmbavGridsettingsrowsperpage_grid.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35GridSettingsRowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbavGridsettingsrowsperpage_grid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpage_grid_Internalname, "Values", cmbavGridsettingsrowsperpage_grid.ToJavascriptSource(), true);
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         /* Execute user event: Refresh */
         E123A2 ();
         RF3A2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV42Pgmname = "WPRecorridos";
         context.Gx_err = 0;
         edtavRecorridoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRecorridoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRecorridoid_Enabled), 5, 0)), !bGXsfl_54_Refreshing);
         edtavRecorridofecha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRecorridofecha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRecorridofecha_Enabled), 5, 0)), !bGXsfl_54_Refreshing);
      }

      protected void RF3A2( )
      {
         initialize_formulas( ) ;
         if ( isAjaxCallMode( ) )
         {
            GridContainer.ClearRows();
         }
         wbStart = 54;
         E143A2 ();
         nGXsfl_54_idx = 1;
         sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
         SubsflControlProps_542( ) ;
         bGXsfl_54_Refreshing = true;
         GridContainer.AddObjectProperty("GridName", "Grid");
         GridContainer.AddObjectProperty("CmpContext", "");
         GridContainer.AddObjectProperty("InMasterPage", "false");
         GridContainer.AddObjectProperty("Class", "Grid_AllowSelection Grid_WorkWith");
         GridContainer.AddObjectProperty("Cellpadding", StringUtil.LTrim( StringUtil.NToC( (decimal)(1), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Cellspacing", StringUtil.LTrim( StringUtil.NToC( (decimal)(2), 4, 0, ".", "")));
         GridContainer.AddObjectProperty("Backcolorstyle", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Backcolorstyle), 1, 0, ".", "")));
         GridContainer.AddObjectProperty("Sortable", StringUtil.LTrim( StringUtil.NToC( (decimal)(subGrid_Sortable), 1, 0, ".", "")));
         GridContainer.PageSize = subGrid_Recordsperpage( );
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            if ( 1 != 0 )
            {
               if ( StringUtil.Len( WebComp_Wcrecorrido_Component) != 0 )
               {
                  WebComp_Wcrecorrido.componentstart();
               }
            }
         }
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            SubsflControlProps_542( ) ;
            E153A2 ();
            wbEnd = 54;
            WB3A0( ) ;
         }
         bGXsfl_54_Refreshing = true;
      }

      protected void send_integrity_lvl_hashes3A2( )
      {
         GxWebStd.gx_hidden_field( context, "vPACIENTEID", AV24PacienteID);
         GxWebStd.gx_hidden_field( context, "gxhash_vPACIENTEID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV24PacienteID, "")), context));
         GxWebStd.gx_hidden_field( context, "gxhash_vRECORRIDOID"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, AV36RecorridoID, context));
      }

      protected int subGrid_Pagecount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordcount( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Recordsperpage( )
      {
         return (int)(-1) ;
      }

      protected int subGrid_Currentpage( )
      {
         return (int)(-1) ;
      }

      protected void STRUP3A0( )
      {
         /* Before Start, stand alone formulas. */
         AV42Pgmname = "WPRecorridos";
         context.Gx_err = 0;
         edtavRecorridoid_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRecorridoid_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRecorridoid_Enabled), 5, 0)), !bGXsfl_54_Refreshing);
         edtavRecorridofecha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtavRecorridofecha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtavRecorridofecha_Enabled), 5, 0)), !bGXsfl_54_Refreshing);
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E133A2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            cmbavGridsettingsrowsperpage_grid.Name = cmbavGridsettingsrowsperpage_grid_Internalname;
            cmbavGridsettingsrowsperpage_grid.CurrentValue = cgiGet( cmbavGridsettingsrowsperpage_grid_Internalname);
            AV35GridSettingsRowsPerPage_Grid = (short)(NumberUtil.Val( cgiGet( cmbavGridsettingsrowsperpage_grid_Internalname), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35GridSettingsRowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0)));
            /* Read saved values. */
            nRC_GXsfl_54 = (short)(context.localUtil.CToN( cgiGet( "nRC_GXsfl_54"), ",", "."));
            AV29CurrentPage_Grid = (short)(context.localUtil.CToN( cgiGet( "vCURRENTPAGE_GRID"), ",", "."));
            AV33HasNextPage_Grid = StringUtil.StrToBool( cgiGet( "vHASNEXTPAGE_GRID"));
            AV34RowsPerPage_Grid = (short)(context.localUtil.CToN( cgiGet( "vROWSPERPAGE_GRID"), ",", "."));
            /* Read subfile selected row values. */
            nGXsfl_54_idx = (short)(context.localUtil.CToN( cgiGet( subGrid_Internalname+"_ROW"), ",", "."));
            sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
            SubsflControlProps_542( ) ;
            if ( nGXsfl_54_idx > 0 )
            {
               AV36RecorridoID = (Guid)(StringUtil.StrToGuid( cgiGet( edtavRecorridoid_Internalname)));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRecorridoid_Internalname, AV36RecorridoID.ToString());
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRECORRIDOID"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, AV36RecorridoID, context));
               AV37RecorridoFecha = context.localUtil.CToT( cgiGet( edtavRecorridofecha_Internalname), 0);
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRecorridofecha_Internalname, context.localUtil.TToC( AV37RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
            }
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void S122( )
      {
         /* 'U_REFRESHPAGE' Routine */
      }

      protected void S112( )
      {
         /* 'U_STARTPAGE' Routine */
      }

      protected void S132( )
      {
         /* 'U_OPENPAGE' Routine */
      }

      protected void E123A2( )
      {
         if ( gx_refresh_fired )
         {
            return  ;
         }
         gx_refresh_fired = true;
         /* Refresh Routine */
         /* Execute user subroutine: 'U_STARTPAGE' */
         S112 ();
         if (returnInSub) return;
         subGrid_Backcolorstyle = 3;
         divGridsettings_contentoutertablegrid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divGridsettings_contentoutertablegrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divGridsettings_contentoutertablegrid_Visible), 5, 0)), true);
         if ( (0==AV29CurrentPage_Grid) )
         {
            AV29CurrentPage_Grid = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29CurrentPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29CurrentPage_Grid), 4, 0)));
         }
         AV30Reload_Grid = true;
         /* Execute user subroutine: 'U_REFRESHPAGE' */
         S122 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E133A2 ();
         if (returnInSub) return;
      }

      protected void E133A2( )
      {
         /* Start Routine */
         AV34RowsPerPage_Grid = 20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34RowsPerPage_Grid), 4, 0)));
         AV35GridSettingsRowsPerPage_Grid = 20;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35GridSettingsRowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0)));
         /* Execute user subroutine: 'U_OPENPAGE' */
         S132 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'LOADGRIDSTATE(GRID)' */
         S142 ();
         if (returnInSub) return;
      }

      protected void S142( )
      {
         /* 'LOADGRIDSTATE(GRID)' Routine */
         AV26GridStateKey = "Grid";
         new k2bloadgridstate(context ).execute(  AV42Pgmname,  AV26GridStateKey, out  AV27GridState) ;
         if ( AV27GridState.gxTpr_Currentpage > 0 )
         {
            AV29CurrentPage_Grid = AV27GridState.gxTpr_Currentpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29CurrentPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29CurrentPage_Grid), 4, 0)));
         }
         if ( AV27GridState.gxTpr_Rowsperpage > 0 )
         {
            AV34RowsPerPage_Grid = AV27GridState.gxTpr_Rowsperpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34RowsPerPage_Grid), 4, 0)));
            AV35GridSettingsRowsPerPage_Grid = AV27GridState.gxTpr_Rowsperpage;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV35GridSettingsRowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0)));
         }
      }

      protected void S152( )
      {
         /* 'SAVEGRIDSTATE(GRID)' Routine */
         AV26GridStateKey = "Grid";
         new k2bloadgridstate(context ).execute(  AV42Pgmname,  AV26GridStateKey, out  AV27GridState) ;
         AV27GridState.gxTpr_Currentpage = AV29CurrentPage_Grid;
         AV27GridState.gxTpr_Rowsperpage = AV34RowsPerPage_Grid;
         AV27GridState.gxTpr_Filtervalues.Clear();
         new k2bsavegridstate(context ).execute(  AV42Pgmname,  AV26GridStateKey,  AV27GridState) ;
      }

      protected void E113A2( )
      {
         /* 'SaveGridSettings(Grid)' Routine */
         if ( AV34RowsPerPage_Grid != AV35GridSettingsRowsPerPage_Grid )
         {
            AV34RowsPerPage_Grid = AV35GridSettingsRowsPerPage_Grid;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV34RowsPerPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV34RowsPerPage_Grid), 4, 0)));
            AV29CurrentPage_Grid = 1;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV29CurrentPage_Grid", StringUtil.LTrim( StringUtil.Str( (decimal)(AV29CurrentPage_Grid), 4, 0)));
         }
         gxgrGrid_refresh( AV42Pgmname, AV29CurrentPage_Grid, AV34RowsPerPage_Grid, AV33HasNextPage_Grid, AV24PacienteID, A21RecorridoDetallePaciente, A23RecorridoID, A24RecorridoFecha) ;
         /* Execute user subroutine: 'SAVEGRIDSTATE(GRID)' */
         S152 ();
         if (returnInSub) return;
         divGridsettings_contentoutertablegrid_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, divGridsettings_contentoutertablegrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divGridsettings_contentoutertablegrid_Visible), 5, 0)), true);
         /*  Sending Event outputs  */
      }

      protected void E143A2( )
      {
         /* Grid_Refresh Routine */
         /* Execute user subroutine: 'SAVEGRIDSTATE(GRID)' */
         S152 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'UPDATEPAGINGCONTROLS(GRID)' */
         S162 ();
         if (returnInSub) return;
         subGrid_Backcolorstyle = 3;
         /* Execute user subroutine: 'U_GRIDREFRESH(GRID)' */
         S172 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S182( )
      {
         /* 'U_LOADROWVARS(GRID)' Routine */
         AV25PacienteGUID = (Guid)(StringUtil.StrToGuid( StringUtil.Trim( AV24PacienteID)));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV25PacienteGUID", AV25PacienteGUID.ToString());
         /* Using cursor H003A2 */
         pr_default.execute(0, new Object[] {AV25PacienteGUID});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A21RecorridoDetallePaciente = (Guid)((Guid)(H003A2_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = H003A2_n21RecorridoDetallePaciente[0];
            A23RecorridoID = (Guid)((Guid)(H003A2_A23RecorridoID[0]));
            A24RecorridoFecha = H003A2_A24RecorridoFecha[0];
            A24RecorridoFecha = H003A2_A24RecorridoFecha[0];
            AV36RecorridoID = (Guid)(A23RecorridoID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRecorridoid_Internalname, AV36RecorridoID.ToString());
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vRECORRIDOID"+"_"+sGXsfl_54_idx, GetSecureSignedToken( sGXsfl_54_idx, AV36RecorridoID, context));
            AV37RecorridoFecha = A24RecorridoFecha;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, edtavRecorridofecha_Internalname, context.localUtil.TToC( AV37RecorridoFecha, 8, 5, 0, 3, "/", ":", " "));
            sendrow_542( ) ;
            if ( isFullAjaxMode( ) && ! bGXsfl_54_Refreshing )
            {
               context.DoAjaxLoad(54, GridRow);
            }
            pr_default.readNext(0);
         }
         pr_default.close(0);
         AV38Exit_Grid = true;
      }

      protected void S162( )
      {
         /* 'UPDATEPAGINGCONTROLS(GRID)' Routine */
         lblPaginationbar_firstpagetextblockgrid_Caption = "1";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgrid_Internalname, "Caption", lblPaginationbar_firstpagetextblockgrid_Caption, true);
         lblPaginationbar_previouspagetextblockgrid_Caption = StringUtil.Str( (decimal)(AV29CurrentPage_Grid-1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgrid_Internalname, "Caption", lblPaginationbar_previouspagetextblockgrid_Caption, true);
         lblPaginationbar_currentpagetextblockgrid_Caption = StringUtil.Str( (decimal)(AV29CurrentPage_Grid), 4, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_currentpagetextblockgrid_Internalname, "Caption", lblPaginationbar_currentpagetextblockgrid_Caption, true);
         lblPaginationbar_nextpagetextblockgrid_Caption = StringUtil.Str( (decimal)(AV29CurrentPage_Grid+1), 10, 0);
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgrid_Internalname, "Caption", lblPaginationbar_nextpagetextblockgrid_Caption, true);
         lblPaginationbar_previouspagebuttontextblockgrid_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagebuttontextblockgrid_Internalname, "Class", lblPaginationbar_previouspagebuttontextblockgrid_Class, true);
         lblPaginationbar_nextpagebuttontextblockgrid_Class = "K2BToolsTextBlock_PaginationNormal";
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagebuttontextblockgrid_Internalname, "Class", lblPaginationbar_nextpagebuttontextblockgrid_Class, true);
         if ( (0==AV29CurrentPage_Grid) || ( AV29CurrentPage_Grid <= 1 ) )
         {
            lblPaginationbar_previouspagebuttontextblockgrid_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagebuttontextblockgrid_Internalname, "Class", lblPaginationbar_previouspagebuttontextblockgrid_Class, true);
            cellPaginationbar_firstpagecellgrid_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgrid_Internalname, "Class", cellPaginationbar_firstpagecellgrid_Class, true);
            lblPaginationbar_firstpagetextblockgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgrid_Visible), 5, 0)), true);
            cellPaginationbar_spacingleftcellgrid_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgrid_Internalname, "Class", cellPaginationbar_spacingleftcellgrid_Class, true);
            lblPaginationbar_spacinglefttextblockgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgrid_Visible), 5, 0)), true);
            cellPaginationbar_previouspagecellgrid_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_previouspagecellgrid_Internalname, "Class", cellPaginationbar_previouspagecellgrid_Class, true);
            lblPaginationbar_previouspagetextblockgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_previouspagetextblockgrid_Visible), 5, 0)), true);
         }
         else
         {
            cellPaginationbar_previouspagecellgrid_Class = "K2BToolsCell_PaginationPrevious";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_previouspagecellgrid_Internalname, "Class", cellPaginationbar_previouspagecellgrid_Class, true);
            lblPaginationbar_previouspagetextblockgrid_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_previouspagetextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_previouspagetextblockgrid_Visible), 5, 0)), true);
            if ( AV29CurrentPage_Grid == 2 )
            {
               cellPaginationbar_firstpagecellgrid_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgrid_Internalname, "Class", cellPaginationbar_firstpagecellgrid_Class, true);
               lblPaginationbar_firstpagetextblockgrid_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgrid_Visible), 5, 0)), true);
               cellPaginationbar_spacingleftcellgrid_Class = "K2BToolsCell_PaginationInvisible";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgrid_Internalname, "Class", cellPaginationbar_spacingleftcellgrid_Class, true);
               lblPaginationbar_spacinglefttextblockgrid_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgrid_Visible), 5, 0)), true);
            }
            else
            {
               cellPaginationbar_firstpagecellgrid_Class = "K2BToolsCell_PaginationLeft";
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_firstpagecellgrid_Internalname, "Class", cellPaginationbar_firstpagecellgrid_Class, true);
               lblPaginationbar_firstpagetextblockgrid_Visible = 1;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_firstpagetextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_firstpagetextblockgrid_Visible), 5, 0)), true);
               if ( AV29CurrentPage_Grid == 3 )
               {
                  cellPaginationbar_spacingleftcellgrid_Class = "K2BToolsCell_PaginationInvisible";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgrid_Internalname, "Class", cellPaginationbar_spacingleftcellgrid_Class, true);
                  lblPaginationbar_spacinglefttextblockgrid_Visible = 0;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgrid_Visible), 5, 0)), true);
               }
               else
               {
                  cellPaginationbar_spacingleftcellgrid_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingleftcellgrid_Internalname, "Class", cellPaginationbar_spacingleftcellgrid_Class, true);
                  lblPaginationbar_spacinglefttextblockgrid_Visible = 1;
                  context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacinglefttextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacinglefttextblockgrid_Visible), 5, 0)), true);
               }
            }
         }
         if ( ! AV33HasNextPage_Grid )
         {
            lblPaginationbar_nextpagebuttontextblockgrid_Class = "K2BToolsTextBlock_PaginationNormal_Disabled";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagebuttontextblockgrid_Internalname, "Class", lblPaginationbar_nextpagebuttontextblockgrid_Class, true);
            cellPaginationbar_spacingrightcellgrid_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingrightcellgrid_Internalname, "Class", cellPaginationbar_spacingrightcellgrid_Class, true);
            lblPaginationbar_spacingrighttextblockgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacingrighttextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacingrighttextblockgrid_Visible), 5, 0)), true);
            cellPaginationbar_nextpagecellgrid_Class = "K2BToolsCell_PaginationInvisible";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_nextpagecellgrid_Internalname, "Class", cellPaginationbar_nextpagecellgrid_Class, true);
            lblPaginationbar_nextpagetextblockgrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_nextpagetextblockgrid_Visible), 5, 0)), true);
         }
         else
         {
            cellPaginationbar_nextpagecellgrid_Class = "K2BToolsCell_PaginationNext";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_nextpagecellgrid_Internalname, "Class", cellPaginationbar_nextpagecellgrid_Class, true);
            lblPaginationbar_nextpagetextblockgrid_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_nextpagetextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_nextpagetextblockgrid_Visible), 5, 0)), true);
            cellPaginationbar_spacingrightcellgrid_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cellPaginationbar_spacingrightcellgrid_Internalname, "Class", cellPaginationbar_spacingrightcellgrid_Class, true);
            lblPaginationbar_spacingrighttextblockgrid_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, lblPaginationbar_spacingrighttextblockgrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(lblPaginationbar_spacingrighttextblockgrid_Visible), 5, 0)), true);
         }
         if ( ( AV29CurrentPage_Grid <= 1 ) && ! AV33HasNextPage_Grid )
         {
            tblPaginationbar_pagingcontainertablegrid_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblPaginationbar_pagingcontainertablegrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblPaginationbar_pagingcontainertablegrid_Visible), 5, 0)), true);
         }
         else
         {
            tblPaginationbar_pagingcontainertablegrid_Visible = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblPaginationbar_pagingcontainertablegrid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblPaginationbar_pagingcontainertablegrid_Visible), 5, 0)), true);
         }
      }

      private void E153A2( )
      {
         /* Grid_Load Routine */
         tblI_noresultsfoundtablename_grid_Visible = 1;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblI_noresultsfoundtablename_grid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblI_noresultsfoundtablename_grid_Visible), 5, 0)), true);
         AV32I_LoadCount_Grid = 0;
         AV33HasNextPage_Grid = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33HasNextPage_Grid", AV33HasNextPage_Grid);
         AV38Exit_Grid = false;
         while ( true )
         {
            AV32I_LoadCount_Grid = (short)(AV32I_LoadCount_Grid+1);
            if ( AV32I_LoadCount_Grid > AV34RowsPerPage_Grid * AV29CurrentPage_Grid )
            {
               AV33HasNextPage_Grid = true;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV33HasNextPage_Grid", AV33HasNextPage_Grid);
               if (true) break;
            }
            /* Execute user subroutine: 'U_LOADROWVARS(GRID)' */
            S182 ();
            if (returnInSub) return;
            if ( AV38Exit_Grid )
            {
               if (true) break;
            }
            if ( ( AV32I_LoadCount_Grid > ( AV34RowsPerPage_Grid * ( AV29CurrentPage_Grid - 1 ) ) ) && ( AV32I_LoadCount_Grid <= ( AV34RowsPerPage_Grid * AV29CurrentPage_Grid ) ) )
            {
               tblI_noresultsfoundtablename_grid_Visible = 0;
               context.httpAjaxContext.ajax_rsp_assign_prop("", false, tblI_noresultsfoundtablename_grid_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblI_noresultsfoundtablename_grid_Visible), 5, 0)), true);
               /* Load Method */
               if ( wbStart != -1 )
               {
                  wbStart = 54;
               }
               sendrow_542( ) ;
               if ( isFullAjaxMode( ) && ! bGXsfl_54_Refreshing )
               {
                  context.DoAjaxLoad(54, GridRow);
               }
            }
         }
         /* Execute user subroutine: 'UPDATEPAGINGCONTROLS(GRID)' */
         S162 ();
         if (returnInSub) return;
         /* Execute user subroutine: 'SAVEGRIDSTATE(GRID)' */
         S152 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S172( )
      {
         /* 'U_GRIDREFRESH(GRID)' Routine */
      }

      protected void E163A2( )
      {
         /* Grid_Onlineactivate Routine */
         /* Execute user subroutine: 'U_ONLINEACTIVATE(GRID)' */
         S192 ();
         if (returnInSub) return;
         /*  Sending Event outputs  */
      }

      protected void S192( )
      {
         /* 'U_ONLINEACTIVATE(GRID)' Routine */
         AV39String = StringUtil.Trim( AV36RecorridoID.ToString());
         /* Object Property */
         if ( StringUtil.StrCmp(StringUtil.Lower( WebComp_Wcrecorrido_Component), StringUtil.Lower( "WCMapa")) != 0 )
         {
            WebComp_Wcrecorrido = getWebComponent(GetType(), "GeneXus.Programs", "wcmapa", new Object[] {context} );
            WebComp_Wcrecorrido.ComponentInit();
            WebComp_Wcrecorrido.Name = "WCMapa";
            WebComp_Wcrecorrido_Component = "WCMapa";
         }
         if ( StringUtil.Len( WebComp_Wcrecorrido_Component) != 0 )
         {
            WebComp_Wcrecorrido.setjustcreated();
            WebComp_Wcrecorrido.componentprepare(new Object[] {(String)"W0087",(String)"",(String)AV39String});
            WebComp_Wcrecorrido.componentbind(new Object[] {(String)""});
         }
         if ( isFullAjaxMode( ) )
         {
            context.httpAjaxContext.ajax_rspStartCmp("gxHTMLWrpW0087"+"");
            WebComp_Wcrecorrido.componentdraw();
            context.httpAjaxContext.ajax_rspEndCmp();
         }
      }

      protected void wb_table3_65_3A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblPaginationbar_pagingcontainertablegrid_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblPaginationbar_pagingcontainertablegrid_Internalname, tblPaginationbar_pagingcontainertablegrid_Internalname, "", "K2BToolsTable_PaginationContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_previouspagebuttoncellgrid_Internalname+"\"  class='K2BToolsCell_PaginationFirst'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_previouspagebuttontextblockgrid_Internalname, "&laquo;", "", "", lblPaginationbar_previouspagebuttontextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e173a1_client"+"'", "", lblPaginationbar_previouspagebuttontextblockgrid_Class, 7, "", 1, 1, 1, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_firstpagecellgrid_Internalname+"\"  class='"+cellPaginationbar_firstpagecellgrid_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_firstpagetextblockgrid_Internalname, lblPaginationbar_firstpagetextblockgrid_Caption, "", "", lblPaginationbar_firstpagetextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e183a1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_firstpagetextblockgrid_Visible, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_spacingleftcellgrid_Internalname+"\"  class='"+cellPaginationbar_spacingleftcellgrid_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_spacinglefttextblockgrid_Internalname, "...", "", "", lblPaginationbar_spacinglefttextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblPaginationbar_spacinglefttextblockgrid_Visible, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_previouspagecellgrid_Internalname+"\"  class='"+cellPaginationbar_previouspagecellgrid_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_previouspagetextblockgrid_Internalname, lblPaginationbar_previouspagetextblockgrid_Caption, "", "", lblPaginationbar_previouspagetextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e173a1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_previouspagetextblockgrid_Visible, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_currentpagecellgrid_Internalname+"\"  class='K2BToolsCell_PaginationCurrentPage'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_currentpagetextblockgrid_Internalname, lblPaginationbar_currentpagetextblockgrid_Caption, "", "", lblPaginationbar_currentpagetextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationCurrent", 0, "", 1, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_nextpagecellgrid_Internalname+"\"  class='"+cellPaginationbar_nextpagecellgrid_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_nextpagetextblockgrid_Internalname, lblPaginationbar_nextpagetextblockgrid_Caption, "", "", lblPaginationbar_nextpagetextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e193a1_client"+"'", "", "K2BToolsTextBlock_PaginationNormal", 7, "", lblPaginationbar_nextpagetextblockgrid_Visible, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_spacingrightcellgrid_Internalname+"\"  class='"+cellPaginationbar_spacingrightcellgrid_Class+"'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_spacingrighttextblockgrid_Internalname, "...", "", "", lblPaginationbar_spacingrighttextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_PaginationDisabled", 0, "", lblPaginationbar_spacingrighttextblockgrid_Visible, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td id=\""+cellPaginationbar_nextpagebuttoncellgrid_Internalname+"\"  class='K2BToolsCell_PaginationLast'>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblPaginationbar_nextpagebuttontextblockgrid_Internalname, "&raquo;", "", "", lblPaginationbar_nextpagebuttontextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e193a1_client"+"'", "", lblPaginationbar_nextpagebuttontextblockgrid_Class, 7, "", 1, 1, 1, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_65_3A2e( true) ;
         }
         else
         {
            wb_table3_65_3A2e( false) ;
         }
      }

      protected void wb_table2_59_3A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblI_noresultsfoundtablename_grid_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblI_noresultsfoundtablename_grid_Internalname, tblI_noresultsfoundtablename_grid_Internalname, "", "K2BToolsTable_NoResultsFound", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblI_noresultsfoundtextblock_grid_Internalname, "No hay resultados", "", "", lblI_noresultsfoundtextblock_grid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "K2BToolsTextBlock_NoResultsFound", 0, "", 1, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_59_3A2e( true) ;
         }
         else
         {
            wb_table2_59_3A2e( false) ;
         }
      }

      protected void wb_table1_27_3A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblLayoutdefined_table2_grid_Internalname, tblLayoutdefined_table2_grid_Internalname, "", "K2BToolsTable_FloatRight", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridsettings_globaltablegrid_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_GridSettingsContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGridsettings_labelgrid_Internalname, "Config. tabla &#x25BC", "", "", lblGridsettings_labelgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+"e203a1_client"+"'", "", "K2BToolsTextBlock_GridSettingsTitle", 7, "", 1, 1, 1, "HLP_WPRecorridos.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divGridsettings_contentoutertablegrid_Internalname, divGridsettings_contentoutertablegrid_Visible, 0, "px", 0, "px", "K2BToolsTable_GridSettings", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            wb_table4_39_3A2( true) ;
         }
         else
         {
            wb_table4_39_3A2( false) ;
         }
         return  ;
      }

      protected void wb_table4_39_3A2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 48,'',false,'',0)\"";
            ClassString = "K2BToolsButton_MainAction";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttGridsettings_savegrid_Internalname, "gx.evt.setGridEvt("+StringUtil.Str( (decimal)(54), 2, 0)+","+"null"+");", "Guardar", bttGridsettings_savegrid_Jsonclick, 5, "Guardar", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",false,"+"'"+"E\\'SAVEGRIDSETTINGS(GRID)\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_WPRecorridos.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_27_3A2e( true) ;
         }
         else
         {
            wb_table1_27_3A2e( false) ;
         }
      }

      protected void wb_table4_39_3A2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblGridsettings_tablecontentgrid_Internalname, tblGridsettings_tablecontentgrid_Internalname, "", "K2BToolsTable_GridSettingsContent", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblGridsettings_rowsperpagetextblockgrid_Internalname, "Filas por p�gina", "", "", lblGridsettings_rowsperpagetextblockgrid_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 0, "HLP_WPRecorridos.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbavGridsettingsrowsperpage_grid_Internalname, "Grid Settings Rows Per Page_Grid", "col-sm-3 K2BToolsEnhancedComboLabel", 0, true);
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 45,'',false,'" + sGXsfl_54_idx + "',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbavGridsettingsrowsperpage_grid, cmbavGridsettingsrowsperpage_grid_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0)), 1, cmbavGridsettingsrowsperpage_grid_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "Filas por p�gina", 1, cmbavGridsettingsrowsperpage_grid.Enabled, 0, 0, 0, "em", 0, "", "", "K2BToolsEnhancedCombo", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,45);\"", "", true, "HLP_WPRecorridos.htm");
            cmbavGridsettingsrowsperpage_grid.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(AV35GridSettingsRowsPerPage_Grid), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbavGridsettingsrowsperpage_grid_Internalname, "Values", (String)(cmbavGridsettingsrowsperpage_grid.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table4_39_3A2e( true) ;
         }
         else
         {
            wb_table4_39_3A2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV24PacienteID = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "AV24PacienteID", AV24PacienteID);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "gxhash_vPACIENTEID", GetSecureSignedToken( "", StringUtil.RTrim( context.localUtil.Format( AV24PacienteID, "")), context));
      }

      public override String getresponse( String sGXDynURL )
      {
         context.SetDefaultTheme("K2BFlat");
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA3A2( ) ;
         WS3A2( ) ;
         WE3A2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("K2BControlBeautify/montrezorro-bootstrap-checkbox/css/bootstrap-checkbox.css", "");
         AddStyleSheetFile("K2BControlBeautify/silviomoreto-bootstrap-select/dist/css/bootstrap-select.css", "");
         AddStyleSheetFile("K2BControlBeautify/toastr-master/toastr.min.css", "");
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         if ( ! ( WebComp_Wcrecorrido == null ) )
         {
            if ( StringUtil.Len( WebComp_Wcrecorrido_Component) != 0 )
            {
               WebComp_Wcrecorrido.componentthemes();
            }
         }
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811181721472", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("wprecorridos.js", "?201811181721472", false);
         context.AddJavascriptSource("K2BControlBeautify/montrezorro-bootstrap-checkbox/js/bootstrap-checkbox.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/silviomoreto-bootstrap-select/dist/js/bootstrap-select.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/toastr-master/toastr.min.js", "", false);
         context.AddJavascriptSource("K2BControlBeautify/K2BControlBeautifyRender.js", "", false);
         /* End function include_jscripts */
      }

      protected void SubsflControlProps_542( )
      {
         edtavRecorridoid_Internalname = "vRECORRIDOID_"+sGXsfl_54_idx;
         edtavRecorridofecha_Internalname = "vRECORRIDOFECHA_"+sGXsfl_54_idx;
      }

      protected void SubsflControlProps_fel_542( )
      {
         edtavRecorridoid_Internalname = "vRECORRIDOID_"+sGXsfl_54_fel_idx;
         edtavRecorridofecha_Internalname = "vRECORRIDOFECHA_"+sGXsfl_54_fel_idx;
      }

      protected void sendrow_542( )
      {
         SubsflControlProps_542( ) ;
         WB3A0( ) ;
         GridRow = GXWebRow.GetNew(context,GridContainer);
         if ( subGrid_Backcolorstyle == 0 )
         {
            /* None style subfile background logic. */
            subGrid_Backstyle = 0;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
         }
         else if ( subGrid_Backcolorstyle == 1 )
         {
            /* Uniform style subfile background logic. */
            subGrid_Backstyle = 0;
            subGrid_Backcolor = subGrid_Allbackcolor;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Uniform";
            }
         }
         else if ( subGrid_Backcolorstyle == 2 )
         {
            /* Header style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
            {
               subGrid_Linesclass = subGrid_Class+"Odd";
            }
            subGrid_Backcolor = (int)(0x0);
         }
         else if ( subGrid_Backcolorstyle == 3 )
         {
            /* Report style subfile background logic. */
            subGrid_Backstyle = 1;
            if ( ((int)((nGXsfl_54_idx) % (2))) == 0 )
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Even";
               }
            }
            else
            {
               subGrid_Backcolor = (int)(0x0);
               if ( StringUtil.StrCmp(subGrid_Class, "") != 0 )
               {
                  subGrid_Linesclass = subGrid_Class+"Odd";
               }
            }
         }
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<tr ") ;
            context.WriteHtmlText( " class=\""+"Grid_AllowSelection Grid_WorkWith"+"\" style=\""+""+"\"") ;
            context.WriteHtmlText( " gxrow=\""+sGXsfl_54_idx+"\">") ;
         }
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+""+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRecorridoid_Internalname,AV36RecorridoID.ToString(),AV36RecorridoID.ToString(),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRecorridoid_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn",(String)"",(short)-1,(int)edtavRecorridoid_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)36,(short)0,(short)0,(short)54,(short)1,(short)0,(short)0,(bool)true,(String)"",(String)"",(bool)false});
         /* Subfile cell */
         if ( GridContainer.GetWrapped() == 1 )
         {
            context.WriteHtmlText( "<td valign=\"middle\" align=\""+"right"+"\""+" style=\""+""+"\">") ;
         }
         /* Single line edit */
         ROClassString = "Attribute_Grid";
         GridRow.AddColumnProperties("edit", 1, isAjaxCallMode( ), new Object[] {(String)edtavRecorridofecha_Internalname,context.localUtil.TToC( AV37RecorridoFecha, 10, 8, 0, 3, "/", ":", " "),context.localUtil.Format( AV37RecorridoFecha, "99/99/99 99:99"),(String)"",(String)"'"+""+"'"+",false,"+"'"+""+"'",(String)"",(String)"",(String)"",(String)"",(String)edtavRecorridofecha_Jsonclick,(short)0,(String)"Attribute_Grid",(String)"",(String)ROClassString,(String)"K2BToolsGridColumn InvisibleInExtraSmallColumn",(String)"",(short)-1,(int)edtavRecorridofecha_Enabled,(short)0,(String)"text",(String)"",(short)0,(String)"px",(short)17,(String)"px",(short)14,(short)0,(short)0,(short)54,(short)1,(short)-1,(short)0,(bool)true,(String)"",(String)"right",(bool)false});
         send_integrity_lvl_hashes3A2( ) ;
         GridContainer.AddRow(GridRow);
         nGXsfl_54_idx = (short)(((subGrid_Islastpage==1)&&(nGXsfl_54_idx+1>subGrid_Recordsperpage( )) ? 1 : nGXsfl_54_idx+1));
         sGXsfl_54_idx = StringUtil.PadL( StringUtil.LTrim( StringUtil.Str( (decimal)(nGXsfl_54_idx), 4, 0)), 4, "0");
         SubsflControlProps_542( ) ;
         /* End function sendrow_542 */
      }

      protected void init_default_properties( )
      {
         lblGridsettings_labelgrid_Internalname = "GRIDSETTINGS_LABELGRID";
         lblGridsettings_rowsperpagetextblockgrid_Internalname = "GRIDSETTINGS_ROWSPERPAGETEXTBLOCKGRID";
         cmbavGridsettingsrowsperpage_grid_Internalname = "vGRIDSETTINGSROWSPERPAGE_GRID";
         tblGridsettings_tablecontentgrid_Internalname = "GRIDSETTINGS_TABLECONTENTGRID";
         bttGridsettings_savegrid_Internalname = "GRIDSETTINGS_SAVEGRID";
         divGridsettings_contentoutertablegrid_Internalname = "GRIDSETTINGS_CONTENTOUTERTABLEGRID";
         divGridsettings_globaltablegrid_Internalname = "GRIDSETTINGS_GLOBALTABLEGRID";
         tblLayoutdefined_table2_grid_Internalname = "LAYOUTDEFINED_TABLE2_GRID";
         divLayoutdefined_tableactionscontainer_grid_Internalname = "LAYOUTDEFINED_TABLEACTIONSCONTAINER_GRID";
         edtavRecorridoid_Internalname = "vRECORRIDOID";
         edtavRecorridofecha_Internalname = "vRECORRIDOFECHA";
         lblI_noresultsfoundtextblock_grid_Internalname = "I_NORESULTSFOUNDTEXTBLOCK_GRID";
         tblI_noresultsfoundtablename_grid_Internalname = "I_NORESULTSFOUNDTABLENAME_GRID";
         divMaingrid_responsivetable_grid_Internalname = "MAINGRID_RESPONSIVETABLE_GRID";
         lblPaginationbar_previouspagebuttontextblockgrid_Internalname = "PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRID";
         cellPaginationbar_previouspagebuttoncellgrid_Internalname = "PAGINATIONBAR_PREVIOUSPAGEBUTTONCELLGRID";
         lblPaginationbar_firstpagetextblockgrid_Internalname = "PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRID";
         cellPaginationbar_firstpagecellgrid_Internalname = "PAGINATIONBAR_FIRSTPAGECELLGRID";
         lblPaginationbar_spacinglefttextblockgrid_Internalname = "PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRID";
         cellPaginationbar_spacingleftcellgrid_Internalname = "PAGINATIONBAR_SPACINGLEFTCELLGRID";
         lblPaginationbar_previouspagetextblockgrid_Internalname = "PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRID";
         cellPaginationbar_previouspagecellgrid_Internalname = "PAGINATIONBAR_PREVIOUSPAGECELLGRID";
         lblPaginationbar_currentpagetextblockgrid_Internalname = "PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRID";
         cellPaginationbar_currentpagecellgrid_Internalname = "PAGINATIONBAR_CURRENTPAGECELLGRID";
         lblPaginationbar_nextpagetextblockgrid_Internalname = "PAGINATIONBAR_NEXTPAGETEXTBLOCKGRID";
         cellPaginationbar_nextpagecellgrid_Internalname = "PAGINATIONBAR_NEXTPAGECELLGRID";
         lblPaginationbar_spacingrighttextblockgrid_Internalname = "PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRID";
         cellPaginationbar_spacingrightcellgrid_Internalname = "PAGINATIONBAR_SPACINGRIGHTCELLGRID";
         lblPaginationbar_nextpagebuttontextblockgrid_Internalname = "PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRID";
         cellPaginationbar_nextpagebuttoncellgrid_Internalname = "PAGINATIONBAR_NEXTPAGEBUTTONCELLGRID";
         tblPaginationbar_pagingcontainertablegrid_Internalname = "PAGINATIONBAR_PAGINGCONTAINERTABLEGRID";
         divLayoutdefined_table3_grid_Internalname = "LAYOUTDEFINED_TABLE3_GRID";
         divLayoutdefined_globalgridtable_grid_Internalname = "LAYOUTDEFINED_GLOBALGRIDTABLE_GRID";
         divColumncontainertable_column_Internalname = "COLUMNCONTAINERTABLE_COLUMN";
         divColumncontainertable_column1_Internalname = "COLUMNCONTAINERTABLE_COLUMN1";
         divColumns1_maincolumnstable_Internalname = "COLUMNS1_MAINCOLUMNSTABLE";
         divContenttable_Internalname = "CONTENTTABLE";
         K2bcontrolbeautify1_Internalname = "K2BCONTROLBEAUTIFY1";
         divMaintable_Internalname = "MAINTABLE";
         Form.Internalname = "FORM";
         subGrid_Internalname = "GRID";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavRecorridofecha_Jsonclick = "";
         edtavRecorridoid_Jsonclick = "";
         cmbavGridsettingsrowsperpage_grid_Jsonclick = "";
         cmbavGridsettingsrowsperpage_grid.Enabled = 1;
         divGridsettings_contentoutertablegrid_Visible = 1;
         lblPaginationbar_spacingrighttextblockgrid_Visible = 1;
         lblPaginationbar_nextpagetextblockgrid_Visible = 1;
         lblPaginationbar_previouspagetextblockgrid_Visible = 1;
         lblPaginationbar_spacinglefttextblockgrid_Visible = 1;
         lblPaginationbar_firstpagetextblockgrid_Visible = 1;
         tblI_noresultsfoundtablename_grid_Visible = 1;
         tblPaginationbar_pagingcontainertablegrid_Visible = 1;
         cellPaginationbar_nextpagecellgrid_Class = "K2BToolsCell_PaginationNext";
         cellPaginationbar_spacingrightcellgrid_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationRight";
         cellPaginationbar_previouspagecellgrid_Class = "K2BToolsCell_PaginationPrevious";
         cellPaginationbar_spacingleftcellgrid_Class = "K2BToolsCell_PaginationDisabled K2BToolsCell_PaginationLeft";
         cellPaginationbar_firstpagecellgrid_Class = "K2BToolsCell_PaginationLeft";
         lblPaginationbar_nextpagebuttontextblockgrid_Class = "K2BToolsTextBlock_PaginationNormal";
         lblPaginationbar_previouspagebuttontextblockgrid_Class = "K2BToolsTextBlock_PaginationNormal";
         lblPaginationbar_nextpagetextblockgrid_Caption = "#";
         lblPaginationbar_currentpagetextblockgrid_Caption = "#";
         lblPaginationbar_previouspagetextblockgrid_Caption = "#";
         lblPaginationbar_firstpagetextblockgrid_Caption = "1";
         subGrid_Allowcollapsing = 0;
         subGrid_Allowhovering = -1;
         subGrid_Allowselection = 1;
         subGrid_Sortable = 0;
         edtavRecorridofecha_Enabled = 0;
         edtavRecorridoid_Enabled = 0;
         subGrid_Class = "Grid_AllowSelection Grid_WorkWith";
         subGrid_Backcolorstyle = 0;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Recorridos";
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false},{av:'A21RecorridoDetallePaciente',fld:'RECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A23RecorridoID',fld:'RECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A24RecorridoFecha',fld:'RECORRIDOFECHA',pic:'99/99/99 99:99',nv:''},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV24PacienteID',fld:'vPACIENTEID',pic:'',hsh:true,nv:''}],oparms:[{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegrid_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRID',prop:'Visible'},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0}]}");
         setEventMetadata("'TOGGLEGRIDSETTINGS(GRID)'","{handler:'E203A1',iparms:[{av:'divGridsettings_contentoutertablegrid_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRID',prop:'Visible'},{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0}],oparms:[{av:'cmbavGridsettingsrowsperpage_grid'},{av:'AV35GridSettingsRowsPerPage_Grid',fld:'vGRIDSETTINGSROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'divGridsettings_contentoutertablegrid_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRID',prop:'Visible'}]}");
         setEventMetadata("'SAVEGRIDSETTINGS(GRID)'","{handler:'E113A2',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false},{av:'AV24PacienteID',fld:'vPACIENTEID',pic:'',hsh:true,nv:''},{av:'A21RecorridoDetallePaciente',fld:'RECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A23RecorridoID',fld:'RECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A24RecorridoFecha',fld:'RECORRIDOFECHA',pic:'99/99/99 99:99',nv:''},{av:'cmbavGridsettingsrowsperpage_grid'},{av:'AV35GridSettingsRowsPerPage_Grid',fld:'vGRIDSETTINGSROWSPERPAGE_GRID',pic:'ZZZ9',nv:0}],oparms:[{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'divGridsettings_contentoutertablegrid_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRID',prop:'Visible'},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'}]}");
         setEventMetadata("GRID.REFRESH","{handler:'E143A2',iparms:[{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false}],oparms:[{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'lblPaginationbar_firstpagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_previouspagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_currentpagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_nextpagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_previouspagebuttontextblockgrid_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRID',prop:'Class'},{av:'lblPaginationbar_nextpagebuttontextblockgrid_Class',ctrl:'PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRID',prop:'Class'},{av:'cellPaginationbar_firstpagecellgrid_Class',ctrl:'PAGINATIONBAR_FIRSTPAGECELLGRID',prop:'Class'},{av:'lblPaginationbar_firstpagetextblockgrid_Visible',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_spacingleftcellgrid_Class',ctrl:'PAGINATIONBAR_SPACINGLEFTCELLGRID',prop:'Class'},{av:'lblPaginationbar_spacinglefttextblockgrid_Visible',ctrl:'PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_previouspagecellgrid_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGECELLGRID',prop:'Class'},{av:'lblPaginationbar_previouspagetextblockgrid_Visible',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_spacingrightcellgrid_Class',ctrl:'PAGINATIONBAR_SPACINGRIGHTCELLGRID',prop:'Class'},{av:'lblPaginationbar_spacingrighttextblockgrid_Visible',ctrl:'PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_nextpagecellgrid_Class',ctrl:'PAGINATIONBAR_NEXTPAGECELLGRID',prop:'Class'},{av:'lblPaginationbar_nextpagetextblockgrid_Visible',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRID',prop:'Visible'},{av:'tblPaginationbar_pagingcontainertablegrid_Visible',ctrl:'PAGINATIONBAR_PAGINGCONTAINERTABLEGRID',prop:'Visible'}]}");
         setEventMetadata("'PAGINGNEXT(GRID)'","{handler:'E193A1',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false},{av:'AV24PacienteID',fld:'vPACIENTEID',pic:'',hsh:true,nv:''},{av:'A21RecorridoDetallePaciente',fld:'RECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A23RecorridoID',fld:'RECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A24RecorridoFecha',fld:'RECORRIDOFECHA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegrid_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRID',prop:'Visible'}]}");
         setEventMetadata("'PAGINGPREVIOUS(GRID)'","{handler:'E173A1',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false},{av:'AV24PacienteID',fld:'vPACIENTEID',pic:'',hsh:true,nv:''},{av:'A21RecorridoDetallePaciente',fld:'RECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A23RecorridoID',fld:'RECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A24RecorridoFecha',fld:'RECORRIDOFECHA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegrid_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRID',prop:'Visible'}]}");
         setEventMetadata("'PAGINGFIRST(GRID)'","{handler:'E183A1',iparms:[{av:'GRID_nFirstRecordOnPage',nv:0},{av:'GRID_nEOF',nv:0},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false},{av:'AV24PacienteID',fld:'vPACIENTEID',pic:'',hsh:true,nv:''},{av:'A21RecorridoDetallePaciente',fld:'RECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A23RecorridoID',fld:'RECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A24RecorridoFecha',fld:'RECORRIDOFECHA',pic:'99/99/99 99:99',nv:''}],oparms:[{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'subGrid_Backcolorstyle',ctrl:'GRID',prop:'Backcolorstyle'},{av:'divGridsettings_contentoutertablegrid_Visible',ctrl:'GRIDSETTINGS_CONTENTOUTERTABLEGRID',prop:'Visible'}]}");
         setEventMetadata("GRID.LOAD","{handler:'E153A2',iparms:[{av:'AV34RowsPerPage_Grid',fld:'vROWSPERPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV29CurrentPage_Grid',fld:'vCURRENTPAGE_GRID',pic:'ZZZ9',nv:0},{av:'AV24PacienteID',fld:'vPACIENTEID',pic:'',hsh:true,nv:''},{av:'A21RecorridoDetallePaciente',fld:'RECORRIDODETALLEPACIENTE',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A23RecorridoID',fld:'RECORRIDOID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'A24RecorridoFecha',fld:'RECORRIDOFECHA',pic:'99/99/99 99:99',nv:''},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false},{av:'AV42Pgmname',fld:'vPGMNAME',pic:'',nv:''}],oparms:[{av:'tblI_noresultsfoundtablename_grid_Visible',ctrl:'I_NORESULTSFOUNDTABLENAME_GRID',prop:'Visible'},{av:'AV33HasNextPage_Grid',fld:'vHASNEXTPAGE_GRID',pic:'',nv:false},{av:'AV25PacienteGUID',fld:'vPACIENTEGUID',pic:'',nv:'00000000-0000-0000-0000-000000000000'},{av:'AV36RecorridoID',fld:'vRECORRIDOID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'},{av:'AV37RecorridoFecha',fld:'vRECORRIDOFECHA',pic:'99/99/99 99:99',nv:''},{av:'lblPaginationbar_firstpagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_previouspagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_currentpagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_CURRENTPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_nextpagetextblockgrid_Caption',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRID',prop:'Caption'},{av:'lblPaginationbar_previouspagebuttontextblockgrid_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGEBUTTONTEXTBLOCKGRID',prop:'Class'},{av:'lblPaginationbar_nextpagebuttontextblockgrid_Class',ctrl:'PAGINATIONBAR_NEXTPAGEBUTTONTEXTBLOCKGRID',prop:'Class'},{av:'cellPaginationbar_firstpagecellgrid_Class',ctrl:'PAGINATIONBAR_FIRSTPAGECELLGRID',prop:'Class'},{av:'lblPaginationbar_firstpagetextblockgrid_Visible',ctrl:'PAGINATIONBAR_FIRSTPAGETEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_spacingleftcellgrid_Class',ctrl:'PAGINATIONBAR_SPACINGLEFTCELLGRID',prop:'Class'},{av:'lblPaginationbar_spacinglefttextblockgrid_Visible',ctrl:'PAGINATIONBAR_SPACINGLEFTTEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_previouspagecellgrid_Class',ctrl:'PAGINATIONBAR_PREVIOUSPAGECELLGRID',prop:'Class'},{av:'lblPaginationbar_previouspagetextblockgrid_Visible',ctrl:'PAGINATIONBAR_PREVIOUSPAGETEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_spacingrightcellgrid_Class',ctrl:'PAGINATIONBAR_SPACINGRIGHTCELLGRID',prop:'Class'},{av:'lblPaginationbar_spacingrighttextblockgrid_Visible',ctrl:'PAGINATIONBAR_SPACINGRIGHTTEXTBLOCKGRID',prop:'Visible'},{av:'cellPaginationbar_nextpagecellgrid_Class',ctrl:'PAGINATIONBAR_NEXTPAGECELLGRID',prop:'Class'},{av:'lblPaginationbar_nextpagetextblockgrid_Visible',ctrl:'PAGINATIONBAR_NEXTPAGETEXTBLOCKGRID',prop:'Visible'},{av:'tblPaginationbar_pagingcontainertablegrid_Visible',ctrl:'PAGINATIONBAR_PAGINGCONTAINERTABLEGRID',prop:'Visible'}]}");
         setEventMetadata("GRID.ONLINEACTIVATE","{handler:'E163A2',iparms:[{av:'AV36RecorridoID',fld:'vRECORRIDOID',pic:'',hsh:true,nv:'00000000-0000-0000-0000-000000000000'}],oparms:[{ctrl:'WCRECORRIDO'}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV24PacienteID = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         AV42Pgmname = "";
         A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         A23RecorridoID = (Guid)(Guid.Empty);
         A24RecorridoFecha = (DateTime)(DateTime.MinValue);
         GXKey = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GX_FocusControl = "";
         Form = new GXWebForm();
         sPrefix = "";
         ClassString = "";
         StyleString = "";
         GridContainer = new GXWebGrid( context);
         sStyleString = "";
         subGrid_Linesclass = "";
         GridColumn = new GXWebColumn();
         AV36RecorridoID = (Guid)(Guid.Empty);
         AV37RecorridoFecha = (DateTime)(DateTime.MinValue);
         WebComp_Wcrecorrido_Component = "";
         OldWcrecorrido = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV26GridStateKey = "";
         AV27GridState = new SdtK2BGridState(context);
         AV25PacienteGUID = (Guid)(Guid.Empty);
         scmdbuf = "";
         H003A2_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         H003A2_n21RecorridoDetallePaciente = new bool[] {false} ;
         H003A2_A23RecorridoID = new Guid[] {Guid.Empty} ;
         H003A2_A24RecorridoFecha = new DateTime[] {DateTime.MinValue} ;
         GridRow = new GXWebRow();
         AV39String = "";
         lblPaginationbar_previouspagebuttontextblockgrid_Jsonclick = "";
         lblPaginationbar_firstpagetextblockgrid_Jsonclick = "";
         lblPaginationbar_spacinglefttextblockgrid_Jsonclick = "";
         lblPaginationbar_previouspagetextblockgrid_Jsonclick = "";
         lblPaginationbar_currentpagetextblockgrid_Jsonclick = "";
         lblPaginationbar_nextpagetextblockgrid_Jsonclick = "";
         lblPaginationbar_spacingrighttextblockgrid_Jsonclick = "";
         lblPaginationbar_nextpagebuttontextblockgrid_Jsonclick = "";
         lblI_noresultsfoundtextblock_grid_Jsonclick = "";
         lblGridsettings_labelgrid_Jsonclick = "";
         TempTags = "";
         bttGridsettings_savegrid_Jsonclick = "";
         lblGridsettings_rowsperpagetextblockgrid_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         ROClassString = "";
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.wprecorridos__default(),
            new Object[][] {
                new Object[] {
               H003A2_A21RecorridoDetallePaciente, H003A2_n21RecorridoDetallePaciente, H003A2_A23RecorridoID, H003A2_A24RecorridoFecha
               }
            }
         );
         WebComp_Wcrecorrido = new GeneXus.Http.GXNullWebComponent();
         AV42Pgmname = "WPRecorridos";
         /* GeneXus formulas. */
         AV42Pgmname = "WPRecorridos";
         context.Gx_err = 0;
         edtavRecorridoid_Enabled = 0;
         edtavRecorridofecha_Enabled = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nRC_GXsfl_54 ;
      private short nGXsfl_54_idx=1 ;
      private short AV29CurrentPage_Grid ;
      private short AV34RowsPerPage_Grid ;
      private short initialized ;
      private short gxajaxcallmode ;
      private short wbEnd ;
      private short wbStart ;
      private short subGrid_Backcolorstyle ;
      private short subGrid_Titlebackstyle ;
      private short subGrid_Sortable ;
      private short subGrid_Allowselection ;
      private short subGrid_Allowhovering ;
      private short subGrid_Allowcollapsing ;
      private short subGrid_Collapsed ;
      private short nCmpId ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short AV35GridSettingsRowsPerPage_Grid ;
      private short GRID_nEOF ;
      private short AV32I_LoadCount_Grid ;
      private short nGXWrapped ;
      private short subGrid_Backstyle ;
      private int divGridsettings_contentoutertablegrid_Visible ;
      private int subGrid_Titlebackcolor ;
      private int subGrid_Allbackcolor ;
      private int edtavRecorridoid_Enabled ;
      private int edtavRecorridofecha_Enabled ;
      private int subGrid_Selectioncolor ;
      private int subGrid_Hoveringcolor ;
      private int subGrid_Islastpage ;
      private int lblPaginationbar_firstpagetextblockgrid_Visible ;
      private int lblPaginationbar_spacinglefttextblockgrid_Visible ;
      private int lblPaginationbar_previouspagetextblockgrid_Visible ;
      private int lblPaginationbar_spacingrighttextblockgrid_Visible ;
      private int lblPaginationbar_nextpagetextblockgrid_Visible ;
      private int tblPaginationbar_pagingcontainertablegrid_Visible ;
      private int tblI_noresultsfoundtablename_grid_Visible ;
      private int idxLst ;
      private int subGrid_Backcolor ;
      private long GRID_nCurrentRecord ;
      private long GRID_nFirstRecordOnPage ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sGXsfl_54_idx="0001" ;
      private String AV42Pgmname ;
      private String GXKey ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GX_FocusControl ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String divContenttable_Internalname ;
      private String divColumns1_maincolumnstable_Internalname ;
      private String divColumncontainertable_column_Internalname ;
      private String divLayoutdefined_globalgridtable_grid_Internalname ;
      private String divLayoutdefined_table3_grid_Internalname ;
      private String divLayoutdefined_tableactionscontainer_grid_Internalname ;
      private String divMaingrid_responsivetable_grid_Internalname ;
      private String sStyleString ;
      private String subGrid_Internalname ;
      private String subGrid_Class ;
      private String subGrid_Linesclass ;
      private String divColumncontainertable_column1_Internalname ;
      private String WebComp_Wcrecorrido_Component ;
      private String OldWcrecorrido ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavRecorridoid_Internalname ;
      private String edtavRecorridofecha_Internalname ;
      private String cmbavGridsettingsrowsperpage_grid_Internalname ;
      private String divGridsettings_contentoutertablegrid_Internalname ;
      private String scmdbuf ;
      private String lblPaginationbar_firstpagetextblockgrid_Caption ;
      private String lblPaginationbar_firstpagetextblockgrid_Internalname ;
      private String lblPaginationbar_previouspagetextblockgrid_Caption ;
      private String lblPaginationbar_previouspagetextblockgrid_Internalname ;
      private String lblPaginationbar_currentpagetextblockgrid_Caption ;
      private String lblPaginationbar_currentpagetextblockgrid_Internalname ;
      private String lblPaginationbar_nextpagetextblockgrid_Caption ;
      private String lblPaginationbar_nextpagetextblockgrid_Internalname ;
      private String lblPaginationbar_previouspagebuttontextblockgrid_Class ;
      private String lblPaginationbar_previouspagebuttontextblockgrid_Internalname ;
      private String lblPaginationbar_nextpagebuttontextblockgrid_Class ;
      private String lblPaginationbar_nextpagebuttontextblockgrid_Internalname ;
      private String cellPaginationbar_firstpagecellgrid_Class ;
      private String cellPaginationbar_firstpagecellgrid_Internalname ;
      private String cellPaginationbar_spacingleftcellgrid_Class ;
      private String cellPaginationbar_spacingleftcellgrid_Internalname ;
      private String lblPaginationbar_spacinglefttextblockgrid_Internalname ;
      private String cellPaginationbar_previouspagecellgrid_Class ;
      private String cellPaginationbar_previouspagecellgrid_Internalname ;
      private String cellPaginationbar_spacingrightcellgrid_Class ;
      private String cellPaginationbar_spacingrightcellgrid_Internalname ;
      private String lblPaginationbar_spacingrighttextblockgrid_Internalname ;
      private String cellPaginationbar_nextpagecellgrid_Class ;
      private String cellPaginationbar_nextpagecellgrid_Internalname ;
      private String tblPaginationbar_pagingcontainertablegrid_Internalname ;
      private String tblI_noresultsfoundtablename_grid_Internalname ;
      private String cellPaginationbar_previouspagebuttoncellgrid_Internalname ;
      private String lblPaginationbar_previouspagebuttontextblockgrid_Jsonclick ;
      private String lblPaginationbar_firstpagetextblockgrid_Jsonclick ;
      private String lblPaginationbar_spacinglefttextblockgrid_Jsonclick ;
      private String lblPaginationbar_previouspagetextblockgrid_Jsonclick ;
      private String cellPaginationbar_currentpagecellgrid_Internalname ;
      private String lblPaginationbar_currentpagetextblockgrid_Jsonclick ;
      private String lblPaginationbar_nextpagetextblockgrid_Jsonclick ;
      private String lblPaginationbar_spacingrighttextblockgrid_Jsonclick ;
      private String cellPaginationbar_nextpagebuttoncellgrid_Internalname ;
      private String lblPaginationbar_nextpagebuttontextblockgrid_Jsonclick ;
      private String lblI_noresultsfoundtextblock_grid_Internalname ;
      private String lblI_noresultsfoundtextblock_grid_Jsonclick ;
      private String tblLayoutdefined_table2_grid_Internalname ;
      private String divGridsettings_globaltablegrid_Internalname ;
      private String lblGridsettings_labelgrid_Internalname ;
      private String lblGridsettings_labelgrid_Jsonclick ;
      private String TempTags ;
      private String bttGridsettings_savegrid_Internalname ;
      private String bttGridsettings_savegrid_Jsonclick ;
      private String tblGridsettings_tablecontentgrid_Internalname ;
      private String lblGridsettings_rowsperpagetextblockgrid_Internalname ;
      private String lblGridsettings_rowsperpagetextblockgrid_Jsonclick ;
      private String cmbavGridsettingsrowsperpage_grid_Jsonclick ;
      private String sGXsfl_54_fel_idx="0001" ;
      private String ROClassString ;
      private String edtavRecorridoid_Jsonclick ;
      private String edtavRecorridofecha_Jsonclick ;
      private String K2bcontrolbeautify1_Internalname ;
      private DateTime A24RecorridoFecha ;
      private DateTime AV37RecorridoFecha ;
      private bool entryPointCalled ;
      private bool AV33HasNextPage_Grid ;
      private bool n21RecorridoDetallePaciente ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool bGXsfl_54_Refreshing=false ;
      private bool gx_refresh_fired ;
      private bool returnInSub ;
      private bool AV30Reload_Grid ;
      private bool AV38Exit_Grid ;
      private String AV24PacienteID ;
      private String wcpOAV24PacienteID ;
      private String AV26GridStateKey ;
      private String AV39String ;
      private Guid A21RecorridoDetallePaciente ;
      private Guid A23RecorridoID ;
      private Guid AV36RecorridoID ;
      private Guid AV25PacienteGUID ;
      private GXWebComponent WebComp_Wcrecorrido ;
      private GXWebGrid GridContainer ;
      private GXWebRow GridRow ;
      private GXWebColumn GridColumn ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_PacienteID ;
      private GXCombobox cmbavGridsettingsrowsperpage_grid ;
      private IDataStoreProvider pr_default ;
      private Guid[] H003A2_A21RecorridoDetallePaciente ;
      private bool[] H003A2_n21RecorridoDetallePaciente ;
      private Guid[] H003A2_A23RecorridoID ;
      private DateTime[] H003A2_A24RecorridoFecha ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXWebForm Form ;
      private SdtK2BGridState AV27GridState ;
   }

   public class wprecorridos__default : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
          new ForEachCursor(def[0])
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          Object[] prmH003A2 ;
          prmH003A2 = new Object[] {
          new Object[] {"@AV25PacienteGUID",SqlDbType.UniqueIdentifier,4,0}
          } ;
          def= new CursorDef[] {
              new CursorDef("H003A2", "SELECT DISTINCT NULL AS [RecorridoDetallePaciente], [RecorridoID], [RecorridoFecha] FROM ( SELECT TOP(100) PERCENT T1.[RecorridoDetallePaciente], T1.[RecorridoID], T2.[RecorridoFecha] FROM ([RecorridoDetalle] T1 WITH (NOLOCK) INNER JOIN [Recorrido] T2 WITH (NOLOCK) ON T2.[RecorridoID] = T1.[RecorridoID]) WHERE T1.[RecorridoDetallePaciente] = @AV25PacienteGUID ORDER BY [RecorridoDetallePaciente]) DistinctT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmH003A2,100,0,true,false )
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
             case 0 :
                ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
                ((bool[]) buf[1])[0] = rslt.wasNull(1);
                ((Guid[]) buf[2])[0] = rslt.getGuid(2) ;
                ((DateTime[]) buf[3])[0] = rslt.getGXDateTime(3) ;
                return;
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
             case 0 :
                stmt.SetParameter(1, (Guid)parms[0]);
                return;
       }
    }

 }

}
