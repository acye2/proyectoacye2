/*
               File: K2BWWMasterPageFlatCompact
        Description: K2 BWWMaster Page Flat Compact
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:46.6
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bwwmasterpageflatcompact : GXMasterPage, System.Web.SessionState.IRequiresSessionState
   {
      public k2bwwmasterpageflatcompact( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
      }

      public k2bwwmasterpageflatcompact( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         initialize_properties( ) ;
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            PA0I2( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV17Pgmname = "K2BWWMasterPageFlatCompact";
               context.Gx_err = 0;
               WS0I2( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  WE0I2( ) ;
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         if ( ! isFullAjaxMode( ) )
         {
            GXWebForm.AddResponsiveMetaHeaders((getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta);
            getDataAreaObject().RenderHtmlHeaders();
         }
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlOpenForm();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         if ( context.isAjaxRequest( ) )
         {
            context.httpAjaxContext.ajax_rsp_assign_sdt_attri("", true, "vMENUITEMS_MPAGE", AV9MenuItems);
         }
         else
         {
            context.httpAjaxContext.ajax_rsp_assign_hidden_sdt("vMENUITEMS_MPAGE", AV9MenuItems);
         }
         GxWebStd.gx_hidden_field( context, "vPGMNAME_MPAGE", StringUtil.RTrim( AV17Pgmname));
         GxWebStd.gx_hidden_field( context, "MYACCOUNTTABLE_MPAGE_Visible", StringUtil.LTrim( StringUtil.NToC( (decimal)(divMyaccounttable_Visible), 5, 0, ".", "")));
         GxWebStd.gx_hidden_field( context, "MENUCONTAINER_MPAGE_Class", StringUtil.RTrim( divMenucontainer_Class));
         GxWebStd.gx_hidden_field( context, "BTNTOGGLEMENU_MPAGE_Class", StringUtil.RTrim( bttBtntogglemenu_Class));
      }

      protected void RenderHtmlCloseForm0I2( )
      {
         SendCloseFormHiddens( ) ;
         SendSecurityToken((String)(sPrefix));
         if ( ! isFullAjaxMode( ) )
         {
            getDataAreaObject().RenderHtmlCloseForm();
         }
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.AddJavascriptSource("K2BAccordionMenu/metisMenu-1.1.3/dist/metisMenu.min.js", "", false);
         context.AddJavascriptSource("K2BAccordionMenu/K2BAccordionMenuRender.js", "", false);
         context.AddJavascriptSource("k2bwwmasterpageflatcompact.js", "?201811171524610", false);
         context.WriteHtmlTextNl( "</body>") ;
         context.WriteHtmlTextNl( "</html>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
      }

      public override String GetPgmname( )
      {
         return "K2BWWMasterPageFlatCompact" ;
      }

      public override String GetPgmdesc( )
      {
         return "K2 BWWMaster Page Flat Compact" ;
      }

      protected void WB0I0( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            RenderHtmlHeaders( ) ;
            RenderHtmlOpenForm( ) ;
            if ( ! ShowMPWhenPopUp( ) && context.isPopUpObject( ) )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableOutput();
               }
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               /* Content placeholder */
               context.WriteHtmlText( "<div") ;
               GxWebStd.ClassAttribute( context, "gx-content-placeholder");
               context.WriteHtmlText( ">") ;
               if ( ! isFullAjaxMode( ) )
               {
                  getDataAreaObject().RenderHtmlContent();
               }
               context.WriteHtmlText( "</div>") ;
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
               wbLoad = true;
               return  ;
            }
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMaintable_Internalname, 1, 0, "px", 0, "px", "MainContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divHeader_Internalname, 1, 0, "px", 0, "px", "ContainerFluid K2BHeader", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-2 ToggleCell", "left", "top", "", "", "div");
            wb_table1_9_0I2( true) ;
         }
         else
         {
            wb_table1_9_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table1_9_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-11 hidden-xs col-sm-10", "left", "top", "", "", "div");
            wb_table2_16_0I2( true) ;
         }
         else
         {
            wb_table2_16_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table2_16_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 PromptAdvancedBarCellCompact", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMenucontainer_Internalname, 1, 0, "px", 0, "px", divMenucontainer_Class, "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* User Defined Control */
            context.WriteHtmlText( "<div class=\"gx_usercontrol\" id=\""+"K2BACCORDIONMENU1_MPAGEContainer"+"\"></div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divContent_Internalname, 1, 0, "px", 0, "px", "BodyContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            /* Content placeholder */
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gx-content-placeholder");
            context.WriteHtmlText( ">") ;
            if ( ! isFullAjaxMode( ) )
            {
               getDataAreaObject().RenderHtmlContent();
            }
            context.WriteHtmlText( "</div>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START0I2( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         wbErr = false;
         STRUP0I0( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( getDataAreaObject().ExecuteStartEvent() != 0 )
            {
               setAjaxCallMode();
            }
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      protected void WS0I2( )
      {
         START0I2( ) ;
         EVT0I2( ) ;
      }

      protected void EVT0I2( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               sEvt = cgiGet( "_EventName");
               EvtGridId = cgiGet( "_EventGridId");
               EvtRowId = cgiGet( "_EventRowId");
               if ( StringUtil.Len( sEvt) > 0 )
               {
                  sEvtType = StringUtil.Left( sEvt, 1);
                  sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "RFR_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "START_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Start */
                           E110I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "REFRESH_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Refresh */
                           E120I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LOAD_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           /* Execute user event: Load */
                           E130I2 ();
                        }
                        else if ( StringUtil.StrCmp(sEvt, "ENTER_MPAGE") == 0 )
                        {
                           context.wbHandled = 1;
                           if ( ! wbErr )
                           {
                              Rfr0gs = false;
                              if ( ! Rfr0gs )
                              {
                              }
                              dynload_actions( ) ;
                           }
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           dynload_actions( ) ;
                           dynload_actions( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  if ( context.wbHandled == 0 )
                  {
                     getDataAreaObject().DispatchEvents();
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void WE0I2( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm0I2( ) ;
            }
         }
      }

      protected void PA0I2( )
      {
         if ( nDonePA == 0 )
         {
            if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
            {
               gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSearchcriteria_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF0I2( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV17Pgmname = "K2BWWMasterPageFlatCompact";
         context.Gx_err = 0;
      }

      protected void RF0I2( )
      {
         initialize_formulas( ) ;
         if ( ShowMPWhenPopUp( ) || ! context.isPopUpObject( ) )
         {
            /* Execute user event: Refresh */
            E120I2 ();
            fix_multi_value_controls( ) ;
         }
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E130I2 ();
            WB0I0( ) ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
      }

      protected void send_integrity_lvl_hashes0I2( )
      {
      }

      protected void STRUP0I0( )
      {
         /* Before Start, stand alone formulas. */
         AV17Pgmname = "K2BWWMasterPageFlatCompact";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E110I2 ();
         context.wbGlbDoneStart = 1;
         /* After Start, stand alone formulas. */
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read saved SDTs. */
            ajax_req_read_hidden_sdt(cgiGet( "vMENUITEMS_MPAGE"), AV9MenuItems);
            /* Read variables values. */
            /* Read saved values. */
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E110I2 ();
         if (returnInSub) return;
      }

      protected void E110I2( )
      {
         /* Start Routine */
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("viewport", "width=device-width; initial-scale=1.0; maximum-scale=1.0; user-scalable=0;", 0) ;
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Meta.addItem("apple-mobile-web-app-capable", "yes", 0) ;
         divMyaccounttable_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, divMyaccounttable_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divMyaccounttable_Visible), 5, 0)), true);
         new k2blistprograms(context ).execute( out  AV8ListPrograms) ;
         GXt_objcol_SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem1 = AV9MenuItems;
         new k2blistprogramstomultilevelmenusdt(context ).execute(  AV8ListPrograms, out  GXt_objcol_SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem1) ;
         AV9MenuItems = GXt_objcol_SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem1;
         GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem2 = AV12SearchableTransactions;
         new k2bgetsearchableentities(context ).execute( out  GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem2) ;
         AV12SearchableTransactions = GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem2;
         if ( new k2bisauthorizedactivityname(context).executeUdp(  "",  "",  "None",  "K2BToolsSearchResult",  AV17Pgmname) && ( AV12SearchableTransactions.Count > 0 ) )
         {
            GXt_char3 = AV14SearchCriteriaSession;
            new k2bsessionget(context ).execute(  AV17Pgmname+"-SearchCriteria", out  GXt_char3) ;
            AV14SearchCriteriaSession = GXt_char3;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14SearchCriteriaSession)) )
            {
               AV13SearchCriteria = AV14SearchCriteriaSession;
               context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV13SearchCriteria", AV13SearchCriteria);
            }
         }
         else
         {
            tblSearchcontainer_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", true, tblSearchcontainer_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(tblSearchcontainer_Visible), 5, 0)), true);
         }
         GXt_char3 = "";
         new k2bgetusercaption(context ).execute( out  GXt_char3) ;
         lblUsername_Caption = GXt_char3;
         context.httpAjaxContext.ajax_rsp_assign_prop("", true, lblUsername_Internalname, "Caption", lblUsername_Caption, true);
      }

      protected void E120I2( )
      {
         /* Refresh Routine */
         if ( ! new k2bisauthenticated(context).executeUdp( ) )
         {
            CallWebObject(formatLink("k2bnotauthorized.aspx") + "?" + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("None")) + "," + UrlEncode(StringUtil.RTrim("")) + "," + UrlEncode(StringUtil.RTrim("")));
            context.wjLocDisableFrm = 1;
         }
         GXt_char3 = AV14SearchCriteriaSession;
         new k2bsessionget(context ).execute(  AV17Pgmname+"-SearchCriteria", out  GXt_char3) ;
         AV14SearchCriteriaSession = GXt_char3;
         if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV14SearchCriteriaSession)) )
         {
            AV13SearchCriteria = AV14SearchCriteriaSession;
            context.httpAjaxContext.ajax_rsp_assign_attri("", true, "AV13SearchCriteria", AV13SearchCriteria);
         }
         /*  Sending Event outputs  */
      }

      protected void nextLoad( )
      {
      }

      protected void E130I2( )
      {
         /* Load Routine */
      }

      protected void wb_table2_16_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable1_Internalname, tblTable1_Internalname, "", "K2BToolsTable_FloatRight", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            wb_table3_19_0I2( true) ;
         }
         else
         {
            wb_table3_19_0I2( false) ;
         }
         return  ;
      }

      protected void wb_table3_19_0I2e( bool wbgen )
      {
         if ( wbgen )
         {
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablemyaccount_Internalname, 1, 0, "px", 0, "px", "K2BToolsTable_MyAccountContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblMyaccount_Internalname, "My account &#x25BC;", "", "", lblMyaccount_Jsonclick, "'"+""+"'"+",true,"+"'"+"e140i1_client"+"'", "", "K2BToolsTextBlock_MyAccount", 7, "", 1, 1, 1, "HLP_K2BWWMasterPageFlatCompact.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divMyaccounttable_Internalname, divMyaccounttable_Visible, 0, "px", 0, "px", "K2BToolsMyAccountTable", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblUsername_Internalname, lblUsername_Caption, "", "", lblUsername_Jsonclick, "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "K2BToolsTextblock_UserName", 0, "", 1, 1, 0, "HLP_K2BWWMasterPageFlatCompact.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblChangepassword_Internalname, "Change Password", "", "", lblChangepassword_Jsonclick, "'"+""+"'"+",true,"+"'"+"e150i1_client"+"'", "", "K2BToolsTextBlock_ChangePassword", 7, "", 1, 1, 0, "HLP_K2BWWMasterPageFlatCompact.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblSignout_Internalname, "Sign Out", "", "", lblSignout_Jsonclick, "'"+""+"'"+",true,"+"'"+"e160i1_client"+"'", "", "K2BToolsTextBlock_Logout", 7, "", 1, 1, 0, "HLP_K2BWWMasterPageFlatCompact.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table2_16_0I2e( true) ;
         }
         else
         {
            wb_table2_16_0I2e( false) ;
         }
      }

      protected void wb_table3_19_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            if ( tblSearchcontainer_Visible == 0 )
            {
               sStyleString = sStyleString + "display:none;";
            }
            GxWebStd.gx_table_start( context, tblSearchcontainer_Internalname, tblSearchcontainer_Internalname, "", "K2BToolsTable_SearchContainer", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td>") ;
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", " gx-attribute", "left", "top", "", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtavSearchcriteria_Internalname, "Search Criteria", "col-sm-3 K2BTools_SearchCriteriaLabel", 0, true);
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 23,'',true,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSearchcriteria_Internalname, StringUtil.RTrim( AV13SearchCriteria), StringUtil.RTrim( context.localUtil.Format( AV13SearchCriteria, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,23);\"", "'"+""+"'"+",true,"+"'"+"E_MPAGE."+"'", "", "", "", "", edtavSearchcriteria_Jsonclick, 0, "K2BTools_SearchCriteria", "", "", "", "", 1, edtavSearchcriteria_Enabled, 0, "text", "", 26, "chr", 1, "row", 150, 0, 0, 0, 1, -1, -1, false, "K2BSearchCriteria", "left", true, "HLP_K2BWWMasterPageFlatCompact.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 25,'',true,'',0)\"";
            ClassString = "K2BToolsButton_Search";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttButton1_Internalname, "", "Search", bttButton1_Jsonclick, 7, "Search", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",true,"+"'"+"e170i1_client"+"'", TempTags, "", 2, "HLP_K2BWWMasterPageFlatCompact.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table3_19_0I2e( true) ;
         }
         else
         {
            wb_table3_19_0I2e( false) ;
         }
      }

      protected void wb_table1_9_0I2( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblTable2_Internalname, tblTable2_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td class='ToggleCell'>") ;
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',true,'',0)\"";
            ClassString = bttBtntogglemenu_Class;
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtntogglemenu_Internalname, "", "|||", bttBtntogglemenu_Jsonclick, 7, "", "", StyleString, ClassString, 1, 1, "standard", "'"+""+"'"+",true,"+"'"+"e180i1_client"+"'", TempTags, "", 2, "HLP_K2BWWMasterPageFlatCompact.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Static images/pictures */
            ClassString = "Image_HeaderLogo";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "bb4462ea-0eb3-44b4-8320-f971481f81b4", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage1_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", " "+"data-gx-image"+" ", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_K2BWWMasterPageFlatCompact.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_9_0I2e( true) ;
         }
         else
         {
            wb_table1_9_0I2e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
      }

      public override String getresponse( String sGXDynURL )
      {
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA0I2( ) ;
         WS0I2( ) ;
         WE0I2( ) ;
         this.cleanup();
         context.SetWrapped(false);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void master_styles( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("Shared/font-awesome-4.4.0/css/font-awesome.min.css", "");
         AddStyleSheetFile("K2BAccordionMenu/metisMenu-1.1.3/dist/metisMenu.min.css", "");
         AddStyleSheetFile("K2BAccordionMenu/metisMenu-1.1.3/k2btoolsresources/metisFolder.css", "");
         AddStyleSheetFile("K2BAccordionMenu/metisMenu-1.1.3/k2btoolsresources/defaultTheme.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)(getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Jscriptsrc.Item(idxLst))), "?201811171524651", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("k2bwwmasterpageflatcompact.js", "?201811171524651", false);
         context.AddJavascriptSource("K2BAccordionMenu/metisMenu-1.1.3/dist/metisMenu.min.js", "", false);
         context.AddJavascriptSource("K2BAccordionMenu/K2BAccordionMenuRender.js", "", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         bttBtntogglemenu_Internalname = "BTNTOGGLEMENU_MPAGE";
         imgImage1_Internalname = "IMAGE1_MPAGE";
         tblTable2_Internalname = "TABLE2_MPAGE";
         edtavSearchcriteria_Internalname = "vSEARCHCRITERIA_MPAGE";
         bttButton1_Internalname = "BUTTON1_MPAGE";
         tblSearchcontainer_Internalname = "SEARCHCONTAINER_MPAGE";
         lblMyaccount_Internalname = "MYACCOUNT_MPAGE";
         lblUsername_Internalname = "USERNAME_MPAGE";
         lblChangepassword_Internalname = "CHANGEPASSWORD_MPAGE";
         lblSignout_Internalname = "SIGNOUT_MPAGE";
         divMyaccounttable_Internalname = "MYACCOUNTTABLE_MPAGE";
         divTablemyaccount_Internalname = "TABLEMYACCOUNT_MPAGE";
         tblTable1_Internalname = "TABLE1_MPAGE";
         divHeader_Internalname = "HEADER_MPAGE";
         K2baccordionmenu1_Internalname = "K2BACCORDIONMENU1_MPAGE";
         divMenucontainer_Internalname = "MENUCONTAINER_MPAGE";
         divContent_Internalname = "CONTENT_MPAGE";
         divMaintable_Internalname = "MAINTABLE_MPAGE";
         (getDataAreaObject() == null ? Form : getDataAreaObject().GetForm()).Internalname = "FORM_MPAGE";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         edtavSearchcriteria_Jsonclick = "";
         edtavSearchcriteria_Enabled = 1;
         divMyaccounttable_Visible = 1;
         lblUsername_Caption = "User Name";
         tblSearchcontainer_Visible = 1;
         bttBtntogglemenu_Class = "K2BToolsButton_BtnToggleCompact";
         divMenucontainer_Class = "K2BToolsMenuContainerInvisibleCompact";
         Contentholder.setDataArea(getDataAreaObject());
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH_MPAGE","{handler:'Refresh',iparms:[{av:'AV17Pgmname',fld:'vPGMNAME_MPAGE',pic:'',nv:''}],oparms:[{av:'AV13SearchCriteria',fld:'vSEARCHCRITERIA_MPAGE',pic:'',nv:''}]}");
         setEventMetadata("DOSEARCH_MPAGE","{handler:'E170I1',iparms:[{av:'AV13SearchCriteria',fld:'vSEARCHCRITERIA_MPAGE',pic:'',nv:''}],oparms:[{av:'AV13SearchCriteria',fld:'vSEARCHCRITERIA_MPAGE',pic:'',nv:''}]}");
         setEventMetadata("OPENTABLE_MPAGE","{handler:'E140I1',iparms:[{av:'divMyaccounttable_Visible',ctrl:'MYACCOUNTTABLE_MPAGE',prop:'Visible'}],oparms:[{av:'divMyaccounttable_Visible',ctrl:'MYACCOUNTTABLE_MPAGE',prop:'Visible'}]}");
         setEventMetadata("TOGGLEMENU_MPAGE","{handler:'E180I1',iparms:[{av:'divMenucontainer_Class',ctrl:'MENUCONTAINER_MPAGE',prop:'Class'},{ctrl:'BTNTOGGLEMENU_MPAGE',prop:'Class'}],oparms:[{av:'divMenucontainer_Class',ctrl:'MENUCONTAINER_MPAGE',prop:'Class'},{ctrl:'BTNTOGGLEMENU_MPAGE',prop:'Class'}]}");
         setEventMetadata("CHANGEPASSWORD_MPAGE","{handler:'E150I1',iparms:[],oparms:[]}");
         setEventMetadata("SIGNOUT_MPAGE","{handler:'E160I1',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         Contentholder = new GXDataAreaControl();
         AV17Pgmname = "";
         GXKey = "";
         AV9MenuItems = new GXBaseCollection<SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem>( context, "K2BMultiLevelMenuItem", "PACYE2");
         sPrefix = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         GX_FocusControl = "";
         AV8ListPrograms = new GXBaseCollection<SdtK2BProgramNames_ProgramName>( context, "ProgramName", "PACYE2");
         GXt_objcol_SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem1 = new GXBaseCollection<SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem>( context, "K2BMultiLevelMenuItem", "PACYE2");
         AV12SearchableTransactions = new GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem>( context, "SearchableTransactionsItem", "PACYE2");
         GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem2 = new GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem>( context, "SearchableTransactionsItem", "PACYE2");
         AV14SearchCriteriaSession = "";
         AV13SearchCriteria = "";
         GXt_char3 = "";
         sStyleString = "";
         lblMyaccount_Jsonclick = "";
         lblUsername_Jsonclick = "";
         lblChangepassword_Jsonclick = "";
         lblSignout_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttButton1_Jsonclick = "";
         bttBtntogglemenu_Jsonclick = "";
         sImgUrl = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sDynURL = "";
         Form = new GXWebForm();
         AV17Pgmname = "K2BWWMasterPageFlatCompact";
         /* GeneXus formulas. */
         AV17Pgmname = "K2BWWMasterPageFlatCompact";
         context.Gx_err = 0;
      }

      private short initialized ;
      private short GxWebError ;
      private short wbEnd ;
      private short wbStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGotPars ;
      private short nGXWrapped ;
      private int divMyaccounttable_Visible ;
      private int tblSearchcontainer_Visible ;
      private int edtavSearchcriteria_Enabled ;
      private int idxLst ;
      private String divMenucontainer_Class ;
      private String bttBtntogglemenu_Class ;
      private String AV17Pgmname ;
      private String GXKey ;
      private String sPrefix ;
      private String divMaintable_Internalname ;
      private String divHeader_Internalname ;
      private String divMenucontainer_Internalname ;
      private String divContent_Internalname ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String GX_FocusControl ;
      private String edtavSearchcriteria_Internalname ;
      private String divMyaccounttable_Internalname ;
      private String AV14SearchCriteriaSession ;
      private String AV13SearchCriteria ;
      private String tblSearchcontainer_Internalname ;
      private String lblUsername_Caption ;
      private String lblUsername_Internalname ;
      private String GXt_char3 ;
      private String sStyleString ;
      private String tblTable1_Internalname ;
      private String divTablemyaccount_Internalname ;
      private String lblMyaccount_Internalname ;
      private String lblMyaccount_Jsonclick ;
      private String lblUsername_Jsonclick ;
      private String lblChangepassword_Internalname ;
      private String lblChangepassword_Jsonclick ;
      private String lblSignout_Internalname ;
      private String lblSignout_Jsonclick ;
      private String TempTags ;
      private String edtavSearchcriteria_Jsonclick ;
      private String ClassString ;
      private String StyleString ;
      private String bttButton1_Internalname ;
      private String bttButton1_Jsonclick ;
      private String tblTable2_Internalname ;
      private String bttBtntogglemenu_Internalname ;
      private String bttBtntogglemenu_Jsonclick ;
      private String sImgUrl ;
      private String imgImage1_Internalname ;
      private String sDynURL ;
      private String K2baccordionmenu1_Internalname ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool toggleJsOutput ;
      private bool returnInSub ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXDataAreaControl Contentholder ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXBaseCollection<SdtK2BProgramNames_ProgramName> AV8ListPrograms ;
      private GXBaseCollection<SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem> AV9MenuItems ;
      private GXBaseCollection<SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem> GXt_objcol_SdtK2BMultiLevelMenu_K2BMultiLevelMenuItem1 ;
      private GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem> AV12SearchableTransactions ;
      private GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem> GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem2 ;
      private GXWebForm Form ;
   }

}
