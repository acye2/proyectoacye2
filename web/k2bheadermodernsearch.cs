/*
               File: K2BHeaderModernSearch
        Description: Application's header
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:2:27.74
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class k2bheadermodernsearch : GXWebComponent, System.Web.SessionState.IRequiresSessionState
   {
      public k2bheadermodernsearch( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BModern");
         }
      }

      public k2bheadermodernsearch( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( String aP0_MainPgmName ,
                           String aP1_MainPgmDesc ,
                           String aP2_FormCaption ,
                           bool aP3_includeInStack )
      {
         this.AV16MainPgmName = aP0_MainPgmName;
         this.AV15MainPgmDesc = aP1_MainPgmDesc;
         this.AV8FormCaption = aP2_FormCaption;
         this.AV9includeInStack = aP3_includeInStack;
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      public override void SetPrefix( String sPPrefix )
      {
         sPrefix = sPPrefix;
      }

      protected override void createObjects( )
      {
      }

      protected void INITWEB( )
      {
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            context.SetDefaultTheme("K2BModern");
         }
         initialize_properties( ) ;
         if ( StringUtil.Len( (String)(sPrefix)) == 0 )
         {
            if ( nGotPars == 0 )
            {
               entryPointCalled = false;
               gxfirstwebparm = GetNextPar( );
               gxfirstwebparm_bkp = gxfirstwebparm;
               gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
               if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
               {
                  setAjaxCallMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  dyncall( GetNextPar( )) ;
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "dyncomponent") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  nDynComponent = 1;
                  sCompPrefix = GetNextPar( );
                  sSFPrefix = GetNextPar( );
                  AV16MainPgmName = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16MainPgmName", AV16MainPgmName);
                  AV15MainPgmDesc = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15MainPgmDesc", AV15MainPgmDesc);
                  AV8FormCaption = GetNextPar( );
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
                  AV9includeInStack = (bool)(BooleanUtil.Val(GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9includeInStack", AV9includeInStack);
                  setjustcreated();
                  componentprepare(new Object[] {(String)sCompPrefix,(String)sSFPrefix,(String)AV16MainPgmName,(String)AV15MainPgmDesc,(String)AV8FormCaption,(bool)AV9includeInStack});
                  componentstart();
                  context.httpAjaxContext.ajax_rspStartCmp(sPrefix);
                  componentdraw();
                  context.httpAjaxContext.ajax_rspEndCmp();
                  return  ;
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
               {
                  setAjaxEventMode();
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
               {
                  if ( ! IsValidAjaxCall( true) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = GetNextPar( );
               }
               else
               {
                  if ( ! IsValidAjaxCall( false) )
                  {
                     GxWebError = 1;
                     return  ;
                  }
                  gxfirstwebparm = gxfirstwebparm_bkp;
               }
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.IsLocalStorageSupported( ) )
            {
               context.PushCurrentUrl();
            }
         }
      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITWEB( ) ;
         if ( ! isAjaxCallMode( ) )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               ValidateSpaRequest();
            }
            PA142( ) ;
            if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
            {
               /* GeneXus formulas. */
               AV23Pgmname = "K2BHeaderModernSearch";
               context.Gx_err = 0;
               WS142( ) ;
               if ( ! isAjaxCallMode( ) )
               {
                  if ( nDynComponent == 0 )
                  {
                     throw new System.Net.WebException("WebComponent is not allowed to run") ;
                  }
               }
            }
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, false);
      }

      protected void RenderHtmlOpenForm( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            context.WriteHtmlText( "<title>") ;
            context.SendWebValue( "Application's header") ;
            context.WriteHtmlTextNl( "</title>") ;
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            if ( StringUtil.Len( sDynURL) > 0 )
            {
               context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
            }
            define_styles( ) ;
         }
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811171522781", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.CloseHtmlHeader();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            FormProcess = " data-HasEnter=\"false\" data-Skiponenter=\"false\"";
            context.WriteHtmlText( "<body ") ;
            bodyStyle = "background-color: #efefef;";
            if ( nGXWrapped == 0 )
            {
               bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
            }
            context.WriteHtmlText( " "+"class=\"K2BForm\""+" "+ "style='"+bodyStyle+"'") ;
            context.WriteHtmlText( FormProcess+">") ;
            context.skipLines(1);
            context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"K2BForm\" data-gx-class=\"K2BForm\" novalidate action=\""+formatLink("k2bheadermodernsearch.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV16MainPgmName)) + "," + UrlEncode(StringUtil.RTrim(AV15MainPgmDesc)) + "," + UrlEncode(StringUtil.RTrim(AV8FormCaption)) + "," + UrlEncode(StringUtil.BoolToStr(AV9includeInStack))+"\">") ;
            GxWebStd.gx_hidden_field( context, "_EventName", "");
            GxWebStd.gx_hidden_field( context, "_EventGridId", "");
            GxWebStd.gx_hidden_field( context, "_EventRowId", "");
            context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, "FORM", "Class", "K2BForm", true);
         }
         else
         {
            bool toggleHtmlOutput = isOutputEnabled( ) ;
            if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableOutput();
               }
            }
            context.WriteHtmlText( "<div") ;
            GxWebStd.ClassAttribute( context, "gxwebcomponent-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "K2BForm" : Form.Class)+"-fx");
            context.WriteHtmlText( ">") ;
            if ( toggleHtmlOutput )
            {
               if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableOutput();
                  }
               }
            }
            toggleJsOutput = isJsOutputEnabled( );
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         if ( StringUtil.StringSearch( sPrefix, "MP", 1) == 1 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV16MainPgmName", StringUtil.RTrim( wcpOAV16MainPgmName));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV15MainPgmDesc", StringUtil.RTrim( wcpOAV15MainPgmDesc));
         GxWebStd.gx_hidden_field( context, sPrefix+"wcpOAV8FormCaption", StringUtil.RTrim( wcpOAV8FormCaption));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"wcpOAV9includeInStack", wcpOAV9includeInStack);
         GxWebStd.gx_hidden_field( context, sPrefix+"vPGMNAME", StringUtil.RTrim( AV23Pgmname));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMAINPGMNAME", StringUtil.RTrim( AV16MainPgmName));
         GxWebStd.gx_hidden_field( context, sPrefix+"vMAINPGMDESC", StringUtil.RTrim( AV15MainPgmDesc));
         GxWebStd.gx_hidden_field( context, sPrefix+"vFORMCAPTION", StringUtil.RTrim( AV8FormCaption));
         GxWebStd.gx_boolean_hidden_field( context, sPrefix+"vINCLUDEINSTACK", AV9includeInStack);
      }

      protected void RenderHtmlCloseForm142( )
      {
         SendCloseFormHiddens( ) ;
         if ( ( StringUtil.Len( sPrefix) != 0 ) && ( context.isAjaxRequest( ) || context.isSpaRequest( ) ) )
         {
            context.AddJavascriptSource("k2bheadermodernsearch.js", "?201811171522784", false);
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"GX_FocusControl", GX_FocusControl);
         define_styles( ) ;
         SendSecurityToken(sPrefix);
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            SendAjaxEncryptionKey();
            SendComponentObjects();
            SendServerCommands();
            SendState();
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
            context.WriteHtmlTextNl( "</form>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
            include_jscripts( ) ;
            context.WriteHtmlTextNl( "</body>") ;
            context.WriteHtmlTextNl( "</html>") ;
            if ( context.isSpaRequest( ) )
            {
               enableOutput();
            }
         }
         else
         {
            SendWebComponentState();
            context.WriteHtmlText( "</div>") ;
            if ( toggleJsOutput )
            {
               if ( context.isSpaRequest( ) )
               {
                  enableJsOutput();
               }
            }
         }
      }

      public override String GetPgmname( )
      {
         return "K2BHeaderModernSearch" ;
      }

      public override String GetPgmdesc( )
      {
         return "Application's header" ;
      }

      protected void WB140( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! wbLoad )
         {
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               RenderHtmlHeaders( ) ;
            }
            RenderHtmlOpenForm( ) ;
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               GxWebStd.gx_hidden_field( context, sPrefix+"_CMPPGM", "k2bheadermodernsearch.aspx");
            }
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, "", "", sPrefix, "false");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section_Header", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divBackground_header_Internalname, 1, 0, "px", 56, "px", "Background_Header", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section_Basic_FloatLeft", "left", "top", "", "", "div");
            /* Static images/pictures */
            ClassString = "Image_HeaderLogo";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "bb4462ea-0eb3-44b4-8320-f971481f81b4", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage1_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_K2BHeaderModernSearch.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divRightoptions_Internalname, 1, 0, "px", 0, "px", "Section_Basic_FloatRight", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section_HeaderActions", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 8,'" + sPrefix + "',false,'',0)\"";
            ClassString = "Button_HeaderClose";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttK2bcloseheader_Internalname, "", "", bttK2bcloseheader_Jsonclick, 5, bttK2bcloseheader_Tooltiptext, "", StyleString, ClassString, 1, 1, "standard", "'"+sPrefix+"'"+",false,"+"'"+sPrefix+"E\\'K2BCLOSE\\'."+"'", TempTags, "", context.GetButtonType( ), "HLP_K2BHeaderModernSearch.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divSearchcontainer_Internalname, divSearchcontainer_Visible, 0, "px", 0, "px", "Section_SearchContainer", "left", "top", "", "", "div");
            wb_table1_10_142( true) ;
         }
         else
         {
            wb_table1_10_142( false) ;
         }
         return  ;
      }

      protected void wb_table1_10_142e( bool wbgen )
      {
         if ( wbgen )
         {
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblUsermenu_Internalname, "", "", "", lblUsermenu_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "TextBlock", 0, "", 1, 1, 2, "HLP_K2BHeaderModernSearch.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
         }
         wbLoad = true;
      }

      protected void START142( )
      {
         wbLoad = false;
         wbEnd = 0;
         wbStart = 0;
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( ! context.isSpaRequest( ) )
            {
               Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
               Form.Meta.addItem("description", "Application's header", 0) ;
            }
            context.wjLoc = "";
            context.nUserReturn = 0;
            context.wbHandled = 0;
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               sXEvt = cgiGet( "_EventName");
               if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
               {
               }
            }
         }
         wbErr = false;
         if ( ( StringUtil.Len( sPrefix) == 0 ) || ( nDraw == 1 ) )
         {
            if ( nDoneStart == 0 )
            {
               STRUP140( ) ;
            }
         }
      }

      protected void WS142( )
      {
         START142( ) ;
         EVT142( ) ;
      }

      protected void EVT142( )
      {
         sXEvt = cgiGet( "_EventName");
         if ( ( ( ( StringUtil.Len( sPrefix) == 0 ) ) || ( StringUtil.StringSearch( sXEvt, sPrefix, 1) > 0 ) ) && ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) && ! wbErr )
            {
               /* Read Web Panel buttons. */
               if ( context.wbHandled == 0 )
               {
                  if ( StringUtil.Len( sPrefix) == 0 )
                  {
                     sEvt = cgiGet( "_EventName");
                     EvtGridId = cgiGet( "_EventGridId");
                     EvtRowId = cgiGet( "_EventRowId");
                  }
                  if ( StringUtil.Len( sEvt) > 0 )
                  {
                     sEvtType = StringUtil.Left( sEvt, 1);
                     sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
                     if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                     {
                        sEvtType = StringUtil.Right( sEvt, 1);
                        if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                        {
                           sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                           if ( StringUtil.StrCmp(sEvt, "RFR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "START") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Start */
                                    E11142 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "REFRESH") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Refresh */
                                    E12142 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "'K2BCLOSE'") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: 'K2BClose' */
                                    E13142 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "VSEARCHCRITERIA.ISVALID") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    E14142 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LOAD") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    /* Execute user event: Load */
                                    E15142 ();
                                 }
                              }
                           }
                           else if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    if ( ! wbErr )
                                    {
                                       Rfr0gs = false;
                                       if ( ! Rfr0gs )
                                       {
                                       }
                                       dynload_actions( ) ;
                                    }
                                 }
                              }
                              /* No code required for Cancel button. It is implemented as the Reset button. */
                           }
                           else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                           {
                              if ( ( StringUtil.Len( sPrefix) != 0 ) && ( nDoneStart == 0 ) )
                              {
                                 STRUP140( ) ;
                              }
                              if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
                              {
                                 context.wbHandled = 1;
                                 if ( ! wbErr )
                                 {
                                    dynload_actions( ) ;
                                    GX_FocusControl = edtavSearchcriteria_Internalname;
                                    context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
                                 }
                              }
                              dynload_actions( ) ;
                           }
                        }
                        else
                        {
                        }
                     }
                     context.wbHandled = 1;
                  }
               }
            }
         }
      }

      protected void WE142( )
      {
         if ( ! GxWebStd.gx_redirect( context) )
         {
            Rfr0gs = true;
            Refresh( ) ;
            if ( ! GxWebStd.gx_redirect( context) )
            {
               RenderHtmlCloseForm142( ) ;
            }
         }
      }

      protected void PA142( )
      {
         if ( nDonePA == 0 )
         {
            if ( StringUtil.Len( sPrefix) != 0 )
            {
               initialize_properties( ) ;
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
               {
                  gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
               }
            }
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
            toggleJsOutput = isJsOutputEnabled( );
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( context.isSpaRequest( ) )
               {
                  disableJsOutput();
               }
            }
            if ( StringUtil.Len( sPrefix) == 0 )
            {
               if ( toggleJsOutput )
               {
                  if ( context.isSpaRequest( ) )
                  {
                     enableJsOutput();
                  }
               }
            }
            if ( ! context.isAjaxRequest( ) )
            {
               GX_FocusControl = edtavSearchcriteria_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "GX_FocusControl", GX_FocusControl);
            }
            nDonePA = 1;
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void send_integrity_hashes( )
      {
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
      }

      public void Refresh( )
      {
         send_integrity_hashes( ) ;
         RF142( ) ;
         if ( isFullAjaxMode( ) )
         {
            send_integrity_footer_hashes( ) ;
         }
      }

      protected void initialize_formulas( )
      {
         /* GeneXus formulas. */
         AV23Pgmname = "K2BHeaderModernSearch";
         context.Gx_err = 0;
      }

      protected void RF142( )
      {
         initialize_formulas( ) ;
         /* Execute user event: Refresh */
         E12142 ();
         fix_multi_value_controls( ) ;
         if ( ! context.WillRedirect( ) && ( context.nUserReturn != 1 ) )
         {
            /* Execute user event: Load */
            E15142 ();
            WB140( ) ;
         }
      }

      protected void send_integrity_lvl_hashes142( )
      {
      }

      protected void STRUP140( )
      {
         /* Before Start, stand alone formulas. */
         AV23Pgmname = "K2BHeaderModernSearch";
         context.Gx_err = 0;
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         /* Execute user event: Start */
         E11142 ();
         context.wbGlbDoneStart = 1;
         nDoneStart = 1;
         /* After Start, stand alone formulas. */
         sXEvt = cgiGet( "_EventName");
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) )
         {
            /* Read saved SDTs. */
            /* Read variables values. */
            AV17SearchCriteria = cgiGet( edtavSearchcriteria_Internalname);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17SearchCriteria", AV17SearchCriteria);
            /* Read saved values. */
            wcpOAV16MainPgmName = cgiGet( sPrefix+"wcpOAV16MainPgmName");
            wcpOAV15MainPgmDesc = cgiGet( sPrefix+"wcpOAV15MainPgmDesc");
            wcpOAV8FormCaption = cgiGet( sPrefix+"wcpOAV8FormCaption");
            wcpOAV9includeInStack = StringUtil.StrToBool( cgiGet( sPrefix+"wcpOAV9includeInStack"));
            /* Read subfile selected row values. */
            /* Read hidden variables. */
            GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         }
         else
         {
            dynload_actions( ) ;
         }
      }

      protected void GXStart( )
      {
         /* Execute user event: Start */
         E11142 ();
         if (returnInSub) return;
      }

      protected void E11142( )
      {
         /* Start Routine */
         new k2bgetcontext(context ).execute( out  AV6Context) ;
         bttK2bcloseheader_Tooltiptext = "Close";
         context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, bttK2bcloseheader_Internalname, "Tooltiptext", bttK2bcloseheader_Tooltiptext, true);
         GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem1 = AV20SearchableTransactions;
         new k2bgetsearchableentities(context ).execute( out  GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem1) ;
         AV20SearchableTransactions = GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem1;
         if ( new k2bisauthorizedactivityname(context).executeUdp(  "",  "",  "None",  "K2BToolsSearchResult",  AV23Pgmname) && ( AV20SearchableTransactions.Count > 0 ) )
         {
            GXt_char2 = AV18SearchCriteriaSession;
            new k2bsessionget(context ).execute(  AV23Pgmname+"-SearchCriteria", out  GXt_char2) ;
            AV18SearchCriteriaSession = GXt_char2;
            if ( ! String.IsNullOrEmpty(StringUtil.RTrim( AV18SearchCriteriaSession)) )
            {
               AV17SearchCriteria = AV18SearchCriteriaSession;
               context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17SearchCriteria", AV17SearchCriteria);
            }
         }
         else
         {
            divSearchcontainer_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop(sPrefix, false, divSearchcontainer_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(divSearchcontainer_Visible), 5, 0)), true);
         }
      }

      protected void E12142( )
      {
         /* Refresh Routine */
      }

      protected void E13142( )
      {
         /* 'K2BClose' Routine */
         new k2bstackgoback(context ).execute( out  AV10K2BUrl) ;
         CallWebObject(formatLink(AV10K2BUrl) );
         context.wjLocDisableFrm = 0;
      }

      protected void E14142( )
      {
         /* Searchcriteria_Isvalid Routine */
         new k2bsessionset(context ).execute(  AV23Pgmname+"-SearchCriteria",  AV17SearchCriteria) ;
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV17SearchCriteria", AV17SearchCriteria);
         CallWebObject(formatLink("k2btoolssearchresult.aspx") + "?" + UrlEncode(StringUtil.RTrim(AV17SearchCriteria)) + "," + UrlEncode(StringUtil.RTrim("")));
         context.wjLocDisableFrm = 1;
         /*  Sending Event outputs  */
      }

      protected void nextLoad( )
      {
      }

      protected void E15142( )
      {
         /* Load Routine */
      }

      protected void wb_table1_10_142( bool wbgen )
      {
         if ( wbgen )
         {
            /* Table start */
            sStyleString = "";
            GxWebStd.gx_table_start( context, tblSearchtable_Internalname, tblSearchtable_Internalname, "", "Table", 0, "", "", 1, 2, sStyleString, "", 0);
            context.WriteHtmlText( "<tbody>") ;
            context.WriteHtmlText( "<tr>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:36px")+"\">") ;
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblWelcomemessage3_Internalname, "Search", "", "", lblWelcomemessage3_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+""+"'", "color:#FFFFFF;", "TextBlock", 0, "", 1, 1, 0, "HLP_K2BHeaderModernSearch.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td style=\""+CSSHelper.Prettify( "width:184px")+"\">") ;
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 15,'" + sPrefix + "',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtavSearchcriteria_Internalname, StringUtil.RTrim( AV17SearchCriteria), StringUtil.RTrim( context.localUtil.Format( AV17SearchCriteria, "")), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,15);\"", "'"+sPrefix+"'"+",false,"+"'"+""+"'", "", "", "", "", edtavSearchcriteria_Jsonclick, 0, "Attribute", "", "", "", "", 1, 1, 0, "text", "", 30, "chr", 1, "row", 150, 0, 0, 0, 1, -1, -1, true, "K2BSearchCriteria", "left", true, "HLP_K2BHeaderModernSearch.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "<td>") ;
            /* Active images/pictures */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 17,'" + sPrefix + "',false,'',0)\"";
            ClassString = "K2BActionGridImage";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "b2f44a32-cd24-4170-a5da-849c2e18a0bd", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgImage2_Internalname, sImgUrl, "", "", "", context.GetTheme( ), 1, 1, "", "", 0, 0, 0, "px", 0, "px", 0, 0, 7, imgImage2_Jsonclick, "'"+sPrefix+"'"+",false,"+"'"+"e16141_client"+"'", StyleString, ClassString, "", "", "", "", ""+TempTags, "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_K2BHeaderModernSearch.htm");
            context.WriteHtmlText( "</td>") ;
            context.WriteHtmlText( "</tr>") ;
            context.WriteHtmlText( "</tbody>") ;
            /* End of table */
            context.WriteHtmlText( "</table>") ;
            wb_table1_10_142e( true) ;
         }
         else
         {
            wb_table1_10_142e( false) ;
         }
      }

      public override void setparameters( Object[] obj )
      {
         createObjects();
         initialize();
         AV16MainPgmName = (String)getParm(obj,0);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16MainPgmName", AV16MainPgmName);
         AV15MainPgmDesc = (String)getParm(obj,1);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15MainPgmDesc", AV15MainPgmDesc);
         AV8FormCaption = (String)getParm(obj,2);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
         AV9includeInStack = (bool)getParm(obj,3);
         context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9includeInStack", AV9includeInStack);
      }

      public override String getresponse( String sGXDynURL )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            context.SetDefaultTheme("K2BModern");
         }
         initialize_properties( ) ;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         sDynURL = sGXDynURL;
         nGotPars = (short)(1);
         nGXWrapped = (short)(1);
         context.SetWrapped(true);
         PA142( ) ;
         WS142( ) ;
         WE142( ) ;
         this.cleanup();
         context.SetWrapped(false);
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
         return "";
      }

      public void responsestatic( String sGXDynURL )
      {
      }

      public override void componentbind( Object[] obj )
      {
         if ( IsUrlCreated( ) )
         {
            return  ;
         }
         sCtrlAV16MainPgmName = (String)((String)getParm(obj,0));
         sCtrlAV15MainPgmDesc = (String)((String)getParm(obj,1));
         sCtrlAV8FormCaption = (String)((String)getParm(obj,2));
         sCtrlAV9includeInStack = (String)((String)getParm(obj,3));
      }

      public override void componentrestorestate( String sPPrefix ,
                                                  String sPSFPrefix )
      {
         sPrefix = sPPrefix + sPSFPrefix;
         PA142( ) ;
         WCParametersGet( ) ;
      }

      public override void componentprepare( Object[] obj )
      {
         wbLoad = false;
         sCompPrefix = (String)getParm(obj,0);
         sSFPrefix = (String)getParm(obj,1);
         sPrefix = sCompPrefix + sSFPrefix;
         AddComponentObject(sPrefix, "k2bheadermodernsearch", GetJustCreated( ));
         if ( ( nDoneStart == 0 ) && ( nDynComponent == 0 ) )
         {
            INITWEB( ) ;
         }
         else
         {
            init_default_properties( ) ;
         }
         PA142( ) ;
         if ( ! GetJustCreated( ) && ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 ) && ( context.wbGlbDoneStart == 0 ) )
         {
            WCParametersGet( ) ;
         }
         else
         {
            AV16MainPgmName = (String)getParm(obj,2);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16MainPgmName", AV16MainPgmName);
            AV15MainPgmDesc = (String)getParm(obj,3);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15MainPgmDesc", AV15MainPgmDesc);
            AV8FormCaption = (String)getParm(obj,4);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
            AV9includeInStack = (bool)getParm(obj,5);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9includeInStack", AV9includeInStack);
         }
         wcpOAV16MainPgmName = cgiGet( sPrefix+"wcpOAV16MainPgmName");
         wcpOAV15MainPgmDesc = cgiGet( sPrefix+"wcpOAV15MainPgmDesc");
         wcpOAV8FormCaption = cgiGet( sPrefix+"wcpOAV8FormCaption");
         wcpOAV9includeInStack = StringUtil.StrToBool( cgiGet( sPrefix+"wcpOAV9includeInStack"));
         if ( ! GetJustCreated( ) && ( ( StringUtil.StrCmp(AV16MainPgmName, wcpOAV16MainPgmName) != 0 ) || ( StringUtil.StrCmp(AV15MainPgmDesc, wcpOAV15MainPgmDesc) != 0 ) || ( StringUtil.StrCmp(AV8FormCaption, wcpOAV8FormCaption) != 0 ) || ( AV9includeInStack != wcpOAV9includeInStack ) ) )
         {
            setjustcreated();
         }
         wcpOAV16MainPgmName = AV16MainPgmName;
         wcpOAV15MainPgmDesc = AV15MainPgmDesc;
         wcpOAV8FormCaption = AV8FormCaption;
         wcpOAV9includeInStack = AV9includeInStack;
      }

      protected void WCParametersGet( )
      {
         /* Read Component Parameters. */
         sCtrlAV16MainPgmName = cgiGet( sPrefix+"AV16MainPgmName_CTRL");
         if ( StringUtil.Len( sCtrlAV16MainPgmName) > 0 )
         {
            AV16MainPgmName = cgiGet( sCtrlAV16MainPgmName);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV16MainPgmName", AV16MainPgmName);
         }
         else
         {
            AV16MainPgmName = cgiGet( sPrefix+"AV16MainPgmName_PARM");
         }
         sCtrlAV15MainPgmDesc = cgiGet( sPrefix+"AV15MainPgmDesc_CTRL");
         if ( StringUtil.Len( sCtrlAV15MainPgmDesc) > 0 )
         {
            AV15MainPgmDesc = cgiGet( sCtrlAV15MainPgmDesc);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV15MainPgmDesc", AV15MainPgmDesc);
         }
         else
         {
            AV15MainPgmDesc = cgiGet( sPrefix+"AV15MainPgmDesc_PARM");
         }
         sCtrlAV8FormCaption = cgiGet( sPrefix+"AV8FormCaption_CTRL");
         if ( StringUtil.Len( sCtrlAV8FormCaption) > 0 )
         {
            AV8FormCaption = cgiGet( sCtrlAV8FormCaption);
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV8FormCaption", AV8FormCaption);
         }
         else
         {
            AV8FormCaption = cgiGet( sPrefix+"AV8FormCaption_PARM");
         }
         sCtrlAV9includeInStack = cgiGet( sPrefix+"AV9includeInStack_CTRL");
         if ( StringUtil.Len( sCtrlAV9includeInStack) > 0 )
         {
            AV9includeInStack = StringUtil.StrToBool( cgiGet( sCtrlAV9includeInStack));
            context.httpAjaxContext.ajax_rsp_assign_attri(sPrefix, false, "AV9includeInStack", AV9includeInStack);
         }
         else
         {
            AV9includeInStack = StringUtil.StrToBool( cgiGet( sPrefix+"AV9includeInStack_PARM"));
         }
      }

      public override void componentprocess( String sPPrefix ,
                                             String sPSFPrefix ,
                                             String sCompEvt )
      {
         sCompPrefix = sPPrefix;
         sSFPrefix = sPSFPrefix;
         sPrefix = sCompPrefix + sSFPrefix;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         INITWEB( ) ;
         nDraw = 0;
         PA142( ) ;
         sEvt = sCompEvt;
         WCParametersGet( ) ;
         WS142( ) ;
         if ( isFullAjaxMode( ) )
         {
            componentdraw();
         }
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override void componentstart( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
      }

      protected void WCStart( )
      {
         nDraw = 1;
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WS142( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      protected void WCParametersSet( )
      {
         GxWebStd.gx_hidden_field( context, sPrefix+"AV16MainPgmName_PARM", StringUtil.RTrim( AV16MainPgmName));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV16MainPgmName)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV16MainPgmName_CTRL", StringUtil.RTrim( sCtrlAV16MainPgmName));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV15MainPgmDesc_PARM", StringUtil.RTrim( AV15MainPgmDesc));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV15MainPgmDesc)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV15MainPgmDesc_CTRL", StringUtil.RTrim( sCtrlAV15MainPgmDesc));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV8FormCaption_PARM", StringUtil.RTrim( AV8FormCaption));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV8FormCaption)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV8FormCaption_CTRL", StringUtil.RTrim( sCtrlAV8FormCaption));
         }
         GxWebStd.gx_hidden_field( context, sPrefix+"AV9includeInStack_PARM", StringUtil.BoolToStr( AV9includeInStack));
         if ( StringUtil.Len( StringUtil.RTrim( sCtrlAV9includeInStack)) > 0 )
         {
            GxWebStd.gx_hidden_field( context, sPrefix+"AV9includeInStack_CTRL", StringUtil.RTrim( sCtrlAV9includeInStack));
         }
      }

      public override void componentdraw( )
      {
         if ( nDoneStart == 0 )
         {
            WCStart( ) ;
         }
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         WCParametersSet( ) ;
         WE142( ) ;
         SaveComponentMsgList(sPrefix);
         context.GX_msglist = BackMsgLst;
      }

      public override String getstring( String sGXControl )
      {
         String sCtrlName ;
         if ( StringUtil.StrCmp(StringUtil.Substring( sGXControl, 1, 1), "&") == 0 )
         {
            sCtrlName = StringUtil.Substring( sGXControl, 2, StringUtil.Len( sGXControl)-1);
         }
         else
         {
            sCtrlName = sGXControl;
         }
         return cgiGet( sPrefix+"v"+StringUtil.Upper( sCtrlName)) ;
      }

      public override void componentjscripts( )
      {
         include_jscripts( ) ;
      }

      public override void componentthemes( )
      {
         define_styles( ) ;
      }

      protected void define_styles( )
      {
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), false);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811171522820", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("k2bheadermodernsearch.js", "?201811171522821", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         imgImage1_Internalname = sPrefix+"IMAGE1";
         bttK2bcloseheader_Internalname = sPrefix+"K2BCLOSEHEADER";
         lblWelcomemessage3_Internalname = sPrefix+"WELCOMEMESSAGE3";
         edtavSearchcriteria_Internalname = sPrefix+"vSEARCHCRITERIA";
         imgImage2_Internalname = sPrefix+"IMAGE2";
         tblSearchtable_Internalname = sPrefix+"SEARCHTABLE";
         divSearchcontainer_Internalname = sPrefix+"SEARCHCONTAINER";
         divRightoptions_Internalname = sPrefix+"RIGHTOPTIONS";
         lblUsermenu_Internalname = sPrefix+"USERMENU";
         divBackground_header_Internalname = sPrefix+"BACKGROUND_HEADER";
         Form.Internalname = sPrefix+"FORM";
      }

      public override void initialize_properties( )
      {
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               disableJsOutput();
            }
         }
         init_default_properties( ) ;
         edtavSearchcriteria_Jsonclick = "";
         divSearchcontainer_Visible = 1;
         bttK2bcloseheader_Tooltiptext = "";
         if ( StringUtil.Len( sPrefix) == 0 )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         setEventMetadata("'K2BCLOSE'","{handler:'E13142',iparms:[],oparms:[]}");
         setEventMetadata("'DOSEARCH'","{handler:'E16141',iparms:[{av:'AV17SearchCriteria',fld:'vSEARCHCRITERIA',pic:'',nv:''}],oparms:[{av:'AV17SearchCriteria',fld:'vSEARCHCRITERIA',pic:'',nv:''}]}");
         setEventMetadata("VSEARCHCRITERIA.ISVALID","{handler:'E14142',iparms:[{av:'AV23Pgmname',fld:'vPGMNAME',pic:'',nv:''},{av:'AV17SearchCriteria',fld:'vSEARCHCRITERIA',pic:'',nv:''}],oparms:[{av:'AV17SearchCriteria',fld:'vSEARCHCRITERIA',pic:'',nv:''}]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         wcpOAV16MainPgmName = "";
         wcpOAV15MainPgmDesc = "";
         wcpOAV8FormCaption = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         sPrefix = "";
         AV23Pgmname = "";
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         GXKey = "";
         GX_FocusControl = "";
         ClassString = "";
         StyleString = "";
         sImgUrl = "";
         TempTags = "";
         bttK2bcloseheader_Jsonclick = "";
         lblUsermenu_Jsonclick = "";
         Form = new GXWebForm();
         sXEvt = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         AV17SearchCriteria = "";
         AV6Context = new SdtK2BContext(context);
         AV20SearchableTransactions = new GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem>( context, "SearchableTransactionsItem", "PACYE2");
         GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem1 = new GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem>( context, "SearchableTransactionsItem", "PACYE2");
         AV18SearchCriteriaSession = "";
         GXt_char2 = "";
         AV10K2BUrl = "";
         sStyleString = "";
         lblWelcomemessage3_Jsonclick = "";
         imgImage2_Jsonclick = "";
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         sCtrlAV16MainPgmName = "";
         sCtrlAV15MainPgmDesc = "";
         sCtrlAV8FormCaption = "";
         sCtrlAV9includeInStack = "";
         AV23Pgmname = "K2BHeaderModernSearch";
         /* GeneXus formulas. */
         AV23Pgmname = "K2BHeaderModernSearch";
         context.Gx_err = 0;
      }

      private short nGotPars ;
      private short GxWebError ;
      private short nDynComponent ;
      private short initialized ;
      private short wbEnd ;
      private short wbStart ;
      private short nDraw ;
      private short nDoneStart ;
      private short nDonePA ;
      private short gxcookieaux ;
      private short nGXWrapped ;
      private int divSearchcontainer_Visible ;
      private int idxLst ;
      private String AV16MainPgmName ;
      private String AV15MainPgmDesc ;
      private String AV8FormCaption ;
      private String wcpOAV16MainPgmName ;
      private String wcpOAV15MainPgmDesc ;
      private String wcpOAV8FormCaption ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String sPrefix ;
      private String sCompPrefix ;
      private String sSFPrefix ;
      private String AV23Pgmname ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private String GXKey ;
      private String GX_FocusControl ;
      private String divBackground_header_Internalname ;
      private String ClassString ;
      private String StyleString ;
      private String sImgUrl ;
      private String imgImage1_Internalname ;
      private String divRightoptions_Internalname ;
      private String TempTags ;
      private String bttK2bcloseheader_Internalname ;
      private String bttK2bcloseheader_Jsonclick ;
      private String bttK2bcloseheader_Tooltiptext ;
      private String divSearchcontainer_Internalname ;
      private String lblUsermenu_Internalname ;
      private String lblUsermenu_Jsonclick ;
      private String sXEvt ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String edtavSearchcriteria_Internalname ;
      private String AV17SearchCriteria ;
      private String AV18SearchCriteriaSession ;
      private String GXt_char2 ;
      private String sStyleString ;
      private String tblSearchtable_Internalname ;
      private String lblWelcomemessage3_Internalname ;
      private String lblWelcomemessage3_Jsonclick ;
      private String edtavSearchcriteria_Jsonclick ;
      private String imgImage2_Internalname ;
      private String imgImage2_Jsonclick ;
      private String sCtrlAV16MainPgmName ;
      private String sCtrlAV15MainPgmDesc ;
      private String sCtrlAV8FormCaption ;
      private String sCtrlAV9includeInStack ;
      private bool AV9includeInStack ;
      private bool wcpOAV9includeInStack ;
      private bool entryPointCalled ;
      private bool toggleJsOutput ;
      private bool wbLoad ;
      private bool Rfr0gs ;
      private bool wbErr ;
      private bool returnInSub ;
      private String AV10K2BUrl ;
      private GXWebForm Form ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem> AV20SearchableTransactions ;
      private GXBaseCollection<SdtSearchableTransactions_SearchableTransactionsItem> GXt_objcol_SdtSearchableTransactions_SearchableTransactionsItem1 ;
      private SdtK2BContext AV6Context ;
   }

}
