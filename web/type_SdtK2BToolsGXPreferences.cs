/*
               File: type_SdtK2BToolsGXPreferences
        Description: K2BToolsGXPreferences
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/17/2018 15:5:13.84
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Web.Services.Protocols;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Reflection;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   [Serializable]
   public class SdtK2BToolsGXPreferences : GxUserType, IGxExternalObject
   {
      public SdtK2BToolsGXPreferences( )
      {
         /* Constructor for serialization */
      }

      public SdtK2BToolsGXPreferences( IGxContext context )
      {
         this.context = context;
         initialize();
      }

      private static Hashtable mapper;
      public override String JsonMap( String value )
      {
         if ( mapper == null )
         {
            mapper = new Hashtable();
         }
         return (String)mapper[value]; ;
      }

      public String getuse_encryption( )
      {
         String returngetuse_encryption ;
         if ( K2BToolsGXPreferences_externalReference == null )
         {
            K2BToolsGXPreferences_externalReference = new K2BTools.Common.GXPreferences();
         }
         returngetuse_encryption = "";
         returngetuse_encryption = (String)(K2BToolsGXPreferences_externalReference.getUSE_ENCRYPTION());
         return returngetuse_encryption ;
      }

      public String gettmpmedia_dir( )
      {
         String returngettmpmedia_dir ;
         if ( K2BToolsGXPreferences_externalReference == null )
         {
            K2BToolsGXPreferences_externalReference = new K2BTools.Common.GXPreferences();
         }
         returngettmpmedia_dir = "";
         returngettmpmedia_dir = (String)(K2BToolsGXPreferences_externalReference.getTMPMEDIA_DIR());
         return returngettmpmedia_dir ;
      }

      public String getpreferencebykey( String gxTp_key )
      {
         String returngetpreferencebykey ;
         if ( K2BToolsGXPreferences_externalReference == null )
         {
            K2BToolsGXPreferences_externalReference = new K2BTools.Common.GXPreferences();
         }
         returngetpreferencebykey = "";
         returngetpreferencebykey = (String)(K2BToolsGXPreferences_externalReference.getPreferenceByKey(gxTp_key));
         return returngetpreferencebykey ;
      }

      public bool haspreferencebykey( String gxTp_key )
      {
         bool returnhaspreferencebykey ;
         if ( K2BToolsGXPreferences_externalReference == null )
         {
            K2BToolsGXPreferences_externalReference = new K2BTools.Common.GXPreferences();
         }
         returnhaspreferencebykey = false;
         returnhaspreferencebykey = (bool)(K2BToolsGXPreferences_externalReference.hasPreferenceByKey(gxTp_key));
         return returnhaspreferencebykey ;
      }

      public Object ExternalInstance
      {
         get {
            if ( K2BToolsGXPreferences_externalReference == null )
            {
               K2BToolsGXPreferences_externalReference = new K2BTools.Common.GXPreferences();
            }
            return K2BToolsGXPreferences_externalReference ;
         }

         set {
            K2BToolsGXPreferences_externalReference = (K2BTools.Common.GXPreferences)(value);
         }

      }

      public void initialize( )
      {
         return  ;
      }

      protected K2BTools.Common.GXPreferences K2BToolsGXPreferences_externalReference=null ;
   }

}
