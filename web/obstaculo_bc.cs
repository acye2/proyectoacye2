/*
               File: Obstaculo_BC
        Description: Obstaculo
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 8:45:20.47
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class obstaculo_bc : GXHttpHandler, IGxSilentTrn
   {
      public obstaculo_bc( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlatCompactGreen");
      }

      public obstaculo_bc( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      protected void INITTRN( )
      {
      }

      public void GetInsDefault( )
      {
         ReadRow046( ) ;
         standaloneNotModal( ) ;
         InitializeNonKey046( ) ;
         standaloneModal( ) ;
         AddRow046( ) ;
         Gx_mode = "INS";
         return  ;
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               Z35ObstaculoID = (Guid)(A35ObstaculoID);
               SetMode( "UPD") ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      public bool Reindex( )
      {
         return true ;
      }

      protected void CONFIRM_040( )
      {
         BeforeValidate046( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               OnDeleteControls046( ) ;
            }
            else
            {
               CheckExtendedTable046( ) ;
               if ( AnyError == 0 )
               {
                  ZM046( 7) ;
               }
               CloseExtendedTableCursors046( ) ;
            }
         }
         if ( AnyError == 0 )
         {
            IsConfirmed = 1;
         }
      }

      protected void ZM046( short GX_JID )
      {
         if ( ( GX_JID == 6 ) || ( GX_JID == 0 ) )
         {
            Z36ObstaculoFechaRegistro = A36ObstaculoFechaRegistro;
            Z37ObstaculoTipo = A37ObstaculoTipo;
            Z40ObstaculoNumeroAdvertencias = A40ObstaculoNumeroAdvertencias;
            Z34ObstaculoPacienteRegistra = (Guid)(A34ObstaculoPacienteRegistra);
         }
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
         }
         if ( GX_JID == -6 )
         {
            Z35ObstaculoID = (Guid)(A35ObstaculoID);
            Z36ObstaculoFechaRegistro = A36ObstaculoFechaRegistro;
            Z37ObstaculoTipo = A37ObstaculoTipo;
            Z38ObstaculoLongitud = A38ObstaculoLongitud;
            Z39ObstaculoLatitud = A39ObstaculoLatitud;
            Z40ObstaculoNumeroAdvertencias = A40ObstaculoNumeroAdvertencias;
            Z34ObstaculoPacienteRegistra = (Guid)(A34ObstaculoPacienteRegistra);
         }
      }

      protected void standaloneNotModal( )
      {
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A35ObstaculoID) )
         {
            A35ObstaculoID = (Guid)(Guid.NewGuid( ));
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load046( )
      {
         /* Using cursor BC00045 */
         pr_default.execute(3, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(3) != 101) )
         {
            RcdFound6 = 1;
            A36ObstaculoFechaRegistro = BC00045_A36ObstaculoFechaRegistro[0];
            A37ObstaculoTipo = BC00045_A37ObstaculoTipo[0];
            A38ObstaculoLongitud = BC00045_A38ObstaculoLongitud[0];
            A39ObstaculoLatitud = BC00045_A39ObstaculoLatitud[0];
            A40ObstaculoNumeroAdvertencias = BC00045_A40ObstaculoNumeroAdvertencias[0];
            n40ObstaculoNumeroAdvertencias = BC00045_n40ObstaculoNumeroAdvertencias[0];
            A34ObstaculoPacienteRegistra = (Guid)((Guid)(BC00045_A34ObstaculoPacienteRegistra[0]));
            n34ObstaculoPacienteRegistra = BC00045_n34ObstaculoPacienteRegistra[0];
            ZM046( -6) ;
         }
         pr_default.close(3);
         OnLoadActions046( ) ;
      }

      protected void OnLoadActions046( )
      {
      }

      protected void CheckExtendedTable046( )
      {
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A36ObstaculoFechaRegistro) || ( A36ObstaculoFechaRegistro >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Obstaculo Fecha Registro fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         if ( ! ( ( A37ObstaculoTipo == 0 ) || ( A37ObstaculoTipo == 1 ) || ( A37ObstaculoTipo == 2 ) || ( A37ObstaculoTipo == 3 ) ) )
         {
            GX_msglist.addItem("Campo Obstaculo Tipo fuera de rango", "OutOfRange", 1, "");
            AnyError = 1;
         }
         /* Using cursor BC00044 */
         pr_default.execute(2, new Object[] {n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (Guid.Empty==A34ObstaculoPacienteRegistra) ) )
            {
               GX_msglist.addItem("No existe 'Obstaculo Paciente_ST'.", "ForeignKeyNotFound", 1, "OBSTACULOPACIENTEREGISTRA");
               AnyError = 1;
            }
         }
         pr_default.close(2);
      }

      protected void CloseExtendedTableCursors046( )
      {
         pr_default.close(2);
      }

      protected void enableDisable( )
      {
      }

      protected void GetKey046( )
      {
         /* Using cursor BC00046 */
         pr_default.execute(4, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound6 = 1;
         }
         else
         {
            RcdFound6 = 0;
         }
         pr_default.close(4);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor BC00043 */
         pr_default.execute(1, new Object[] {A35ObstaculoID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM046( 6) ;
            RcdFound6 = 1;
            A35ObstaculoID = (Guid)((Guid)(BC00043_A35ObstaculoID[0]));
            A36ObstaculoFechaRegistro = BC00043_A36ObstaculoFechaRegistro[0];
            A37ObstaculoTipo = BC00043_A37ObstaculoTipo[0];
            A38ObstaculoLongitud = BC00043_A38ObstaculoLongitud[0];
            A39ObstaculoLatitud = BC00043_A39ObstaculoLatitud[0];
            A40ObstaculoNumeroAdvertencias = BC00043_A40ObstaculoNumeroAdvertencias[0];
            n40ObstaculoNumeroAdvertencias = BC00043_n40ObstaculoNumeroAdvertencias[0];
            A34ObstaculoPacienteRegistra = (Guid)((Guid)(BC00043_A34ObstaculoPacienteRegistra[0]));
            n34ObstaculoPacienteRegistra = BC00043_n34ObstaculoPacienteRegistra[0];
            Z35ObstaculoID = (Guid)(A35ObstaculoID);
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Load046( ) ;
            if ( AnyError == 1 )
            {
               RcdFound6 = 0;
               InitializeNonKey046( ) ;
            }
            Gx_mode = sMode6;
         }
         else
         {
            RcdFound6 = 0;
            InitializeNonKey046( ) ;
            sMode6 = Gx_mode;
            Gx_mode = "DSP";
            standaloneModal( ) ;
            Gx_mode = sMode6;
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey046( ) ;
         if ( RcdFound6 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
         }
         getByPrimaryKey( ) ;
      }

      protected void insert_Check( )
      {
         CONFIRM_040( ) ;
         IsConfirmed = 0;
      }

      protected void update_Check( )
      {
         insert_Check( ) ;
      }

      protected void delete_Check( )
      {
         insert_Check( ) ;
      }

      protected void CheckOptimisticConcurrency046( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor BC00042 */
            pr_default.execute(0, new Object[] {A35ObstaculoID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Obstaculo"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z36ObstaculoFechaRegistro != BC00042_A36ObstaculoFechaRegistro[0] ) || ( Z37ObstaculoTipo != BC00042_A37ObstaculoTipo[0] ) || ( Z40ObstaculoNumeroAdvertencias != BC00042_A40ObstaculoNumeroAdvertencias[0] ) || ( Z34ObstaculoPacienteRegistra != BC00042_A34ObstaculoPacienteRegistra[0] ) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"Obstaculo"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert046( )
      {
         BeforeValidate046( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable046( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM046( 0) ;
            CheckOptimisticConcurrency046( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm046( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert046( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00047 */
                     pr_default.execute(5, new Object[] {A36ObstaculoFechaRegistro, A37ObstaculoTipo, A38ObstaculoLongitud, A39ObstaculoLatitud, n40ObstaculoNumeroAdvertencias, A40ObstaculoNumeroAdvertencias, n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra, A35ObstaculoID});
                     pr_default.close(5);
                     dsDefault.SmartCacheProvider.SetUpdated("Obstaculo") ;
                     if ( (pr_default.getStatus(5) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load046( ) ;
            }
            EndLevel046( ) ;
         }
         CloseExtendedTableCursors046( ) ;
      }

      protected void Update046( )
      {
         BeforeValidate046( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable046( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency046( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm046( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate046( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor BC00048 */
                     pr_default.execute(6, new Object[] {A36ObstaculoFechaRegistro, A37ObstaculoTipo, A38ObstaculoLongitud, A39ObstaculoLatitud, n40ObstaculoNumeroAdvertencias, A40ObstaculoNumeroAdvertencias, n34ObstaculoPacienteRegistra, A34ObstaculoPacienteRegistra, A35ObstaculoID});
                     pr_default.close(6);
                     dsDefault.SmartCacheProvider.SetUpdated("Obstaculo") ;
                     if ( (pr_default.getStatus(6) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"Obstaculo"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate046( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel046( ) ;
         }
         CloseExtendedTableCursors046( ) ;
      }

      protected void DeferredUpdate046( )
      {
      }

      protected void delete( )
      {
         Gx_mode = "DLT";
         BeforeValidate046( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency046( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls046( ) ;
            AfterConfirm046( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete046( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor BC00049 */
                  pr_default.execute(7, new Object[] {A35ObstaculoID});
                  pr_default.close(7);
                  dsDefault.SmartCacheProvider.SetUpdated("Obstaculo") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode6 = Gx_mode;
         Gx_mode = "DLT";
         EndLevel046( ) ;
         Gx_mode = sMode6;
      }

      protected void OnDeleteControls046( )
      {
         standaloneModal( ) ;
         /* No delete mode formulas found. */
      }

      protected void EndLevel046( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete046( ) ;
         }
         if ( AnyError == 0 )
         {
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanKeyStart046( )
      {
         /* Using cursor BC000410 */
         pr_default.execute(8, new Object[] {A35ObstaculoID});
         RcdFound6 = 0;
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound6 = 1;
            A35ObstaculoID = (Guid)((Guid)(BC000410_A35ObstaculoID[0]));
            A36ObstaculoFechaRegistro = BC000410_A36ObstaculoFechaRegistro[0];
            A37ObstaculoTipo = BC000410_A37ObstaculoTipo[0];
            A38ObstaculoLongitud = BC000410_A38ObstaculoLongitud[0];
            A39ObstaculoLatitud = BC000410_A39ObstaculoLatitud[0];
            A40ObstaculoNumeroAdvertencias = BC000410_A40ObstaculoNumeroAdvertencias[0];
            n40ObstaculoNumeroAdvertencias = BC000410_n40ObstaculoNumeroAdvertencias[0];
            A34ObstaculoPacienteRegistra = (Guid)((Guid)(BC000410_A34ObstaculoPacienteRegistra[0]));
            n34ObstaculoPacienteRegistra = BC000410_n34ObstaculoPacienteRegistra[0];
         }
         /* Load Subordinate Levels */
      }

      protected void ScanKeyNext046( )
      {
         /* Scan next routine */
         pr_default.readNext(8);
         RcdFound6 = 0;
         ScanKeyLoad046( ) ;
      }

      protected void ScanKeyLoad046( )
      {
         sMode6 = Gx_mode;
         Gx_mode = "DSP";
         if ( (pr_default.getStatus(8) != 101) )
         {
            RcdFound6 = 1;
            A35ObstaculoID = (Guid)((Guid)(BC000410_A35ObstaculoID[0]));
            A36ObstaculoFechaRegistro = BC000410_A36ObstaculoFechaRegistro[0];
            A37ObstaculoTipo = BC000410_A37ObstaculoTipo[0];
            A38ObstaculoLongitud = BC000410_A38ObstaculoLongitud[0];
            A39ObstaculoLatitud = BC000410_A39ObstaculoLatitud[0];
            A40ObstaculoNumeroAdvertencias = BC000410_A40ObstaculoNumeroAdvertencias[0];
            n40ObstaculoNumeroAdvertencias = BC000410_n40ObstaculoNumeroAdvertencias[0];
            A34ObstaculoPacienteRegistra = (Guid)((Guid)(BC000410_A34ObstaculoPacienteRegistra[0]));
            n34ObstaculoPacienteRegistra = BC000410_n34ObstaculoPacienteRegistra[0];
         }
         Gx_mode = sMode6;
      }

      protected void ScanKeyEnd046( )
      {
         pr_default.close(8);
      }

      protected void AfterConfirm046( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert046( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate046( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete046( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete046( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate046( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes046( )
      {
      }

      protected void send_integrity_lvl_hashes046( )
      {
      }

      protected void AddRow046( )
      {
         VarsToRow6( bcObstaculo) ;
      }

      protected void ReadRow046( )
      {
         RowToVars6( bcObstaculo, 1) ;
      }

      protected void InitializeNonKey046( )
      {
         A36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         A37ObstaculoTipo = 0;
         A34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
         n34ObstaculoPacienteRegistra = false;
         A38ObstaculoLongitud = "";
         A39ObstaculoLatitud = "";
         A40ObstaculoNumeroAdvertencias = 0;
         n40ObstaculoNumeroAdvertencias = false;
         Z36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         Z37ObstaculoTipo = 0;
         Z40ObstaculoNumeroAdvertencias = 0;
         Z34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
      }

      protected void InitAll046( )
      {
         A35ObstaculoID = (Guid)(Guid.NewGuid( ));
         InitializeNonKey046( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      public void VarsToRow6( SdtObstaculo obj6 )
      {
         obj6.gxTpr_Mode = Gx_mode;
         obj6.gxTpr_Obstaculofecharegistro = A36ObstaculoFechaRegistro;
         obj6.gxTpr_Obstaculotipo = A37ObstaculoTipo;
         obj6.gxTpr_Obstaculopacienteregistra = (Guid)(A34ObstaculoPacienteRegistra);
         obj6.gxTpr_Obstaculolongitud = A38ObstaculoLongitud;
         obj6.gxTpr_Obstaculolatitud = A39ObstaculoLatitud;
         obj6.gxTpr_Obstaculonumeroadvertencias = A40ObstaculoNumeroAdvertencias;
         obj6.gxTpr_Obstaculoid = (Guid)(A35ObstaculoID);
         obj6.gxTpr_Obstaculoid_Z = (Guid)(Z35ObstaculoID);
         obj6.gxTpr_Obstaculofecharegistro_Z = Z36ObstaculoFechaRegistro;
         obj6.gxTpr_Obstaculotipo_Z = Z37ObstaculoTipo;
         obj6.gxTpr_Obstaculopacienteregistra_Z = (Guid)(Z34ObstaculoPacienteRegistra);
         obj6.gxTpr_Obstaculonumeroadvertencias_Z = Z40ObstaculoNumeroAdvertencias;
         obj6.gxTpr_Obstaculopacienteregistra_N = (short)(Convert.ToInt16(n34ObstaculoPacienteRegistra));
         obj6.gxTpr_Obstaculonumeroadvertencias_N = (short)(Convert.ToInt16(n40ObstaculoNumeroAdvertencias));
         obj6.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void KeyVarsToRow6( SdtObstaculo obj6 )
      {
         obj6.gxTpr_Obstaculoid = (Guid)(A35ObstaculoID);
         return  ;
      }

      public void RowToVars6( SdtObstaculo obj6 ,
                              int forceLoad )
      {
         Gx_mode = obj6.gxTpr_Mode;
         A36ObstaculoFechaRegistro = obj6.gxTpr_Obstaculofecharegistro;
         A37ObstaculoTipo = obj6.gxTpr_Obstaculotipo;
         A34ObstaculoPacienteRegistra = (Guid)(obj6.gxTpr_Obstaculopacienteregistra);
         n34ObstaculoPacienteRegistra = false;
         A38ObstaculoLongitud = obj6.gxTpr_Obstaculolongitud;
         A39ObstaculoLatitud = obj6.gxTpr_Obstaculolatitud;
         A40ObstaculoNumeroAdvertencias = obj6.gxTpr_Obstaculonumeroadvertencias;
         n40ObstaculoNumeroAdvertencias = false;
         A35ObstaculoID = (Guid)(obj6.gxTpr_Obstaculoid);
         Z35ObstaculoID = (Guid)(obj6.gxTpr_Obstaculoid_Z);
         Z36ObstaculoFechaRegistro = obj6.gxTpr_Obstaculofecharegistro_Z;
         Z37ObstaculoTipo = obj6.gxTpr_Obstaculotipo_Z;
         Z34ObstaculoPacienteRegistra = (Guid)(obj6.gxTpr_Obstaculopacienteregistra_Z);
         Z40ObstaculoNumeroAdvertencias = obj6.gxTpr_Obstaculonumeroadvertencias_Z;
         n34ObstaculoPacienteRegistra = (bool)(Convert.ToBoolean(obj6.gxTpr_Obstaculopacienteregistra_N));
         n40ObstaculoNumeroAdvertencias = (bool)(Convert.ToBoolean(obj6.gxTpr_Obstaculonumeroadvertencias_N));
         Gx_mode = obj6.gxTpr_Mode;
         return  ;
      }

      public void LoadKey( Object[] obj )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         A35ObstaculoID = (Guid)((Guid)getParm(obj,0));
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         InitializeNonKey046( ) ;
         ScanKeyStart046( ) ;
         if ( RcdFound6 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z35ObstaculoID = (Guid)(A35ObstaculoID);
         }
         ZM046( -6) ;
         OnLoadActions046( ) ;
         AddRow046( ) ;
         ScanKeyEnd046( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      public void Load( )
      {
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         RowToVars6( bcObstaculo, 0) ;
         ScanKeyStart046( ) ;
         if ( RcdFound6 == 0 )
         {
            Gx_mode = "INS";
         }
         else
         {
            Gx_mode = "UPD";
            Z35ObstaculoID = (Guid)(A35ObstaculoID);
         }
         ZM046( -6) ;
         OnLoadActions046( ) ;
         AddRow046( ) ;
         ScanKeyEnd046( ) ;
         if ( RcdFound6 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "");
            AnyError = 1;
         }
         context.GX_msglist = BackMsgLst;
      }

      protected void SaveImpl( )
      {
         nKeyPressed = 1;
         GetKey046( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            Insert046( ) ;
         }
         else
         {
            if ( RcdFound6 == 1 )
            {
               if ( A35ObstaculoID != Z35ObstaculoID )
               {
                  A35ObstaculoID = (Guid)(Z35ObstaculoID);
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
               }
               else
               {
                  Gx_mode = "UPD";
                  /* Update record */
                  Update046( ) ;
               }
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "");
                  AnyError = 1;
               }
               else
               {
                  if ( A35ObstaculoID != Z35ObstaculoID )
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert046( ) ;
                     }
                  }
                  else
                  {
                     if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                        AnyError = 1;
                     }
                     else
                     {
                        Gx_mode = "INS";
                        /* Insert record */
                        Insert046( ) ;
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      public void Save( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars6( bcObstaculo, 0) ;
         SaveImpl( ) ;
         VarsToRow6( bcObstaculo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public bool Insert( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars6( bcObstaculo, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert046( ) ;
         AfterTrn( ) ;
         VarsToRow6( bcObstaculo) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      protected void UpdateImpl( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
         {
            SaveImpl( ) ;
         }
         else
         {
            SdtObstaculo auxBC = new SdtObstaculo(context) ;
            auxBC.Load(A35ObstaculoID);
            auxBC.UpdateDirties(bcObstaculo);
            auxBC.Save();
            IGxSilentTrn auxTrn = auxBC.getTransaction() ;
            LclMsgLst = (msglist)(auxTrn.GetMessages());
            AnyError = (short)(auxTrn.Errors());
            Gx_mode = auxTrn.GetMode();
            context.GX_msglist = LclMsgLst;
            AfterTrn( ) ;
         }
      }

      public bool Update( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars6( bcObstaculo, 0) ;
         UpdateImpl( ) ;
         VarsToRow6( bcObstaculo) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public bool InsertOrUpdate( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         IsConfirmed = 1;
         RowToVars6( bcObstaculo, 0) ;
         Gx_mode = "INS";
         /* Insert record */
         Insert046( ) ;
         if ( AnyError == 1 )
         {
            AnyError = 0;
            context.GX_msglist.removeAllItems();
            UpdateImpl( ) ;
         }
         else
         {
            AfterTrn( ) ;
         }
         VarsToRow6( bcObstaculo) ;
         context.GX_msglist = BackMsgLst;
         return (AnyError==0) ;
      }

      public void Check( )
      {
         BackMsgLst = context.GX_msglist;
         context.GX_msglist = LclMsgLst;
         AnyError = 0;
         context.GX_msglist.removeAllItems();
         RowToVars6( bcObstaculo, 0) ;
         nKeyPressed = 3;
         IsConfirmed = 0;
         GetKey046( ) ;
         if ( RcdFound6 == 1 )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( A35ObstaculoID != Z35ObstaculoID )
            {
               A35ObstaculoID = (Guid)(Z35ObstaculoID);
               GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "DuplicatePrimaryKey", 1, "");
               AnyError = 1;
            }
            else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               delete_Check( ) ;
            }
            else
            {
               Gx_mode = "UPD";
               update_Check( ) ;
            }
         }
         else
         {
            if ( A35ObstaculoID != Z35ObstaculoID )
            {
               Gx_mode = "INS";
               insert_Check( ) ;
            }
            else
            {
               if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "");
                  AnyError = 1;
               }
               else
               {
                  Gx_mode = "INS";
                  insert_Check( ) ;
               }
            }
         }
         pr_default.close(1);
         pr_gam.rollback( "Obstaculo_BC");
         pr_default.rollback( "Obstaculo_BC");
         VarsToRow6( bcObstaculo) ;
         context.GX_msglist = BackMsgLst;
         return  ;
      }

      public int Errors( )
      {
         if ( AnyError == 0 )
         {
            return (int)(0) ;
         }
         return (int)(1) ;
      }

      public msglist GetMessages( )
      {
         return LclMsgLst ;
      }

      public String GetMode( )
      {
         Gx_mode = bcObstaculo.gxTpr_Mode;
         return Gx_mode ;
      }

      public void SetMode( String lMode )
      {
         Gx_mode = lMode;
         bcObstaculo.gxTpr_Mode = Gx_mode;
         return  ;
      }

      public void SetSDT( GxSilentTrnSdt sdt ,
                          short sdtToBc )
      {
         if ( sdt != bcObstaculo )
         {
            bcObstaculo = (SdtObstaculo)(sdt);
            if ( StringUtil.StrCmp(bcObstaculo.gxTpr_Mode, "") == 0 )
            {
               bcObstaculo.gxTpr_Mode = "INS";
            }
            if ( sdtToBc == 1 )
            {
               VarsToRow6( bcObstaculo) ;
            }
            else
            {
               RowToVars6( bcObstaculo, 1) ;
            }
         }
         else
         {
            if ( StringUtil.StrCmp(bcObstaculo.gxTpr_Mode, "") == 0 )
            {
               bcObstaculo.gxTpr_Mode = "INS";
            }
         }
         return  ;
      }

      public void ReloadFromSDT( )
      {
         RowToVars6( bcObstaculo, 1) ;
         return  ;
      }

      public SdtObstaculo Obstaculo_BC
      {
         get {
            return bcObstaculo ;
         }

      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "obstaculo_Execute" ;
         }

      }

      public override void webExecute( )
      {
         createObjects();
         initialize();
      }

      protected override void createObjects( )
      {
      }

      protected void Process( )
      {
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
      }

      public override void initialize( )
      {
         scmdbuf = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Gx_mode = "";
         Z35ObstaculoID = (Guid)(Guid.Empty);
         A35ObstaculoID = (Guid)(Guid.Empty);
         Z36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         A36ObstaculoFechaRegistro = (DateTime)(DateTime.MinValue);
         Z34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
         A34ObstaculoPacienteRegistra = (Guid)(Guid.Empty);
         Z38ObstaculoLongitud = "";
         A38ObstaculoLongitud = "";
         Z39ObstaculoLatitud = "";
         A39ObstaculoLatitud = "";
         BC00045_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         BC00045_A36ObstaculoFechaRegistro = new DateTime[] {DateTime.MinValue} ;
         BC00045_A37ObstaculoTipo = new short[1] ;
         BC00045_A38ObstaculoLongitud = new String[] {""} ;
         BC00045_A39ObstaculoLatitud = new String[] {""} ;
         BC00045_A40ObstaculoNumeroAdvertencias = new short[1] ;
         BC00045_n40ObstaculoNumeroAdvertencias = new bool[] {false} ;
         BC00045_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         BC00045_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         BC00044_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         BC00044_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         BC00046_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         BC00043_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         BC00043_A36ObstaculoFechaRegistro = new DateTime[] {DateTime.MinValue} ;
         BC00043_A37ObstaculoTipo = new short[1] ;
         BC00043_A38ObstaculoLongitud = new String[] {""} ;
         BC00043_A39ObstaculoLatitud = new String[] {""} ;
         BC00043_A40ObstaculoNumeroAdvertencias = new short[1] ;
         BC00043_n40ObstaculoNumeroAdvertencias = new bool[] {false} ;
         BC00043_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         BC00043_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         sMode6 = "";
         BC00042_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         BC00042_A36ObstaculoFechaRegistro = new DateTime[] {DateTime.MinValue} ;
         BC00042_A37ObstaculoTipo = new short[1] ;
         BC00042_A38ObstaculoLongitud = new String[] {""} ;
         BC00042_A39ObstaculoLatitud = new String[] {""} ;
         BC00042_A40ObstaculoNumeroAdvertencias = new short[1] ;
         BC00042_n40ObstaculoNumeroAdvertencias = new bool[] {false} ;
         BC00042_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         BC00042_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         BC000410_A35ObstaculoID = new Guid[] {Guid.Empty} ;
         BC000410_A36ObstaculoFechaRegistro = new DateTime[] {DateTime.MinValue} ;
         BC000410_A37ObstaculoTipo = new short[1] ;
         BC000410_A38ObstaculoLongitud = new String[] {""} ;
         BC000410_A39ObstaculoLatitud = new String[] {""} ;
         BC000410_A40ObstaculoNumeroAdvertencias = new short[1] ;
         BC000410_n40ObstaculoNumeroAdvertencias = new bool[] {false} ;
         BC000410_A34ObstaculoPacienteRegistra = new Guid[] {Guid.Empty} ;
         BC000410_n34ObstaculoPacienteRegistra = new bool[] {false} ;
         BackMsgLst = new msglist();
         LclMsgLst = new msglist();
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.obstaculo_bc__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.obstaculo_bc__default(),
            new Object[][] {
                new Object[] {
               BC00042_A35ObstaculoID, BC00042_A36ObstaculoFechaRegistro, BC00042_A37ObstaculoTipo, BC00042_A38ObstaculoLongitud, BC00042_A39ObstaculoLatitud, BC00042_A40ObstaculoNumeroAdvertencias, BC00042_n40ObstaculoNumeroAdvertencias, BC00042_A34ObstaculoPacienteRegistra, BC00042_n34ObstaculoPacienteRegistra
               }
               , new Object[] {
               BC00043_A35ObstaculoID, BC00043_A36ObstaculoFechaRegistro, BC00043_A37ObstaculoTipo, BC00043_A38ObstaculoLongitud, BC00043_A39ObstaculoLatitud, BC00043_A40ObstaculoNumeroAdvertencias, BC00043_n40ObstaculoNumeroAdvertencias, BC00043_A34ObstaculoPacienteRegistra, BC00043_n34ObstaculoPacienteRegistra
               }
               , new Object[] {
               BC00044_A34ObstaculoPacienteRegistra
               }
               , new Object[] {
               BC00045_A35ObstaculoID, BC00045_A36ObstaculoFechaRegistro, BC00045_A37ObstaculoTipo, BC00045_A38ObstaculoLongitud, BC00045_A39ObstaculoLatitud, BC00045_A40ObstaculoNumeroAdvertencias, BC00045_n40ObstaculoNumeroAdvertencias, BC00045_A34ObstaculoPacienteRegistra, BC00045_n34ObstaculoPacienteRegistra
               }
               , new Object[] {
               BC00046_A35ObstaculoID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               BC000410_A35ObstaculoID, BC000410_A36ObstaculoFechaRegistro, BC000410_A37ObstaculoTipo, BC000410_A38ObstaculoLongitud, BC000410_A39ObstaculoLatitud, BC000410_A40ObstaculoNumeroAdvertencias, BC000410_n40ObstaculoNumeroAdvertencias, BC000410_A34ObstaculoPacienteRegistra, BC000410_n34ObstaculoPacienteRegistra
               }
            }
         );
         Z35ObstaculoID = (Guid)(Guid.NewGuid( ));
         A35ObstaculoID = (Guid)(Guid.NewGuid( ));
         INITTRN();
         /* Execute Start event if defined. */
         standaloneNotModal( ) ;
      }

      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short GX_JID ;
      private short Z37ObstaculoTipo ;
      private short A37ObstaculoTipo ;
      private short Z40ObstaculoNumeroAdvertencias ;
      private short A40ObstaculoNumeroAdvertencias ;
      private short Gx_BScreen ;
      private short RcdFound6 ;
      private int trnEnded ;
      private String scmdbuf ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String Gx_mode ;
      private String sMode6 ;
      private DateTime Z36ObstaculoFechaRegistro ;
      private DateTime A36ObstaculoFechaRegistro ;
      private bool n40ObstaculoNumeroAdvertencias ;
      private bool n34ObstaculoPacienteRegistra ;
      private String Z38ObstaculoLongitud ;
      private String A38ObstaculoLongitud ;
      private String Z39ObstaculoLatitud ;
      private String A39ObstaculoLatitud ;
      private Guid Z35ObstaculoID ;
      private Guid A35ObstaculoID ;
      private Guid Z34ObstaculoPacienteRegistra ;
      private Guid A34ObstaculoPacienteRegistra ;
      private SdtObstaculo bcObstaculo ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private IDataStoreProvider pr_default ;
      private Guid[] BC00045_A35ObstaculoID ;
      private DateTime[] BC00045_A36ObstaculoFechaRegistro ;
      private short[] BC00045_A37ObstaculoTipo ;
      private String[] BC00045_A38ObstaculoLongitud ;
      private String[] BC00045_A39ObstaculoLatitud ;
      private short[] BC00045_A40ObstaculoNumeroAdvertencias ;
      private bool[] BC00045_n40ObstaculoNumeroAdvertencias ;
      private Guid[] BC00045_A34ObstaculoPacienteRegistra ;
      private bool[] BC00045_n34ObstaculoPacienteRegistra ;
      private Guid[] BC00044_A34ObstaculoPacienteRegistra ;
      private bool[] BC00044_n34ObstaculoPacienteRegistra ;
      private Guid[] BC00046_A35ObstaculoID ;
      private Guid[] BC00043_A35ObstaculoID ;
      private DateTime[] BC00043_A36ObstaculoFechaRegistro ;
      private short[] BC00043_A37ObstaculoTipo ;
      private String[] BC00043_A38ObstaculoLongitud ;
      private String[] BC00043_A39ObstaculoLatitud ;
      private short[] BC00043_A40ObstaculoNumeroAdvertencias ;
      private bool[] BC00043_n40ObstaculoNumeroAdvertencias ;
      private Guid[] BC00043_A34ObstaculoPacienteRegistra ;
      private bool[] BC00043_n34ObstaculoPacienteRegistra ;
      private Guid[] BC00042_A35ObstaculoID ;
      private DateTime[] BC00042_A36ObstaculoFechaRegistro ;
      private short[] BC00042_A37ObstaculoTipo ;
      private String[] BC00042_A38ObstaculoLongitud ;
      private String[] BC00042_A39ObstaculoLatitud ;
      private short[] BC00042_A40ObstaculoNumeroAdvertencias ;
      private bool[] BC00042_n40ObstaculoNumeroAdvertencias ;
      private Guid[] BC00042_A34ObstaculoPacienteRegistra ;
      private bool[] BC00042_n34ObstaculoPacienteRegistra ;
      private Guid[] BC000410_A35ObstaculoID ;
      private DateTime[] BC000410_A36ObstaculoFechaRegistro ;
      private short[] BC000410_A37ObstaculoTipo ;
      private String[] BC000410_A38ObstaculoLongitud ;
      private String[] BC000410_A39ObstaculoLatitud ;
      private short[] BC000410_A40ObstaculoNumeroAdvertencias ;
      private bool[] BC000410_n40ObstaculoNumeroAdvertencias ;
      private Guid[] BC000410_A34ObstaculoPacienteRegistra ;
      private bool[] BC000410_n34ObstaculoPacienteRegistra ;
      private msglist BackMsgLst ;
      private msglist LclMsgLst ;
      private IDataStoreProvider pr_gam ;
   }

   public class obstaculo_bc__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class obstaculo_bc__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new UpdateCursor(def[5])
       ,new UpdateCursor(def[6])
       ,new UpdateCursor(def[7])
       ,new ForEachCursor(def[8])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmBC00045 ;
        prmBC00045 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00044 ;
        prmBC00044 = new Object[] {
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00046 ;
        prmBC00046 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00043 ;
        prmBC00043 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00042 ;
        prmBC00042 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00047 ;
        prmBC00047 = new Object[] {
        new Object[] {"@ObstaculoFechaRegistro",SqlDbType.DateTime,8,5} ,
        new Object[] {"@ObstaculoTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoNumeroAdvertencias",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00048 ;
        prmBC00048 = new Object[] {
        new Object[] {"@ObstaculoFechaRegistro",SqlDbType.DateTime,8,5} ,
        new Object[] {"@ObstaculoTipo",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@ObstaculoNumeroAdvertencias",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@ObstaculoPacienteRegistra",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC00049 ;
        prmBC00049 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmBC000410 ;
        prmBC000410 = new Object[] {
        new Object[] {"@ObstaculoID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("BC00042", "SELECT [ObstaculoID], [ObstaculoFechaRegistro], [ObstaculoTipo], [ObstaculoLongitud], [ObstaculoLatitud], [ObstaculoNumeroAdvertencias], [ObstaculoPacienteRegistra] AS ObstaculoPacienteRegistra FROM [Obstaculo] WITH (UPDLOCK) WHERE [ObstaculoID] = @ObstaculoID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00042,1,0,true,false )
           ,new CursorDef("BC00043", "SELECT [ObstaculoID], [ObstaculoFechaRegistro], [ObstaculoTipo], [ObstaculoLongitud], [ObstaculoLatitud], [ObstaculoNumeroAdvertencias], [ObstaculoPacienteRegistra] AS ObstaculoPacienteRegistra FROM [Obstaculo] WITH (NOLOCK) WHERE [ObstaculoID] = @ObstaculoID ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00043,1,0,true,false )
           ,new CursorDef("BC00044", "SELECT [PersonaID] AS ObstaculoPacienteRegistra FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @ObstaculoPacienteRegistra ",true, GxErrorMask.GX_NOMASK, false, this,prmBC00044,1,0,true,false )
           ,new CursorDef("BC00045", "SELECT TM1.[ObstaculoID], TM1.[ObstaculoFechaRegistro], TM1.[ObstaculoTipo], TM1.[ObstaculoLongitud], TM1.[ObstaculoLatitud], TM1.[ObstaculoNumeroAdvertencias], TM1.[ObstaculoPacienteRegistra] AS ObstaculoPacienteRegistra FROM [Obstaculo] TM1 WITH (NOLOCK) WHERE TM1.[ObstaculoID] = @ObstaculoID ORDER BY TM1.[ObstaculoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00045,100,0,true,false )
           ,new CursorDef("BC00046", "SELECT [ObstaculoID] FROM [Obstaculo] WITH (NOLOCK) WHERE [ObstaculoID] = @ObstaculoID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmBC00046,1,0,true,false )
           ,new CursorDef("BC00047", "INSERT INTO [Obstaculo]([ObstaculoFechaRegistro], [ObstaculoTipo], [ObstaculoLongitud], [ObstaculoLatitud], [ObstaculoNumeroAdvertencias], [ObstaculoPacienteRegistra], [ObstaculoID]) VALUES(@ObstaculoFechaRegistro, @ObstaculoTipo, @ObstaculoLongitud, @ObstaculoLatitud, @ObstaculoNumeroAdvertencias, @ObstaculoPacienteRegistra, @ObstaculoID)", GxErrorMask.GX_NOMASK,prmBC00047)
           ,new CursorDef("BC00048", "UPDATE [Obstaculo] SET [ObstaculoFechaRegistro]=@ObstaculoFechaRegistro, [ObstaculoTipo]=@ObstaculoTipo, [ObstaculoLongitud]=@ObstaculoLongitud, [ObstaculoLatitud]=@ObstaculoLatitud, [ObstaculoNumeroAdvertencias]=@ObstaculoNumeroAdvertencias, [ObstaculoPacienteRegistra]=@ObstaculoPacienteRegistra  WHERE [ObstaculoID] = @ObstaculoID", GxErrorMask.GX_NOMASK,prmBC00048)
           ,new CursorDef("BC00049", "DELETE FROM [Obstaculo]  WHERE [ObstaculoID] = @ObstaculoID", GxErrorMask.GX_NOMASK,prmBC00049)
           ,new CursorDef("BC000410", "SELECT TM1.[ObstaculoID], TM1.[ObstaculoFechaRegistro], TM1.[ObstaculoTipo], TM1.[ObstaculoLongitud], TM1.[ObstaculoLatitud], TM1.[ObstaculoNumeroAdvertencias], TM1.[ObstaculoPacienteRegistra] AS ObstaculoPacienteRegistra FROM [Obstaculo] TM1 WITH (NOLOCK) WHERE TM1.[ObstaculoID] = @ObstaculoID ORDER BY TM1.[ObstaculoID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmBC000410,100,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
              ((short[]) buf[5])[0] = rslt.getShort(6) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(6);
              ((Guid[]) buf[7])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(7);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
              ((short[]) buf[5])[0] = rslt.getShort(6) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(6);
              ((Guid[]) buf[7])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(7);
              return;
           case 2 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
              ((short[]) buf[5])[0] = rslt.getShort(6) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(6);
              ((Guid[]) buf[7])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(7);
              return;
           case 4 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 8 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((String[]) buf[4])[0] = rslt.getLongVarchar(5) ;
              ((short[]) buf[5])[0] = rslt.getShort(6) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(6);
              ((Guid[]) buf[7])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(7);
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 2 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 3 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 5 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              stmt.SetParameter(3, (String)parms[2]);
              stmt.SetParameter(4, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[7]);
              }
              stmt.SetParameter(7, (Guid)parms[8]);
              return;
           case 6 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              stmt.SetParameter(3, (String)parms[2]);
              stmt.SetParameter(4, (String)parms[3]);
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 5 , SqlDbType.SmallInt );
              }
              else
              {
                 stmt.SetParameter(5, (short)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[7]);
              }
              stmt.SetParameter(7, (Guid)parms[8]);
              return;
           case 7 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
