/*
               File: EmergenciaBitacora
        Description: Emergencia Bitacora
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 17:34:7.48
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class emergenciabitacora : GXDataArea, System.Web.SessionState.IRequiresSessionState
   {
      protected void INITENV( )
      {
         if ( GxWebError != 0 )
         {
            return  ;
         }
      }

      protected void INITTRN( )
      {
         initialize_properties( ) ;
         entryPointCalled = false;
         gxfirstwebparm = GetNextPar( );
         gxfirstwebparm_bkp = gxfirstwebparm;
         gxfirstwebparm = DecryptAjaxCall( gxfirstwebparm);
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         if ( StringUtil.StrCmp(gxfirstwebparm, "dyncall") == 0 )
         {
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            dyncall( GetNextPar( )) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_8") == 0 )
         {
            A41EmergenciaPaciente = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            n41EmergenciaPaciente = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41EmergenciaPaciente", A41EmergenciaPaciente.ToString());
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_8( A41EmergenciaPaciente) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxExecAct_"+"gxLoad_9") == 0 )
         {
            A42EmergenciaCoach = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
            n42EmergenciaCoach = false;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42EmergenciaCoach", A42EmergenciaCoach.ToString());
            setAjaxCallMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxLoad_9( A42EmergenciaCoach) ;
            return  ;
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxajaxEvt") == 0 )
         {
            setAjaxEventMode();
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else if ( StringUtil.StrCmp(gxfirstwebparm, "gxfullajaxEvt") == 0 )
         {
            if ( ! IsValidAjaxCall( true) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = GetNextPar( );
         }
         else
         {
            if ( ! IsValidAjaxCall( false) )
            {
               GxWebError = 1;
               return  ;
            }
            gxfirstwebparm = gxfirstwebparm_bkp;
         }
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
         if ( String.IsNullOrEmpty(StringUtil.RTrim( context.GetCookie( "GX_SESSION_ID"))) )
         {
            gxcookieaux = context.SetCookie( "GX_SESSION_ID", Crypto.Encrypt64( Crypto.GetEncryptionKey( ), Crypto.GetServerKey( )), "", (DateTime)(DateTime.MinValue), "", 0);
         }
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         cmbEmergenciaEstado.Name = "EMERGENCIAESTADO";
         cmbEmergenciaEstado.WebTags = "";
         cmbEmergenciaEstado.addItem("1", "Activa", 0);
         cmbEmergenciaEstado.addItem("2", "Finalizada", 0);
         if ( cmbEmergenciaEstado.ItemCount > 0 )
         {
            A45EmergenciaEstado = (short)(NumberUtil.Val( cmbEmergenciaEstado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45EmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)));
         }
         if ( toggleJsOutput )
         {
            if ( context.isSpaRequest( ) )
            {
               enableJsOutput();
            }
         }
         if ( ! context.isSpaRequest( ) )
         {
            Form.Meta.addItem("generator", "GeneXus C# 15_0_9-121631", 0) ;
            Form.Meta.addItem("description", "Emergencia Bitacora", 0) ;
         }
         context.wjLoc = "";
         context.nUserReturn = 0;
         context.wbHandled = 0;
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
         }
         GX_FocusControl = edtEmergenciaID_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         wbErr = false;
         context.SetDefaultTheme("K2BFlat");
         if ( ! context.IsLocalStorageSupported( ) )
         {
            context.PushCurrentUrl();
         }
      }

      public emergenciabitacora( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public emergenciabitacora( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( )
      {
         executePrivate();
      }

      void executePrivate( )
      {
         isStatic = false;
         webExecute();
      }

      protected override void createObjects( )
      {
         cmbEmergenciaEstado = new GXCombobox();
      }

      protected override bool IntegratedSecurityEnabled
      {
         get {
            return true ;
         }

      }

      protected override GAMSecurityLevel IntegratedSecurityLevel
      {
         get {
            return GAMSecurityLevel.SecurityHigh ;
         }

      }

      protected override string IntegratedSecurityPermissionName
      {
         get {
            return "emergenciabitacora_Execute" ;
         }

      }

      public override void webExecute( )
      {
         if ( initialized == 0 )
         {
            createObjects();
            initialize();
         }
         INITENV( ) ;
         INITTRN( ) ;
         if ( ( GxWebError == 0 ) && ! isAjaxCallMode( ) )
         {
            MasterPageObj = (GXMasterPage) ClassLoader.GetInstance("k2bwwmasterpageflat", "GeneXus.Programs.k2bwwmasterpageflat", new Object[] {new GxContext( context.handle, context.DataStores, context.HttpContext)});
            MasterPageObj.setDataArea(this,false);
            ValidateSpaRequest();
            MasterPageObj.webExecute();
            if ( ( GxWebError == 0 ) && context.isAjaxRequest( ) )
            {
               enableOutput();
               if ( ! context.isAjaxRequest( ) )
               {
                  context.GX_webresponse.AppendHeader("Cache-Control", "max-age=0");
               }
               if ( ! context.WillRedirect( ) )
               {
                  context.GX_webresponse.AddString((String)(context.getJSONResponse( )));
               }
               else
               {
                  if ( context.isAjaxRequest( ) )
                  {
                     disableOutput();
                  }
                  RenderHtmlHeaders( ) ;
                  context.Redirect( context.wjLoc );
                  context.DispatchAjaxCommands();
               }
            }
         }
         this.cleanup();
      }

      protected void fix_multi_value_controls( )
      {
         if ( context.isAjaxRequest( ) )
         {
         }
         if ( cmbEmergenciaEstado.ItemCount > 0 )
         {
            A45EmergenciaEstado = (short)(NumberUtil.Val( cmbEmergenciaEstado.getValidValue(StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0))), "."));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45EmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)));
         }
         if ( context.isAjaxRequest( ) )
         {
            cmbEmergenciaEstado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmergenciaEstado_Internalname, "Values", cmbEmergenciaEstado.ToJavascriptSource(), true);
         }
      }

      protected void Draw( )
      {
         if ( context.isAjaxRequest( ) )
         {
            disableOutput();
         }
         if ( ! GxWebStd.gx_redirect( context) )
         {
            disable_std_buttons( ) ;
            enableDisable( ) ;
            set_caption( ) ;
            /* Form start */
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "Section", "left", "top", " "+"data-abstract-form"+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, divTablemain_Internalname, 1, 0, "px", 0, "px", "Container FormContainer", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Text block */
            GxWebStd.gx_label_ctrl( context, lblTitle_Internalname, "Emergencia Bitacora", "", "", lblTitle_Jsonclick, "'"+""+"'"+",false,"+"'"+""+"'", "", "Title", 0, "", 1, 1, 0, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group ActionGroup", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "btn-group", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 12,'',false,'',0)\"";
            ClassString = "BtnFirst";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_first_Internalname, "", "|<", bttBtn_first_Jsonclick, 5, "|<", "", StyleString, ClassString, bttBtn_first_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EFIRST."+"'", TempTags, "", context.GetButtonType( ), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 14,'',false,'',0)\"";
            ClassString = "BtnPrevious";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_previous_Internalname, "", "<", bttBtn_previous_Jsonclick, 5, "<", "", StyleString, ClassString, bttBtn_previous_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"EPREVIOUS."+"'", TempTags, "", context.GetButtonType( ), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 16,'',false,'',0)\"";
            ClassString = "BtnNext";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_next_Internalname, "", ">", bttBtn_next_Jsonclick, 5, ">", "", StyleString, ClassString, bttBtn_next_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ENEXT."+"'", TempTags, "", context.GetButtonType( ), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 18,'',false,'',0)\"";
            ClassString = "BtnLast";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_last_Internalname, "", ">|", bttBtn_last_Jsonclick, 5, ">|", "", StyleString, ClassString, bttBtn_last_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ELAST."+"'", TempTags, "", context.GetButtonType( ), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 20,'',false,'',0)\"";
            ClassString = "BtnSelect";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_select_Internalname, "", "Seleccionar", bttBtn_select_Jsonclick, 4, "Seleccionar", "", StyleString, ClassString, bttBtn_select_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ESELECT."+"'", TempTags, "gx.popup.openPrompt('"+"gx0070.aspx"+"',["+"{Ctrl:gx.dom.el('"+"EMERGENCIAID"+"'), id:'"+"EMERGENCIAID"+"'"+",IOType:'out',isKey:true,isLastKey:true}"+"],"+"null"+","+"'', false"+","+"true"+");"+"return false;", 2, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            ClassString = "ErrorViewer";
            StyleString = "";
            GxWebStd.gx_msg_list( context, "", context.GX_msglist.DisplayMode, StyleString, ClassString, "", "false");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaID_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaID_Internalname, "ID", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 28,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmergenciaID_Internalname, A43EmergenciaID.ToString(), A43EmergenciaID.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,28);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmergenciaID_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmergenciaID_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaFecha_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaFecha_Internalname, "Fecha", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 33,'',false,'',0)\"";
            context.WriteHtmlText( "<div id=\""+edtEmergenciaFecha_Internalname+"_dp_container\" class=\"dp_container\" style=\"white-space:nowrap;display:inline;\">") ;
            GxWebStd.gx_single_line_edit( context, edtEmergenciaFecha_Internalname, context.localUtil.TToC( A44EmergenciaFecha, 10, 8, 0, 3, "/", ":", " "), context.localUtil.Format( A44EmergenciaFecha, "99/99/99 99:99"), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+"gx.date.valid_date(this, 8,'DMY',5,24,'spa',false,0);"+";gx.evt.onblur(this,33);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmergenciaFecha_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmergenciaFecha_Enabled, 0, "text", "", 14, "chr", 1, "row", 14, 0, 0, 0, 1, -1, 0, true, "", "right", false, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_bitmap( context, edtEmergenciaFecha_Internalname+"_dp_trigger", context.GetImagePath( "61b9b5d3-dff6-4d59-9b00-da61bc2cbe93", "", context.GetTheme( )), "", "", "", "", ((1==0)||(edtEmergenciaFecha_Enabled==0) ? 0 : 1), 0, "Date selector", "Date selector", 0, 1, 0, "", 0, "", 0, 0, 0, "", "", "cursor: pointer;", "", "", "", "", "", "", "", "", 1, false, false, "", "HLP_EmergenciaBitacora.htm");
            context.WriteHtmlTextNl( "</div>") ;
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+cmbEmergenciaEstado_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, cmbEmergenciaEstado_Internalname, "Estado", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 38,'',false,'',0)\"";
            /* ComboBox */
            GxWebStd.gx_combobox_ctrl1( context, cmbEmergenciaEstado, cmbEmergenciaEstado_Internalname, StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)), 1, cmbEmergenciaEstado_Jsonclick, 0, "'"+""+"'"+",false,"+"'"+""+"'", "int", "", 1, cmbEmergenciaEstado.Enabled, 0, 0, 0, "em", 0, "", "", "Attribute", "", "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,38);\"", "", true, "HLP_EmergenciaBitacora.htm");
            cmbEmergenciaEstado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0));
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmergenciaEstado_Internalname, "Values", (String)(cmbEmergenciaEstado.ToJavascriptSource()), true);
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaPaciente_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaPaciente_Internalname, "Paciente", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 43,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmergenciaPaciente_Internalname, A41EmergenciaPaciente.ToString(), A41EmergenciaPaciente.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,43);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmergenciaPaciente_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmergenciaPaciente_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_EmergenciaBitacora.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_41_Internalname, sImgUrl, imgprompt_41_Link, "", "", context.GetTheme( ), imgprompt_41_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaPacienteNombre_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaPacienteNombre_Internalname, "Paciente Nombre", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            GxWebStd.gx_single_line_edit( context, edtEmergenciaPacienteNombre_Internalname, A49EmergenciaPacienteNombre, StringUtil.RTrim( context.localUtil.Format( A49EmergenciaPacienteNombre, "")), "", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmergenciaPacienteNombre_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmergenciaPacienteNombre_Enabled, 0, "text", "", 40, "chr", 1, "row", 40, 0, 0, 0, 1, -1, -1, true, "", "left", true, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaCoach_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaCoach_Internalname, "Coach", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Single line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 53,'',false,'',0)\"";
            GxWebStd.gx_single_line_edit( context, edtEmergenciaCoach_Internalname, A42EmergenciaCoach.ToString(), A42EmergenciaCoach.ToString(), TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,53);\"", "'"+""+"'"+",false,"+"'"+""+"'", "", "", "", "", edtEmergenciaCoach_Jsonclick, 0, "Attribute", "", "", "", "", 1, edtEmergenciaCoach_Enabled, 0, "text", "", 36, "chr", 1, "row", 36, 0, 0, 0, 1, 0, 0, true, "", "", false, "HLP_EmergenciaBitacora.htm");
            /* Static images/pictures */
            ClassString = "gx-prompt Image";
            StyleString = "";
            sImgUrl = (String)(context.GetImagePath( "f5b04895-0024-488b-8e3b-b687ca4598ee", "", context.GetTheme( )));
            GxWebStd.gx_bitmap( context, imgprompt_42_Internalname, sImgUrl, imgprompt_42_Link, "", "", context.GetTheme( ), imgprompt_42_Visible, 1, "", "", 0, 0, 0, "", 0, "", 0, 0, 0, "", "", StyleString, ClassString, "", "", "", "", "", "", "", 1, false, false, context.GetImageSrcSet( sImgUrl), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaLongitud_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaLongitud_Internalname, "Longitud", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 58,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtEmergenciaLongitud_Internalname, A46EmergenciaLongitud, "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,58);\"", 0, 1, edtEmergenciaLongitud_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "2097152", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaLatitud_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaLatitud_Internalname, "Latitud", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 63,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtEmergenciaLatitud_Internalname, A47EmergenciaLatitud, "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,63);\"", 0, 1, edtEmergenciaLatitud_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "2097152", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "form-group gx-form-group", "left", "top", ""+" data-gx-for=\""+edtEmergenciaMensaje_Internalname+"\"", "", "div");
            /* Attribute/Variable Label */
            GxWebStd.gx_label_element( context, edtEmergenciaMensaje_Internalname, "Mensaje", "col-sm-3 AttributeLabel", 1, true);
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-sm-9 gx-attribute", "left", "top", "", "", "div");
            /* Multiple line edit */
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 68,'',false,'',0)\"";
            ClassString = "Attribute";
            StyleString = "";
            ClassString = "Attribute";
            StyleString = "";
            GxWebStd.gx_html_textarea( context, edtEmergenciaMensaje_Internalname, A48EmergenciaMensaje, "", TempTags+" onchange=\"gx.evt.onchange(this, event)\" "+" onblur=\""+""+";gx.evt.onblur(this,68);\"", 0, 1, edtEmergenciaMensaje_Enabled, 0, 80, "chr", 10, "row", StyleString, ClassString, "", "", "2097152", -1, 0, "", "", -1, true, "", "'"+""+"'"+",false,"+"'"+""+"'", 0, "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "row", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "col-xs-12 col-sm-9 col-sm-offset-3", "left", "top", "", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-action-group Confirm", "left", "top", " "+"data-gx-actiongroup-type=\"toolbar\""+" ", "", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 73,'',false,'',0)\"";
            ClassString = "BtnEnter";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_enter_Internalname, "", "Confirmar", bttBtn_enter_Jsonclick, 5, "Confirmar", "", StyleString, ClassString, bttBtn_enter_Visible, bttBtn_enter_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EENTER."+"'", TempTags, "", context.GetButtonType( ), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 75,'',false,'',0)\"";
            ClassString = "BtnCancel";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_cancel_Internalname, "", "Cancelar", bttBtn_cancel_Jsonclick, 1, "Cancelar", "", StyleString, ClassString, bttBtn_cancel_Visible, 1, "standard", "'"+""+"'"+",false,"+"'"+"ECANCEL."+"'", TempTags, "", context.GetButtonType( ), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            /* Div Control */
            GxWebStd.gx_div_start( context, "", 1, 0, "px", 0, "px", "gx-button", "left", "top", "", "", "div");
            TempTags = "  onfocus=\"gx.evt.onfocus(this, 77,'',false,'',0)\"";
            ClassString = "BtnDelete";
            StyleString = "";
            GxWebStd.gx_button_ctrl( context, bttBtn_delete_Internalname, "", "Eliminar", bttBtn_delete_Jsonclick, 5, "Eliminar", "", StyleString, ClassString, bttBtn_delete_Visible, bttBtn_delete_Enabled, "standard", "'"+""+"'"+",false,"+"'"+"EDELETE."+"'", TempTags, "", context.GetButtonType( ), "HLP_EmergenciaBitacora.htm");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            GxWebStd.gx_div_end( context, "left", "top", "div");
            fix_multi_value_controls( ) ;
         }
         /* Execute Exit event if defined. */
      }

      protected void UserMain( )
      {
         standaloneStartup( ) ;
      }

      protected void UserMainFullajax( )
      {
         INITENV( ) ;
         INITTRN( ) ;
         UserMain( ) ;
         Draw( ) ;
         SendCloseFormHiddens( ) ;
      }

      protected void standaloneStartup( )
      {
         standaloneStartupServer( ) ;
         disable_std_buttons( ) ;
         enableDisable( ) ;
         Process( ) ;
      }

      protected void standaloneStartupServer( )
      {
         /* Execute Start event if defined. */
         context.wbGlbDoneStart = 0;
         context.wbGlbDoneStart = 1;
         assign_properties_default( ) ;
         if ( AnyError == 0 )
         {
            if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
            {
               /* Read saved SDTs. */
               /* Read variables values. */
               if ( StringUtil.StrCmp(cgiGet( edtEmergenciaID_Internalname), "") == 0 )
               {
                  A43EmergenciaID = (Guid)(Guid.Empty);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
               }
               else
               {
                  try
                  {
                     A43EmergenciaID = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmergenciaID_Internalname)));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "EMERGENCIAID");
                     AnyError = 1;
                     GX_FocusControl = edtEmergenciaID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               if ( context.localUtil.VCDateTime( cgiGet( edtEmergenciaFecha_Internalname), 2, 0) == 0 )
               {
                  GX_msglist.addItem(context.GetMessage( "GXM_baddatetime", new   object[]  {"Emergencia Fecha"}), 1, "EMERGENCIAFECHA");
                  AnyError = 1;
                  GX_FocusControl = edtEmergenciaFecha_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  wbErr = true;
                  A44EmergenciaFecha = (DateTime)(DateTime.MinValue);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44EmergenciaFecha", context.localUtil.TToC( A44EmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
               }
               else
               {
                  A44EmergenciaFecha = context.localUtil.CToT( cgiGet( edtEmergenciaFecha_Internalname));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44EmergenciaFecha", context.localUtil.TToC( A44EmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
               }
               cmbEmergenciaEstado.CurrentValue = cgiGet( cmbEmergenciaEstado_Internalname);
               A45EmergenciaEstado = (short)(NumberUtil.Val( cgiGet( cmbEmergenciaEstado_Internalname), "."));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45EmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)));
               if ( StringUtil.StrCmp(cgiGet( edtEmergenciaPaciente_Internalname), "") == 0 )
               {
                  A41EmergenciaPaciente = (Guid)(Guid.Empty);
                  n41EmergenciaPaciente = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41EmergenciaPaciente", A41EmergenciaPaciente.ToString());
               }
               else
               {
                  try
                  {
                     A41EmergenciaPaciente = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmergenciaPaciente_Internalname)));
                     n41EmergenciaPaciente = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41EmergenciaPaciente", A41EmergenciaPaciente.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "EMERGENCIAPACIENTE");
                     AnyError = 1;
                     GX_FocusControl = edtEmergenciaPaciente_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               n41EmergenciaPaciente = ((Guid.Empty==A41EmergenciaPaciente) ? true : false);
               A49EmergenciaPacienteNombre = cgiGet( edtEmergenciaPacienteNombre_Internalname);
               n49EmergenciaPacienteNombre = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmergenciaPacienteNombre", A49EmergenciaPacienteNombre);
               if ( StringUtil.StrCmp(cgiGet( edtEmergenciaCoach_Internalname), "") == 0 )
               {
                  A42EmergenciaCoach = (Guid)(Guid.Empty);
                  n42EmergenciaCoach = false;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42EmergenciaCoach", A42EmergenciaCoach.ToString());
               }
               else
               {
                  try
                  {
                     A42EmergenciaCoach = (Guid)(StringUtil.StrToGuid( cgiGet( edtEmergenciaCoach_Internalname)));
                     n42EmergenciaCoach = false;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42EmergenciaCoach", A42EmergenciaCoach.ToString());
                  }
                  catch ( Exception e )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_invalidguid", ""), 1, "EMERGENCIACOACH");
                     AnyError = 1;
                     GX_FocusControl = edtEmergenciaCoach_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     wbErr = true;
                  }
               }
               n42EmergenciaCoach = ((Guid.Empty==A42EmergenciaCoach) ? true : false);
               A46EmergenciaLongitud = cgiGet( edtEmergenciaLongitud_Internalname);
               n46EmergenciaLongitud = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46EmergenciaLongitud", A46EmergenciaLongitud);
               n46EmergenciaLongitud = (String.IsNullOrEmpty(StringUtil.RTrim( A46EmergenciaLongitud)) ? true : false);
               A47EmergenciaLatitud = cgiGet( edtEmergenciaLatitud_Internalname);
               n47EmergenciaLatitud = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47EmergenciaLatitud", A47EmergenciaLatitud);
               n47EmergenciaLatitud = (String.IsNullOrEmpty(StringUtil.RTrim( A47EmergenciaLatitud)) ? true : false);
               A48EmergenciaMensaje = cgiGet( edtEmergenciaMensaje_Internalname);
               n48EmergenciaMensaje = false;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48EmergenciaMensaje", A48EmergenciaMensaje);
               n48EmergenciaMensaje = (String.IsNullOrEmpty(StringUtil.RTrim( A48EmergenciaMensaje)) ? true : false);
               /* Read saved values. */
               Z43EmergenciaID = (Guid)(StringUtil.StrToGuid( cgiGet( "Z43EmergenciaID")));
               Z44EmergenciaFecha = context.localUtil.CToT( cgiGet( "Z44EmergenciaFecha"), 0);
               Z45EmergenciaEstado = (short)(context.localUtil.CToN( cgiGet( "Z45EmergenciaEstado"), ",", "."));
               Z41EmergenciaPaciente = (Guid)(StringUtil.StrToGuid( cgiGet( "Z41EmergenciaPaciente")));
               n41EmergenciaPaciente = ((Guid.Empty==A41EmergenciaPaciente) ? true : false);
               Z42EmergenciaCoach = (Guid)(StringUtil.StrToGuid( cgiGet( "Z42EmergenciaCoach")));
               n42EmergenciaCoach = ((Guid.Empty==A42EmergenciaCoach) ? true : false);
               IsConfirmed = (short)(context.localUtil.CToN( cgiGet( "IsConfirmed"), ",", "."));
               IsModified = (short)(context.localUtil.CToN( cgiGet( "IsModified"), ",", "."));
               Gx_mode = cgiGet( "Mode");
               Gx_BScreen = (short)(context.localUtil.CToN( cgiGet( "vGXBSCREEN"), ",", "."));
               Gx_mode = cgiGet( "vMODE");
               /* Read subfile selected row values. */
               /* Read hidden variables. */
               GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
               standaloneNotModal( ) ;
            }
            else
            {
               standaloneNotModal( ) ;
               if ( StringUtil.StrCmp(gxfirstwebparm, "viewer") == 0 )
               {
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  A43EmergenciaID = (Guid)(StringUtil.StrToGuid( GetNextPar( )));
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
                  getEqualNoModal( ) ;
                  if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A43EmergenciaID) && ( Gx_BScreen == 0 ) )
                  {
                     A43EmergenciaID = (Guid)(Guid.NewGuid( ));
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
                  }
                  Gx_mode = "DSP";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  disable_std_buttons_dsp( ) ;
                  standaloneModal( ) ;
               }
               else
               {
                  getEqualNoModal( ) ;
                  standaloneModal( ) ;
               }
            }
         }
      }

      protected void Process( )
      {
         if ( StringUtil.StrCmp(context.GetRequestMethod( ), "POST") == 0 )
         {
            /* Read Transaction buttons. */
            sEvt = cgiGet( "_EventName");
            EvtGridId = cgiGet( "_EventGridId");
            EvtRowId = cgiGet( "_EventRowId");
            if ( StringUtil.Len( sEvt) > 0 )
            {
               sEvtType = StringUtil.Left( sEvt, 1);
               sEvt = StringUtil.Right( sEvt, (short)(StringUtil.Len( sEvt)-1));
               if ( StringUtil.StrCmp(sEvtType, "M") != 0 )
               {
                  if ( StringUtil.StrCmp(sEvtType, "E") == 0 )
                  {
                     sEvtType = StringUtil.Right( sEvt, 1);
                     if ( StringUtil.StrCmp(sEvtType, ".") == 0 )
                     {
                        sEvt = StringUtil.Left( sEvt, (short)(StringUtil.Len( sEvt)-1));
                        if ( StringUtil.StrCmp(sEvt, "ENTER") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_enter( ) ;
                           /* No code required for Cancel button. It is implemented as the Reset button. */
                        }
                        else if ( StringUtil.StrCmp(sEvt, "FIRST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_first( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "PREVIOUS") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_previous( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "NEXT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_next( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LAST") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_last( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "SELECT") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_select( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "DELETE") == 0 )
                        {
                           context.wbHandled = 1;
                           btn_delete( ) ;
                        }
                        else if ( StringUtil.StrCmp(sEvt, "LSCR") == 0 )
                        {
                           context.wbHandled = 1;
                           AfterKeyLoadScreen( ) ;
                        }
                     }
                     else
                     {
                     }
                  }
                  context.wbHandled = 1;
               }
            }
         }
      }

      protected void AfterTrn( )
      {
         if ( trnEnded == 1 )
         {
            trnEnded = 0;
            standaloneNotModal( ) ;
            standaloneModal( ) ;
            if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  )
            {
               /* Clear variables for new insertion. */
               InitAll057( ) ;
               standaloneNotModal( ) ;
               standaloneModal( ) ;
            }
         }
      }

      public override String ToString( )
      {
         return "" ;
      }

      public GxContentInfo GetContentInfo( )
      {
         return (GxContentInfo)(null) ;
      }

      protected void disable_std_buttons( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
      }

      protected void disable_std_buttons_dsp( )
      {
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         bttBtn_first_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_first_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_first_Visible), 5, 0)), true);
         bttBtn_previous_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_previous_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_previous_Visible), 5, 0)), true);
         bttBtn_next_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_next_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_next_Visible), 5, 0)), true);
         bttBtn_last_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_last_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_last_Visible), 5, 0)), true);
         bttBtn_select_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_select_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_select_Visible), 5, 0)), true);
         bttBtn_delete_Visible = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Visible), 5, 0)), true);
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Visible = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Visible", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Visible), 5, 0)), true);
         }
         DisableAttributes057( ) ;
      }

      protected void set_caption( )
      {
         if ( ( IsConfirmed == 1 ) && ( AnyError == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_confdelete", ""), 0, "", true);
            }
            else
            {
               GX_msglist.addItem(context.GetMessage( "GXM_mustconfirm", ""), 0, "", true);
            }
         }
      }

      protected void ResetCaption050( )
      {
      }

      protected void ZM057( short GX_JID )
      {
         if ( ( GX_JID == 7 ) || ( GX_JID == 0 ) )
         {
            if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
            {
               Z44EmergenciaFecha = T00053_A44EmergenciaFecha[0];
               Z45EmergenciaEstado = T00053_A45EmergenciaEstado[0];
               Z41EmergenciaPaciente = (Guid)(T00053_A41EmergenciaPaciente[0]);
               Z42EmergenciaCoach = (Guid)(T00053_A42EmergenciaCoach[0]);
            }
            else
            {
               Z44EmergenciaFecha = A44EmergenciaFecha;
               Z45EmergenciaEstado = A45EmergenciaEstado;
               Z41EmergenciaPaciente = (Guid)(A41EmergenciaPaciente);
               Z42EmergenciaCoach = (Guid)(A42EmergenciaCoach);
            }
         }
         if ( GX_JID == -7 )
         {
            Z43EmergenciaID = (Guid)(A43EmergenciaID);
            Z44EmergenciaFecha = A44EmergenciaFecha;
            Z45EmergenciaEstado = A45EmergenciaEstado;
            Z46EmergenciaLongitud = A46EmergenciaLongitud;
            Z47EmergenciaLatitud = A47EmergenciaLatitud;
            Z48EmergenciaMensaje = A48EmergenciaMensaje;
            Z41EmergenciaPaciente = (Guid)(A41EmergenciaPaciente);
            Z42EmergenciaCoach = (Guid)(A42EmergenciaCoach);
            Z49EmergenciaPacienteNombre = A49EmergenciaPacienteNombre;
         }
      }

      protected void standaloneNotModal( )
      {
         Gx_BScreen = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         imgprompt_41_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"EMERGENCIAPACIENTE"+"'), id:'"+"EMERGENCIAPACIENTE"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
         imgprompt_42_Link = ((StringUtil.StrCmp(Gx_mode, "DSP")==0) ? "" : "javascript:"+"gx.popup.openPrompt('"+"gx0010.aspx"+"',["+"{Ctrl:gx.dom.el('"+"EMERGENCIACOACH"+"'), id:'"+"EMERGENCIACOACH"+"'"+",IOType:'out'}"+"],"+"null"+","+"'', false"+","+"false"+");");
      }

      protected void standaloneModal( )
      {
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )  && (Guid.Empty==A43EmergenciaID) && ( Gx_BScreen == 0 ) )
         {
            A43EmergenciaID = (Guid)(Guid.NewGuid( ));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
         }
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            bttBtn_delete_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_delete_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_delete_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_delete_Enabled), 5, 0)), true);
         }
         if ( StringUtil.StrCmp(Gx_mode, "DSP") == 0 )
         {
            bttBtn_enter_Enabled = 0;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         else
         {
            bttBtn_enter_Enabled = 1;
            context.httpAjaxContext.ajax_rsp_assign_prop("", false, bttBtn_enter_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(bttBtn_enter_Enabled), 5, 0)), true);
         }
         if ( ( StringUtil.StrCmp(Gx_mode, "INS") == 0 ) && ( Gx_BScreen == 0 ) )
         {
         }
      }

      protected void Load057( )
      {
         /* Using cursor T00056 */
         pr_default.execute(4, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(4) != 101) )
         {
            RcdFound7 = 1;
            A44EmergenciaFecha = T00056_A44EmergenciaFecha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44EmergenciaFecha", context.localUtil.TToC( A44EmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
            A45EmergenciaEstado = T00056_A45EmergenciaEstado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45EmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)));
            A49EmergenciaPacienteNombre = T00056_A49EmergenciaPacienteNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmergenciaPacienteNombre", A49EmergenciaPacienteNombre);
            n49EmergenciaPacienteNombre = T00056_n49EmergenciaPacienteNombre[0];
            A46EmergenciaLongitud = T00056_A46EmergenciaLongitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46EmergenciaLongitud", A46EmergenciaLongitud);
            n46EmergenciaLongitud = T00056_n46EmergenciaLongitud[0];
            A47EmergenciaLatitud = T00056_A47EmergenciaLatitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47EmergenciaLatitud", A47EmergenciaLatitud);
            n47EmergenciaLatitud = T00056_n47EmergenciaLatitud[0];
            A48EmergenciaMensaje = T00056_A48EmergenciaMensaje[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48EmergenciaMensaje", A48EmergenciaMensaje);
            n48EmergenciaMensaje = T00056_n48EmergenciaMensaje[0];
            A41EmergenciaPaciente = (Guid)((Guid)(T00056_A41EmergenciaPaciente[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41EmergenciaPaciente", A41EmergenciaPaciente.ToString());
            n41EmergenciaPaciente = T00056_n41EmergenciaPaciente[0];
            A42EmergenciaCoach = (Guid)((Guid)(T00056_A42EmergenciaCoach[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42EmergenciaCoach", A42EmergenciaCoach.ToString());
            n42EmergenciaCoach = T00056_n42EmergenciaCoach[0];
            ZM057( -7) ;
         }
         pr_default.close(4);
         OnLoadActions057( ) ;
      }

      protected void OnLoadActions057( )
      {
      }

      protected void CheckExtendedTable057( )
      {
         Gx_BScreen = 1;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_BScreen", StringUtil.Str( (decimal)(Gx_BScreen), 1, 0));
         standaloneModal( ) ;
         if ( ! ( (DateTime.MinValue==A44EmergenciaFecha) || ( A44EmergenciaFecha >= context.localUtil.YMDHMSToT( 1753, 1, 1, 0, 0, 0) ) ) )
         {
            GX_msglist.addItem("Campo Emergencia Fecha fuera de rango", "OutOfRange", 1, "EMERGENCIAFECHA");
            AnyError = 1;
            GX_FocusControl = edtEmergenciaFecha_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( ! ( ( A45EmergenciaEstado == 1 ) || ( A45EmergenciaEstado == 2 ) ) )
         {
            GX_msglist.addItem("Campo Emergencia Estado fuera de rango", "OutOfRange", 1, "EMERGENCIAESTADO");
            AnyError = 1;
            GX_FocusControl = cmbEmergenciaEstado_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         /* Using cursor T00054 */
         pr_default.execute(2, new Object[] {n41EmergenciaPaciente, A41EmergenciaPaciente});
         if ( (pr_default.getStatus(2) == 101) )
         {
            if ( ! ( (Guid.Empty==A41EmergenciaPaciente) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Paciente ST'.", "ForeignKeyNotFound", 1, "EMERGENCIAPACIENTE");
               AnyError = 1;
               GX_FocusControl = edtEmergenciaPaciente_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A49EmergenciaPacienteNombre = T00054_A49EmergenciaPacienteNombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmergenciaPacienteNombre", A49EmergenciaPacienteNombre);
         n49EmergenciaPacienteNombre = T00054_n49EmergenciaPacienteNombre[0];
         pr_default.close(2);
         /* Using cursor T00055 */
         pr_default.execute(3, new Object[] {n42EmergenciaCoach, A42EmergenciaCoach});
         if ( (pr_default.getStatus(3) == 101) )
         {
            if ( ! ( (Guid.Empty==A42EmergenciaCoach) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Coach ST'.", "ForeignKeyNotFound", 1, "EMERGENCIACOACH");
               AnyError = 1;
               GX_FocusControl = edtEmergenciaCoach_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         pr_default.close(3);
      }

      protected void CloseExtendedTableCursors057( )
      {
         pr_default.close(2);
         pr_default.close(3);
      }

      protected void enableDisable( )
      {
      }

      protected void gxLoad_8( Guid A41EmergenciaPaciente )
      {
         /* Using cursor T00057 */
         pr_default.execute(5, new Object[] {n41EmergenciaPaciente, A41EmergenciaPaciente});
         if ( (pr_default.getStatus(5) == 101) )
         {
            if ( ! ( (Guid.Empty==A41EmergenciaPaciente) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Paciente ST'.", "ForeignKeyNotFound", 1, "EMERGENCIAPACIENTE");
               AnyError = 1;
               GX_FocusControl = edtEmergenciaPaciente_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         A49EmergenciaPacienteNombre = T00057_A49EmergenciaPacienteNombre[0];
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmergenciaPacienteNombre", A49EmergenciaPacienteNombre);
         n49EmergenciaPacienteNombre = T00057_n49EmergenciaPacienteNombre[0];
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("\""+GXUtil.EncodeJSConstant( A49EmergenciaPacienteNombre)+"\"");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(5) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(5);
      }

      protected void gxLoad_9( Guid A42EmergenciaCoach )
      {
         /* Using cursor T00058 */
         pr_default.execute(6, new Object[] {n42EmergenciaCoach, A42EmergenciaCoach});
         if ( (pr_default.getStatus(6) == 101) )
         {
            if ( ! ( (Guid.Empty==A42EmergenciaCoach) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Coach ST'.", "ForeignKeyNotFound", 1, "EMERGENCIACOACH");
               AnyError = 1;
               GX_FocusControl = edtEmergenciaCoach_Internalname;
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         GxWebStd.set_html_headers( context, 0, "", "");
         context.GX_webresponse.AddString("[[");
         context.GX_webresponse.AddString("");
         context.GX_webresponse.AddString("]");
         if ( (pr_default.getStatus(6) == 101) )
         {
            context.GX_webresponse.AddString(",");
            context.GX_webresponse.AddString("101");
         }
         context.GX_webresponse.AddString(")");
         pr_default.close(6);
      }

      protected void GetKey057( )
      {
         /* Using cursor T00059 */
         pr_default.execute(7, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(7) != 101) )
         {
            RcdFound7 = 1;
         }
         else
         {
            RcdFound7 = 0;
         }
         pr_default.close(7);
      }

      protected void getByPrimaryKey( )
      {
         /* Using cursor T00053 */
         pr_default.execute(1, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(1) != 101) )
         {
            ZM057( 7) ;
            RcdFound7 = 1;
            A43EmergenciaID = (Guid)((Guid)(T00053_A43EmergenciaID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
            A44EmergenciaFecha = T00053_A44EmergenciaFecha[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44EmergenciaFecha", context.localUtil.TToC( A44EmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
            A45EmergenciaEstado = T00053_A45EmergenciaEstado[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45EmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)));
            A46EmergenciaLongitud = T00053_A46EmergenciaLongitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46EmergenciaLongitud", A46EmergenciaLongitud);
            n46EmergenciaLongitud = T00053_n46EmergenciaLongitud[0];
            A47EmergenciaLatitud = T00053_A47EmergenciaLatitud[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47EmergenciaLatitud", A47EmergenciaLatitud);
            n47EmergenciaLatitud = T00053_n47EmergenciaLatitud[0];
            A48EmergenciaMensaje = T00053_A48EmergenciaMensaje[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48EmergenciaMensaje", A48EmergenciaMensaje);
            n48EmergenciaMensaje = T00053_n48EmergenciaMensaje[0];
            A41EmergenciaPaciente = (Guid)((Guid)(T00053_A41EmergenciaPaciente[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41EmergenciaPaciente", A41EmergenciaPaciente.ToString());
            n41EmergenciaPaciente = T00053_n41EmergenciaPaciente[0];
            A42EmergenciaCoach = (Guid)((Guid)(T00053_A42EmergenciaCoach[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42EmergenciaCoach", A42EmergenciaCoach.ToString());
            n42EmergenciaCoach = T00053_n42EmergenciaCoach[0];
            Z43EmergenciaID = (Guid)(A43EmergenciaID);
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Load057( ) ;
            if ( AnyError == 1 )
            {
               RcdFound7 = 0;
               InitializeNonKey057( ) ;
            }
            Gx_mode = sMode7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            RcdFound7 = 0;
            InitializeNonKey057( ) ;
            sMode7 = Gx_mode;
            Gx_mode = "DSP";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
            standaloneModal( ) ;
            Gx_mode = sMode7;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         pr_default.close(1);
      }

      protected void getEqualNoModal( )
      {
         GetKey057( ) ;
         if ( RcdFound7 == 0 )
         {
            Gx_mode = "INS";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         getByPrimaryKey( ) ;
      }

      protected void move_next( )
      {
         RcdFound7 = 0;
         /* Using cursor T000510 */
         pr_default.execute(8, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(8) != 101) )
         {
            while ( (pr_default.getStatus(8) != 101) && ( ( GuidUtil.Compare(T000510_A43EmergenciaID[0], A43EmergenciaID, 1) < 0 ) ) )
            {
               pr_default.readNext(8);
            }
            if ( (pr_default.getStatus(8) != 101) && ( ( GuidUtil.Compare(T000510_A43EmergenciaID[0], A43EmergenciaID, 1) > 0 ) ) )
            {
               A43EmergenciaID = (Guid)((Guid)(T000510_A43EmergenciaID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
               RcdFound7 = 1;
            }
         }
         pr_default.close(8);
      }

      protected void move_previous( )
      {
         RcdFound7 = 0;
         /* Using cursor T000511 */
         pr_default.execute(9, new Object[] {A43EmergenciaID});
         if ( (pr_default.getStatus(9) != 101) )
         {
            while ( (pr_default.getStatus(9) != 101) && ( ( GuidUtil.Compare(T000511_A43EmergenciaID[0], A43EmergenciaID, 1) > 0 ) ) )
            {
               pr_default.readNext(9);
            }
            if ( (pr_default.getStatus(9) != 101) && ( ( GuidUtil.Compare(T000511_A43EmergenciaID[0], A43EmergenciaID, 1) < 0 ) ) )
            {
               A43EmergenciaID = (Guid)((Guid)(T000511_A43EmergenciaID[0]));
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
               RcdFound7 = 1;
            }
         }
         pr_default.close(9);
      }

      protected void btn_enter( )
      {
         nKeyPressed = 1;
         GetKey057( ) ;
         if ( StringUtil.StrCmp(Gx_mode, "INS") == 0 )
         {
            /* Insert record */
            GX_FocusControl = edtEmergenciaID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            Insert057( ) ;
            if ( AnyError == 1 )
            {
               GX_FocusControl = "";
               context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
            }
         }
         else
         {
            if ( RcdFound7 == 1 )
            {
               if ( A43EmergenciaID != Z43EmergenciaID )
               {
                  A43EmergenciaID = (Guid)(Z43EmergenciaID);
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
                  GX_msglist.addItem(context.GetMessage( "GXM_getbeforeupd", ""), "CandidateKeyNotFound", 1, "EMERGENCIAID");
                  AnyError = 1;
                  GX_FocusControl = edtEmergenciaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else if ( StringUtil.StrCmp(Gx_mode, "DLT") == 0 )
               {
                  delete( ) ;
                  AfterTrn( ) ;
                  GX_FocusControl = edtEmergenciaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
               else
               {
                  Gx_mode = "UPD";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Update record */
                  Update057( ) ;
                  GX_FocusControl = edtEmergenciaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
               }
            }
            else
            {
               if ( A43EmergenciaID != Z43EmergenciaID )
               {
                  Gx_mode = "INS";
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                  /* Insert record */
                  GX_FocusControl = edtEmergenciaID_Internalname;
                  context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  Insert057( ) ;
                  if ( AnyError == 1 )
                  {
                     GX_FocusControl = "";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
               }
               else
               {
                  if ( StringUtil.StrCmp(Gx_mode, "UPD") == 0 )
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_recdeleted", ""), 1, "EMERGENCIAID");
                     AnyError = 1;
                     GX_FocusControl = edtEmergenciaID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                  }
                  else
                  {
                     Gx_mode = "INS";
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                     /* Insert record */
                     GX_FocusControl = edtEmergenciaID_Internalname;
                     context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     Insert057( ) ;
                     if ( AnyError == 1 )
                     {
                        GX_FocusControl = "";
                        context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
                     }
                  }
               }
            }
         }
         AfterTrn( ) ;
      }

      protected void btn_delete( )
      {
         if ( A43EmergenciaID != Z43EmergenciaID )
         {
            A43EmergenciaID = (Guid)(Z43EmergenciaID);
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
            GX_msglist.addItem(context.GetMessage( "GXM_getbeforedlt", ""), 1, "EMERGENCIAID");
            AnyError = 1;
            GX_FocusControl = edtEmergenciaID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         else
         {
            delete( ) ;
            AfterTrn( ) ;
            GX_FocusControl = edtEmergenciaID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         if ( AnyError != 0 )
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         else
         {
            getByPrimaryKey( ) ;
         }
         CloseOpenCursors();
      }

      protected void btn_get( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         if ( RcdFound7 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_keynfound", ""), "PrimaryKeyNotFound", 1, "EMERGENCIAID");
            AnyError = 1;
            GX_FocusControl = edtEmergenciaID_Internalname;
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         }
         GX_FocusControl = edtEmergenciaFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_first( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart057( ) ;
         if ( RcdFound7 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmergenciaFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd057( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_previous( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_previous( ) ;
         if ( RcdFound7 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmergenciaFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_next( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         move_next( ) ;
         if ( RcdFound7 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmergenciaFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_last( )
      {
         nKeyPressed = 2;
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         ScanStart057( ) ;
         if ( RcdFound7 == 0 )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_norectobrow", ""), 0, "", true);
         }
         else
         {
            while ( RcdFound7 != 0 )
            {
               ScanNext057( ) ;
            }
            Gx_mode = "UPD";
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         }
         GX_FocusControl = edtEmergenciaFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         ScanEnd057( ) ;
         getByPrimaryKey( ) ;
         standaloneNotModal( ) ;
         standaloneModal( ) ;
      }

      protected void btn_select( )
      {
         getEqualNoModal( ) ;
      }

      protected void CheckOptimisticConcurrency057( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            /* Using cursor T00052 */
            pr_default.execute(0, new Object[] {A43EmergenciaID});
            if ( (pr_default.getStatus(0) == 103) )
            {
               GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"EmergenciaBitacora"}), "RecordIsLocked", 1, "");
               AnyError = 1;
               return  ;
            }
            if ( (pr_default.getStatus(0) == 101) || ( Z44EmergenciaFecha != T00052_A44EmergenciaFecha[0] ) || ( Z45EmergenciaEstado != T00052_A45EmergenciaEstado[0] ) || ( Z41EmergenciaPaciente != T00052_A41EmergenciaPaciente[0] ) || ( Z42EmergenciaCoach != T00052_A42EmergenciaCoach[0] ) )
            {
               if ( Z44EmergenciaFecha != T00052_A44EmergenciaFecha[0] )
               {
                  GXUtil.WriteLog("emergenciabitacora:[seudo value changed for attri]"+"EmergenciaFecha");
                  GXUtil.WriteLogRaw("Old: ",Z44EmergenciaFecha);
                  GXUtil.WriteLogRaw("Current: ",T00052_A44EmergenciaFecha[0]);
               }
               if ( Z45EmergenciaEstado != T00052_A45EmergenciaEstado[0] )
               {
                  GXUtil.WriteLog("emergenciabitacora:[seudo value changed for attri]"+"EmergenciaEstado");
                  GXUtil.WriteLogRaw("Old: ",Z45EmergenciaEstado);
                  GXUtil.WriteLogRaw("Current: ",T00052_A45EmergenciaEstado[0]);
               }
               if ( Z41EmergenciaPaciente != T00052_A41EmergenciaPaciente[0] )
               {
                  GXUtil.WriteLog("emergenciabitacora:[seudo value changed for attri]"+"EmergenciaPaciente");
                  GXUtil.WriteLogRaw("Old: ",Z41EmergenciaPaciente);
                  GXUtil.WriteLogRaw("Current: ",T00052_A41EmergenciaPaciente[0]);
               }
               if ( Z42EmergenciaCoach != T00052_A42EmergenciaCoach[0] )
               {
                  GXUtil.WriteLog("emergenciabitacora:[seudo value changed for attri]"+"EmergenciaCoach");
                  GXUtil.WriteLogRaw("Old: ",Z42EmergenciaCoach);
                  GXUtil.WriteLogRaw("Current: ",T00052_A42EmergenciaCoach[0]);
               }
               GX_msglist.addItem(context.GetMessage( "GXM_waschg", new   object[]  {"EmergenciaBitacora"}), "RecordWasChanged", 1, "");
               AnyError = 1;
               return  ;
            }
         }
      }

      protected void Insert057( )
      {
         if ( ! IsAuthorized("emergenciabitacora_Insert") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate057( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable057( ) ;
         }
         if ( AnyError == 0 )
         {
            ZM057( 0) ;
            CheckOptimisticConcurrency057( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm057( ) ;
               if ( AnyError == 0 )
               {
                  BeforeInsert057( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000512 */
                     pr_default.execute(10, new Object[] {A44EmergenciaFecha, A45EmergenciaEstado, n46EmergenciaLongitud, A46EmergenciaLongitud, n47EmergenciaLatitud, A47EmergenciaLatitud, n48EmergenciaMensaje, A48EmergenciaMensaje, n41EmergenciaPaciente, A41EmergenciaPaciente, n42EmergenciaCoach, A42EmergenciaCoach, A43EmergenciaID});
                     pr_default.close(10);
                     dsDefault.SmartCacheProvider.SetUpdated("EmergenciaBitacora") ;
                     if ( (pr_default.getStatus(10) == 1) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_noupdate", ""), "DuplicatePrimaryKey", 1, "");
                        AnyError = 1;
                     }
                     if ( AnyError == 0 )
                     {
                        /* Start of After( Insert) rules */
                        /* End of After( Insert) rules */
                        if ( AnyError == 0 )
                        {
                           /* Save values for previous() function. */
                           GX_msglist.addItem(context.GetMessage( "GXM_sucadded", ""), "SuccessfullyAdded", 0, "", true);
                           ResetCaption050( ) ;
                        }
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
            else
            {
               Load057( ) ;
            }
            EndLevel057( ) ;
         }
         CloseExtendedTableCursors057( ) ;
      }

      protected void Update057( )
      {
         if ( ! IsAuthorized("emergenciabitacora_Update") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         BeforeValidate057( ) ;
         if ( AnyError == 0 )
         {
            CheckExtendedTable057( ) ;
         }
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency057( ) ;
            if ( AnyError == 0 )
            {
               AfterConfirm057( ) ;
               if ( AnyError == 0 )
               {
                  BeforeUpdate057( ) ;
                  if ( AnyError == 0 )
                  {
                     /* Using cursor T000513 */
                     pr_default.execute(11, new Object[] {A44EmergenciaFecha, A45EmergenciaEstado, n46EmergenciaLongitud, A46EmergenciaLongitud, n47EmergenciaLatitud, A47EmergenciaLatitud, n48EmergenciaMensaje, A48EmergenciaMensaje, n41EmergenciaPaciente, A41EmergenciaPaciente, n42EmergenciaCoach, A42EmergenciaCoach, A43EmergenciaID});
                     pr_default.close(11);
                     dsDefault.SmartCacheProvider.SetUpdated("EmergenciaBitacora") ;
                     if ( (pr_default.getStatus(11) == 103) )
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_lock", new   object[]  {"EmergenciaBitacora"}), "RecordIsLocked", 1, "");
                        AnyError = 1;
                     }
                     DeferredUpdate057( ) ;
                     if ( AnyError == 0 )
                     {
                        /* Start of After( update) rules */
                        /* End of After( update) rules */
                        if ( AnyError == 0 )
                        {
                           getByPrimaryKey( ) ;
                           GX_msglist.addItem(context.GetMessage( "GXM_sucupdated", ""), "SuccessfullyUpdated", 0, "", true);
                           ResetCaption050( ) ;
                        }
                     }
                     else
                     {
                        GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                        AnyError = 1;
                     }
                  }
               }
            }
            EndLevel057( ) ;
         }
         CloseExtendedTableCursors057( ) ;
      }

      protected void DeferredUpdate057( )
      {
      }

      protected void delete( )
      {
         if ( ! IsAuthorized("emergenciabitacora_Delete") )
         {
            GX_msglist.addItem(context.GetMessage( "GXM_notauthorized", ""), 1, "");
            AnyError = 1;
            return  ;
         }
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         BeforeValidate057( ) ;
         if ( AnyError == 0 )
         {
            CheckOptimisticConcurrency057( ) ;
         }
         if ( AnyError == 0 )
         {
            OnDeleteControls057( ) ;
            AfterConfirm057( ) ;
            if ( AnyError == 0 )
            {
               BeforeDelete057( ) ;
               if ( AnyError == 0 )
               {
                  /* No cascading delete specified. */
                  /* Using cursor T000514 */
                  pr_default.execute(12, new Object[] {A43EmergenciaID});
                  pr_default.close(12);
                  dsDefault.SmartCacheProvider.SetUpdated("EmergenciaBitacora") ;
                  if ( AnyError == 0 )
                  {
                     /* Start of After( delete) rules */
                     /* End of After( delete) rules */
                     if ( AnyError == 0 )
                     {
                        move_next( ) ;
                        if ( RcdFound7 == 0 )
                        {
                           InitAll057( ) ;
                           Gx_mode = "INS";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        else
                        {
                           getByPrimaryKey( ) ;
                           Gx_mode = "UPD";
                           context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
                        }
                        GX_msglist.addItem(context.GetMessage( "GXM_sucdeleted", ""), "SuccessfullyDeleted", 0, "", true);
                        ResetCaption050( ) ;
                     }
                  }
                  else
                  {
                     GX_msglist.addItem(context.GetMessage( "GXM_unexp", ""), 1, "");
                     AnyError = 1;
                  }
               }
            }
         }
         sMode7 = Gx_mode;
         Gx_mode = "DLT";
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
         EndLevel057( ) ;
         Gx_mode = sMode7;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "Gx_mode", Gx_mode);
      }

      protected void OnDeleteControls057( )
      {
         standaloneModal( ) ;
         if ( AnyError == 0 )
         {
            /* Delete mode formulas */
            /* Using cursor T000515 */
            pr_default.execute(13, new Object[] {n41EmergenciaPaciente, A41EmergenciaPaciente});
            A49EmergenciaPacienteNombre = T000515_A49EmergenciaPacienteNombre[0];
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmergenciaPacienteNombre", A49EmergenciaPacienteNombre);
            n49EmergenciaPacienteNombre = T000515_n49EmergenciaPacienteNombre[0];
            pr_default.close(13);
         }
      }

      protected void EndLevel057( )
      {
         if ( StringUtil.StrCmp(Gx_mode, "INS") != 0 )
         {
            pr_default.close(0);
         }
         if ( AnyError == 0 )
         {
            BeforeComplete057( ) ;
         }
         if ( AnyError == 0 )
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_gam.commit( "EmergenciaBitacora");
            pr_default.commit( "EmergenciaBitacora");
            if ( AnyError == 0 )
            {
               ConfirmValues050( ) ;
            }
            /* After transaction rules */
            /* Execute 'After Trn' event if defined. */
            trnEnded = 1;
         }
         else
         {
            pr_default.close(1);
            pr_default.close(13);
            pr_gam.rollback( "EmergenciaBitacora");
            pr_default.rollback( "EmergenciaBitacora");
         }
         IsModified = 0;
         if ( AnyError != 0 )
         {
            context.wjLoc = "";
            context.nUserReturn = 0;
         }
      }

      public void ScanStart057( )
      {
         /* Using cursor T000516 */
         pr_default.execute(14);
         RcdFound7 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound7 = 1;
            A43EmergenciaID = (Guid)((Guid)(T000516_A43EmergenciaID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
         }
         /* Load Subordinate Levels */
      }

      protected void ScanNext057( )
      {
         /* Scan next routine */
         pr_default.readNext(14);
         RcdFound7 = 0;
         if ( (pr_default.getStatus(14) != 101) )
         {
            RcdFound7 = 1;
            A43EmergenciaID = (Guid)((Guid)(T000516_A43EmergenciaID[0]));
            context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
         }
      }

      protected void ScanEnd057( )
      {
         pr_default.close(14);
      }

      protected void AfterConfirm057( )
      {
         /* After Confirm Rules */
      }

      protected void BeforeInsert057( )
      {
         /* Before Insert Rules */
      }

      protected void BeforeUpdate057( )
      {
         /* Before Update Rules */
      }

      protected void BeforeDelete057( )
      {
         /* Before Delete Rules */
      }

      protected void BeforeComplete057( )
      {
         /* Before Complete Rules */
      }

      protected void BeforeValidate057( )
      {
         /* Before Validate Rules */
      }

      protected void DisableAttributes057( )
      {
         edtEmergenciaID_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaID_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaID_Enabled), 5, 0)), true);
         edtEmergenciaFecha_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaFecha_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaFecha_Enabled), 5, 0)), true);
         cmbEmergenciaEstado.Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, cmbEmergenciaEstado_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(cmbEmergenciaEstado.Enabled), 5, 0)), true);
         edtEmergenciaPaciente_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaPaciente_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaPaciente_Enabled), 5, 0)), true);
         edtEmergenciaPacienteNombre_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaPacienteNombre_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaPacienteNombre_Enabled), 5, 0)), true);
         edtEmergenciaCoach_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaCoach_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaCoach_Enabled), 5, 0)), true);
         edtEmergenciaLongitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaLongitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaLongitud_Enabled), 5, 0)), true);
         edtEmergenciaLatitud_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaLatitud_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaLatitud_Enabled), 5, 0)), true);
         edtEmergenciaMensaje_Enabled = 0;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, edtEmergenciaMensaje_Internalname, "Enabled", StringUtil.LTrim( StringUtil.Str( (decimal)(edtEmergenciaMensaje_Enabled), 5, 0)), true);
      }

      protected void send_integrity_lvl_hashes057( )
      {
      }

      protected void assign_properties_default( )
      {
      }

      protected void ConfirmValues050( )
      {
      }

      public override void RenderHtmlHeaders( )
      {
         GxWebStd.gx_html_headers( context, 0, "", "", Form.Meta, Form.Metaequiv, true);
      }

      public override void RenderHtmlOpenForm( )
      {
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.WriteHtmlText( "<title>") ;
         context.SendWebValue( Form.Caption) ;
         context.WriteHtmlTextNl( "</title>") ;
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         if ( StringUtil.Len( sDynURL) > 0 )
         {
            context.WriteHtmlText( "<BASE href=\""+sDynURL+"\" />") ;
         }
         define_styles( ) ;
         MasterPageObj.master_styles();
         if ( ( ( context.GetBrowserType( ) == 1 ) || ( context.GetBrowserType( ) == 5 ) ) && ( StringUtil.StrCmp(context.GetBrowserVersion( ), "7.0") == 0 ) )
         {
            context.AddJavascriptSource("json2.js", "?"+context.GetBuildNumber( 121631), false);
         }
         context.AddJavascriptSource("jquery.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxtimezone.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxgral.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("bootstrap/js/bootstrap.min.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("gxcfg.js", "?201811181734858", false);
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         context.AddJavascriptSource("calendar.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-setup.js", "?"+context.GetBuildNumber( 121631), false);
         context.AddJavascriptSource("calendar-es.js", "?"+context.GetBuildNumber( 121631), false);
         context.WriteHtmlText( Form.Headerrawhtml) ;
         context.CloseHtmlHeader();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         FormProcess = " data-HasEnter=\"true\" data-Skiponenter=\"false\"";
         context.WriteHtmlText( "<body ") ;
         bodyStyle = "" + "background-color:" + context.BuildHTMLColor( Form.Backcolor) + ";color:" + context.BuildHTMLColor( Form.Textcolor) + ";";
         bodyStyle = bodyStyle + ";-moz-opacity:0;opacity:0;";
         if ( ! ( String.IsNullOrEmpty(StringUtil.RTrim( Form.Background)) ) )
         {
            bodyStyle = bodyStyle + " background-image:url(" + context.convertURL( Form.Background) + ")";
         }
         context.WriteHtmlText( " "+"class=\"form-horizontal Form\""+" "+ "style='"+bodyStyle+"'") ;
         context.WriteHtmlText( FormProcess+">") ;
         context.skipLines(1);
         context.WriteHtmlTextNl( "<form id=\"MAINFORM\" name=\"MAINFORM\" method=\"post\" tabindex=-1  class=\"form-horizontal Form\" data-gx-class=\"form-horizontal Form\" novalidate action=\""+formatLink("emergenciabitacora.aspx") +"\">") ;
         GxWebStd.gx_hidden_field( context, "_EventName", "");
         GxWebStd.gx_hidden_field( context, "_EventGridId", "");
         GxWebStd.gx_hidden_field( context, "_EventRowId", "");
         context.WriteHtmlText( "<input type=\"submit\" style=\"display:none\">") ;
         context.httpAjaxContext.ajax_rsp_assign_prop("", false, "FORM", "Class", "form-horizontal Form", true);
         toggleJsOutput = isJsOutputEnabled( );
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
      }

      protected void send_integrity_footer_hashes( )
      {
         GXKey = Crypto.Decrypt64( context.GetCookie( "GX_SESSION_ID"), Crypto.GetServerKey( ));
      }

      protected void SendCloseFormHiddens( )
      {
         /* Send hidden variables. */
         /* Send saved values. */
         send_integrity_footer_hashes( ) ;
         GxWebStd.gx_hidden_field( context, "Z43EmergenciaID", Z43EmergenciaID.ToString());
         GxWebStd.gx_hidden_field( context, "Z44EmergenciaFecha", context.localUtil.TToC( Z44EmergenciaFecha, 10, 8, 0, 0, "/", ":", " "));
         GxWebStd.gx_hidden_field( context, "Z45EmergenciaEstado", StringUtil.LTrim( StringUtil.NToC( (decimal)(Z45EmergenciaEstado), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Z41EmergenciaPaciente", Z41EmergenciaPaciente.ToString());
         GxWebStd.gx_hidden_field( context, "Z42EmergenciaCoach", Z42EmergenciaCoach.ToString());
         GxWebStd.gx_hidden_field( context, "IsConfirmed", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsConfirmed), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "IsModified", StringUtil.LTrim( StringUtil.NToC( (decimal)(IsModified), 4, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "Mode", StringUtil.RTrim( Gx_mode));
         GxWebStd.gx_hidden_field( context, "vGXBSCREEN", StringUtil.LTrim( StringUtil.NToC( (decimal)(Gx_BScreen), 1, 0, ",", "")));
         GxWebStd.gx_hidden_field( context, "vMODE", StringUtil.RTrim( Gx_mode));
      }

      public override void RenderHtmlCloseForm( )
      {
         SendCloseFormHiddens( ) ;
         GxWebStd.gx_hidden_field( context, "GX_FocusControl", GX_FocusControl);
         SendAjaxEncryptionKey();
         SendComponentObjects();
         SendServerCommands();
         SendState();
         if ( context.isSpaRequest( ) )
         {
            disableOutput();
         }
         context.WriteHtmlTextNl( "</form>") ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         include_jscripts( ) ;
      }

      public override short ExecuteStartEvent( )
      {
         standaloneStartup( ) ;
         gxajaxcallmode = (short)((isAjaxCallMode( ) ? 1 : 0));
         return gxajaxcallmode ;
      }

      public override void RenderHtmlContent( )
      {
         context.WriteHtmlText( "<div") ;
         GxWebStd.ClassAttribute( context, "gx-ct-body"+" "+(String.IsNullOrEmpty(StringUtil.RTrim( Form.Class)) ? "form-horizontal Form" : Form.Class)+"-fx");
         context.WriteHtmlText( ">") ;
         Draw( ) ;
         context.WriteHtmlText( "</div>") ;
      }

      public override void DispatchEvents( )
      {
         Process( ) ;
      }

      public override bool HasEnterEvent( )
      {
         return true ;
      }

      public override GXWebForm GetForm( )
      {
         return Form ;
      }

      public override String GetSelfLink( )
      {
         return formatLink("emergenciabitacora.aspx")  ;
      }

      public override String GetPgmname( )
      {
         return "EmergenciaBitacora" ;
      }

      public override String GetPgmdesc( )
      {
         return "Emergencia Bitacora" ;
      }

      protected void InitializeNonKey057( )
      {
         A44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A44EmergenciaFecha", context.localUtil.TToC( A44EmergenciaFecha, 8, 5, 0, 3, "/", ":", " "));
         A45EmergenciaEstado = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A45EmergenciaEstado", StringUtil.LTrim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0)));
         A41EmergenciaPaciente = (Guid)(Guid.Empty);
         n41EmergenciaPaciente = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A41EmergenciaPaciente", A41EmergenciaPaciente.ToString());
         n41EmergenciaPaciente = ((Guid.Empty==A41EmergenciaPaciente) ? true : false);
         A49EmergenciaPacienteNombre = "";
         n49EmergenciaPacienteNombre = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A49EmergenciaPacienteNombre", A49EmergenciaPacienteNombre);
         A42EmergenciaCoach = (Guid)(Guid.Empty);
         n42EmergenciaCoach = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A42EmergenciaCoach", A42EmergenciaCoach.ToString());
         n42EmergenciaCoach = ((Guid.Empty==A42EmergenciaCoach) ? true : false);
         A46EmergenciaLongitud = "";
         n46EmergenciaLongitud = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A46EmergenciaLongitud", A46EmergenciaLongitud);
         n46EmergenciaLongitud = (String.IsNullOrEmpty(StringUtil.RTrim( A46EmergenciaLongitud)) ? true : false);
         A47EmergenciaLatitud = "";
         n47EmergenciaLatitud = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A47EmergenciaLatitud", A47EmergenciaLatitud);
         n47EmergenciaLatitud = (String.IsNullOrEmpty(StringUtil.RTrim( A47EmergenciaLatitud)) ? true : false);
         A48EmergenciaMensaje = "";
         n48EmergenciaMensaje = false;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A48EmergenciaMensaje", A48EmergenciaMensaje);
         n48EmergenciaMensaje = (String.IsNullOrEmpty(StringUtil.RTrim( A48EmergenciaMensaje)) ? true : false);
         Z44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         Z45EmergenciaEstado = 0;
         Z41EmergenciaPaciente = (Guid)(Guid.Empty);
         Z42EmergenciaCoach = (Guid)(Guid.Empty);
      }

      protected void InitAll057( )
      {
         A43EmergenciaID = (Guid)(Guid.NewGuid( ));
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "A43EmergenciaID", A43EmergenciaID.ToString());
         InitializeNonKey057( ) ;
      }

      protected void StandaloneModalInsert( )
      {
      }

      protected void define_styles( )
      {
         AddStyleSheetFile("calendar-system.css", "");
         AddThemeStyleSheetFile("", context.GetTheme( )+".css", "?"+GetCacheInvalidationToken( ), true);
         bool outputEnabled = isOutputEnabled( ) ;
         if ( context.isSpaRequest( ) )
         {
            enableOutput();
         }
         idxLst = 1;
         while ( idxLst <= Form.Jscriptsrc.Count )
         {
            context.AddJavascriptSource(StringUtil.RTrim( ((String)Form.Jscriptsrc.Item(idxLst))), "?201811181734863", true);
            idxLst = (int)(idxLst+1);
         }
         if ( ! outputEnabled )
         {
            if ( context.isSpaRequest( ) )
            {
               disableOutput();
            }
         }
         /* End function define_styles */
      }

      protected void include_jscripts( )
      {
         context.AddJavascriptSource("messages.spa.js", "?"+GetCacheInvalidationToken( ), false);
         context.AddJavascriptSource("emergenciabitacora.js", "?201811181734863", false);
         /* End function include_jscripts */
      }

      protected void init_default_properties( )
      {
         lblTitle_Internalname = "TITLE";
         bttBtn_first_Internalname = "BTN_FIRST";
         bttBtn_previous_Internalname = "BTN_PREVIOUS";
         bttBtn_next_Internalname = "BTN_NEXT";
         bttBtn_last_Internalname = "BTN_LAST";
         bttBtn_select_Internalname = "BTN_SELECT";
         edtEmergenciaID_Internalname = "EMERGENCIAID";
         edtEmergenciaFecha_Internalname = "EMERGENCIAFECHA";
         cmbEmergenciaEstado_Internalname = "EMERGENCIAESTADO";
         edtEmergenciaPaciente_Internalname = "EMERGENCIAPACIENTE";
         edtEmergenciaPacienteNombre_Internalname = "EMERGENCIAPACIENTENOMBRE";
         edtEmergenciaCoach_Internalname = "EMERGENCIACOACH";
         edtEmergenciaLongitud_Internalname = "EMERGENCIALONGITUD";
         edtEmergenciaLatitud_Internalname = "EMERGENCIALATITUD";
         edtEmergenciaMensaje_Internalname = "EMERGENCIAMENSAJE";
         bttBtn_enter_Internalname = "BTN_ENTER";
         bttBtn_cancel_Internalname = "BTN_CANCEL";
         bttBtn_delete_Internalname = "BTN_DELETE";
         divTablemain_Internalname = "TABLEMAIN";
         Form.Internalname = "FORM";
         imgprompt_41_Internalname = "PROMPT_41";
         imgprompt_42_Internalname = "PROMPT_42";
      }

      public override void initialize_properties( )
      {
         if ( context.isSpaRequest( ) )
         {
            disableJsOutput();
         }
         init_default_properties( ) ;
         Form.Headerrawhtml = "";
         Form.Background = "";
         Form.Textcolor = 0;
         Form.Backcolor = (int)(0xFFFFFF);
         Form.Caption = "Emergencia Bitacora";
         bttBtn_delete_Enabled = 1;
         bttBtn_delete_Visible = 1;
         bttBtn_cancel_Visible = 1;
         bttBtn_enter_Enabled = 1;
         bttBtn_enter_Visible = 1;
         edtEmergenciaMensaje_Enabled = 1;
         edtEmergenciaLatitud_Enabled = 1;
         edtEmergenciaLongitud_Enabled = 1;
         imgprompt_42_Visible = 1;
         imgprompt_42_Link = "";
         edtEmergenciaCoach_Jsonclick = "";
         edtEmergenciaCoach_Enabled = 1;
         edtEmergenciaPacienteNombre_Jsonclick = "";
         edtEmergenciaPacienteNombre_Enabled = 0;
         imgprompt_41_Visible = 1;
         imgprompt_41_Link = "";
         edtEmergenciaPaciente_Jsonclick = "";
         edtEmergenciaPaciente_Enabled = 1;
         cmbEmergenciaEstado_Jsonclick = "";
         cmbEmergenciaEstado.Enabled = 1;
         edtEmergenciaFecha_Jsonclick = "";
         edtEmergenciaFecha_Enabled = 1;
         edtEmergenciaID_Jsonclick = "";
         edtEmergenciaID_Enabled = 1;
         bttBtn_select_Visible = 1;
         bttBtn_last_Visible = 1;
         bttBtn_next_Visible = 1;
         bttBtn_previous_Visible = 1;
         bttBtn_first_Visible = 1;
         context.GX_msglist.DisplayMode = 1;
         if ( context.isSpaRequest( ) )
         {
            enableJsOutput();
         }
      }

      protected void dynload_actions( )
      {
         /* End function dynload_actions */
      }

      protected void AfterKeyLoadScreen( )
      {
         IsConfirmed = 0;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "IsConfirmed", StringUtil.LTrim( StringUtil.Str( (decimal)(IsConfirmed), 4, 0)));
         getEqualNoModal( ) ;
         GX_FocusControl = edtEmergenciaFecha_Internalname;
         context.httpAjaxContext.ajax_rsp_assign_attri("", false, "GX_FocusControl", GX_FocusControl);
         standaloneNotModal( ) ;
         standaloneModal( ) ;
         /* End function AfterKeyLoadScreen */
      }

      public void Valid_Emergenciaid( Guid GX_Parm1 ,
                                      DateTime GX_Parm2 ,
                                      GXCombobox cmbGX_Parm3 ,
                                      String GX_Parm4 ,
                                      String GX_Parm5 ,
                                      String GX_Parm6 ,
                                      Guid GX_Parm7 ,
                                      Guid GX_Parm8 )
      {
         A43EmergenciaID = (Guid)(GX_Parm1);
         A44EmergenciaFecha = GX_Parm2;
         cmbEmergenciaEstado = cmbGX_Parm3;
         A45EmergenciaEstado = (short)(NumberUtil.Val( cmbEmergenciaEstado.CurrentValue, "."));
         cmbEmergenciaEstado.CurrentValue = StringUtil.LTrim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0));
         A46EmergenciaLongitud = GX_Parm4;
         n46EmergenciaLongitud = false;
         A47EmergenciaLatitud = GX_Parm5;
         n47EmergenciaLatitud = false;
         A48EmergenciaMensaje = GX_Parm6;
         n48EmergenciaMensaje = false;
         A41EmergenciaPaciente = (Guid)(GX_Parm7);
         n41EmergenciaPaciente = false;
         A42EmergenciaCoach = (Guid)(GX_Parm8);
         n42EmergenciaCoach = false;
         context.wbHandled = 1;
         AfterKeyLoadScreen( ) ;
         Draw( ) ;
         send_integrity_footer_hashes( ) ;
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A49EmergenciaPacienteNombre = "";
            n49EmergenciaPacienteNombre = false;
         }
         isValidOutput.Add(context.localUtil.TToC( A44EmergenciaFecha, 10, 8, 0, 3, "/", ":", " "));
         cmbEmergenciaEstado.CurrentValue = StringUtil.Trim( StringUtil.Str( (decimal)(A45EmergenciaEstado), 4, 0));
         isValidOutput.Add(cmbEmergenciaEstado);
         isValidOutput.Add(A41EmergenciaPaciente.ToString());
         isValidOutput.Add(A42EmergenciaCoach.ToString());
         isValidOutput.Add(A46EmergenciaLongitud);
         isValidOutput.Add(A47EmergenciaLatitud);
         isValidOutput.Add(A48EmergenciaMensaje);
         isValidOutput.Add(A49EmergenciaPacienteNombre);
         isValidOutput.Add(StringUtil.RTrim( Gx_mode));
         isValidOutput.Add(Z43EmergenciaID.ToString());
         isValidOutput.Add(context.localUtil.TToC( Z44EmergenciaFecha, 10, 8, 0, 0, "/", ":", " "));
         isValidOutput.Add(StringUtil.LTrim( StringUtil.NToC( (decimal)(Z45EmergenciaEstado), 4, 0, ",", "")));
         isValidOutput.Add(Z41EmergenciaPaciente.ToString());
         isValidOutput.Add(Z42EmergenciaCoach.ToString());
         isValidOutput.Add(Z46EmergenciaLongitud);
         isValidOutput.Add(Z47EmergenciaLatitud);
         isValidOutput.Add(Z48EmergenciaMensaje);
         isValidOutput.Add(Z49EmergenciaPacienteNombre);
         isValidOutput.Add(bttBtn_delete_Enabled);
         isValidOutput.Add(bttBtn_enter_Enabled);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Emergenciapaciente( Guid GX_Parm1 ,
                                            String GX_Parm2 )
      {
         A41EmergenciaPaciente = (Guid)(GX_Parm1);
         n41EmergenciaPaciente = false;
         A49EmergenciaPacienteNombre = GX_Parm2;
         n49EmergenciaPacienteNombre = false;
         /* Using cursor T000515 */
         pr_default.execute(13, new Object[] {n41EmergenciaPaciente, A41EmergenciaPaciente});
         if ( (pr_default.getStatus(13) == 101) )
         {
            if ( ! ( (Guid.Empty==A41EmergenciaPaciente) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Paciente ST'.", "ForeignKeyNotFound", 1, "EMERGENCIAPACIENTE");
               AnyError = 1;
               GX_FocusControl = edtEmergenciaPaciente_Internalname;
            }
         }
         A49EmergenciaPacienteNombre = T000515_A49EmergenciaPacienteNombre[0];
         n49EmergenciaPacienteNombre = T000515_n49EmergenciaPacienteNombre[0];
         pr_default.close(13);
         dynload_actions( ) ;
         if ( AnyError == 1 )
         {
            A49EmergenciaPacienteNombre = "";
            n49EmergenciaPacienteNombre = false;
         }
         isValidOutput.Add(A49EmergenciaPacienteNombre);
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public void Valid_Emergenciacoach( Guid GX_Parm1 )
      {
         A42EmergenciaCoach = (Guid)(GX_Parm1);
         n42EmergenciaCoach = false;
         /* Using cursor T000517 */
         pr_default.execute(15, new Object[] {n42EmergenciaCoach, A42EmergenciaCoach});
         if ( (pr_default.getStatus(15) == 101) )
         {
            if ( ! ( (Guid.Empty==A42EmergenciaCoach) ) )
            {
               GX_msglist.addItem("No existe 'Emergencia Coach ST'.", "ForeignKeyNotFound", 1, "EMERGENCIACOACH");
               AnyError = 1;
               GX_FocusControl = edtEmergenciaCoach_Internalname;
            }
         }
         pr_default.close(15);
         dynload_actions( ) ;
         isValidOutput.Add(context.GX_msglist.ToJavascriptSource());
         isValidOutput.Add(context.httpAjaxContext.ajax_rsp_get_hiddens( ));
         context.GX_webresponse.AddString(isValidOutput.ToJSonString());
         wbTemp = context.ResponseContentType( "application/json");
      }

      public override bool SupportAjaxEvent( )
      {
         return true ;
      }

      public override void InitializeDynEvents( )
      {
         setEventMetadata("ENTER","{handler:'UserMainFullajax',iparms:[{postForm:true}],oparms:[]}");
         setEventMetadata("REFRESH","{handler:'Refresh',iparms:[],oparms:[]}");
         return  ;
      }

      public override void cleanup( )
      {
         flushBuffer();
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
      }

      protected void CloseOpenCursors( )
      {
         pr_default.close(1);
         pr_default.close(13);
         pr_default.close(15);
      }

      public override void initialize( )
      {
         sPrefix = "";
         Z43EmergenciaID = (Guid)(Guid.Empty);
         Z44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         Z41EmergenciaPaciente = (Guid)(Guid.Empty);
         Z42EmergenciaCoach = (Guid)(Guid.Empty);
         scmdbuf = "";
         gxfirstwebparm = "";
         gxfirstwebparm_bkp = "";
         A41EmergenciaPaciente = (Guid)(Guid.Empty);
         A42EmergenciaCoach = (Guid)(Guid.Empty);
         GXKey = "";
         PreviousTooltip = "";
         PreviousCaption = "";
         Form = new GXWebForm();
         GX_FocusControl = "";
         lblTitle_Jsonclick = "";
         TempTags = "";
         ClassString = "";
         StyleString = "";
         bttBtn_first_Jsonclick = "";
         bttBtn_previous_Jsonclick = "";
         bttBtn_next_Jsonclick = "";
         bttBtn_last_Jsonclick = "";
         bttBtn_select_Jsonclick = "";
         A43EmergenciaID = (Guid)(Guid.Empty);
         A44EmergenciaFecha = (DateTime)(DateTime.MinValue);
         sImgUrl = "";
         A49EmergenciaPacienteNombre = "";
         A46EmergenciaLongitud = "";
         A47EmergenciaLatitud = "";
         A48EmergenciaMensaje = "";
         bttBtn_enter_Jsonclick = "";
         bttBtn_cancel_Jsonclick = "";
         bttBtn_delete_Jsonclick = "";
         Gx_mode = "";
         sEvt = "";
         EvtGridId = "";
         EvtRowId = "";
         sEvtType = "";
         Z46EmergenciaLongitud = "";
         Z47EmergenciaLatitud = "";
         Z48EmergenciaMensaje = "";
         Z49EmergenciaPacienteNombre = "";
         T00056_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T00056_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         T00056_A45EmergenciaEstado = new short[1] ;
         T00056_A49EmergenciaPacienteNombre = new String[] {""} ;
         T00056_n49EmergenciaPacienteNombre = new bool[] {false} ;
         T00056_A46EmergenciaLongitud = new String[] {""} ;
         T00056_n46EmergenciaLongitud = new bool[] {false} ;
         T00056_A47EmergenciaLatitud = new String[] {""} ;
         T00056_n47EmergenciaLatitud = new bool[] {false} ;
         T00056_A48EmergenciaMensaje = new String[] {""} ;
         T00056_n48EmergenciaMensaje = new bool[] {false} ;
         T00056_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         T00056_n41EmergenciaPaciente = new bool[] {false} ;
         T00056_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         T00056_n42EmergenciaCoach = new bool[] {false} ;
         T00054_A49EmergenciaPacienteNombre = new String[] {""} ;
         T00054_n49EmergenciaPacienteNombre = new bool[] {false} ;
         T00055_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         T00055_n42EmergenciaCoach = new bool[] {false} ;
         T00057_A49EmergenciaPacienteNombre = new String[] {""} ;
         T00057_n49EmergenciaPacienteNombre = new bool[] {false} ;
         T00058_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         T00058_n42EmergenciaCoach = new bool[] {false} ;
         T00059_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T00053_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T00053_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         T00053_A45EmergenciaEstado = new short[1] ;
         T00053_A46EmergenciaLongitud = new String[] {""} ;
         T00053_n46EmergenciaLongitud = new bool[] {false} ;
         T00053_A47EmergenciaLatitud = new String[] {""} ;
         T00053_n47EmergenciaLatitud = new bool[] {false} ;
         T00053_A48EmergenciaMensaje = new String[] {""} ;
         T00053_n48EmergenciaMensaje = new bool[] {false} ;
         T00053_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         T00053_n41EmergenciaPaciente = new bool[] {false} ;
         T00053_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         T00053_n42EmergenciaCoach = new bool[] {false} ;
         sMode7 = "";
         T000510_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T000511_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T00052_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         T00052_A44EmergenciaFecha = new DateTime[] {DateTime.MinValue} ;
         T00052_A45EmergenciaEstado = new short[1] ;
         T00052_A46EmergenciaLongitud = new String[] {""} ;
         T00052_n46EmergenciaLongitud = new bool[] {false} ;
         T00052_A47EmergenciaLatitud = new String[] {""} ;
         T00052_n47EmergenciaLatitud = new bool[] {false} ;
         T00052_A48EmergenciaMensaje = new String[] {""} ;
         T00052_n48EmergenciaMensaje = new bool[] {false} ;
         T00052_A41EmergenciaPaciente = new Guid[] {Guid.Empty} ;
         T00052_n41EmergenciaPaciente = new bool[] {false} ;
         T00052_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         T00052_n42EmergenciaCoach = new bool[] {false} ;
         T000515_A49EmergenciaPacienteNombre = new String[] {""} ;
         T000515_n49EmergenciaPacienteNombre = new bool[] {false} ;
         T000516_A43EmergenciaID = new Guid[] {Guid.Empty} ;
         sDynURL = "";
         FormProcess = "";
         bodyStyle = "";
         isValidOutput = new GxUnknownObjectCollection();
         T000517_A42EmergenciaCoach = new Guid[] {Guid.Empty} ;
         T000517_n42EmergenciaCoach = new bool[] {false} ;
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.emergenciabitacora__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.emergenciabitacora__default(),
            new Object[][] {
                new Object[] {
               T00052_A43EmergenciaID, T00052_A44EmergenciaFecha, T00052_A45EmergenciaEstado, T00052_A46EmergenciaLongitud, T00052_n46EmergenciaLongitud, T00052_A47EmergenciaLatitud, T00052_n47EmergenciaLatitud, T00052_A48EmergenciaMensaje, T00052_n48EmergenciaMensaje, T00052_A41EmergenciaPaciente,
               T00052_n41EmergenciaPaciente, T00052_A42EmergenciaCoach, T00052_n42EmergenciaCoach
               }
               , new Object[] {
               T00053_A43EmergenciaID, T00053_A44EmergenciaFecha, T00053_A45EmergenciaEstado, T00053_A46EmergenciaLongitud, T00053_n46EmergenciaLongitud, T00053_A47EmergenciaLatitud, T00053_n47EmergenciaLatitud, T00053_A48EmergenciaMensaje, T00053_n48EmergenciaMensaje, T00053_A41EmergenciaPaciente,
               T00053_n41EmergenciaPaciente, T00053_A42EmergenciaCoach, T00053_n42EmergenciaCoach
               }
               , new Object[] {
               T00054_A49EmergenciaPacienteNombre, T00054_n49EmergenciaPacienteNombre
               }
               , new Object[] {
               T00055_A42EmergenciaCoach
               }
               , new Object[] {
               T00056_A43EmergenciaID, T00056_A44EmergenciaFecha, T00056_A45EmergenciaEstado, T00056_A49EmergenciaPacienteNombre, T00056_n49EmergenciaPacienteNombre, T00056_A46EmergenciaLongitud, T00056_n46EmergenciaLongitud, T00056_A47EmergenciaLatitud, T00056_n47EmergenciaLatitud, T00056_A48EmergenciaMensaje,
               T00056_n48EmergenciaMensaje, T00056_A41EmergenciaPaciente, T00056_n41EmergenciaPaciente, T00056_A42EmergenciaCoach, T00056_n42EmergenciaCoach
               }
               , new Object[] {
               T00057_A49EmergenciaPacienteNombre, T00057_n49EmergenciaPacienteNombre
               }
               , new Object[] {
               T00058_A42EmergenciaCoach
               }
               , new Object[] {
               T00059_A43EmergenciaID
               }
               , new Object[] {
               T000510_A43EmergenciaID
               }
               , new Object[] {
               T000511_A43EmergenciaID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               T000515_A49EmergenciaPacienteNombre, T000515_n49EmergenciaPacienteNombre
               }
               , new Object[] {
               T000516_A43EmergenciaID
               }
               , new Object[] {
               T000517_A42EmergenciaCoach
               }
            }
         );
         Z43EmergenciaID = (Guid)(Guid.NewGuid( ));
         A43EmergenciaID = (Guid)(Guid.NewGuid( ));
      }

      private short Z45EmergenciaEstado ;
      private short GxWebError ;
      private short gxcookieaux ;
      private short A45EmergenciaEstado ;
      private short IsConfirmed ;
      private short IsModified ;
      private short AnyError ;
      private short nKeyPressed ;
      private short initialized ;
      private short Gx_BScreen ;
      private short GX_JID ;
      private short RcdFound7 ;
      private short gxajaxcallmode ;
      private short wbTemp ;
      private int trnEnded ;
      private int bttBtn_first_Visible ;
      private int bttBtn_previous_Visible ;
      private int bttBtn_next_Visible ;
      private int bttBtn_last_Visible ;
      private int bttBtn_select_Visible ;
      private int edtEmergenciaID_Enabled ;
      private int edtEmergenciaFecha_Enabled ;
      private int edtEmergenciaPaciente_Enabled ;
      private int imgprompt_41_Visible ;
      private int edtEmergenciaPacienteNombre_Enabled ;
      private int edtEmergenciaCoach_Enabled ;
      private int imgprompt_42_Visible ;
      private int edtEmergenciaLongitud_Enabled ;
      private int edtEmergenciaLatitud_Enabled ;
      private int edtEmergenciaMensaje_Enabled ;
      private int bttBtn_enter_Visible ;
      private int bttBtn_enter_Enabled ;
      private int bttBtn_cancel_Visible ;
      private int bttBtn_delete_Visible ;
      private int bttBtn_delete_Enabled ;
      private int idxLst ;
      private String sPrefix ;
      private String scmdbuf ;
      private String gxfirstwebparm ;
      private String gxfirstwebparm_bkp ;
      private String GXKey ;
      private String PreviousTooltip ;
      private String PreviousCaption ;
      private String GX_FocusControl ;
      private String edtEmergenciaID_Internalname ;
      private String cmbEmergenciaEstado_Internalname ;
      private String divTablemain_Internalname ;
      private String lblTitle_Internalname ;
      private String lblTitle_Jsonclick ;
      private String TempTags ;
      private String ClassString ;
      private String StyleString ;
      private String bttBtn_first_Internalname ;
      private String bttBtn_first_Jsonclick ;
      private String bttBtn_previous_Internalname ;
      private String bttBtn_previous_Jsonclick ;
      private String bttBtn_next_Internalname ;
      private String bttBtn_next_Jsonclick ;
      private String bttBtn_last_Internalname ;
      private String bttBtn_last_Jsonclick ;
      private String bttBtn_select_Internalname ;
      private String bttBtn_select_Jsonclick ;
      private String edtEmergenciaID_Jsonclick ;
      private String edtEmergenciaFecha_Internalname ;
      private String edtEmergenciaFecha_Jsonclick ;
      private String cmbEmergenciaEstado_Jsonclick ;
      private String edtEmergenciaPaciente_Internalname ;
      private String edtEmergenciaPaciente_Jsonclick ;
      private String sImgUrl ;
      private String imgprompt_41_Internalname ;
      private String imgprompt_41_Link ;
      private String edtEmergenciaPacienteNombre_Internalname ;
      private String edtEmergenciaPacienteNombre_Jsonclick ;
      private String edtEmergenciaCoach_Internalname ;
      private String edtEmergenciaCoach_Jsonclick ;
      private String imgprompt_42_Internalname ;
      private String imgprompt_42_Link ;
      private String edtEmergenciaLongitud_Internalname ;
      private String edtEmergenciaLatitud_Internalname ;
      private String edtEmergenciaMensaje_Internalname ;
      private String bttBtn_enter_Internalname ;
      private String bttBtn_enter_Jsonclick ;
      private String bttBtn_cancel_Internalname ;
      private String bttBtn_cancel_Jsonclick ;
      private String bttBtn_delete_Internalname ;
      private String bttBtn_delete_Jsonclick ;
      private String Gx_mode ;
      private String sEvt ;
      private String EvtGridId ;
      private String EvtRowId ;
      private String sEvtType ;
      private String sMode7 ;
      private String sDynURL ;
      private String FormProcess ;
      private String bodyStyle ;
      private DateTime Z44EmergenciaFecha ;
      private DateTime A44EmergenciaFecha ;
      private bool entryPointCalled ;
      private bool n41EmergenciaPaciente ;
      private bool n42EmergenciaCoach ;
      private bool toggleJsOutput ;
      private bool wbErr ;
      private bool n49EmergenciaPacienteNombre ;
      private bool n46EmergenciaLongitud ;
      private bool n47EmergenciaLatitud ;
      private bool n48EmergenciaMensaje ;
      private String A46EmergenciaLongitud ;
      private String A47EmergenciaLatitud ;
      private String A48EmergenciaMensaje ;
      private String Z46EmergenciaLongitud ;
      private String Z47EmergenciaLatitud ;
      private String Z48EmergenciaMensaje ;
      private String A49EmergenciaPacienteNombre ;
      private String Z49EmergenciaPacienteNombre ;
      private Guid Z43EmergenciaID ;
      private Guid Z41EmergenciaPaciente ;
      private Guid Z42EmergenciaCoach ;
      private Guid A41EmergenciaPaciente ;
      private Guid A42EmergenciaCoach ;
      private Guid A43EmergenciaID ;
      private GxUnknownObjectCollection isValidOutput ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private GXCombobox cmbEmergenciaEstado ;
      private IDataStoreProvider pr_default ;
      private Guid[] T00056_A43EmergenciaID ;
      private DateTime[] T00056_A44EmergenciaFecha ;
      private short[] T00056_A45EmergenciaEstado ;
      private String[] T00056_A49EmergenciaPacienteNombre ;
      private bool[] T00056_n49EmergenciaPacienteNombre ;
      private String[] T00056_A46EmergenciaLongitud ;
      private bool[] T00056_n46EmergenciaLongitud ;
      private String[] T00056_A47EmergenciaLatitud ;
      private bool[] T00056_n47EmergenciaLatitud ;
      private String[] T00056_A48EmergenciaMensaje ;
      private bool[] T00056_n48EmergenciaMensaje ;
      private Guid[] T00056_A41EmergenciaPaciente ;
      private bool[] T00056_n41EmergenciaPaciente ;
      private Guid[] T00056_A42EmergenciaCoach ;
      private bool[] T00056_n42EmergenciaCoach ;
      private String[] T00054_A49EmergenciaPacienteNombre ;
      private bool[] T00054_n49EmergenciaPacienteNombre ;
      private Guid[] T00055_A42EmergenciaCoach ;
      private bool[] T00055_n42EmergenciaCoach ;
      private String[] T00057_A49EmergenciaPacienteNombre ;
      private bool[] T00057_n49EmergenciaPacienteNombre ;
      private Guid[] T00058_A42EmergenciaCoach ;
      private bool[] T00058_n42EmergenciaCoach ;
      private Guid[] T00059_A43EmergenciaID ;
      private Guid[] T00053_A43EmergenciaID ;
      private DateTime[] T00053_A44EmergenciaFecha ;
      private short[] T00053_A45EmergenciaEstado ;
      private String[] T00053_A46EmergenciaLongitud ;
      private bool[] T00053_n46EmergenciaLongitud ;
      private String[] T00053_A47EmergenciaLatitud ;
      private bool[] T00053_n47EmergenciaLatitud ;
      private String[] T00053_A48EmergenciaMensaje ;
      private bool[] T00053_n48EmergenciaMensaje ;
      private Guid[] T00053_A41EmergenciaPaciente ;
      private bool[] T00053_n41EmergenciaPaciente ;
      private Guid[] T00053_A42EmergenciaCoach ;
      private bool[] T00053_n42EmergenciaCoach ;
      private Guid[] T000510_A43EmergenciaID ;
      private Guid[] T000511_A43EmergenciaID ;
      private Guid[] T00052_A43EmergenciaID ;
      private DateTime[] T00052_A44EmergenciaFecha ;
      private short[] T00052_A45EmergenciaEstado ;
      private String[] T00052_A46EmergenciaLongitud ;
      private bool[] T00052_n46EmergenciaLongitud ;
      private String[] T00052_A47EmergenciaLatitud ;
      private bool[] T00052_n47EmergenciaLatitud ;
      private String[] T00052_A48EmergenciaMensaje ;
      private bool[] T00052_n48EmergenciaMensaje ;
      private Guid[] T00052_A41EmergenciaPaciente ;
      private bool[] T00052_n41EmergenciaPaciente ;
      private Guid[] T00052_A42EmergenciaCoach ;
      private bool[] T00052_n42EmergenciaCoach ;
      private String[] T000515_A49EmergenciaPacienteNombre ;
      private bool[] T000515_n49EmergenciaPacienteNombre ;
      private IDataStoreProvider pr_gam ;
      private Guid[] T000516_A43EmergenciaID ;
      private Guid[] T000517_A42EmergenciaCoach ;
      private bool[] T000517_n42EmergenciaCoach ;
      private GXWebForm Form ;
   }

   public class emergenciabitacora__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class emergenciabitacora__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new ForEachCursor(def[1])
       ,new ForEachCursor(def[2])
       ,new ForEachCursor(def[3])
       ,new ForEachCursor(def[4])
       ,new ForEachCursor(def[5])
       ,new ForEachCursor(def[6])
       ,new ForEachCursor(def[7])
       ,new ForEachCursor(def[8])
       ,new ForEachCursor(def[9])
       ,new UpdateCursor(def[10])
       ,new UpdateCursor(def[11])
       ,new UpdateCursor(def[12])
       ,new ForEachCursor(def[13])
       ,new ForEachCursor(def[14])
       ,new ForEachCursor(def[15])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmT00056 ;
        prmT00056 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00054 ;
        prmT00054 = new Object[] {
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00055 ;
        prmT00055 = new Object[] {
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00057 ;
        prmT00057 = new Object[] {
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00058 ;
        prmT00058 = new Object[] {
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00059 ;
        prmT00059 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00053 ;
        prmT00053 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000510 ;
        prmT000510 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000511 ;
        prmT000511 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT00052 ;
        prmT00052 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000512 ;
        prmT000512 = new Object[] {
        new Object[] {"@EmergenciaFecha",SqlDbType.DateTime,8,5} ,
        new Object[] {"@EmergenciaEstado",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@EmergenciaLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaMensaje",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000513 ;
        prmT000513 = new Object[] {
        new Object[] {"@EmergenciaFecha",SqlDbType.DateTime,8,5} ,
        new Object[] {"@EmergenciaEstado",SqlDbType.SmallInt,4,0} ,
        new Object[] {"@EmergenciaLongitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaLatitud",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaMensaje",SqlDbType.VarChar,2097152,0} ,
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0} ,
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000514 ;
        prmT000514 = new Object[] {
        new Object[] {"@EmergenciaID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000516 ;
        prmT000516 = new Object[] {
        } ;
        Object[] prmT000515 ;
        prmT000515 = new Object[] {
        new Object[] {"@EmergenciaPaciente",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmT000517 ;
        prmT000517 = new Object[] {
        new Object[] {"@EmergenciaCoach",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("T00052", "SELECT [EmergenciaID], [EmergenciaFecha], [EmergenciaEstado], [EmergenciaLongitud], [EmergenciaLatitud], [EmergenciaMensaje], [EmergenciaPaciente] AS EmergenciaPaciente, [EmergenciaCoach] AS EmergenciaCoach FROM [EmergenciaBitacora] WITH (UPDLOCK) WHERE [EmergenciaID] = @EmergenciaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00052,1,0,true,false )
           ,new CursorDef("T00053", "SELECT [EmergenciaID], [EmergenciaFecha], [EmergenciaEstado], [EmergenciaLongitud], [EmergenciaLatitud], [EmergenciaMensaje], [EmergenciaPaciente] AS EmergenciaPaciente, [EmergenciaCoach] AS EmergenciaCoach FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaID] = @EmergenciaID ",true, GxErrorMask.GX_NOMASK, false, this,prmT00053,1,0,true,false )
           ,new CursorDef("T00054", "SELECT [PersonaPNombre] AS EmergenciaPacienteNombre FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaPaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT00054,1,0,true,false )
           ,new CursorDef("T00055", "SELECT [PersonaID] AS EmergenciaCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmT00055,1,0,true,false )
           ,new CursorDef("T00056", "SELECT TM1.[EmergenciaID], TM1.[EmergenciaFecha], TM1.[EmergenciaEstado], T2.[PersonaPNombre] AS EmergenciaPacienteNombre, TM1.[EmergenciaLongitud], TM1.[EmergenciaLatitud], TM1.[EmergenciaMensaje], TM1.[EmergenciaPaciente] AS EmergenciaPaciente, TM1.[EmergenciaCoach] AS EmergenciaCoach FROM ([EmergenciaBitacora] TM1 WITH (NOLOCK) LEFT JOIN [Persona] T2 WITH (NOLOCK) ON T2.[PersonaID] = TM1.[EmergenciaPaciente]) WHERE TM1.[EmergenciaID] = @EmergenciaID ORDER BY TM1.[EmergenciaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT00056,100,0,true,false )
           ,new CursorDef("T00057", "SELECT [PersonaPNombre] AS EmergenciaPacienteNombre FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaPaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT00057,1,0,true,false )
           ,new CursorDef("T00058", "SELECT [PersonaID] AS EmergenciaCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmT00058,1,0,true,false )
           ,new CursorDef("T00059", "SELECT [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE [EmergenciaID] = @EmergenciaID  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT00059,1,0,true,false )
           ,new CursorDef("T000510", "SELECT TOP 1 [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE ( [EmergenciaID] > @EmergenciaID) ORDER BY [EmergenciaID]  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000510,1,0,true,true )
           ,new CursorDef("T000511", "SELECT TOP 1 [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) WHERE ( [EmergenciaID] < @EmergenciaID) ORDER BY [EmergenciaID] DESC  OPTION (FAST 1)",true, GxErrorMask.GX_NOMASK, false, this,prmT000511,1,0,true,true )
           ,new CursorDef("T000512", "INSERT INTO [EmergenciaBitacora]([EmergenciaFecha], [EmergenciaEstado], [EmergenciaLongitud], [EmergenciaLatitud], [EmergenciaMensaje], [EmergenciaPaciente], [EmergenciaCoach], [EmergenciaID]) VALUES(@EmergenciaFecha, @EmergenciaEstado, @EmergenciaLongitud, @EmergenciaLatitud, @EmergenciaMensaje, @EmergenciaPaciente, @EmergenciaCoach, @EmergenciaID)", GxErrorMask.GX_NOMASK,prmT000512)
           ,new CursorDef("T000513", "UPDATE [EmergenciaBitacora] SET [EmergenciaFecha]=@EmergenciaFecha, [EmergenciaEstado]=@EmergenciaEstado, [EmergenciaLongitud]=@EmergenciaLongitud, [EmergenciaLatitud]=@EmergenciaLatitud, [EmergenciaMensaje]=@EmergenciaMensaje, [EmergenciaPaciente]=@EmergenciaPaciente, [EmergenciaCoach]=@EmergenciaCoach  WHERE [EmergenciaID] = @EmergenciaID", GxErrorMask.GX_NOMASK,prmT000513)
           ,new CursorDef("T000514", "DELETE FROM [EmergenciaBitacora]  WHERE [EmergenciaID] = @EmergenciaID", GxErrorMask.GX_NOMASK,prmT000514)
           ,new CursorDef("T000515", "SELECT [PersonaPNombre] AS EmergenciaPacienteNombre FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaPaciente ",true, GxErrorMask.GX_NOMASK, false, this,prmT000515,1,0,true,false )
           ,new CursorDef("T000516", "SELECT [EmergenciaID] FROM [EmergenciaBitacora] WITH (NOLOCK) ORDER BY [EmergenciaID]  OPTION (FAST 100)",true, GxErrorMask.GX_NOMASK, false, this,prmT000516,100,0,true,false )
           ,new CursorDef("T000517", "SELECT [PersonaID] AS EmergenciaCoach FROM [Persona] WITH (NOLOCK) WHERE [PersonaID] = @EmergenciaCoach ",true, GxErrorMask.GX_NOMASK, false, this,prmT000517,1,0,true,false )
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[4])[0] = rslt.wasNull(4);
              ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((Guid[]) buf[11])[0] = rslt.getGuid(8) ;
              ((bool[]) buf[12])[0] = rslt.wasNull(8);
              return;
           case 1 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getLongVarchar(4) ;
              ((bool[]) buf[4])[0] = rslt.wasNull(4);
              ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((Guid[]) buf[9])[0] = rslt.getGuid(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((Guid[]) buf[11])[0] = rslt.getGuid(8) ;
              ((bool[]) buf[12])[0] = rslt.wasNull(8);
              return;
           case 2 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              return;
           case 3 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 4 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((DateTime[]) buf[1])[0] = rslt.getGXDateTime(2) ;
              ((short[]) buf[2])[0] = rslt.getShort(3) ;
              ((String[]) buf[3])[0] = rslt.getVarchar(4) ;
              ((bool[]) buf[4])[0] = rslt.wasNull(4);
              ((String[]) buf[5])[0] = rslt.getLongVarchar(5) ;
              ((bool[]) buf[6])[0] = rslt.wasNull(5);
              ((String[]) buf[7])[0] = rslt.getLongVarchar(6) ;
              ((bool[]) buf[8])[0] = rslt.wasNull(6);
              ((String[]) buf[9])[0] = rslt.getLongVarchar(7) ;
              ((bool[]) buf[10])[0] = rslt.wasNull(7);
              ((Guid[]) buf[11])[0] = rslt.getGuid(8) ;
              ((bool[]) buf[12])[0] = rslt.wasNull(8);
              ((Guid[]) buf[13])[0] = rslt.getGuid(9) ;
              ((bool[]) buf[14])[0] = rslt.wasNull(9);
              return;
           case 5 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              return;
           case 6 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 7 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 8 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 9 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 13 :
              ((String[]) buf[0])[0] = rslt.getVarchar(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              return;
           case 14 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
           case 15 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 2 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 3 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 4 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 5 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 6 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 7 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 8 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 9 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 10 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[9]);
              }
              if ( (bool)parms[10] )
              {
                 stmt.setNull( 7 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(7, (Guid)parms[11]);
              }
              stmt.SetParameter(8, (Guid)parms[12]);
              return;
           case 11 :
              stmt.SetParameterDatetime(1, (DateTime)parms[0]);
              stmt.SetParameter(2, (short)parms[1]);
              if ( (bool)parms[2] )
              {
                 stmt.setNull( 3 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(3, (String)parms[3]);
              }
              if ( (bool)parms[4] )
              {
                 stmt.setNull( 4 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(4, (String)parms[5]);
              }
              if ( (bool)parms[6] )
              {
                 stmt.setNull( 5 , SqlDbType.VarChar );
              }
              else
              {
                 stmt.SetParameter(5, (String)parms[7]);
              }
              if ( (bool)parms[8] )
              {
                 stmt.setNull( 6 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(6, (Guid)parms[9]);
              }
              if ( (bool)parms[10] )
              {
                 stmt.setNull( 7 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(7, (Guid)parms[11]);
              }
              stmt.SetParameter(8, (Guid)parms[12]);
              return;
           case 12 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 13 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
           case 15 :
              if ( (bool)parms[0] )
              {
                 stmt.setNull( 1 , SqlDbType.UniqueIdentifier );
              }
              else
              {
                 stmt.SetParameter(1, (Guid)parms[1]);
              }
              return;
     }
  }

}

}
