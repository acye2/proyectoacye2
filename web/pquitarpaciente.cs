/*
               File: PQuitarPaciente
        Description: PQuitar Paciente
             Author: GeneXus C# Generator version 15_0_9-121631
       Generated on: 11/18/2018 16:41:56.20
       Program type: Callable routine
          Main DBMS: SQL Server
*/
using System;
using System.Collections;
using GeneXus.Utils;
using GeneXus.Resources;
using GeneXus.Application;
using GeneXus.Metadata;
using GeneXus.Cryptography;
using System.Data;
using System.Data.SqlClient;
using GeneXus.Data;
using com.genexus;
using GeneXus.Data.ADO;
using GeneXus.Data.NTier;
using GeneXus.Data.NTier.ADO;
using System.Runtime.Remoting;
using GeneXus.WebControls;
using GeneXus.Http;
using GeneXus.Procedure;
using GeneXus.XML;
using GeneXus.Search;
using GeneXus.Encryption;
using GeneXus.Http.Client;
using System.Threading;
using System.Xml.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.Runtime.Serialization;
namespace GeneXus.Programs {
   public class pquitarpaciente : GXProcedure
   {
      public pquitarpaciente( )
      {
         context = new GxContext(  );
         DataStoreUtil.LoadDataStores( context);
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
         IsMain = true;
         context.SetDefaultTheme("K2BFlat");
      }

      public pquitarpaciente( IGxContext context )
      {
         this.context = context;
         IsMain = false;
         dsGAM = context.GetDataStore("GAM");
         dsDefault = context.GetDataStore("Default");
      }

      public void release( )
      {
      }

      public void execute( ref String aP0_Coach ,
                           ref String aP1_Paciente )
      {
         this.AV8Coach = aP0_Coach;
         this.AV9Paciente = aP1_Paciente;
         initialize();
         executePrivate();
         aP0_Coach=this.AV8Coach;
         aP1_Paciente=this.AV9Paciente;
      }

      public String executeUdp( ref String aP0_Coach )
      {
         this.AV8Coach = aP0_Coach;
         this.AV9Paciente = aP1_Paciente;
         initialize();
         executePrivate();
         aP0_Coach=this.AV8Coach;
         aP1_Paciente=this.AV9Paciente;
         return AV9Paciente ;
      }

      public void executeSubmit( ref String aP0_Coach ,
                                 ref String aP1_Paciente )
      {
         pquitarpaciente objpquitarpaciente;
         objpquitarpaciente = new pquitarpaciente();
         objpquitarpaciente.AV8Coach = aP0_Coach;
         objpquitarpaciente.AV9Paciente = aP1_Paciente;
         objpquitarpaciente.context.SetSubmitInitialConfig(context);
         objpquitarpaciente.initialize();
         ThreadPool.QueueUserWorkItem( PropagateCulture(new WaitCallback( executePrivateCatch )),objpquitarpaciente);
         aP0_Coach=this.AV8Coach;
         aP1_Paciente=this.AV9Paciente;
      }

      void executePrivateCatch( object stateInfo )
      {
         try
         {
            ((pquitarpaciente)stateInfo).executePrivate();
         }
         catch ( Exception e )
         {
            GXUtil.SaveToEventLog( "Design", e);
            throw e ;
         }
      }

      void executePrivate( )
      {
         /* GeneXus formulas */
         /* Output device settings */
         AV10CoachID = (Guid)(StringUtil.StrToGuid( AV8Coach));
         AV11PacienteID = (Guid)(StringUtil.StrToGuid( AV9Paciente));
         /* Using cursor P002N2 */
         pr_default.execute(0, new Object[] {AV11PacienteID});
         while ( (pr_default.getStatus(0) != 101) )
         {
            A21RecorridoDetallePaciente = (Guid)((Guid)(P002N2_A21RecorridoDetallePaciente[0]));
            n21RecorridoDetallePaciente = P002N2_n21RecorridoDetallePaciente[0];
            A23RecorridoID = (Guid)((Guid)(P002N2_A23RecorridoID[0]));
            AV14RecorridoID = (Guid)(A23RecorridoID);
            AV15Recorrido.Load(AV14RecorridoID);
            AV15Recorrido.Delete();
            AV15Recorrido.Save();
            pr_default.readNext(0);
         }
         pr_default.close(0);
         /* Optimized DELETE. */
         /* Using cursor P002N3 */
         pr_default.execute(1, new Object[] {AV11PacienteID});
         pr_default.close(1);
         dsDefault.SmartCacheProvider.SetUpdated("Obstaculo") ;
         /* End optimized DELETE. */
         /* Optimized DELETE. */
         /* Using cursor P002N4 */
         pr_default.execute(2, new Object[] {AV11PacienteID});
         pr_default.close(2);
         dsDefault.SmartCacheProvider.SetUpdated("EmergenciaBitacora") ;
         /* End optimized DELETE. */
         /* Optimized DELETE. */
         /* Using cursor P002N5 */
         pr_default.execute(3, new Object[] {AV11PacienteID});
         pr_default.close(3);
         dsDefault.SmartCacheProvider.SetUpdated("CoachPaciente") ;
         /* End optimized DELETE. */
         pr_gam.commit( "PQuitarPaciente");
         pr_default.commit( "PQuitarPaciente");
         this.cleanup();
      }

      public override void cleanup( )
      {
         pr_gam.commit( "PQuitarPaciente");
         pr_default.commit( "PQuitarPaciente");
         CloseOpenCursors();
         if ( IsMain )
         {
            context.CloseConnections() ;
         }
         exitApplication();
      }

      protected void CloseOpenCursors( )
      {
      }

      public override void initialize( )
      {
         AV10CoachID = (Guid)(Guid.Empty);
         AV11PacienteID = (Guid)(Guid.Empty);
         scmdbuf = "";
         P002N2_A21RecorridoDetallePaciente = new Guid[] {Guid.Empty} ;
         P002N2_n21RecorridoDetallePaciente = new bool[] {false} ;
         P002N2_A23RecorridoID = new Guid[] {Guid.Empty} ;
         A21RecorridoDetallePaciente = (Guid)(Guid.Empty);
         A23RecorridoID = (Guid)(Guid.Empty);
         AV14RecorridoID = (Guid)(Guid.Empty);
         AV15Recorrido = new SdtRecorrido(context);
         pr_gam = new DataStoreProvider(context, new GeneXus.Programs.pquitarpaciente__gam(),
            new Object[][] {
            }
         );
         pr_default = new DataStoreProvider(context, new GeneXus.Programs.pquitarpaciente__default(),
            new Object[][] {
                new Object[] {
               P002N2_A21RecorridoDetallePaciente, P002N2_n21RecorridoDetallePaciente, P002N2_A23RecorridoID
               }
               , new Object[] {
               }
               , new Object[] {
               }
               , new Object[] {
               }
            }
         );
         /* GeneXus formulas. */
         context.Gx_err = 0;
      }

      private String scmdbuf ;
      private bool n21RecorridoDetallePaciente ;
      private String AV8Coach ;
      private String AV9Paciente ;
      private Guid AV10CoachID ;
      private Guid AV11PacienteID ;
      private Guid A21RecorridoDetallePaciente ;
      private Guid A23RecorridoID ;
      private Guid AV14RecorridoID ;
      private IGxDataStore dsGAM ;
      private IGxDataStore dsDefault ;
      private String aP0_Coach ;
      private String aP1_Paciente ;
      private IDataStoreProvider pr_default ;
      private Guid[] P002N2_A21RecorridoDetallePaciente ;
      private bool[] P002N2_n21RecorridoDetallePaciente ;
      private Guid[] P002N2_A23RecorridoID ;
      private IDataStoreProvider pr_gam ;
      private SdtRecorrido AV15Recorrido ;
   }

   public class pquitarpaciente__gam : DataStoreHelperBase, IDataStoreHelper
   {
      public ICursor[] getCursors( )
      {
         cursorDefinitions();
         return new Cursor[] {
       };
    }

    private static CursorDef[] def;
    private void cursorDefinitions( )
    {
       if ( def == null )
       {
          def= new CursorDef[] {
          };
       }
    }

    public void getResults( int cursor ,
                            IFieldGetter rslt ,
                            Object[] buf )
    {
       switch ( cursor )
       {
       }
    }

    public void setParameters( int cursor ,
                               IFieldSetter stmt ,
                               Object[] parms )
    {
       switch ( cursor )
       {
       }
    }

    public String getDataStoreName( )
    {
       return "GAM";
    }

 }

 public class pquitarpaciente__default : DataStoreHelperBase, IDataStoreHelper
 {
    public ICursor[] getCursors( )
    {
       cursorDefinitions();
       return new Cursor[] {
        new ForEachCursor(def[0])
       ,new UpdateCursor(def[1])
       ,new UpdateCursor(def[2])
       ,new UpdateCursor(def[3])
     };
  }

  private static CursorDef[] def;
  private void cursorDefinitions( )
  {
     if ( def == null )
     {
        Object[] prmP002N2 ;
        prmP002N2 = new Object[] {
        new Object[] {"@AV11PacienteID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmP002N3 ;
        prmP002N3 = new Object[] {
        new Object[] {"@AV11PacienteID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmP002N4 ;
        prmP002N4 = new Object[] {
        new Object[] {"@AV11PacienteID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        Object[] prmP002N5 ;
        prmP002N5 = new Object[] {
        new Object[] {"@AV11PacienteID",SqlDbType.UniqueIdentifier,4,0}
        } ;
        def= new CursorDef[] {
            new CursorDef("P002N2", "SELECT DISTINCT NULL AS [RecorridoDetallePaciente], [RecorridoID] FROM ( SELECT TOP(100) PERCENT [RecorridoDetallePaciente], [RecorridoID] FROM [RecorridoDetalle] WITH (NOLOCK) WHERE [RecorridoDetallePaciente] = @AV11PacienteID ORDER BY [RecorridoDetallePaciente]) DistinctT ",false, GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK, false, this,prmP002N2,100,0,true,false )
           ,new CursorDef("P002N3", "DELETE FROM [Obstaculo]  WHERE [ObstaculoPacienteRegistra] = @AV11PacienteID", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002N3)
           ,new CursorDef("P002N4", "DELETE FROM [EmergenciaBitacora]  WHERE [EmergenciaPaciente] = @AV11PacienteID", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002N4)
           ,new CursorDef("P002N5", "DELETE FROM [CoachPaciente]  WHERE [CoachPaciente_Paciente] = @AV11PacienteID", GxErrorMask.GX_NOMASK | GxErrorMask.GX_MASKLOOPLOCK,prmP002N5)
        };
     }
  }

  public void getResults( int cursor ,
                          IFieldGetter rslt ,
                          Object[] buf )
  {
     switch ( cursor )
     {
           case 0 :
              ((Guid[]) buf[0])[0] = rslt.getGuid(1) ;
              ((bool[]) buf[1])[0] = rslt.wasNull(1);
              ((Guid[]) buf[2])[0] = rslt.getGuid(2) ;
              return;
     }
  }

  public void setParameters( int cursor ,
                             IFieldSetter stmt ,
                             Object[] parms )
  {
     switch ( cursor )
     {
           case 0 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 1 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 2 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
           case 3 :
              stmt.SetParameter(1, (Guid)parms[0]);
              return;
     }
  }

}

}
